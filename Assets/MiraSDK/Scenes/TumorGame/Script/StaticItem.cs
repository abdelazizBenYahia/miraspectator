﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticItem : MonoBehaviour {
    public GameObject Camera;
    public GameObject positionToFollow;
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
	// Use this for initialization
	void Start () {
        startTime = Time.time;
        journeyLength = Vector3.Distance(transform.position, positionToFollow.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(Camera.transform);
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(transform.position, positionToFollow.transform.position, fracJourney);
	}
}
