﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> {
    private AudioSource audiosource;
    public List<AudioClip> soundEffects;
	// Use this for initialization
	void Start () {
        audiosource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void playsound(int i)
    {

        audiosource.Stop();
        audiosource.clip = soundEffects[i];
        audiosource.Play();
    }
}
