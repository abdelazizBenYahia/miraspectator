﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {

public float moveSpeed;
public Transform TrackedItem;
private Vector3 moveDirection;
public Transform Camera;
public Transform Player;
public Transform scorePivot;
public float smooth = 2.0F;
    public string horizontalAxis = "Horizontal";
    public string verticalAxis = "Vertical";
    private float inputHorizontal;
    private float inputVertical;
    private Vector3 dir;
    private Vector3 dir2;
    private AudioManager am;
private GameManager gm;
 void Start()
{
        inputHorizontal = SimpleInput.GetAxis( horizontalAxis );

        inputVertical = SimpleInput.GetAxis( verticalAxis );
        am = AudioManager.Instance;
    gm = GameManager.Instance;
}




void FixedUpdate()
{
        //this makes the score text always looke at the camera
        scorePivot.transform.LookAt(Camera.transform);
        //rotation math for the planets
        inputHorizontal = SimpleInput.GetAxis(horizontalAxis);

        inputVertical = SimpleInput.GetAxis(verticalAxis);
    float x = 0, y = 0;
        x = inputHorizontal;

        y = inputVertical;

        dir = Camera.position - TrackedItem.position;
        dir = dir.normalized;
        dir = Vector3.Cross(dir, Vector3.up);
        TrackedItem.Rotate(dir, -Time.deltaTime * y*moveSpeed, Space.World);
        dir2 = Vector3.up;
        TrackedItem.Rotate(dir2, Time.deltaTime * x*moveSpeed, Space.World);


}

private void OnCollisionEnter(Collision collision)
{
    if (collision.collider.gameObject.CompareTag("collectables")|| collision.collider.gameObject.name=="collectable") 
    {
            am.playsound(1);
        gm.Collect(collision.collider.gameObject);
    }
}
}
