﻿using UnityEngine;
using System.Collections;



    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject go;
                    T createdInEditor = FindObjectOfType<T>();

                    if (createdInEditor == null)
                    {
                        go = new GameObject();
                        instance = go.AddComponent<T>();
                    }
                    else
                    {
                        go = createdInEditor.gameObject;
                        instance = createdInEditor;
                    }

                    go.name = string.Format("Singleton -- {0}", typeof(T).ToString());
                    DontDestroyOnLoad(go);
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        protected virtual void Awake()
        {
            if (Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
        }
    }
