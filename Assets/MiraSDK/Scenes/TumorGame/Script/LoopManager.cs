﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopManager : MonoBehaviour {
    public float speed = 10f;
    public GameObject Sphere;
    public GameObject StartPoint;
    public float SpawnTime=2f;
    private float Cooldown=2f;
    public bool StartSpawn = false;
	// Use this for initialization
	void Start () {
        Cooldown = SpawnTime;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Rotate(-Vector3.forward, speed * Time.deltaTime);

        if(StartSpawn)
        {
            Cooldown -= Time.deltaTime;
            if(Cooldown<0)
            {
                Cooldown= SpawnTime;
                SpawnSphere();
            }

        }

	}
    void SpawnSphere(){

      GameObject Cell= GameObject.Instantiate(Sphere,StartPoint.transform.position,Quaternion.identity);
        Cell.transform.parent = gameObject.transform;
    }

}
