﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public Camera cam;
    public GameObject target;

    Vector3 dir;
    Vector3 dir2;
	
	// Update is called once per frame
	void Update () 
    {
        cam.transform.LookAt(target.transform);

        dir = cam.transform.position - target.transform.position;
        dir = dir.normalized;
        dir = Vector3.Cross(dir, Vector3.up);
		target.transform.Rotate(dir, Time.deltaTime * 10f, Space.World);
        dir2 = Vector3.up;
        target.transform.Rotate(dir2, Time.deltaTime * 10f, Space.World);
	}

    private void OnGUI()
    {
        GUILayout.Box(dir.ToString());
    }
}