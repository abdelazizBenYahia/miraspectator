﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class GameManager : Singleton<GameManager>
{

    //In Scene objects
    public List<GameObject> Collectables;
    public int goal;
    public TextMeshPro numberOfCellstokill;
    public GameObject player;
    public List<string> framesText;
    private int framesIterator = 0;
    public GameObject fireworks;
    public TextMeshPro TutorialFrames;
    public TextMeshPro GameFrames;
    public GameObject TrackedItem;
    public GameObject FakeSphere;
    public Transform Camera;
    public GameObject BloodSplatter;
    public GameObject RealSphere;
    private AudioManager am;

    //States of game
    public bool SlidesON = false;
    public bool MovementTutorialStarted = true;
    public bool MovementTutorialFinished = true;
    public bool GameON = true;
    public bool ResetPlayer = false;

    public GameObject StartButton;
    public GameObject ResetBallButton;
    public GameObject ArrowsButton;
    // Use this for initialization
    void Awake()
    {

        MovementTutorialStarted = false;
        am = AudioManager.Instance;

        goal = Collectables.Count;
        TrackedItem.SetActive(false);

    }
    public void ResetBall()
    {

        am.playsound(0);
        player.transform.position = Camera.position;
        ResetPlayer = false;
    }
    public void StartTutorial()
    {
        if (MovementTutorialFinished)
        {
            MovementTutorialFinished = false;
            am.playsound(0);
            fireworks.SetActive(true);
            MovementTutorialStarted = false;
            Reinvoke();
        }

    }
    // Update is called once per frame
    void Update()
    {

    }
    // this handels the collection function
    public void Collect(GameObject collectable)
    {
        Collectables.Remove(collectable);
        Destroy(collectable);
        numberOfCellstokill.text = Collectables.Count.ToString();
        if (Collectables.Count <= 0)
        {
            GameOver();
            GameON = false;
        }
    }
    // this is called when all the collectables are collected
    public void GameOver()
    {
        ArrowsButton.SetActive(false);
        ResetBallButton.SetActive(false);
        GameFrames.text = "Good. The first stage of immunotherapy is completed. This tumor has been destroyed by the activated killer T cell. ";
        GameFrames.rectTransform.localPosition = new Vector3(0, 0.9f, 0);
        BloodSplatter.SetActive(true);
        am.playsound(2);
        StartCoroutine(ScaleOverTime(7));

    }
    //This is called by the MiraTracker game object in the scene

    public void OnMarkerDetected()
    {
        if (SlidesON == true)
        {
            ArrowsButton.SetActive(true);
            ResetBallButton.SetActive(true);
            GameFrames.gameObject.SetActive(true);
            TutorialFrames.gameObject.SetActive(false);
            FakeSphere.SetActive(false);
            TrackedItem.SetActive(true);



        }
    }
    // This wrapper functions was made to start the slides, and appearently invoke repeating can't be called directly in update
    public void Reinvoke()
    {

        InvokeRepeating("swapFrames", 0.1f, 7f);

    }

    public void swapFrames()
    {

        TutorialFrames.text = framesText[framesIterator];
        framesIterator++;

        if (framesIterator > framesText.Count - 1)
        {
            SlidesON = true;
            FakeSphere.SetActive(false);
            Cancel();

        }

    }
    void Cancel()
    {
        CancelInvoke(); 
    }
    // this handles the size diminution of the sphere towards the end.
IEnumerator ScaleOverTime(float time)
{
Vector3 originalScale = RealSphere.transform.localScale;
Vector3 destinationScale = new Vector3(0, 0, 0);

float currentTime = 0.0f;

do
{
    RealSphere.transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
    currentTime += Time.deltaTime;
    yield return null;
} while (currentTime <= time);

    RealSphere.SetActive(false);
    player.SetActive(false);
BloodSplatter.SetActive(false);

        SceneManager.LoadSceneAsync(0);
    yield return null;
}
}

