﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_EmptyCallback_m393913881(char* ___identifier0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_BoolCallback_m3225305019(char* ___identifier0, int32_t ___value1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_IntCallback_m160860817(char* ___identifier0, int32_t ___value1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_FloatCallback_m320080893(char* ___identifier0, float ___value1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_ErrorCallback_m1355638687(char* ___identifier0, int32_t ___success1, int32_t ___errorCode2, char* ___message3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_DiscoveredRemoteCallback_m1094739607(char* ___identifier0, char* ___name1, char* ___remoteIdentifier2, int32_t ___rssi3, int32_t ___isPreferred4);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_ConnectedRemoteCallback_m1696284216(char* ___identifier0, char* ___name1, char* ___remoteIdentifier2, char* ___productName3, char* ___serialNumber4, char* ___hardwareIdentifier5, char* ___firmwareVersion6, float ___batteryPercentage7, int32_t ___rssi8, int32_t ___isConnected9, int32_t ___isPreferred10);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_RemoteMotionCallback_m1575931452(char* ___identifier0, float ___x1, float ___y2, float ___z3);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[8] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_EmptyCallback_m393913881),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_BoolCallback_m3225305019),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_IntCallback_m160860817),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_FloatCallback_m320080893),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_ErrorCallback_m1355638687),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_DiscoveredRemoteCallback_m1094739607),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_ConnectedRemoteCallback_m1696284216),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_RemoteMotionCallback_m1575931452),
};
