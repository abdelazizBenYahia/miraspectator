﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// FadeInScene
struct FadeInScene_t3296889266;
// TMPro.Examples.TeleType
struct TeleType_t2409835159;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t3766250034;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// Mira.Transition2D
struct Transition2D_t2580652709;
// MiraExampleInteractionScript
struct MiraExampleInteractionScript_t2843749276;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.String
struct String_t;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t3617420994;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3460249701;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_t546443028;
// TMPro.Examples.VertexShakeB
struct VertexShakeB_t1533164784;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// TMPro.Examples.VertexShakeA
struct VertexShakeA_t4262048139;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t3003193665;
// TMPro.Examples.VertexJitter/VertexAnim[]
struct VertexAnimU5BU5D_t1611656175;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// TMPro.Examples.VertexJitter
struct VertexJitter_t4087429332;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t3821118074;
// EnvMapAnimator
struct EnvMapAnimator_t1140999784;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// TMPro.Examples.VertexZoom
struct VertexZoom_t550798657;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Camera
struct Camera_t4157153871;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.TextContainer
struct TextContainer_t97923372;
// UnityEngine.Transform
struct Transform_t3600365921;
// MarkerRotationWatcher
struct MarkerRotationWatcher_t711531681;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Font
struct Font_t1956802104;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// AxisSpin
struct AxisSpin_t1710076314;
// MarkerRotationWatcher/RotateAction
struct RotateAction_t1819018245;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// MiraReticle
struct MiraReticle_t910965139;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// Mira.Fishingline
struct Fishingline_t4150313569;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t1869054637;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t3109943174;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t1841909953;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t2868010532;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t1590929858;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.Renderer
struct Renderer_t2627027031;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSTARTLOGOUTU3EC__ITERATOR0_T3361594076_H
#define U3CSTARTLOGOUTU3EC__ITERATOR0_T3361594076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeInScene/<StartLogOut>c__Iterator0
struct  U3CStartLogOutU3Ec__Iterator0_t3361594076  : public RuntimeObject
{
public:
	// System.Int32 FadeInScene/<StartLogOut>c__Iterator0::sceneToLoad
	int32_t ___sceneToLoad_0;
	// FadeInScene FadeInScene/<StartLogOut>c__Iterator0::$this
	FadeInScene_t3296889266 * ___U24this_1;
	// System.Object FadeInScene/<StartLogOut>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean FadeInScene/<StartLogOut>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 FadeInScene/<StartLogOut>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_sceneToLoad_0() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t3361594076, ___sceneToLoad_0)); }
	inline int32_t get_sceneToLoad_0() const { return ___sceneToLoad_0; }
	inline int32_t* get_address_of_sceneToLoad_0() { return &___sceneToLoad_0; }
	inline void set_sceneToLoad_0(int32_t value)
	{
		___sceneToLoad_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t3361594076, ___U24this_1)); }
	inline FadeInScene_t3296889266 * get_U24this_1() const { return ___U24this_1; }
	inline FadeInScene_t3296889266 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FadeInScene_t3296889266 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t3361594076, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t3361594076, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t3361594076, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTLOGOUTU3EC__ITERATOR0_T3361594076_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3341539328_H
#define U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3341539328  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2409835159 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24this_3)); }
	inline TeleType_t2409835159 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2409835159 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2409835159 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t1343183262  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef PLATFORMBASE_T2513924928_H
#define PLATFORMBASE_T2513924928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PlatformBase
struct  PlatformBase_t2513924928  : public RuntimeObject
{
public:

public:
};

struct PlatformBase_t2513924928_StaticFields
{
public:
	// Wikitude.PlatformBase Wikitude.PlatformBase::_instance
	PlatformBase_t2513924928 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PlatformBase_t2513924928_StaticFields, ____instance_0)); }
	inline PlatformBase_t2513924928 * get__instance_0() const { return ____instance_0; }
	inline PlatformBase_t2513924928 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PlatformBase_t2513924928 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMBASE_T2513924928_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t860191687  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t3766250034 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24this_4)); }
	inline TextConsoleSimulator_t3766250034 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t3766250034 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t3766250034 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef U3CTRANSITIONTOLANDSCAPEU3EC__ITERATOR1_T3322580752_H
#define U3CTRANSITIONTOLANDSCAPEU3EC__ITERATOR1_T3322580752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D/<TransitionToLandscape>c__Iterator1
struct  U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752  : public RuntimeObject
{
public:
	// Mira.Transition2D Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$this
	Transition2D_t2580652709 * ___U24this_0;
	// System.Object Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752, ___U24this_0)); }
	inline Transition2D_t2580652709 * get_U24this_0() const { return ___U24this_0; }
	inline Transition2D_t2580652709 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Transition2D_t2580652709 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONTOLANDSCAPEU3EC__ITERATOR1_T3322580752_H
#ifndef U3CTRANSITIONTOPORTRAITU3EC__ITERATOR2_T4216413023_H
#define U3CTRANSITIONTOPORTRAITU3EC__ITERATOR2_T4216413023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D/<TransitionToPortrait>c__Iterator2
struct  U3CTransitionToPortraitU3Ec__Iterator2_t4216413023  : public RuntimeObject
{
public:
	// Mira.Transition2D Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$this
	Transition2D_t2580652709 * ___U24this_0;
	// System.Object Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t4216413023, ___U24this_0)); }
	inline Transition2D_t2580652709 * get_U24this_0() const { return ___U24this_0; }
	inline Transition2D_t2580652709 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Transition2D_t2580652709 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t4216413023, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t4216413023, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t4216413023, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONTOPORTRAITU3EC__ITERATOR2_T4216413023_H
#ifndef U3CTEMPERTANTRUMU3EC__ITERATOR0_T1951222535_H
#define U3CTEMPERTANTRUMU3EC__ITERATOR0_T1951222535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraExampleInteractionScript/<temperTantrum>c__Iterator0
struct  U3CtemperTantrumU3Ec__Iterator0_t1951222535  : public RuntimeObject
{
public:
	// System.Int32 MiraExampleInteractionScript/<temperTantrum>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Single MiraExampleInteractionScript/<temperTantrum>c__Iterator0::<distance>__2
	float ___U3CdistanceU3E__2_1;
	// System.Int32 MiraExampleInteractionScript/<temperTantrum>c__Iterator0::<i>__3
	int32_t ___U3CiU3E__3_2;
	// MiraExampleInteractionScript MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$this
	MiraExampleInteractionScript_t2843749276 * ___U24this_3;
	// System.Object MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E__2_1() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U3CdistanceU3E__2_1)); }
	inline float get_U3CdistanceU3E__2_1() const { return ___U3CdistanceU3E__2_1; }
	inline float* get_address_of_U3CdistanceU3E__2_1() { return &___U3CdistanceU3E__2_1; }
	inline void set_U3CdistanceU3E__2_1(float value)
	{
		___U3CdistanceU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__3_2() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U3CiU3E__3_2)); }
	inline int32_t get_U3CiU3E__3_2() const { return ___U3CiU3E__3_2; }
	inline int32_t* get_address_of_U3CiU3E__3_2() { return &___U3CiU3E__3_2; }
	inline void set_U3CiU3E__3_2(int32_t value)
	{
		___U3CiU3E__3_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U24this_3)); }
	inline MiraExampleInteractionScript_t2843749276 * get_U24this_3() const { return ___U24this_3; }
	inline MiraExampleInteractionScript_t2843749276 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MiraExampleInteractionScript_t2843749276 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t1951222535, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTEMPERTANTRUMU3EC__ITERATOR0_T1951222535_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2404751466_H
#define U3CSTARTU3EC__ITERATOR0_T2404751466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2404751466  : public RuntimeObject
{
public:
	// Mira.Transition2D Mira.Transition2D/<Start>c__Iterator0::$this
	Transition2D_t2580652709 * ___U24this_0;
	// System.Object Mira.Transition2D/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.Transition2D/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.Transition2D/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2404751466, ___U24this_0)); }
	inline Transition2D_t2580652709 * get_U24this_0() const { return ___U24this_0; }
	inline Transition2D_t2580652709 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Transition2D_t2580652709 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2404751466, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2404751466, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2404751466, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2404751466_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t4041402054  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t3617420994 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24this_1)); }
	inline ShaderPropAnimator_t3617420994 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t3617420994 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t3617420994 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#define U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct  U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::modifiedCharScale
	List_1_t2869341516 * ___modifiedCharScale_0;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::<>f__ref$0
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514, ___modifiedCharScale_0)); }
	inline List_1_t2869341516 * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_t2869341516 ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_t2869341516 * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((&___modifiedCharScale_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514, ___U3CU3Ef__refU240_1)); }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#ifndef PLATFORM_T1043456379_H
#define PLATFORM_T1043456379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Platform
struct  Platform_t1043456379  : public PlatformBase_t2513924928
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T1043456379_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_3_T1597070127_H
#define UNITYEVENT_3_T1597070127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t1597070127  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1597070127, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1597070127_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_3_T2493613095_H
#define UNITYEVENT_3_T2493613095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_t2493613095  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2493613095, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2493613095_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef UNITYEVENT_2_T1169440328_H
#define UNITYEVENT_2_T1169440328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_t1169440328  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1169440328, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1169440328_H
#ifndef VERTEXANIM_T2231884842_H
#define VERTEXANIM_T2231884842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/VertexAnim
struct  VertexAnim_t2231884842 
{
public:
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angleRange
	float ___angleRange_0;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angle
	float ___angle_1;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_angleRange_0() { return static_cast<int32_t>(offsetof(VertexAnim_t2231884842, ___angleRange_0)); }
	inline float get_angleRange_0() const { return ___angleRange_0; }
	inline float* get_address_of_angleRange_0() { return &___angleRange_0; }
	inline void set_angleRange_0(float value)
	{
		___angleRange_0 = value;
	}

	inline static int32_t get_offset_of_angle_1() { return static_cast<int32_t>(offsetof(VertexAnim_t2231884842, ___angle_1)); }
	inline float get_angle_1() const { return ___angle_1; }
	inline float* get_address_of_angle_1() { return &___angle_1; }
	inline void set_angle_1(float value)
	{
		___angle_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(VertexAnim_t2231884842, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXANIM_T2231884842_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#define U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t116130919  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3460249701 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24this_9)); }
	inline SkewTextExample_t3460249701 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3460249701 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3460249701 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T168300594_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T168300594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t168300594  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_t546443028* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeB TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeB_t1533164784 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_t546443028* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_t546443028** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_t546443028* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24this_5)); }
	inline VertexShakeB_t1533164784 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeB_t1533164784 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeB_t1533164784 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T168300594_H
#ifndef U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#define U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0
struct  U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$this
	TextMeshProFloatingText_t845872552 * ___U24this_7;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CfadeDurationU3E__0_6)); }
	inline float get_U3CfadeDurationU3E__0_6() const { return ___U3CfadeDurationU3E__0_6; }
	inline float* get_address_of_U3CfadeDurationU3E__0_6() { return &___U3CfadeDurationU3E__0_6; }
	inline void set_U3CfadeDurationU3E__0_6(float value)
	{
		___U3CfadeDurationU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24this_7)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_7() const { return ___U24this_7; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T956521787_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T956521787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t956521787  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_t546443028* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeA TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeA_t4262048139 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_t546443028* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_t546443028** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_t546443028* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24this_5)); }
	inline VertexShakeA_t4262048139 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeA_t4262048139 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeA_t4262048139 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T956521787_H
#ifndef CHARACTERSELECTIONEVENT_T3109943174_H
#define CHARACTERSELECTIONEVENT_T3109943174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct  CharacterSelectionEvent_t3109943174  : public UnityEvent_2_t1169440328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T3109943174_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T897284962_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T897284962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t897284962  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<currentCharacter>__0
	int32_t ___U3CcurrentCharacterU3E__0_1;
	// UnityEngine.Color32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<c0>__0
	Color32_t2600501292  ___U3Cc0U3E__0_2;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<materialIndex>__1
	int32_t ___U3CmaterialIndexU3E__1_4;
	// UnityEngine.Color32[] TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<newVertexColors>__1
	Color32U5BU5D_t3850468773* ___U3CnewVertexColorsU3E__1_5;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<vertexIndex>__1
	int32_t ___U3CvertexIndexU3E__1_6;
	// TMPro.Examples.VertexColorCycler TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$this
	VertexColorCycler_t3003193665 * ___U24this_7;
	// System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentCharacterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CcurrentCharacterU3E__0_1)); }
	inline int32_t get_U3CcurrentCharacterU3E__0_1() const { return ___U3CcurrentCharacterU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentCharacterU3E__0_1() { return &___U3CcurrentCharacterU3E__0_1; }
	inline void set_U3CcurrentCharacterU3E__0_1(int32_t value)
	{
		___U3CcurrentCharacterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cc0U3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3Cc0U3E__0_2)); }
	inline Color32_t2600501292  get_U3Cc0U3E__0_2() const { return ___U3Cc0U3E__0_2; }
	inline Color32_t2600501292 * get_address_of_U3Cc0U3E__0_2() { return &___U3Cc0U3E__0_2; }
	inline void set_U3Cc0U3E__0_2(Color32_t2600501292  value)
	{
		___U3Cc0U3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CmaterialIndexU3E__1_4)); }
	inline int32_t get_U3CmaterialIndexU3E__1_4() const { return ___U3CmaterialIndexU3E__1_4; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E__1_4() { return &___U3CmaterialIndexU3E__1_4; }
	inline void set_U3CmaterialIndexU3E__1_4(int32_t value)
	{
		___U3CmaterialIndexU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewVertexColorsU3E__1_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CnewVertexColorsU3E__1_5)); }
	inline Color32U5BU5D_t3850468773* get_U3CnewVertexColorsU3E__1_5() const { return ___U3CnewVertexColorsU3E__1_5; }
	inline Color32U5BU5D_t3850468773** get_address_of_U3CnewVertexColorsU3E__1_5() { return &___U3CnewVertexColorsU3E__1_5; }
	inline void set_U3CnewVertexColorsU3E__1_5(Color32U5BU5D_t3850468773* value)
	{
		___U3CnewVertexColorsU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewVertexColorsU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E__1_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CvertexIndexU3E__1_6)); }
	inline int32_t get_U3CvertexIndexU3E__1_6() const { return ___U3CvertexIndexU3E__1_6; }
	inline int32_t* get_address_of_U3CvertexIndexU3E__1_6() { return &___U3CvertexIndexU3E__1_6; }
	inline void set_U3CvertexIndexU3E__1_6(int32_t value)
	{
		___U3CvertexIndexU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24this_7)); }
	inline VertexColorCycler_t3003193665 * get_U24this_7() const { return ___U24this_7; }
	inline VertexColorCycler_t3003193665 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(VertexColorCycler_t3003193665 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T897284962_H
#ifndef WORDSELECTIONEVENT_T1841909953_H
#define WORDSELECTIONEVENT_T1841909953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_t1841909953  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_T1841909953_H
#ifndef LINKSELECTIONEVENT_T1590929858_H
#define LINKSELECTIONEVENT_T1590929858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct  LinkSelectionEvent_t1590929858  : public UnityEvent_3_t2493613095
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T1590929858_H
#ifndef LINESELECTIONEVENT_T2868010532_H
#define LINESELECTIONEVENT_T2868010532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct  LineSelectionEvent_t2868010532  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T2868010532_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T225534713_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T225534713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t225534713  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<loopCount>__0
	int32_t ___U3CloopCountU3E__0_1;
	// TMPro.Examples.VertexJitter/VertexAnim[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<vertexAnim>__0
	VertexAnimU5BU5D_t1611656175* ___U3CvertexAnimU3E__0_2;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<cachedMeshInfo>__0
	TMP_MeshInfoU5BU5D_t3365986247* ___U3CcachedMeshInfoU3E__0_3;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_5;
	// TMPro.Examples.VertexJitter TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$this
	VertexJitter_t4087429332 * ___U24this_6;
	// System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CloopCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CloopCountU3E__0_1)); }
	inline int32_t get_U3CloopCountU3E__0_1() const { return ___U3CloopCountU3E__0_1; }
	inline int32_t* get_address_of_U3CloopCountU3E__0_1() { return &___U3CloopCountU3E__0_1; }
	inline void set_U3CloopCountU3E__0_1(int32_t value)
	{
		___U3CloopCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvertexAnimU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CvertexAnimU3E__0_2)); }
	inline VertexAnimU5BU5D_t1611656175* get_U3CvertexAnimU3E__0_2() const { return ___U3CvertexAnimU3E__0_2; }
	inline VertexAnimU5BU5D_t1611656175** get_address_of_U3CvertexAnimU3E__0_2() { return &___U3CvertexAnimU3E__0_2; }
	inline void set_U3CvertexAnimU3E__0_2(VertexAnimU5BU5D_t1611656175* value)
	{
		___U3CvertexAnimU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvertexAnimU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CcachedMeshInfoU3E__0_3)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_U3CcachedMeshInfoU3E__0_3() const { return ___U3CcachedMeshInfoU3E__0_3; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_U3CcachedMeshInfoU3E__0_3() { return &___U3CcachedMeshInfoU3E__0_3; }
	inline void set_U3CcachedMeshInfoU3E__0_3(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___U3CcachedMeshInfoU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CmatrixU3E__2_5)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_5() const { return ___U3CmatrixU3E__2_5; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_5() { return &___U3CmatrixU3E__2_5; }
	inline void set_U3CmatrixU3E__2_5(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24this_6)); }
	inline VertexJitter_t4087429332 * get_U24this_6() const { return ___U24this_6; }
	inline VertexJitter_t4087429332 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(VertexJitter_t4087429332 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T225534713_H
#ifndef U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#define U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1
struct  U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifndef OBJECTTYPE_T4082700821_H
#define OBJECTTYPE_T4082700821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t4082700821 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t4082700821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T4082700821_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#define FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t1585798158 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t1585798158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2334657565_H
#define FPSCOUNTERANCHORPOSITIONS_T2334657565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2334657565 
{
public:
	// System.Int32 TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2334657565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2334657565_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#define U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t4025661343  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_1;
	// TMPro.TMP_TextInfo TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_2;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_4;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_5;
	// UnityEngine.Vector3[] TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_6;
	// UnityEngine.Matrix4x4 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_7;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$this
	WarpTextExample_t3821118074 * ___U24this_8;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3Cold_curveU3E__0_1)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_1() const { return ___U3Cold_curveU3E__0_1; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_1() { return &___U3Cold_curveU3E__0_1; }
	inline void set_U3Cold_curveU3E__0_1(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CtextInfoU3E__1_2)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_2() const { return ___U3CtextInfoU3E__1_2; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_2() { return &___U3CtextInfoU3E__1_2; }
	inline void set_U3CtextInfoU3E__1_2(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CboundsMinXU3E__1_4)); }
	inline float get_U3CboundsMinXU3E__1_4() const { return ___U3CboundsMinXU3E__1_4; }
	inline float* get_address_of_U3CboundsMinXU3E__1_4() { return &___U3CboundsMinXU3E__1_4; }
	inline void set_U3CboundsMinXU3E__1_4(float value)
	{
		___U3CboundsMinXU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CboundsMaxXU3E__1_5)); }
	inline float get_U3CboundsMaxXU3E__1_5() const { return ___U3CboundsMaxXU3E__1_5; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_5() { return &___U3CboundsMaxXU3E__1_5; }
	inline void set_U3CboundsMaxXU3E__1_5(float value)
	{
		___U3CboundsMaxXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CverticesU3E__2_6)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_6() const { return ___U3CverticesU3E__2_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_6() { return &___U3CverticesU3E__2_6; }
	inline void set_U3CverticesU3E__2_6(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CmatrixU3E__2_7)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_7() const { return ___U3CmatrixU3E__2_7; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_7() { return &___U3CmatrixU3E__2_7; }
	inline void set_U3CmatrixU3E__2_7(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24this_8)); }
	inline WarpTextExample_t3821118074 * get_U24this_8() const { return ___U24this_8; }
	inline WarpTextExample_t3821118074 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(WarpTextExample_t3821118074 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1520811813_H
#define U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1520811813  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t1817901843  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1140999784 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24this_1)); }
	inline EnvMapAnimator_t1140999784 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1140999784 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1140999784 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#define FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2550331785 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2550331785, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifndef RIGIDBODYCONSTRAINTS_T3348042902_H
#define RIGIDBODYCONSTRAINTS_T3348042902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_t3348042902 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_t3348042902, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS_T3348042902_H
#ifndef CAMERAMODES_T3200559075_H
#define CAMERAMODES_T3200559075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t3200559075 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t3200559075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T3200559075_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3792186008_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3792186008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<cachedMeshInfoVertexData>__0
	TMP_MeshInfoU5BU5D_t3365986247* ___U3CcachedMeshInfoVertexDataU3E__0_1;
	// System.Collections.Generic.List`1<System.Int32> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<scaleSortingOrder>__0
	List_1_t128053199 * ___U3CscaleSortingOrderU3E__0_2;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexZoom TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$this
	VertexZoom_t550798657 * ___U24this_5;
	// System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$locvar0
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CcachedMeshInfoVertexDataU3E__0_1)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_U3CcachedMeshInfoVertexDataU3E__0_1() const { return ___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return &___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline void set_U3CcachedMeshInfoVertexDataU3E__0_1(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___U3CcachedMeshInfoVertexDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoVertexDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CscaleSortingOrderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CscaleSortingOrderU3E__0_2)); }
	inline List_1_t128053199 * get_U3CscaleSortingOrderU3E__0_2() const { return ___U3CscaleSortingOrderU3E__0_2; }
	inline List_1_t128053199 ** get_address_of_U3CscaleSortingOrderU3E__0_2() { return &___U3CscaleSortingOrderU3E__0_2; }
	inline void set_U3CscaleSortingOrderU3E__0_2(List_1_t128053199 * value)
	{
		___U3CscaleSortingOrderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscaleSortingOrderU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24this_5)); }
	inline VertexZoom_t550798657 * get_U24this_5() const { return ___U24this_5; }
	inline VertexZoom_t550798657 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexZoom_t550798657 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24locvar0_9)); }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3792186008_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MOTIONTYPE_T1905163921_H
#define MOTIONTYPE_T1905163921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t1905163921 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t1905163921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T1905163921_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef ROTATEACTION_T1819018245_H
#define ROTATEACTION_T1819018245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerRotationWatcher/RotateAction
struct  RotateAction_t1819018245  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEACTION_T1819018245_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TMP_PHONENUMBERVALIDATOR_T743649728_H
#define TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t743649728  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifndef TMP_DIGITVALIDATOR_T573672104_H
#define TMP_DIGITVALIDATOR_T573672104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_t573672104  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_T573672104_H
#ifndef VERTEXZOOM_T550798657_H
#define VERTEXZOOM_T550798657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom
struct  VertexZoom_t550798657  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexZoom::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexZoom::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexZoom::CurveScale
	float ___CurveScale_4;
	// TMPro.TMP_Text TMPro.Examples.VertexZoom::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_5;
	// System.Boolean TMPro.Examples.VertexZoom::hasTextChanged
	bool ___hasTextChanged_6;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___m_TextComponent_5)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_6() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___hasTextChanged_6)); }
	inline bool get_hasTextChanged_6() const { return ___hasTextChanged_6; }
	inline bool* get_address_of_hasTextChanged_6() { return &___hasTextChanged_6; }
	inline void set_hasTextChanged_6(bool value)
	{
		___hasTextChanged_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXZOOM_T550798657_H
#ifndef TRANSITION2D_T2580652709_H
#define TRANSITION2D_T2580652709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D
struct  Transition2D_t2580652709  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Mira.Transition2D::displayOnce
	bool ___displayOnce_2;
	// UnityEngine.GameObject Mira.Transition2D::MiraCameraRig
	GameObject_t1113636619 * ___MiraCameraRig_3;
	// UnityEngine.Canvas Mira.Transition2D::instructionScreen
	Canvas_t3310196443 * ___instructionScreen_4;
	// UnityEngine.Camera Mira.Transition2D::twoDCamera
	Camera_t4157153871 * ___twoDCamera_5;
	// UnityEngine.Camera Mira.Transition2D::distortionL
	Camera_t4157153871 * ___distortionL_6;
	// UnityEngine.Camera Mira.Transition2D::distortionR
	Camera_t4157153871 * ___distortionR_7;

public:
	inline static int32_t get_offset_of_displayOnce_2() { return static_cast<int32_t>(offsetof(Transition2D_t2580652709, ___displayOnce_2)); }
	inline bool get_displayOnce_2() const { return ___displayOnce_2; }
	inline bool* get_address_of_displayOnce_2() { return &___displayOnce_2; }
	inline void set_displayOnce_2(bool value)
	{
		___displayOnce_2 = value;
	}

	inline static int32_t get_offset_of_MiraCameraRig_3() { return static_cast<int32_t>(offsetof(Transition2D_t2580652709, ___MiraCameraRig_3)); }
	inline GameObject_t1113636619 * get_MiraCameraRig_3() const { return ___MiraCameraRig_3; }
	inline GameObject_t1113636619 ** get_address_of_MiraCameraRig_3() { return &___MiraCameraRig_3; }
	inline void set_MiraCameraRig_3(GameObject_t1113636619 * value)
	{
		___MiraCameraRig_3 = value;
		Il2CppCodeGenWriteBarrier((&___MiraCameraRig_3), value);
	}

	inline static int32_t get_offset_of_instructionScreen_4() { return static_cast<int32_t>(offsetof(Transition2D_t2580652709, ___instructionScreen_4)); }
	inline Canvas_t3310196443 * get_instructionScreen_4() const { return ___instructionScreen_4; }
	inline Canvas_t3310196443 ** get_address_of_instructionScreen_4() { return &___instructionScreen_4; }
	inline void set_instructionScreen_4(Canvas_t3310196443 * value)
	{
		___instructionScreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___instructionScreen_4), value);
	}

	inline static int32_t get_offset_of_twoDCamera_5() { return static_cast<int32_t>(offsetof(Transition2D_t2580652709, ___twoDCamera_5)); }
	inline Camera_t4157153871 * get_twoDCamera_5() const { return ___twoDCamera_5; }
	inline Camera_t4157153871 ** get_address_of_twoDCamera_5() { return &___twoDCamera_5; }
	inline void set_twoDCamera_5(Camera_t4157153871 * value)
	{
		___twoDCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___twoDCamera_5), value);
	}

	inline static int32_t get_offset_of_distortionL_6() { return static_cast<int32_t>(offsetof(Transition2D_t2580652709, ___distortionL_6)); }
	inline Camera_t4157153871 * get_distortionL_6() const { return ___distortionL_6; }
	inline Camera_t4157153871 ** get_address_of_distortionL_6() { return &___distortionL_6; }
	inline void set_distortionL_6(Camera_t4157153871 * value)
	{
		___distortionL_6 = value;
		Il2CppCodeGenWriteBarrier((&___distortionL_6), value);
	}

	inline static int32_t get_offset_of_distortionR_7() { return static_cast<int32_t>(offsetof(Transition2D_t2580652709, ___distortionR_7)); }
	inline Camera_t4157153871 * get_distortionR_7() const { return ___distortionR_7; }
	inline Camera_t4157153871 ** get_address_of_distortionR_7() { return &___distortionR_7; }
	inline void set_distortionR_7(Camera_t4157153871 * value)
	{
		___distortionR_7 = value;
		Il2CppCodeGenWriteBarrier((&___distortionR_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION2D_T2580652709_H
#ifndef VERTEXCOLORCYCLER_T3003193665_H
#define VERTEXCOLORCYCLER_T3003193665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler
struct  VertexColorCycler_t3003193665  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.VertexColorCycler::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(VertexColorCycler_t3003193665, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXCOLORCYCLER_T3003193665_H
#ifndef TMPRO_INSTRUCTIONOVERLAY_T4246705477_H
#define TMPRO_INSTRUCTIONOVERLAY_T4246705477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay
struct  TMPro_InstructionOverlay_t4246705477  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions TMPro.Examples.TMPro_InstructionOverlay::AnchorPosition
	int32_t ___AnchorPosition_2;
	// TMPro.TextMeshPro TMPro.Examples.TMPro_InstructionOverlay::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_4;
	// TMPro.TextContainer TMPro.Examples.TMPro_InstructionOverlay::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_5;
	// UnityEngine.Transform TMPro.Examples.TMPro_InstructionOverlay::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_6;
	// UnityEngine.Camera TMPro.Examples.TMPro_InstructionOverlay::m_camera
	Camera_t4157153871 * ___m_camera_7;

public:
	inline static int32_t get_offset_of_AnchorPosition_2() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___AnchorPosition_2)); }
	inline int32_t get_AnchorPosition_2() const { return ___AnchorPosition_2; }
	inline int32_t* get_address_of_AnchorPosition_2() { return &___AnchorPosition_2; }
	inline void set_AnchorPosition_2(int32_t value)
	{
		___AnchorPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_TextMeshPro_4() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_TextMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_4() const { return ___m_TextMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_4() { return &___m_TextMeshPro_4; }
	inline void set_m_TextMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_textContainer_5() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_textContainer_5)); }
	inline TextContainer_t97923372 * get_m_textContainer_5() const { return ___m_textContainer_5; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_5() { return &___m_textContainer_5; }
	inline void set_m_textContainer_5(TextContainer_t97923372 * value)
	{
		___m_textContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_5), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_6() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_frameCounter_transform_6)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_6() const { return ___m_frameCounter_transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_6() { return &___m_frameCounter_transform_6; }
	inline void set_m_frameCounter_transform_6(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_6), value);
	}

	inline static int32_t get_offset_of_m_camera_7() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_camera_7)); }
	inline Camera_t4157153871 * get_m_camera_7() const { return ___m_camera_7; }
	inline Camera_t4157153871 ** get_address_of_m_camera_7() { return &___m_camera_7; }
	inline void set_m_camera_7(Camera_t4157153871 * value)
	{
		___m_camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_INSTRUCTIONOVERLAY_T4246705477_H
#ifndef VERTEXSHAKEB_T1533164784_H
#define VERTEXSHAKEB_T1533164784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB
struct  VertexShakeB_t1533164784  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexShakeB::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexShakeB::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexShakeB::CurveScale
	float ___CurveScale_4;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeB::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_5;
	// System.Boolean TMPro.Examples.VertexShakeB::hasTextChanged
	bool ___hasTextChanged_6;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___m_TextComponent_5)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_6() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___hasTextChanged_6)); }
	inline bool get_hasTextChanged_6() const { return ___hasTextChanged_6; }
	inline bool* get_address_of_hasTextChanged_6() { return &___hasTextChanged_6; }
	inline void set_hasTextChanged_6(bool value)
	{
		___hasTextChanged_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEB_T1533164784_H
#ifndef WARPTEXTEXAMPLE_T3821118074_H
#define WARPTEXTEXAMPLE_T3821118074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample
struct  WarpTextExample_t3821118074  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_3;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_6;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___VertexCurve_3)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARPTEXTEXAMPLE_T3821118074_H
#ifndef VERTEXSHAKEA_T4262048139_H
#define VERTEXSHAKEA_T4262048139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA
struct  VertexShakeA_t4262048139  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexShakeA::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexShakeA::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexShakeA::ScaleMultiplier
	float ___ScaleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeA::RotationMultiplier
	float ___RotationMultiplier_5;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeA::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_6;
	// System.Boolean TMPro.Examples.VertexShakeA::hasTextChanged
	bool ___hasTextChanged_7;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___ScaleMultiplier_4)); }
	inline float get_ScaleMultiplier_4() const { return ___ScaleMultiplier_4; }
	inline float* get_address_of_ScaleMultiplier_4() { return &___ScaleMultiplier_4; }
	inline void set_ScaleMultiplier_4(float value)
	{
		___ScaleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_RotationMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___RotationMultiplier_5)); }
	inline float get_RotationMultiplier_5() const { return ___RotationMultiplier_5; }
	inline float* get_address_of_RotationMultiplier_5() { return &___RotationMultiplier_5; }
	inline void set_RotationMultiplier_5(float value)
	{
		___RotationMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_6() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___m_TextComponent_6)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_6() const { return ___m_TextComponent_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_6() { return &___m_TextComponent_6; }
	inline void set_m_TextComponent_6(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_6), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_7() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___hasTextChanged_7)); }
	inline bool get_hasTextChanged_7() const { return ___hasTextChanged_7; }
	inline bool* get_address_of_hasTextChanged_7() { return &___hasTextChanged_7; }
	inline void set_hasTextChanged_7(bool value)
	{
		___hasTextChanged_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEA_T4262048139_H
#ifndef VERTEXJITTER_T4087429332_H
#define VERTEXJITTER_T4087429332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter
struct  VertexJitter_t4087429332  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexJitter::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexJitter::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexJitter::CurveScale
	float ___CurveScale_4;
	// TMPro.TMP_Text TMPro.Examples.VertexJitter::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_5;
	// System.Boolean TMPro.Examples.VertexJitter::hasTextChanged
	bool ___hasTextChanged_6;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___m_TextComponent_5)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_6() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___hasTextChanged_6)); }
	inline bool get_hasTextChanged_6() const { return ___hasTextChanged_6; }
	inline bool* get_address_of_hasTextChanged_6() { return &___hasTextChanged_6; }
	inline void set_hasTextChanged_6(bool value)
	{
		___hasTextChanged_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXJITTER_T4087429332_H
#ifndef TRANSFORMOVERRIDE_T3828400655_H
#define TRANSFORMOVERRIDE_T3828400655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformOverride
struct  TransformOverride_t3828400655  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMOVERRIDE_T3828400655_H
#ifndef TRACKERLERPDRIVER_T2407623450_H
#define TRACKERLERPDRIVER_T2407623450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackerLerpDriver
struct  TrackerLerpDriver_t2407623450  : public MonoBehaviour_t3962482529
{
public:
	// MarkerRotationWatcher TrackerLerpDriver::rotationWatcher
	MarkerRotationWatcher_t711531681 * ___rotationWatcher_2;
	// UnityEngine.Vector3 TrackerLerpDriver::startPosition
	Vector3_t3722313464  ___startPosition_3;
	// UnityEngine.Vector3 TrackerLerpDriver::endPosition
	Vector3_t3722313464  ___endPosition_4;

public:
	inline static int32_t get_offset_of_rotationWatcher_2() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t2407623450, ___rotationWatcher_2)); }
	inline MarkerRotationWatcher_t711531681 * get_rotationWatcher_2() const { return ___rotationWatcher_2; }
	inline MarkerRotationWatcher_t711531681 ** get_address_of_rotationWatcher_2() { return &___rotationWatcher_2; }
	inline void set_rotationWatcher_2(MarkerRotationWatcher_t711531681 * value)
	{
		___rotationWatcher_2 = value;
		Il2CppCodeGenWriteBarrier((&___rotationWatcher_2), value);
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t2407623450, ___startPosition_3)); }
	inline Vector3_t3722313464  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t3722313464 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t3722313464  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_endPosition_4() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t2407623450, ___endPosition_4)); }
	inline Vector3_t3722313464  get_endPosition_4() const { return ___endPosition_4; }
	inline Vector3_t3722313464 * get_address_of_endPosition_4() { return &___endPosition_4; }
	inline void set_endPosition_4(Vector3_t3722313464  value)
	{
		___endPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERLERPDRIVER_T2407623450_H
#ifndef EXPOSURECONTROLLER_T1901463639_H
#define EXPOSURECONTROLLER_T1901463639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExposureController
struct  ExposureController_t1901463639  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.WikitudeCamera ExposureController::WikiCamera
	WikitudeCamera_t2188881370 * ___WikiCamera_2;

public:
	inline static int32_t get_offset_of_WikiCamera_2() { return static_cast<int32_t>(offsetof(ExposureController_t1901463639, ___WikiCamera_2)); }
	inline WikitudeCamera_t2188881370 * get_WikiCamera_2() const { return ___WikiCamera_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_WikiCamera_2() { return &___WikiCamera_2; }
	inline void set_WikiCamera_2(WikitudeCamera_t2188881370 * value)
	{
		___WikiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___WikiCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSURECONTROLLER_T1901463639_H
#ifndef TOGGLEMODES_T4014462119_H
#define TOGGLEMODES_T4014462119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleModes
struct  ToggleModes_t4014462119  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEMODES_T4014462119_H
#ifndef MIRAPHYSICSGRABEXAMPLE_T3867022233_H
#define MIRAPHYSICSGRABEXAMPLE_T3867022233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsGrabExample
struct  MiraPhysicsGrabExample_t3867022233  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MiraPhysicsGrabExample::isGrabbing
	bool ___isGrabbing_2;
	// UnityEngine.RigidbodyConstraints MiraPhysicsGrabExample::originalConstraints
	int32_t ___originalConstraints_3;
	// UnityEngine.Rigidbody MiraPhysicsGrabExample::rigidBody
	Rigidbody_t3916780224 * ___rigidBody_4;
	// System.Single MiraPhysicsGrabExample::lastTouchPosition
	float ___lastTouchPosition_5;

public:
	inline static int32_t get_offset_of_isGrabbing_2() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t3867022233, ___isGrabbing_2)); }
	inline bool get_isGrabbing_2() const { return ___isGrabbing_2; }
	inline bool* get_address_of_isGrabbing_2() { return &___isGrabbing_2; }
	inline void set_isGrabbing_2(bool value)
	{
		___isGrabbing_2 = value;
	}

	inline static int32_t get_offset_of_originalConstraints_3() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t3867022233, ___originalConstraints_3)); }
	inline int32_t get_originalConstraints_3() const { return ___originalConstraints_3; }
	inline int32_t* get_address_of_originalConstraints_3() { return &___originalConstraints_3; }
	inline void set_originalConstraints_3(int32_t value)
	{
		___originalConstraints_3 = value;
	}

	inline static int32_t get_offset_of_rigidBody_4() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t3867022233, ___rigidBody_4)); }
	inline Rigidbody_t3916780224 * get_rigidBody_4() const { return ___rigidBody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_rigidBody_4() { return &___rigidBody_4; }
	inline void set_rigidBody_4(Rigidbody_t3916780224 * value)
	{
		___rigidBody_4 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_4), value);
	}

	inline static int32_t get_offset_of_lastTouchPosition_5() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t3867022233, ___lastTouchPosition_5)); }
	inline float get_lastTouchPosition_5() const { return ___lastTouchPosition_5; }
	inline float* get_address_of_lastTouchPosition_5() { return &___lastTouchPosition_5; }
	inline void set_lastTouchPosition_5(float value)
	{
		___lastTouchPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSGRABEXAMPLE_T3867022233_H
#ifndef MIRAPHYSICSPLAYPENBOUNDARY_T2335368901_H
#define MIRAPHYSICSPLAYPENBOUNDARY_T2335368901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsPlaypenBoundary
struct  MiraPhysicsPlaypenBoundary_t2335368901  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSPLAYPENBOUNDARY_T2335368901_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_4)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_4() const { return ___floatingText_Script_4; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_4() { return &___floatingText_Script_4; }
	inline void set_floatingText_Script_4(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_4 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_3)); }
	inline Canvas_t3310196443 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t3310196443 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t3310196443 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_3), value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_4)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_4), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_5)); }
	inline Font_t1956802104 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t1956802104 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_7)); }
	inline Text_t1901882714 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t1901882714 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef IMAGETRACKINGCONTROLLER_T390588479_H
#define IMAGETRACKINGCONTROLLER_T390588479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageTrackingController
struct  ImageTrackingController_t390588479  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETRACKINGCONTROLLER_T390588479_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_3)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_4)); }
	inline Font_t1956802104 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t1956802104 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_6)); }
	inline TextContainer_t97923372 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t97923372 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef FACECAMERA_T985191632_H
#define FACECAMERA_T985191632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCamera
struct  FaceCamera_t985191632  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FaceCamera::stereoCamRig
	Transform_t3600365921 * ___stereoCamRig_2;

public:
	inline static int32_t get_offset_of_stereoCamRig_2() { return static_cast<int32_t>(offsetof(FaceCamera_t985191632, ___stereoCamRig_2)); }
	inline Transform_t3600365921 * get_stereoCamRig_2() const { return ___stereoCamRig_2; }
	inline Transform_t3600365921 ** get_address_of_stereoCamRig_2() { return &___stereoCamRig_2; }
	inline void set_stereoCamRig_2(Transform_t3600365921 * value)
	{
		___stereoCamRig_2 = value;
		Il2CppCodeGenWriteBarrier((&___stereoCamRig_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECAMERA_T985191632_H
#ifndef FADEINSCENE_T3296889266_H
#define FADEINSCENE_T3296889266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeInScene
struct  FadeInScene_t3296889266  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean FadeInScene::shouldFadeOut
	bool ___shouldFadeOut_2;
	// UnityEngine.Texture2D FadeInScene::fadeTexture
	Texture2D_t3840446185 * ___fadeTexture_3;
	// System.Single FadeInScene::fadeSpeed
	float ___fadeSpeed_4;
	// System.Int32 FadeInScene::drawDepth
	int32_t ___drawDepth_5;
	// System.Single FadeInScene::alpha
	float ___alpha_6;
	// System.Int32 FadeInScene::fadeDir
	int32_t ___fadeDir_7;
	// System.Boolean FadeInScene::shouldBlackOut
	bool ___shouldBlackOut_8;

public:
	inline static int32_t get_offset_of_shouldFadeOut_2() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___shouldFadeOut_2)); }
	inline bool get_shouldFadeOut_2() const { return ___shouldFadeOut_2; }
	inline bool* get_address_of_shouldFadeOut_2() { return &___shouldFadeOut_2; }
	inline void set_shouldFadeOut_2(bool value)
	{
		___shouldFadeOut_2 = value;
	}

	inline static int32_t get_offset_of_fadeTexture_3() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___fadeTexture_3)); }
	inline Texture2D_t3840446185 * get_fadeTexture_3() const { return ___fadeTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_fadeTexture_3() { return &___fadeTexture_3; }
	inline void set_fadeTexture_3(Texture2D_t3840446185 * value)
	{
		___fadeTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___fadeTexture_3), value);
	}

	inline static int32_t get_offset_of_fadeSpeed_4() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___fadeSpeed_4)); }
	inline float get_fadeSpeed_4() const { return ___fadeSpeed_4; }
	inline float* get_address_of_fadeSpeed_4() { return &___fadeSpeed_4; }
	inline void set_fadeSpeed_4(float value)
	{
		___fadeSpeed_4 = value;
	}

	inline static int32_t get_offset_of_drawDepth_5() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___drawDepth_5)); }
	inline int32_t get_drawDepth_5() const { return ___drawDepth_5; }
	inline int32_t* get_address_of_drawDepth_5() { return &___drawDepth_5; }
	inline void set_drawDepth_5(int32_t value)
	{
		___drawDepth_5 = value;
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}

	inline static int32_t get_offset_of_fadeDir_7() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___fadeDir_7)); }
	inline int32_t get_fadeDir_7() const { return ___fadeDir_7; }
	inline int32_t* get_address_of_fadeDir_7() { return &___fadeDir_7; }
	inline void set_fadeDir_7(int32_t value)
	{
		___fadeDir_7 = value;
	}

	inline static int32_t get_offset_of_shouldBlackOut_8() { return static_cast<int32_t>(offsetof(FadeInScene_t3296889266, ___shouldBlackOut_8)); }
	inline bool get_shouldBlackOut_8() const { return ___shouldBlackOut_8; }
	inline bool* get_address_of_shouldBlackOut_8() { return &___shouldBlackOut_8; }
	inline void set_shouldBlackOut_8(bool value)
	{
		___shouldBlackOut_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEINSCENE_T3296889266_H
#ifndef DYNAMICALLYCHANGEIPD_T3573109106_H
#define DYNAMICALLYCHANGEIPD_T3573109106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicallyChangeIPD
struct  DynamicallyChangeIPD_t3573109106  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICALLYCHANGEIPD_T3573109106_H
#ifndef AXISSPIN_T1710076314_H
#define AXISSPIN_T1710076314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AxisSpin
struct  AxisSpin_t1710076314  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AxisSpin::spinRate
	float ___spinRate_2;
	// UnityEngine.Vector3 AxisSpin::spinDirection
	Vector3_t3722313464  ___spinDirection_3;

public:
	inline static int32_t get_offset_of_spinRate_2() { return static_cast<int32_t>(offsetof(AxisSpin_t1710076314, ___spinRate_2)); }
	inline float get_spinRate_2() const { return ___spinRate_2; }
	inline float* get_address_of_spinRate_2() { return &___spinRate_2; }
	inline void set_spinRate_2(float value)
	{
		___spinRate_2 = value;
	}

	inline static int32_t get_offset_of_spinDirection_3() { return static_cast<int32_t>(offsetof(AxisSpin_t1710076314, ___spinDirection_3)); }
	inline Vector3_t3722313464  get_spinDirection_3() const { return ___spinDirection_3; }
	inline Vector3_t3722313464 * get_address_of_spinDirection_3() { return &___spinDirection_3; }
	inline void set_spinDirection_3(Vector3_t3722313464  value)
	{
		___spinDirection_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSPIN_T1710076314_H
#ifndef DEMOSCENEMANAGER_T570878136_H
#define DEMOSCENEMANAGER_T570878136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoSceneManager
struct  DemoSceneManager_t570878136  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct DemoSceneManager_t570878136_StaticFields
{
public:
	// System.Boolean DemoSceneManager::isSpectator
	bool ___isSpectator_2;
	// DemoSceneManager DemoSceneManager::instance
	DemoSceneManager_t570878136 * ___instance_3;

public:
	inline static int32_t get_offset_of_isSpectator_2() { return static_cast<int32_t>(offsetof(DemoSceneManager_t570878136_StaticFields, ___isSpectator_2)); }
	inline bool get_isSpectator_2() const { return ___isSpectator_2; }
	inline bool* get_address_of_isSpectator_2() { return &___isSpectator_2; }
	inline void set_isSpectator_2(bool value)
	{
		___isSpectator_2 = value;
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(DemoSceneManager_t570878136_StaticFields, ___instance_3)); }
	inline DemoSceneManager_t570878136 * get_instance_3() const { return ___instance_3; }
	inline DemoSceneManager_t570878136 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(DemoSceneManager_t570878136 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCENEMANAGER_T570878136_H
#ifndef MIRAEXAMPLEINTERACTIONSCRIPT_T2843749276_H
#define MIRAEXAMPLEINTERACTIONSCRIPT_T2843749276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraExampleInteractionScript
struct  MiraExampleInteractionScript_t2843749276  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MiraExampleInteractionScript::textBeneathPlanet
	Text_t1901882714 * ___textBeneathPlanet_2;
	// System.Single MiraExampleInteractionScript::timeSinceLastInteraction
	float ___timeSinceLastInteraction_3;
	// UnityEngine.Vector3 MiraExampleInteractionScript::startingLocation
	Vector3_t3722313464  ___startingLocation_4;
	// System.Boolean MiraExampleInteractionScript::isPlanetFeelingSatisfied
	bool ___isPlanetFeelingSatisfied_5;
	// System.Boolean MiraExampleInteractionScript::isUserPointingAtPlanet
	bool ___isUserPointingAtPlanet_6;
	// AxisSpin MiraExampleInteractionScript::spin
	AxisSpin_t1710076314 * ___spin_7;

public:
	inline static int32_t get_offset_of_textBeneathPlanet_2() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t2843749276, ___textBeneathPlanet_2)); }
	inline Text_t1901882714 * get_textBeneathPlanet_2() const { return ___textBeneathPlanet_2; }
	inline Text_t1901882714 ** get_address_of_textBeneathPlanet_2() { return &___textBeneathPlanet_2; }
	inline void set_textBeneathPlanet_2(Text_t1901882714 * value)
	{
		___textBeneathPlanet_2 = value;
		Il2CppCodeGenWriteBarrier((&___textBeneathPlanet_2), value);
	}

	inline static int32_t get_offset_of_timeSinceLastInteraction_3() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t2843749276, ___timeSinceLastInteraction_3)); }
	inline float get_timeSinceLastInteraction_3() const { return ___timeSinceLastInteraction_3; }
	inline float* get_address_of_timeSinceLastInteraction_3() { return &___timeSinceLastInteraction_3; }
	inline void set_timeSinceLastInteraction_3(float value)
	{
		___timeSinceLastInteraction_3 = value;
	}

	inline static int32_t get_offset_of_startingLocation_4() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t2843749276, ___startingLocation_4)); }
	inline Vector3_t3722313464  get_startingLocation_4() const { return ___startingLocation_4; }
	inline Vector3_t3722313464 * get_address_of_startingLocation_4() { return &___startingLocation_4; }
	inline void set_startingLocation_4(Vector3_t3722313464  value)
	{
		___startingLocation_4 = value;
	}

	inline static int32_t get_offset_of_isPlanetFeelingSatisfied_5() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t2843749276, ___isPlanetFeelingSatisfied_5)); }
	inline bool get_isPlanetFeelingSatisfied_5() const { return ___isPlanetFeelingSatisfied_5; }
	inline bool* get_address_of_isPlanetFeelingSatisfied_5() { return &___isPlanetFeelingSatisfied_5; }
	inline void set_isPlanetFeelingSatisfied_5(bool value)
	{
		___isPlanetFeelingSatisfied_5 = value;
	}

	inline static int32_t get_offset_of_isUserPointingAtPlanet_6() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t2843749276, ___isUserPointingAtPlanet_6)); }
	inline bool get_isUserPointingAtPlanet_6() const { return ___isUserPointingAtPlanet_6; }
	inline bool* get_address_of_isUserPointingAtPlanet_6() { return &___isUserPointingAtPlanet_6; }
	inline void set_isUserPointingAtPlanet_6(bool value)
	{
		___isUserPointingAtPlanet_6 = value;
	}

	inline static int32_t get_offset_of_spin_7() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t2843749276, ___spin_7)); }
	inline AxisSpin_t1710076314 * get_spin_7() const { return ___spin_7; }
	inline AxisSpin_t1710076314 ** get_address_of_spin_7() { return &___spin_7; }
	inline void set_spin_7(AxisSpin_t1710076314 * value)
	{
		___spin_7 = value;
		Il2CppCodeGenWriteBarrier((&___spin_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAEXAMPLEINTERACTIONSCRIPT_T2843749276_H
#ifndef MIRAGRABEXAMPLE_T2273097562_H
#define MIRAGRABEXAMPLE_T2273097562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraGrabExample
struct  MiraGrabExample_t2273097562  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MiraGrabExample::textBeneathPlanet
	Text_t1901882714 * ___textBeneathPlanet_2;
	// System.Boolean MiraGrabExample::isGrabbing
	bool ___isGrabbing_3;
	// System.Single MiraGrabExample::lastTouchPosition
	float ___lastTouchPosition_4;

public:
	inline static int32_t get_offset_of_textBeneathPlanet_2() { return static_cast<int32_t>(offsetof(MiraGrabExample_t2273097562, ___textBeneathPlanet_2)); }
	inline Text_t1901882714 * get_textBeneathPlanet_2() const { return ___textBeneathPlanet_2; }
	inline Text_t1901882714 ** get_address_of_textBeneathPlanet_2() { return &___textBeneathPlanet_2; }
	inline void set_textBeneathPlanet_2(Text_t1901882714 * value)
	{
		___textBeneathPlanet_2 = value;
		Il2CppCodeGenWriteBarrier((&___textBeneathPlanet_2), value);
	}

	inline static int32_t get_offset_of_isGrabbing_3() { return static_cast<int32_t>(offsetof(MiraGrabExample_t2273097562, ___isGrabbing_3)); }
	inline bool get_isGrabbing_3() const { return ___isGrabbing_3; }
	inline bool* get_address_of_isGrabbing_3() { return &___isGrabbing_3; }
	inline void set_isGrabbing_3(bool value)
	{
		___isGrabbing_3 = value;
	}

	inline static int32_t get_offset_of_lastTouchPosition_4() { return static_cast<int32_t>(offsetof(MiraGrabExample_t2273097562, ___lastTouchPosition_4)); }
	inline float get_lastTouchPosition_4() const { return ___lastTouchPosition_4; }
	inline float* get_address_of_lastTouchPosition_4() { return &___lastTouchPosition_4; }
	inline void set_lastTouchPosition_4(float value)
	{
		___lastTouchPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAGRABEXAMPLE_T2273097562_H
#ifndef MARKERROTATIONWATCHER_T711531681_H
#define MARKERROTATIONWATCHER_T711531681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerRotationWatcher
struct  MarkerRotationWatcher_t711531681  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform MarkerRotationWatcher::mainCamera
	Transform_t3600365921 * ___mainCamera_2;
	// System.Single MarkerRotationWatcher::deltaDotProduct
	float ___deltaDotProduct_3;
	// System.Single MarkerRotationWatcher::necessaryRotation
	float ___necessaryRotation_4;
	// System.Single MarkerRotationWatcher::dot
	float ___dot_5;
	// UnityEngine.Vector3 MarkerRotationWatcher::startVector
	Vector3_t3722313464  ___startVector_7;
	// System.Boolean MarkerRotationWatcher::firstAngle
	bool ___firstAngle_8;
	// System.Single MarkerRotationWatcher::counter
	float ___counter_9;
	// System.Boolean MarkerRotationWatcher::hasRotated
	bool ___hasRotated_10;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___mainCamera_2)); }
	inline Transform_t3600365921 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Transform_t3600365921 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Transform_t3600365921 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}

	inline static int32_t get_offset_of_deltaDotProduct_3() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___deltaDotProduct_3)); }
	inline float get_deltaDotProduct_3() const { return ___deltaDotProduct_3; }
	inline float* get_address_of_deltaDotProduct_3() { return &___deltaDotProduct_3; }
	inline void set_deltaDotProduct_3(float value)
	{
		___deltaDotProduct_3 = value;
	}

	inline static int32_t get_offset_of_necessaryRotation_4() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___necessaryRotation_4)); }
	inline float get_necessaryRotation_4() const { return ___necessaryRotation_4; }
	inline float* get_address_of_necessaryRotation_4() { return &___necessaryRotation_4; }
	inline void set_necessaryRotation_4(float value)
	{
		___necessaryRotation_4 = value;
	}

	inline static int32_t get_offset_of_dot_5() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___dot_5)); }
	inline float get_dot_5() const { return ___dot_5; }
	inline float* get_address_of_dot_5() { return &___dot_5; }
	inline void set_dot_5(float value)
	{
		___dot_5 = value;
	}

	inline static int32_t get_offset_of_startVector_7() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___startVector_7)); }
	inline Vector3_t3722313464  get_startVector_7() const { return ___startVector_7; }
	inline Vector3_t3722313464 * get_address_of_startVector_7() { return &___startVector_7; }
	inline void set_startVector_7(Vector3_t3722313464  value)
	{
		___startVector_7 = value;
	}

	inline static int32_t get_offset_of_firstAngle_8() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___firstAngle_8)); }
	inline bool get_firstAngle_8() const { return ___firstAngle_8; }
	inline bool* get_address_of_firstAngle_8() { return &___firstAngle_8; }
	inline void set_firstAngle_8(bool value)
	{
		___firstAngle_8 = value;
	}

	inline static int32_t get_offset_of_counter_9() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___counter_9)); }
	inline float get_counter_9() const { return ___counter_9; }
	inline float* get_address_of_counter_9() { return &___counter_9; }
	inline void set_counter_9(float value)
	{
		___counter_9 = value;
	}

	inline static int32_t get_offset_of_hasRotated_10() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681, ___hasRotated_10)); }
	inline bool get_hasRotated_10() const { return ___hasRotated_10; }
	inline bool* get_address_of_hasRotated_10() { return &___hasRotated_10; }
	inline void set_hasRotated_10(bool value)
	{
		___hasRotated_10 = value;
	}
};

struct MarkerRotationWatcher_t711531681_StaticFields
{
public:
	// MarkerRotationWatcher/RotateAction MarkerRotationWatcher::OnRotated
	RotateAction_t1819018245 * ___OnRotated_6;

public:
	inline static int32_t get_offset_of_OnRotated_6() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t711531681_StaticFields, ___OnRotated_6)); }
	inline RotateAction_t1819018245 * get_OnRotated_6() const { return ___OnRotated_6; }
	inline RotateAction_t1819018245 ** get_address_of_OnRotated_6() { return &___OnRotated_6; }
	inline void set_OnRotated_6(RotateAction_t1819018245 * value)
	{
		___OnRotated_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnRotated_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERROTATIONWATCHER_T711531681_H
#ifndef FISHINGLINE_T4150313569_H
#define FISHINGLINE_T4150313569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Fishingline
struct  Fishingline_t4150313569  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform[] Mira.Fishingline::currentSegments
	TransformU5BU5D_t807237628* ___currentSegments_2;
	// UnityEngine.Transform[] Mira.Fishingline::referencePoints
	TransformU5BU5D_t807237628* ___referencePoints_3;
	// UnityEngine.Vector3[] Mira.Fishingline::defaultPositions
	Vector3U5BU5D_t1718750761* ___defaultPositions_4;
	// System.Single Mira.Fishingline::defaultFishlineLength
	float ___defaultFishlineLength_5;
	// System.Int32 Mira.Fishingline::numSegments
	int32_t ___numSegments_6;
	// UnityEngine.Transform Mira.Fishingline::destination
	Transform_t3600365921 * ___destination_7;
	// System.Single Mira.Fishingline::hitDistance
	float ___hitDistance_8;
	// MiraReticle Mira.Fishingline::reticle
	MiraReticle_t910965139 * ___reticle_9;
	// System.Single Mira.Fishingline::lerpSpeedMult
	float ___lerpSpeedMult_10;
	// UnityEngine.Transform Mira.Fishingline::fishinglineParent
	Transform_t3600365921 * ___fishinglineParent_11;
	// UnityEngine.LineRenderer Mira.Fishingline::lineRend
	LineRenderer_t3154350270 * ___lineRend_12;
	// System.Boolean Mira.Fishingline::colliders
	bool ___colliders_13;
	// UnityEngine.GameObject Mira.Fishingline::parentedToEnd
	GameObject_t1113636619 * ___parentedToEnd_14;
	// UnityEngine.Transform Mira.Fishingline::attachPoint
	Transform_t3600365921 * ___attachPoint_15;

public:
	inline static int32_t get_offset_of_currentSegments_2() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___currentSegments_2)); }
	inline TransformU5BU5D_t807237628* get_currentSegments_2() const { return ___currentSegments_2; }
	inline TransformU5BU5D_t807237628** get_address_of_currentSegments_2() { return &___currentSegments_2; }
	inline void set_currentSegments_2(TransformU5BU5D_t807237628* value)
	{
		___currentSegments_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentSegments_2), value);
	}

	inline static int32_t get_offset_of_referencePoints_3() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___referencePoints_3)); }
	inline TransformU5BU5D_t807237628* get_referencePoints_3() const { return ___referencePoints_3; }
	inline TransformU5BU5D_t807237628** get_address_of_referencePoints_3() { return &___referencePoints_3; }
	inline void set_referencePoints_3(TransformU5BU5D_t807237628* value)
	{
		___referencePoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___referencePoints_3), value);
	}

	inline static int32_t get_offset_of_defaultPositions_4() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___defaultPositions_4)); }
	inline Vector3U5BU5D_t1718750761* get_defaultPositions_4() const { return ___defaultPositions_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_defaultPositions_4() { return &___defaultPositions_4; }
	inline void set_defaultPositions_4(Vector3U5BU5D_t1718750761* value)
	{
		___defaultPositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultPositions_4), value);
	}

	inline static int32_t get_offset_of_defaultFishlineLength_5() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___defaultFishlineLength_5)); }
	inline float get_defaultFishlineLength_5() const { return ___defaultFishlineLength_5; }
	inline float* get_address_of_defaultFishlineLength_5() { return &___defaultFishlineLength_5; }
	inline void set_defaultFishlineLength_5(float value)
	{
		___defaultFishlineLength_5 = value;
	}

	inline static int32_t get_offset_of_numSegments_6() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___numSegments_6)); }
	inline int32_t get_numSegments_6() const { return ___numSegments_6; }
	inline int32_t* get_address_of_numSegments_6() { return &___numSegments_6; }
	inline void set_numSegments_6(int32_t value)
	{
		___numSegments_6 = value;
	}

	inline static int32_t get_offset_of_destination_7() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___destination_7)); }
	inline Transform_t3600365921 * get_destination_7() const { return ___destination_7; }
	inline Transform_t3600365921 ** get_address_of_destination_7() { return &___destination_7; }
	inline void set_destination_7(Transform_t3600365921 * value)
	{
		___destination_7 = value;
		Il2CppCodeGenWriteBarrier((&___destination_7), value);
	}

	inline static int32_t get_offset_of_hitDistance_8() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___hitDistance_8)); }
	inline float get_hitDistance_8() const { return ___hitDistance_8; }
	inline float* get_address_of_hitDistance_8() { return &___hitDistance_8; }
	inline void set_hitDistance_8(float value)
	{
		___hitDistance_8 = value;
	}

	inline static int32_t get_offset_of_reticle_9() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___reticle_9)); }
	inline MiraReticle_t910965139 * get_reticle_9() const { return ___reticle_9; }
	inline MiraReticle_t910965139 ** get_address_of_reticle_9() { return &___reticle_9; }
	inline void set_reticle_9(MiraReticle_t910965139 * value)
	{
		___reticle_9 = value;
		Il2CppCodeGenWriteBarrier((&___reticle_9), value);
	}

	inline static int32_t get_offset_of_lerpSpeedMult_10() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___lerpSpeedMult_10)); }
	inline float get_lerpSpeedMult_10() const { return ___lerpSpeedMult_10; }
	inline float* get_address_of_lerpSpeedMult_10() { return &___lerpSpeedMult_10; }
	inline void set_lerpSpeedMult_10(float value)
	{
		___lerpSpeedMult_10 = value;
	}

	inline static int32_t get_offset_of_fishinglineParent_11() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___fishinglineParent_11)); }
	inline Transform_t3600365921 * get_fishinglineParent_11() const { return ___fishinglineParent_11; }
	inline Transform_t3600365921 ** get_address_of_fishinglineParent_11() { return &___fishinglineParent_11; }
	inline void set_fishinglineParent_11(Transform_t3600365921 * value)
	{
		___fishinglineParent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fishinglineParent_11), value);
	}

	inline static int32_t get_offset_of_lineRend_12() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___lineRend_12)); }
	inline LineRenderer_t3154350270 * get_lineRend_12() const { return ___lineRend_12; }
	inline LineRenderer_t3154350270 ** get_address_of_lineRend_12() { return &___lineRend_12; }
	inline void set_lineRend_12(LineRenderer_t3154350270 * value)
	{
		___lineRend_12 = value;
		Il2CppCodeGenWriteBarrier((&___lineRend_12), value);
	}

	inline static int32_t get_offset_of_colliders_13() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___colliders_13)); }
	inline bool get_colliders_13() const { return ___colliders_13; }
	inline bool* get_address_of_colliders_13() { return &___colliders_13; }
	inline void set_colliders_13(bool value)
	{
		___colliders_13 = value;
	}

	inline static int32_t get_offset_of_parentedToEnd_14() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___parentedToEnd_14)); }
	inline GameObject_t1113636619 * get_parentedToEnd_14() const { return ___parentedToEnd_14; }
	inline GameObject_t1113636619 ** get_address_of_parentedToEnd_14() { return &___parentedToEnd_14; }
	inline void set_parentedToEnd_14(GameObject_t1113636619 * value)
	{
		___parentedToEnd_14 = value;
		Il2CppCodeGenWriteBarrier((&___parentedToEnd_14), value);
	}

	inline static int32_t get_offset_of_attachPoint_15() { return static_cast<int32_t>(offsetof(Fishingline_t4150313569, ___attachPoint_15)); }
	inline Transform_t3600365921 * get_attachPoint_15() const { return ___attachPoint_15; }
	inline Transform_t3600365921 ** get_address_of_attachPoint_15() { return &___attachPoint_15; }
	inline void set_attachPoint_15(Transform_t3600365921 * value)
	{
		___attachPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___attachPoint_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHINGLINE_T4150313569_H
#ifndef FISHINGLINECAST_T2818965060_H
#define FISHINGLINECAST_T2818965060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.FishinglineCast
struct  FishinglineCast_t2818965060  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.FishinglineCast::minCast
	float ___minCast_2;
	// System.Single Mira.FishinglineCast::maxCast
	float ___maxCast_3;
	// System.Single Mira.FishinglineCast::castSpeedMultiplier
	float ___castSpeedMultiplier_4;
	// System.Boolean Mira.FishinglineCast::castStart
	bool ___castStart_5;
	// System.Single Mira.FishinglineCast::lastCast
	float ___lastCast_6;
	// Mira.Fishingline Mira.FishinglineCast::fishingline
	Fishingline_t4150313569 * ___fishingline_7;
	// System.Single Mira.FishinglineCast::currentScaleMult
	float ___currentScaleMult_8;

public:
	inline static int32_t get_offset_of_minCast_2() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___minCast_2)); }
	inline float get_minCast_2() const { return ___minCast_2; }
	inline float* get_address_of_minCast_2() { return &___minCast_2; }
	inline void set_minCast_2(float value)
	{
		___minCast_2 = value;
	}

	inline static int32_t get_offset_of_maxCast_3() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___maxCast_3)); }
	inline float get_maxCast_3() const { return ___maxCast_3; }
	inline float* get_address_of_maxCast_3() { return &___maxCast_3; }
	inline void set_maxCast_3(float value)
	{
		___maxCast_3 = value;
	}

	inline static int32_t get_offset_of_castSpeedMultiplier_4() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___castSpeedMultiplier_4)); }
	inline float get_castSpeedMultiplier_4() const { return ___castSpeedMultiplier_4; }
	inline float* get_address_of_castSpeedMultiplier_4() { return &___castSpeedMultiplier_4; }
	inline void set_castSpeedMultiplier_4(float value)
	{
		___castSpeedMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_castStart_5() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___castStart_5)); }
	inline bool get_castStart_5() const { return ___castStart_5; }
	inline bool* get_address_of_castStart_5() { return &___castStart_5; }
	inline void set_castStart_5(bool value)
	{
		___castStart_5 = value;
	}

	inline static int32_t get_offset_of_lastCast_6() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___lastCast_6)); }
	inline float get_lastCast_6() const { return ___lastCast_6; }
	inline float* get_address_of_lastCast_6() { return &___lastCast_6; }
	inline void set_lastCast_6(float value)
	{
		___lastCast_6 = value;
	}

	inline static int32_t get_offset_of_fishingline_7() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___fishingline_7)); }
	inline Fishingline_t4150313569 * get_fishingline_7() const { return ___fishingline_7; }
	inline Fishingline_t4150313569 ** get_address_of_fishingline_7() { return &___fishingline_7; }
	inline void set_fishingline_7(Fishingline_t4150313569 * value)
	{
		___fishingline_7 = value;
		Il2CppCodeGenWriteBarrier((&___fishingline_7), value);
	}

	inline static int32_t get_offset_of_currentScaleMult_8() { return static_cast<int32_t>(offsetof(FishinglineCast_t2818965060, ___currentScaleMult_8)); }
	inline float get_currentScaleMult_8() const { return ___currentScaleMult_8; }
	inline float* get_address_of_currentScaleMult_8() { return &___currentScaleMult_8; }
	inline void set_currentScaleMult_8(float value)
	{
		___currentScaleMult_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHINGLINECAST_T2818965060_H
#ifndef TMP_FRAMERATECOUNTER_T314972976_H
#define TMP_FRAMERATECOUNTER_T314972976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t314972976  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_3;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_4;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_5;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_6;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_8;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_9;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t4157153871 * ___m_camera_10;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_11;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_3() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_LastInterval_3)); }
	inline float get_m_LastInterval_3() const { return ___m_LastInterval_3; }
	inline float* get_address_of_m_LastInterval_3() { return &___m_LastInterval_3; }
	inline void set_m_LastInterval_3(float value)
	{
		___m_LastInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_Frames_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_Frames_4)); }
	inline int32_t get_m_Frames_4() const { return ___m_Frames_4; }
	inline int32_t* get_address_of_m_Frames_4() { return &___m_Frames_4; }
	inline void set_m_Frames_4(int32_t value)
	{
		___m_Frames_4 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___AnchorPosition_5)); }
	inline int32_t get_AnchorPosition_5() const { return ___AnchorPosition_5; }
	inline int32_t* get_address_of_AnchorPosition_5() { return &___AnchorPosition_5; }
	inline void set_AnchorPosition_5(int32_t value)
	{
		___AnchorPosition_5 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___htmlColorTag_6)); }
	inline String_t* get_htmlColorTag_6() const { return ___htmlColorTag_6; }
	inline String_t** get_address_of_htmlColorTag_6() { return &___htmlColorTag_6; }
	inline void set_htmlColorTag_6(String_t* value)
	{
		___htmlColorTag_6 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_TextMeshPro_8)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_8() const { return ___m_TextMeshPro_8; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_8() { return &___m_TextMeshPro_8; }
	inline void set_m_TextMeshPro_8(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_9() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_frameCounter_transform_9)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_9() const { return ___m_frameCounter_transform_9; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_9() { return &___m_frameCounter_transform_9; }
	inline void set_m_frameCounter_transform_9(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_9), value);
	}

	inline static int32_t get_offset_of_m_camera_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_camera_10)); }
	inline Camera_t4157153871 * get_m_camera_10() const { return ___m_camera_10; }
	inline Camera_t4157153871 ** get_address_of_m_camera_10() { return &___m_camera_10; }
	inline void set_m_camera_10(Camera_t4157153871 * value)
	{
		___m_camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_10), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___last_AnchorPosition_11)); }
	inline int32_t get_last_AnchorPosition_11() const { return ___last_AnchorPosition_11; }
	inline int32_t* get_address_of_last_AnchorPosition_11() { return &___last_AnchorPosition_11; }
	inline void set_last_AnchorPosition_11(int32_t value)
	{
		___last_AnchorPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T314972976_H
#ifndef TMP_TEXTEVENTCHECK_T1103849140_H
#define TMP_TEXTEVENTCHECK_T1103849140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextEventCheck
struct  TMP_TextEventCheck_t1103849140  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t1869054637 * ___TextEventHandler_2;

public:
	inline static int32_t get_offset_of_TextEventHandler_2() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t1103849140, ___TextEventHandler_2)); }
	inline TMP_TextEventHandler_t1869054637 * get_TextEventHandler_2() const { return ___TextEventHandler_2; }
	inline TMP_TextEventHandler_t1869054637 ** get_address_of_TextEventHandler_2() { return &___TextEventHandler_2; }
	inline void set_TextEventHandler_2(TMP_TextEventHandler_t1869054637 * value)
	{
		___TextEventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextEventHandler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTCHECK_T1103849140_H
#ifndef TMP_EXAMPLESCRIPT_01_T3051742005_H
#define TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t3051742005  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_2;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_3;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t2599618874 * ___m_text_4;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_ObjectType_2() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___ObjectType_2)); }
	inline int32_t get_ObjectType_2() const { return ___ObjectType_2; }
	inline int32_t* get_address_of_ObjectType_2() { return &___ObjectType_2; }
	inline void set_ObjectType_2(int32_t value)
	{
		___ObjectType_2 = value;
	}

	inline static int32_t get_offset_of_isStatic_3() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___isStatic_3)); }
	inline bool get_isStatic_3() const { return ___isStatic_3; }
	inline bool* get_address_of_isStatic_3() { return &___isStatic_3; }
	inline void set_isStatic_3(bool value)
	{
		___isStatic_3 = value;
	}

	inline static int32_t get_offset_of_m_text_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___m_text_4)); }
	inline TMP_Text_t2599618874 * get_m_text_4() const { return ___m_text_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_text_4() { return &___m_text_4; }
	inline void set_m_text_4(TMP_Text_t2599618874 * value)
	{
		___m_text_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_4), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifndef TEXTMESHPROFLOATINGTEXT_T845872552_H
#define TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText
struct  TextMeshProFloatingText_t845872552  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_t1956802104 * ___TheFont_2;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_t1113636619 * ___m_floatingText_3;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_4;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_5;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_t3600365921 * ___m_transform_6;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_t3600365921 * ___m_floatingText_Transform_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_t3600365921 * ___m_cameraTransform_8;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t3722313464  ___lastPOS_9;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t2301928331  ___lastRotation_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_11;

public:
	inline static int32_t get_offset_of_TheFont_2() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___TheFont_2)); }
	inline Font_t1956802104 * get_TheFont_2() const { return ___TheFont_2; }
	inline Font_t1956802104 ** get_address_of_TheFont_2() { return &___TheFont_2; }
	inline void set_TheFont_2(Font_t1956802104 * value)
	{
		___TheFont_2 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_2), value);
	}

	inline static int32_t get_offset_of_m_floatingText_3() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_3)); }
	inline GameObject_t1113636619 * get_m_floatingText_3() const { return ___m_floatingText_3; }
	inline GameObject_t1113636619 ** get_address_of_m_floatingText_3() { return &___m_floatingText_3; }
	inline void set_m_floatingText_3(GameObject_t1113636619 * value)
	{
		___m_floatingText_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_textMesh_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMesh_5)); }
	inline TextMesh_t1536577757 * get_m_textMesh_5() const { return ___m_textMesh_5; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_5() { return &___m_textMesh_5; }
	inline void set_m_textMesh_5(TextMesh_t1536577757 * value)
	{
		___m_textMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_5), value);
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_transform_6)); }
	inline Transform_t3600365921 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_t3600365921 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_Transform_7)); }
	inline Transform_t3600365921 * get_m_floatingText_Transform_7() const { return ___m_floatingText_Transform_7; }
	inline Transform_t3600365921 ** get_address_of_m_floatingText_Transform_7() { return &___m_floatingText_Transform_7; }
	inline void set_m_floatingText_Transform_7(Transform_t3600365921 * value)
	{
		___m_floatingText_Transform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_Transform_7), value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_cameraTransform_8)); }
	inline Transform_t3600365921 * get_m_cameraTransform_8() const { return ___m_cameraTransform_8; }
	inline Transform_t3600365921 ** get_address_of_m_cameraTransform_8() { return &___m_cameraTransform_8; }
	inline void set_m_cameraTransform_8(Transform_t3600365921 * value)
	{
		___m_cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_lastPOS_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastPOS_9)); }
	inline Vector3_t3722313464  get_lastPOS_9() const { return ___lastPOS_9; }
	inline Vector3_t3722313464 * get_address_of_lastPOS_9() { return &___lastPOS_9; }
	inline void set_lastPOS_9(Vector3_t3722313464  value)
	{
		___lastPOS_9 = value;
	}

	inline static int32_t get_offset_of_lastRotation_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastRotation_10)); }
	inline Quaternion_t2301928331  get_lastRotation_10() const { return ___lastRotation_10; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_10() { return &___lastRotation_10; }
	inline void set_lastRotation_10(Quaternion_t2301928331  value)
	{
		___lastRotation_10 = value;
	}

	inline static int32_t get_offset_of_SpawnType_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___SpawnType_11)); }
	inline int32_t get_SpawnType_11() const { return ___SpawnType_11; }
	inline int32_t* get_address_of_SpawnType_11() { return &___SpawnType_11; }
	inline void set_SpawnType_11(int32_t value)
	{
		___SpawnType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifndef TEXTMESHSPAWNER_T177691618_H
#define TEXTMESHSPAWNER_T177691618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t177691618  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t1956802104 * ___TheFont_4;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_5;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___floatingText_Script_5)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_5() const { return ___floatingText_Script_5; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_5() { return &___floatingText_Script_5; }
	inline void set_floatingText_Script_5(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_5 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T177691618_H
#ifndef TMP_TEXTSELECTOR_B_T3982526505_H
#define TMP_TEXTSELECTOR_B_T3982526505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_B
struct  TMP_TextSelector_B_t3982526505  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t3704657025 * ___TextPopup_Prefab_01_2;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t3704657025 * ___m_TextPopup_RectTransform_3;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_t529313277 * ___m_TextPopup_TMPComponent_4;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_7;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_8;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_t4157153871 * ___m_Camera_9;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_10;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_11;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_13;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_t1817901843  ___m_matrix_14;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t3365986247* ___m_cachedMeshInfoVertexData_15;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_2() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___TextPopup_Prefab_01_2)); }
	inline RectTransform_t3704657025 * get_TextPopup_Prefab_01_2() const { return ___TextPopup_Prefab_01_2; }
	inline RectTransform_t3704657025 ** get_address_of_TextPopup_Prefab_01_2() { return &___TextPopup_Prefab_01_2; }
	inline void set_TextPopup_Prefab_01_2(RectTransform_t3704657025 * value)
	{
		___TextPopup_Prefab_01_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextPopup_Prefab_01_2), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_3() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_RectTransform_3)); }
	inline RectTransform_t3704657025 * get_m_TextPopup_RectTransform_3() const { return ___m_TextPopup_RectTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextPopup_RectTransform_3() { return &___m_TextPopup_RectTransform_3; }
	inline void set_m_TextPopup_RectTransform_3(RectTransform_t3704657025 * value)
	{
		___m_TextPopup_RectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_RectTransform_3), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_TMPComponent_4)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextPopup_TMPComponent_4() const { return ___m_TextPopup_TMPComponent_4; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextPopup_TMPComponent_4() { return &___m_TextPopup_TMPComponent_4; }
	inline void set_m_TextPopup_TMPComponent_4(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextPopup_TMPComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_TMPComponent_4), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextMeshPro_7)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_7() const { return ___m_TextMeshPro_7; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_7() { return &___m_TextMeshPro_7; }
	inline void set_m_TextMeshPro_7(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Canvas_8)); }
	inline Canvas_t3310196443 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_t3310196443 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_Camera_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Camera_9)); }
	inline Camera_t4157153871 * get_m_Camera_9() const { return ___m_Camera_9; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_9() { return &___m_Camera_9; }
	inline void set_m_Camera_9(Camera_t4157153871 * value)
	{
		___m_Camera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_9), value);
	}

	inline static int32_t get_offset_of_isHoveringObject_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___isHoveringObject_10)); }
	inline bool get_isHoveringObject_10() const { return ___isHoveringObject_10; }
	inline bool* get_address_of_isHoveringObject_10() { return &___isHoveringObject_10; }
	inline void set_isHoveringObject_10(bool value)
	{
		___isHoveringObject_10 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedWord_11)); }
	inline int32_t get_m_selectedWord_11() const { return ___m_selectedWord_11; }
	inline int32_t* get_address_of_m_selectedWord_11() { return &___m_selectedWord_11; }
	inline void set_m_selectedWord_11(int32_t value)
	{
		___m_selectedWord_11 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_lastIndex_13)); }
	inline int32_t get_m_lastIndex_13() const { return ___m_lastIndex_13; }
	inline int32_t* get_address_of_m_lastIndex_13() { return &___m_lastIndex_13; }
	inline void set_m_lastIndex_13(int32_t value)
	{
		___m_lastIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_matrix_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_matrix_14)); }
	inline Matrix4x4_t1817901843  get_m_matrix_14() const { return ___m_matrix_14; }
	inline Matrix4x4_t1817901843 * get_address_of_m_matrix_14() { return &___m_matrix_14; }
	inline void set_m_matrix_14(Matrix4x4_t1817901843  value)
	{
		___m_matrix_14 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_cachedMeshInfoVertexData_15)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_cachedMeshInfoVertexData_15() const { return ___m_cachedMeshInfoVertexData_15; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_cachedMeshInfoVertexData_15() { return &___m_cachedMeshInfoVertexData_15; }
	inline void set_m_cachedMeshInfoVertexData_15(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_cachedMeshInfoVertexData_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedMeshInfoVertexData_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_B_T3982526505_H
#ifndef TMP_UIFRAMERATECOUNTER_T811747397_H
#define TMP_UIFRAMERATECOUNTER_T811747397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter
struct  TMP_UiFrameRateCounter_t811747397  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_3;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_4;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_5;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_8;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t3704657025 * ___m_frameCounter_transform_9;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_10;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_3() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_LastInterval_3)); }
	inline float get_m_LastInterval_3() const { return ___m_LastInterval_3; }
	inline float* get_address_of_m_LastInterval_3() { return &___m_LastInterval_3; }
	inline void set_m_LastInterval_3(float value)
	{
		___m_LastInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_Frames_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_Frames_4)); }
	inline int32_t get_m_Frames_4() const { return ___m_Frames_4; }
	inline int32_t* get_address_of_m_Frames_4() { return &___m_Frames_4; }
	inline void set_m_Frames_4(int32_t value)
	{
		___m_Frames_4 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___AnchorPosition_5)); }
	inline int32_t get_AnchorPosition_5() const { return ___AnchorPosition_5; }
	inline int32_t* get_address_of_AnchorPosition_5() { return &___AnchorPosition_5; }
	inline void set_AnchorPosition_5(int32_t value)
	{
		___AnchorPosition_5 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___htmlColorTag_6)); }
	inline String_t* get_htmlColorTag_6() const { return ___htmlColorTag_6; }
	inline String_t** get_address_of_htmlColorTag_6() { return &___htmlColorTag_6; }
	inline void set_htmlColorTag_6(String_t* value)
	{
		___htmlColorTag_6 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_TextMeshPro_8)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_8() const { return ___m_TextMeshPro_8; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_8() { return &___m_TextMeshPro_8; }
	inline void set_m_TextMeshPro_8(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_9() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_frameCounter_transform_9)); }
	inline RectTransform_t3704657025 * get_m_frameCounter_transform_9() const { return ___m_frameCounter_transform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_frameCounter_transform_9() { return &___m_frameCounter_transform_9; }
	inline void set_m_frameCounter_transform_9(RectTransform_t3704657025 * value)
	{
		___m_frameCounter_transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_9), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___last_AnchorPosition_10)); }
	inline int32_t get_last_AnchorPosition_10() const { return ___last_AnchorPosition_10; }
	inline int32_t* get_address_of_last_AnchorPosition_10() { return &___last_AnchorPosition_10; }
	inline void set_last_AnchorPosition_10(int32_t value)
	{
		___last_AnchorPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UIFRAMERATECOUNTER_T811747397_H
#ifndef TMP_TEXTSELECTOR_A_T3982526506_H
#define TMP_TEXTSELECTOR_A_T3982526506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_A
struct  TMP_TextSelector_A_t3982526506  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_2;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_t4157153871 * ___m_Camera_3;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_4;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_5;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_7;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_2() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_TextMeshPro_2)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_2() const { return ___m_TextMeshPro_2; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_2() { return &___m_TextMeshPro_2; }
	inline void set_m_TextMeshPro_2(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_Camera_3() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_Camera_3)); }
	inline Camera_t4157153871 * get_m_Camera_3() const { return ___m_Camera_3; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_3() { return &___m_Camera_3; }
	inline void set_m_Camera_3(Camera_t4157153871 * value)
	{
		___m_Camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_3), value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_isHoveringObject_4)); }
	inline bool get_m_isHoveringObject_4() const { return ___m_isHoveringObject_4; }
	inline bool* get_address_of_m_isHoveringObject_4() { return &___m_isHoveringObject_4; }
	inline void set_m_isHoveringObject_4(bool value)
	{
		___m_isHoveringObject_4 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_selectedLink_5)); }
	inline int32_t get_m_selectedLink_5() const { return ___m_selectedLink_5; }
	inline int32_t* get_address_of_m_selectedLink_5() { return &___m_selectedLink_5; }
	inline void set_m_selectedLink_5(int32_t value)
	{
		___m_selectedLink_5 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastCharIndex_6)); }
	inline int32_t get_m_lastCharIndex_6() const { return ___m_lastCharIndex_6; }
	inline int32_t* get_address_of_m_lastCharIndex_6() { return &___m_lastCharIndex_6; }
	inline void set_m_lastCharIndex_6(int32_t value)
	{
		___m_lastCharIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastWordIndex_7)); }
	inline int32_t get_m_lastWordIndex_7() const { return ___m_lastWordIndex_7; }
	inline int32_t* get_address_of_m_lastWordIndex_7() { return &___m_lastWordIndex_7; }
	inline void set_m_lastWordIndex_7(int32_t value)
	{
		___m_lastWordIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_A_T3982526506_H
#ifndef TMP_TEXTEVENTHANDLER_T1869054637_H
#define TMP_TEXTEVENTHANDLER_T1869054637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t1869054637  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t3109943174 * ___m_OnCharacterSelection_2;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_t1841909953 * ___m_OnWordSelection_3;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t2868010532 * ___m_OnLineSelection_4;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t1590929858 * ___m_OnLinkSelection_5;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_6;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t4157153871 * ___m_Camera_7;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_8;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_9;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_10;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_12;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_2() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnCharacterSelection_2)); }
	inline CharacterSelectionEvent_t3109943174 * get_m_OnCharacterSelection_2() const { return ___m_OnCharacterSelection_2; }
	inline CharacterSelectionEvent_t3109943174 ** get_address_of_m_OnCharacterSelection_2() { return &___m_OnCharacterSelection_2; }
	inline void set_m_OnCharacterSelection_2(CharacterSelectionEvent_t3109943174 * value)
	{
		___m_OnCharacterSelection_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_2), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_3() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnWordSelection_3)); }
	inline WordSelectionEvent_t1841909953 * get_m_OnWordSelection_3() const { return ___m_OnWordSelection_3; }
	inline WordSelectionEvent_t1841909953 ** get_address_of_m_OnWordSelection_3() { return &___m_OnWordSelection_3; }
	inline void set_m_OnWordSelection_3(WordSelectionEvent_t1841909953 * value)
	{
		___m_OnWordSelection_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_3), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLineSelection_4)); }
	inline LineSelectionEvent_t2868010532 * get_m_OnLineSelection_4() const { return ___m_OnLineSelection_4; }
	inline LineSelectionEvent_t2868010532 ** get_address_of_m_OnLineSelection_4() { return &___m_OnLineSelection_4; }
	inline void set_m_OnLineSelection_4(LineSelectionEvent_t2868010532 * value)
	{
		___m_OnLineSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLinkSelection_5)); }
	inline LinkSelectionEvent_t1590929858 * get_m_OnLinkSelection_5() const { return ___m_OnLinkSelection_5; }
	inline LinkSelectionEvent_t1590929858 ** get_address_of_m_OnLinkSelection_5() { return &___m_OnLinkSelection_5; }
	inline void set_m_OnLinkSelection_5(LinkSelectionEvent_t1590929858 * value)
	{
		___m_OnLinkSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_5), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_TextComponent_6)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_6() const { return ___m_TextComponent_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_6() { return &___m_TextComponent_6; }
	inline void set_m_TextComponent_6(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_6), value);
	}

	inline static int32_t get_offset_of_m_Camera_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Camera_7)); }
	inline Camera_t4157153871 * get_m_Camera_7() const { return ___m_Camera_7; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_7() { return &___m_Camera_7; }
	inline void set_m_Camera_7(Camera_t4157153871 * value)
	{
		___m_Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Canvas_8)); }
	inline Canvas_t3310196443 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_t3310196443 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_selectedLink_9)); }
	inline int32_t get_m_selectedLink_9() const { return ___m_selectedLink_9; }
	inline int32_t* get_address_of_m_selectedLink_9() { return &___m_selectedLink_9; }
	inline void set_m_selectedLink_9(int32_t value)
	{
		___m_selectedLink_9 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastCharIndex_10)); }
	inline int32_t get_m_lastCharIndex_10() const { return ___m_lastCharIndex_10; }
	inline int32_t* get_address_of_m_lastCharIndex_10() { return &___m_lastCharIndex_10; }
	inline void set_m_lastCharIndex_10(int32_t value)
	{
		___m_lastCharIndex_10 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastWordIndex_11)); }
	inline int32_t get_m_lastWordIndex_11() const { return ___m_lastWordIndex_11; }
	inline int32_t* get_address_of_m_lastWordIndex_11() { return &___m_lastWordIndex_11; }
	inline void set_m_lastWordIndex_11(int32_t value)
	{
		___m_lastWordIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastLineIndex_12)); }
	inline int32_t get_m_lastLineIndex_12() const { return ___m_lastLineIndex_12; }
	inline int32_t* get_address_of_m_lastLineIndex_12() { return &___m_lastLineIndex_12; }
	inline void set_m_lastLineIndex_12(int32_t value)
	{
		___m_lastLineIndex_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T1869054637_H
#ifndef TMP_TEXTINFODEBUGTOOL_T1868681444_H
#define TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextInfoDebugTool
struct  TMP_TextInfoDebugTool_t1868681444  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowCharacters
	bool ___ShowCharacters_2;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowWords
	bool ___ShowWords_3;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLinks
	bool ___ShowLinks_4;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLines
	bool ___ShowLines_5;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowMeshBounds
	bool ___ShowMeshBounds_6;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowTextBounds
	bool ___ShowTextBounds_7;
	// System.String TMPro.Examples.TMP_TextInfoDebugTool::ObjectStats
	String_t* ___ObjectStats_8;
	// TMPro.TMP_Text TMPro.Examples.TMP_TextInfoDebugTool::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_9;
	// UnityEngine.Transform TMPro.Examples.TMP_TextInfoDebugTool::m_Transform
	Transform_t3600365921 * ___m_Transform_10;

public:
	inline static int32_t get_offset_of_ShowCharacters_2() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowCharacters_2)); }
	inline bool get_ShowCharacters_2() const { return ___ShowCharacters_2; }
	inline bool* get_address_of_ShowCharacters_2() { return &___ShowCharacters_2; }
	inline void set_ShowCharacters_2(bool value)
	{
		___ShowCharacters_2 = value;
	}

	inline static int32_t get_offset_of_ShowWords_3() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowWords_3)); }
	inline bool get_ShowWords_3() const { return ___ShowWords_3; }
	inline bool* get_address_of_ShowWords_3() { return &___ShowWords_3; }
	inline void set_ShowWords_3(bool value)
	{
		___ShowWords_3 = value;
	}

	inline static int32_t get_offset_of_ShowLinks_4() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLinks_4)); }
	inline bool get_ShowLinks_4() const { return ___ShowLinks_4; }
	inline bool* get_address_of_ShowLinks_4() { return &___ShowLinks_4; }
	inline void set_ShowLinks_4(bool value)
	{
		___ShowLinks_4 = value;
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_ShowMeshBounds_6() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowMeshBounds_6)); }
	inline bool get_ShowMeshBounds_6() const { return ___ShowMeshBounds_6; }
	inline bool* get_address_of_ShowMeshBounds_6() { return &___ShowMeshBounds_6; }
	inline void set_ShowMeshBounds_6(bool value)
	{
		___ShowMeshBounds_6 = value;
	}

	inline static int32_t get_offset_of_ShowTextBounds_7() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowTextBounds_7)); }
	inline bool get_ShowTextBounds_7() const { return ___ShowTextBounds_7; }
	inline bool* get_address_of_ShowTextBounds_7() { return &___ShowTextBounds_7; }
	inline void set_ShowTextBounds_7(bool value)
	{
		___ShowTextBounds_7 = value;
	}

	inline static int32_t get_offset_of_ObjectStats_8() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ObjectStats_8)); }
	inline String_t* get_ObjectStats_8() const { return ___ObjectStats_8; }
	inline String_t** get_address_of_ObjectStats_8() { return &___ObjectStats_8; }
	inline void set_ObjectStats_8(String_t* value)
	{
		___ObjectStats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectStats_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_TextComponent_9)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Transform_10() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_Transform_10)); }
	inline Transform_t3600365921 * get_m_Transform_10() const { return ___m_Transform_10; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_10() { return &___m_Transform_10; }
	inline void set_m_Transform_10(Transform_t3600365921 * value)
	{
		___m_Transform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifndef TEXTCONSOLESIMULATOR_T3766250034_H
#define TEXTCONSOLESIMULATOR_T3766250034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t3766250034  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_3;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_3() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___hasTextChanged_3)); }
	inline bool get_hasTextChanged_3() const { return ___hasTextChanged_3; }
	inline bool* get_address_of_hasTextChanged_3() { return &___hasTextChanged_3; }
	inline void set_hasTextChanged_3(bool value)
	{
		___hasTextChanged_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T3766250034_H
#ifndef CHATCONTROLLER_T3486202795_H
#define CHATCONTROLLER_T3486202795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t3486202795  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1099764886 * ___TMP_ChatInput_2;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t2599618874 * ___TMP_ChatOutput_3;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t1494447233 * ___ChatScrollbar_4;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_2() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatInput_2)); }
	inline TMP_InputField_t1099764886 * get_TMP_ChatInput_2() const { return ___TMP_ChatInput_2; }
	inline TMP_InputField_t1099764886 ** get_address_of_TMP_ChatInput_2() { return &___TMP_ChatInput_2; }
	inline void set_TMP_ChatInput_2(TMP_InputField_t1099764886 * value)
	{
		___TMP_ChatInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_2), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_3() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatOutput_3)); }
	inline TMP_Text_t2599618874 * get_TMP_ChatOutput_3() const { return ___TMP_ChatOutput_3; }
	inline TMP_Text_t2599618874 ** get_address_of_TMP_ChatOutput_3() { return &___TMP_ChatOutput_3; }
	inline void set_TMP_ChatOutput_3(TMP_Text_t2599618874 * value)
	{
		___TMP_ChatOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_3), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_4() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___ChatScrollbar_4)); }
	inline Scrollbar_t1494447233 * get_ChatScrollbar_4() const { return ___ChatScrollbar_4; }
	inline Scrollbar_t1494447233 ** get_address_of_ChatScrollbar_4() { return &___ChatScrollbar_4; }
	inline void set_ChatScrollbar_4(Scrollbar_t1494447233 * value)
	{
		___ChatScrollbar_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T3486202795_H
#ifndef ENVMAPANIMATOR_T1140999784_H
#define ENVMAPANIMATOR_T1140999784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_2;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_3;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_4;

public:
	inline static int32_t get_offset_of_RotationSpeeds_2() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_2)); }
	inline Vector3_t3722313464  get_RotationSpeeds_2() const { return ___RotationSpeeds_2; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_2() { return &___RotationSpeeds_2; }
	inline void set_RotationSpeeds_2(Vector3_t3722313464  value)
	{
		___RotationSpeeds_2 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_3() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_3)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_3() const { return ___m_textMeshPro_3; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_3() { return &___m_textMeshPro_3; }
	inline void set_m_textMeshPro_3(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1140999784_H
#ifndef BENCHMARK04_T1570876016_H
#define BENCHMARK04_T1570876016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t1570876016  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_3;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_4;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_5;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3600365921 * ___m_Transform_6;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_3() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MinPointSize_3)); }
	inline int32_t get_MinPointSize_3() const { return ___MinPointSize_3; }
	inline int32_t* get_address_of_MinPointSize_3() { return &___MinPointSize_3; }
	inline void set_MinPointSize_3(int32_t value)
	{
		___MinPointSize_3 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_4() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MaxPointSize_4)); }
	inline int32_t get_MaxPointSize_4() const { return ___MaxPointSize_4; }
	inline int32_t* get_address_of_MaxPointSize_4() { return &___MaxPointSize_4; }
	inline void set_MaxPointSize_4(int32_t value)
	{
		___MaxPointSize_4 = value;
	}

	inline static int32_t get_offset_of_Steps_5() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___Steps_5)); }
	inline int32_t get_Steps_5() const { return ___Steps_5; }
	inline int32_t* get_address_of_Steps_5() { return &___Steps_5; }
	inline void set_Steps_5(int32_t value)
	{
		___Steps_5 = value;
	}

	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___m_Transform_6)); }
	inline Transform_t3600365921 * get_m_Transform_6() const { return ___m_Transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Transform_t3600365921 * value)
	{
		___m_Transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T1570876016_H
#ifndef CAMERACONTROLLER_T2264742161_H
#define CAMERACONTROLLER_T2264742161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t2264742161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3600365921 * ___cameraTransform_2;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3600365921 * ___dummyTarget_3;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3600365921 * ___CameraTarget_4;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_5;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_6;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_7;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_8;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_9;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_11;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_12;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_13;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_14;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_15;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_16;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_17;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_18;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_19;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t3722313464  ___desiredPosition_20;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_21;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_22;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t3722313464  ___moveVector_23;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_24;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___cameraTransform_2)); }
	inline Transform_t3600365921 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3600365921 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}

	inline static int32_t get_offset_of_dummyTarget_3() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___dummyTarget_3)); }
	inline Transform_t3600365921 * get_dummyTarget_3() const { return ___dummyTarget_3; }
	inline Transform_t3600365921 ** get_address_of_dummyTarget_3() { return &___dummyTarget_3; }
	inline void set_dummyTarget_3(Transform_t3600365921 * value)
	{
		___dummyTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_3), value);
	}

	inline static int32_t get_offset_of_CameraTarget_4() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraTarget_4)); }
	inline Transform_t3600365921 * get_CameraTarget_4() const { return ___CameraTarget_4; }
	inline Transform_t3600365921 ** get_address_of_CameraTarget_4() { return &___CameraTarget_4; }
	inline void set_CameraTarget_4(Transform_t3600365921 * value)
	{
		___CameraTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_4), value);
	}

	inline static int32_t get_offset_of_FollowDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___FollowDistance_5)); }
	inline float get_FollowDistance_5() const { return ___FollowDistance_5; }
	inline float* get_address_of_FollowDistance_5() { return &___FollowDistance_5; }
	inline void set_FollowDistance_5(float value)
	{
		___FollowDistance_5 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_6() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxFollowDistance_6)); }
	inline float get_MaxFollowDistance_6() const { return ___MaxFollowDistance_6; }
	inline float* get_address_of_MaxFollowDistance_6() { return &___MaxFollowDistance_6; }
	inline void set_MaxFollowDistance_6(float value)
	{
		___MaxFollowDistance_6 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinFollowDistance_7)); }
	inline float get_MinFollowDistance_7() const { return ___MinFollowDistance_7; }
	inline float* get_address_of_MinFollowDistance_7() { return &___MinFollowDistance_7; }
	inline void set_MinFollowDistance_7(float value)
	{
		___MinFollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_8() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___ElevationAngle_8)); }
	inline float get_ElevationAngle_8() const { return ___ElevationAngle_8; }
	inline float* get_address_of_ElevationAngle_8() { return &___ElevationAngle_8; }
	inline void set_ElevationAngle_8(float value)
	{
		___ElevationAngle_8 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_9() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxElevationAngle_9)); }
	inline float get_MaxElevationAngle_9() const { return ___MaxElevationAngle_9; }
	inline float* get_address_of_MaxElevationAngle_9() { return &___MaxElevationAngle_9; }
	inline void set_MaxElevationAngle_9(float value)
	{
		___MaxElevationAngle_9 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinElevationAngle_10)); }
	inline float get_MinElevationAngle_10() const { return ___MinElevationAngle_10; }
	inline float* get_address_of_MinElevationAngle_10() { return &___MinElevationAngle_10; }
	inline void set_MinElevationAngle_10(float value)
	{
		___MinElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___OrbitalAngle_11)); }
	inline float get_OrbitalAngle_11() const { return ___OrbitalAngle_11; }
	inline float* get_address_of_OrbitalAngle_11() { return &___OrbitalAngle_11; }
	inline void set_OrbitalAngle_11(float value)
	{
		___OrbitalAngle_11 = value;
	}

	inline static int32_t get_offset_of_CameraMode_12() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraMode_12)); }
	inline int32_t get_CameraMode_12() const { return ___CameraMode_12; }
	inline int32_t* get_address_of_CameraMode_12() { return &___CameraMode_12; }
	inline void set_CameraMode_12(int32_t value)
	{
		___CameraMode_12 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_13() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothing_13)); }
	inline bool get_MovementSmoothing_13() const { return ___MovementSmoothing_13; }
	inline bool* get_address_of_MovementSmoothing_13() { return &___MovementSmoothing_13; }
	inline void set_MovementSmoothing_13(bool value)
	{
		___MovementSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_14() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothing_14)); }
	inline bool get_RotationSmoothing_14() const { return ___RotationSmoothing_14; }
	inline bool* get_address_of_RotationSmoothing_14() { return &___RotationSmoothing_14; }
	inline void set_RotationSmoothing_14(bool value)
	{
		___RotationSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___previousSmoothing_15)); }
	inline bool get_previousSmoothing_15() const { return ___previousSmoothing_15; }
	inline bool* get_address_of_previousSmoothing_15() { return &___previousSmoothing_15; }
	inline void set_previousSmoothing_15(bool value)
	{
		___previousSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_16() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothingValue_16)); }
	inline float get_MovementSmoothingValue_16() const { return ___MovementSmoothingValue_16; }
	inline float* get_address_of_MovementSmoothingValue_16() { return &___MovementSmoothingValue_16; }
	inline void set_MovementSmoothingValue_16(float value)
	{
		___MovementSmoothingValue_16 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_17() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothingValue_17)); }
	inline float get_RotationSmoothingValue_17() const { return ___RotationSmoothingValue_17; }
	inline float* get_address_of_RotationSmoothingValue_17() { return &___RotationSmoothingValue_17; }
	inline void set_RotationSmoothingValue_17(float value)
	{
		___RotationSmoothingValue_17 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_18() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MoveSensitivity_18)); }
	inline float get_MoveSensitivity_18() const { return ___MoveSensitivity_18; }
	inline float* get_address_of_MoveSensitivity_18() { return &___MoveSensitivity_18; }
	inline void set_MoveSensitivity_18(float value)
	{
		___MoveSensitivity_18 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_19() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___currentVelocity_19)); }
	inline Vector3_t3722313464  get_currentVelocity_19() const { return ___currentVelocity_19; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_19() { return &___currentVelocity_19; }
	inline void set_currentVelocity_19(Vector3_t3722313464  value)
	{
		___currentVelocity_19 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_20() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___desiredPosition_20)); }
	inline Vector3_t3722313464  get_desiredPosition_20() const { return ___desiredPosition_20; }
	inline Vector3_t3722313464 * get_address_of_desiredPosition_20() { return &___desiredPosition_20; }
	inline void set_desiredPosition_20(Vector3_t3722313464  value)
	{
		___desiredPosition_20 = value;
	}

	inline static int32_t get_offset_of_mouseX_21() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseX_21)); }
	inline float get_mouseX_21() const { return ___mouseX_21; }
	inline float* get_address_of_mouseX_21() { return &___mouseX_21; }
	inline void set_mouseX_21(float value)
	{
		___mouseX_21 = value;
	}

	inline static int32_t get_offset_of_mouseY_22() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseY_22)); }
	inline float get_mouseY_22() const { return ___mouseY_22; }
	inline float* get_address_of_mouseY_22() { return &___mouseY_22; }
	inline void set_mouseY_22(float value)
	{
		___mouseY_22 = value;
	}

	inline static int32_t get_offset_of_moveVector_23() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___moveVector_23)); }
	inline Vector3_t3722313464  get_moveVector_23() const { return ___moveVector_23; }
	inline Vector3_t3722313464 * get_address_of_moveVector_23() { return &___moveVector_23; }
	inline void set_moveVector_23(Vector3_t3722313464  value)
	{
		___moveVector_23 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_24() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseWheel_24)); }
	inline float get_mouseWheel_24() const { return ___mouseWheel_24; }
	inline float* get_address_of_mouseWheel_24() { return &___mouseWheel_24; }
	inline void set_mouseWheel_24(float value)
	{
		___mouseWheel_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T2264742161_H
#ifndef OBJECTSPIN_T341713598_H
#define OBJECTSPIN_T341713598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t341713598  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_2;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_3;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3600365921 * ___m_transform_4;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_5;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t3722313464  ___m_prevPOS_6;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t3722313464  ___m_initial_Rotation_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t3722313464  ___m_initial_Position_8;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t2600501292  ___m_lightColor_9;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_10;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_11;

public:
	inline static int32_t get_offset_of_SpinSpeed_2() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___SpinSpeed_2)); }
	inline float get_SpinSpeed_2() const { return ___SpinSpeed_2; }
	inline float* get_address_of_SpinSpeed_2() { return &___SpinSpeed_2; }
	inline void set_SpinSpeed_2(float value)
	{
		___SpinSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RotationRange_3() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___RotationRange_3)); }
	inline int32_t get_RotationRange_3() const { return ___RotationRange_3; }
	inline int32_t* get_address_of_RotationRange_3() { return &___RotationRange_3; }
	inline void set_RotationRange_3(int32_t value)
	{
		___RotationRange_3 = value;
	}

	inline static int32_t get_offset_of_m_transform_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_transform_4)); }
	inline Transform_t3600365921 * get_m_transform_4() const { return ___m_transform_4; }
	inline Transform_t3600365921 ** get_address_of_m_transform_4() { return &___m_transform_4; }
	inline void set_m_transform_4(Transform_t3600365921 * value)
	{
		___m_transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_4), value);
	}

	inline static int32_t get_offset_of_m_time_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_time_5)); }
	inline float get_m_time_5() const { return ___m_time_5; }
	inline float* get_address_of_m_time_5() { return &___m_time_5; }
	inline void set_m_time_5(float value)
	{
		___m_time_5 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_prevPOS_6)); }
	inline Vector3_t3722313464  get_m_prevPOS_6() const { return ___m_prevPOS_6; }
	inline Vector3_t3722313464 * get_address_of_m_prevPOS_6() { return &___m_prevPOS_6; }
	inline void set_m_prevPOS_6(Vector3_t3722313464  value)
	{
		___m_prevPOS_6 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Rotation_7)); }
	inline Vector3_t3722313464  get_m_initial_Rotation_7() const { return ___m_initial_Rotation_7; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Rotation_7() { return &___m_initial_Rotation_7; }
	inline void set_m_initial_Rotation_7(Vector3_t3722313464  value)
	{
		___m_initial_Rotation_7 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Position_8)); }
	inline Vector3_t3722313464  get_m_initial_Position_8() const { return ___m_initial_Position_8; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Position_8() { return &___m_initial_Position_8; }
	inline void set_m_initial_Position_8(Vector3_t3722313464  value)
	{
		___m_initial_Position_8 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_lightColor_9)); }
	inline Color32_t2600501292  get_m_lightColor_9() const { return ___m_lightColor_9; }
	inline Color32_t2600501292 * get_address_of_m_lightColor_9() { return &___m_lightColor_9; }
	inline void set_m_lightColor_9(Color32_t2600501292  value)
	{
		___m_lightColor_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___frames_10)); }
	inline int32_t get_frames_10() const { return ___frames_10; }
	inline int32_t* get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(int32_t value)
	{
		___frames_10 = value;
	}

	inline static int32_t get_offset_of_Motion_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___Motion_11)); }
	inline int32_t get_Motion_11() const { return ___Motion_11; }
	inline int32_t* get_address_of_Motion_11() { return &___Motion_11; }
	inline void set_Motion_11(int32_t value)
	{
		___Motion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T341713598_H
#ifndef TELETYPE_T2409835159_H
#define TELETYPE_T2409835159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2409835159  : public MonoBehaviour_t3962482529
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_2;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_3;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_4;

public:
	inline static int32_t get_offset_of_label01_2() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label01_2)); }
	inline String_t* get_label01_2() const { return ___label01_2; }
	inline String_t** get_address_of_label01_2() { return &___label01_2; }
	inline void set_label01_2(String_t* value)
	{
		___label01_2 = value;
		Il2CppCodeGenWriteBarrier((&___label01_2), value);
	}

	inline static int32_t get_offset_of_label02_3() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label02_3)); }
	inline String_t* get_label02_3() const { return ___label02_3; }
	inline String_t** get_address_of_label02_3() { return &___label02_3; }
	inline void set_label02_3(String_t* value)
	{
		___label02_3 = value;
		Il2CppCodeGenWriteBarrier((&___label02_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___m_textMeshPro_4)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2409835159_H
#ifndef SKEWTEXTEXAMPLE_T3460249701_H
#define SKEWTEXTEXAMPLE_T3460249701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3460249701  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_3;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_4;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___VertexCurve_3)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___ShearAmount_5)); }
	inline float get_ShearAmount_5() const { return ___ShearAmount_5; }
	inline float* get_address_of_ShearAmount_5() { return &___ShearAmount_5; }
	inline void set_ShearAmount_5(float value)
	{
		___ShearAmount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3460249701_H
#ifndef SIMPLESCRIPT_T3279312205_H
#define SIMPLESCRIPT_T3279312205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t3279312205  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_2;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_4;

public:
	inline static int32_t get_offset_of_m_textMeshPro_2() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_textMeshPro_2)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_2() const { return ___m_textMeshPro_2; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_2() { return &___m_textMeshPro_2; }
	inline void set_m_textMeshPro_2(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_frame_4() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_frame_4)); }
	inline float get_m_frame_4() const { return ___m_frame_4; }
	inline float* get_address_of_m_frame_4() { return &___m_frame_4; }
	inline void set_m_frame_4(float value)
	{
		___m_frame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T3279312205_H
#ifndef SHADERPROPANIMATOR_T3617420994_H
#define SHADERPROPANIMATOR_T3617420994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t3617420994  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_2;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t340375123 * ___m_Material_3;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3046754366 * ___GlowCurve_4;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_5;

public:
	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Renderer_2)); }
	inline Renderer_t2627027031 * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(Renderer_t2627027031 * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Material_3)); }
	inline Material_t340375123 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t340375123 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t340375123 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}

	inline static int32_t get_offset_of_GlowCurve_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___GlowCurve_4)); }
	inline AnimationCurve_t3046754366 * get_GlowCurve_4() const { return ___GlowCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_GlowCurve_4() { return &___GlowCurve_4; }
	inline void set_GlowCurve_4(AnimationCurve_t3046754366 * value)
	{
		___GlowCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_4), value);
	}

	inline static int32_t get_offset_of_m_frame_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_frame_5)); }
	inline float get_m_frame_5() const { return ___m_frame_5; }
	inline float* get_address_of_m_frame_5() { return &___m_frame_5; }
	inline void set_m_frame_5(float value)
	{
		___m_frame_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T3617420994_H
#ifndef SCALEOVERRIDE_T259152881_H
#define SCALEOVERRIDE_T259152881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleOverride
struct  ScaleOverride_t259152881  : public TransformOverride_t3828400655
{
public:
	// Wikitude.WikitudeCamera ScaleOverride::wikiCamera
	WikitudeCamera_t2188881370 * ___wikiCamera_2;

public:
	inline static int32_t get_offset_of_wikiCamera_2() { return static_cast<int32_t>(offsetof(ScaleOverride_t259152881, ___wikiCamera_2)); }
	inline WikitudeCamera_t2188881370 * get_wikiCamera_2() const { return ___wikiCamera_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_wikiCamera_2() { return &___wikiCamera_2; }
	inline void set_wikiCamera_2(WikitudeCamera_t2188881370 * value)
	{
		___wikiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___wikiCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEOVERRIDE_T259152881_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (Transition2D_t2580652709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[6] = 
{
	Transition2D_t2580652709::get_offset_of_displayOnce_2(),
	Transition2D_t2580652709::get_offset_of_MiraCameraRig_3(),
	Transition2D_t2580652709::get_offset_of_instructionScreen_4(),
	Transition2D_t2580652709::get_offset_of_twoDCamera_5(),
	Transition2D_t2580652709::get_offset_of_distortionL_6(),
	Transition2D_t2580652709::get_offset_of_distortionR_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CStartU3Ec__Iterator0_t2404751466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	U3CStartU3Ec__Iterator0_t2404751466::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2404751466::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2404751466::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2404751466::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[4] = 
{
	U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752::get_offset_of_U24this_0(),
	U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752::get_offset_of_U24current_1(),
	U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752::get_offset_of_U24disposing_2(),
	U3CTransitionToLandscapeU3Ec__Iterator1_t3322580752::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U3CTransitionToPortraitU3Ec__Iterator2_t4216413023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[4] = 
{
	U3CTransitionToPortraitU3Ec__Iterator2_t4216413023::get_offset_of_U24this_0(),
	U3CTransitionToPortraitU3Ec__Iterator2_t4216413023::get_offset_of_U24current_1(),
	U3CTransitionToPortraitU3Ec__Iterator2_t4216413023::get_offset_of_U24disposing_2(),
	U3CTransitionToPortraitU3Ec__Iterator2_t4216413023::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (AxisSpin_t1710076314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[2] = 
{
	AxisSpin_t1710076314::get_offset_of_spinRate_2(),
	AxisSpin_t1710076314::get_offset_of_spinDirection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (DemoSceneManager_t570878136), -1, sizeof(DemoSceneManager_t570878136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2305[2] = 
{
	DemoSceneManager_t570878136_StaticFields::get_offset_of_isSpectator_2(),
	DemoSceneManager_t570878136_StaticFields::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (DynamicallyChangeIPD_t3573109106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (FaceCamera_t985191632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[1] = 
{
	FaceCamera_t985191632::get_offset_of_stereoCamRig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (FadeInScene_t3296889266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[7] = 
{
	FadeInScene_t3296889266::get_offset_of_shouldFadeOut_2(),
	FadeInScene_t3296889266::get_offset_of_fadeTexture_3(),
	FadeInScene_t3296889266::get_offset_of_fadeSpeed_4(),
	FadeInScene_t3296889266::get_offset_of_drawDepth_5(),
	FadeInScene_t3296889266::get_offset_of_alpha_6(),
	FadeInScene_t3296889266::get_offset_of_fadeDir_7(),
	FadeInScene_t3296889266::get_offset_of_shouldBlackOut_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (U3CStartLogOutU3Ec__Iterator0_t3361594076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[5] = 
{
	U3CStartLogOutU3Ec__Iterator0_t3361594076::get_offset_of_sceneToLoad_0(),
	U3CStartLogOutU3Ec__Iterator0_t3361594076::get_offset_of_U24this_1(),
	U3CStartLogOutU3Ec__Iterator0_t3361594076::get_offset_of_U24current_2(),
	U3CStartLogOutU3Ec__Iterator0_t3361594076::get_offset_of_U24disposing_3(),
	U3CStartLogOutU3Ec__Iterator0_t3361594076::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (Fishingline_t4150313569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[14] = 
{
	Fishingline_t4150313569::get_offset_of_currentSegments_2(),
	Fishingline_t4150313569::get_offset_of_referencePoints_3(),
	Fishingline_t4150313569::get_offset_of_defaultPositions_4(),
	Fishingline_t4150313569::get_offset_of_defaultFishlineLength_5(),
	Fishingline_t4150313569::get_offset_of_numSegments_6(),
	Fishingline_t4150313569::get_offset_of_destination_7(),
	Fishingline_t4150313569::get_offset_of_hitDistance_8(),
	Fishingline_t4150313569::get_offset_of_reticle_9(),
	Fishingline_t4150313569::get_offset_of_lerpSpeedMult_10(),
	Fishingline_t4150313569::get_offset_of_fishinglineParent_11(),
	Fishingline_t4150313569::get_offset_of_lineRend_12(),
	Fishingline_t4150313569::get_offset_of_colliders_13(),
	Fishingline_t4150313569::get_offset_of_parentedToEnd_14(),
	Fishingline_t4150313569::get_offset_of_attachPoint_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (FishinglineCast_t2818965060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[7] = 
{
	FishinglineCast_t2818965060::get_offset_of_minCast_2(),
	FishinglineCast_t2818965060::get_offset_of_maxCast_3(),
	FishinglineCast_t2818965060::get_offset_of_castSpeedMultiplier_4(),
	FishinglineCast_t2818965060::get_offset_of_castStart_5(),
	FishinglineCast_t2818965060::get_offset_of_lastCast_6(),
	FishinglineCast_t2818965060::get_offset_of_fishingline_7(),
	FishinglineCast_t2818965060::get_offset_of_currentScaleMult_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (MarkerRotationWatcher_t711531681), -1, sizeof(MarkerRotationWatcher_t711531681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[9] = 
{
	MarkerRotationWatcher_t711531681::get_offset_of_mainCamera_2(),
	MarkerRotationWatcher_t711531681::get_offset_of_deltaDotProduct_3(),
	MarkerRotationWatcher_t711531681::get_offset_of_necessaryRotation_4(),
	MarkerRotationWatcher_t711531681::get_offset_of_dot_5(),
	MarkerRotationWatcher_t711531681_StaticFields::get_offset_of_OnRotated_6(),
	MarkerRotationWatcher_t711531681::get_offset_of_startVector_7(),
	MarkerRotationWatcher_t711531681::get_offset_of_firstAngle_8(),
	MarkerRotationWatcher_t711531681::get_offset_of_counter_9(),
	MarkerRotationWatcher_t711531681::get_offset_of_hasRotated_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (RotateAction_t1819018245), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (MiraExampleInteractionScript_t2843749276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[6] = 
{
	MiraExampleInteractionScript_t2843749276::get_offset_of_textBeneathPlanet_2(),
	MiraExampleInteractionScript_t2843749276::get_offset_of_timeSinceLastInteraction_3(),
	MiraExampleInteractionScript_t2843749276::get_offset_of_startingLocation_4(),
	MiraExampleInteractionScript_t2843749276::get_offset_of_isPlanetFeelingSatisfied_5(),
	MiraExampleInteractionScript_t2843749276::get_offset_of_isUserPointingAtPlanet_6(),
	MiraExampleInteractionScript_t2843749276::get_offset_of_spin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (U3CtemperTantrumU3Ec__Iterator0_t1951222535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[7] = 
{
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U3CiU3E__1_0(),
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U3CdistanceU3E__2_1(),
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U3CiU3E__3_2(),
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U24this_3(),
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U24current_4(),
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U24disposing_5(),
	U3CtemperTantrumU3Ec__Iterator0_t1951222535::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (MiraGrabExample_t2273097562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[3] = 
{
	MiraGrabExample_t2273097562::get_offset_of_textBeneathPlanet_2(),
	MiraGrabExample_t2273097562::get_offset_of_isGrabbing_3(),
	MiraGrabExample_t2273097562::get_offset_of_lastTouchPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (MiraPhysicsGrabExample_t3867022233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	MiraPhysicsGrabExample_t3867022233::get_offset_of_isGrabbing_2(),
	MiraPhysicsGrabExample_t3867022233::get_offset_of_originalConstraints_3(),
	MiraPhysicsGrabExample_t3867022233::get_offset_of_rigidBody_4(),
	MiraPhysicsGrabExample_t3867022233::get_offset_of_lastTouchPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (MiraPhysicsPlaypenBoundary_t2335368901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (ToggleModes_t4014462119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (TrackerLerpDriver_t2407623450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	TrackerLerpDriver_t2407623450::get_offset_of_rotationWatcher_2(),
	TrackerLerpDriver_t2407623450::get_offset_of_startPosition_3(),
	TrackerLerpDriver_t2407623450::get_offset_of_endPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (Platform_t1043456379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (ExposureController_t1901463639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[1] = 
{
	ExposureController_t1901463639::get_offset_of_WikiCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ImageTrackingController_t390588479), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ScaleOverride_t259152881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[1] = 
{
	ScaleOverride_t259152881::get_offset_of_wikiCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_2(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_3(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_4(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_10(),
	Benchmark01_t1571072624::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_2(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_3(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_10(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_2(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_3(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_2(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_3(),
	Benchmark03_t1571203696::get_offset_of_TheFont_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (Benchmark04_t1570876016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[5] = 
{
	Benchmark04_t1570876016::get_offset_of_SpawnType_2(),
	Benchmark04_t1570876016::get_offset_of_MinPointSize_3(),
	Benchmark04_t1570876016::get_offset_of_MaxPointSize_4(),
	Benchmark04_t1570876016::get_offset_of_Steps_5(),
	Benchmark04_t1570876016::get_offset_of_m_Transform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (CameraController_t2264742161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[25] = 
{
	CameraController_t2264742161::get_offset_of_cameraTransform_2(),
	CameraController_t2264742161::get_offset_of_dummyTarget_3(),
	CameraController_t2264742161::get_offset_of_CameraTarget_4(),
	CameraController_t2264742161::get_offset_of_FollowDistance_5(),
	CameraController_t2264742161::get_offset_of_MaxFollowDistance_6(),
	CameraController_t2264742161::get_offset_of_MinFollowDistance_7(),
	CameraController_t2264742161::get_offset_of_ElevationAngle_8(),
	CameraController_t2264742161::get_offset_of_MaxElevationAngle_9(),
	CameraController_t2264742161::get_offset_of_MinElevationAngle_10(),
	CameraController_t2264742161::get_offset_of_OrbitalAngle_11(),
	CameraController_t2264742161::get_offset_of_CameraMode_12(),
	CameraController_t2264742161::get_offset_of_MovementSmoothing_13(),
	CameraController_t2264742161::get_offset_of_RotationSmoothing_14(),
	CameraController_t2264742161::get_offset_of_previousSmoothing_15(),
	CameraController_t2264742161::get_offset_of_MovementSmoothingValue_16(),
	CameraController_t2264742161::get_offset_of_RotationSmoothingValue_17(),
	CameraController_t2264742161::get_offset_of_MoveSensitivity_18(),
	CameraController_t2264742161::get_offset_of_currentVelocity_19(),
	CameraController_t2264742161::get_offset_of_desiredPosition_20(),
	CameraController_t2264742161::get_offset_of_mouseX_21(),
	CameraController_t2264742161::get_offset_of_mouseY_22(),
	CameraController_t2264742161::get_offset_of_moveVector_23(),
	CameraController_t2264742161::get_offset_of_mouseWheel_24(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (CameraModes_t3200559075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2333[4] = 
{
	CameraModes_t3200559075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (ChatController_t3486202795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	ChatController_t3486202795::get_offset_of_TMP_ChatInput_2(),
	ChatController_t3486202795::get_offset_of_TMP_ChatOutput_3(),
	ChatController_t3486202795::get_offset_of_ChatScrollbar_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (EnvMapAnimator_t1140999784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[3] = 
{
	EnvMapAnimator_t1140999784::get_offset_of_RotationSpeeds_2(),
	EnvMapAnimator_t1140999784::get_offset_of_m_textMeshPro_3(),
	EnvMapAnimator_t1140999784::get_offset_of_m_material_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (U3CStartU3Ec__Iterator0_t1520811813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[5] = 
{
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (ObjectSpin_t341713598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[10] = 
{
	ObjectSpin_t341713598::get_offset_of_SpinSpeed_2(),
	ObjectSpin_t341713598::get_offset_of_RotationRange_3(),
	ObjectSpin_t341713598::get_offset_of_m_transform_4(),
	ObjectSpin_t341713598::get_offset_of_m_time_5(),
	ObjectSpin_t341713598::get_offset_of_m_prevPOS_6(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Rotation_7(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Position_8(),
	ObjectSpin_t341713598::get_offset_of_m_lightColor_9(),
	ObjectSpin_t341713598::get_offset_of_frames_10(),
	ObjectSpin_t341713598::get_offset_of_Motion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (MotionType_t1905163921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2338[4] = 
{
	MotionType_t1905163921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (ShaderPropAnimator_t3617420994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[4] = 
{
	ShaderPropAnimator_t3617420994::get_offset_of_m_Renderer_2(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_Material_3(),
	ShaderPropAnimator_t3617420994::get_offset_of_GlowCurve_4(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_frame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t4041402054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (SimpleScript_t3279312205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[3] = 
{
	SimpleScript_t3279312205::get_offset_of_m_textMeshPro_2(),
	0,
	SimpleScript_t3279312205::get_offset_of_m_frame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (SkewTextExample_t3460249701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[4] = 
{
	SkewTextExample_t3460249701::get_offset_of_m_TextComponent_2(),
	SkewTextExample_t3460249701::get_offset_of_VertexCurve_3(),
	SkewTextExample_t3460249701::get_offset_of_CurveScale_4(),
	SkewTextExample_t3460249701::get_offset_of_ShearAmount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (U3CWarpTextU3Ec__Iterator0_t116130919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (TeleType_t2409835159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[3] = 
{
	TeleType_t2409835159::get_offset_of_label01_2(),
	TeleType_t2409835159::get_offset_of_label02_3(),
	TeleType_t2409835159::get_offset_of_m_textMeshPro_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (U3CStartU3Ec__Iterator0_t3341539328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[7] = 
{
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (TextConsoleSimulator_t3766250034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	TextConsoleSimulator_t3766250034::get_offset_of_m_TextComponent_2(),
	TextConsoleSimulator_t3766250034::get_offset_of_hasTextChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t860191687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t1343183262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (TextMeshProFloatingText_t845872552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[10] = 
{
	TextMeshProFloatingText_t845872552::get_offset_of_TheFont_2(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_3(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMeshPro_4(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMesh_5(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_transform_6(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_Transform_7(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_cameraTransform_8(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastPOS_9(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastRotation_10(),
	TextMeshProFloatingText_t845872552::get_offset_of_SpawnType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[11] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CfadeDurationU3E__0_6(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24this_7(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24current_8(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24disposing_9(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[12] = 
{
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24this_8(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24current_9(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (TextMeshSpawner_t177691618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[4] = 
{
	TextMeshSpawner_t177691618::get_offset_of_SpawnType_2(),
	TextMeshSpawner_t177691618::get_offset_of_NumberOfNPC_3(),
	TextMeshSpawner_t177691618::get_offset_of_TheFont_4(),
	TextMeshSpawner_t177691618::get_offset_of_floatingText_Script_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (TMP_DigitValidator_t573672104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (TMP_ExampleScript_01_t3051742005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	TMP_ExampleScript_01_t3051742005::get_offset_of_ObjectType_2(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_isStatic_3(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_m_text_4(),
	0,
	TMP_ExampleScript_01_t3051742005::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (objectType_t4082700821)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2355[3] = 
{
	objectType_t4082700821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (TMP_FrameRateCounter_t314972976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[10] = 
{
	TMP_FrameRateCounter_t314972976::get_offset_of_UpdateInterval_2(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_LastInterval_3(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_Frames_4(),
	TMP_FrameRateCounter_t314972976::get_offset_of_AnchorPosition_5(),
	TMP_FrameRateCounter_t314972976::get_offset_of_htmlColorTag_6(),
	0,
	TMP_FrameRateCounter_t314972976::get_offset_of_m_TextMeshPro_8(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_frameCounter_transform_9(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_camera_10(),
	TMP_FrameRateCounter_t314972976::get_offset_of_last_AnchorPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (FpsCounterAnchorPositions_t1585798158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2357[5] = 
{
	FpsCounterAnchorPositions_t1585798158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (TMP_PhoneNumberValidator_t743649728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (TMP_TextEventCheck_t1103849140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[1] = 
{
	TMP_TextEventCheck_t1103849140::get_offset_of_TextEventHandler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (TMP_TextEventHandler_t1869054637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[11] = 
{
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnCharacterSelection_2(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnWordSelection_3(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLineSelection_4(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLinkSelection_5(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_TextComponent_6(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Camera_7(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Canvas_8(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_selectedLink_9(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastCharIndex_10(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastWordIndex_11(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastLineIndex_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (CharacterSelectionEvent_t3109943174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (WordSelectionEvent_t1841909953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (LineSelectionEvent_t2868010532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (LinkSelectionEvent_t1590929858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (TMP_TextInfoDebugTool_t1868681444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[9] = 
{
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowCharacters_2(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowWords_3(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLinks_4(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLines_5(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowMeshBounds_6(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowTextBounds_7(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ObjectStats_8(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_TextComponent_9(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_Transform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (TMP_TextSelector_A_t3982526506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[6] = 
{
	TMP_TextSelector_A_t3982526506::get_offset_of_m_TextMeshPro_2(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_Camera_3(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_isHoveringObject_4(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_selectedLink_5(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastCharIndex_6(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastWordIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (TMP_TextSelector_B_t3982526505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[14] = 
{
	TMP_TextSelector_B_t3982526505::get_offset_of_TextPopup_Prefab_01_2(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_RectTransform_3(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_TMPComponent_4(),
	0,
	0,
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextMeshPro_7(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Canvas_8(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Camera_9(),
	TMP_TextSelector_B_t3982526505::get_offset_of_isHoveringObject_10(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedWord_11(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedLink_12(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_lastIndex_13(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_matrix_14(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_cachedMeshInfoVertexData_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (TMP_UiFrameRateCounter_t811747397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[9] = 
{
	TMP_UiFrameRateCounter_t811747397::get_offset_of_UpdateInterval_2(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_LastInterval_3(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_Frames_4(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_AnchorPosition_5(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_htmlColorTag_6(),
	0,
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_TextMeshPro_8(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_frameCounter_transform_9(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_last_AnchorPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (FpsCounterAnchorPositions_t2550331785)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[5] = 
{
	FpsCounterAnchorPositions_t2550331785::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (TMPro_InstructionOverlay_t4246705477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[6] = 
{
	TMPro_InstructionOverlay_t4246705477::get_offset_of_AnchorPosition_2(),
	0,
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_TextMeshPro_4(),
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_textContainer_5(),
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_frameCounter_transform_6(),
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_camera_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (FpsCounterAnchorPositions_t2334657565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[5] = 
{
	FpsCounterAnchorPositions_t2334657565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (VertexColorCycler_t3003193665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[1] = 
{
	VertexColorCycler_t3003193665::get_offset_of_m_TextComponent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t897284962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[11] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CcurrentCharacterU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3Cc0U3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CmaterialIndexU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CnewVertexColorsU3E__1_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CvertexIndexU3E__1_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24this_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24current_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24disposing_9(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (VertexJitter_t4087429332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[5] = 
{
	VertexJitter_t4087429332::get_offset_of_AngleMultiplier_2(),
	VertexJitter_t4087429332::get_offset_of_SpeedMultiplier_3(),
	VertexJitter_t4087429332::get_offset_of_CurveScale_4(),
	VertexJitter_t4087429332::get_offset_of_m_TextComponent_5(),
	VertexJitter_t4087429332::get_offset_of_hasTextChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (VertexAnim_t2231884842)+ sizeof (RuntimeObject), sizeof(VertexAnim_t2231884842 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2375[3] = 
{
	VertexAnim_t2231884842::get_offset_of_angleRange_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t2231884842::get_offset_of_angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t2231884842::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t225534713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CloopCountU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CvertexAnimU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CcachedMeshInfoU3E__0_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CmatrixU3E__2_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24this_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24current_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24disposing_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (VertexShakeA_t4262048139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[6] = 
{
	VertexShakeA_t4262048139::get_offset_of_AngleMultiplier_2(),
	VertexShakeA_t4262048139::get_offset_of_SpeedMultiplier_3(),
	VertexShakeA_t4262048139::get_offset_of_ScaleMultiplier_4(),
	VertexShakeA_t4262048139::get_offset_of_RotationMultiplier_5(),
	VertexShakeA_t4262048139::get_offset_of_m_TextComponent_6(),
	VertexShakeA_t4262048139::get_offset_of_hasTextChanged_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t956521787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (VertexShakeB_t1533164784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[5] = 
{
	VertexShakeB_t1533164784::get_offset_of_AngleMultiplier_2(),
	VertexShakeB_t1533164784::get_offset_of_SpeedMultiplier_3(),
	VertexShakeB_t1533164784::get_offset_of_CurveScale_4(),
	VertexShakeB_t1533164784::get_offset_of_m_TextComponent_5(),
	VertexShakeB_t1533164784::get_offset_of_hasTextChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t168300594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (VertexZoom_t550798657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[5] = 
{
	VertexZoom_t550798657::get_offset_of_AngleMultiplier_2(),
	VertexZoom_t550798657::get_offset_of_SpeedMultiplier_3(),
	VertexZoom_t550798657::get_offset_of_CurveScale_4(),
	VertexZoom_t550798657::get_offset_of_m_TextComponent_5(),
	VertexZoom_t550798657::get_offset_of_hasTextChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CscaleSortingOrderU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24PC_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24locvar0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[2] = 
{
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_modifiedCharScale_0(),
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (WarpTextExample_t3821118074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[5] = 
{
	WarpTextExample_t3821118074::get_offset_of_m_TextComponent_2(),
	WarpTextExample_t3821118074::get_offset_of_VertexCurve_3(),
	WarpTextExample_t3821118074::get_offset_of_AngleMultiplier_4(),
	WarpTextExample_t3821118074::get_offset_of_SpeedMultiplier_5(),
	WarpTextExample_t3821118074::get_offset_of_CurveScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (U3CWarpTextU3Ec__Iterator0_t4025661343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[12] = 
{
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_curveU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CtextInfoU3E__1_2(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMinXU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMaxXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CverticesU3E__2_6(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CmatrixU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24this_8(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24current_9(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24disposing_10(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24PC_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
