﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Material
struct Material_t340375123;
// FlatLighting.MaterialBlender
struct MaterialBlender_t4195463282;
// System.String
struct String_t;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// ConnectToPreviousRemote
struct ConnectToPreviousRemote_t2022260273;
// System.Collections.Generic.List`1<SimpleInput/AxisInput>
struct List_1_t2274532392;
// System.Action`1<MiraRemoteException>
struct Action_1_t3895617434;
// System.Collections.Generic.Dictionary`2<System.String,System.Action>
struct Dictionary_2_t1049633776;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Boolean>>
struct Dictionary_2_t55011859;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Int32>>
struct Dictionary_2_t2908669647;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Single>>
struct Dictionary_2_t1354990668;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<MiraRemoteException>>
struct Dictionary_2_t3680873733;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<Remote>>
struct Dictionary_2_t976236017;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.Single,System.Single,System.Single>>
struct Dictionary_2_t2710841014;
// NativeBridge/MRIntCallback
struct MRIntCallback_t3089559991;
// NativeBridge/MRBoolCallback
struct MRBoolCallback_t3968806430;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// NativeBridge/MRDiscoveredRemoteCallback
struct MRDiscoveredRemoteCallback_t179447917;
// NativeBridge/MRErrorCallback
struct MRErrorCallback_t1000734509;
// NativeBridge/MRConnectedRemoteCallback
struct MRConnectedRemoteCallback_t1852255652;
// NativeBridge/MRFloatCallback
struct MRFloatCallback_t806206182;
// NativeBridge/MREmptyCallback
struct MREmptyCallback_t625280627;
// NativeBridge/MRRemoteMotionCallback
struct MRRemoteMotionCallback_t366018504;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// Wikitude.IPlatformBridge
struct IPlatformBridge_t1124789254;
// Wikitude.SDKBuildInformation
struct SDKBuildInformation_t1497247620;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// Wikitude.TargetCollectionResource/OnFinishLoadingEvent
struct OnFinishLoadingEvent_t3173959162;
// Wikitude.TargetCollectionResource/OnErrorLoadingEvent
struct OnErrorLoadingEvent_t3790443523;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Char[]
struct CharU5BU5D_t3528271667;
// GameManager
struct GameManager_t1536523654;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>
struct List_1_t45323880;
// System.Collections.Generic.List`1<SimpleInput/KeyInput>
struct List_1_t570456766;
// System.Collections.Generic.List`1<SimpleInput/ButtonInput>
struct List_1_t4291026645;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// SimpleInput/AxisInput
struct AxisInput_t802457650;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Image
struct Image_t2670269651;
// FlatLighting.LightSource`1/LightBag<FlatLighting.DirectionalLight>
struct LightBag_t1663991692;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// FlatLighting.LightSource`1/LightBag<FlatLighting.ShadowProjector>
struct LightBag_t2413274991;
// FlatLighting.LightSource`1/LightBag<FlatLighting.PointLight>
struct LightBag_t186156872;
// FlatLighting.LightSource`1/LightBag<FlatLighting.SpotLight>
struct LightBag_t1736700798;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.String[]
struct StringU5BU5D_t1281789340;
// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent
struct OnEnterFieldOfVisionEvent_t2975486640;
// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent
struct OnExitFieldOfVisionEvent_t2776302756;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// AudioManager
struct AudioManager_t3267510698;
// UnityEngine.Transform
struct Transform_t3600365921;
// PlanetScript
struct PlanetScript_t2937373613;
// SceneLigtingSetup
struct SceneLigtingSetup_t3397062989;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// SimpleInput/ButtonInput
struct ButtonInput_t2818951903;
// SimpleInput/KeyInput
struct KeyInput_t3393349320;
// SimpleInputNamespace.ISimpleInputDraggable
struct ISimpleInputDraggable_t3592242264;
// SimpleInputNamespace.Touchpad/MouseButton[]
struct MouseButtonU5BU5D_t3311405085;
// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>
struct Dictionary_2_t1968145125;
// System.Collections.Generic.List`1<SimpleInput/Axis>
struct List_1_t3654963568;
// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>
struct Dictionary_2_t2716226479;
// System.Collections.Generic.List`1<SimpleInput/Button>
struct List_1_t108077626;
// System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>
struct Dictionary_2_t4181968371;
// System.Collections.Generic.List`1<SimpleInput/MouseButton>
struct List_1_t2470362486;
// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>
struct Dictionary_2_t316807927;
// System.Collections.Generic.List`1<SimpleInput/Key>
struct List_1_t1062181654;
// SimpleInput/UpdateCallback
struct UpdateCallback_t3991193291;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2165061829;
// SimpleInput/MouseButtonInput
struct MouseButtonInput_t2868216434;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t2302988098;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t857997111;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef BASEINPUT_2_T3479630992_H
#define BASEINPUT_2_T3479630992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>
struct  BaseInput_2_t3479630992  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	int32_t ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	bool ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t3479630992, ___m_key_0)); }
	inline int32_t get_m_key_0() const { return ___m_key_0; }
	inline int32_t* get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(int32_t value)
	{
		___m_key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t3479630992, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t3479630992, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T3479630992_H
#ifndef U3CUPDATEBLENDINGU3EC__ITERATOR0_T3841162924_H
#define U3CUPDATEBLENDINGU3EC__ITERATOR0_T3841162924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0
struct  U3CUpdateBlendingU3Ec__Iterator0_t3841162924  : public RuntimeObject
{
public:
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<currentMaterialIndex>__0
	int32_t ___U3CcurrentMaterialIndexU3E__0_0;
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<nextMaterialIndex>__0
	int32_t ___U3CnextMaterialIndexU3E__0_1;
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_2;
	// UnityEngine.Material FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<currentMaterial>__2
	Material_t340375123 * ___U3CcurrentMaterialU3E__2_3;
	// UnityEngine.Material FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<nextMaterial>__2
	Material_t340375123 * ___U3CnextMaterialU3E__2_4;
	// System.Single FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<t>__3
	float ___U3CtU3E__3_5;
	// FlatLighting.MaterialBlender FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$this
	MaterialBlender_t4195463282 * ___U24this_6;
	// System.Object FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CcurrentMaterialIndexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U3CcurrentMaterialIndexU3E__0_0)); }
	inline int32_t get_U3CcurrentMaterialIndexU3E__0_0() const { return ___U3CcurrentMaterialIndexU3E__0_0; }
	inline int32_t* get_address_of_U3CcurrentMaterialIndexU3E__0_0() { return &___U3CcurrentMaterialIndexU3E__0_0; }
	inline void set_U3CcurrentMaterialIndexU3E__0_0(int32_t value)
	{
		___U3CcurrentMaterialIndexU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnextMaterialIndexU3E__0_1() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U3CnextMaterialIndexU3E__0_1)); }
	inline int32_t get_U3CnextMaterialIndexU3E__0_1() const { return ___U3CnextMaterialIndexU3E__0_1; }
	inline int32_t* get_address_of_U3CnextMaterialIndexU3E__0_1() { return &___U3CnextMaterialIndexU3E__0_1; }
	inline void set_U3CnextMaterialIndexU3E__0_1(int32_t value)
	{
		___U3CnextMaterialIndexU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_2() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U3CiU3E__1_2)); }
	inline int32_t get_U3CiU3E__1_2() const { return ___U3CiU3E__1_2; }
	inline int32_t* get_address_of_U3CiU3E__1_2() { return &___U3CiU3E__1_2; }
	inline void set_U3CiU3E__1_2(int32_t value)
	{
		___U3CiU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentMaterialU3E__2_3() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U3CcurrentMaterialU3E__2_3)); }
	inline Material_t340375123 * get_U3CcurrentMaterialU3E__2_3() const { return ___U3CcurrentMaterialU3E__2_3; }
	inline Material_t340375123 ** get_address_of_U3CcurrentMaterialU3E__2_3() { return &___U3CcurrentMaterialU3E__2_3; }
	inline void set_U3CcurrentMaterialU3E__2_3(Material_t340375123 * value)
	{
		___U3CcurrentMaterialU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentMaterialU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U3CnextMaterialU3E__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U3CnextMaterialU3E__2_4)); }
	inline Material_t340375123 * get_U3CnextMaterialU3E__2_4() const { return ___U3CnextMaterialU3E__2_4; }
	inline Material_t340375123 ** get_address_of_U3CnextMaterialU3E__2_4() { return &___U3CnextMaterialU3E__2_4; }
	inline void set_U3CnextMaterialU3E__2_4(Material_t340375123 * value)
	{
		___U3CnextMaterialU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnextMaterialU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3CtU3E__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U3CtU3E__3_5)); }
	inline float get_U3CtU3E__3_5() const { return ___U3CtU3E__3_5; }
	inline float* get_address_of_U3CtU3E__3_5() { return &___U3CtU3E__3_5; }
	inline void set_U3CtU3E__3_5(float value)
	{
		___U3CtU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U24this_6)); }
	inline MaterialBlender_t4195463282 * get_U24this_6() const { return ___U24this_6; }
	inline MaterialBlender_t4195463282 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(MaterialBlender_t4195463282 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3841162924, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBLENDINGU3EC__ITERATOR0_T3841162924_H
#ifndef BASEINPUT_2_T1381185473_H
#define BASEINPUT_2_T1381185473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<System.String,System.Single>
struct  BaseInput_2_t1381185473  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	String_t* ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	float ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t1381185473, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t1381185473, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t1381185473, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T1381185473_H
#ifndef POINTCLOUDRENDERER_T3664016937_H
#define POINTCLOUDRENDERER_T3664016937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WikitudeEditor.PointCloudRenderer
struct  PointCloudRenderer_t3664016937  : public RuntimeObject
{
public:
	// UnityEngine.Mesh WikitudeEditor.PointCloudRenderer::_testMesh
	Mesh_t3648964284 * ____testMesh_0;
	// UnityEngine.Camera WikitudeEditor.PointCloudRenderer::_sceneCamera
	Camera_t4157153871 * ____sceneCamera_1;
	// UnityEngine.Material WikitudeEditor.PointCloudRenderer::_renderMaterial
	Material_t340375123 * ____renderMaterial_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> WikitudeEditor.PointCloudRenderer::_cubePoints
	List_1_t899420910 * ____cubePoints_3;
	// System.Collections.Generic.List`1<System.Int32> WikitudeEditor.PointCloudRenderer::_cubeIndices
	List_1_t128053199 * ____cubeIndices_4;
	// System.Int32 WikitudeEditor.PointCloudRenderer::_meshVertexCount
	int32_t ____meshVertexCount_5;
	// System.Boolean WikitudeEditor.PointCloudRenderer::_drawWithCommandBuffer
	bool ____drawWithCommandBuffer_6;
	// UnityEngine.Vector3[] WikitudeEditor.PointCloudRenderer::_vertices
	Vector3U5BU5D_t1718750761* ____vertices_7;
	// System.Int32[] WikitudeEditor.PointCloudRenderer::_indices
	Int32U5BU5D_t385246372* ____indices_8;
	// UnityEngine.Color[] WikitudeEditor.PointCloudRenderer::_vertexColors
	ColorU5BU5D_t941916413* ____vertexColors_9;

public:
	inline static int32_t get_offset_of__testMesh_0() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____testMesh_0)); }
	inline Mesh_t3648964284 * get__testMesh_0() const { return ____testMesh_0; }
	inline Mesh_t3648964284 ** get_address_of__testMesh_0() { return &____testMesh_0; }
	inline void set__testMesh_0(Mesh_t3648964284 * value)
	{
		____testMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&____testMesh_0), value);
	}

	inline static int32_t get_offset_of__sceneCamera_1() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____sceneCamera_1)); }
	inline Camera_t4157153871 * get__sceneCamera_1() const { return ____sceneCamera_1; }
	inline Camera_t4157153871 ** get_address_of__sceneCamera_1() { return &____sceneCamera_1; }
	inline void set__sceneCamera_1(Camera_t4157153871 * value)
	{
		____sceneCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneCamera_1), value);
	}

	inline static int32_t get_offset_of__renderMaterial_2() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____renderMaterial_2)); }
	inline Material_t340375123 * get__renderMaterial_2() const { return ____renderMaterial_2; }
	inline Material_t340375123 ** get_address_of__renderMaterial_2() { return &____renderMaterial_2; }
	inline void set__renderMaterial_2(Material_t340375123 * value)
	{
		____renderMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&____renderMaterial_2), value);
	}

	inline static int32_t get_offset_of__cubePoints_3() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____cubePoints_3)); }
	inline List_1_t899420910 * get__cubePoints_3() const { return ____cubePoints_3; }
	inline List_1_t899420910 ** get_address_of__cubePoints_3() { return &____cubePoints_3; }
	inline void set__cubePoints_3(List_1_t899420910 * value)
	{
		____cubePoints_3 = value;
		Il2CppCodeGenWriteBarrier((&____cubePoints_3), value);
	}

	inline static int32_t get_offset_of__cubeIndices_4() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____cubeIndices_4)); }
	inline List_1_t128053199 * get__cubeIndices_4() const { return ____cubeIndices_4; }
	inline List_1_t128053199 ** get_address_of__cubeIndices_4() { return &____cubeIndices_4; }
	inline void set__cubeIndices_4(List_1_t128053199 * value)
	{
		____cubeIndices_4 = value;
		Il2CppCodeGenWriteBarrier((&____cubeIndices_4), value);
	}

	inline static int32_t get_offset_of__meshVertexCount_5() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____meshVertexCount_5)); }
	inline int32_t get__meshVertexCount_5() const { return ____meshVertexCount_5; }
	inline int32_t* get_address_of__meshVertexCount_5() { return &____meshVertexCount_5; }
	inline void set__meshVertexCount_5(int32_t value)
	{
		____meshVertexCount_5 = value;
	}

	inline static int32_t get_offset_of__drawWithCommandBuffer_6() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____drawWithCommandBuffer_6)); }
	inline bool get__drawWithCommandBuffer_6() const { return ____drawWithCommandBuffer_6; }
	inline bool* get_address_of__drawWithCommandBuffer_6() { return &____drawWithCommandBuffer_6; }
	inline void set__drawWithCommandBuffer_6(bool value)
	{
		____drawWithCommandBuffer_6 = value;
	}

	inline static int32_t get_offset_of__vertices_7() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____vertices_7)); }
	inline Vector3U5BU5D_t1718750761* get__vertices_7() const { return ____vertices_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of__vertices_7() { return &____vertices_7; }
	inline void set__vertices_7(Vector3U5BU5D_t1718750761* value)
	{
		____vertices_7 = value;
		Il2CppCodeGenWriteBarrier((&____vertices_7), value);
	}

	inline static int32_t get_offset_of__indices_8() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____indices_8)); }
	inline Int32U5BU5D_t385246372* get__indices_8() const { return ____indices_8; }
	inline Int32U5BU5D_t385246372** get_address_of__indices_8() { return &____indices_8; }
	inline void set__indices_8(Int32U5BU5D_t385246372* value)
	{
		____indices_8 = value;
		Il2CppCodeGenWriteBarrier((&____indices_8), value);
	}

	inline static int32_t get_offset_of__vertexColors_9() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____vertexColors_9)); }
	inline ColorU5BU5D_t941916413* get__vertexColors_9() const { return ____vertexColors_9; }
	inline ColorU5BU5D_t941916413** get_address_of__vertexColors_9() { return &____vertexColors_9; }
	inline void set__vertexColors_9(ColorU5BU5D_t941916413* value)
	{
		____vertexColors_9 = value;
		Il2CppCodeGenWriteBarrier((&____vertexColors_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDRENDERER_T3664016937_H
#ifndef MAPPOINTCLOUD_T955983077_H
#define MAPPOINTCLOUD_T955983077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.MapPointCloud
struct  MapPointCloud_t955983077  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Wikitude.MapPointCloud::FeaturePoints
	Vector3U5BU5D_t1718750761* ___FeaturePoints_0;
	// UnityEngine.Color[] Wikitude.MapPointCloud::Colors
	ColorU5BU5D_t941916413* ___Colors_1;
	// System.Single Wikitude.MapPointCloud::Scale
	float ___Scale_2;

public:
	inline static int32_t get_offset_of_FeaturePoints_0() { return static_cast<int32_t>(offsetof(MapPointCloud_t955983077, ___FeaturePoints_0)); }
	inline Vector3U5BU5D_t1718750761* get_FeaturePoints_0() const { return ___FeaturePoints_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_FeaturePoints_0() { return &___FeaturePoints_0; }
	inline void set_FeaturePoints_0(Vector3U5BU5D_t1718750761* value)
	{
		___FeaturePoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___FeaturePoints_0), value);
	}

	inline static int32_t get_offset_of_Colors_1() { return static_cast<int32_t>(offsetof(MapPointCloud_t955983077, ___Colors_1)); }
	inline ColorU5BU5D_t941916413* get_Colors_1() const { return ___Colors_1; }
	inline ColorU5BU5D_t941916413** get_address_of_Colors_1() { return &___Colors_1; }
	inline void set_Colors_1(ColorU5BU5D_t941916413* value)
	{
		___Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_1), value);
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(MapPointCloud_t955983077, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPOINTCLOUD_T955983077_H
#ifndef PLATFORMBASE_T2513924928_H
#define PLATFORMBASE_T2513924928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PlatformBase
struct  PlatformBase_t2513924928  : public RuntimeObject
{
public:

public:
};

struct PlatformBase_t2513924928_StaticFields
{
public:
	// Wikitude.PlatformBase Wikitude.PlatformBase::_instance
	PlatformBase_t2513924928 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PlatformBase_t2513924928_StaticFields, ____instance_0)); }
	inline PlatformBase_t2513924928 * get__instance_0() const { return ____instance_0; }
	inline PlatformBase_t2513924928 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PlatformBase_t2513924928 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMBASE_T2513924928_H
#ifndef U3CAUTOCONNECTLASTREMOTEU3EC__ITERATOR0_T1411973361_H
#define U3CAUTOCONNECTLASTREMOTEU3EC__ITERATOR0_T1411973361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0
struct  U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361  : public RuntimeObject
{
public:
	// ConnectToPreviousRemote ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$this
	ConnectToPreviousRemote_t2022260273 * ___U24this_0;
	// System.Object ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361, ___U24this_0)); }
	inline ConnectToPreviousRemote_t2022260273 * get_U24this_0() const { return ___U24this_0; }
	inline ConnectToPreviousRemote_t2022260273 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ConnectToPreviousRemote_t2022260273 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOCONNECTLASTREMOTEU3EC__ITERATOR0_T1411973361_H
#ifndef AXIS_T2182888826_H
#define AXIS_T2182888826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/Axis
struct  Axis_t2182888826  : public RuntimeObject
{
public:
	// System.String SimpleInput/Axis::name
	String_t* ___name_0;
	// System.Collections.Generic.List`1<SimpleInput/AxisInput> SimpleInput/Axis::inputs
	List_1_t2274532392 * ___inputs_1;
	// System.Single SimpleInput/Axis::value
	float ___value_2;
	// System.Single SimpleInput/Axis::valueRaw
	float ___valueRaw_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___inputs_1)); }
	inline List_1_t2274532392 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t2274532392 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t2274532392 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___value_2)); }
	inline float get_value_2() const { return ___value_2; }
	inline float* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(float value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_valueRaw_3() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___valueRaw_3)); }
	inline float get_valueRaw_3() const { return ___valueRaw_3; }
	inline float* get_address_of_valueRaw_3() { return &___valueRaw_3; }
	inline void set_valueRaw_3(float value)
	{
		___valueRaw_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T2182888826_H
#ifndef SHADER_T1748525893_H
#define SHADER_T1748525893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.Constants/Shader
struct  Shader_t1748525893  : public RuntimeObject
{
public:

public:
};

struct Shader_t1748525893_StaticFields
{
public:
	// System.String FlatLighting.Constants/Shader::AXIS_COLORS_LOCAL
	String_t* ___AXIS_COLORS_LOCAL_0;
	// System.String FlatLighting.Constants/Shader::AXIS_COLORS_GLOBAL
	String_t* ___AXIS_COLORS_GLOBAL_1;
	// System.String FlatLighting.Constants/Shader::SYMETRIC_COLORS_ON_KEYWORD
	String_t* ___SYMETRIC_COLORS_ON_KEYWORD_2;
	// System.String FlatLighting.Constants/Shader::SYMETRIC_COLORS_OFF_KEYWORD
	String_t* ___SYMETRIC_COLORS_OFF_KEYWORD_3;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_ON_X_KEYWORD
	String_t* ___AXIS_GRADIENT_ON_X_KEYWORD_4;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_ON_Y_KEYWORD
	String_t* ___AXIS_GRADIENT_ON_Y_KEYWORD_5;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_ON_Z_KEYWORD
	String_t* ___AXIS_GRADIENT_ON_Z_KEYWORD_6;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_OFF_KEYWORD
	String_t* ___AXIS_GRADIENT_OFF_KEYWORD_7;
	// System.String FlatLighting.Constants/Shader::VERTEX_COLOR_KEYWORD
	String_t* ___VERTEX_COLOR_KEYWORD_8;
	// System.String FlatLighting.Constants/Shader::AMBIENT_LIGHT_KEYWORD
	String_t* ___AMBIENT_LIGHT_KEYWORD_9;
	// System.String FlatLighting.Constants/Shader::DIRECT_LIGHT_KEYWORD
	String_t* ___DIRECT_LIGHT_KEYWORD_10;
	// System.String FlatLighting.Constants/Shader::SPOT_LIGHT_KEYWORD
	String_t* ___SPOT_LIGHT_KEYWORD_11;
	// System.String FlatLighting.Constants/Shader::POINT_LIGHT_KEYWORD
	String_t* ___POINT_LIGHT_KEYWORD_12;
	// System.String FlatLighting.Constants/Shader::BLEND_LIGHT_SOURCES_KEYWORD
	String_t* ___BLEND_LIGHT_SOURCES_KEYWORD_13;
	// System.String FlatLighting.Constants/Shader::GRADIENT_LOCAL_KEYWORD
	String_t* ___GRADIENT_LOCAL_KEYWORD_14;
	// System.String FlatLighting.Constants/Shader::GRADIENT_WORLD_KEYWORD
	String_t* ___GRADIENT_WORLD_KEYWORD_15;
	// System.String FlatLighting.Constants/Shader::CUSTOM_LIGHTMAPPING_KEYWORD
	String_t* ___CUSTOM_LIGHTMAPPING_KEYWORD_16;
	// System.String FlatLighting.Constants/Shader::UNITY_LIGHTMAPPING_KEYWORD
	String_t* ___UNITY_LIGHTMAPPING_KEYWORD_17;
	// System.String FlatLighting.Constants/Shader::RECEIVE_CUSTOM_SHADOW_KEYWORD
	String_t* ___RECEIVE_CUSTOM_SHADOW_KEYWORD_18;
	// System.String FlatLighting.Constants/Shader::CAST_CUSTOM_SHADOW_ON_KEYWORD
	String_t* ___CAST_CUSTOM_SHADOW_ON_KEYWORD_19;
	// System.String FlatLighting.Constants/Shader::CAST_CUSTOM_SHADOW_OFF_KEYWORD
	String_t* ___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20;
	// System.String FlatLighting.Constants/Shader::USE_MAIN_TEXTURE_KEYWORD
	String_t* ___USE_MAIN_TEXTURE_KEYWORD_21;
	// System.String FlatLighting.Constants/Shader::LightPositiveX
	String_t* ___LightPositiveX_22;
	// System.String FlatLighting.Constants/Shader::LightPositiveY
	String_t* ___LightPositiveY_23;
	// System.String FlatLighting.Constants/Shader::LightPositiveZ
	String_t* ___LightPositiveZ_24;
	// System.String FlatLighting.Constants/Shader::LightNegativeX
	String_t* ___LightNegativeX_25;
	// System.String FlatLighting.Constants/Shader::LightNegativeY
	String_t* ___LightNegativeY_26;
	// System.String FlatLighting.Constants/Shader::LightNegativeZ
	String_t* ___LightNegativeZ_27;
	// System.String FlatLighting.Constants/Shader::LightPositive2X
	String_t* ___LightPositive2X_28;
	// System.String FlatLighting.Constants/Shader::LightPositive2Y
	String_t* ___LightPositive2Y_29;
	// System.String FlatLighting.Constants/Shader::LightPositive2Z
	String_t* ___LightPositive2Z_30;
	// System.String FlatLighting.Constants/Shader::LightNegative2X
	String_t* ___LightNegative2X_31;
	// System.String FlatLighting.Constants/Shader::LightNegative2Y
	String_t* ___LightNegative2Y_32;
	// System.String FlatLighting.Constants/Shader::LightNegative2Z
	String_t* ___LightNegative2Z_33;
	// System.String FlatLighting.Constants/Shader::GradientWidthPositiveX
	String_t* ___GradientWidthPositiveX_34;
	// System.String FlatLighting.Constants/Shader::GradientWidthPositiveY
	String_t* ___GradientWidthPositiveY_35;
	// System.String FlatLighting.Constants/Shader::GradientWidthPositiveZ
	String_t* ___GradientWidthPositiveZ_36;
	// System.String FlatLighting.Constants/Shader::GradientWidthNegativeX
	String_t* ___GradientWidthNegativeX_37;
	// System.String FlatLighting.Constants/Shader::GradientWidthNegativeY
	String_t* ___GradientWidthNegativeY_38;
	// System.String FlatLighting.Constants/Shader::GradientWidthNegativeZ
	String_t* ___GradientWidthNegativeZ_39;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetPositiveX
	String_t* ___GradientOriginOffsetPositiveX_40;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetPositiveY
	String_t* ___GradientOriginOffsetPositiveY_41;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetPositiveZ
	String_t* ___GradientOriginOffsetPositiveZ_42;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetNegativeX
	String_t* ___GradientOriginOffsetNegativeX_43;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetNegativeY
	String_t* ___GradientOriginOffsetNegativeY_44;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetNegativeZ
	String_t* ___GradientOriginOffsetNegativeZ_45;

public:
	inline static int32_t get_offset_of_AXIS_COLORS_LOCAL_0() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AXIS_COLORS_LOCAL_0)); }
	inline String_t* get_AXIS_COLORS_LOCAL_0() const { return ___AXIS_COLORS_LOCAL_0; }
	inline String_t** get_address_of_AXIS_COLORS_LOCAL_0() { return &___AXIS_COLORS_LOCAL_0; }
	inline void set_AXIS_COLORS_LOCAL_0(String_t* value)
	{
		___AXIS_COLORS_LOCAL_0 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_COLORS_LOCAL_0), value);
	}

	inline static int32_t get_offset_of_AXIS_COLORS_GLOBAL_1() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AXIS_COLORS_GLOBAL_1)); }
	inline String_t* get_AXIS_COLORS_GLOBAL_1() const { return ___AXIS_COLORS_GLOBAL_1; }
	inline String_t** get_address_of_AXIS_COLORS_GLOBAL_1() { return &___AXIS_COLORS_GLOBAL_1; }
	inline void set_AXIS_COLORS_GLOBAL_1(String_t* value)
	{
		___AXIS_COLORS_GLOBAL_1 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_COLORS_GLOBAL_1), value);
	}

	inline static int32_t get_offset_of_SYMETRIC_COLORS_ON_KEYWORD_2() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___SYMETRIC_COLORS_ON_KEYWORD_2)); }
	inline String_t* get_SYMETRIC_COLORS_ON_KEYWORD_2() const { return ___SYMETRIC_COLORS_ON_KEYWORD_2; }
	inline String_t** get_address_of_SYMETRIC_COLORS_ON_KEYWORD_2() { return &___SYMETRIC_COLORS_ON_KEYWORD_2; }
	inline void set_SYMETRIC_COLORS_ON_KEYWORD_2(String_t* value)
	{
		___SYMETRIC_COLORS_ON_KEYWORD_2 = value;
		Il2CppCodeGenWriteBarrier((&___SYMETRIC_COLORS_ON_KEYWORD_2), value);
	}

	inline static int32_t get_offset_of_SYMETRIC_COLORS_OFF_KEYWORD_3() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___SYMETRIC_COLORS_OFF_KEYWORD_3)); }
	inline String_t* get_SYMETRIC_COLORS_OFF_KEYWORD_3() const { return ___SYMETRIC_COLORS_OFF_KEYWORD_3; }
	inline String_t** get_address_of_SYMETRIC_COLORS_OFF_KEYWORD_3() { return &___SYMETRIC_COLORS_OFF_KEYWORD_3; }
	inline void set_SYMETRIC_COLORS_OFF_KEYWORD_3(String_t* value)
	{
		___SYMETRIC_COLORS_OFF_KEYWORD_3 = value;
		Il2CppCodeGenWriteBarrier((&___SYMETRIC_COLORS_OFF_KEYWORD_3), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_ON_X_KEYWORD_4() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AXIS_GRADIENT_ON_X_KEYWORD_4)); }
	inline String_t* get_AXIS_GRADIENT_ON_X_KEYWORD_4() const { return ___AXIS_GRADIENT_ON_X_KEYWORD_4; }
	inline String_t** get_address_of_AXIS_GRADIENT_ON_X_KEYWORD_4() { return &___AXIS_GRADIENT_ON_X_KEYWORD_4; }
	inline void set_AXIS_GRADIENT_ON_X_KEYWORD_4(String_t* value)
	{
		___AXIS_GRADIENT_ON_X_KEYWORD_4 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_ON_X_KEYWORD_4), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_ON_Y_KEYWORD_5() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AXIS_GRADIENT_ON_Y_KEYWORD_5)); }
	inline String_t* get_AXIS_GRADIENT_ON_Y_KEYWORD_5() const { return ___AXIS_GRADIENT_ON_Y_KEYWORD_5; }
	inline String_t** get_address_of_AXIS_GRADIENT_ON_Y_KEYWORD_5() { return &___AXIS_GRADIENT_ON_Y_KEYWORD_5; }
	inline void set_AXIS_GRADIENT_ON_Y_KEYWORD_5(String_t* value)
	{
		___AXIS_GRADIENT_ON_Y_KEYWORD_5 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_ON_Y_KEYWORD_5), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_ON_Z_KEYWORD_6() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AXIS_GRADIENT_ON_Z_KEYWORD_6)); }
	inline String_t* get_AXIS_GRADIENT_ON_Z_KEYWORD_6() const { return ___AXIS_GRADIENT_ON_Z_KEYWORD_6; }
	inline String_t** get_address_of_AXIS_GRADIENT_ON_Z_KEYWORD_6() { return &___AXIS_GRADIENT_ON_Z_KEYWORD_6; }
	inline void set_AXIS_GRADIENT_ON_Z_KEYWORD_6(String_t* value)
	{
		___AXIS_GRADIENT_ON_Z_KEYWORD_6 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_ON_Z_KEYWORD_6), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_OFF_KEYWORD_7() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AXIS_GRADIENT_OFF_KEYWORD_7)); }
	inline String_t* get_AXIS_GRADIENT_OFF_KEYWORD_7() const { return ___AXIS_GRADIENT_OFF_KEYWORD_7; }
	inline String_t** get_address_of_AXIS_GRADIENT_OFF_KEYWORD_7() { return &___AXIS_GRADIENT_OFF_KEYWORD_7; }
	inline void set_AXIS_GRADIENT_OFF_KEYWORD_7(String_t* value)
	{
		___AXIS_GRADIENT_OFF_KEYWORD_7 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_OFF_KEYWORD_7), value);
	}

	inline static int32_t get_offset_of_VERTEX_COLOR_KEYWORD_8() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___VERTEX_COLOR_KEYWORD_8)); }
	inline String_t* get_VERTEX_COLOR_KEYWORD_8() const { return ___VERTEX_COLOR_KEYWORD_8; }
	inline String_t** get_address_of_VERTEX_COLOR_KEYWORD_8() { return &___VERTEX_COLOR_KEYWORD_8; }
	inline void set_VERTEX_COLOR_KEYWORD_8(String_t* value)
	{
		___VERTEX_COLOR_KEYWORD_8 = value;
		Il2CppCodeGenWriteBarrier((&___VERTEX_COLOR_KEYWORD_8), value);
	}

	inline static int32_t get_offset_of_AMBIENT_LIGHT_KEYWORD_9() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___AMBIENT_LIGHT_KEYWORD_9)); }
	inline String_t* get_AMBIENT_LIGHT_KEYWORD_9() const { return ___AMBIENT_LIGHT_KEYWORD_9; }
	inline String_t** get_address_of_AMBIENT_LIGHT_KEYWORD_9() { return &___AMBIENT_LIGHT_KEYWORD_9; }
	inline void set_AMBIENT_LIGHT_KEYWORD_9(String_t* value)
	{
		___AMBIENT_LIGHT_KEYWORD_9 = value;
		Il2CppCodeGenWriteBarrier((&___AMBIENT_LIGHT_KEYWORD_9), value);
	}

	inline static int32_t get_offset_of_DIRECT_LIGHT_KEYWORD_10() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___DIRECT_LIGHT_KEYWORD_10)); }
	inline String_t* get_DIRECT_LIGHT_KEYWORD_10() const { return ___DIRECT_LIGHT_KEYWORD_10; }
	inline String_t** get_address_of_DIRECT_LIGHT_KEYWORD_10() { return &___DIRECT_LIGHT_KEYWORD_10; }
	inline void set_DIRECT_LIGHT_KEYWORD_10(String_t* value)
	{
		___DIRECT_LIGHT_KEYWORD_10 = value;
		Il2CppCodeGenWriteBarrier((&___DIRECT_LIGHT_KEYWORD_10), value);
	}

	inline static int32_t get_offset_of_SPOT_LIGHT_KEYWORD_11() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___SPOT_LIGHT_KEYWORD_11)); }
	inline String_t* get_SPOT_LIGHT_KEYWORD_11() const { return ___SPOT_LIGHT_KEYWORD_11; }
	inline String_t** get_address_of_SPOT_LIGHT_KEYWORD_11() { return &___SPOT_LIGHT_KEYWORD_11; }
	inline void set_SPOT_LIGHT_KEYWORD_11(String_t* value)
	{
		___SPOT_LIGHT_KEYWORD_11 = value;
		Il2CppCodeGenWriteBarrier((&___SPOT_LIGHT_KEYWORD_11), value);
	}

	inline static int32_t get_offset_of_POINT_LIGHT_KEYWORD_12() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___POINT_LIGHT_KEYWORD_12)); }
	inline String_t* get_POINT_LIGHT_KEYWORD_12() const { return ___POINT_LIGHT_KEYWORD_12; }
	inline String_t** get_address_of_POINT_LIGHT_KEYWORD_12() { return &___POINT_LIGHT_KEYWORD_12; }
	inline void set_POINT_LIGHT_KEYWORD_12(String_t* value)
	{
		___POINT_LIGHT_KEYWORD_12 = value;
		Il2CppCodeGenWriteBarrier((&___POINT_LIGHT_KEYWORD_12), value);
	}

	inline static int32_t get_offset_of_BLEND_LIGHT_SOURCES_KEYWORD_13() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___BLEND_LIGHT_SOURCES_KEYWORD_13)); }
	inline String_t* get_BLEND_LIGHT_SOURCES_KEYWORD_13() const { return ___BLEND_LIGHT_SOURCES_KEYWORD_13; }
	inline String_t** get_address_of_BLEND_LIGHT_SOURCES_KEYWORD_13() { return &___BLEND_LIGHT_SOURCES_KEYWORD_13; }
	inline void set_BLEND_LIGHT_SOURCES_KEYWORD_13(String_t* value)
	{
		___BLEND_LIGHT_SOURCES_KEYWORD_13 = value;
		Il2CppCodeGenWriteBarrier((&___BLEND_LIGHT_SOURCES_KEYWORD_13), value);
	}

	inline static int32_t get_offset_of_GRADIENT_LOCAL_KEYWORD_14() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GRADIENT_LOCAL_KEYWORD_14)); }
	inline String_t* get_GRADIENT_LOCAL_KEYWORD_14() const { return ___GRADIENT_LOCAL_KEYWORD_14; }
	inline String_t** get_address_of_GRADIENT_LOCAL_KEYWORD_14() { return &___GRADIENT_LOCAL_KEYWORD_14; }
	inline void set_GRADIENT_LOCAL_KEYWORD_14(String_t* value)
	{
		___GRADIENT_LOCAL_KEYWORD_14 = value;
		Il2CppCodeGenWriteBarrier((&___GRADIENT_LOCAL_KEYWORD_14), value);
	}

	inline static int32_t get_offset_of_GRADIENT_WORLD_KEYWORD_15() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GRADIENT_WORLD_KEYWORD_15)); }
	inline String_t* get_GRADIENT_WORLD_KEYWORD_15() const { return ___GRADIENT_WORLD_KEYWORD_15; }
	inline String_t** get_address_of_GRADIENT_WORLD_KEYWORD_15() { return &___GRADIENT_WORLD_KEYWORD_15; }
	inline void set_GRADIENT_WORLD_KEYWORD_15(String_t* value)
	{
		___GRADIENT_WORLD_KEYWORD_15 = value;
		Il2CppCodeGenWriteBarrier((&___GRADIENT_WORLD_KEYWORD_15), value);
	}

	inline static int32_t get_offset_of_CUSTOM_LIGHTMAPPING_KEYWORD_16() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___CUSTOM_LIGHTMAPPING_KEYWORD_16)); }
	inline String_t* get_CUSTOM_LIGHTMAPPING_KEYWORD_16() const { return ___CUSTOM_LIGHTMAPPING_KEYWORD_16; }
	inline String_t** get_address_of_CUSTOM_LIGHTMAPPING_KEYWORD_16() { return &___CUSTOM_LIGHTMAPPING_KEYWORD_16; }
	inline void set_CUSTOM_LIGHTMAPPING_KEYWORD_16(String_t* value)
	{
		___CUSTOM_LIGHTMAPPING_KEYWORD_16 = value;
		Il2CppCodeGenWriteBarrier((&___CUSTOM_LIGHTMAPPING_KEYWORD_16), value);
	}

	inline static int32_t get_offset_of_UNITY_LIGHTMAPPING_KEYWORD_17() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___UNITY_LIGHTMAPPING_KEYWORD_17)); }
	inline String_t* get_UNITY_LIGHTMAPPING_KEYWORD_17() const { return ___UNITY_LIGHTMAPPING_KEYWORD_17; }
	inline String_t** get_address_of_UNITY_LIGHTMAPPING_KEYWORD_17() { return &___UNITY_LIGHTMAPPING_KEYWORD_17; }
	inline void set_UNITY_LIGHTMAPPING_KEYWORD_17(String_t* value)
	{
		___UNITY_LIGHTMAPPING_KEYWORD_17 = value;
		Il2CppCodeGenWriteBarrier((&___UNITY_LIGHTMAPPING_KEYWORD_17), value);
	}

	inline static int32_t get_offset_of_RECEIVE_CUSTOM_SHADOW_KEYWORD_18() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___RECEIVE_CUSTOM_SHADOW_KEYWORD_18)); }
	inline String_t* get_RECEIVE_CUSTOM_SHADOW_KEYWORD_18() const { return ___RECEIVE_CUSTOM_SHADOW_KEYWORD_18; }
	inline String_t** get_address_of_RECEIVE_CUSTOM_SHADOW_KEYWORD_18() { return &___RECEIVE_CUSTOM_SHADOW_KEYWORD_18; }
	inline void set_RECEIVE_CUSTOM_SHADOW_KEYWORD_18(String_t* value)
	{
		___RECEIVE_CUSTOM_SHADOW_KEYWORD_18 = value;
		Il2CppCodeGenWriteBarrier((&___RECEIVE_CUSTOM_SHADOW_KEYWORD_18), value);
	}

	inline static int32_t get_offset_of_CAST_CUSTOM_SHADOW_ON_KEYWORD_19() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___CAST_CUSTOM_SHADOW_ON_KEYWORD_19)); }
	inline String_t* get_CAST_CUSTOM_SHADOW_ON_KEYWORD_19() const { return ___CAST_CUSTOM_SHADOW_ON_KEYWORD_19; }
	inline String_t** get_address_of_CAST_CUSTOM_SHADOW_ON_KEYWORD_19() { return &___CAST_CUSTOM_SHADOW_ON_KEYWORD_19; }
	inline void set_CAST_CUSTOM_SHADOW_ON_KEYWORD_19(String_t* value)
	{
		___CAST_CUSTOM_SHADOW_ON_KEYWORD_19 = value;
		Il2CppCodeGenWriteBarrier((&___CAST_CUSTOM_SHADOW_ON_KEYWORD_19), value);
	}

	inline static int32_t get_offset_of_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20)); }
	inline String_t* get_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20() const { return ___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20; }
	inline String_t** get_address_of_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20() { return &___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20; }
	inline void set_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20(String_t* value)
	{
		___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20 = value;
		Il2CppCodeGenWriteBarrier((&___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20), value);
	}

	inline static int32_t get_offset_of_USE_MAIN_TEXTURE_KEYWORD_21() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___USE_MAIN_TEXTURE_KEYWORD_21)); }
	inline String_t* get_USE_MAIN_TEXTURE_KEYWORD_21() const { return ___USE_MAIN_TEXTURE_KEYWORD_21; }
	inline String_t** get_address_of_USE_MAIN_TEXTURE_KEYWORD_21() { return &___USE_MAIN_TEXTURE_KEYWORD_21; }
	inline void set_USE_MAIN_TEXTURE_KEYWORD_21(String_t* value)
	{
		___USE_MAIN_TEXTURE_KEYWORD_21 = value;
		Il2CppCodeGenWriteBarrier((&___USE_MAIN_TEXTURE_KEYWORD_21), value);
	}

	inline static int32_t get_offset_of_LightPositiveX_22() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightPositiveX_22)); }
	inline String_t* get_LightPositiveX_22() const { return ___LightPositiveX_22; }
	inline String_t** get_address_of_LightPositiveX_22() { return &___LightPositiveX_22; }
	inline void set_LightPositiveX_22(String_t* value)
	{
		___LightPositiveX_22 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositiveX_22), value);
	}

	inline static int32_t get_offset_of_LightPositiveY_23() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightPositiveY_23)); }
	inline String_t* get_LightPositiveY_23() const { return ___LightPositiveY_23; }
	inline String_t** get_address_of_LightPositiveY_23() { return &___LightPositiveY_23; }
	inline void set_LightPositiveY_23(String_t* value)
	{
		___LightPositiveY_23 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositiveY_23), value);
	}

	inline static int32_t get_offset_of_LightPositiveZ_24() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightPositiveZ_24)); }
	inline String_t* get_LightPositiveZ_24() const { return ___LightPositiveZ_24; }
	inline String_t** get_address_of_LightPositiveZ_24() { return &___LightPositiveZ_24; }
	inline void set_LightPositiveZ_24(String_t* value)
	{
		___LightPositiveZ_24 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositiveZ_24), value);
	}

	inline static int32_t get_offset_of_LightNegativeX_25() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightNegativeX_25)); }
	inline String_t* get_LightNegativeX_25() const { return ___LightNegativeX_25; }
	inline String_t** get_address_of_LightNegativeX_25() { return &___LightNegativeX_25; }
	inline void set_LightNegativeX_25(String_t* value)
	{
		___LightNegativeX_25 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegativeX_25), value);
	}

	inline static int32_t get_offset_of_LightNegativeY_26() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightNegativeY_26)); }
	inline String_t* get_LightNegativeY_26() const { return ___LightNegativeY_26; }
	inline String_t** get_address_of_LightNegativeY_26() { return &___LightNegativeY_26; }
	inline void set_LightNegativeY_26(String_t* value)
	{
		___LightNegativeY_26 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegativeY_26), value);
	}

	inline static int32_t get_offset_of_LightNegativeZ_27() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightNegativeZ_27)); }
	inline String_t* get_LightNegativeZ_27() const { return ___LightNegativeZ_27; }
	inline String_t** get_address_of_LightNegativeZ_27() { return &___LightNegativeZ_27; }
	inline void set_LightNegativeZ_27(String_t* value)
	{
		___LightNegativeZ_27 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegativeZ_27), value);
	}

	inline static int32_t get_offset_of_LightPositive2X_28() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightPositive2X_28)); }
	inline String_t* get_LightPositive2X_28() const { return ___LightPositive2X_28; }
	inline String_t** get_address_of_LightPositive2X_28() { return &___LightPositive2X_28; }
	inline void set_LightPositive2X_28(String_t* value)
	{
		___LightPositive2X_28 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositive2X_28), value);
	}

	inline static int32_t get_offset_of_LightPositive2Y_29() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightPositive2Y_29)); }
	inline String_t* get_LightPositive2Y_29() const { return ___LightPositive2Y_29; }
	inline String_t** get_address_of_LightPositive2Y_29() { return &___LightPositive2Y_29; }
	inline void set_LightPositive2Y_29(String_t* value)
	{
		___LightPositive2Y_29 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositive2Y_29), value);
	}

	inline static int32_t get_offset_of_LightPositive2Z_30() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightPositive2Z_30)); }
	inline String_t* get_LightPositive2Z_30() const { return ___LightPositive2Z_30; }
	inline String_t** get_address_of_LightPositive2Z_30() { return &___LightPositive2Z_30; }
	inline void set_LightPositive2Z_30(String_t* value)
	{
		___LightPositive2Z_30 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositive2Z_30), value);
	}

	inline static int32_t get_offset_of_LightNegative2X_31() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightNegative2X_31)); }
	inline String_t* get_LightNegative2X_31() const { return ___LightNegative2X_31; }
	inline String_t** get_address_of_LightNegative2X_31() { return &___LightNegative2X_31; }
	inline void set_LightNegative2X_31(String_t* value)
	{
		___LightNegative2X_31 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegative2X_31), value);
	}

	inline static int32_t get_offset_of_LightNegative2Y_32() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightNegative2Y_32)); }
	inline String_t* get_LightNegative2Y_32() const { return ___LightNegative2Y_32; }
	inline String_t** get_address_of_LightNegative2Y_32() { return &___LightNegative2Y_32; }
	inline void set_LightNegative2Y_32(String_t* value)
	{
		___LightNegative2Y_32 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegative2Y_32), value);
	}

	inline static int32_t get_offset_of_LightNegative2Z_33() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___LightNegative2Z_33)); }
	inline String_t* get_LightNegative2Z_33() const { return ___LightNegative2Z_33; }
	inline String_t** get_address_of_LightNegative2Z_33() { return &___LightNegative2Z_33; }
	inline void set_LightNegative2Z_33(String_t* value)
	{
		___LightNegative2Z_33 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegative2Z_33), value);
	}

	inline static int32_t get_offset_of_GradientWidthPositiveX_34() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientWidthPositiveX_34)); }
	inline String_t* get_GradientWidthPositiveX_34() const { return ___GradientWidthPositiveX_34; }
	inline String_t** get_address_of_GradientWidthPositiveX_34() { return &___GradientWidthPositiveX_34; }
	inline void set_GradientWidthPositiveX_34(String_t* value)
	{
		___GradientWidthPositiveX_34 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthPositiveX_34), value);
	}

	inline static int32_t get_offset_of_GradientWidthPositiveY_35() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientWidthPositiveY_35)); }
	inline String_t* get_GradientWidthPositiveY_35() const { return ___GradientWidthPositiveY_35; }
	inline String_t** get_address_of_GradientWidthPositiveY_35() { return &___GradientWidthPositiveY_35; }
	inline void set_GradientWidthPositiveY_35(String_t* value)
	{
		___GradientWidthPositiveY_35 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthPositiveY_35), value);
	}

	inline static int32_t get_offset_of_GradientWidthPositiveZ_36() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientWidthPositiveZ_36)); }
	inline String_t* get_GradientWidthPositiveZ_36() const { return ___GradientWidthPositiveZ_36; }
	inline String_t** get_address_of_GradientWidthPositiveZ_36() { return &___GradientWidthPositiveZ_36; }
	inline void set_GradientWidthPositiveZ_36(String_t* value)
	{
		___GradientWidthPositiveZ_36 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthPositiveZ_36), value);
	}

	inline static int32_t get_offset_of_GradientWidthNegativeX_37() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientWidthNegativeX_37)); }
	inline String_t* get_GradientWidthNegativeX_37() const { return ___GradientWidthNegativeX_37; }
	inline String_t** get_address_of_GradientWidthNegativeX_37() { return &___GradientWidthNegativeX_37; }
	inline void set_GradientWidthNegativeX_37(String_t* value)
	{
		___GradientWidthNegativeX_37 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthNegativeX_37), value);
	}

	inline static int32_t get_offset_of_GradientWidthNegativeY_38() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientWidthNegativeY_38)); }
	inline String_t* get_GradientWidthNegativeY_38() const { return ___GradientWidthNegativeY_38; }
	inline String_t** get_address_of_GradientWidthNegativeY_38() { return &___GradientWidthNegativeY_38; }
	inline void set_GradientWidthNegativeY_38(String_t* value)
	{
		___GradientWidthNegativeY_38 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthNegativeY_38), value);
	}

	inline static int32_t get_offset_of_GradientWidthNegativeZ_39() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientWidthNegativeZ_39)); }
	inline String_t* get_GradientWidthNegativeZ_39() const { return ___GradientWidthNegativeZ_39; }
	inline String_t** get_address_of_GradientWidthNegativeZ_39() { return &___GradientWidthNegativeZ_39; }
	inline void set_GradientWidthNegativeZ_39(String_t* value)
	{
		___GradientWidthNegativeZ_39 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthNegativeZ_39), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetPositiveX_40() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientOriginOffsetPositiveX_40)); }
	inline String_t* get_GradientOriginOffsetPositiveX_40() const { return ___GradientOriginOffsetPositiveX_40; }
	inline String_t** get_address_of_GradientOriginOffsetPositiveX_40() { return &___GradientOriginOffsetPositiveX_40; }
	inline void set_GradientOriginOffsetPositiveX_40(String_t* value)
	{
		___GradientOriginOffsetPositiveX_40 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetPositiveX_40), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetPositiveY_41() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientOriginOffsetPositiveY_41)); }
	inline String_t* get_GradientOriginOffsetPositiveY_41() const { return ___GradientOriginOffsetPositiveY_41; }
	inline String_t** get_address_of_GradientOriginOffsetPositiveY_41() { return &___GradientOriginOffsetPositiveY_41; }
	inline void set_GradientOriginOffsetPositiveY_41(String_t* value)
	{
		___GradientOriginOffsetPositiveY_41 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetPositiveY_41), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetPositiveZ_42() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientOriginOffsetPositiveZ_42)); }
	inline String_t* get_GradientOriginOffsetPositiveZ_42() const { return ___GradientOriginOffsetPositiveZ_42; }
	inline String_t** get_address_of_GradientOriginOffsetPositiveZ_42() { return &___GradientOriginOffsetPositiveZ_42; }
	inline void set_GradientOriginOffsetPositiveZ_42(String_t* value)
	{
		___GradientOriginOffsetPositiveZ_42 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetPositiveZ_42), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetNegativeX_43() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientOriginOffsetNegativeX_43)); }
	inline String_t* get_GradientOriginOffsetNegativeX_43() const { return ___GradientOriginOffsetNegativeX_43; }
	inline String_t** get_address_of_GradientOriginOffsetNegativeX_43() { return &___GradientOriginOffsetNegativeX_43; }
	inline void set_GradientOriginOffsetNegativeX_43(String_t* value)
	{
		___GradientOriginOffsetNegativeX_43 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetNegativeX_43), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetNegativeY_44() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientOriginOffsetNegativeY_44)); }
	inline String_t* get_GradientOriginOffsetNegativeY_44() const { return ___GradientOriginOffsetNegativeY_44; }
	inline String_t** get_address_of_GradientOriginOffsetNegativeY_44() { return &___GradientOriginOffsetNegativeY_44; }
	inline void set_GradientOriginOffsetNegativeY_44(String_t* value)
	{
		___GradientOriginOffsetNegativeY_44 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetNegativeY_44), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetNegativeZ_45() { return static_cast<int32_t>(offsetof(Shader_t1748525893_StaticFields, ___GradientOriginOffsetNegativeZ_45)); }
	inline String_t* get_GradientOriginOffsetNegativeZ_45() const { return ___GradientOriginOffsetNegativeZ_45; }
	inline String_t** get_address_of_GradientOriginOffsetNegativeZ_45() { return &___GradientOriginOffsetNegativeZ_45; }
	inline void set_GradientOriginOffsetNegativeZ_45(String_t* value)
	{
		___GradientOriginOffsetNegativeZ_45 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetNegativeZ_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T1748525893_H
#ifndef U3CCHECKREMOTESU3EC__ITERATOR1_T2653031284_H
#define U3CCHECKREMOTESU3EC__ITERATOR1_T2653031284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectToPreviousRemote/<CheckRemotes>c__Iterator1
struct  U3CCheckRemotesU3Ec__Iterator1_t2653031284  : public RuntimeObject
{
public:
	// ConnectToPreviousRemote ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$this
	ConnectToPreviousRemote_t2022260273 * ___U24this_0;
	// System.Object ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t2653031284, ___U24this_0)); }
	inline ConnectToPreviousRemote_t2022260273 * get_U24this_0() const { return ___U24this_0; }
	inline ConnectToPreviousRemote_t2022260273 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ConnectToPreviousRemote_t2022260273 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t2653031284, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t2653031284, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t2653031284, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

struct U3CCheckRemotesU3Ec__Iterator1_t2653031284_StaticFields
{
public:
	// System.Action`1<MiraRemoteException> ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::<>f__am$cache0
	Action_1_t3895617434 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t2653031284_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_1_t3895617434 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_1_t3895617434 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_1_t3895617434 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKREMOTESU3EC__ITERATOR1_T2653031284_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CONSTANTS_T439474454_H
#define CONSTANTS_T439474454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.Constants
struct  Constants_t439474454  : public RuntimeObject
{
public:

public:
};

struct Constants_t439474454_StaticFields
{
public:
	// System.String FlatLighting.Constants::FlatLightingShaderPath
	String_t* ___FlatLightingShaderPath_0;
	// System.String FlatLighting.Constants::FlatLightingTag
	String_t* ___FlatLightingTag_1;
	// System.String FlatLighting.Constants::FlatLightingBakedTag
	String_t* ___FlatLightingBakedTag_2;
	// System.String FlatLighting.Constants::GizmoIconsPath
	String_t* ___GizmoIconsPath_3;

public:
	inline static int32_t get_offset_of_FlatLightingShaderPath_0() { return static_cast<int32_t>(offsetof(Constants_t439474454_StaticFields, ___FlatLightingShaderPath_0)); }
	inline String_t* get_FlatLightingShaderPath_0() const { return ___FlatLightingShaderPath_0; }
	inline String_t** get_address_of_FlatLightingShaderPath_0() { return &___FlatLightingShaderPath_0; }
	inline void set_FlatLightingShaderPath_0(String_t* value)
	{
		___FlatLightingShaderPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightingShaderPath_0), value);
	}

	inline static int32_t get_offset_of_FlatLightingTag_1() { return static_cast<int32_t>(offsetof(Constants_t439474454_StaticFields, ___FlatLightingTag_1)); }
	inline String_t* get_FlatLightingTag_1() const { return ___FlatLightingTag_1; }
	inline String_t** get_address_of_FlatLightingTag_1() { return &___FlatLightingTag_1; }
	inline void set_FlatLightingTag_1(String_t* value)
	{
		___FlatLightingTag_1 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightingTag_1), value);
	}

	inline static int32_t get_offset_of_FlatLightingBakedTag_2() { return static_cast<int32_t>(offsetof(Constants_t439474454_StaticFields, ___FlatLightingBakedTag_2)); }
	inline String_t* get_FlatLightingBakedTag_2() const { return ___FlatLightingBakedTag_2; }
	inline String_t** get_address_of_FlatLightingBakedTag_2() { return &___FlatLightingBakedTag_2; }
	inline void set_FlatLightingBakedTag_2(String_t* value)
	{
		___FlatLightingBakedTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightingBakedTag_2), value);
	}

	inline static int32_t get_offset_of_GizmoIconsPath_3() { return static_cast<int32_t>(offsetof(Constants_t439474454_StaticFields, ___GizmoIconsPath_3)); }
	inline String_t* get_GizmoIconsPath_3() const { return ___GizmoIconsPath_3; }
	inline String_t** get_address_of_GizmoIconsPath_3() { return &___GizmoIconsPath_3; }
	inline void set_GizmoIconsPath_3(String_t* value)
	{
		___GizmoIconsPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___GizmoIconsPath_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T439474454_H
#ifndef NATIVEBRIDGE_T1552383108_H
#define NATIVEBRIDGE_T1552383108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge
struct  NativeBridge_t1552383108  : public RuntimeObject
{
public:

public:
};

struct NativeBridge_t1552383108_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Action> NativeBridge::emptyActions
	Dictionary_2_t1049633776 * ___emptyActions_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Boolean>> NativeBridge::boolActions
	Dictionary_2_t55011859 * ___boolActions_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Int32>> NativeBridge::intActions
	Dictionary_2_t2908669647 * ___intActions_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Single>> NativeBridge::floatActions
	Dictionary_2_t1354990668 * ___floatActions_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<MiraRemoteException>> NativeBridge::exceptionActions
	Dictionary_2_t3680873733 * ___exceptionActions_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<Remote>> NativeBridge::remoteActions
	Dictionary_2_t976236017 * ___remoteActions_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.Single,System.Single,System.Single>> NativeBridge::remoteMotionActions
	Dictionary_2_t2710841014 * ___remoteMotionActions_6;
	// NativeBridge/MRIntCallback NativeBridge::<>f__mg$cache0
	MRIntCallback_t3089559991 * ___U3CU3Ef__mgU24cache0_7;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cache1
	MRBoolCallback_t3968806430 * ___U3CU3Ef__mgU24cache1_8;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cache2
	MRBoolCallback_t3968806430 * ___U3CU3Ef__mgU24cache2_9;
	// System.Action`1<System.Boolean> NativeBridge::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_10;
	// NativeBridge/MRDiscoveredRemoteCallback NativeBridge::<>f__mg$cache3
	MRDiscoveredRemoteCallback_t179447917 * ___U3CU3Ef__mgU24cache3_11;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache4
	MRErrorCallback_t1000734509 * ___U3CU3Ef__mgU24cache4_12;
	// NativeBridge/MRConnectedRemoteCallback NativeBridge::<>f__mg$cache5
	MRConnectedRemoteCallback_t1852255652 * ___U3CU3Ef__mgU24cache5_13;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache6
	MRErrorCallback_t1000734509 * ___U3CU3Ef__mgU24cache6_14;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache7
	MRErrorCallback_t1000734509 * ___U3CU3Ef__mgU24cache7_15;
	// NativeBridge/MRConnectedRemoteCallback NativeBridge::<>f__mg$cache8
	MRConnectedRemoteCallback_t1852255652 * ___U3CU3Ef__mgU24cache8_16;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache9
	MRErrorCallback_t1000734509 * ___U3CU3Ef__mgU24cache9_17;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cacheA
	MRBoolCallback_t3968806430 * ___U3CU3Ef__mgU24cacheA_18;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cacheB
	MRBoolCallback_t3968806430 * ___U3CU3Ef__mgU24cacheB_19;
	// NativeBridge/MRFloatCallback NativeBridge::<>f__mg$cacheC
	MRFloatCallback_t806206182 * ___U3CU3Ef__mgU24cacheC_20;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cacheD
	MRBoolCallback_t3968806430 * ___U3CU3Ef__mgU24cacheD_21;
	// NativeBridge/MREmptyCallback NativeBridge::<>f__mg$cacheE
	MREmptyCallback_t625280627 * ___U3CU3Ef__mgU24cacheE_22;
	// NativeBridge/MRRemoteMotionCallback NativeBridge::<>f__mg$cacheF
	MRRemoteMotionCallback_t366018504 * ___U3CU3Ef__mgU24cacheF_23;
	// NativeBridge/MRRemoteMotionCallback NativeBridge::<>f__mg$cache10
	MRRemoteMotionCallback_t366018504 * ___U3CU3Ef__mgU24cache10_24;
	// NativeBridge/MRConnectedRemoteCallback NativeBridge::<>f__mg$cache11
	MRConnectedRemoteCallback_t1852255652 * ___U3CU3Ef__mgU24cache11_25;
	// NativeBridge/MRDiscoveredRemoteCallback NativeBridge::<>f__mg$cache12
	MRDiscoveredRemoteCallback_t179447917 * ___U3CU3Ef__mgU24cache12_26;
	// NativeBridge/MRDiscoveredRemoteCallback NativeBridge::<>f__mg$cache13
	MRDiscoveredRemoteCallback_t179447917 * ___U3CU3Ef__mgU24cache13_27;

public:
	inline static int32_t get_offset_of_emptyActions_0() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___emptyActions_0)); }
	inline Dictionary_2_t1049633776 * get_emptyActions_0() const { return ___emptyActions_0; }
	inline Dictionary_2_t1049633776 ** get_address_of_emptyActions_0() { return &___emptyActions_0; }
	inline void set_emptyActions_0(Dictionary_2_t1049633776 * value)
	{
		___emptyActions_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyActions_0), value);
	}

	inline static int32_t get_offset_of_boolActions_1() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___boolActions_1)); }
	inline Dictionary_2_t55011859 * get_boolActions_1() const { return ___boolActions_1; }
	inline Dictionary_2_t55011859 ** get_address_of_boolActions_1() { return &___boolActions_1; }
	inline void set_boolActions_1(Dictionary_2_t55011859 * value)
	{
		___boolActions_1 = value;
		Il2CppCodeGenWriteBarrier((&___boolActions_1), value);
	}

	inline static int32_t get_offset_of_intActions_2() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___intActions_2)); }
	inline Dictionary_2_t2908669647 * get_intActions_2() const { return ___intActions_2; }
	inline Dictionary_2_t2908669647 ** get_address_of_intActions_2() { return &___intActions_2; }
	inline void set_intActions_2(Dictionary_2_t2908669647 * value)
	{
		___intActions_2 = value;
		Il2CppCodeGenWriteBarrier((&___intActions_2), value);
	}

	inline static int32_t get_offset_of_floatActions_3() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___floatActions_3)); }
	inline Dictionary_2_t1354990668 * get_floatActions_3() const { return ___floatActions_3; }
	inline Dictionary_2_t1354990668 ** get_address_of_floatActions_3() { return &___floatActions_3; }
	inline void set_floatActions_3(Dictionary_2_t1354990668 * value)
	{
		___floatActions_3 = value;
		Il2CppCodeGenWriteBarrier((&___floatActions_3), value);
	}

	inline static int32_t get_offset_of_exceptionActions_4() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___exceptionActions_4)); }
	inline Dictionary_2_t3680873733 * get_exceptionActions_4() const { return ___exceptionActions_4; }
	inline Dictionary_2_t3680873733 ** get_address_of_exceptionActions_4() { return &___exceptionActions_4; }
	inline void set_exceptionActions_4(Dictionary_2_t3680873733 * value)
	{
		___exceptionActions_4 = value;
		Il2CppCodeGenWriteBarrier((&___exceptionActions_4), value);
	}

	inline static int32_t get_offset_of_remoteActions_5() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___remoteActions_5)); }
	inline Dictionary_2_t976236017 * get_remoteActions_5() const { return ___remoteActions_5; }
	inline Dictionary_2_t976236017 ** get_address_of_remoteActions_5() { return &___remoteActions_5; }
	inline void set_remoteActions_5(Dictionary_2_t976236017 * value)
	{
		___remoteActions_5 = value;
		Il2CppCodeGenWriteBarrier((&___remoteActions_5), value);
	}

	inline static int32_t get_offset_of_remoteMotionActions_6() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___remoteMotionActions_6)); }
	inline Dictionary_2_t2710841014 * get_remoteMotionActions_6() const { return ___remoteMotionActions_6; }
	inline Dictionary_2_t2710841014 ** get_address_of_remoteMotionActions_6() { return &___remoteMotionActions_6; }
	inline void set_remoteMotionActions_6(Dictionary_2_t2710841014 * value)
	{
		___remoteMotionActions_6 = value;
		Il2CppCodeGenWriteBarrier((&___remoteMotionActions_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline MRIntCallback_t3089559991 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline MRIntCallback_t3089559991 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(MRIntCallback_t3089559991 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline MRBoolCallback_t3968806430 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline MRBoolCallback_t3968806430 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(MRBoolCallback_t3968806430 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline MRBoolCallback_t3968806430 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline MRBoolCallback_t3968806430 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(MRBoolCallback_t3968806430 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_11() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache3_11)); }
	inline MRDiscoveredRemoteCallback_t179447917 * get_U3CU3Ef__mgU24cache3_11() const { return ___U3CU3Ef__mgU24cache3_11; }
	inline MRDiscoveredRemoteCallback_t179447917 ** get_address_of_U3CU3Ef__mgU24cache3_11() { return &___U3CU3Ef__mgU24cache3_11; }
	inline void set_U3CU3Ef__mgU24cache3_11(MRDiscoveredRemoteCallback_t179447917 * value)
	{
		___U3CU3Ef__mgU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_12() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache4_12)); }
	inline MRErrorCallback_t1000734509 * get_U3CU3Ef__mgU24cache4_12() const { return ___U3CU3Ef__mgU24cache4_12; }
	inline MRErrorCallback_t1000734509 ** get_address_of_U3CU3Ef__mgU24cache4_12() { return &___U3CU3Ef__mgU24cache4_12; }
	inline void set_U3CU3Ef__mgU24cache4_12(MRErrorCallback_t1000734509 * value)
	{
		___U3CU3Ef__mgU24cache4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_13() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache5_13)); }
	inline MRConnectedRemoteCallback_t1852255652 * get_U3CU3Ef__mgU24cache5_13() const { return ___U3CU3Ef__mgU24cache5_13; }
	inline MRConnectedRemoteCallback_t1852255652 ** get_address_of_U3CU3Ef__mgU24cache5_13() { return &___U3CU3Ef__mgU24cache5_13; }
	inline void set_U3CU3Ef__mgU24cache5_13(MRConnectedRemoteCallback_t1852255652 * value)
	{
		___U3CU3Ef__mgU24cache5_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_14() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache6_14)); }
	inline MRErrorCallback_t1000734509 * get_U3CU3Ef__mgU24cache6_14() const { return ___U3CU3Ef__mgU24cache6_14; }
	inline MRErrorCallback_t1000734509 ** get_address_of_U3CU3Ef__mgU24cache6_14() { return &___U3CU3Ef__mgU24cache6_14; }
	inline void set_U3CU3Ef__mgU24cache6_14(MRErrorCallback_t1000734509 * value)
	{
		___U3CU3Ef__mgU24cache6_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_15() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache7_15)); }
	inline MRErrorCallback_t1000734509 * get_U3CU3Ef__mgU24cache7_15() const { return ___U3CU3Ef__mgU24cache7_15; }
	inline MRErrorCallback_t1000734509 ** get_address_of_U3CU3Ef__mgU24cache7_15() { return &___U3CU3Ef__mgU24cache7_15; }
	inline void set_U3CU3Ef__mgU24cache7_15(MRErrorCallback_t1000734509 * value)
	{
		___U3CU3Ef__mgU24cache7_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_16() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache8_16)); }
	inline MRConnectedRemoteCallback_t1852255652 * get_U3CU3Ef__mgU24cache8_16() const { return ___U3CU3Ef__mgU24cache8_16; }
	inline MRConnectedRemoteCallback_t1852255652 ** get_address_of_U3CU3Ef__mgU24cache8_16() { return &___U3CU3Ef__mgU24cache8_16; }
	inline void set_U3CU3Ef__mgU24cache8_16(MRConnectedRemoteCallback_t1852255652 * value)
	{
		___U3CU3Ef__mgU24cache8_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_17() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache9_17)); }
	inline MRErrorCallback_t1000734509 * get_U3CU3Ef__mgU24cache9_17() const { return ___U3CU3Ef__mgU24cache9_17; }
	inline MRErrorCallback_t1000734509 ** get_address_of_U3CU3Ef__mgU24cache9_17() { return &___U3CU3Ef__mgU24cache9_17; }
	inline void set_U3CU3Ef__mgU24cache9_17(MRErrorCallback_t1000734509 * value)
	{
		___U3CU3Ef__mgU24cache9_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_18() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cacheA_18)); }
	inline MRBoolCallback_t3968806430 * get_U3CU3Ef__mgU24cacheA_18() const { return ___U3CU3Ef__mgU24cacheA_18; }
	inline MRBoolCallback_t3968806430 ** get_address_of_U3CU3Ef__mgU24cacheA_18() { return &___U3CU3Ef__mgU24cacheA_18; }
	inline void set_U3CU3Ef__mgU24cacheA_18(MRBoolCallback_t3968806430 * value)
	{
		___U3CU3Ef__mgU24cacheA_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_19() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cacheB_19)); }
	inline MRBoolCallback_t3968806430 * get_U3CU3Ef__mgU24cacheB_19() const { return ___U3CU3Ef__mgU24cacheB_19; }
	inline MRBoolCallback_t3968806430 ** get_address_of_U3CU3Ef__mgU24cacheB_19() { return &___U3CU3Ef__mgU24cacheB_19; }
	inline void set_U3CU3Ef__mgU24cacheB_19(MRBoolCallback_t3968806430 * value)
	{
		___U3CU3Ef__mgU24cacheB_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_20() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cacheC_20)); }
	inline MRFloatCallback_t806206182 * get_U3CU3Ef__mgU24cacheC_20() const { return ___U3CU3Ef__mgU24cacheC_20; }
	inline MRFloatCallback_t806206182 ** get_address_of_U3CU3Ef__mgU24cacheC_20() { return &___U3CU3Ef__mgU24cacheC_20; }
	inline void set_U3CU3Ef__mgU24cacheC_20(MRFloatCallback_t806206182 * value)
	{
		___U3CU3Ef__mgU24cacheC_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_21() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cacheD_21)); }
	inline MRBoolCallback_t3968806430 * get_U3CU3Ef__mgU24cacheD_21() const { return ___U3CU3Ef__mgU24cacheD_21; }
	inline MRBoolCallback_t3968806430 ** get_address_of_U3CU3Ef__mgU24cacheD_21() { return &___U3CU3Ef__mgU24cacheD_21; }
	inline void set_U3CU3Ef__mgU24cacheD_21(MRBoolCallback_t3968806430 * value)
	{
		___U3CU3Ef__mgU24cacheD_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_22() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cacheE_22)); }
	inline MREmptyCallback_t625280627 * get_U3CU3Ef__mgU24cacheE_22() const { return ___U3CU3Ef__mgU24cacheE_22; }
	inline MREmptyCallback_t625280627 ** get_address_of_U3CU3Ef__mgU24cacheE_22() { return &___U3CU3Ef__mgU24cacheE_22; }
	inline void set_U3CU3Ef__mgU24cacheE_22(MREmptyCallback_t625280627 * value)
	{
		___U3CU3Ef__mgU24cacheE_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_23() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cacheF_23)); }
	inline MRRemoteMotionCallback_t366018504 * get_U3CU3Ef__mgU24cacheF_23() const { return ___U3CU3Ef__mgU24cacheF_23; }
	inline MRRemoteMotionCallback_t366018504 ** get_address_of_U3CU3Ef__mgU24cacheF_23() { return &___U3CU3Ef__mgU24cacheF_23; }
	inline void set_U3CU3Ef__mgU24cacheF_23(MRRemoteMotionCallback_t366018504 * value)
	{
		___U3CU3Ef__mgU24cacheF_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_24() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache10_24)); }
	inline MRRemoteMotionCallback_t366018504 * get_U3CU3Ef__mgU24cache10_24() const { return ___U3CU3Ef__mgU24cache10_24; }
	inline MRRemoteMotionCallback_t366018504 ** get_address_of_U3CU3Ef__mgU24cache10_24() { return &___U3CU3Ef__mgU24cache10_24; }
	inline void set_U3CU3Ef__mgU24cache10_24(MRRemoteMotionCallback_t366018504 * value)
	{
		___U3CU3Ef__mgU24cache10_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_25() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache11_25)); }
	inline MRConnectedRemoteCallback_t1852255652 * get_U3CU3Ef__mgU24cache11_25() const { return ___U3CU3Ef__mgU24cache11_25; }
	inline MRConnectedRemoteCallback_t1852255652 ** get_address_of_U3CU3Ef__mgU24cache11_25() { return &___U3CU3Ef__mgU24cache11_25; }
	inline void set_U3CU3Ef__mgU24cache11_25(MRConnectedRemoteCallback_t1852255652 * value)
	{
		___U3CU3Ef__mgU24cache11_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_26() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache12_26)); }
	inline MRDiscoveredRemoteCallback_t179447917 * get_U3CU3Ef__mgU24cache12_26() const { return ___U3CU3Ef__mgU24cache12_26; }
	inline MRDiscoveredRemoteCallback_t179447917 ** get_address_of_U3CU3Ef__mgU24cache12_26() { return &___U3CU3Ef__mgU24cache12_26; }
	inline void set_U3CU3Ef__mgU24cache12_26(MRDiscoveredRemoteCallback_t179447917 * value)
	{
		___U3CU3Ef__mgU24cache12_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache13_27() { return static_cast<int32_t>(offsetof(NativeBridge_t1552383108_StaticFields, ___U3CU3Ef__mgU24cache13_27)); }
	inline MRDiscoveredRemoteCallback_t179447917 * get_U3CU3Ef__mgU24cache13_27() const { return ___U3CU3Ef__mgU24cache13_27; }
	inline MRDiscoveredRemoteCallback_t179447917 ** get_address_of_U3CU3Ef__mgU24cache13_27() { return &___U3CU3Ef__mgU24cache13_27; }
	inline void set_U3CU3Ef__mgU24cache13_27(MRDiscoveredRemoteCallback_t179447917 * value)
	{
		___U3CU3Ef__mgU24cache13_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache13_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEBRIDGE_T1552383108_H
#ifndef SDKBUILDINFORMATION_T1497247620_H
#define SDKBUILDINFORMATION_T1497247620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.SDKBuildInformation
struct  SDKBuildInformation_t1497247620  : public RuntimeObject
{
public:
	// System.String Wikitude.SDKBuildInformation::BuildConfiguration
	String_t* ___BuildConfiguration_0;
	// System.String Wikitude.SDKBuildInformation::BuildDate
	String_t* ___BuildDate_1;
	// System.String Wikitude.SDKBuildInformation::BuildNumber
	String_t* ___BuildNumber_2;
	// System.String Wikitude.SDKBuildInformation::SDKVersion
	String_t* ___SDKVersion_3;

public:
	inline static int32_t get_offset_of_BuildConfiguration_0() { return static_cast<int32_t>(offsetof(SDKBuildInformation_t1497247620, ___BuildConfiguration_0)); }
	inline String_t* get_BuildConfiguration_0() const { return ___BuildConfiguration_0; }
	inline String_t** get_address_of_BuildConfiguration_0() { return &___BuildConfiguration_0; }
	inline void set_BuildConfiguration_0(String_t* value)
	{
		___BuildConfiguration_0 = value;
		Il2CppCodeGenWriteBarrier((&___BuildConfiguration_0), value);
	}

	inline static int32_t get_offset_of_BuildDate_1() { return static_cast<int32_t>(offsetof(SDKBuildInformation_t1497247620, ___BuildDate_1)); }
	inline String_t* get_BuildDate_1() const { return ___BuildDate_1; }
	inline String_t** get_address_of_BuildDate_1() { return &___BuildDate_1; }
	inline void set_BuildDate_1(String_t* value)
	{
		___BuildDate_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuildDate_1), value);
	}

	inline static int32_t get_offset_of_BuildNumber_2() { return static_cast<int32_t>(offsetof(SDKBuildInformation_t1497247620, ___BuildNumber_2)); }
	inline String_t* get_BuildNumber_2() const { return ___BuildNumber_2; }
	inline String_t** get_address_of_BuildNumber_2() { return &___BuildNumber_2; }
	inline void set_BuildNumber_2(String_t* value)
	{
		___BuildNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___BuildNumber_2), value);
	}

	inline static int32_t get_offset_of_SDKVersion_3() { return static_cast<int32_t>(offsetof(SDKBuildInformation_t1497247620, ___SDKVersion_3)); }
	inline String_t* get_SDKVersion_3() const { return ___SDKVersion_3; }
	inline String_t** get_address_of_SDKVersion_3() { return &___SDKVersion_3; }
	inline void set_SDKVersion_3(String_t* value)
	{
		___SDKVersion_3 = value;
		Il2CppCodeGenWriteBarrier((&___SDKVersion_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SDKBUILDINFORMATION_T1497247620_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef WIKITUDESDK_T895845057_H
#define WIKITUDESDK_T895845057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeSDK
struct  WikitudeSDK_t895845057  : public RuntimeObject
{
public:

public:
};

struct WikitudeSDK_t895845057_StaticFields
{
public:
	// Wikitude.IPlatformBridge Wikitude.WikitudeSDK::_bridge
	RuntimeObject* ____bridge_1;
	// Wikitude.SDKBuildInformation Wikitude.WikitudeSDK::_cachedBuildInformation
	SDKBuildInformation_t1497247620 * ____cachedBuildInformation_2;

public:
	inline static int32_t get_offset_of__bridge_1() { return static_cast<int32_t>(offsetof(WikitudeSDK_t895845057_StaticFields, ____bridge_1)); }
	inline RuntimeObject* get__bridge_1() const { return ____bridge_1; }
	inline RuntimeObject** get_address_of__bridge_1() { return &____bridge_1; }
	inline void set__bridge_1(RuntimeObject* value)
	{
		____bridge_1 = value;
		Il2CppCodeGenWriteBarrier((&____bridge_1), value);
	}

	inline static int32_t get_offset_of__cachedBuildInformation_2() { return static_cast<int32_t>(offsetof(WikitudeSDK_t895845057_StaticFields, ____cachedBuildInformation_2)); }
	inline SDKBuildInformation_t1497247620 * get__cachedBuildInformation_2() const { return ____cachedBuildInformation_2; }
	inline SDKBuildInformation_t1497247620 ** get_address_of__cachedBuildInformation_2() { return &____cachedBuildInformation_2; }
	inline void set__cachedBuildInformation_2(SDKBuildInformation_t1497247620 * value)
	{
		____cachedBuildInformation_2 = value;
		Il2CppCodeGenWriteBarrier((&____cachedBuildInformation_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIKITUDESDK_T895845057_H
#ifndef TARGETSOURCE_T2046190744_H
#define TARGETSOURCE_T2046190744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetSource
struct  TargetSource_t2046190744  : public RuntimeObject
{
public:
	// System.Int64 Wikitude.TargetSource::_identifier
	int64_t ____identifier_0;
	// System.Boolean Wikitude.TargetSource::<IsRegistered>k__BackingField
	bool ___U3CIsRegisteredU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__identifier_0() { return static_cast<int32_t>(offsetof(TargetSource_t2046190744, ____identifier_0)); }
	inline int64_t get__identifier_0() const { return ____identifier_0; }
	inline int64_t* get_address_of__identifier_0() { return &____identifier_0; }
	inline void set__identifier_0(int64_t value)
	{
		____identifier_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsRegisteredU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TargetSource_t2046190744, ___U3CIsRegisteredU3Ek__BackingField_1)); }
	inline bool get_U3CIsRegisteredU3Ek__BackingField_1() const { return ___U3CIsRegisteredU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsRegisteredU3Ek__BackingField_1() { return &___U3CIsRegisteredU3Ek__BackingField_1; }
	inline void set_U3CIsRegisteredU3Ek__BackingField_1(bool value)
	{
		___U3CIsRegisteredU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETSOURCE_T2046190744_H
#ifndef RECOGNIZEDTARGET_T3529911668_H
#define RECOGNIZEDTARGET_T3529911668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.RecognizedTarget
struct  RecognizedTarget_t3529911668  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Wikitude.RecognizedTarget::Drawable
	GameObject_t1113636619 * ___Drawable_0;
	// System.String Wikitude.RecognizedTarget::Name
	String_t* ___Name_1;
	// System.Int64 Wikitude.RecognizedTarget::ID
	int64_t ___ID_2;
	// System.Single[] Wikitude.RecognizedTarget::ModelViewMatrix
	SingleU5BU5D_t1444911251* ___ModelViewMatrix_3;
	// System.Boolean Wikitude.RecognizedTarget::IsKnown
	bool ___IsKnown_4;
	// System.Single Wikitude.RecognizedTarget::_physicalTargetHeight
	float ____physicalTargetHeight_5;

public:
	inline static int32_t get_offset_of_Drawable_0() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___Drawable_0)); }
	inline GameObject_t1113636619 * get_Drawable_0() const { return ___Drawable_0; }
	inline GameObject_t1113636619 ** get_address_of_Drawable_0() { return &___Drawable_0; }
	inline void set_Drawable_0(GameObject_t1113636619 * value)
	{
		___Drawable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Drawable_0), value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_ID_2() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___ID_2)); }
	inline int64_t get_ID_2() const { return ___ID_2; }
	inline int64_t* get_address_of_ID_2() { return &___ID_2; }
	inline void set_ID_2(int64_t value)
	{
		___ID_2 = value;
	}

	inline static int32_t get_offset_of_ModelViewMatrix_3() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___ModelViewMatrix_3)); }
	inline SingleU5BU5D_t1444911251* get_ModelViewMatrix_3() const { return ___ModelViewMatrix_3; }
	inline SingleU5BU5D_t1444911251** get_address_of_ModelViewMatrix_3() { return &___ModelViewMatrix_3; }
	inline void set_ModelViewMatrix_3(SingleU5BU5D_t1444911251* value)
	{
		___ModelViewMatrix_3 = value;
		Il2CppCodeGenWriteBarrier((&___ModelViewMatrix_3), value);
	}

	inline static int32_t get_offset_of_IsKnown_4() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___IsKnown_4)); }
	inline bool get_IsKnown_4() const { return ___IsKnown_4; }
	inline bool* get_address_of_IsKnown_4() { return &___IsKnown_4; }
	inline void set_IsKnown_4(bool value)
	{
		___IsKnown_4 = value;
	}

	inline static int32_t get_offset_of__physicalTargetHeight_5() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ____physicalTargetHeight_5)); }
	inline float get__physicalTargetHeight_5() const { return ____physicalTargetHeight_5; }
	inline float* get_address_of__physicalTargetHeight_5() { return &____physicalTargetHeight_5; }
	inline void set__physicalTargetHeight_5(float value)
	{
		____physicalTargetHeight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOGNIZEDTARGET_T3529911668_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MATH_T1859187520_H
#define MATH_T1859187520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.Math
struct  Math_t1859187520  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATH_T1859187520_H
#ifndef BASEINPUT_2_T81206664_H
#define BASEINPUT_2_T81206664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>
struct  BaseInput_2_t81206664  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	String_t* ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	bool ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t81206664, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t81206664, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t81206664, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T81206664_H
#ifndef LOG_T2685642413_H
#define LOG_T2685642413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.Log
struct  Log_t2685642413  : public RuntimeObject
{
public:

public:
};

struct Log_t2685642413_StaticFields
{
public:
	// System.String Wikitude.Log::TAG
	String_t* ___TAG_0;

public:
	inline static int32_t get_offset_of_TAG_0() { return static_cast<int32_t>(offsetof(Log_t2685642413_StaticFields, ___TAG_0)); }
	inline String_t* get_TAG_0() const { return ___TAG_0; }
	inline String_t** get_address_of_TAG_0() { return &___TAG_0; }
	inline void set_TAG_0(String_t* value)
	{
		___TAG_0 = value;
		Il2CppCodeGenWriteBarrier((&___TAG_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_T2685642413_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef TARGETCOLLECTIONRESOURCE_T66041399_H
#define TARGETCOLLECTIONRESOURCE_T66041399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetCollectionResource
struct  TargetCollectionResource_t66041399  : public TargetSource_t2046190744
{
public:
	// System.String Wikitude.TargetCollectionResource::_targetPath
	String_t* ____targetPath_2;
	// System.Boolean Wikitude.TargetCollectionResource::_useCustomURL
	bool ____useCustomURL_3;
	// Wikitude.TargetCollectionResource/OnFinishLoadingEvent Wikitude.TargetCollectionResource::OnFinishLoading
	OnFinishLoadingEvent_t3173959162 * ___OnFinishLoading_4;
	// Wikitude.TargetCollectionResource/OnErrorLoadingEvent Wikitude.TargetCollectionResource::OnErrorLoading
	OnErrorLoadingEvent_t3790443523 * ___OnErrorLoading_5;

public:
	inline static int32_t get_offset_of__targetPath_2() { return static_cast<int32_t>(offsetof(TargetCollectionResource_t66041399, ____targetPath_2)); }
	inline String_t* get__targetPath_2() const { return ____targetPath_2; }
	inline String_t** get_address_of__targetPath_2() { return &____targetPath_2; }
	inline void set__targetPath_2(String_t* value)
	{
		____targetPath_2 = value;
		Il2CppCodeGenWriteBarrier((&____targetPath_2), value);
	}

	inline static int32_t get_offset_of__useCustomURL_3() { return static_cast<int32_t>(offsetof(TargetCollectionResource_t66041399, ____useCustomURL_3)); }
	inline bool get__useCustomURL_3() const { return ____useCustomURL_3; }
	inline bool* get_address_of__useCustomURL_3() { return &____useCustomURL_3; }
	inline void set__useCustomURL_3(bool value)
	{
		____useCustomURL_3 = value;
	}

	inline static int32_t get_offset_of_OnFinishLoading_4() { return static_cast<int32_t>(offsetof(TargetCollectionResource_t66041399, ___OnFinishLoading_4)); }
	inline OnFinishLoadingEvent_t3173959162 * get_OnFinishLoading_4() const { return ___OnFinishLoading_4; }
	inline OnFinishLoadingEvent_t3173959162 ** get_address_of_OnFinishLoading_4() { return &___OnFinishLoading_4; }
	inline void set_OnFinishLoading_4(OnFinishLoadingEvent_t3173959162 * value)
	{
		___OnFinishLoading_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinishLoading_4), value);
	}

	inline static int32_t get_offset_of_OnErrorLoading_5() { return static_cast<int32_t>(offsetof(TargetCollectionResource_t66041399, ___OnErrorLoading_5)); }
	inline OnErrorLoadingEvent_t3790443523 * get_OnErrorLoading_5() const { return ___OnErrorLoading_5; }
	inline OnErrorLoadingEvent_t3790443523 ** get_address_of_OnErrorLoading_5() { return &___OnErrorLoading_5; }
	inline void set_OnErrorLoading_5(OnErrorLoadingEvent_t3790443523 * value)
	{
		___OnErrorLoading_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnErrorLoading_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETCOLLECTIONRESOURCE_T66041399_H
#ifndef INSTANTTARGET_T3313867941_H
#define INSTANTTARGET_T3313867941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTarget
struct  InstantTarget_t3313867941  : public RecognizedTarget_t3529911668
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTTARGET_T3313867941_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MOUSEBUTTONINPUT_T2868216434_H
#define MOUSEBUTTONINPUT_T2868216434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/MouseButtonInput
struct  MouseButtonInput_t2868216434  : public BaseInput_2_t3479630992
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINPUT_T2868216434_H
#ifndef AXISINPUT_T802457650_H
#define AXISINPUT_T802457650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/AxisInput
struct  AxisInput_t802457650  : public BaseInput_2_t1381185473
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUT_T802457650_H
#ifndef BUTTONINPUT_T2818951903_H
#define BUTTONINPUT_T2818951903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/ButtonInput
struct  ButtonInput_t2818951903  : public BaseInput_2_t81206664
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUT_T2818951903_H
#ifndef UNITYEVENT_2_T1217887265_H
#define UNITYEVENT_2_T1217887265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.String>
struct  UnityEvent_2_t1217887265  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1217887265, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1217887265_H
#ifndef U24ARRAYTYPEU3D64_T498138225_H
#define U24ARRAYTYPEU3D64_T498138225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t498138225 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t498138225__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D64_T498138225_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RANGEWITHSTEPATTRIBUTE_T2191810823_H
#define RANGEWITHSTEPATTRIBUTE_T2191810823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.RangeWithStepAttribute
struct  RangeWithStepAttribute_t2191810823  : public PropertyAttribute_t3677895545
{
public:
	// System.Int32 FlatLighting.RangeWithStepAttribute::min
	int32_t ___min_0;
	// System.Int32 FlatLighting.RangeWithStepAttribute::max
	int32_t ___max_1;
	// System.Int32 FlatLighting.RangeWithStepAttribute::step
	int32_t ___step_2;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeWithStepAttribute_t2191810823, ___min_0)); }
	inline int32_t get_min_0() const { return ___min_0; }
	inline int32_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int32_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeWithStepAttribute_t2191810823, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_step_2() { return static_cast<int32_t>(offsetof(RangeWithStepAttribute_t2191810823, ___step_2)); }
	inline int32_t get_step_2() const { return ___step_2; }
	inline int32_t* get_address_of_step_2() { return &___step_2; }
	inline void set_step_2(int32_t value)
	{
		___step_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEWITHSTEPATTRIBUTE_T2191810823_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef U3CSCALEOVERTIMEU3EC__ITERATOR0_T1452294761_H
#define U3CSCALEOVERTIMEU3EC__ITERATOR0_T1452294761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<ScaleOverTime>c__Iterator0
struct  U3CScaleOverTimeU3Ec__Iterator0_t1452294761  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 GameManager/<ScaleOverTime>c__Iterator0::<originalScale>__0
	Vector3_t3722313464  ___U3CoriginalScaleU3E__0_0;
	// UnityEngine.Vector3 GameManager/<ScaleOverTime>c__Iterator0::<destinationScale>__0
	Vector3_t3722313464  ___U3CdestinationScaleU3E__0_1;
	// System.Single GameManager/<ScaleOverTime>c__Iterator0::<currentTime>__0
	float ___U3CcurrentTimeU3E__0_2;
	// System.Single GameManager/<ScaleOverTime>c__Iterator0::time
	float ___time_3;
	// GameManager GameManager/<ScaleOverTime>c__Iterator0::$this
	GameManager_t1536523654 * ___U24this_4;
	// System.Object GameManager/<ScaleOverTime>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean GameManager/<ScaleOverTime>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 GameManager/<ScaleOverTime>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CoriginalScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U3CoriginalScaleU3E__0_0)); }
	inline Vector3_t3722313464  get_U3CoriginalScaleU3E__0_0() const { return ___U3CoriginalScaleU3E__0_0; }
	inline Vector3_t3722313464 * get_address_of_U3CoriginalScaleU3E__0_0() { return &___U3CoriginalScaleU3E__0_0; }
	inline void set_U3CoriginalScaleU3E__0_0(Vector3_t3722313464  value)
	{
		___U3CoriginalScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdestinationScaleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U3CdestinationScaleU3E__0_1)); }
	inline Vector3_t3722313464  get_U3CdestinationScaleU3E__0_1() const { return ___U3CdestinationScaleU3E__0_1; }
	inline Vector3_t3722313464 * get_address_of_U3CdestinationScaleU3E__0_1() { return &___U3CdestinationScaleU3E__0_1; }
	inline void set_U3CdestinationScaleU3E__0_1(Vector3_t3722313464  value)
	{
		___U3CdestinationScaleU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E__0_2() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U3CcurrentTimeU3E__0_2)); }
	inline float get_U3CcurrentTimeU3E__0_2() const { return ___U3CcurrentTimeU3E__0_2; }
	inline float* get_address_of_U3CcurrentTimeU3E__0_2() { return &___U3CcurrentTimeU3E__0_2; }
	inline void set_U3CcurrentTimeU3E__0_2(float value)
	{
		___U3CcurrentTimeU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___time_3)); }
	inline float get_time_3() const { return ___time_3; }
	inline float* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(float value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U24this_4)); }
	inline GameManager_t1536523654 * get_U24this_4() const { return ___U24this_4; }
	inline GameManager_t1536523654 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GameManager_t1536523654 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CScaleOverTimeU3Ec__Iterator0_t1452294761, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALEOVERTIMEU3EC__ITERATOR0_T1452294761_H
#ifndef VECTORASSLIDERSATTRIBUTE_T2873057700_H
#define VECTORASSLIDERSATTRIBUTE_T2873057700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.VectorAsSlidersAttribute
struct  VectorAsSlidersAttribute_t2873057700  : public PropertyAttribute_t3677895545
{
public:
	// System.String FlatLighting.VectorAsSlidersAttribute::label
	String_t* ___label_0;
	// System.Single FlatLighting.VectorAsSlidersAttribute::min
	float ___min_1;
	// System.Single FlatLighting.VectorAsSlidersAttribute::max
	float ___max_2;
	// System.Int32 FlatLighting.VectorAsSlidersAttribute::dimensions
	int32_t ___dimensions_3;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t2873057700, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((&___label_0), value);
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t2873057700, ___min_1)); }
	inline float get_min_1() const { return ___min_1; }
	inline float* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(float value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t2873057700, ___max_2)); }
	inline float get_max_2() const { return ___max_2; }
	inline float* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(float value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_dimensions_3() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t2873057700, ___dimensions_3)); }
	inline int32_t get_dimensions_3() const { return ___dimensions_3; }
	inline int32_t* get_address_of_dimensions_3() { return &___dimensions_3; }
	inline void set_dimensions_3(int32_t value)
	{
		___dimensions_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORASSLIDERSATTRIBUTE_T2873057700_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef MIRAREMOTEERRORCODE_T1406415247_H
#define MIRAREMOTEERRORCODE_T1406415247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraRemoteErrorCode
struct  MiraRemoteErrorCode_t1406415247 
{
public:
	// System.Int32 MiraRemoteErrorCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MiraRemoteErrorCode_t1406415247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAREMOTEERRORCODE_T1406415247_H
#ifndef COMPATIBILITYLEVEL_T1827897472_H
#define COMPATIBILITYLEVEL_T1827897472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeSDK/CompatibilityLevel
struct  CompatibilityLevel_t1827897472 
{
public:
	// System.Int32 Wikitude.WikitudeSDK/CompatibilityLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompatibilityLevel_t1827897472, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPATIBILITYLEVEL_T1827897472_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-5C26586BC03E63060E1A58A973915A014ECFCA38
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-CD81E62297C7ECE146ADA340591341414F9422DB
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3;

public:
	inline static int32_t get_offset_of_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0() const { return ___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0() { return &___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0; }
	inline void set_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1() const { return ___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1() { return &___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1; }
	inline void set_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2() const { return ___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2() { return &___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2; }
	inline void set_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3() const { return ___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3() { return &___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3; }
	inline void set_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef TARGETSOURCETYPE_T830527111_H
#define TARGETSOURCETYPE_T830527111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetSourceType
struct  TargetSourceType_t830527111 
{
public:
	// System.Int32 Wikitude.TargetSourceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetSourceType_t830527111, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETSOURCETYPE_T830527111_H
#ifndef TRANSFORMPROPERTIES_T2631539258_H
#define TRANSFORMPROPERTIES_T2631539258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformProperties
struct  TransformProperties_t2631539258 
{
public:
	// UnityEngine.Vector3 Wikitude.TransformProperties::Position
	Vector3_t3722313464  ___Position_0;
	// UnityEngine.Quaternion Wikitude.TransformProperties::Rotation
	Quaternion_t2301928331  ___Rotation_1;
	// UnityEngine.Vector3 Wikitude.TransformProperties::Scale
	Vector3_t3722313464  ___Scale_2;
	// System.Single Wikitude.TransformProperties::FieldOfView
	float ___FieldOfView_3;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___Position_0)); }
	inline Vector3_t3722313464  get_Position_0() const { return ___Position_0; }
	inline Vector3_t3722313464 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t3722313464  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___Rotation_1)); }
	inline Quaternion_t2301928331  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t2301928331  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___Scale_2)); }
	inline Vector3_t3722313464  get_Scale_2() const { return ___Scale_2; }
	inline Vector3_t3722313464 * get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(Vector3_t3722313464  value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_3() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___FieldOfView_3)); }
	inline float get_FieldOfView_3() const { return ___FieldOfView_3; }
	inline float* get_address_of_FieldOfView_3() { return &___FieldOfView_3; }
	inline void set_FieldOfView_3(float value)
	{
		___FieldOfView_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMPROPERTIES_T2631539258_H
#ifndef ONEXITFIELDOFVISIONEVENT_T2776302756_H
#define ONEXITFIELDOFVISIONEVENT_T2776302756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent
struct  OnExitFieldOfVisionEvent_t2776302756  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEXITFIELDOFVISIONEVENT_T2776302756_H
#ifndef INPUTSTATE_T3726306521_H
#define INPUTSTATE_T3726306521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/InputState
struct  InputState_t3726306521 
{
public:
	// System.Int32 SimpleInput/InputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputState_t3726306521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T3726306521_H
#ifndef UNITYVERSION_T2410471165_H
#define UNITYVERSION_T2410471165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityVersion
struct  UnityVersion_t2410471165 
{
public:
	// System.Int32 Wikitude.UnityVersion::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityVersion_t2410471165, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVERSION_T2410471165_H
#ifndef MOUSEBUTTON_T3761347860_H
#define MOUSEBUTTON_T3761347860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Touchpad/MouseButton
struct  MouseButton_t3761347860 
{
public:
	// System.Int32 SimpleInputNamespace.Touchpad/MouseButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseButton_t3761347860, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_T3761347860_H
#ifndef MOVEMENTAXES_T2821595006_H
#define MOVEMENTAXES_T2821595006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Joystick/MovementAxes
struct  MovementAxes_t2821595006 
{
public:
	// System.Int32 SimpleInputNamespace.Joystick/MovementAxes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementAxes_t2821595006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTAXES_T2821595006_H
#ifndef ONENTERFIELDOFVISIONEVENT_T2975486640_H
#define ONENTERFIELDOFVISIONEVENT_T2975486640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent
struct  OnEnterFieldOfVisionEvent_t2975486640  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENTERFIELDOFVISIONEVENT_T2975486640_H
#ifndef OBJECTTARGET_T688225044_H
#define OBJECTTARGET_T688225044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTarget
struct  ObjectTarget_t688225044  : public RecognizedTarget_t3529911668
{
public:
	// UnityEngine.Vector3 Wikitude.ObjectTarget::_scale
	Vector3_t3722313464  ____scale_6;

public:
	inline static int32_t get_offset_of__scale_6() { return static_cast<int32_t>(offsetof(ObjectTarget_t688225044, ____scale_6)); }
	inline Vector3_t3722313464  get__scale_6() const { return ____scale_6; }
	inline Vector3_t3722313464 * get_address_of__scale_6() { return &____scale_6; }
	inline void set__scale_6(Vector3_t3722313464  value)
	{
		____scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGET_T688225044_H
#ifndef IMAGETARGET_T2322341322_H
#define IMAGETARGET_T2322341322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTarget
struct  ImageTarget_t2322341322  : public RecognizedTarget_t3529911668
{
public:
	// UnityEngine.Vector2 Wikitude.ImageTarget::_scale
	Vector2_t2156229523  ____scale_6;

public:
	inline static int32_t get_offset_of__scale_6() { return static_cast<int32_t>(offsetof(ImageTarget_t2322341322, ____scale_6)); }
	inline Vector2_t2156229523  get__scale_6() const { return ____scale_6; }
	inline Vector2_t2156229523 * get_address_of__scale_6() { return &____scale_6; }
	inline void set__scale_6(Vector2_t2156229523  value)
	{
		____scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGET_T2322341322_H
#ifndef ONFINISHLOADINGEVENT_T3173959162_H
#define ONFINISHLOADINGEVENT_T3173959162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetCollectionResource/OnFinishLoadingEvent
struct  OnFinishLoadingEvent_t3173959162  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHLOADINGEVENT_T3173959162_H
#ifndef ONERRORLOADINGEVENT_T3790443523_H
#define ONERRORLOADINGEVENT_T3790443523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetCollectionResource/OnErrorLoadingEvent
struct  OnErrorLoadingEvent_t3790443523  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORLOADINGEVENT_T3790443523_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef MIRAREMOTEEXCEPTION_T3723149839_H
#define MIRAREMOTEEXCEPTION_T3723149839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraRemoteException
struct  MiraRemoteException_t3723149839  : public Exception_t
{
public:
	// MiraRemoteErrorCode MiraRemoteException::<errorCode>k__BackingField
	int32_t ___U3CerrorCodeU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CerrorCodeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MiraRemoteException_t3723149839, ___U3CerrorCodeU3Ek__BackingField_11)); }
	inline int32_t get_U3CerrorCodeU3Ek__BackingField_11() const { return ___U3CerrorCodeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CerrorCodeU3Ek__BackingField_11() { return &___U3CerrorCodeU3Ek__BackingField_11; }
	inline void set_U3CerrorCodeU3Ek__BackingField_11(int32_t value)
	{
		___U3CerrorCodeU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAREMOTEEXCEPTION_T3723149839_H
#ifndef BASEINPUT_2_T1022651380_H
#define BASEINPUT_2_T1022651380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>
struct  BaseInput_2_t1022651380  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	int32_t ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	bool ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t1022651380, ___m_key_0)); }
	inline int32_t get_m_key_0() const { return ___m_key_0; }
	inline int32_t* get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(int32_t value)
	{
		___m_key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t1022651380, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t1022651380, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T1022651380_H
#ifndef MOUSEBUTTON_T998287744_H
#define MOUSEBUTTON_T998287744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/MouseButton
struct  MouseButton_t998287744  : public RuntimeObject
{
public:
	// System.Int32 SimpleInput/MouseButton::button
	int32_t ___button_0;
	// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput> SimpleInput/MouseButton::inputs
	List_1_t45323880 * ___inputs_1;
	// SimpleInput/InputState SimpleInput/MouseButton::state
	int32_t ___state_2;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(MouseButton_t998287744, ___button_0)); }
	inline int32_t get_button_0() const { return ___button_0; }
	inline int32_t* get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(int32_t value)
	{
		___button_0 = value;
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(MouseButton_t998287744, ___inputs_1)); }
	inline List_1_t45323880 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t45323880 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t45323880 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(MouseButton_t998287744, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_T998287744_H
#ifndef KEY_T3885074208_H
#define KEY_T3885074208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/Key
struct  Key_t3885074208  : public RuntimeObject
{
public:
	// UnityEngine.KeyCode SimpleInput/Key::key
	int32_t ___key_0;
	// System.Collections.Generic.List`1<SimpleInput/KeyInput> SimpleInput/Key::inputs
	List_1_t570456766 * ___inputs_1;
	// SimpleInput/InputState SimpleInput/Key::state
	int32_t ___state_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(Key_t3885074208, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Key_t3885074208, ___inputs_1)); }
	inline List_1_t570456766 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t570456766 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t570456766 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Key_t3885074208, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEY_T3885074208_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BUTTON_T2930970180_H
#define BUTTON_T2930970180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/Button
struct  Button_t2930970180  : public RuntimeObject
{
public:
	// System.String SimpleInput/Button::button
	String_t* ___button_0;
	// System.Collections.Generic.List`1<SimpleInput/ButtonInput> SimpleInput/Button::inputs
	List_1_t4291026645 * ___inputs_1;
	// SimpleInput/InputState SimpleInput/Button::state
	int32_t ___state_2;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(Button_t2930970180, ___button_0)); }
	inline String_t* get_button_0() const { return ___button_0; }
	inline String_t** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(String_t* value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier((&___button_0), value);
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Button_t2930970180, ___inputs_1)); }
	inline List_1_t4291026645 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t4291026645 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t4291026645 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Button_t2930970180, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T2930970180_H
#ifndef MRDISCOVEREDREMOTECALLBACK_T179447917_H
#define MRDISCOVEREDREMOTECALLBACK_T179447917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRDiscoveredRemoteCallback
struct  MRDiscoveredRemoteCallback_t179447917  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRDISCOVEREDREMOTECALLBACK_T179447917_H
#ifndef MRFLOATCALLBACK_T806206182_H
#define MRFLOATCALLBACK_T806206182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRFloatCallback
struct  MRFloatCallback_t806206182  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRFLOATCALLBACK_T806206182_H
#ifndef MRERRORCALLBACK_T1000734509_H
#define MRERRORCALLBACK_T1000734509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRErrorCallback
struct  MRErrorCallback_t1000734509  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRERRORCALLBACK_T1000734509_H
#ifndef MRINTCALLBACK_T3089559991_H
#define MRINTCALLBACK_T3089559991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRIntCallback
struct  MRIntCallback_t3089559991  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRINTCALLBACK_T3089559991_H
#ifndef MREMPTYCALLBACK_T625280627_H
#define MREMPTYCALLBACK_T625280627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MREmptyCallback
struct  MREmptyCallback_t625280627  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MREMPTYCALLBACK_T625280627_H
#ifndef MRBOOLCALLBACK_T3968806430_H
#define MRBOOLCALLBACK_T3968806430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRBoolCallback
struct  MRBoolCallback_t3968806430  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRBOOLCALLBACK_T3968806430_H
#ifndef MRCONNECTEDREMOTECALLBACK_T1852255652_H
#define MRCONNECTEDREMOTECALLBACK_T1852255652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRConnectedRemoteCallback
struct  MRConnectedRemoteCallback_t1852255652  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRCONNECTEDREMOTECALLBACK_T1852255652_H
#ifndef UPDATECALLBACK_T3991193291_H
#define UPDATECALLBACK_T3991193291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/UpdateCallback
struct  UpdateCallback_t3991193291  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECALLBACK_T3991193291_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef KEYINPUT_T3393349320_H
#define KEYINPUT_T3393349320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/KeyInput
struct  KeyInput_t3393349320  : public BaseInput_2_t1022651380
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINPUT_T3393349320_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AXISINPUTMOUSE_T3174651034_H
#define AXISINPUTMOUSE_T3174651034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.AxisInputMouse
struct  AxisInputMouse_t3174651034  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputMouse::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputMouse::yAxis
	AxisInput_t802457650 * ___yAxis_3;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(AxisInputMouse_t3174651034, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(AxisInputMouse_t3174651034, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTMOUSE_T3174651034_H
#ifndef AXISINPUTKEYBOARD_T1690261069_H
#define AXISINPUTKEYBOARD_T1690261069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.AxisInputKeyboard
struct  AxisInputKeyboard_t1690261069  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.AxisInputKeyboard::key
	int32_t ___key_2;
	// System.Single SimpleInputNamespace.AxisInputKeyboard::value
	float ___value_3;
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputKeyboard::axis
	AxisInput_t802457650 * ___axis_4;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(AxisInputKeyboard_t1690261069, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(AxisInputKeyboard_t1690261069, ___value_3)); }
	inline float get_value_3() const { return ___value_3; }
	inline float* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(float value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(AxisInputKeyboard_t1690261069, ___axis_4)); }
	inline AxisInput_t802457650 * get_axis_4() const { return ___axis_4; }
	inline AxisInput_t802457650 ** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(AxisInput_t802457650 * value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTKEYBOARD_T1690261069_H
#ifndef JOYSTICK_T1225167045_H
#define JOYSTICK_T1225167045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Joystick
struct  Joystick_t1225167045  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.Joystick::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.Joystick::yAxis
	AxisInput_t802457650 * ___yAxis_3;
	// UnityEngine.RectTransform SimpleInputNamespace.Joystick::joystickTR
	RectTransform_t3704657025 * ___joystickTR_4;
	// UnityEngine.UI.Image SimpleInputNamespace.Joystick::background
	Image_t2670269651 * ___background_5;
	// UnityEngine.UI.Image SimpleInputNamespace.Joystick::thumb
	Image_t2670269651 * ___thumb_6;
	// UnityEngine.RectTransform SimpleInputNamespace.Joystick::thumbTR
	RectTransform_t3704657025 * ___thumbTR_7;
	// SimpleInputNamespace.Joystick/MovementAxes SimpleInputNamespace.Joystick::movementAxes
	int32_t ___movementAxes_8;
	// System.Single SimpleInputNamespace.Joystick::movementAreaRadius
	float ___movementAreaRadius_9;
	// System.Boolean SimpleInputNamespace.Joystick::isDynamicJoystick
	bool ___isDynamicJoystick_10;
	// UnityEngine.RectTransform SimpleInputNamespace.Joystick::dynamicJoystickMovementArea
	RectTransform_t3704657025 * ___dynamicJoystickMovementArea_11;
	// System.Boolean SimpleInputNamespace.Joystick::joystickHeld
	bool ___joystickHeld_12;
	// UnityEngine.Vector2 SimpleInputNamespace.Joystick::pointerInitialPos
	Vector2_t2156229523  ___pointerInitialPos_13;
	// System.Single SimpleInputNamespace.Joystick::_1OverMovementAreaRadius
	float ____1OverMovementAreaRadius_14;
	// System.Single SimpleInputNamespace.Joystick::movementAreaRadiusSqr
	float ___movementAreaRadiusSqr_15;
	// System.Single SimpleInputNamespace.Joystick::opacity
	float ___opacity_16;
	// UnityEngine.Vector2 SimpleInputNamespace.Joystick::m_value
	Vector2_t2156229523  ___m_value_17;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}

	inline static int32_t get_offset_of_joystickTR_4() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___joystickTR_4)); }
	inline RectTransform_t3704657025 * get_joystickTR_4() const { return ___joystickTR_4; }
	inline RectTransform_t3704657025 ** get_address_of_joystickTR_4() { return &___joystickTR_4; }
	inline void set_joystickTR_4(RectTransform_t3704657025 * value)
	{
		___joystickTR_4 = value;
		Il2CppCodeGenWriteBarrier((&___joystickTR_4), value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___background_5)); }
	inline Image_t2670269651 * get_background_5() const { return ___background_5; }
	inline Image_t2670269651 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(Image_t2670269651 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier((&___background_5), value);
	}

	inline static int32_t get_offset_of_thumb_6() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___thumb_6)); }
	inline Image_t2670269651 * get_thumb_6() const { return ___thumb_6; }
	inline Image_t2670269651 ** get_address_of_thumb_6() { return &___thumb_6; }
	inline void set_thumb_6(Image_t2670269651 * value)
	{
		___thumb_6 = value;
		Il2CppCodeGenWriteBarrier((&___thumb_6), value);
	}

	inline static int32_t get_offset_of_thumbTR_7() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___thumbTR_7)); }
	inline RectTransform_t3704657025 * get_thumbTR_7() const { return ___thumbTR_7; }
	inline RectTransform_t3704657025 ** get_address_of_thumbTR_7() { return &___thumbTR_7; }
	inline void set_thumbTR_7(RectTransform_t3704657025 * value)
	{
		___thumbTR_7 = value;
		Il2CppCodeGenWriteBarrier((&___thumbTR_7), value);
	}

	inline static int32_t get_offset_of_movementAxes_8() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___movementAxes_8)); }
	inline int32_t get_movementAxes_8() const { return ___movementAxes_8; }
	inline int32_t* get_address_of_movementAxes_8() { return &___movementAxes_8; }
	inline void set_movementAxes_8(int32_t value)
	{
		___movementAxes_8 = value;
	}

	inline static int32_t get_offset_of_movementAreaRadius_9() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___movementAreaRadius_9)); }
	inline float get_movementAreaRadius_9() const { return ___movementAreaRadius_9; }
	inline float* get_address_of_movementAreaRadius_9() { return &___movementAreaRadius_9; }
	inline void set_movementAreaRadius_9(float value)
	{
		___movementAreaRadius_9 = value;
	}

	inline static int32_t get_offset_of_isDynamicJoystick_10() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___isDynamicJoystick_10)); }
	inline bool get_isDynamicJoystick_10() const { return ___isDynamicJoystick_10; }
	inline bool* get_address_of_isDynamicJoystick_10() { return &___isDynamicJoystick_10; }
	inline void set_isDynamicJoystick_10(bool value)
	{
		___isDynamicJoystick_10 = value;
	}

	inline static int32_t get_offset_of_dynamicJoystickMovementArea_11() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___dynamicJoystickMovementArea_11)); }
	inline RectTransform_t3704657025 * get_dynamicJoystickMovementArea_11() const { return ___dynamicJoystickMovementArea_11; }
	inline RectTransform_t3704657025 ** get_address_of_dynamicJoystickMovementArea_11() { return &___dynamicJoystickMovementArea_11; }
	inline void set_dynamicJoystickMovementArea_11(RectTransform_t3704657025 * value)
	{
		___dynamicJoystickMovementArea_11 = value;
		Il2CppCodeGenWriteBarrier((&___dynamicJoystickMovementArea_11), value);
	}

	inline static int32_t get_offset_of_joystickHeld_12() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___joystickHeld_12)); }
	inline bool get_joystickHeld_12() const { return ___joystickHeld_12; }
	inline bool* get_address_of_joystickHeld_12() { return &___joystickHeld_12; }
	inline void set_joystickHeld_12(bool value)
	{
		___joystickHeld_12 = value;
	}

	inline static int32_t get_offset_of_pointerInitialPos_13() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___pointerInitialPos_13)); }
	inline Vector2_t2156229523  get_pointerInitialPos_13() const { return ___pointerInitialPos_13; }
	inline Vector2_t2156229523 * get_address_of_pointerInitialPos_13() { return &___pointerInitialPos_13; }
	inline void set_pointerInitialPos_13(Vector2_t2156229523  value)
	{
		___pointerInitialPos_13 = value;
	}

	inline static int32_t get_offset_of__1OverMovementAreaRadius_14() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ____1OverMovementAreaRadius_14)); }
	inline float get__1OverMovementAreaRadius_14() const { return ____1OverMovementAreaRadius_14; }
	inline float* get_address_of__1OverMovementAreaRadius_14() { return &____1OverMovementAreaRadius_14; }
	inline void set__1OverMovementAreaRadius_14(float value)
	{
		____1OverMovementAreaRadius_14 = value;
	}

	inline static int32_t get_offset_of_movementAreaRadiusSqr_15() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___movementAreaRadiusSqr_15)); }
	inline float get_movementAreaRadiusSqr_15() const { return ___movementAreaRadiusSqr_15; }
	inline float* get_address_of_movementAreaRadiusSqr_15() { return &___movementAreaRadiusSqr_15; }
	inline void set_movementAreaRadiusSqr_15(float value)
	{
		___movementAreaRadiusSqr_15 = value;
	}

	inline static int32_t get_offset_of_opacity_16() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___opacity_16)); }
	inline float get_opacity_16() const { return ___opacity_16; }
	inline float* get_address_of_opacity_16() { return &___opacity_16; }
	inline void set_opacity_16(float value)
	{
		___opacity_16 = value;
	}

	inline static int32_t get_offset_of_m_value_17() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___m_value_17)); }
	inline Vector2_t2156229523  get_m_value_17() const { return ___m_value_17; }
	inline Vector2_t2156229523 * get_address_of_m_value_17() { return &___m_value_17; }
	inline void set_m_value_17(Vector2_t2156229523  value)
	{
		___m_value_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T1225167045_H
#ifndef LIGHTSOURCE_1_T4063250156_H
#define LIGHTSOURCE_1_T4063250156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.DirectionalLight>
struct  LightSource_1_t4063250156  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t4063250156, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t4063250156_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t1663991692 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t4063250156_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t4063250156_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t4063250156_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t4063250156_StaticFields, ___lights_5)); }
	inline LightBag_t1663991692 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t1663991692 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t1663991692 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T4063250156_H
#ifndef DPAD_T1007306070_H
#define DPAD_T1007306070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Dpad
struct  Dpad_t1007306070  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.Dpad::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.Dpad::yAxis
	AxisInput_t802457650 * ___yAxis_3;
	// UnityEngine.RectTransform SimpleInputNamespace.Dpad::rectTransform
	RectTransform_t3704657025 * ___rectTransform_4;
	// UnityEngine.UI.Graphic SimpleInputNamespace.Dpad::background
	Graphic_t1660335611 * ___background_5;
	// UnityEngine.Vector2 SimpleInputNamespace.Dpad::m_value
	Vector2_t2156229523  ___m_value_6;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___rectTransform_4)); }
	inline RectTransform_t3704657025 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t3704657025 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_4), value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___background_5)); }
	inline Graphic_t1660335611 * get_background_5() const { return ___background_5; }
	inline Graphic_t1660335611 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(Graphic_t1660335611 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier((&___background_5), value);
	}

	inline static int32_t get_offset_of_m_value_6() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___m_value_6)); }
	inline Vector2_t2156229523  get_m_value_6() const { return ___m_value_6; }
	inline Vector2_t2156229523 * get_address_of_m_value_6() { return &___m_value_6; }
	inline void set_m_value_6(Vector2_t2156229523  value)
	{
		___m_value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPAD_T1007306070_H
#ifndef AXISINPUTUI_T3093488737_H
#define AXISINPUTUI_T3093488737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.AxisInputUI
struct  AxisInputUI_t3093488737  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SimpleInputNamespace.AxisInputUI::value
	float ___value_2;
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputUI::axis
	AxisInput_t802457650 * ___axis_3;

public:
	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(AxisInputUI_t3093488737, ___value_2)); }
	inline float get_value_2() const { return ___value_2; }
	inline float* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(float value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_axis_3() { return static_cast<int32_t>(offsetof(AxisInputUI_t3093488737, ___axis_3)); }
	inline AxisInput_t802457650 * get_axis_3() const { return ___axis_3; }
	inline AxisInput_t802457650 ** get_address_of_axis_3() { return &___axis_3; }
	inline void set_axis_3(AxisInput_t802457650 * value)
	{
		___axis_3 = value;
		Il2CppCodeGenWriteBarrier((&___axis_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTUI_T3093488737_H
#ifndef EXAMPLEPLAYERCONTROLLER_T740376480_H
#define EXAMPLEPLAYERCONTROLLER_T740376480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExamplePlayerController
struct  ExamplePlayerController_t740376480  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color ExamplePlayerController::materialColor
	Color_t2555686324  ___materialColor_2;
	// UnityEngine.Rigidbody ExamplePlayerController::m_rigidbody
	Rigidbody_t3916780224 * ___m_rigidbody_3;
	// System.String ExamplePlayerController::horizontalAxis
	String_t* ___horizontalAxis_4;
	// System.String ExamplePlayerController::verticalAxis
	String_t* ___verticalAxis_5;
	// System.String ExamplePlayerController::jumpButton
	String_t* ___jumpButton_6;
	// System.Single ExamplePlayerController::inputHorizontal
	float ___inputHorizontal_7;
	// System.Single ExamplePlayerController::inputVertical
	float ___inputVertical_8;

public:
	inline static int32_t get_offset_of_materialColor_2() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___materialColor_2)); }
	inline Color_t2555686324  get_materialColor_2() const { return ___materialColor_2; }
	inline Color_t2555686324 * get_address_of_materialColor_2() { return &___materialColor_2; }
	inline void set_materialColor_2(Color_t2555686324  value)
	{
		___materialColor_2 = value;
	}

	inline static int32_t get_offset_of_m_rigidbody_3() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___m_rigidbody_3)); }
	inline Rigidbody_t3916780224 * get_m_rigidbody_3() const { return ___m_rigidbody_3; }
	inline Rigidbody_t3916780224 ** get_address_of_m_rigidbody_3() { return &___m_rigidbody_3; }
	inline void set_m_rigidbody_3(Rigidbody_t3916780224 * value)
	{
		___m_rigidbody_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_rigidbody_3), value);
	}

	inline static int32_t get_offset_of_horizontalAxis_4() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___horizontalAxis_4)); }
	inline String_t* get_horizontalAxis_4() const { return ___horizontalAxis_4; }
	inline String_t** get_address_of_horizontalAxis_4() { return &___horizontalAxis_4; }
	inline void set_horizontalAxis_4(String_t* value)
	{
		___horizontalAxis_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_4), value);
	}

	inline static int32_t get_offset_of_verticalAxis_5() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___verticalAxis_5)); }
	inline String_t* get_verticalAxis_5() const { return ___verticalAxis_5; }
	inline String_t** get_address_of_verticalAxis_5() { return &___verticalAxis_5; }
	inline void set_verticalAxis_5(String_t* value)
	{
		___verticalAxis_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_5), value);
	}

	inline static int32_t get_offset_of_jumpButton_6() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___jumpButton_6)); }
	inline String_t* get_jumpButton_6() const { return ___jumpButton_6; }
	inline String_t** get_address_of_jumpButton_6() { return &___jumpButton_6; }
	inline void set_jumpButton_6(String_t* value)
	{
		___jumpButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___jumpButton_6), value);
	}

	inline static int32_t get_offset_of_inputHorizontal_7() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___inputHorizontal_7)); }
	inline float get_inputHorizontal_7() const { return ___inputHorizontal_7; }
	inline float* get_address_of_inputHorizontal_7() { return &___inputHorizontal_7; }
	inline void set_inputHorizontal_7(float value)
	{
		___inputHorizontal_7 = value;
	}

	inline static int32_t get_offset_of_inputVertical_8() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___inputVertical_8)); }
	inline float get_inputVertical_8() const { return ___inputVertical_8; }
	inline float* get_address_of_inputVertical_8() { return &___inputVertical_8; }
	inline void set_inputVertical_8(float value)
	{
		___inputVertical_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEPLAYERCONTROLLER_T740376480_H
#ifndef LIGHTSOURCE_1_T517566159_H
#define LIGHTSOURCE_1_T517566159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.ShadowProjector>
struct  LightSource_1_t517566159  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t517566159, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t517566159_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t2413274991 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t517566159_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t517566159_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t517566159_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t517566159_StaticFields, ___lights_5)); }
	inline LightBag_t2413274991 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t2413274991 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t2413274991 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T517566159_H
#ifndef LIGHTSOURCE_1_T2585415336_H
#define LIGHTSOURCE_1_T2585415336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.PointLight>
struct  LightSource_1_t2585415336  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t2585415336, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t2585415336_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t186156872 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t2585415336_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t2585415336_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t2585415336_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t2585415336_StaticFields, ___lights_5)); }
	inline LightBag_t186156872 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t186156872 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t186156872 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T2585415336_H
#ifndef LIGHTSOURCE_1_T4135959262_H
#define LIGHTSOURCE_1_T4135959262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.SpotLight>
struct  LightSource_1_t4135959262  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t4135959262, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t4135959262_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t1736700798 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t4135959262_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t4135959262_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t4135959262_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t4135959262_StaticFields, ___lights_5)); }
	inline LightBag_t1736700798 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t1736700798 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t1736700798 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T4135959262_H
#ifndef SINGLETON_1_T2713370399_H
#define SINGLETON_1_T2713370399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<GameManager>
struct  Singleton_1_t2713370399  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2713370399_StaticFields
{
public:
	// T Singleton`1::instance
	GameManager_t1536523654 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2713370399_StaticFields, ___instance_2)); }
	inline GameManager_t1536523654 * get_instance_2() const { return ___instance_2; }
	inline GameManager_t1536523654 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GameManager_t1536523654 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2713370399_H
#ifndef TRANSFORMOVERRIDE_T3828400655_H
#define TRANSFORMOVERRIDE_T3828400655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformOverride
struct  TransformOverride_t3828400655  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMOVERRIDE_T3828400655_H
#ifndef TRACKABLEBEHAVIOUR_T2469814643_H
#define TRACKABLEBEHAVIOUR_T2469814643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour
struct  TrackableBehaviour_t2469814643  : public MonoBehaviour_t3962482529
{
public:
	// System.String Wikitude.TrackableBehaviour::_targetPattern
	String_t* ____targetPattern_2;
	// System.Text.RegularExpressions.Regex Wikitude.TrackableBehaviour::_targetPatternRegex
	Regex_t3657309853 * ____targetPatternRegex_3;
	// System.Boolean Wikitude.TrackableBehaviour::_isKnown
	bool ____isKnown_4;
	// System.String Wikitude.TrackableBehaviour::_currentTargetName
	String_t* ____currentTargetName_5;
	// System.Boolean Wikitude.TrackableBehaviour::_extendedTracking
	bool ____extendedTracking_6;
	// System.String[] Wikitude.TrackableBehaviour::_targetsForExtendedTracking
	StringU5BU5D_t1281789340* ____targetsForExtendedTracking_7;
	// System.Boolean Wikitude.TrackableBehaviour::_autoToggleVisibility
	bool ____autoToggleVisibility_8;
	// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent Wikitude.TrackableBehaviour::OnEnterFieldOfVision
	OnEnterFieldOfVisionEvent_t2975486640 * ___OnEnterFieldOfVision_9;
	// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent Wikitude.TrackableBehaviour::OnExitFieldOfVision
	OnExitFieldOfVisionEvent_t2776302756 * ___OnExitFieldOfVision_10;
	// System.Boolean Wikitude.TrackableBehaviour::_eventsFoldout
	bool ____eventsFoldout_11;
	// System.Boolean Wikitude.TrackableBehaviour::_registeredToTracker
	bool ____registeredToTracker_12;
	// UnityEngine.Texture2D Wikitude.TrackableBehaviour::_preview
	Texture2D_t3840446185 * ____preview_13;
	// UnityEngine.Material Wikitude.TrackableBehaviour::_previewMaterial
	Material_t340375123 * ____previewMaterial_14;
	// UnityEngine.Mesh Wikitude.TrackableBehaviour::_previewMesh
	Mesh_t3648964284 * ____previewMesh_15;

public:
	inline static int32_t get_offset_of__targetPattern_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____targetPattern_2)); }
	inline String_t* get__targetPattern_2() const { return ____targetPattern_2; }
	inline String_t** get_address_of__targetPattern_2() { return &____targetPattern_2; }
	inline void set__targetPattern_2(String_t* value)
	{
		____targetPattern_2 = value;
		Il2CppCodeGenWriteBarrier((&____targetPattern_2), value);
	}

	inline static int32_t get_offset_of__targetPatternRegex_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____targetPatternRegex_3)); }
	inline Regex_t3657309853 * get__targetPatternRegex_3() const { return ____targetPatternRegex_3; }
	inline Regex_t3657309853 ** get_address_of__targetPatternRegex_3() { return &____targetPatternRegex_3; }
	inline void set__targetPatternRegex_3(Regex_t3657309853 * value)
	{
		____targetPatternRegex_3 = value;
		Il2CppCodeGenWriteBarrier((&____targetPatternRegex_3), value);
	}

	inline static int32_t get_offset_of__isKnown_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____isKnown_4)); }
	inline bool get__isKnown_4() const { return ____isKnown_4; }
	inline bool* get_address_of__isKnown_4() { return &____isKnown_4; }
	inline void set__isKnown_4(bool value)
	{
		____isKnown_4 = value;
	}

	inline static int32_t get_offset_of__currentTargetName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____currentTargetName_5)); }
	inline String_t* get__currentTargetName_5() const { return ____currentTargetName_5; }
	inline String_t** get_address_of__currentTargetName_5() { return &____currentTargetName_5; }
	inline void set__currentTargetName_5(String_t* value)
	{
		____currentTargetName_5 = value;
		Il2CppCodeGenWriteBarrier((&____currentTargetName_5), value);
	}

	inline static int32_t get_offset_of__extendedTracking_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____extendedTracking_6)); }
	inline bool get__extendedTracking_6() const { return ____extendedTracking_6; }
	inline bool* get_address_of__extendedTracking_6() { return &____extendedTracking_6; }
	inline void set__extendedTracking_6(bool value)
	{
		____extendedTracking_6 = value;
	}

	inline static int32_t get_offset_of__targetsForExtendedTracking_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____targetsForExtendedTracking_7)); }
	inline StringU5BU5D_t1281789340* get__targetsForExtendedTracking_7() const { return ____targetsForExtendedTracking_7; }
	inline StringU5BU5D_t1281789340** get_address_of__targetsForExtendedTracking_7() { return &____targetsForExtendedTracking_7; }
	inline void set__targetsForExtendedTracking_7(StringU5BU5D_t1281789340* value)
	{
		____targetsForExtendedTracking_7 = value;
		Il2CppCodeGenWriteBarrier((&____targetsForExtendedTracking_7), value);
	}

	inline static int32_t get_offset_of__autoToggleVisibility_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____autoToggleVisibility_8)); }
	inline bool get__autoToggleVisibility_8() const { return ____autoToggleVisibility_8; }
	inline bool* get_address_of__autoToggleVisibility_8() { return &____autoToggleVisibility_8; }
	inline void set__autoToggleVisibility_8(bool value)
	{
		____autoToggleVisibility_8 = value;
	}

	inline static int32_t get_offset_of_OnEnterFieldOfVision_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ___OnEnterFieldOfVision_9)); }
	inline OnEnterFieldOfVisionEvent_t2975486640 * get_OnEnterFieldOfVision_9() const { return ___OnEnterFieldOfVision_9; }
	inline OnEnterFieldOfVisionEvent_t2975486640 ** get_address_of_OnEnterFieldOfVision_9() { return &___OnEnterFieldOfVision_9; }
	inline void set_OnEnterFieldOfVision_9(OnEnterFieldOfVisionEvent_t2975486640 * value)
	{
		___OnEnterFieldOfVision_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnterFieldOfVision_9), value);
	}

	inline static int32_t get_offset_of_OnExitFieldOfVision_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ___OnExitFieldOfVision_10)); }
	inline OnExitFieldOfVisionEvent_t2776302756 * get_OnExitFieldOfVision_10() const { return ___OnExitFieldOfVision_10; }
	inline OnExitFieldOfVisionEvent_t2776302756 ** get_address_of_OnExitFieldOfVision_10() { return &___OnExitFieldOfVision_10; }
	inline void set_OnExitFieldOfVision_10(OnExitFieldOfVisionEvent_t2776302756 * value)
	{
		___OnExitFieldOfVision_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnExitFieldOfVision_10), value);
	}

	inline static int32_t get_offset_of__eventsFoldout_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____eventsFoldout_11)); }
	inline bool get__eventsFoldout_11() const { return ____eventsFoldout_11; }
	inline bool* get_address_of__eventsFoldout_11() { return &____eventsFoldout_11; }
	inline void set__eventsFoldout_11(bool value)
	{
		____eventsFoldout_11 = value;
	}

	inline static int32_t get_offset_of__registeredToTracker_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____registeredToTracker_12)); }
	inline bool get__registeredToTracker_12() const { return ____registeredToTracker_12; }
	inline bool* get_address_of__registeredToTracker_12() { return &____registeredToTracker_12; }
	inline void set__registeredToTracker_12(bool value)
	{
		____registeredToTracker_12 = value;
	}

	inline static int32_t get_offset_of__preview_13() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____preview_13)); }
	inline Texture2D_t3840446185 * get__preview_13() const { return ____preview_13; }
	inline Texture2D_t3840446185 ** get_address_of__preview_13() { return &____preview_13; }
	inline void set__preview_13(Texture2D_t3840446185 * value)
	{
		____preview_13 = value;
		Il2CppCodeGenWriteBarrier((&____preview_13), value);
	}

	inline static int32_t get_offset_of__previewMaterial_14() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____previewMaterial_14)); }
	inline Material_t340375123 * get__previewMaterial_14() const { return ____previewMaterial_14; }
	inline Material_t340375123 ** get_address_of__previewMaterial_14() { return &____previewMaterial_14; }
	inline void set__previewMaterial_14(Material_t340375123 * value)
	{
		____previewMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&____previewMaterial_14), value);
	}

	inline static int32_t get_offset_of__previewMesh_15() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____previewMesh_15)); }
	inline Mesh_t3648964284 * get__previewMesh_15() const { return ____previewMesh_15; }
	inline Mesh_t3648964284 ** get_address_of__previewMesh_15() { return &____previewMesh_15; }
	inline void set__previewMesh_15(Mesh_t3648964284 * value)
	{
		____previewMesh_15 = value;
		Il2CppCodeGenWriteBarrier((&____previewMesh_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T2469814643_H
#ifndef SINGLETON_1_T149390147_H
#define SINGLETON_1_T149390147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<AudioManager>
struct  Singleton_1_t149390147  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t149390147_StaticFields
{
public:
	// T Singleton`1::instance
	AudioManager_t3267510698 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t149390147_StaticFields, ___instance_2)); }
	inline AudioManager_t3267510698 * get_instance_2() const { return ___instance_2; }
	inline AudioManager_t3267510698 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AudioManager_t3267510698 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T149390147_H
#ifndef STEERINGWHEEL_T1692308694_H
#define STEERINGWHEEL_T1692308694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.SteeringWheel
struct  SteeringWheel_t1692308694  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.SteeringWheel::axis
	AxisInput_t802457650 * ___axis_2;
	// UnityEngine.UI.Graphic SimpleInputNamespace.SteeringWheel::wheel
	Graphic_t1660335611 * ___wheel_3;
	// UnityEngine.RectTransform SimpleInputNamespace.SteeringWheel::wheelTR
	RectTransform_t3704657025 * ___wheelTR_4;
	// UnityEngine.Vector2 SimpleInputNamespace.SteeringWheel::centerPoint
	Vector2_t2156229523  ___centerPoint_5;
	// System.Single SimpleInputNamespace.SteeringWheel::maximumSteeringAngle
	float ___maximumSteeringAngle_6;
	// System.Single SimpleInputNamespace.SteeringWheel::wheelReleasedSpeed
	float ___wheelReleasedSpeed_7;
	// System.Single SimpleInputNamespace.SteeringWheel::wheelAngle
	float ___wheelAngle_8;
	// System.Single SimpleInputNamespace.SteeringWheel::wheelPrevAngle
	float ___wheelPrevAngle_9;
	// System.Boolean SimpleInputNamespace.SteeringWheel::wheelBeingHeld
	bool ___wheelBeingHeld_10;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___axis_2)); }
	inline AxisInput_t802457650 * get_axis_2() const { return ___axis_2; }
	inline AxisInput_t802457650 ** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(AxisInput_t802457650 * value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}

	inline static int32_t get_offset_of_wheel_3() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheel_3)); }
	inline Graphic_t1660335611 * get_wheel_3() const { return ___wheel_3; }
	inline Graphic_t1660335611 ** get_address_of_wheel_3() { return &___wheel_3; }
	inline void set_wheel_3(Graphic_t1660335611 * value)
	{
		___wheel_3 = value;
		Il2CppCodeGenWriteBarrier((&___wheel_3), value);
	}

	inline static int32_t get_offset_of_wheelTR_4() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelTR_4)); }
	inline RectTransform_t3704657025 * get_wheelTR_4() const { return ___wheelTR_4; }
	inline RectTransform_t3704657025 ** get_address_of_wheelTR_4() { return &___wheelTR_4; }
	inline void set_wheelTR_4(RectTransform_t3704657025 * value)
	{
		___wheelTR_4 = value;
		Il2CppCodeGenWriteBarrier((&___wheelTR_4), value);
	}

	inline static int32_t get_offset_of_centerPoint_5() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___centerPoint_5)); }
	inline Vector2_t2156229523  get_centerPoint_5() const { return ___centerPoint_5; }
	inline Vector2_t2156229523 * get_address_of_centerPoint_5() { return &___centerPoint_5; }
	inline void set_centerPoint_5(Vector2_t2156229523  value)
	{
		___centerPoint_5 = value;
	}

	inline static int32_t get_offset_of_maximumSteeringAngle_6() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___maximumSteeringAngle_6)); }
	inline float get_maximumSteeringAngle_6() const { return ___maximumSteeringAngle_6; }
	inline float* get_address_of_maximumSteeringAngle_6() { return &___maximumSteeringAngle_6; }
	inline void set_maximumSteeringAngle_6(float value)
	{
		___maximumSteeringAngle_6 = value;
	}

	inline static int32_t get_offset_of_wheelReleasedSpeed_7() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelReleasedSpeed_7)); }
	inline float get_wheelReleasedSpeed_7() const { return ___wheelReleasedSpeed_7; }
	inline float* get_address_of_wheelReleasedSpeed_7() { return &___wheelReleasedSpeed_7; }
	inline void set_wheelReleasedSpeed_7(float value)
	{
		___wheelReleasedSpeed_7 = value;
	}

	inline static int32_t get_offset_of_wheelAngle_8() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelAngle_8)); }
	inline float get_wheelAngle_8() const { return ___wheelAngle_8; }
	inline float* get_address_of_wheelAngle_8() { return &___wheelAngle_8; }
	inline void set_wheelAngle_8(float value)
	{
		___wheelAngle_8 = value;
	}

	inline static int32_t get_offset_of_wheelPrevAngle_9() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelPrevAngle_9)); }
	inline float get_wheelPrevAngle_9() const { return ___wheelPrevAngle_9; }
	inline float* get_address_of_wheelPrevAngle_9() { return &___wheelPrevAngle_9; }
	inline void set_wheelPrevAngle_9(float value)
	{
		___wheelPrevAngle_9 = value;
	}

	inline static int32_t get_offset_of_wheelBeingHeld_10() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelBeingHeld_10)); }
	inline bool get_wheelBeingHeld_10() const { return ___wheelBeingHeld_10; }
	inline bool* get_address_of_wheelBeingHeld_10() { return &___wheelBeingHeld_10; }
	inline void set_wheelBeingHeld_10(bool value)
	{
		___wheelBeingHeld_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEERINGWHEEL_T1692308694_H
#ifndef PLANETSCRIPT_T2937373613_H
#define PLANETSCRIPT_T2937373613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlanetScript
struct  PlanetScript_t2937373613  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PlanetScript::gravity
	float ___gravity_2;

public:
	inline static int32_t get_offset_of_gravity_2() { return static_cast<int32_t>(offsetof(PlanetScript_t2937373613, ___gravity_2)); }
	inline float get_gravity_2() const { return ___gravity_2; }
	inline float* get_address_of_gravity_2() { return &___gravity_2; }
	inline void set_gravity_2(float value)
	{
		___gravity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETSCRIPT_T2937373613_H
#ifndef LOOPMANAGER_T2087799844_H
#define LOOPMANAGER_T2087799844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoopManager
struct  LoopManager_t2087799844  : public MonoBehaviour_t3962482529
{
public:
	// System.Single LoopManager::speed
	float ___speed_2;
	// UnityEngine.GameObject LoopManager::Sphere
	GameObject_t1113636619 * ___Sphere_3;
	// UnityEngine.GameObject LoopManager::StartPoint
	GameObject_t1113636619 * ___StartPoint_4;
	// System.Single LoopManager::SpawnTime
	float ___SpawnTime_5;
	// System.Single LoopManager::Cooldown
	float ___Cooldown_6;
	// System.Boolean LoopManager::StartSpawn
	bool ___StartSpawn_7;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(LoopManager_t2087799844, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_Sphere_3() { return static_cast<int32_t>(offsetof(LoopManager_t2087799844, ___Sphere_3)); }
	inline GameObject_t1113636619 * get_Sphere_3() const { return ___Sphere_3; }
	inline GameObject_t1113636619 ** get_address_of_Sphere_3() { return &___Sphere_3; }
	inline void set_Sphere_3(GameObject_t1113636619 * value)
	{
		___Sphere_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sphere_3), value);
	}

	inline static int32_t get_offset_of_StartPoint_4() { return static_cast<int32_t>(offsetof(LoopManager_t2087799844, ___StartPoint_4)); }
	inline GameObject_t1113636619 * get_StartPoint_4() const { return ___StartPoint_4; }
	inline GameObject_t1113636619 ** get_address_of_StartPoint_4() { return &___StartPoint_4; }
	inline void set_StartPoint_4(GameObject_t1113636619 * value)
	{
		___StartPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___StartPoint_4), value);
	}

	inline static int32_t get_offset_of_SpawnTime_5() { return static_cast<int32_t>(offsetof(LoopManager_t2087799844, ___SpawnTime_5)); }
	inline float get_SpawnTime_5() const { return ___SpawnTime_5; }
	inline float* get_address_of_SpawnTime_5() { return &___SpawnTime_5; }
	inline void set_SpawnTime_5(float value)
	{
		___SpawnTime_5 = value;
	}

	inline static int32_t get_offset_of_Cooldown_6() { return static_cast<int32_t>(offsetof(LoopManager_t2087799844, ___Cooldown_6)); }
	inline float get_Cooldown_6() const { return ___Cooldown_6; }
	inline float* get_address_of_Cooldown_6() { return &___Cooldown_6; }
	inline void set_Cooldown_6(float value)
	{
		___Cooldown_6 = value;
	}

	inline static int32_t get_offset_of_StartSpawn_7() { return static_cast<int32_t>(offsetof(LoopManager_t2087799844, ___StartSpawn_7)); }
	inline bool get_StartSpawn_7() const { return ___StartSpawn_7; }
	inline bool* get_address_of_StartSpawn_7() { return &___StartSpawn_7; }
	inline void set_StartSpawn_7(bool value)
	{
		___StartSpawn_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPMANAGER_T2087799844_H
#ifndef ACTIVETCELLSCRIPT_T3652656283_H
#define ACTIVETCELLSCRIPT_T3652656283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveTCellScript
struct  ActiveTCellScript_t3652656283  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ActiveTCellScript::ActivCell
	GameObject_t1113636619 * ___ActivCell_2;
	// UnityEngine.GameObject ActiveTCellScript::AntiGen
	GameObject_t1113636619 * ___AntiGen_3;

public:
	inline static int32_t get_offset_of_ActivCell_2() { return static_cast<int32_t>(offsetof(ActiveTCellScript_t3652656283, ___ActivCell_2)); }
	inline GameObject_t1113636619 * get_ActivCell_2() const { return ___ActivCell_2; }
	inline GameObject_t1113636619 ** get_address_of_ActivCell_2() { return &___ActivCell_2; }
	inline void set_ActivCell_2(GameObject_t1113636619 * value)
	{
		___ActivCell_2 = value;
		Il2CppCodeGenWriteBarrier((&___ActivCell_2), value);
	}

	inline static int32_t get_offset_of_AntiGen_3() { return static_cast<int32_t>(offsetof(ActiveTCellScript_t3652656283, ___AntiGen_3)); }
	inline GameObject_t1113636619 * get_AntiGen_3() const { return ___AntiGen_3; }
	inline GameObject_t1113636619 ** get_address_of_AntiGen_3() { return &___AntiGen_3; }
	inline void set_AntiGen_3(GameObject_t1113636619 * value)
	{
		___AntiGen_3 = value;
		Il2CppCodeGenWriteBarrier((&___AntiGen_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVETCELLSCRIPT_T3652656283_H
#ifndef ROTATE_T1850091912_H
#define ROTATE_T1850091912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotate
struct  Rotate_t1850091912  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera Rotate::cam
	Camera_t4157153871 * ___cam_2;
	// UnityEngine.GameObject Rotate::target
	GameObject_t1113636619 * ___target_3;
	// UnityEngine.Vector3 Rotate::dir
	Vector3_t3722313464  ___dir_4;
	// UnityEngine.Vector3 Rotate::dir2
	Vector3_t3722313464  ___dir2_5;

public:
	inline static int32_t get_offset_of_cam_2() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___cam_2)); }
	inline Camera_t4157153871 * get_cam_2() const { return ___cam_2; }
	inline Camera_t4157153871 ** get_address_of_cam_2() { return &___cam_2; }
	inline void set_cam_2(Camera_t4157153871 * value)
	{
		___cam_2 = value;
		Il2CppCodeGenWriteBarrier((&___cam_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___target_3)); }
	inline GameObject_t1113636619 * get_target_3() const { return ___target_3; }
	inline GameObject_t1113636619 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(GameObject_t1113636619 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_dir_4() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___dir_4)); }
	inline Vector3_t3722313464  get_dir_4() const { return ___dir_4; }
	inline Vector3_t3722313464 * get_address_of_dir_4() { return &___dir_4; }
	inline void set_dir_4(Vector3_t3722313464  value)
	{
		___dir_4 = value;
	}

	inline static int32_t get_offset_of_dir2_5() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___dir2_5)); }
	inline Vector3_t3722313464  get_dir2_5() const { return ___dir2_5; }
	inline Vector3_t3722313464 * get_address_of_dir2_5() { return &___dir2_5; }
	inline void set_dir2_5(Vector3_t3722313464  value)
	{
		___dir2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T1850091912_H
#ifndef PLAYERMOVEMENTSCRIPT_T179248481_H
#define PLAYERMOVEMENTSCRIPT_T179248481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMovementScript
struct  PlayerMovementScript_t179248481  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PlayerMovementScript::moveSpeed
	float ___moveSpeed_2;
	// UnityEngine.Transform PlayerMovementScript::TrackedItem
	Transform_t3600365921 * ___TrackedItem_3;
	// UnityEngine.Vector3 PlayerMovementScript::moveDirection
	Vector3_t3722313464  ___moveDirection_4;
	// UnityEngine.Transform PlayerMovementScript::Camera
	Transform_t3600365921 * ___Camera_5;
	// UnityEngine.Transform PlayerMovementScript::Player
	Transform_t3600365921 * ___Player_6;
	// UnityEngine.Transform PlayerMovementScript::scorePivot
	Transform_t3600365921 * ___scorePivot_7;
	// System.Single PlayerMovementScript::smooth
	float ___smooth_8;
	// System.String PlayerMovementScript::horizontalAxis
	String_t* ___horizontalAxis_9;
	// System.String PlayerMovementScript::verticalAxis
	String_t* ___verticalAxis_10;
	// System.Single PlayerMovementScript::inputHorizontal
	float ___inputHorizontal_11;
	// System.Single PlayerMovementScript::inputVertical
	float ___inputVertical_12;
	// UnityEngine.Vector3 PlayerMovementScript::dir
	Vector3_t3722313464  ___dir_13;
	// UnityEngine.Vector3 PlayerMovementScript::dir2
	Vector3_t3722313464  ___dir2_14;
	// AudioManager PlayerMovementScript::am
	AudioManager_t3267510698 * ___am_15;
	// GameManager PlayerMovementScript::gm
	GameManager_t1536523654 * ___gm_16;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_TrackedItem_3() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___TrackedItem_3)); }
	inline Transform_t3600365921 * get_TrackedItem_3() const { return ___TrackedItem_3; }
	inline Transform_t3600365921 ** get_address_of_TrackedItem_3() { return &___TrackedItem_3; }
	inline void set_TrackedItem_3(Transform_t3600365921 * value)
	{
		___TrackedItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___TrackedItem_3), value);
	}

	inline static int32_t get_offset_of_moveDirection_4() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___moveDirection_4)); }
	inline Vector3_t3722313464  get_moveDirection_4() const { return ___moveDirection_4; }
	inline Vector3_t3722313464 * get_address_of_moveDirection_4() { return &___moveDirection_4; }
	inline void set_moveDirection_4(Vector3_t3722313464  value)
	{
		___moveDirection_4 = value;
	}

	inline static int32_t get_offset_of_Camera_5() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___Camera_5)); }
	inline Transform_t3600365921 * get_Camera_5() const { return ___Camera_5; }
	inline Transform_t3600365921 ** get_address_of_Camera_5() { return &___Camera_5; }
	inline void set_Camera_5(Transform_t3600365921 * value)
	{
		___Camera_5 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_5), value);
	}

	inline static int32_t get_offset_of_Player_6() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___Player_6)); }
	inline Transform_t3600365921 * get_Player_6() const { return ___Player_6; }
	inline Transform_t3600365921 ** get_address_of_Player_6() { return &___Player_6; }
	inline void set_Player_6(Transform_t3600365921 * value)
	{
		___Player_6 = value;
		Il2CppCodeGenWriteBarrier((&___Player_6), value);
	}

	inline static int32_t get_offset_of_scorePivot_7() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___scorePivot_7)); }
	inline Transform_t3600365921 * get_scorePivot_7() const { return ___scorePivot_7; }
	inline Transform_t3600365921 ** get_address_of_scorePivot_7() { return &___scorePivot_7; }
	inline void set_scorePivot_7(Transform_t3600365921 * value)
	{
		___scorePivot_7 = value;
		Il2CppCodeGenWriteBarrier((&___scorePivot_7), value);
	}

	inline static int32_t get_offset_of_smooth_8() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___smooth_8)); }
	inline float get_smooth_8() const { return ___smooth_8; }
	inline float* get_address_of_smooth_8() { return &___smooth_8; }
	inline void set_smooth_8(float value)
	{
		___smooth_8 = value;
	}

	inline static int32_t get_offset_of_horizontalAxis_9() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___horizontalAxis_9)); }
	inline String_t* get_horizontalAxis_9() const { return ___horizontalAxis_9; }
	inline String_t** get_address_of_horizontalAxis_9() { return &___horizontalAxis_9; }
	inline void set_horizontalAxis_9(String_t* value)
	{
		___horizontalAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_9), value);
	}

	inline static int32_t get_offset_of_verticalAxis_10() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___verticalAxis_10)); }
	inline String_t* get_verticalAxis_10() const { return ___verticalAxis_10; }
	inline String_t** get_address_of_verticalAxis_10() { return &___verticalAxis_10; }
	inline void set_verticalAxis_10(String_t* value)
	{
		___verticalAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_10), value);
	}

	inline static int32_t get_offset_of_inputHorizontal_11() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___inputHorizontal_11)); }
	inline float get_inputHorizontal_11() const { return ___inputHorizontal_11; }
	inline float* get_address_of_inputHorizontal_11() { return &___inputHorizontal_11; }
	inline void set_inputHorizontal_11(float value)
	{
		___inputHorizontal_11 = value;
	}

	inline static int32_t get_offset_of_inputVertical_12() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___inputVertical_12)); }
	inline float get_inputVertical_12() const { return ___inputVertical_12; }
	inline float* get_address_of_inputVertical_12() { return &___inputVertical_12; }
	inline void set_inputVertical_12(float value)
	{
		___inputVertical_12 = value;
	}

	inline static int32_t get_offset_of_dir_13() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___dir_13)); }
	inline Vector3_t3722313464  get_dir_13() const { return ___dir_13; }
	inline Vector3_t3722313464 * get_address_of_dir_13() { return &___dir_13; }
	inline void set_dir_13(Vector3_t3722313464  value)
	{
		___dir_13 = value;
	}

	inline static int32_t get_offset_of_dir2_14() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___dir2_14)); }
	inline Vector3_t3722313464  get_dir2_14() const { return ___dir2_14; }
	inline Vector3_t3722313464 * get_address_of_dir2_14() { return &___dir2_14; }
	inline void set_dir2_14(Vector3_t3722313464  value)
	{
		___dir2_14 = value;
	}

	inline static int32_t get_offset_of_am_15() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___am_15)); }
	inline AudioManager_t3267510698 * get_am_15() const { return ___am_15; }
	inline AudioManager_t3267510698 ** get_address_of_am_15() { return &___am_15; }
	inline void set_am_15(AudioManager_t3267510698 * value)
	{
		___am_15 = value;
		Il2CppCodeGenWriteBarrier((&___am_15), value);
	}

	inline static int32_t get_offset_of_gm_16() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t179248481, ___gm_16)); }
	inline GameManager_t1536523654 * get_gm_16() const { return ___gm_16; }
	inline GameManager_t1536523654 ** get_address_of_gm_16() { return &___gm_16; }
	inline void set_gm_16(GameManager_t1536523654 * value)
	{
		___gm_16 = value;
		Il2CppCodeGenWriteBarrier((&___gm_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOVEMENTSCRIPT_T179248481_H
#ifndef PLAYERGRAVITYBODY_T2486658507_H
#define PLAYERGRAVITYBODY_T2486658507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerGravityBody
struct  PlayerGravityBody_t2486658507  : public MonoBehaviour_t3962482529
{
public:
	// PlanetScript PlayerGravityBody::attractorPlanet
	PlanetScript_t2937373613 * ___attractorPlanet_2;
	// UnityEngine.Transform PlayerGravityBody::playerTransform
	Transform_t3600365921 * ___playerTransform_3;

public:
	inline static int32_t get_offset_of_attractorPlanet_2() { return static_cast<int32_t>(offsetof(PlayerGravityBody_t2486658507, ___attractorPlanet_2)); }
	inline PlanetScript_t2937373613 * get_attractorPlanet_2() const { return ___attractorPlanet_2; }
	inline PlanetScript_t2937373613 ** get_address_of_attractorPlanet_2() { return &___attractorPlanet_2; }
	inline void set_attractorPlanet_2(PlanetScript_t2937373613 * value)
	{
		___attractorPlanet_2 = value;
		Il2CppCodeGenWriteBarrier((&___attractorPlanet_2), value);
	}

	inline static int32_t get_offset_of_playerTransform_3() { return static_cast<int32_t>(offsetof(PlayerGravityBody_t2486658507, ___playerTransform_3)); }
	inline Transform_t3600365921 * get_playerTransform_3() const { return ___playerTransform_3; }
	inline Transform_t3600365921 ** get_address_of_playerTransform_3() { return &___playerTransform_3; }
	inline void set_playerTransform_3(Transform_t3600365921 * value)
	{
		___playerTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerTransform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERGRAVITYBODY_T2486658507_H
#ifndef LIGHTINGMANAGER_T1042243518_H
#define LIGHTINGMANAGER_T1042243518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightingManager
struct  LightingManager_t1042243518  : public MonoBehaviour_t3962482529
{
public:
	// SceneLigtingSetup LightingManager::day
	SceneLigtingSetup_t3397062989 * ___day_2;
	// SceneLigtingSetup LightingManager::night
	SceneLigtingSetup_t3397062989 * ___night_3;
	// UnityEngine.GameObject LightingManager::root
	GameObject_t1113636619 * ___root_4;

public:
	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(LightingManager_t1042243518, ___day_2)); }
	inline SceneLigtingSetup_t3397062989 * get_day_2() const { return ___day_2; }
	inline SceneLigtingSetup_t3397062989 ** get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(SceneLigtingSetup_t3397062989 * value)
	{
		___day_2 = value;
		Il2CppCodeGenWriteBarrier((&___day_2), value);
	}

	inline static int32_t get_offset_of_night_3() { return static_cast<int32_t>(offsetof(LightingManager_t1042243518, ___night_3)); }
	inline SceneLigtingSetup_t3397062989 * get_night_3() const { return ___night_3; }
	inline SceneLigtingSetup_t3397062989 ** get_address_of_night_3() { return &___night_3; }
	inline void set_night_3(SceneLigtingSetup_t3397062989 * value)
	{
		___night_3 = value;
		Il2CppCodeGenWriteBarrier((&___night_3), value);
	}

	inline static int32_t get_offset_of_root_4() { return static_cast<int32_t>(offsetof(LightingManager_t1042243518, ___root_4)); }
	inline GameObject_t1113636619 * get_root_4() const { return ___root_4; }
	inline GameObject_t1113636619 ** get_address_of_root_4() { return &___root_4; }
	inline void set_root_4(GameObject_t1113636619 * value)
	{
		___root_4 = value;
		Il2CppCodeGenWriteBarrier((&___root_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTINGMANAGER_T1042243518_H
#ifndef MATERIALSWAPPER_T1465658300_H
#define MATERIALSWAPPER_T1465658300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialSwapper
struct  MaterialSwapper_t1465658300  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material[] MaterialSwapper::materials
	MaterialU5BU5D_t561872642* ___materials_2;
	// UnityEngine.GameObject MaterialSwapper::UnityLightsRoot
	GameObject_t1113636619 * ___UnityLightsRoot_3;
	// System.Int32 MaterialSwapper::UnityLightsMaterialIndex
	int32_t ___UnityLightsMaterialIndex_4;
	// UnityEngine.GameObject MaterialSwapper::FlatLightsRoot
	GameObject_t1113636619 * ___FlatLightsRoot_5;
	// System.Int32 MaterialSwapper::FlatLightsMaterialIndex
	int32_t ___FlatLightsMaterialIndex_6;
	// UnityEngine.Renderer MaterialSwapper::myRenderer
	Renderer_t2627027031 * ___myRenderer_7;

public:
	inline static int32_t get_offset_of_materials_2() { return static_cast<int32_t>(offsetof(MaterialSwapper_t1465658300, ___materials_2)); }
	inline MaterialU5BU5D_t561872642* get_materials_2() const { return ___materials_2; }
	inline MaterialU5BU5D_t561872642** get_address_of_materials_2() { return &___materials_2; }
	inline void set_materials_2(MaterialU5BU5D_t561872642* value)
	{
		___materials_2 = value;
		Il2CppCodeGenWriteBarrier((&___materials_2), value);
	}

	inline static int32_t get_offset_of_UnityLightsRoot_3() { return static_cast<int32_t>(offsetof(MaterialSwapper_t1465658300, ___UnityLightsRoot_3)); }
	inline GameObject_t1113636619 * get_UnityLightsRoot_3() const { return ___UnityLightsRoot_3; }
	inline GameObject_t1113636619 ** get_address_of_UnityLightsRoot_3() { return &___UnityLightsRoot_3; }
	inline void set_UnityLightsRoot_3(GameObject_t1113636619 * value)
	{
		___UnityLightsRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___UnityLightsRoot_3), value);
	}

	inline static int32_t get_offset_of_UnityLightsMaterialIndex_4() { return static_cast<int32_t>(offsetof(MaterialSwapper_t1465658300, ___UnityLightsMaterialIndex_4)); }
	inline int32_t get_UnityLightsMaterialIndex_4() const { return ___UnityLightsMaterialIndex_4; }
	inline int32_t* get_address_of_UnityLightsMaterialIndex_4() { return &___UnityLightsMaterialIndex_4; }
	inline void set_UnityLightsMaterialIndex_4(int32_t value)
	{
		___UnityLightsMaterialIndex_4 = value;
	}

	inline static int32_t get_offset_of_FlatLightsRoot_5() { return static_cast<int32_t>(offsetof(MaterialSwapper_t1465658300, ___FlatLightsRoot_5)); }
	inline GameObject_t1113636619 * get_FlatLightsRoot_5() const { return ___FlatLightsRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_FlatLightsRoot_5() { return &___FlatLightsRoot_5; }
	inline void set_FlatLightsRoot_5(GameObject_t1113636619 * value)
	{
		___FlatLightsRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightsRoot_5), value);
	}

	inline static int32_t get_offset_of_FlatLightsMaterialIndex_6() { return static_cast<int32_t>(offsetof(MaterialSwapper_t1465658300, ___FlatLightsMaterialIndex_6)); }
	inline int32_t get_FlatLightsMaterialIndex_6() const { return ___FlatLightsMaterialIndex_6; }
	inline int32_t* get_address_of_FlatLightsMaterialIndex_6() { return &___FlatLightsMaterialIndex_6; }
	inline void set_FlatLightsMaterialIndex_6(int32_t value)
	{
		___FlatLightsMaterialIndex_6 = value;
	}

	inline static int32_t get_offset_of_myRenderer_7() { return static_cast<int32_t>(offsetof(MaterialSwapper_t1465658300, ___myRenderer_7)); }
	inline Renderer_t2627027031 * get_myRenderer_7() const { return ___myRenderer_7; }
	inline Renderer_t2627027031 ** get_address_of_myRenderer_7() { return &___myRenderer_7; }
	inline void set_myRenderer_7(Renderer_t2627027031 * value)
	{
		___myRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___myRenderer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSWAPPER_T1465658300_H
#ifndef MOUSEORBIT_T2077911214_H
#define MOUSEORBIT_T2077911214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.MouseOrbit
struct  MouseOrbit_t2077911214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FlatLighting.MouseOrbit::target
	Transform_t3600365921 * ___target_2;
	// System.Single FlatLighting.MouseOrbit::yPosClamp
	float ___yPosClamp_3;
	// System.Single FlatLighting.MouseOrbit::targetFocusLerpSpeed
	float ___targetFocusLerpSpeed_4;
	// System.Single FlatLighting.MouseOrbit::initialDistance
	float ___initialDistance_5;
	// System.Single FlatLighting.MouseOrbit::smooth
	float ___smooth_6;
	// System.Single FlatLighting.MouseOrbit::xSpeed
	float ___xSpeed_7;
	// System.Single FlatLighting.MouseOrbit::ySpeed
	float ___ySpeed_8;
	// System.Single FlatLighting.MouseOrbit::wasdSpeed
	float ___wasdSpeed_9;
	// System.Single FlatLighting.MouseOrbit::wasdSmooth
	float ___wasdSmooth_10;
	// System.Single FlatLighting.MouseOrbit::panSpeed
	float ___panSpeed_11;
	// System.Single FlatLighting.MouseOrbit::zoomSmooth
	float ___zoomSmooth_12;
	// System.Single FlatLighting.MouseOrbit::minZoom
	float ___minZoom_13;
	// System.Single FlatLighting.MouseOrbit::maxZoom
	float ___maxZoom_14;
	// System.Single FlatLighting.MouseOrbit::mouseSensitivityScaler
	float ___mouseSensitivityScaler_15;
	// System.Single FlatLighting.MouseOrbit::scrollSensitivityScaler
	float ___scrollSensitivityScaler_16;
	// System.Single FlatLighting.MouseOrbit::distance
	float ___distance_17;
	// System.Single FlatLighting.MouseOrbit::yMaxLimit
	float ___yMaxLimit_18;
	// System.Single FlatLighting.MouseOrbit::yMinLimit
	float ___yMinLimit_19;
	// System.Single FlatLighting.MouseOrbit::smoothY
	float ___smoothY_20;
	// System.Single FlatLighting.MouseOrbit::smoothX
	float ___smoothX_21;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::movementPOS
	Vector3_t3722313464  ___movementPOS_22;
	// UnityEngine.Quaternion FlatLighting.MouseOrbit::rotation
	Quaternion_t2301928331  ___rotation_23;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::position
	Vector3_t3722313464  ___position_24;
	// System.Single FlatLighting.MouseOrbit::clickTimey
	float ___clickTimey_25;
	// UnityEngine.Transform FlatLighting.MouseOrbit::myTransform
	Transform_t3600365921 * ___myTransform_26;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::posToBe
	Vector3_t3722313464  ___posToBe_27;
	// System.Single FlatLighting.MouseOrbit::zoomSmoothDelegate
	float ___zoomSmoothDelegate_28;
	// System.Single FlatLighting.MouseOrbit::x
	float ___x_29;
	// System.Single FlatLighting.MouseOrbit::y
	float ___y_30;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::movementPosOffset
	Vector3_t3722313464  ___movementPosOffset_31;
	// System.Boolean FlatLighting.MouseOrbit::isOrthographic
	bool ___isOrthographic_32;
	// UnityEngine.Camera FlatLighting.MouseOrbit::myCamera
	Camera_t4157153871 * ___myCamera_33;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_yPosClamp_3() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___yPosClamp_3)); }
	inline float get_yPosClamp_3() const { return ___yPosClamp_3; }
	inline float* get_address_of_yPosClamp_3() { return &___yPosClamp_3; }
	inline void set_yPosClamp_3(float value)
	{
		___yPosClamp_3 = value;
	}

	inline static int32_t get_offset_of_targetFocusLerpSpeed_4() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___targetFocusLerpSpeed_4)); }
	inline float get_targetFocusLerpSpeed_4() const { return ___targetFocusLerpSpeed_4; }
	inline float* get_address_of_targetFocusLerpSpeed_4() { return &___targetFocusLerpSpeed_4; }
	inline void set_targetFocusLerpSpeed_4(float value)
	{
		___targetFocusLerpSpeed_4 = value;
	}

	inline static int32_t get_offset_of_initialDistance_5() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___initialDistance_5)); }
	inline float get_initialDistance_5() const { return ___initialDistance_5; }
	inline float* get_address_of_initialDistance_5() { return &___initialDistance_5; }
	inline void set_initialDistance_5(float value)
	{
		___initialDistance_5 = value;
	}

	inline static int32_t get_offset_of_smooth_6() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___smooth_6)); }
	inline float get_smooth_6() const { return ___smooth_6; }
	inline float* get_address_of_smooth_6() { return &___smooth_6; }
	inline void set_smooth_6(float value)
	{
		___smooth_6 = value;
	}

	inline static int32_t get_offset_of_xSpeed_7() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___xSpeed_7)); }
	inline float get_xSpeed_7() const { return ___xSpeed_7; }
	inline float* get_address_of_xSpeed_7() { return &___xSpeed_7; }
	inline void set_xSpeed_7(float value)
	{
		___xSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ySpeed_8() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___ySpeed_8)); }
	inline float get_ySpeed_8() const { return ___ySpeed_8; }
	inline float* get_address_of_ySpeed_8() { return &___ySpeed_8; }
	inline void set_ySpeed_8(float value)
	{
		___ySpeed_8 = value;
	}

	inline static int32_t get_offset_of_wasdSpeed_9() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___wasdSpeed_9)); }
	inline float get_wasdSpeed_9() const { return ___wasdSpeed_9; }
	inline float* get_address_of_wasdSpeed_9() { return &___wasdSpeed_9; }
	inline void set_wasdSpeed_9(float value)
	{
		___wasdSpeed_9 = value;
	}

	inline static int32_t get_offset_of_wasdSmooth_10() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___wasdSmooth_10)); }
	inline float get_wasdSmooth_10() const { return ___wasdSmooth_10; }
	inline float* get_address_of_wasdSmooth_10() { return &___wasdSmooth_10; }
	inline void set_wasdSmooth_10(float value)
	{
		___wasdSmooth_10 = value;
	}

	inline static int32_t get_offset_of_panSpeed_11() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___panSpeed_11)); }
	inline float get_panSpeed_11() const { return ___panSpeed_11; }
	inline float* get_address_of_panSpeed_11() { return &___panSpeed_11; }
	inline void set_panSpeed_11(float value)
	{
		___panSpeed_11 = value;
	}

	inline static int32_t get_offset_of_zoomSmooth_12() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___zoomSmooth_12)); }
	inline float get_zoomSmooth_12() const { return ___zoomSmooth_12; }
	inline float* get_address_of_zoomSmooth_12() { return &___zoomSmooth_12; }
	inline void set_zoomSmooth_12(float value)
	{
		___zoomSmooth_12 = value;
	}

	inline static int32_t get_offset_of_minZoom_13() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___minZoom_13)); }
	inline float get_minZoom_13() const { return ___minZoom_13; }
	inline float* get_address_of_minZoom_13() { return &___minZoom_13; }
	inline void set_minZoom_13(float value)
	{
		___minZoom_13 = value;
	}

	inline static int32_t get_offset_of_maxZoom_14() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___maxZoom_14)); }
	inline float get_maxZoom_14() const { return ___maxZoom_14; }
	inline float* get_address_of_maxZoom_14() { return &___maxZoom_14; }
	inline void set_maxZoom_14(float value)
	{
		___maxZoom_14 = value;
	}

	inline static int32_t get_offset_of_mouseSensitivityScaler_15() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___mouseSensitivityScaler_15)); }
	inline float get_mouseSensitivityScaler_15() const { return ___mouseSensitivityScaler_15; }
	inline float* get_address_of_mouseSensitivityScaler_15() { return &___mouseSensitivityScaler_15; }
	inline void set_mouseSensitivityScaler_15(float value)
	{
		___mouseSensitivityScaler_15 = value;
	}

	inline static int32_t get_offset_of_scrollSensitivityScaler_16() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___scrollSensitivityScaler_16)); }
	inline float get_scrollSensitivityScaler_16() const { return ___scrollSensitivityScaler_16; }
	inline float* get_address_of_scrollSensitivityScaler_16() { return &___scrollSensitivityScaler_16; }
	inline void set_scrollSensitivityScaler_16(float value)
	{
		___scrollSensitivityScaler_16 = value;
	}

	inline static int32_t get_offset_of_distance_17() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___distance_17)); }
	inline float get_distance_17() const { return ___distance_17; }
	inline float* get_address_of_distance_17() { return &___distance_17; }
	inline void set_distance_17(float value)
	{
		___distance_17 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_18() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___yMaxLimit_18)); }
	inline float get_yMaxLimit_18() const { return ___yMaxLimit_18; }
	inline float* get_address_of_yMaxLimit_18() { return &___yMaxLimit_18; }
	inline void set_yMaxLimit_18(float value)
	{
		___yMaxLimit_18 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_19() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___yMinLimit_19)); }
	inline float get_yMinLimit_19() const { return ___yMinLimit_19; }
	inline float* get_address_of_yMinLimit_19() { return &___yMinLimit_19; }
	inline void set_yMinLimit_19(float value)
	{
		___yMinLimit_19 = value;
	}

	inline static int32_t get_offset_of_smoothY_20() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___smoothY_20)); }
	inline float get_smoothY_20() const { return ___smoothY_20; }
	inline float* get_address_of_smoothY_20() { return &___smoothY_20; }
	inline void set_smoothY_20(float value)
	{
		___smoothY_20 = value;
	}

	inline static int32_t get_offset_of_smoothX_21() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___smoothX_21)); }
	inline float get_smoothX_21() const { return ___smoothX_21; }
	inline float* get_address_of_smoothX_21() { return &___smoothX_21; }
	inline void set_smoothX_21(float value)
	{
		___smoothX_21 = value;
	}

	inline static int32_t get_offset_of_movementPOS_22() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___movementPOS_22)); }
	inline Vector3_t3722313464  get_movementPOS_22() const { return ___movementPOS_22; }
	inline Vector3_t3722313464 * get_address_of_movementPOS_22() { return &___movementPOS_22; }
	inline void set_movementPOS_22(Vector3_t3722313464  value)
	{
		___movementPOS_22 = value;
	}

	inline static int32_t get_offset_of_rotation_23() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___rotation_23)); }
	inline Quaternion_t2301928331  get_rotation_23() const { return ___rotation_23; }
	inline Quaternion_t2301928331 * get_address_of_rotation_23() { return &___rotation_23; }
	inline void set_rotation_23(Quaternion_t2301928331  value)
	{
		___rotation_23 = value;
	}

	inline static int32_t get_offset_of_position_24() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___position_24)); }
	inline Vector3_t3722313464  get_position_24() const { return ___position_24; }
	inline Vector3_t3722313464 * get_address_of_position_24() { return &___position_24; }
	inline void set_position_24(Vector3_t3722313464  value)
	{
		___position_24 = value;
	}

	inline static int32_t get_offset_of_clickTimey_25() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___clickTimey_25)); }
	inline float get_clickTimey_25() const { return ___clickTimey_25; }
	inline float* get_address_of_clickTimey_25() { return &___clickTimey_25; }
	inline void set_clickTimey_25(float value)
	{
		___clickTimey_25 = value;
	}

	inline static int32_t get_offset_of_myTransform_26() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___myTransform_26)); }
	inline Transform_t3600365921 * get_myTransform_26() const { return ___myTransform_26; }
	inline Transform_t3600365921 ** get_address_of_myTransform_26() { return &___myTransform_26; }
	inline void set_myTransform_26(Transform_t3600365921 * value)
	{
		___myTransform_26 = value;
		Il2CppCodeGenWriteBarrier((&___myTransform_26), value);
	}

	inline static int32_t get_offset_of_posToBe_27() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___posToBe_27)); }
	inline Vector3_t3722313464  get_posToBe_27() const { return ___posToBe_27; }
	inline Vector3_t3722313464 * get_address_of_posToBe_27() { return &___posToBe_27; }
	inline void set_posToBe_27(Vector3_t3722313464  value)
	{
		___posToBe_27 = value;
	}

	inline static int32_t get_offset_of_zoomSmoothDelegate_28() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___zoomSmoothDelegate_28)); }
	inline float get_zoomSmoothDelegate_28() const { return ___zoomSmoothDelegate_28; }
	inline float* get_address_of_zoomSmoothDelegate_28() { return &___zoomSmoothDelegate_28; }
	inline void set_zoomSmoothDelegate_28(float value)
	{
		___zoomSmoothDelegate_28 = value;
	}

	inline static int32_t get_offset_of_x_29() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___x_29)); }
	inline float get_x_29() const { return ___x_29; }
	inline float* get_address_of_x_29() { return &___x_29; }
	inline void set_x_29(float value)
	{
		___x_29 = value;
	}

	inline static int32_t get_offset_of_y_30() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___y_30)); }
	inline float get_y_30() const { return ___y_30; }
	inline float* get_address_of_y_30() { return &___y_30; }
	inline void set_y_30(float value)
	{
		___y_30 = value;
	}

	inline static int32_t get_offset_of_movementPosOffset_31() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___movementPosOffset_31)); }
	inline Vector3_t3722313464  get_movementPosOffset_31() const { return ___movementPosOffset_31; }
	inline Vector3_t3722313464 * get_address_of_movementPosOffset_31() { return &___movementPosOffset_31; }
	inline void set_movementPosOffset_31(Vector3_t3722313464  value)
	{
		___movementPosOffset_31 = value;
	}

	inline static int32_t get_offset_of_isOrthographic_32() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___isOrthographic_32)); }
	inline bool get_isOrthographic_32() const { return ___isOrthographic_32; }
	inline bool* get_address_of_isOrthographic_32() { return &___isOrthographic_32; }
	inline void set_isOrthographic_32(bool value)
	{
		___isOrthographic_32 = value;
	}

	inline static int32_t get_offset_of_myCamera_33() { return static_cast<int32_t>(offsetof(MouseOrbit_t2077911214, ___myCamera_33)); }
	inline Camera_t4157153871 * get_myCamera_33() const { return ___myCamera_33; }
	inline Camera_t4157153871 ** get_address_of_myCamera_33() { return &___myCamera_33; }
	inline void set_myCamera_33(Camera_t4157153871 * value)
	{
		___myCamera_33 = value;
		Il2CppCodeGenWriteBarrier((&___myCamera_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORBIT_T2077911214_H
#ifndef SPINANIMATION_T2236150191_H
#define SPINANIMATION_T2236150191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpinAnimation
struct  SpinAnimation_t2236150191  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINANIMATION_T2236150191_H
#ifndef MATERIALBLENDER_T4195463282_H
#define MATERIALBLENDER_T4195463282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.MaterialBlender
struct  MaterialBlender_t4195463282  : public MonoBehaviour_t3962482529
{
public:
	// System.Single FlatLighting.MaterialBlender::durationSeconds
	float ___durationSeconds_2;
	// System.Boolean FlatLighting.MaterialBlender::loop
	bool ___loop_3;
	// UnityEngine.Material[] FlatLighting.MaterialBlender::materials
	MaterialU5BU5D_t561872642* ___materials_4;
	// UnityEngine.Material FlatLighting.MaterialBlender::internalMaterial
	Material_t340375123 * ___internalMaterial_5;

public:
	inline static int32_t get_offset_of_durationSeconds_2() { return static_cast<int32_t>(offsetof(MaterialBlender_t4195463282, ___durationSeconds_2)); }
	inline float get_durationSeconds_2() const { return ___durationSeconds_2; }
	inline float* get_address_of_durationSeconds_2() { return &___durationSeconds_2; }
	inline void set_durationSeconds_2(float value)
	{
		___durationSeconds_2 = value;
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(MaterialBlender_t4195463282, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}

	inline static int32_t get_offset_of_materials_4() { return static_cast<int32_t>(offsetof(MaterialBlender_t4195463282, ___materials_4)); }
	inline MaterialU5BU5D_t561872642* get_materials_4() const { return ___materials_4; }
	inline MaterialU5BU5D_t561872642** get_address_of_materials_4() { return &___materials_4; }
	inline void set_materials_4(MaterialU5BU5D_t561872642* value)
	{
		___materials_4 = value;
		Il2CppCodeGenWriteBarrier((&___materials_4), value);
	}

	inline static int32_t get_offset_of_internalMaterial_5() { return static_cast<int32_t>(offsetof(MaterialBlender_t4195463282, ___internalMaterial_5)); }
	inline Material_t340375123 * get_internalMaterial_5() const { return ___internalMaterial_5; }
	inline Material_t340375123 ** get_address_of_internalMaterial_5() { return &___internalMaterial_5; }
	inline void set_internalMaterial_5(Material_t340375123 * value)
	{
		___internalMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALBLENDER_T4195463282_H
#ifndef SCENELIGTINGSETUP_T3397062989_H
#define SCENELIGTINGSETUP_T3397062989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLigtingSetup
struct  SceneLigtingSetup_t3397062989  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color SceneLigtingSetup::cameraBackground
	Color_t2555686324  ___cameraBackground_2;
	// UnityEngine.Material SceneLigtingSetup::globalMaterial
	Material_t340375123 * ___globalMaterial_3;
	// UnityEngine.Material SceneLigtingSetup::vegetationMaterial
	Material_t340375123 * ___vegetationMaterial_4;
	// UnityEngine.Transform SceneLigtingSetup::vegetationRoot
	Transform_t3600365921 * ___vegetationRoot_5;
	// UnityEngine.Material SceneLigtingSetup::bridgeMaterial
	Material_t340375123 * ___bridgeMaterial_6;
	// UnityEngine.Transform SceneLigtingSetup::bridgeRoot
	Transform_t3600365921 * ___bridgeRoot_7;
	// UnityEngine.Material SceneLigtingSetup::lookoutMaterial
	Material_t340375123 * ___lookoutMaterial_8;
	// UnityEngine.Transform SceneLigtingSetup::lookoutRoot
	Transform_t3600365921 * ___lookoutRoot_9;
	// UnityEngine.Material SceneLigtingSetup::towerMaterial
	Material_t340375123 * ___towerMaterial_10;
	// UnityEngine.Transform SceneLigtingSetup::towerRoot
	Transform_t3600365921 * ___towerRoot_11;
	// UnityEngine.Material SceneLigtingSetup::deadTreeMaterial
	Material_t340375123 * ___deadTreeMaterial_12;
	// UnityEngine.Transform SceneLigtingSetup::deadTreeRoot
	Transform_t3600365921 * ___deadTreeRoot_13;
	// UnityEngine.Material SceneLigtingSetup::rocksMaterial
	Material_t340375123 * ___rocksMaterial_14;
	// UnityEngine.Transform SceneLigtingSetup::rocksTreeRoot
	Transform_t3600365921 * ___rocksTreeRoot_15;
	// UnityEngine.GameObject[] SceneLigtingSetup::objectsToEnable
	GameObjectU5BU5D_t3328599146* ___objectsToEnable_16;
	// UnityEngine.Renderer[] SceneLigtingSetup::sceneRenderers
	RendererU5BU5D_t3210418286* ___sceneRenderers_17;
	// UnityEngine.Renderer[] SceneLigtingSetup::vegetationRenderers
	RendererU5BU5D_t3210418286* ___vegetationRenderers_18;
	// UnityEngine.Renderer[] SceneLigtingSetup::bridgeRenderers
	RendererU5BU5D_t3210418286* ___bridgeRenderers_19;
	// UnityEngine.Renderer[] SceneLigtingSetup::lookoutRenderers
	RendererU5BU5D_t3210418286* ___lookoutRenderers_20;
	// UnityEngine.Renderer[] SceneLigtingSetup::towerRenderers
	RendererU5BU5D_t3210418286* ___towerRenderers_21;
	// UnityEngine.Renderer[] SceneLigtingSetup::deadTreeRenderers
	RendererU5BU5D_t3210418286* ___deadTreeRenderers_22;
	// UnityEngine.Renderer[] SceneLigtingSetup::boatRenderers
	RendererU5BU5D_t3210418286* ___boatRenderers_23;
	// UnityEngine.Renderer[] SceneLigtingSetup::rocksRenderers
	RendererU5BU5D_t3210418286* ___rocksRenderers_24;

public:
	inline static int32_t get_offset_of_cameraBackground_2() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___cameraBackground_2)); }
	inline Color_t2555686324  get_cameraBackground_2() const { return ___cameraBackground_2; }
	inline Color_t2555686324 * get_address_of_cameraBackground_2() { return &___cameraBackground_2; }
	inline void set_cameraBackground_2(Color_t2555686324  value)
	{
		___cameraBackground_2 = value;
	}

	inline static int32_t get_offset_of_globalMaterial_3() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___globalMaterial_3)); }
	inline Material_t340375123 * get_globalMaterial_3() const { return ___globalMaterial_3; }
	inline Material_t340375123 ** get_address_of_globalMaterial_3() { return &___globalMaterial_3; }
	inline void set_globalMaterial_3(Material_t340375123 * value)
	{
		___globalMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___globalMaterial_3), value);
	}

	inline static int32_t get_offset_of_vegetationMaterial_4() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___vegetationMaterial_4)); }
	inline Material_t340375123 * get_vegetationMaterial_4() const { return ___vegetationMaterial_4; }
	inline Material_t340375123 ** get_address_of_vegetationMaterial_4() { return &___vegetationMaterial_4; }
	inline void set_vegetationMaterial_4(Material_t340375123 * value)
	{
		___vegetationMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___vegetationMaterial_4), value);
	}

	inline static int32_t get_offset_of_vegetationRoot_5() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___vegetationRoot_5)); }
	inline Transform_t3600365921 * get_vegetationRoot_5() const { return ___vegetationRoot_5; }
	inline Transform_t3600365921 ** get_address_of_vegetationRoot_5() { return &___vegetationRoot_5; }
	inline void set_vegetationRoot_5(Transform_t3600365921 * value)
	{
		___vegetationRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___vegetationRoot_5), value);
	}

	inline static int32_t get_offset_of_bridgeMaterial_6() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___bridgeMaterial_6)); }
	inline Material_t340375123 * get_bridgeMaterial_6() const { return ___bridgeMaterial_6; }
	inline Material_t340375123 ** get_address_of_bridgeMaterial_6() { return &___bridgeMaterial_6; }
	inline void set_bridgeMaterial_6(Material_t340375123 * value)
	{
		___bridgeMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___bridgeMaterial_6), value);
	}

	inline static int32_t get_offset_of_bridgeRoot_7() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___bridgeRoot_7)); }
	inline Transform_t3600365921 * get_bridgeRoot_7() const { return ___bridgeRoot_7; }
	inline Transform_t3600365921 ** get_address_of_bridgeRoot_7() { return &___bridgeRoot_7; }
	inline void set_bridgeRoot_7(Transform_t3600365921 * value)
	{
		___bridgeRoot_7 = value;
		Il2CppCodeGenWriteBarrier((&___bridgeRoot_7), value);
	}

	inline static int32_t get_offset_of_lookoutMaterial_8() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___lookoutMaterial_8)); }
	inline Material_t340375123 * get_lookoutMaterial_8() const { return ___lookoutMaterial_8; }
	inline Material_t340375123 ** get_address_of_lookoutMaterial_8() { return &___lookoutMaterial_8; }
	inline void set_lookoutMaterial_8(Material_t340375123 * value)
	{
		___lookoutMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___lookoutMaterial_8), value);
	}

	inline static int32_t get_offset_of_lookoutRoot_9() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___lookoutRoot_9)); }
	inline Transform_t3600365921 * get_lookoutRoot_9() const { return ___lookoutRoot_9; }
	inline Transform_t3600365921 ** get_address_of_lookoutRoot_9() { return &___lookoutRoot_9; }
	inline void set_lookoutRoot_9(Transform_t3600365921 * value)
	{
		___lookoutRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&___lookoutRoot_9), value);
	}

	inline static int32_t get_offset_of_towerMaterial_10() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___towerMaterial_10)); }
	inline Material_t340375123 * get_towerMaterial_10() const { return ___towerMaterial_10; }
	inline Material_t340375123 ** get_address_of_towerMaterial_10() { return &___towerMaterial_10; }
	inline void set_towerMaterial_10(Material_t340375123 * value)
	{
		___towerMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___towerMaterial_10), value);
	}

	inline static int32_t get_offset_of_towerRoot_11() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___towerRoot_11)); }
	inline Transform_t3600365921 * get_towerRoot_11() const { return ___towerRoot_11; }
	inline Transform_t3600365921 ** get_address_of_towerRoot_11() { return &___towerRoot_11; }
	inline void set_towerRoot_11(Transform_t3600365921 * value)
	{
		___towerRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&___towerRoot_11), value);
	}

	inline static int32_t get_offset_of_deadTreeMaterial_12() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___deadTreeMaterial_12)); }
	inline Material_t340375123 * get_deadTreeMaterial_12() const { return ___deadTreeMaterial_12; }
	inline Material_t340375123 ** get_address_of_deadTreeMaterial_12() { return &___deadTreeMaterial_12; }
	inline void set_deadTreeMaterial_12(Material_t340375123 * value)
	{
		___deadTreeMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___deadTreeMaterial_12), value);
	}

	inline static int32_t get_offset_of_deadTreeRoot_13() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___deadTreeRoot_13)); }
	inline Transform_t3600365921 * get_deadTreeRoot_13() const { return ___deadTreeRoot_13; }
	inline Transform_t3600365921 ** get_address_of_deadTreeRoot_13() { return &___deadTreeRoot_13; }
	inline void set_deadTreeRoot_13(Transform_t3600365921 * value)
	{
		___deadTreeRoot_13 = value;
		Il2CppCodeGenWriteBarrier((&___deadTreeRoot_13), value);
	}

	inline static int32_t get_offset_of_rocksMaterial_14() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___rocksMaterial_14)); }
	inline Material_t340375123 * get_rocksMaterial_14() const { return ___rocksMaterial_14; }
	inline Material_t340375123 ** get_address_of_rocksMaterial_14() { return &___rocksMaterial_14; }
	inline void set_rocksMaterial_14(Material_t340375123 * value)
	{
		___rocksMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___rocksMaterial_14), value);
	}

	inline static int32_t get_offset_of_rocksTreeRoot_15() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___rocksTreeRoot_15)); }
	inline Transform_t3600365921 * get_rocksTreeRoot_15() const { return ___rocksTreeRoot_15; }
	inline Transform_t3600365921 ** get_address_of_rocksTreeRoot_15() { return &___rocksTreeRoot_15; }
	inline void set_rocksTreeRoot_15(Transform_t3600365921 * value)
	{
		___rocksTreeRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&___rocksTreeRoot_15), value);
	}

	inline static int32_t get_offset_of_objectsToEnable_16() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___objectsToEnable_16)); }
	inline GameObjectU5BU5D_t3328599146* get_objectsToEnable_16() const { return ___objectsToEnable_16; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objectsToEnable_16() { return &___objectsToEnable_16; }
	inline void set_objectsToEnable_16(GameObjectU5BU5D_t3328599146* value)
	{
		___objectsToEnable_16 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToEnable_16), value);
	}

	inline static int32_t get_offset_of_sceneRenderers_17() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___sceneRenderers_17)); }
	inline RendererU5BU5D_t3210418286* get_sceneRenderers_17() const { return ___sceneRenderers_17; }
	inline RendererU5BU5D_t3210418286** get_address_of_sceneRenderers_17() { return &___sceneRenderers_17; }
	inline void set_sceneRenderers_17(RendererU5BU5D_t3210418286* value)
	{
		___sceneRenderers_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneRenderers_17), value);
	}

	inline static int32_t get_offset_of_vegetationRenderers_18() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___vegetationRenderers_18)); }
	inline RendererU5BU5D_t3210418286* get_vegetationRenderers_18() const { return ___vegetationRenderers_18; }
	inline RendererU5BU5D_t3210418286** get_address_of_vegetationRenderers_18() { return &___vegetationRenderers_18; }
	inline void set_vegetationRenderers_18(RendererU5BU5D_t3210418286* value)
	{
		___vegetationRenderers_18 = value;
		Il2CppCodeGenWriteBarrier((&___vegetationRenderers_18), value);
	}

	inline static int32_t get_offset_of_bridgeRenderers_19() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___bridgeRenderers_19)); }
	inline RendererU5BU5D_t3210418286* get_bridgeRenderers_19() const { return ___bridgeRenderers_19; }
	inline RendererU5BU5D_t3210418286** get_address_of_bridgeRenderers_19() { return &___bridgeRenderers_19; }
	inline void set_bridgeRenderers_19(RendererU5BU5D_t3210418286* value)
	{
		___bridgeRenderers_19 = value;
		Il2CppCodeGenWriteBarrier((&___bridgeRenderers_19), value);
	}

	inline static int32_t get_offset_of_lookoutRenderers_20() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___lookoutRenderers_20)); }
	inline RendererU5BU5D_t3210418286* get_lookoutRenderers_20() const { return ___lookoutRenderers_20; }
	inline RendererU5BU5D_t3210418286** get_address_of_lookoutRenderers_20() { return &___lookoutRenderers_20; }
	inline void set_lookoutRenderers_20(RendererU5BU5D_t3210418286* value)
	{
		___lookoutRenderers_20 = value;
		Il2CppCodeGenWriteBarrier((&___lookoutRenderers_20), value);
	}

	inline static int32_t get_offset_of_towerRenderers_21() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___towerRenderers_21)); }
	inline RendererU5BU5D_t3210418286* get_towerRenderers_21() const { return ___towerRenderers_21; }
	inline RendererU5BU5D_t3210418286** get_address_of_towerRenderers_21() { return &___towerRenderers_21; }
	inline void set_towerRenderers_21(RendererU5BU5D_t3210418286* value)
	{
		___towerRenderers_21 = value;
		Il2CppCodeGenWriteBarrier((&___towerRenderers_21), value);
	}

	inline static int32_t get_offset_of_deadTreeRenderers_22() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___deadTreeRenderers_22)); }
	inline RendererU5BU5D_t3210418286* get_deadTreeRenderers_22() const { return ___deadTreeRenderers_22; }
	inline RendererU5BU5D_t3210418286** get_address_of_deadTreeRenderers_22() { return &___deadTreeRenderers_22; }
	inline void set_deadTreeRenderers_22(RendererU5BU5D_t3210418286* value)
	{
		___deadTreeRenderers_22 = value;
		Il2CppCodeGenWriteBarrier((&___deadTreeRenderers_22), value);
	}

	inline static int32_t get_offset_of_boatRenderers_23() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___boatRenderers_23)); }
	inline RendererU5BU5D_t3210418286* get_boatRenderers_23() const { return ___boatRenderers_23; }
	inline RendererU5BU5D_t3210418286** get_address_of_boatRenderers_23() { return &___boatRenderers_23; }
	inline void set_boatRenderers_23(RendererU5BU5D_t3210418286* value)
	{
		___boatRenderers_23 = value;
		Il2CppCodeGenWriteBarrier((&___boatRenderers_23), value);
	}

	inline static int32_t get_offset_of_rocksRenderers_24() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3397062989, ___rocksRenderers_24)); }
	inline RendererU5BU5D_t3210418286* get_rocksRenderers_24() const { return ___rocksRenderers_24; }
	inline RendererU5BU5D_t3210418286** get_address_of_rocksRenderers_24() { return &___rocksRenderers_24; }
	inline void set_rocksRenderers_24(RendererU5BU5D_t3210418286* value)
	{
		___rocksRenderers_24 = value;
		Il2CppCodeGenWriteBarrier((&___rocksRenderers_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELIGTINGSETUP_T3397062989_H
#ifndef STATICITEM_T47160647_H
#define STATICITEM_T47160647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StaticItem
struct  StaticItem_t47160647  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject StaticItem::Camera
	GameObject_t1113636619 * ___Camera_2;
	// UnityEngine.GameObject StaticItem::positionToFollow
	GameObject_t1113636619 * ___positionToFollow_3;
	// System.Single StaticItem::speed
	float ___speed_4;
	// System.Single StaticItem::startTime
	float ___startTime_5;
	// System.Single StaticItem::journeyLength
	float ___journeyLength_6;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(StaticItem_t47160647, ___Camera_2)); }
	inline GameObject_t1113636619 * get_Camera_2() const { return ___Camera_2; }
	inline GameObject_t1113636619 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(GameObject_t1113636619 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_2), value);
	}

	inline static int32_t get_offset_of_positionToFollow_3() { return static_cast<int32_t>(offsetof(StaticItem_t47160647, ___positionToFollow_3)); }
	inline GameObject_t1113636619 * get_positionToFollow_3() const { return ___positionToFollow_3; }
	inline GameObject_t1113636619 ** get_address_of_positionToFollow_3() { return &___positionToFollow_3; }
	inline void set_positionToFollow_3(GameObject_t1113636619 * value)
	{
		___positionToFollow_3 = value;
		Il2CppCodeGenWriteBarrier((&___positionToFollow_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(StaticItem_t47160647, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_startTime_5() { return static_cast<int32_t>(offsetof(StaticItem_t47160647, ___startTime_5)); }
	inline float get_startTime_5() const { return ___startTime_5; }
	inline float* get_address_of_startTime_5() { return &___startTime_5; }
	inline void set_startTime_5(float value)
	{
		___startTime_5 = value;
	}

	inline static int32_t get_offset_of_journeyLength_6() { return static_cast<int32_t>(offsetof(StaticItem_t47160647, ___journeyLength_6)); }
	inline float get_journeyLength_6() const { return ___journeyLength_6; }
	inline float* get_address_of_journeyLength_6() { return &___journeyLength_6; }
	inline void set_journeyLength_6(float value)
	{
		___journeyLength_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICITEM_T47160647_H
#ifndef BUTTONINPUTUI_T1749738659_H
#define BUTTONINPUTUI_T1749738659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.ButtonInputUI
struct  ButtonInputUI_t1749738659  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/ButtonInput SimpleInputNamespace.ButtonInputUI::button
	ButtonInput_t2818951903 * ___button_2;

public:
	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(ButtonInputUI_t1749738659, ___button_2)); }
	inline ButtonInput_t2818951903 * get_button_2() const { return ___button_2; }
	inline ButtonInput_t2818951903 ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(ButtonInput_t2818951903 * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUTUI_T1749738659_H
#ifndef BUTTONINPUTKEYBOARD_T3640535414_H
#define BUTTONINPUTKEYBOARD_T3640535414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.ButtonInputKeyboard
struct  ButtonInputKeyboard_t3640535414  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.ButtonInputKeyboard::key
	int32_t ___key_2;
	// SimpleInput/ButtonInput SimpleInputNamespace.ButtonInputKeyboard::button
	ButtonInput_t2818951903 * ___button_3;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(ButtonInputKeyboard_t3640535414, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(ButtonInputKeyboard_t3640535414, ___button_3)); }
	inline ButtonInput_t2818951903 * get_button_3() const { return ___button_3; }
	inline ButtonInput_t2818951903 ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(ButtonInput_t2818951903 * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier((&___button_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUTKEYBOARD_T3640535414_H
#ifndef KEYINPUTUI_T1141642093_H
#define KEYINPUTUI_T1141642093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.KeyInputUI
struct  KeyInputUI_t1141642093  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/KeyInput SimpleInputNamespace.KeyInputUI::key
	KeyInput_t3393349320 * ___key_2;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KeyInputUI_t1141642093, ___key_2)); }
	inline KeyInput_t3393349320 * get_key_2() const { return ___key_2; }
	inline KeyInput_t3393349320 ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(KeyInput_t3393349320 * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINPUTUI_T1141642093_H
#ifndef KEYINPUTKEYBOARD_T2444570555_H
#define KEYINPUTKEYBOARD_T2444570555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.KeyInputKeyboard
struct  KeyInputKeyboard_t2444570555  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.KeyInputKeyboard::realKey
	int32_t ___realKey_2;
	// SimpleInput/KeyInput SimpleInputNamespace.KeyInputKeyboard::key
	KeyInput_t3393349320 * ___key_3;

public:
	inline static int32_t get_offset_of_realKey_2() { return static_cast<int32_t>(offsetof(KeyInputKeyboard_t2444570555, ___realKey_2)); }
	inline int32_t get_realKey_2() const { return ___realKey_2; }
	inline int32_t* get_address_of_realKey_2() { return &___realKey_2; }
	inline void set_realKey_2(int32_t value)
	{
		___realKey_2 = value;
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(KeyInputKeyboard_t2444570555, ___key_3)); }
	inline KeyInput_t3393349320 * get_key_3() const { return ___key_3; }
	inline KeyInput_t3393349320 ** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(KeyInput_t3393349320 * value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier((&___key_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINPUTKEYBOARD_T2444570555_H
#ifndef CAMERACONTROL_T3123314556_H
#define CAMERACONTROL_T3123314556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraControl
struct  CameraControl_t3123314556  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CameraControl::pointOfInterest
	Transform_t3600365921 * ___pointOfInterest_2;
	// System.Boolean CameraControl::shouldRotateAutomatic
	bool ___shouldRotateAutomatic_3;
	// System.Single CameraControl::rotationSpeed
	float ___rotationSpeed_4;
	// System.Single CameraControl::minRadius
	float ___minRadius_5;
	// System.Single CameraControl::maxRadius
	float ___maxRadius_6;
	// System.Single CameraControl::minZoom
	float ___minZoom_7;
	// System.Single CameraControl::maxZoom
	float ___maxZoom_8;
	// UnityEngine.Camera CameraControl::myCamera
	Camera_t4157153871 * ___myCamera_9;
	// UnityEngine.Vector3 CameraControl::lastNormalizedPosition
	Vector3_t3722313464  ___lastNormalizedPosition_10;
	// System.Single CameraControl::radius
	float ___radius_11;

public:
	inline static int32_t get_offset_of_pointOfInterest_2() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___pointOfInterest_2)); }
	inline Transform_t3600365921 * get_pointOfInterest_2() const { return ___pointOfInterest_2; }
	inline Transform_t3600365921 ** get_address_of_pointOfInterest_2() { return &___pointOfInterest_2; }
	inline void set_pointOfInterest_2(Transform_t3600365921 * value)
	{
		___pointOfInterest_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointOfInterest_2), value);
	}

	inline static int32_t get_offset_of_shouldRotateAutomatic_3() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___shouldRotateAutomatic_3)); }
	inline bool get_shouldRotateAutomatic_3() const { return ___shouldRotateAutomatic_3; }
	inline bool* get_address_of_shouldRotateAutomatic_3() { return &___shouldRotateAutomatic_3; }
	inline void set_shouldRotateAutomatic_3(bool value)
	{
		___shouldRotateAutomatic_3 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_4() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___rotationSpeed_4)); }
	inline float get_rotationSpeed_4() const { return ___rotationSpeed_4; }
	inline float* get_address_of_rotationSpeed_4() { return &___rotationSpeed_4; }
	inline void set_rotationSpeed_4(float value)
	{
		___rotationSpeed_4 = value;
	}

	inline static int32_t get_offset_of_minRadius_5() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___minRadius_5)); }
	inline float get_minRadius_5() const { return ___minRadius_5; }
	inline float* get_address_of_minRadius_5() { return &___minRadius_5; }
	inline void set_minRadius_5(float value)
	{
		___minRadius_5 = value;
	}

	inline static int32_t get_offset_of_maxRadius_6() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___maxRadius_6)); }
	inline float get_maxRadius_6() const { return ___maxRadius_6; }
	inline float* get_address_of_maxRadius_6() { return &___maxRadius_6; }
	inline void set_maxRadius_6(float value)
	{
		___maxRadius_6 = value;
	}

	inline static int32_t get_offset_of_minZoom_7() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___minZoom_7)); }
	inline float get_minZoom_7() const { return ___minZoom_7; }
	inline float* get_address_of_minZoom_7() { return &___minZoom_7; }
	inline void set_minZoom_7(float value)
	{
		___minZoom_7 = value;
	}

	inline static int32_t get_offset_of_maxZoom_8() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___maxZoom_8)); }
	inline float get_maxZoom_8() const { return ___maxZoom_8; }
	inline float* get_address_of_maxZoom_8() { return &___maxZoom_8; }
	inline void set_maxZoom_8(float value)
	{
		___maxZoom_8 = value;
	}

	inline static int32_t get_offset_of_myCamera_9() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___myCamera_9)); }
	inline Camera_t4157153871 * get_myCamera_9() const { return ___myCamera_9; }
	inline Camera_t4157153871 ** get_address_of_myCamera_9() { return &___myCamera_9; }
	inline void set_myCamera_9(Camera_t4157153871 * value)
	{
		___myCamera_9 = value;
		Il2CppCodeGenWriteBarrier((&___myCamera_9), value);
	}

	inline static int32_t get_offset_of_lastNormalizedPosition_10() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___lastNormalizedPosition_10)); }
	inline Vector3_t3722313464  get_lastNormalizedPosition_10() const { return ___lastNormalizedPosition_10; }
	inline Vector3_t3722313464 * get_address_of_lastNormalizedPosition_10() { return &___lastNormalizedPosition_10; }
	inline void set_lastNormalizedPosition_10(Vector3_t3722313464  value)
	{
		___lastNormalizedPosition_10 = value;
	}

	inline static int32_t get_offset_of_radius_11() { return static_cast<int32_t>(offsetof(CameraControl_t3123314556, ___radius_11)); }
	inline float get_radius_11() const { return ___radius_11; }
	inline float* get_address_of_radius_11() { return &___radius_11; }
	inline void set_radius_11(float value)
	{
		___radius_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROL_T3123314556_H
#ifndef SIMPLEINPUTDRAGLISTENER_T3058349199_H
#define SIMPLEINPUTDRAGLISTENER_T3058349199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.SimpleInputDragListener
struct  SimpleInputDragListener_t3058349199  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInputNamespace.ISimpleInputDraggable SimpleInputNamespace.SimpleInputDragListener::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_2;
	// System.Int32 SimpleInputNamespace.SimpleInputDragListener::pointerId
	int32_t ___pointerId_3;

public:
	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SimpleInputDragListener_t3058349199, ___U3CListenerU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_2() const { return ___U3CListenerU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_2() { return &___U3CListenerU3Ek__BackingField_2; }
	inline void set_U3CListenerU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_pointerId_3() { return static_cast<int32_t>(offsetof(SimpleInputDragListener_t3058349199, ___pointerId_3)); }
	inline int32_t get_pointerId_3() const { return ___pointerId_3; }
	inline int32_t* get_address_of_pointerId_3() { return &___pointerId_3; }
	inline void set_pointerId_3(int32_t value)
	{
		___pointerId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEINPUTDRAGLISTENER_T3058349199_H
#ifndef TOUCHPAD_T3466294403_H
#define TOUCHPAD_T3466294403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Touchpad
struct  Touchpad_t3466294403  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.Touchpad::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.Touchpad::yAxis
	AxisInput_t802457650 * ___yAxis_3;
	// UnityEngine.RectTransform SimpleInputNamespace.Touchpad::rectTransform
	RectTransform_t3704657025 * ___rectTransform_4;
	// System.Single SimpleInputNamespace.Touchpad::sensitivity
	float ___sensitivity_5;
	// System.Boolean SimpleInputNamespace.Touchpad::allowTouchInput
	bool ___allowTouchInput_6;
	// SimpleInputNamespace.Touchpad/MouseButton[] SimpleInputNamespace.Touchpad::allowedMouseButtons
	MouseButtonU5BU5D_t3311405085* ___allowedMouseButtons_7;
	// System.Boolean SimpleInputNamespace.Touchpad::ignoreUIElements
	bool ___ignoreUIElements_8;
	// System.Single SimpleInputNamespace.Touchpad::resolutionMultiplier
	float ___resolutionMultiplier_9;
	// System.Int32 SimpleInputNamespace.Touchpad::fingerId
	int32_t ___fingerId_10;
	// UnityEngine.Vector2 SimpleInputNamespace.Touchpad::prevMouseInputPos
	Vector2_t2156229523  ___prevMouseInputPos_11;
	// System.Boolean SimpleInputNamespace.Touchpad::trackMouseInput
	bool ___trackMouseInput_12;
	// UnityEngine.Vector2 SimpleInputNamespace.Touchpad::m_value
	Vector2_t2156229523  ___m_value_13;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___rectTransform_4)); }
	inline RectTransform_t3704657025 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t3704657025 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_4), value);
	}

	inline static int32_t get_offset_of_sensitivity_5() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___sensitivity_5)); }
	inline float get_sensitivity_5() const { return ___sensitivity_5; }
	inline float* get_address_of_sensitivity_5() { return &___sensitivity_5; }
	inline void set_sensitivity_5(float value)
	{
		___sensitivity_5 = value;
	}

	inline static int32_t get_offset_of_allowTouchInput_6() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___allowTouchInput_6)); }
	inline bool get_allowTouchInput_6() const { return ___allowTouchInput_6; }
	inline bool* get_address_of_allowTouchInput_6() { return &___allowTouchInput_6; }
	inline void set_allowTouchInput_6(bool value)
	{
		___allowTouchInput_6 = value;
	}

	inline static int32_t get_offset_of_allowedMouseButtons_7() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___allowedMouseButtons_7)); }
	inline MouseButtonU5BU5D_t3311405085* get_allowedMouseButtons_7() const { return ___allowedMouseButtons_7; }
	inline MouseButtonU5BU5D_t3311405085** get_address_of_allowedMouseButtons_7() { return &___allowedMouseButtons_7; }
	inline void set_allowedMouseButtons_7(MouseButtonU5BU5D_t3311405085* value)
	{
		___allowedMouseButtons_7 = value;
		Il2CppCodeGenWriteBarrier((&___allowedMouseButtons_7), value);
	}

	inline static int32_t get_offset_of_ignoreUIElements_8() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___ignoreUIElements_8)); }
	inline bool get_ignoreUIElements_8() const { return ___ignoreUIElements_8; }
	inline bool* get_address_of_ignoreUIElements_8() { return &___ignoreUIElements_8; }
	inline void set_ignoreUIElements_8(bool value)
	{
		___ignoreUIElements_8 = value;
	}

	inline static int32_t get_offset_of_resolutionMultiplier_9() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___resolutionMultiplier_9)); }
	inline float get_resolutionMultiplier_9() const { return ___resolutionMultiplier_9; }
	inline float* get_address_of_resolutionMultiplier_9() { return &___resolutionMultiplier_9; }
	inline void set_resolutionMultiplier_9(float value)
	{
		___resolutionMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_fingerId_10() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___fingerId_10)); }
	inline int32_t get_fingerId_10() const { return ___fingerId_10; }
	inline int32_t* get_address_of_fingerId_10() { return &___fingerId_10; }
	inline void set_fingerId_10(int32_t value)
	{
		___fingerId_10 = value;
	}

	inline static int32_t get_offset_of_prevMouseInputPos_11() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___prevMouseInputPos_11)); }
	inline Vector2_t2156229523  get_prevMouseInputPos_11() const { return ___prevMouseInputPos_11; }
	inline Vector2_t2156229523 * get_address_of_prevMouseInputPos_11() { return &___prevMouseInputPos_11; }
	inline void set_prevMouseInputPos_11(Vector2_t2156229523  value)
	{
		___prevMouseInputPos_11 = value;
	}

	inline static int32_t get_offset_of_trackMouseInput_12() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___trackMouseInput_12)); }
	inline bool get_trackMouseInput_12() const { return ___trackMouseInput_12; }
	inline bool* get_address_of_trackMouseInput_12() { return &___trackMouseInput_12; }
	inline void set_trackMouseInput_12(bool value)
	{
		___trackMouseInput_12 = value;
	}

	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___m_value_13)); }
	inline Vector2_t2156229523  get_m_value_13() const { return ___m_value_13; }
	inline Vector2_t2156229523 * get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(Vector2_t2156229523  value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T3466294403_H
#ifndef SIMPLEINPUT_T4265260572_H
#define SIMPLEINPUT_T4265260572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput
struct  SimpleInput_t4265260572  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct SimpleInput_t4265260572_StaticFields
{
public:
	// SimpleInput SimpleInput::instance
	SimpleInput_t4265260572 * ___instance_3;
	// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis> SimpleInput::axes
	Dictionary_2_t1968145125 * ___axes_4;
	// System.Collections.Generic.List`1<SimpleInput/Axis> SimpleInput::axesList
	List_1_t3654963568 * ___axesList_5;
	// System.Collections.Generic.List`1<SimpleInput/AxisInput> SimpleInput::trackedUnityAxes
	List_1_t2274532392 * ___trackedUnityAxes_6;
	// System.Collections.Generic.List`1<SimpleInput/AxisInput> SimpleInput::trackedTemporaryAxes
	List_1_t2274532392 * ___trackedTemporaryAxes_7;
	// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button> SimpleInput::buttons
	Dictionary_2_t2716226479 * ___buttons_8;
	// System.Collections.Generic.List`1<SimpleInput/Button> SimpleInput::buttonsList
	List_1_t108077626 * ___buttonsList_9;
	// System.Collections.Generic.List`1<SimpleInput/ButtonInput> SimpleInput::trackedUnityButtons
	List_1_t4291026645 * ___trackedUnityButtons_10;
	// System.Collections.Generic.List`1<SimpleInput/ButtonInput> SimpleInput::trackedTemporaryButtons
	List_1_t4291026645 * ___trackedTemporaryButtons_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton> SimpleInput::mouseButtons
	Dictionary_2_t4181968371 * ___mouseButtons_12;
	// System.Collections.Generic.List`1<SimpleInput/MouseButton> SimpleInput::mouseButtonsList
	List_1_t2470362486 * ___mouseButtonsList_13;
	// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput> SimpleInput::trackedUnityMouseButtons
	List_1_t45323880 * ___trackedUnityMouseButtons_14;
	// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput> SimpleInput::trackedTemporaryMouseButtons
	List_1_t45323880 * ___trackedTemporaryMouseButtons_15;
	// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key> SimpleInput::keys
	Dictionary_2_t316807927 * ___keys_16;
	// System.Collections.Generic.List`1<SimpleInput/Key> SimpleInput::keysList
	List_1_t1062181654 * ___keysList_17;
	// SimpleInput/UpdateCallback SimpleInput::OnUpdate
	UpdateCallback_t3991193291 * ___OnUpdate_18;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> SimpleInput::<>f__mg$cache0
	UnityAction_2_t2165061829 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> SimpleInput::<>f__mg$cache1
	UnityAction_2_t2165061829 * ___U3CU3Ef__mgU24cache1_20;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___instance_3)); }
	inline SimpleInput_t4265260572 * get_instance_3() const { return ___instance_3; }
	inline SimpleInput_t4265260572 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(SimpleInput_t4265260572 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_axes_4() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___axes_4)); }
	inline Dictionary_2_t1968145125 * get_axes_4() const { return ___axes_4; }
	inline Dictionary_2_t1968145125 ** get_address_of_axes_4() { return &___axes_4; }
	inline void set_axes_4(Dictionary_2_t1968145125 * value)
	{
		___axes_4 = value;
		Il2CppCodeGenWriteBarrier((&___axes_4), value);
	}

	inline static int32_t get_offset_of_axesList_5() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___axesList_5)); }
	inline List_1_t3654963568 * get_axesList_5() const { return ___axesList_5; }
	inline List_1_t3654963568 ** get_address_of_axesList_5() { return &___axesList_5; }
	inline void set_axesList_5(List_1_t3654963568 * value)
	{
		___axesList_5 = value;
		Il2CppCodeGenWriteBarrier((&___axesList_5), value);
	}

	inline static int32_t get_offset_of_trackedUnityAxes_6() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedUnityAxes_6)); }
	inline List_1_t2274532392 * get_trackedUnityAxes_6() const { return ___trackedUnityAxes_6; }
	inline List_1_t2274532392 ** get_address_of_trackedUnityAxes_6() { return &___trackedUnityAxes_6; }
	inline void set_trackedUnityAxes_6(List_1_t2274532392 * value)
	{
		___trackedUnityAxes_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackedUnityAxes_6), value);
	}

	inline static int32_t get_offset_of_trackedTemporaryAxes_7() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedTemporaryAxes_7)); }
	inline List_1_t2274532392 * get_trackedTemporaryAxes_7() const { return ___trackedTemporaryAxes_7; }
	inline List_1_t2274532392 ** get_address_of_trackedTemporaryAxes_7() { return &___trackedTemporaryAxes_7; }
	inline void set_trackedTemporaryAxes_7(List_1_t2274532392 * value)
	{
		___trackedTemporaryAxes_7 = value;
		Il2CppCodeGenWriteBarrier((&___trackedTemporaryAxes_7), value);
	}

	inline static int32_t get_offset_of_buttons_8() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___buttons_8)); }
	inline Dictionary_2_t2716226479 * get_buttons_8() const { return ___buttons_8; }
	inline Dictionary_2_t2716226479 ** get_address_of_buttons_8() { return &___buttons_8; }
	inline void set_buttons_8(Dictionary_2_t2716226479 * value)
	{
		___buttons_8 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_8), value);
	}

	inline static int32_t get_offset_of_buttonsList_9() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___buttonsList_9)); }
	inline List_1_t108077626 * get_buttonsList_9() const { return ___buttonsList_9; }
	inline List_1_t108077626 ** get_address_of_buttonsList_9() { return &___buttonsList_9; }
	inline void set_buttonsList_9(List_1_t108077626 * value)
	{
		___buttonsList_9 = value;
		Il2CppCodeGenWriteBarrier((&___buttonsList_9), value);
	}

	inline static int32_t get_offset_of_trackedUnityButtons_10() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedUnityButtons_10)); }
	inline List_1_t4291026645 * get_trackedUnityButtons_10() const { return ___trackedUnityButtons_10; }
	inline List_1_t4291026645 ** get_address_of_trackedUnityButtons_10() { return &___trackedUnityButtons_10; }
	inline void set_trackedUnityButtons_10(List_1_t4291026645 * value)
	{
		___trackedUnityButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___trackedUnityButtons_10), value);
	}

	inline static int32_t get_offset_of_trackedTemporaryButtons_11() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedTemporaryButtons_11)); }
	inline List_1_t4291026645 * get_trackedTemporaryButtons_11() const { return ___trackedTemporaryButtons_11; }
	inline List_1_t4291026645 ** get_address_of_trackedTemporaryButtons_11() { return &___trackedTemporaryButtons_11; }
	inline void set_trackedTemporaryButtons_11(List_1_t4291026645 * value)
	{
		___trackedTemporaryButtons_11 = value;
		Il2CppCodeGenWriteBarrier((&___trackedTemporaryButtons_11), value);
	}

	inline static int32_t get_offset_of_mouseButtons_12() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___mouseButtons_12)); }
	inline Dictionary_2_t4181968371 * get_mouseButtons_12() const { return ___mouseButtons_12; }
	inline Dictionary_2_t4181968371 ** get_address_of_mouseButtons_12() { return &___mouseButtons_12; }
	inline void set_mouseButtons_12(Dictionary_2_t4181968371 * value)
	{
		___mouseButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtons_12), value);
	}

	inline static int32_t get_offset_of_mouseButtonsList_13() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___mouseButtonsList_13)); }
	inline List_1_t2470362486 * get_mouseButtonsList_13() const { return ___mouseButtonsList_13; }
	inline List_1_t2470362486 ** get_address_of_mouseButtonsList_13() { return &___mouseButtonsList_13; }
	inline void set_mouseButtonsList_13(List_1_t2470362486 * value)
	{
		___mouseButtonsList_13 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtonsList_13), value);
	}

	inline static int32_t get_offset_of_trackedUnityMouseButtons_14() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedUnityMouseButtons_14)); }
	inline List_1_t45323880 * get_trackedUnityMouseButtons_14() const { return ___trackedUnityMouseButtons_14; }
	inline List_1_t45323880 ** get_address_of_trackedUnityMouseButtons_14() { return &___trackedUnityMouseButtons_14; }
	inline void set_trackedUnityMouseButtons_14(List_1_t45323880 * value)
	{
		___trackedUnityMouseButtons_14 = value;
		Il2CppCodeGenWriteBarrier((&___trackedUnityMouseButtons_14), value);
	}

	inline static int32_t get_offset_of_trackedTemporaryMouseButtons_15() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedTemporaryMouseButtons_15)); }
	inline List_1_t45323880 * get_trackedTemporaryMouseButtons_15() const { return ___trackedTemporaryMouseButtons_15; }
	inline List_1_t45323880 ** get_address_of_trackedTemporaryMouseButtons_15() { return &___trackedTemporaryMouseButtons_15; }
	inline void set_trackedTemporaryMouseButtons_15(List_1_t45323880 * value)
	{
		___trackedTemporaryMouseButtons_15 = value;
		Il2CppCodeGenWriteBarrier((&___trackedTemporaryMouseButtons_15), value);
	}

	inline static int32_t get_offset_of_keys_16() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___keys_16)); }
	inline Dictionary_2_t316807927 * get_keys_16() const { return ___keys_16; }
	inline Dictionary_2_t316807927 ** get_address_of_keys_16() { return &___keys_16; }
	inline void set_keys_16(Dictionary_2_t316807927 * value)
	{
		___keys_16 = value;
		Il2CppCodeGenWriteBarrier((&___keys_16), value);
	}

	inline static int32_t get_offset_of_keysList_17() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___keysList_17)); }
	inline List_1_t1062181654 * get_keysList_17() const { return ___keysList_17; }
	inline List_1_t1062181654 ** get_address_of_keysList_17() { return &___keysList_17; }
	inline void set_keysList_17(List_1_t1062181654 * value)
	{
		___keysList_17 = value;
		Il2CppCodeGenWriteBarrier((&___keysList_17), value);
	}

	inline static int32_t get_offset_of_OnUpdate_18() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___OnUpdate_18)); }
	inline UpdateCallback_t3991193291 * get_OnUpdate_18() const { return ___OnUpdate_18; }
	inline UpdateCallback_t3991193291 ** get_address_of_OnUpdate_18() { return &___OnUpdate_18; }
	inline void set_OnUpdate_18(UpdateCallback_t3991193291 * value)
	{
		___OnUpdate_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdate_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline UnityAction_2_t2165061829 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline UnityAction_2_t2165061829 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(UnityAction_2_t2165061829 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline UnityAction_2_t2165061829 * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline UnityAction_2_t2165061829 ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(UnityAction_2_t2165061829 * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEINPUT_T4265260572_H
#ifndef CONNECTTOPREVIOUSREMOTE_T2022260273_H
#define CONNECTTOPREVIOUSREMOTE_T2022260273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectToPreviousRemote
struct  ConnectToPreviousRemote_t2022260273  : public MonoBehaviour_t3962482529
{
public:
	// System.Guid ConnectToPreviousRemote::lastRemoteID
	Guid_t  ___lastRemoteID_2;
	// System.Boolean ConnectToPreviousRemote::activelySearching
	bool ___activelySearching_3;

public:
	inline static int32_t get_offset_of_lastRemoteID_2() { return static_cast<int32_t>(offsetof(ConnectToPreviousRemote_t2022260273, ___lastRemoteID_2)); }
	inline Guid_t  get_lastRemoteID_2() const { return ___lastRemoteID_2; }
	inline Guid_t * get_address_of_lastRemoteID_2() { return &___lastRemoteID_2; }
	inline void set_lastRemoteID_2(Guid_t  value)
	{
		___lastRemoteID_2 = value;
	}

	inline static int32_t get_offset_of_activelySearching_3() { return static_cast<int32_t>(offsetof(ConnectToPreviousRemote_t2022260273, ___activelySearching_3)); }
	inline bool get_activelySearching_3() const { return ___activelySearching_3; }
	inline bool* get_address_of_activelySearching_3() { return &___activelySearching_3; }
	inline void set_activelySearching_3(bool value)
	{
		___activelySearching_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOPREVIOUSREMOTE_T2022260273_H
#ifndef ASYMMETRICFRUSTUM_T2453563558_H
#define ASYMMETRICFRUSTUM_T2453563558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.AsymmetricFrustum
struct  AsymmetricFrustum_t2453563558  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.AsymmetricFrustum::left
	float ___left_2;
	// System.Single Mira.AsymmetricFrustum::right
	float ___right_3;
	// System.Single Mira.AsymmetricFrustum::top
	float ___top_4;
	// System.Single Mira.AsymmetricFrustum::bottom
	float ___bottom_5;
	// System.Single Mira.AsymmetricFrustum::near
	float ___near_6;
	// System.Single Mira.AsymmetricFrustum::far
	float ___far_7;
	// System.Single Mira.AsymmetricFrustum::fov
	float ___fov_8;
	// System.Single Mira.AsymmetricFrustum::calibratedCamNear
	float ___calibratedCamNear_9;
	// System.Single Mira.AsymmetricFrustum::slideX
	float ___slideX_10;
	// System.Boolean Mira.AsymmetricFrustum::isLeftCam
	bool ___isLeftCam_11;
	// UnityEngine.Camera Mira.AsymmetricFrustum::cam
	Camera_t4157153871 * ___cam_12;
	// System.Boolean Mira.AsymmetricFrustum::asymmetricFrust
	bool ___asymmetricFrust_13;

public:
	inline static int32_t get_offset_of_left_2() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___left_2)); }
	inline float get_left_2() const { return ___left_2; }
	inline float* get_address_of_left_2() { return &___left_2; }
	inline void set_left_2(float value)
	{
		___left_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___right_3)); }
	inline float get_right_3() const { return ___right_3; }
	inline float* get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(float value)
	{
		___right_3 = value;
	}

	inline static int32_t get_offset_of_top_4() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___top_4)); }
	inline float get_top_4() const { return ___top_4; }
	inline float* get_address_of_top_4() { return &___top_4; }
	inline void set_top_4(float value)
	{
		___top_4 = value;
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___bottom_5)); }
	inline float get_bottom_5() const { return ___bottom_5; }
	inline float* get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(float value)
	{
		___bottom_5 = value;
	}

	inline static int32_t get_offset_of_near_6() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___near_6)); }
	inline float get_near_6() const { return ___near_6; }
	inline float* get_address_of_near_6() { return &___near_6; }
	inline void set_near_6(float value)
	{
		___near_6 = value;
	}

	inline static int32_t get_offset_of_far_7() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___far_7)); }
	inline float get_far_7() const { return ___far_7; }
	inline float* get_address_of_far_7() { return &___far_7; }
	inline void set_far_7(float value)
	{
		___far_7 = value;
	}

	inline static int32_t get_offset_of_fov_8() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___fov_8)); }
	inline float get_fov_8() const { return ___fov_8; }
	inline float* get_address_of_fov_8() { return &___fov_8; }
	inline void set_fov_8(float value)
	{
		___fov_8 = value;
	}

	inline static int32_t get_offset_of_calibratedCamNear_9() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___calibratedCamNear_9)); }
	inline float get_calibratedCamNear_9() const { return ___calibratedCamNear_9; }
	inline float* get_address_of_calibratedCamNear_9() { return &___calibratedCamNear_9; }
	inline void set_calibratedCamNear_9(float value)
	{
		___calibratedCamNear_9 = value;
	}

	inline static int32_t get_offset_of_slideX_10() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___slideX_10)); }
	inline float get_slideX_10() const { return ___slideX_10; }
	inline float* get_address_of_slideX_10() { return &___slideX_10; }
	inline void set_slideX_10(float value)
	{
		___slideX_10 = value;
	}

	inline static int32_t get_offset_of_isLeftCam_11() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___isLeftCam_11)); }
	inline bool get_isLeftCam_11() const { return ___isLeftCam_11; }
	inline bool* get_address_of_isLeftCam_11() { return &___isLeftCam_11; }
	inline void set_isLeftCam_11(bool value)
	{
		___isLeftCam_11 = value;
	}

	inline static int32_t get_offset_of_cam_12() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___cam_12)); }
	inline Camera_t4157153871 * get_cam_12() const { return ___cam_12; }
	inline Camera_t4157153871 ** get_address_of_cam_12() { return &___cam_12; }
	inline void set_cam_12(Camera_t4157153871 * value)
	{
		___cam_12 = value;
		Il2CppCodeGenWriteBarrier((&___cam_12), value);
	}

	inline static int32_t get_offset_of_asymmetricFrust_13() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2453563558, ___asymmetricFrust_13)); }
	inline bool get_asymmetricFrust_13() const { return ___asymmetricFrust_13; }
	inline bool* get_address_of_asymmetricFrust_13() { return &___asymmetricFrust_13; }
	inline void set_asymmetricFrust_13(bool value)
	{
		___asymmetricFrust_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICFRUSTUM_T2453563558_H
#ifndef MOUSEBUTTONINPUTUI_T1438106573_H
#define MOUSEBUTTONINPUTUI_T1438106573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.MouseButtonInputUI
struct  MouseButtonInputUI_t1438106573  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/MouseButtonInput SimpleInputNamespace.MouseButtonInputUI::mouseButton
	MouseButtonInput_t2868216434 * ___mouseButton_2;

public:
	inline static int32_t get_offset_of_mouseButton_2() { return static_cast<int32_t>(offsetof(MouseButtonInputUI_t1438106573, ___mouseButton_2)); }
	inline MouseButtonInput_t2868216434 * get_mouseButton_2() const { return ___mouseButton_2; }
	inline MouseButtonInput_t2868216434 ** get_address_of_mouseButton_2() { return &___mouseButton_2; }
	inline void set_mouseButton_2(MouseButtonInput_t2868216434 * value)
	{
		___mouseButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButton_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINPUTUI_T1438106573_H
#ifndef MOUSEBUTTONINPUTKEYBOARD_T4221569981_H
#define MOUSEBUTTONINPUTKEYBOARD_T4221569981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.MouseButtonInputKeyboard
struct  MouseButtonInputKeyboard_t4221569981  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.MouseButtonInputKeyboard::key
	int32_t ___key_2;
	// SimpleInput/MouseButtonInput SimpleInputNamespace.MouseButtonInputKeyboard::mouseButton
	MouseButtonInput_t2868216434 * ___mouseButton_3;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(MouseButtonInputKeyboard_t4221569981, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_mouseButton_3() { return static_cast<int32_t>(offsetof(MouseButtonInputKeyboard_t4221569981, ___mouseButton_3)); }
	inline MouseButtonInput_t2868216434 * get_mouseButton_3() const { return ___mouseButton_3; }
	inline MouseButtonInput_t2868216434 ** get_address_of_mouseButton_3() { return &___mouseButton_3; }
	inline void set_mouseButton_3(MouseButtonInput_t2868216434 * value)
	{
		___mouseButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButton_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINPUTKEYBOARD_T4221569981_H
#ifndef POINTLIGHT_T2259825046_H
#define POINTLIGHT_T2259825046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.PointLight
struct  PointLight_t2259825046  : public LightSource_1_t2585415336
{
public:
	// System.Single FlatLighting.PointLight::Range
	float ___Range_13;
	// UnityEngine.Color FlatLighting.PointLight::LightColor
	Color_t2555686324  ___LightColor_14;
	// UnityEngine.Vector4 FlatLighting.PointLight::LightDistances
	Vector4_t3319028937  ___LightDistances_15;
	// UnityEngine.Vector4 FlatLighting.PointLight::LightIntensities
	Vector4_t3319028937  ___LightIntensities_16;
	// System.Boolean FlatLighting.PointLight::Smooth
	bool ___Smooth_17;
	// System.Boolean FlatLighting.PointLight::isRealTime
	bool ___isRealTime_18;
	// System.Boolean FlatLighting.PointLight::isFirstPass
	bool ___isFirstPass_19;

public:
	inline static int32_t get_offset_of_Range_13() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___Range_13)); }
	inline float get_Range_13() const { return ___Range_13; }
	inline float* get_address_of_Range_13() { return &___Range_13; }
	inline void set_Range_13(float value)
	{
		___Range_13 = value;
	}

	inline static int32_t get_offset_of_LightColor_14() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___LightColor_14)); }
	inline Color_t2555686324  get_LightColor_14() const { return ___LightColor_14; }
	inline Color_t2555686324 * get_address_of_LightColor_14() { return &___LightColor_14; }
	inline void set_LightColor_14(Color_t2555686324  value)
	{
		___LightColor_14 = value;
	}

	inline static int32_t get_offset_of_LightDistances_15() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___LightDistances_15)); }
	inline Vector4_t3319028937  get_LightDistances_15() const { return ___LightDistances_15; }
	inline Vector4_t3319028937 * get_address_of_LightDistances_15() { return &___LightDistances_15; }
	inline void set_LightDistances_15(Vector4_t3319028937  value)
	{
		___LightDistances_15 = value;
	}

	inline static int32_t get_offset_of_LightIntensities_16() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___LightIntensities_16)); }
	inline Vector4_t3319028937  get_LightIntensities_16() const { return ___LightIntensities_16; }
	inline Vector4_t3319028937 * get_address_of_LightIntensities_16() { return &___LightIntensities_16; }
	inline void set_LightIntensities_16(Vector4_t3319028937  value)
	{
		___LightIntensities_16 = value;
	}

	inline static int32_t get_offset_of_Smooth_17() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___Smooth_17)); }
	inline bool get_Smooth_17() const { return ___Smooth_17; }
	inline bool* get_address_of_Smooth_17() { return &___Smooth_17; }
	inline void set_Smooth_17(bool value)
	{
		___Smooth_17 = value;
	}

	inline static int32_t get_offset_of_isRealTime_18() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___isRealTime_18)); }
	inline bool get_isRealTime_18() const { return ___isRealTime_18; }
	inline bool* get_address_of_isRealTime_18() { return &___isRealTime_18; }
	inline void set_isRealTime_18(bool value)
	{
		___isRealTime_18 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_19() { return static_cast<int32_t>(offsetof(PointLight_t2259825046, ___isFirstPass_19)); }
	inline bool get_isFirstPass_19() const { return ___isFirstPass_19; }
	inline bool* get_address_of_isFirstPass_19() { return &___isFirstPass_19; }
	inline void set_isFirstPass_19(bool value)
	{
		___isFirstPass_19 = value;
	}
};

struct PointLight_t2259825046_StaticFields
{
public:
	// System.String FlatLighting.PointLight::pointLightCountProperty
	String_t* ___pointLightCountProperty_7;
	// System.String FlatLighting.PointLight::pointLight0WorldToModelProperty
	String_t* ___pointLight0WorldToModelProperty_8;
	// System.String FlatLighting.PointLight::pointLightColorProperty
	String_t* ___pointLightColorProperty_9;
	// System.String FlatLighting.PointLight::pointLightDistancesProperty
	String_t* ___pointLightDistancesProperty_10;
	// System.String FlatLighting.PointLight::pointLightIntensitiesProperty
	String_t* ___pointLightIntensitiesProperty_11;
	// System.String FlatLighting.PointLight::pointLightSmoothnessProperty
	String_t* ___pointLightSmoothnessProperty_12;
	// UnityEngine.Matrix4x4[] FlatLighting.PointLight::worldToModel
	Matrix4x4U5BU5D_t2302988098* ___worldToModel_20;
	// UnityEngine.Vector4[] FlatLighting.PointLight::distances
	Vector4U5BU5D_t934056436* ___distances_21;
	// UnityEngine.Vector4[] FlatLighting.PointLight::intensities
	Vector4U5BU5D_t934056436* ___intensities_22;
	// System.Single[] FlatLighting.PointLight::smoothness
	SingleU5BU5D_t1444911251* ___smoothness_23;
	// UnityEngine.Vector4[] FlatLighting.PointLight::color
	Vector4U5BU5D_t934056436* ___color_24;

public:
	inline static int32_t get_offset_of_pointLightCountProperty_7() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___pointLightCountProperty_7)); }
	inline String_t* get_pointLightCountProperty_7() const { return ___pointLightCountProperty_7; }
	inline String_t** get_address_of_pointLightCountProperty_7() { return &___pointLightCountProperty_7; }
	inline void set_pointLightCountProperty_7(String_t* value)
	{
		___pointLightCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightCountProperty_7), value);
	}

	inline static int32_t get_offset_of_pointLight0WorldToModelProperty_8() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___pointLight0WorldToModelProperty_8)); }
	inline String_t* get_pointLight0WorldToModelProperty_8() const { return ___pointLight0WorldToModelProperty_8; }
	inline String_t** get_address_of_pointLight0WorldToModelProperty_8() { return &___pointLight0WorldToModelProperty_8; }
	inline void set_pointLight0WorldToModelProperty_8(String_t* value)
	{
		___pointLight0WorldToModelProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___pointLight0WorldToModelProperty_8), value);
	}

	inline static int32_t get_offset_of_pointLightColorProperty_9() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___pointLightColorProperty_9)); }
	inline String_t* get_pointLightColorProperty_9() const { return ___pointLightColorProperty_9; }
	inline String_t** get_address_of_pointLightColorProperty_9() { return &___pointLightColorProperty_9; }
	inline void set_pointLightColorProperty_9(String_t* value)
	{
		___pointLightColorProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightColorProperty_9), value);
	}

	inline static int32_t get_offset_of_pointLightDistancesProperty_10() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___pointLightDistancesProperty_10)); }
	inline String_t* get_pointLightDistancesProperty_10() const { return ___pointLightDistancesProperty_10; }
	inline String_t** get_address_of_pointLightDistancesProperty_10() { return &___pointLightDistancesProperty_10; }
	inline void set_pointLightDistancesProperty_10(String_t* value)
	{
		___pointLightDistancesProperty_10 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightDistancesProperty_10), value);
	}

	inline static int32_t get_offset_of_pointLightIntensitiesProperty_11() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___pointLightIntensitiesProperty_11)); }
	inline String_t* get_pointLightIntensitiesProperty_11() const { return ___pointLightIntensitiesProperty_11; }
	inline String_t** get_address_of_pointLightIntensitiesProperty_11() { return &___pointLightIntensitiesProperty_11; }
	inline void set_pointLightIntensitiesProperty_11(String_t* value)
	{
		___pointLightIntensitiesProperty_11 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightIntensitiesProperty_11), value);
	}

	inline static int32_t get_offset_of_pointLightSmoothnessProperty_12() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___pointLightSmoothnessProperty_12)); }
	inline String_t* get_pointLightSmoothnessProperty_12() const { return ___pointLightSmoothnessProperty_12; }
	inline String_t** get_address_of_pointLightSmoothnessProperty_12() { return &___pointLightSmoothnessProperty_12; }
	inline void set_pointLightSmoothnessProperty_12(String_t* value)
	{
		___pointLightSmoothnessProperty_12 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightSmoothnessProperty_12), value);
	}

	inline static int32_t get_offset_of_worldToModel_20() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___worldToModel_20)); }
	inline Matrix4x4U5BU5D_t2302988098* get_worldToModel_20() const { return ___worldToModel_20; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_worldToModel_20() { return &___worldToModel_20; }
	inline void set_worldToModel_20(Matrix4x4U5BU5D_t2302988098* value)
	{
		___worldToModel_20 = value;
		Il2CppCodeGenWriteBarrier((&___worldToModel_20), value);
	}

	inline static int32_t get_offset_of_distances_21() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___distances_21)); }
	inline Vector4U5BU5D_t934056436* get_distances_21() const { return ___distances_21; }
	inline Vector4U5BU5D_t934056436** get_address_of_distances_21() { return &___distances_21; }
	inline void set_distances_21(Vector4U5BU5D_t934056436* value)
	{
		___distances_21 = value;
		Il2CppCodeGenWriteBarrier((&___distances_21), value);
	}

	inline static int32_t get_offset_of_intensities_22() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___intensities_22)); }
	inline Vector4U5BU5D_t934056436* get_intensities_22() const { return ___intensities_22; }
	inline Vector4U5BU5D_t934056436** get_address_of_intensities_22() { return &___intensities_22; }
	inline void set_intensities_22(Vector4U5BU5D_t934056436* value)
	{
		___intensities_22 = value;
		Il2CppCodeGenWriteBarrier((&___intensities_22), value);
	}

	inline static int32_t get_offset_of_smoothness_23() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___smoothness_23)); }
	inline SingleU5BU5D_t1444911251* get_smoothness_23() const { return ___smoothness_23; }
	inline SingleU5BU5D_t1444911251** get_address_of_smoothness_23() { return &___smoothness_23; }
	inline void set_smoothness_23(SingleU5BU5D_t1444911251* value)
	{
		___smoothness_23 = value;
		Il2CppCodeGenWriteBarrier((&___smoothness_23), value);
	}

	inline static int32_t get_offset_of_color_24() { return static_cast<int32_t>(offsetof(PointLight_t2259825046_StaticFields, ___color_24)); }
	inline Vector4U5BU5D_t934056436* get_color_24() const { return ___color_24; }
	inline Vector4U5BU5D_t934056436** get_address_of_color_24() { return &___color_24; }
	inline void set_color_24(Vector4U5BU5D_t934056436* value)
	{
		___color_24 = value;
		Il2CppCodeGenWriteBarrier((&___color_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTLIGHT_T2259825046_H
#ifndef AUDIOMANAGER_T3267510698_H
#define AUDIOMANAGER_T3267510698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t3267510698  : public Singleton_1_t149390147
{
public:
	// UnityEngine.AudioSource AudioManager::audiosource
	AudioSource_t3935305588 * ___audiosource_3;
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> AudioManager::soundEffects
	List_1_t857997111 * ___soundEffects_4;

public:
	inline static int32_t get_offset_of_audiosource_3() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698, ___audiosource_3)); }
	inline AudioSource_t3935305588 * get_audiosource_3() const { return ___audiosource_3; }
	inline AudioSource_t3935305588 ** get_address_of_audiosource_3() { return &___audiosource_3; }
	inline void set_audiosource_3(AudioSource_t3935305588 * value)
	{
		___audiosource_3 = value;
		Il2CppCodeGenWriteBarrier((&___audiosource_3), value);
	}

	inline static int32_t get_offset_of_soundEffects_4() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698, ___soundEffects_4)); }
	inline List_1_t857997111 * get_soundEffects_4() const { return ___soundEffects_4; }
	inline List_1_t857997111 ** get_address_of_soundEffects_4() { return &___soundEffects_4; }
	inline void set_soundEffects_4(List_1_t857997111 * value)
	{
		___soundEffects_4 = value;
		Il2CppCodeGenWriteBarrier((&___soundEffects_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T3267510698_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public Singleton_1_t2713370399
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::Collectables
	List_1_t2585711361 * ___Collectables_3;
	// System.Int32 GameManager::goal
	int32_t ___goal_4;
	// TMPro.TextMeshPro GameManager::numberOfCellstokill
	TextMeshPro_t2393593166 * ___numberOfCellstokill_5;
	// UnityEngine.GameObject GameManager::player
	GameObject_t1113636619 * ___player_6;
	// System.Collections.Generic.List`1<System.String> GameManager::framesText
	List_1_t3319525431 * ___framesText_7;
	// System.Int32 GameManager::framesIterator
	int32_t ___framesIterator_8;
	// UnityEngine.GameObject GameManager::fireworks
	GameObject_t1113636619 * ___fireworks_9;
	// TMPro.TextMeshPro GameManager::TutorialFrames
	TextMeshPro_t2393593166 * ___TutorialFrames_10;
	// TMPro.TextMeshPro GameManager::GameFrames
	TextMeshPro_t2393593166 * ___GameFrames_11;
	// UnityEngine.GameObject GameManager::TrackedItem
	GameObject_t1113636619 * ___TrackedItem_12;
	// UnityEngine.GameObject GameManager::FakeSphere
	GameObject_t1113636619 * ___FakeSphere_13;
	// UnityEngine.Transform GameManager::Camera
	Transform_t3600365921 * ___Camera_14;
	// UnityEngine.GameObject GameManager::BloodSplatter
	GameObject_t1113636619 * ___BloodSplatter_15;
	// UnityEngine.GameObject GameManager::RealSphere
	GameObject_t1113636619 * ___RealSphere_16;
	// AudioManager GameManager::am
	AudioManager_t3267510698 * ___am_17;
	// System.Boolean GameManager::SlidesON
	bool ___SlidesON_18;
	// System.Boolean GameManager::MovementTutorialStarted
	bool ___MovementTutorialStarted_19;
	// System.Boolean GameManager::MovementTutorialFinished
	bool ___MovementTutorialFinished_20;
	// System.Boolean GameManager::GameON
	bool ___GameON_21;
	// System.Boolean GameManager::ResetPlayer
	bool ___ResetPlayer_22;
	// UnityEngine.GameObject GameManager::StartButton
	GameObject_t1113636619 * ___StartButton_23;
	// UnityEngine.GameObject GameManager::ResetBallButton
	GameObject_t1113636619 * ___ResetBallButton_24;
	// UnityEngine.GameObject GameManager::ArrowsButton
	GameObject_t1113636619 * ___ArrowsButton_25;

public:
	inline static int32_t get_offset_of_Collectables_3() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Collectables_3)); }
	inline List_1_t2585711361 * get_Collectables_3() const { return ___Collectables_3; }
	inline List_1_t2585711361 ** get_address_of_Collectables_3() { return &___Collectables_3; }
	inline void set_Collectables_3(List_1_t2585711361 * value)
	{
		___Collectables_3 = value;
		Il2CppCodeGenWriteBarrier((&___Collectables_3), value);
	}

	inline static int32_t get_offset_of_goal_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___goal_4)); }
	inline int32_t get_goal_4() const { return ___goal_4; }
	inline int32_t* get_address_of_goal_4() { return &___goal_4; }
	inline void set_goal_4(int32_t value)
	{
		___goal_4 = value;
	}

	inline static int32_t get_offset_of_numberOfCellstokill_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___numberOfCellstokill_5)); }
	inline TextMeshPro_t2393593166 * get_numberOfCellstokill_5() const { return ___numberOfCellstokill_5; }
	inline TextMeshPro_t2393593166 ** get_address_of_numberOfCellstokill_5() { return &___numberOfCellstokill_5; }
	inline void set_numberOfCellstokill_5(TextMeshPro_t2393593166 * value)
	{
		___numberOfCellstokill_5 = value;
		Il2CppCodeGenWriteBarrier((&___numberOfCellstokill_5), value);
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___player_6)); }
	inline GameObject_t1113636619 * get_player_6() const { return ___player_6; }
	inline GameObject_t1113636619 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GameObject_t1113636619 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_framesText_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___framesText_7)); }
	inline List_1_t3319525431 * get_framesText_7() const { return ___framesText_7; }
	inline List_1_t3319525431 ** get_address_of_framesText_7() { return &___framesText_7; }
	inline void set_framesText_7(List_1_t3319525431 * value)
	{
		___framesText_7 = value;
		Il2CppCodeGenWriteBarrier((&___framesText_7), value);
	}

	inline static int32_t get_offset_of_framesIterator_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___framesIterator_8)); }
	inline int32_t get_framesIterator_8() const { return ___framesIterator_8; }
	inline int32_t* get_address_of_framesIterator_8() { return &___framesIterator_8; }
	inline void set_framesIterator_8(int32_t value)
	{
		___framesIterator_8 = value;
	}

	inline static int32_t get_offset_of_fireworks_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___fireworks_9)); }
	inline GameObject_t1113636619 * get_fireworks_9() const { return ___fireworks_9; }
	inline GameObject_t1113636619 ** get_address_of_fireworks_9() { return &___fireworks_9; }
	inline void set_fireworks_9(GameObject_t1113636619 * value)
	{
		___fireworks_9 = value;
		Il2CppCodeGenWriteBarrier((&___fireworks_9), value);
	}

	inline static int32_t get_offset_of_TutorialFrames_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___TutorialFrames_10)); }
	inline TextMeshPro_t2393593166 * get_TutorialFrames_10() const { return ___TutorialFrames_10; }
	inline TextMeshPro_t2393593166 ** get_address_of_TutorialFrames_10() { return &___TutorialFrames_10; }
	inline void set_TutorialFrames_10(TextMeshPro_t2393593166 * value)
	{
		___TutorialFrames_10 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialFrames_10), value);
	}

	inline static int32_t get_offset_of_GameFrames_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___GameFrames_11)); }
	inline TextMeshPro_t2393593166 * get_GameFrames_11() const { return ___GameFrames_11; }
	inline TextMeshPro_t2393593166 ** get_address_of_GameFrames_11() { return &___GameFrames_11; }
	inline void set_GameFrames_11(TextMeshPro_t2393593166 * value)
	{
		___GameFrames_11 = value;
		Il2CppCodeGenWriteBarrier((&___GameFrames_11), value);
	}

	inline static int32_t get_offset_of_TrackedItem_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___TrackedItem_12)); }
	inline GameObject_t1113636619 * get_TrackedItem_12() const { return ___TrackedItem_12; }
	inline GameObject_t1113636619 ** get_address_of_TrackedItem_12() { return &___TrackedItem_12; }
	inline void set_TrackedItem_12(GameObject_t1113636619 * value)
	{
		___TrackedItem_12 = value;
		Il2CppCodeGenWriteBarrier((&___TrackedItem_12), value);
	}

	inline static int32_t get_offset_of_FakeSphere_13() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___FakeSphere_13)); }
	inline GameObject_t1113636619 * get_FakeSphere_13() const { return ___FakeSphere_13; }
	inline GameObject_t1113636619 ** get_address_of_FakeSphere_13() { return &___FakeSphere_13; }
	inline void set_FakeSphere_13(GameObject_t1113636619 * value)
	{
		___FakeSphere_13 = value;
		Il2CppCodeGenWriteBarrier((&___FakeSphere_13), value);
	}

	inline static int32_t get_offset_of_Camera_14() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Camera_14)); }
	inline Transform_t3600365921 * get_Camera_14() const { return ___Camera_14; }
	inline Transform_t3600365921 ** get_address_of_Camera_14() { return &___Camera_14; }
	inline void set_Camera_14(Transform_t3600365921 * value)
	{
		___Camera_14 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_14), value);
	}

	inline static int32_t get_offset_of_BloodSplatter_15() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___BloodSplatter_15)); }
	inline GameObject_t1113636619 * get_BloodSplatter_15() const { return ___BloodSplatter_15; }
	inline GameObject_t1113636619 ** get_address_of_BloodSplatter_15() { return &___BloodSplatter_15; }
	inline void set_BloodSplatter_15(GameObject_t1113636619 * value)
	{
		___BloodSplatter_15 = value;
		Il2CppCodeGenWriteBarrier((&___BloodSplatter_15), value);
	}

	inline static int32_t get_offset_of_RealSphere_16() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___RealSphere_16)); }
	inline GameObject_t1113636619 * get_RealSphere_16() const { return ___RealSphere_16; }
	inline GameObject_t1113636619 ** get_address_of_RealSphere_16() { return &___RealSphere_16; }
	inline void set_RealSphere_16(GameObject_t1113636619 * value)
	{
		___RealSphere_16 = value;
		Il2CppCodeGenWriteBarrier((&___RealSphere_16), value);
	}

	inline static int32_t get_offset_of_am_17() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___am_17)); }
	inline AudioManager_t3267510698 * get_am_17() const { return ___am_17; }
	inline AudioManager_t3267510698 ** get_address_of_am_17() { return &___am_17; }
	inline void set_am_17(AudioManager_t3267510698 * value)
	{
		___am_17 = value;
		Il2CppCodeGenWriteBarrier((&___am_17), value);
	}

	inline static int32_t get_offset_of_SlidesON_18() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___SlidesON_18)); }
	inline bool get_SlidesON_18() const { return ___SlidesON_18; }
	inline bool* get_address_of_SlidesON_18() { return &___SlidesON_18; }
	inline void set_SlidesON_18(bool value)
	{
		___SlidesON_18 = value;
	}

	inline static int32_t get_offset_of_MovementTutorialStarted_19() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___MovementTutorialStarted_19)); }
	inline bool get_MovementTutorialStarted_19() const { return ___MovementTutorialStarted_19; }
	inline bool* get_address_of_MovementTutorialStarted_19() { return &___MovementTutorialStarted_19; }
	inline void set_MovementTutorialStarted_19(bool value)
	{
		___MovementTutorialStarted_19 = value;
	}

	inline static int32_t get_offset_of_MovementTutorialFinished_20() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___MovementTutorialFinished_20)); }
	inline bool get_MovementTutorialFinished_20() const { return ___MovementTutorialFinished_20; }
	inline bool* get_address_of_MovementTutorialFinished_20() { return &___MovementTutorialFinished_20; }
	inline void set_MovementTutorialFinished_20(bool value)
	{
		___MovementTutorialFinished_20 = value;
	}

	inline static int32_t get_offset_of_GameON_21() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___GameON_21)); }
	inline bool get_GameON_21() const { return ___GameON_21; }
	inline bool* get_address_of_GameON_21() { return &___GameON_21; }
	inline void set_GameON_21(bool value)
	{
		___GameON_21 = value;
	}

	inline static int32_t get_offset_of_ResetPlayer_22() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ResetPlayer_22)); }
	inline bool get_ResetPlayer_22() const { return ___ResetPlayer_22; }
	inline bool* get_address_of_ResetPlayer_22() { return &___ResetPlayer_22; }
	inline void set_ResetPlayer_22(bool value)
	{
		___ResetPlayer_22 = value;
	}

	inline static int32_t get_offset_of_StartButton_23() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___StartButton_23)); }
	inline GameObject_t1113636619 * get_StartButton_23() const { return ___StartButton_23; }
	inline GameObject_t1113636619 ** get_address_of_StartButton_23() { return &___StartButton_23; }
	inline void set_StartButton_23(GameObject_t1113636619 * value)
	{
		___StartButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___StartButton_23), value);
	}

	inline static int32_t get_offset_of_ResetBallButton_24() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ResetBallButton_24)); }
	inline GameObject_t1113636619 * get_ResetBallButton_24() const { return ___ResetBallButton_24; }
	inline GameObject_t1113636619 ** get_address_of_ResetBallButton_24() { return &___ResetBallButton_24; }
	inline void set_ResetBallButton_24(GameObject_t1113636619 * value)
	{
		___ResetBallButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___ResetBallButton_24), value);
	}

	inline static int32_t get_offset_of_ArrowsButton_25() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ArrowsButton_25)); }
	inline GameObject_t1113636619 * get_ArrowsButton_25() const { return ___ArrowsButton_25; }
	inline GameObject_t1113636619 ** get_address_of_ArrowsButton_25() { return &___ArrowsButton_25; }
	inline void set_ArrowsButton_25(GameObject_t1113636619 * value)
	{
		___ArrowsButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___ArrowsButton_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef SHADOWPROJECTOR_T191975869_H
#define SHADOWPROJECTOR_T191975869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.ShadowProjector
struct  ShadowProjector_t191975869  : public LightSource_1_t517566159
{
public:
	// UnityEngine.Color FlatLighting.ShadowProjector::ShadowColor
	Color_t2555686324  ___ShadowColor_15;
	// UnityEngine.Shader FlatLighting.ShadowProjector::ShadowMapShader
	Shader_t4151988712 * ___ShadowMapShader_16;
	// System.Single FlatLighting.ShadowProjector::Bias
	float ___Bias_17;
	// System.Single FlatLighting.ShadowProjector::ShadowBlur
	float ___ShadowBlur_18;
	// System.Int32 FlatLighting.ShadowProjector::MemoryToUse
	int32_t ___MemoryToUse_19;
	// System.Boolean FlatLighting.ShadowProjector::isRealTime
	bool ___isRealTime_20;
	// System.Boolean FlatLighting.ShadowProjector::DebugShadowMappingTexture
	bool ___DebugShadowMappingTexture_21;
	// System.Boolean FlatLighting.ShadowProjector::isFirstPass
	bool ___isFirstPass_22;
	// UnityEngine.GameObject FlatLighting.ShadowProjector::debugObject
	GameObject_t1113636619 * ___debugObject_23;
	// UnityEngine.Material FlatLighting.ShadowProjector::debugMaterial
	Material_t340375123 * ___debugMaterial_24;
	// UnityEngine.Camera FlatLighting.ShadowProjector::ShadowCamera
	Camera_t4157153871 * ___ShadowCamera_25;
	// System.Boolean FlatLighting.ShadowProjector::ShadowMapRequested
	bool ___ShadowMapRequested_26;
	// UnityEngine.Matrix4x4 FlatLighting.ShadowProjector::ShadowMapOffset
	Matrix4x4_t1817901843  ___ShadowMapOffset_27;
	// UnityEngine.RenderTexture FlatLighting.ShadowProjector::ShadowMapTexture
	RenderTexture_t2108887433 * ___ShadowMapTexture_28;

public:
	inline static int32_t get_offset_of_ShadowColor_15() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowColor_15)); }
	inline Color_t2555686324  get_ShadowColor_15() const { return ___ShadowColor_15; }
	inline Color_t2555686324 * get_address_of_ShadowColor_15() { return &___ShadowColor_15; }
	inline void set_ShadowColor_15(Color_t2555686324  value)
	{
		___ShadowColor_15 = value;
	}

	inline static int32_t get_offset_of_ShadowMapShader_16() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowMapShader_16)); }
	inline Shader_t4151988712 * get_ShadowMapShader_16() const { return ___ShadowMapShader_16; }
	inline Shader_t4151988712 ** get_address_of_ShadowMapShader_16() { return &___ShadowMapShader_16; }
	inline void set_ShadowMapShader_16(Shader_t4151988712 * value)
	{
		___ShadowMapShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___ShadowMapShader_16), value);
	}

	inline static int32_t get_offset_of_Bias_17() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___Bias_17)); }
	inline float get_Bias_17() const { return ___Bias_17; }
	inline float* get_address_of_Bias_17() { return &___Bias_17; }
	inline void set_Bias_17(float value)
	{
		___Bias_17 = value;
	}

	inline static int32_t get_offset_of_ShadowBlur_18() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowBlur_18)); }
	inline float get_ShadowBlur_18() const { return ___ShadowBlur_18; }
	inline float* get_address_of_ShadowBlur_18() { return &___ShadowBlur_18; }
	inline void set_ShadowBlur_18(float value)
	{
		___ShadowBlur_18 = value;
	}

	inline static int32_t get_offset_of_MemoryToUse_19() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___MemoryToUse_19)); }
	inline int32_t get_MemoryToUse_19() const { return ___MemoryToUse_19; }
	inline int32_t* get_address_of_MemoryToUse_19() { return &___MemoryToUse_19; }
	inline void set_MemoryToUse_19(int32_t value)
	{
		___MemoryToUse_19 = value;
	}

	inline static int32_t get_offset_of_isRealTime_20() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___isRealTime_20)); }
	inline bool get_isRealTime_20() const { return ___isRealTime_20; }
	inline bool* get_address_of_isRealTime_20() { return &___isRealTime_20; }
	inline void set_isRealTime_20(bool value)
	{
		___isRealTime_20 = value;
	}

	inline static int32_t get_offset_of_DebugShadowMappingTexture_21() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___DebugShadowMappingTexture_21)); }
	inline bool get_DebugShadowMappingTexture_21() const { return ___DebugShadowMappingTexture_21; }
	inline bool* get_address_of_DebugShadowMappingTexture_21() { return &___DebugShadowMappingTexture_21; }
	inline void set_DebugShadowMappingTexture_21(bool value)
	{
		___DebugShadowMappingTexture_21 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_22() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___isFirstPass_22)); }
	inline bool get_isFirstPass_22() const { return ___isFirstPass_22; }
	inline bool* get_address_of_isFirstPass_22() { return &___isFirstPass_22; }
	inline void set_isFirstPass_22(bool value)
	{
		___isFirstPass_22 = value;
	}

	inline static int32_t get_offset_of_debugObject_23() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___debugObject_23)); }
	inline GameObject_t1113636619 * get_debugObject_23() const { return ___debugObject_23; }
	inline GameObject_t1113636619 ** get_address_of_debugObject_23() { return &___debugObject_23; }
	inline void set_debugObject_23(GameObject_t1113636619 * value)
	{
		___debugObject_23 = value;
		Il2CppCodeGenWriteBarrier((&___debugObject_23), value);
	}

	inline static int32_t get_offset_of_debugMaterial_24() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___debugMaterial_24)); }
	inline Material_t340375123 * get_debugMaterial_24() const { return ___debugMaterial_24; }
	inline Material_t340375123 ** get_address_of_debugMaterial_24() { return &___debugMaterial_24; }
	inline void set_debugMaterial_24(Material_t340375123 * value)
	{
		___debugMaterial_24 = value;
		Il2CppCodeGenWriteBarrier((&___debugMaterial_24), value);
	}

	inline static int32_t get_offset_of_ShadowCamera_25() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowCamera_25)); }
	inline Camera_t4157153871 * get_ShadowCamera_25() const { return ___ShadowCamera_25; }
	inline Camera_t4157153871 ** get_address_of_ShadowCamera_25() { return &___ShadowCamera_25; }
	inline void set_ShadowCamera_25(Camera_t4157153871 * value)
	{
		___ShadowCamera_25 = value;
		Il2CppCodeGenWriteBarrier((&___ShadowCamera_25), value);
	}

	inline static int32_t get_offset_of_ShadowMapRequested_26() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowMapRequested_26)); }
	inline bool get_ShadowMapRequested_26() const { return ___ShadowMapRequested_26; }
	inline bool* get_address_of_ShadowMapRequested_26() { return &___ShadowMapRequested_26; }
	inline void set_ShadowMapRequested_26(bool value)
	{
		___ShadowMapRequested_26 = value;
	}

	inline static int32_t get_offset_of_ShadowMapOffset_27() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowMapOffset_27)); }
	inline Matrix4x4_t1817901843  get_ShadowMapOffset_27() const { return ___ShadowMapOffset_27; }
	inline Matrix4x4_t1817901843 * get_address_of_ShadowMapOffset_27() { return &___ShadowMapOffset_27; }
	inline void set_ShadowMapOffset_27(Matrix4x4_t1817901843  value)
	{
		___ShadowMapOffset_27 = value;
	}

	inline static int32_t get_offset_of_ShadowMapTexture_28() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869, ___ShadowMapTexture_28)); }
	inline RenderTexture_t2108887433 * get_ShadowMapTexture_28() const { return ___ShadowMapTexture_28; }
	inline RenderTexture_t2108887433 ** get_address_of_ShadowMapTexture_28() { return &___ShadowMapTexture_28; }
	inline void set_ShadowMapTexture_28(RenderTexture_t2108887433 * value)
	{
		___ShadowMapTexture_28 = value;
		Il2CppCodeGenWriteBarrier((&___ShadowMapTexture_28), value);
	}
};

struct ShadowProjector_t191975869_StaticFields
{
public:
	// System.String FlatLighting.ShadowProjector::shadowProjectorCountProperty
	String_t* ___shadowProjectorCountProperty_7;
	// System.String FlatLighting.ShadowProjector::shadowMapMatrixProperty
	String_t* ___shadowMapMatrixProperty_8;
	// System.String FlatLighting.ShadowProjector::shadowModelToViewProperty
	String_t* ___shadowModelToViewProperty_9;
	// System.String FlatLighting.ShadowProjector::shadowColorProperty
	String_t* ___shadowColorProperty_10;
	// System.String FlatLighting.ShadowProjector::shadowBlurProperty
	String_t* ___shadowBlurProperty_11;
	// System.String FlatLighting.ShadowProjector::shadowCameraSettingsProperty
	String_t* ___shadowCameraSettingsProperty_12;
	// System.String FlatLighting.ShadowProjector::shadowTextureProperty
	String_t* ___shadowTextureProperty_13;
	// System.String FlatLighting.ShadowProjector::DEBUG_SHADOW_TEXTURE_OBJECT_NAME
	String_t* ___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14;
	// UnityEngine.Matrix4x4[] FlatLighting.ShadowProjector::mapMatrix
	Matrix4x4U5BU5D_t2302988098* ___mapMatrix_29;
	// UnityEngine.Matrix4x4[] FlatLighting.ShadowProjector::modelToView
	Matrix4x4U5BU5D_t2302988098* ___modelToView_30;
	// UnityEngine.Vector4[] FlatLighting.ShadowProjector::cameraSettings
	Vector4U5BU5D_t934056436* ___cameraSettings_31;
	// System.Single[] FlatLighting.ShadowProjector::blur
	SingleU5BU5D_t1444911251* ___blur_32;
	// UnityEngine.Vector4[] FlatLighting.ShadowProjector::shadowColor
	Vector4U5BU5D_t934056436* ___shadowColor_33;

public:
	inline static int32_t get_offset_of_shadowProjectorCountProperty_7() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowProjectorCountProperty_7)); }
	inline String_t* get_shadowProjectorCountProperty_7() const { return ___shadowProjectorCountProperty_7; }
	inline String_t** get_address_of_shadowProjectorCountProperty_7() { return &___shadowProjectorCountProperty_7; }
	inline void set_shadowProjectorCountProperty_7(String_t* value)
	{
		___shadowProjectorCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___shadowProjectorCountProperty_7), value);
	}

	inline static int32_t get_offset_of_shadowMapMatrixProperty_8() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowMapMatrixProperty_8)); }
	inline String_t* get_shadowMapMatrixProperty_8() const { return ___shadowMapMatrixProperty_8; }
	inline String_t** get_address_of_shadowMapMatrixProperty_8() { return &___shadowMapMatrixProperty_8; }
	inline void set_shadowMapMatrixProperty_8(String_t* value)
	{
		___shadowMapMatrixProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___shadowMapMatrixProperty_8), value);
	}

	inline static int32_t get_offset_of_shadowModelToViewProperty_9() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowModelToViewProperty_9)); }
	inline String_t* get_shadowModelToViewProperty_9() const { return ___shadowModelToViewProperty_9; }
	inline String_t** get_address_of_shadowModelToViewProperty_9() { return &___shadowModelToViewProperty_9; }
	inline void set_shadowModelToViewProperty_9(String_t* value)
	{
		___shadowModelToViewProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___shadowModelToViewProperty_9), value);
	}

	inline static int32_t get_offset_of_shadowColorProperty_10() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowColorProperty_10)); }
	inline String_t* get_shadowColorProperty_10() const { return ___shadowColorProperty_10; }
	inline String_t** get_address_of_shadowColorProperty_10() { return &___shadowColorProperty_10; }
	inline void set_shadowColorProperty_10(String_t* value)
	{
		___shadowColorProperty_10 = value;
		Il2CppCodeGenWriteBarrier((&___shadowColorProperty_10), value);
	}

	inline static int32_t get_offset_of_shadowBlurProperty_11() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowBlurProperty_11)); }
	inline String_t* get_shadowBlurProperty_11() const { return ___shadowBlurProperty_11; }
	inline String_t** get_address_of_shadowBlurProperty_11() { return &___shadowBlurProperty_11; }
	inline void set_shadowBlurProperty_11(String_t* value)
	{
		___shadowBlurProperty_11 = value;
		Il2CppCodeGenWriteBarrier((&___shadowBlurProperty_11), value);
	}

	inline static int32_t get_offset_of_shadowCameraSettingsProperty_12() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowCameraSettingsProperty_12)); }
	inline String_t* get_shadowCameraSettingsProperty_12() const { return ___shadowCameraSettingsProperty_12; }
	inline String_t** get_address_of_shadowCameraSettingsProperty_12() { return &___shadowCameraSettingsProperty_12; }
	inline void set_shadowCameraSettingsProperty_12(String_t* value)
	{
		___shadowCameraSettingsProperty_12 = value;
		Il2CppCodeGenWriteBarrier((&___shadowCameraSettingsProperty_12), value);
	}

	inline static int32_t get_offset_of_shadowTextureProperty_13() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowTextureProperty_13)); }
	inline String_t* get_shadowTextureProperty_13() const { return ___shadowTextureProperty_13; }
	inline String_t** get_address_of_shadowTextureProperty_13() { return &___shadowTextureProperty_13; }
	inline void set_shadowTextureProperty_13(String_t* value)
	{
		___shadowTextureProperty_13 = value;
		Il2CppCodeGenWriteBarrier((&___shadowTextureProperty_13), value);
	}

	inline static int32_t get_offset_of_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14)); }
	inline String_t* get_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14() const { return ___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14; }
	inline String_t** get_address_of_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14() { return &___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14; }
	inline void set_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14(String_t* value)
	{
		___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14 = value;
		Il2CppCodeGenWriteBarrier((&___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14), value);
	}

	inline static int32_t get_offset_of_mapMatrix_29() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___mapMatrix_29)); }
	inline Matrix4x4U5BU5D_t2302988098* get_mapMatrix_29() const { return ___mapMatrix_29; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_mapMatrix_29() { return &___mapMatrix_29; }
	inline void set_mapMatrix_29(Matrix4x4U5BU5D_t2302988098* value)
	{
		___mapMatrix_29 = value;
		Il2CppCodeGenWriteBarrier((&___mapMatrix_29), value);
	}

	inline static int32_t get_offset_of_modelToView_30() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___modelToView_30)); }
	inline Matrix4x4U5BU5D_t2302988098* get_modelToView_30() const { return ___modelToView_30; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_modelToView_30() { return &___modelToView_30; }
	inline void set_modelToView_30(Matrix4x4U5BU5D_t2302988098* value)
	{
		___modelToView_30 = value;
		Il2CppCodeGenWriteBarrier((&___modelToView_30), value);
	}

	inline static int32_t get_offset_of_cameraSettings_31() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___cameraSettings_31)); }
	inline Vector4U5BU5D_t934056436* get_cameraSettings_31() const { return ___cameraSettings_31; }
	inline Vector4U5BU5D_t934056436** get_address_of_cameraSettings_31() { return &___cameraSettings_31; }
	inline void set_cameraSettings_31(Vector4U5BU5D_t934056436* value)
	{
		___cameraSettings_31 = value;
		Il2CppCodeGenWriteBarrier((&___cameraSettings_31), value);
	}

	inline static int32_t get_offset_of_blur_32() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___blur_32)); }
	inline SingleU5BU5D_t1444911251* get_blur_32() const { return ___blur_32; }
	inline SingleU5BU5D_t1444911251** get_address_of_blur_32() { return &___blur_32; }
	inline void set_blur_32(SingleU5BU5D_t1444911251* value)
	{
		___blur_32 = value;
		Il2CppCodeGenWriteBarrier((&___blur_32), value);
	}

	inline static int32_t get_offset_of_shadowColor_33() { return static_cast<int32_t>(offsetof(ShadowProjector_t191975869_StaticFields, ___shadowColor_33)); }
	inline Vector4U5BU5D_t934056436* get_shadowColor_33() const { return ___shadowColor_33; }
	inline Vector4U5BU5D_t934056436** get_address_of_shadowColor_33() { return &___shadowColor_33; }
	inline void set_shadowColor_33(Vector4U5BU5D_t934056436* value)
	{
		___shadowColor_33 = value;
		Il2CppCodeGenWriteBarrier((&___shadowColor_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWPROJECTOR_T191975869_H
#ifndef DIRECTIONALLIGHT_T3737659866_H
#define DIRECTIONALLIGHT_T3737659866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.DirectionalLight
struct  DirectionalLight_t3737659866  : public LightSource_1_t4063250156
{
public:
	// System.Boolean FlatLighting.DirectionalLight::isRealTime
	bool ___isRealTime_10;
	// UnityEngine.Color FlatLighting.DirectionalLight::LightColor
	Color_t2555686324  ___LightColor_11;
	// System.Boolean FlatLighting.DirectionalLight::isFirstPass
	bool ___isFirstPass_12;

public:
	inline static int32_t get_offset_of_isRealTime_10() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866, ___isRealTime_10)); }
	inline bool get_isRealTime_10() const { return ___isRealTime_10; }
	inline bool* get_address_of_isRealTime_10() { return &___isRealTime_10; }
	inline void set_isRealTime_10(bool value)
	{
		___isRealTime_10 = value;
	}

	inline static int32_t get_offset_of_LightColor_11() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866, ___LightColor_11)); }
	inline Color_t2555686324  get_LightColor_11() const { return ___LightColor_11; }
	inline Color_t2555686324 * get_address_of_LightColor_11() { return &___LightColor_11; }
	inline void set_LightColor_11(Color_t2555686324  value)
	{
		___LightColor_11 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_12() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866, ___isFirstPass_12)); }
	inline bool get_isFirstPass_12() const { return ___isFirstPass_12; }
	inline bool* get_address_of_isFirstPass_12() { return &___isFirstPass_12; }
	inline void set_isFirstPass_12(bool value)
	{
		___isFirstPass_12 = value;
	}
};

struct DirectionalLight_t3737659866_StaticFields
{
public:
	// System.String FlatLighting.DirectionalLight::directionalLightCountProperty
	String_t* ___directionalLightCountProperty_7;
	// System.String FlatLighting.DirectionalLight::directionalLightColorProperty
	String_t* ___directionalLightColorProperty_8;
	// System.String FlatLighting.DirectionalLight::directionalLightForwardProperty
	String_t* ___directionalLightForwardProperty_9;
	// UnityEngine.Vector4[] FlatLighting.DirectionalLight::forward
	Vector4U5BU5D_t934056436* ___forward_13;
	// UnityEngine.Vector4[] FlatLighting.DirectionalLight::color
	Vector4U5BU5D_t934056436* ___color_14;

public:
	inline static int32_t get_offset_of_directionalLightCountProperty_7() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866_StaticFields, ___directionalLightCountProperty_7)); }
	inline String_t* get_directionalLightCountProperty_7() const { return ___directionalLightCountProperty_7; }
	inline String_t** get_address_of_directionalLightCountProperty_7() { return &___directionalLightCountProperty_7; }
	inline void set_directionalLightCountProperty_7(String_t* value)
	{
		___directionalLightCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___directionalLightCountProperty_7), value);
	}

	inline static int32_t get_offset_of_directionalLightColorProperty_8() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866_StaticFields, ___directionalLightColorProperty_8)); }
	inline String_t* get_directionalLightColorProperty_8() const { return ___directionalLightColorProperty_8; }
	inline String_t** get_address_of_directionalLightColorProperty_8() { return &___directionalLightColorProperty_8; }
	inline void set_directionalLightColorProperty_8(String_t* value)
	{
		___directionalLightColorProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___directionalLightColorProperty_8), value);
	}

	inline static int32_t get_offset_of_directionalLightForwardProperty_9() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866_StaticFields, ___directionalLightForwardProperty_9)); }
	inline String_t* get_directionalLightForwardProperty_9() const { return ___directionalLightForwardProperty_9; }
	inline String_t** get_address_of_directionalLightForwardProperty_9() { return &___directionalLightForwardProperty_9; }
	inline void set_directionalLightForwardProperty_9(String_t* value)
	{
		___directionalLightForwardProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___directionalLightForwardProperty_9), value);
	}

	inline static int32_t get_offset_of_forward_13() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866_StaticFields, ___forward_13)); }
	inline Vector4U5BU5D_t934056436* get_forward_13() const { return ___forward_13; }
	inline Vector4U5BU5D_t934056436** get_address_of_forward_13() { return &___forward_13; }
	inline void set_forward_13(Vector4U5BU5D_t934056436* value)
	{
		___forward_13 = value;
		Il2CppCodeGenWriteBarrier((&___forward_13), value);
	}

	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(DirectionalLight_t3737659866_StaticFields, ___color_14)); }
	inline Vector4U5BU5D_t934056436* get_color_14() const { return ___color_14; }
	inline Vector4U5BU5D_t934056436** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(Vector4U5BU5D_t934056436* value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONALLIGHT_T3737659866_H
#ifndef SPOTLIGHT_T3810368972_H
#define SPOTLIGHT_T3810368972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.SpotLight
struct  SpotLight_t3810368972  : public LightSource_1_t4135959262
{
public:
	// System.Single FlatLighting.SpotLight::BaseRadius
	float ___BaseRadius_16;
	// System.Single FlatLighting.SpotLight::Height
	float ___Height_17;
	// UnityEngine.Color FlatLighting.SpotLight::LightColor
	Color_t2555686324  ___LightColor_18;
	// UnityEngine.Vector4 FlatLighting.SpotLight::LightDistances
	Vector4_t3319028937  ___LightDistances_19;
	// UnityEngine.Vector4 FlatLighting.SpotLight::LightIntensities
	Vector4_t3319028937  ___LightIntensities_20;
	// System.Boolean FlatLighting.SpotLight::Smooth
	bool ___Smooth_21;
	// System.Boolean FlatLighting.SpotLight::isRealTime
	bool ___isRealTime_22;
	// System.Boolean FlatLighting.SpotLight::isFirstPass
	bool ___isFirstPass_23;

public:
	inline static int32_t get_offset_of_BaseRadius_16() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___BaseRadius_16)); }
	inline float get_BaseRadius_16() const { return ___BaseRadius_16; }
	inline float* get_address_of_BaseRadius_16() { return &___BaseRadius_16; }
	inline void set_BaseRadius_16(float value)
	{
		___BaseRadius_16 = value;
	}

	inline static int32_t get_offset_of_Height_17() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___Height_17)); }
	inline float get_Height_17() const { return ___Height_17; }
	inline float* get_address_of_Height_17() { return &___Height_17; }
	inline void set_Height_17(float value)
	{
		___Height_17 = value;
	}

	inline static int32_t get_offset_of_LightColor_18() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___LightColor_18)); }
	inline Color_t2555686324  get_LightColor_18() const { return ___LightColor_18; }
	inline Color_t2555686324 * get_address_of_LightColor_18() { return &___LightColor_18; }
	inline void set_LightColor_18(Color_t2555686324  value)
	{
		___LightColor_18 = value;
	}

	inline static int32_t get_offset_of_LightDistances_19() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___LightDistances_19)); }
	inline Vector4_t3319028937  get_LightDistances_19() const { return ___LightDistances_19; }
	inline Vector4_t3319028937 * get_address_of_LightDistances_19() { return &___LightDistances_19; }
	inline void set_LightDistances_19(Vector4_t3319028937  value)
	{
		___LightDistances_19 = value;
	}

	inline static int32_t get_offset_of_LightIntensities_20() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___LightIntensities_20)); }
	inline Vector4_t3319028937  get_LightIntensities_20() const { return ___LightIntensities_20; }
	inline Vector4_t3319028937 * get_address_of_LightIntensities_20() { return &___LightIntensities_20; }
	inline void set_LightIntensities_20(Vector4_t3319028937  value)
	{
		___LightIntensities_20 = value;
	}

	inline static int32_t get_offset_of_Smooth_21() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___Smooth_21)); }
	inline bool get_Smooth_21() const { return ___Smooth_21; }
	inline bool* get_address_of_Smooth_21() { return &___Smooth_21; }
	inline void set_Smooth_21(bool value)
	{
		___Smooth_21 = value;
	}

	inline static int32_t get_offset_of_isRealTime_22() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___isRealTime_22)); }
	inline bool get_isRealTime_22() const { return ___isRealTime_22; }
	inline bool* get_address_of_isRealTime_22() { return &___isRealTime_22; }
	inline void set_isRealTime_22(bool value)
	{
		___isRealTime_22 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_23() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972, ___isFirstPass_23)); }
	inline bool get_isFirstPass_23() const { return ___isFirstPass_23; }
	inline bool* get_address_of_isFirstPass_23() { return &___isFirstPass_23; }
	inline void set_isFirstPass_23(bool value)
	{
		___isFirstPass_23 = value;
	}
};

struct SpotLight_t3810368972_StaticFields
{
public:
	// System.String FlatLighting.SpotLight::spotLightCountProperty
	String_t* ___spotLightCountProperty_7;
	// System.String FlatLighting.SpotLight::spotLightWorldToModelProperty
	String_t* ___spotLightWorldToModelProperty_8;
	// System.String FlatLighting.SpotLight::spotLightForwardProperty
	String_t* ___spotLightForwardProperty_9;
	// System.String FlatLighting.SpotLight::spotLightColorProperty
	String_t* ___spotLightColorProperty_10;
	// System.String FlatLighting.SpotLight::spotLightBaseRadiusProperty
	String_t* ___spotLightBaseRadiusProperty_11;
	// System.String FlatLighting.SpotLight::spotLightHeightProperty
	String_t* ___spotLightHeightProperty_12;
	// System.String FlatLighting.SpotLight::spotLightDistancesProperty
	String_t* ___spotLightDistancesProperty_13;
	// System.String FlatLighting.SpotLight::spotLightIntensitiesProperty
	String_t* ___spotLightIntensitiesProperty_14;
	// System.String FlatLighting.SpotLight::spotLightSmoothnessProperty
	String_t* ___spotLightSmoothnessProperty_15;
	// UnityEngine.Matrix4x4[] FlatLighting.SpotLight::worldToModel
	Matrix4x4U5BU5D_t2302988098* ___worldToModel_24;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::forward
	Vector4U5BU5D_t934056436* ___forward_25;
	// System.Single[] FlatLighting.SpotLight::baseRadius
	SingleU5BU5D_t1444911251* ___baseRadius_26;
	// System.Single[] FlatLighting.SpotLight::height
	SingleU5BU5D_t1444911251* ___height_27;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::distances
	Vector4U5BU5D_t934056436* ___distances_28;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::intensities
	Vector4U5BU5D_t934056436* ___intensities_29;
	// System.Single[] FlatLighting.SpotLight::smoothness
	SingleU5BU5D_t1444911251* ___smoothness_30;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::color
	Vector4U5BU5D_t934056436* ___color_31;

public:
	inline static int32_t get_offset_of_spotLightCountProperty_7() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightCountProperty_7)); }
	inline String_t* get_spotLightCountProperty_7() const { return ___spotLightCountProperty_7; }
	inline String_t** get_address_of_spotLightCountProperty_7() { return &___spotLightCountProperty_7; }
	inline void set_spotLightCountProperty_7(String_t* value)
	{
		___spotLightCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightCountProperty_7), value);
	}

	inline static int32_t get_offset_of_spotLightWorldToModelProperty_8() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightWorldToModelProperty_8)); }
	inline String_t* get_spotLightWorldToModelProperty_8() const { return ___spotLightWorldToModelProperty_8; }
	inline String_t** get_address_of_spotLightWorldToModelProperty_8() { return &___spotLightWorldToModelProperty_8; }
	inline void set_spotLightWorldToModelProperty_8(String_t* value)
	{
		___spotLightWorldToModelProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightWorldToModelProperty_8), value);
	}

	inline static int32_t get_offset_of_spotLightForwardProperty_9() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightForwardProperty_9)); }
	inline String_t* get_spotLightForwardProperty_9() const { return ___spotLightForwardProperty_9; }
	inline String_t** get_address_of_spotLightForwardProperty_9() { return &___spotLightForwardProperty_9; }
	inline void set_spotLightForwardProperty_9(String_t* value)
	{
		___spotLightForwardProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightForwardProperty_9), value);
	}

	inline static int32_t get_offset_of_spotLightColorProperty_10() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightColorProperty_10)); }
	inline String_t* get_spotLightColorProperty_10() const { return ___spotLightColorProperty_10; }
	inline String_t** get_address_of_spotLightColorProperty_10() { return &___spotLightColorProperty_10; }
	inline void set_spotLightColorProperty_10(String_t* value)
	{
		___spotLightColorProperty_10 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightColorProperty_10), value);
	}

	inline static int32_t get_offset_of_spotLightBaseRadiusProperty_11() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightBaseRadiusProperty_11)); }
	inline String_t* get_spotLightBaseRadiusProperty_11() const { return ___spotLightBaseRadiusProperty_11; }
	inline String_t** get_address_of_spotLightBaseRadiusProperty_11() { return &___spotLightBaseRadiusProperty_11; }
	inline void set_spotLightBaseRadiusProperty_11(String_t* value)
	{
		___spotLightBaseRadiusProperty_11 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightBaseRadiusProperty_11), value);
	}

	inline static int32_t get_offset_of_spotLightHeightProperty_12() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightHeightProperty_12)); }
	inline String_t* get_spotLightHeightProperty_12() const { return ___spotLightHeightProperty_12; }
	inline String_t** get_address_of_spotLightHeightProperty_12() { return &___spotLightHeightProperty_12; }
	inline void set_spotLightHeightProperty_12(String_t* value)
	{
		___spotLightHeightProperty_12 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightHeightProperty_12), value);
	}

	inline static int32_t get_offset_of_spotLightDistancesProperty_13() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightDistancesProperty_13)); }
	inline String_t* get_spotLightDistancesProperty_13() const { return ___spotLightDistancesProperty_13; }
	inline String_t** get_address_of_spotLightDistancesProperty_13() { return &___spotLightDistancesProperty_13; }
	inline void set_spotLightDistancesProperty_13(String_t* value)
	{
		___spotLightDistancesProperty_13 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightDistancesProperty_13), value);
	}

	inline static int32_t get_offset_of_spotLightIntensitiesProperty_14() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightIntensitiesProperty_14)); }
	inline String_t* get_spotLightIntensitiesProperty_14() const { return ___spotLightIntensitiesProperty_14; }
	inline String_t** get_address_of_spotLightIntensitiesProperty_14() { return &___spotLightIntensitiesProperty_14; }
	inline void set_spotLightIntensitiesProperty_14(String_t* value)
	{
		___spotLightIntensitiesProperty_14 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightIntensitiesProperty_14), value);
	}

	inline static int32_t get_offset_of_spotLightSmoothnessProperty_15() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___spotLightSmoothnessProperty_15)); }
	inline String_t* get_spotLightSmoothnessProperty_15() const { return ___spotLightSmoothnessProperty_15; }
	inline String_t** get_address_of_spotLightSmoothnessProperty_15() { return &___spotLightSmoothnessProperty_15; }
	inline void set_spotLightSmoothnessProperty_15(String_t* value)
	{
		___spotLightSmoothnessProperty_15 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightSmoothnessProperty_15), value);
	}

	inline static int32_t get_offset_of_worldToModel_24() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___worldToModel_24)); }
	inline Matrix4x4U5BU5D_t2302988098* get_worldToModel_24() const { return ___worldToModel_24; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_worldToModel_24() { return &___worldToModel_24; }
	inline void set_worldToModel_24(Matrix4x4U5BU5D_t2302988098* value)
	{
		___worldToModel_24 = value;
		Il2CppCodeGenWriteBarrier((&___worldToModel_24), value);
	}

	inline static int32_t get_offset_of_forward_25() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___forward_25)); }
	inline Vector4U5BU5D_t934056436* get_forward_25() const { return ___forward_25; }
	inline Vector4U5BU5D_t934056436** get_address_of_forward_25() { return &___forward_25; }
	inline void set_forward_25(Vector4U5BU5D_t934056436* value)
	{
		___forward_25 = value;
		Il2CppCodeGenWriteBarrier((&___forward_25), value);
	}

	inline static int32_t get_offset_of_baseRadius_26() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___baseRadius_26)); }
	inline SingleU5BU5D_t1444911251* get_baseRadius_26() const { return ___baseRadius_26; }
	inline SingleU5BU5D_t1444911251** get_address_of_baseRadius_26() { return &___baseRadius_26; }
	inline void set_baseRadius_26(SingleU5BU5D_t1444911251* value)
	{
		___baseRadius_26 = value;
		Il2CppCodeGenWriteBarrier((&___baseRadius_26), value);
	}

	inline static int32_t get_offset_of_height_27() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___height_27)); }
	inline SingleU5BU5D_t1444911251* get_height_27() const { return ___height_27; }
	inline SingleU5BU5D_t1444911251** get_address_of_height_27() { return &___height_27; }
	inline void set_height_27(SingleU5BU5D_t1444911251* value)
	{
		___height_27 = value;
		Il2CppCodeGenWriteBarrier((&___height_27), value);
	}

	inline static int32_t get_offset_of_distances_28() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___distances_28)); }
	inline Vector4U5BU5D_t934056436* get_distances_28() const { return ___distances_28; }
	inline Vector4U5BU5D_t934056436** get_address_of_distances_28() { return &___distances_28; }
	inline void set_distances_28(Vector4U5BU5D_t934056436* value)
	{
		___distances_28 = value;
		Il2CppCodeGenWriteBarrier((&___distances_28), value);
	}

	inline static int32_t get_offset_of_intensities_29() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___intensities_29)); }
	inline Vector4U5BU5D_t934056436* get_intensities_29() const { return ___intensities_29; }
	inline Vector4U5BU5D_t934056436** get_address_of_intensities_29() { return &___intensities_29; }
	inline void set_intensities_29(Vector4U5BU5D_t934056436* value)
	{
		___intensities_29 = value;
		Il2CppCodeGenWriteBarrier((&___intensities_29), value);
	}

	inline static int32_t get_offset_of_smoothness_30() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___smoothness_30)); }
	inline SingleU5BU5D_t1444911251* get_smoothness_30() const { return ___smoothness_30; }
	inline SingleU5BU5D_t1444911251** get_address_of_smoothness_30() { return &___smoothness_30; }
	inline void set_smoothness_30(SingleU5BU5D_t1444911251* value)
	{
		___smoothness_30 = value;
		Il2CppCodeGenWriteBarrier((&___smoothness_30), value);
	}

	inline static int32_t get_offset_of_color_31() { return static_cast<int32_t>(offsetof(SpotLight_t3810368972_StaticFields, ___color_31)); }
	inline Vector4U5BU5D_t934056436* get_color_31() const { return ___color_31; }
	inline Vector4U5BU5D_t934056436** get_address_of_color_31() { return &___color_31; }
	inline void set_color_31(Vector4U5BU5D_t934056436* value)
	{
		___color_31 = value;
		Il2CppCodeGenWriteBarrier((&___color_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPOTLIGHT_T3810368972_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (ImageTarget_t2322341322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[1] = 
{
	ImageTarget_t2322341322::get_offset_of__scale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (InstantTarget_t3313867941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (ObjectTarget_t688225044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[1] = 
{
	ObjectTarget_t688225044::get_offset_of__scale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (RecognizedTarget_t3529911668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[6] = 
{
	RecognizedTarget_t3529911668::get_offset_of_Drawable_0(),
	RecognizedTarget_t3529911668::get_offset_of_Name_1(),
	RecognizedTarget_t3529911668::get_offset_of_ID_2(),
	RecognizedTarget_t3529911668::get_offset_of_ModelViewMatrix_3(),
	RecognizedTarget_t3529911668::get_offset_of_IsKnown_4(),
	RecognizedTarget_t3529911668::get_offset_of__physicalTargetHeight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (SDKBuildInformation_t1497247620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	SDKBuildInformation_t1497247620::get_offset_of_BuildConfiguration_0(),
	SDKBuildInformation_t1497247620::get_offset_of_BuildDate_1(),
	SDKBuildInformation_t1497247620::get_offset_of_BuildNumber_2(),
	SDKBuildInformation_t1497247620::get_offset_of_SDKVersion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TargetCollectionResource_t66041399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[4] = 
{
	TargetCollectionResource_t66041399::get_offset_of__targetPath_2(),
	TargetCollectionResource_t66041399::get_offset_of__useCustomURL_3(),
	TargetCollectionResource_t66041399::get_offset_of_OnFinishLoading_4(),
	TargetCollectionResource_t66041399::get_offset_of_OnErrorLoading_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (OnFinishLoadingEvent_t3173959162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (OnErrorLoadingEvent_t3790443523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (TargetSourceType_t830527111)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[3] = 
{
	TargetSourceType_t830527111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (TargetSource_t2046190744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[2] = 
{
	TargetSource_t2046190744::get_offset_of__identifier_0(),
	TargetSource_t2046190744::get_offset_of_U3CIsRegisteredU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (TransformOverride_t3828400655), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (WikitudeSDK_t895845057), -1, sizeof(WikitudeSDK_t895845057_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2111[3] = 
{
	0,
	WikitudeSDK_t895845057_StaticFields::get_offset_of__bridge_1(),
	WikitudeSDK_t895845057_StaticFields::get_offset_of__cachedBuildInformation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (CompatibilityLevel_t1827897472)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	CompatibilityLevel_t1827897472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Log_t2685642413), -1, sizeof(Log_t2685642413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	Log_t2685642413_StaticFields::get_offset_of_TAG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (Math_t1859187520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (TransformProperties_t2631539258)+ sizeof (RuntimeObject), sizeof(TransformProperties_t2631539258 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2115[4] = 
{
	TransformProperties_t2631539258::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformProperties_t2631539258::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformProperties_t2631539258::get_offset_of_Scale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformProperties_t2631539258::get_offset_of_FieldOfView_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (UnityVersion_t2410471165)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[7] = 
{
	UnityVersion_t2410471165::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (PlatformBase_t2513924928), -1, sizeof(PlatformBase_t2513924928_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2117[1] = 
{
	PlatformBase_t2513924928_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (PointCloudRenderer_t3664016937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[10] = 
{
	PointCloudRenderer_t3664016937::get_offset_of__testMesh_0(),
	PointCloudRenderer_t3664016937::get_offset_of__sceneCamera_1(),
	PointCloudRenderer_t3664016937::get_offset_of__renderMaterial_2(),
	PointCloudRenderer_t3664016937::get_offset_of__cubePoints_3(),
	PointCloudRenderer_t3664016937::get_offset_of__cubeIndices_4(),
	PointCloudRenderer_t3664016937::get_offset_of__meshVertexCount_5(),
	PointCloudRenderer_t3664016937::get_offset_of__drawWithCommandBuffer_6(),
	PointCloudRenderer_t3664016937::get_offset_of__vertices_7(),
	PointCloudRenderer_t3664016937::get_offset_of__indices_8(),
	PointCloudRenderer_t3664016937::get_offset_of__vertexColors_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (MapPointCloud_t955983077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[3] = 
{
	MapPointCloud_t955983077::get_offset_of_FeaturePoints_0(),
	MapPointCloud_t955983077::get_offset_of_Colors_1(),
	MapPointCloud_t955983077::get_offset_of_Scale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (TrackableBehaviour_t2469814643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[14] = 
{
	TrackableBehaviour_t2469814643::get_offset_of__targetPattern_2(),
	TrackableBehaviour_t2469814643::get_offset_of__targetPatternRegex_3(),
	TrackableBehaviour_t2469814643::get_offset_of__isKnown_4(),
	TrackableBehaviour_t2469814643::get_offset_of__currentTargetName_5(),
	TrackableBehaviour_t2469814643::get_offset_of__extendedTracking_6(),
	TrackableBehaviour_t2469814643::get_offset_of__targetsForExtendedTracking_7(),
	TrackableBehaviour_t2469814643::get_offset_of__autoToggleVisibility_8(),
	TrackableBehaviour_t2469814643::get_offset_of_OnEnterFieldOfVision_9(),
	TrackableBehaviour_t2469814643::get_offset_of_OnExitFieldOfVision_10(),
	TrackableBehaviour_t2469814643::get_offset_of__eventsFoldout_11(),
	TrackableBehaviour_t2469814643::get_offset_of__registeredToTracker_12(),
	TrackableBehaviour_t2469814643::get_offset_of__preview_13(),
	TrackableBehaviour_t2469814643::get_offset_of__previewMaterial_14(),
	TrackableBehaviour_t2469814643::get_offset_of__previewMesh_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (OnEnterFieldOfVisionEvent_t2975486640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (OnExitFieldOfVisionEvent_t2776302756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2123[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (U24ArrayTypeU3D64_t498138225)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D64_t498138225 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (ExamplePlayerController_t740376480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[7] = 
{
	ExamplePlayerController_t740376480::get_offset_of_materialColor_2(),
	ExamplePlayerController_t740376480::get_offset_of_m_rigidbody_3(),
	ExamplePlayerController_t740376480::get_offset_of_horizontalAxis_4(),
	ExamplePlayerController_t740376480::get_offset_of_verticalAxis_5(),
	ExamplePlayerController_t740376480::get_offset_of_jumpButton_6(),
	ExamplePlayerController_t740376480::get_offset_of_inputHorizontal_7(),
	ExamplePlayerController_t740376480::get_offset_of_inputVertical_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (AxisInputKeyboard_t1690261069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[3] = 
{
	AxisInputKeyboard_t1690261069::get_offset_of_key_2(),
	AxisInputKeyboard_t1690261069::get_offset_of_value_3(),
	AxisInputKeyboard_t1690261069::get_offset_of_axis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (AxisInputMouse_t3174651034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	AxisInputMouse_t3174651034::get_offset_of_xAxis_2(),
	AxisInputMouse_t3174651034::get_offset_of_yAxis_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (AxisInputUI_t3093488737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[2] = 
{
	AxisInputUI_t3093488737::get_offset_of_value_2(),
	AxisInputUI_t3093488737::get_offset_of_axis_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (Dpad_t1007306070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[5] = 
{
	Dpad_t1007306070::get_offset_of_xAxis_2(),
	Dpad_t1007306070::get_offset_of_yAxis_3(),
	Dpad_t1007306070::get_offset_of_rectTransform_4(),
	Dpad_t1007306070::get_offset_of_background_5(),
	Dpad_t1007306070::get_offset_of_m_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (Joystick_t1225167045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[16] = 
{
	Joystick_t1225167045::get_offset_of_xAxis_2(),
	Joystick_t1225167045::get_offset_of_yAxis_3(),
	Joystick_t1225167045::get_offset_of_joystickTR_4(),
	Joystick_t1225167045::get_offset_of_background_5(),
	Joystick_t1225167045::get_offset_of_thumb_6(),
	Joystick_t1225167045::get_offset_of_thumbTR_7(),
	Joystick_t1225167045::get_offset_of_movementAxes_8(),
	Joystick_t1225167045::get_offset_of_movementAreaRadius_9(),
	Joystick_t1225167045::get_offset_of_isDynamicJoystick_10(),
	Joystick_t1225167045::get_offset_of_dynamicJoystickMovementArea_11(),
	Joystick_t1225167045::get_offset_of_joystickHeld_12(),
	Joystick_t1225167045::get_offset_of_pointerInitialPos_13(),
	Joystick_t1225167045::get_offset_of__1OverMovementAreaRadius_14(),
	Joystick_t1225167045::get_offset_of_movementAreaRadiusSqr_15(),
	Joystick_t1225167045::get_offset_of_opacity_16(),
	Joystick_t1225167045::get_offset_of_m_value_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (MovementAxes_t2821595006)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2132[4] = 
{
	MovementAxes_t2821595006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (SteeringWheel_t1692308694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[9] = 
{
	SteeringWheel_t1692308694::get_offset_of_axis_2(),
	SteeringWheel_t1692308694::get_offset_of_wheel_3(),
	SteeringWheel_t1692308694::get_offset_of_wheelTR_4(),
	SteeringWheel_t1692308694::get_offset_of_centerPoint_5(),
	SteeringWheel_t1692308694::get_offset_of_maximumSteeringAngle_6(),
	SteeringWheel_t1692308694::get_offset_of_wheelReleasedSpeed_7(),
	SteeringWheel_t1692308694::get_offset_of_wheelAngle_8(),
	SteeringWheel_t1692308694::get_offset_of_wheelPrevAngle_9(),
	SteeringWheel_t1692308694::get_offset_of_wheelBeingHeld_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (Touchpad_t3466294403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[12] = 
{
	Touchpad_t3466294403::get_offset_of_xAxis_2(),
	Touchpad_t3466294403::get_offset_of_yAxis_3(),
	Touchpad_t3466294403::get_offset_of_rectTransform_4(),
	Touchpad_t3466294403::get_offset_of_sensitivity_5(),
	Touchpad_t3466294403::get_offset_of_allowTouchInput_6(),
	Touchpad_t3466294403::get_offset_of_allowedMouseButtons_7(),
	Touchpad_t3466294403::get_offset_of_ignoreUIElements_8(),
	Touchpad_t3466294403::get_offset_of_resolutionMultiplier_9(),
	Touchpad_t3466294403::get_offset_of_fingerId_10(),
	Touchpad_t3466294403::get_offset_of_prevMouseInputPos_11(),
	Touchpad_t3466294403::get_offset_of_trackMouseInput_12(),
	Touchpad_t3466294403::get_offset_of_m_value_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (MouseButton_t3761347860)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2135[4] = 
{
	MouseButton_t3761347860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (ButtonInputKeyboard_t3640535414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[2] = 
{
	ButtonInputKeyboard_t3640535414::get_offset_of_key_2(),
	ButtonInputKeyboard_t3640535414::get_offset_of_button_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (ButtonInputUI_t1749738659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	ButtonInputUI_t1749738659::get_offset_of_button_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (SimpleInputDragListener_t3058349199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	SimpleInputDragListener_t3058349199::get_offset_of_U3CListenerU3Ek__BackingField_2(),
	SimpleInputDragListener_t3058349199::get_offset_of_pointerId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (KeyInputKeyboard_t2444570555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[2] = 
{
	KeyInputKeyboard_t2444570555::get_offset_of_realKey_2(),
	KeyInputKeyboard_t2444570555::get_offset_of_key_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (KeyInputUI_t1141642093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[1] = 
{
	KeyInputUI_t1141642093::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (MouseButtonInputKeyboard_t4221569981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[2] = 
{
	MouseButtonInputKeyboard_t4221569981::get_offset_of_key_2(),
	MouseButtonInputKeyboard_t4221569981::get_offset_of_mouseButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (MouseButtonInputUI_t1438106573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[1] = 
{
	MouseButtonInputUI_t1438106573::get_offset_of_mouseButton_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (SimpleInput_t4265260572), -1, sizeof(SimpleInput_t4265260572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[19] = 
{
	0,
	SimpleInput_t4265260572_StaticFields::get_offset_of_instance_3(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_axes_4(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_axesList_5(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_trackedUnityAxes_6(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_trackedTemporaryAxes_7(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_buttons_8(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_buttonsList_9(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_trackedUnityButtons_10(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_trackedTemporaryButtons_11(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_mouseButtons_12(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_mouseButtonsList_13(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_trackedUnityMouseButtons_14(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_trackedTemporaryMouseButtons_15(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_keys_16(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_keysList_17(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_OnUpdate_18(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	SimpleInput_t4265260572_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (InputState_t3726306521)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2146[5] = 
{
	InputState_t3726306521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (Axis_t2182888826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[4] = 
{
	Axis_t2182888826::get_offset_of_name_0(),
	Axis_t2182888826::get_offset_of_inputs_1(),
	Axis_t2182888826::get_offset_of_value_2(),
	Axis_t2182888826::get_offset_of_valueRaw_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (Button_t2930970180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[3] = 
{
	Button_t2930970180::get_offset_of_button_0(),
	Button_t2930970180::get_offset_of_inputs_1(),
	Button_t2930970180::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (MouseButton_t998287744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	MouseButton_t998287744::get_offset_of_button_0(),
	MouseButton_t998287744::get_offset_of_inputs_1(),
	MouseButton_t998287744::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (Key_t3885074208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[3] = 
{
	Key_t3885074208::get_offset_of_key_0(),
	Key_t3885074208::get_offset_of_inputs_1(),
	Key_t3885074208::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (AxisInput_t802457650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (ButtonInput_t2818951903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (MouseButtonInput_t2868216434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (KeyInput_t3393349320), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (UpdateCallback_t3991193291), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (SpinAnimation_t2236150191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (CameraControl_t3123314556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[10] = 
{
	CameraControl_t3123314556::get_offset_of_pointOfInterest_2(),
	CameraControl_t3123314556::get_offset_of_shouldRotateAutomatic_3(),
	CameraControl_t3123314556::get_offset_of_rotationSpeed_4(),
	CameraControl_t3123314556::get_offset_of_minRadius_5(),
	CameraControl_t3123314556::get_offset_of_maxRadius_6(),
	CameraControl_t3123314556::get_offset_of_minZoom_7(),
	CameraControl_t3123314556::get_offset_of_maxZoom_8(),
	CameraControl_t3123314556::get_offset_of_myCamera_9(),
	CameraControl_t3123314556::get_offset_of_lastNormalizedPosition_10(),
	CameraControl_t3123314556::get_offset_of_radius_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (MouseOrbit_t2077911214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[32] = 
{
	MouseOrbit_t2077911214::get_offset_of_target_2(),
	MouseOrbit_t2077911214::get_offset_of_yPosClamp_3(),
	MouseOrbit_t2077911214::get_offset_of_targetFocusLerpSpeed_4(),
	MouseOrbit_t2077911214::get_offset_of_initialDistance_5(),
	MouseOrbit_t2077911214::get_offset_of_smooth_6(),
	MouseOrbit_t2077911214::get_offset_of_xSpeed_7(),
	MouseOrbit_t2077911214::get_offset_of_ySpeed_8(),
	MouseOrbit_t2077911214::get_offset_of_wasdSpeed_9(),
	MouseOrbit_t2077911214::get_offset_of_wasdSmooth_10(),
	MouseOrbit_t2077911214::get_offset_of_panSpeed_11(),
	MouseOrbit_t2077911214::get_offset_of_zoomSmooth_12(),
	MouseOrbit_t2077911214::get_offset_of_minZoom_13(),
	MouseOrbit_t2077911214::get_offset_of_maxZoom_14(),
	MouseOrbit_t2077911214::get_offset_of_mouseSensitivityScaler_15(),
	MouseOrbit_t2077911214::get_offset_of_scrollSensitivityScaler_16(),
	MouseOrbit_t2077911214::get_offset_of_distance_17(),
	MouseOrbit_t2077911214::get_offset_of_yMaxLimit_18(),
	MouseOrbit_t2077911214::get_offset_of_yMinLimit_19(),
	MouseOrbit_t2077911214::get_offset_of_smoothY_20(),
	MouseOrbit_t2077911214::get_offset_of_smoothX_21(),
	MouseOrbit_t2077911214::get_offset_of_movementPOS_22(),
	MouseOrbit_t2077911214::get_offset_of_rotation_23(),
	MouseOrbit_t2077911214::get_offset_of_position_24(),
	MouseOrbit_t2077911214::get_offset_of_clickTimey_25(),
	MouseOrbit_t2077911214::get_offset_of_myTransform_26(),
	MouseOrbit_t2077911214::get_offset_of_posToBe_27(),
	MouseOrbit_t2077911214::get_offset_of_zoomSmoothDelegate_28(),
	MouseOrbit_t2077911214::get_offset_of_x_29(),
	MouseOrbit_t2077911214::get_offset_of_y_30(),
	MouseOrbit_t2077911214::get_offset_of_movementPosOffset_31(),
	MouseOrbit_t2077911214::get_offset_of_isOrthographic_32(),
	MouseOrbit_t2077911214::get_offset_of_myCamera_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (MaterialSwapper_t1465658300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[6] = 
{
	MaterialSwapper_t1465658300::get_offset_of_materials_2(),
	MaterialSwapper_t1465658300::get_offset_of_UnityLightsRoot_3(),
	MaterialSwapper_t1465658300::get_offset_of_UnityLightsMaterialIndex_4(),
	MaterialSwapper_t1465658300::get_offset_of_FlatLightsRoot_5(),
	MaterialSwapper_t1465658300::get_offset_of_FlatLightsMaterialIndex_6(),
	MaterialSwapper_t1465658300::get_offset_of_myRenderer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (LightingManager_t1042243518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[3] = 
{
	LightingManager_t1042243518::get_offset_of_day_2(),
	LightingManager_t1042243518::get_offset_of_night_3(),
	LightingManager_t1042243518::get_offset_of_root_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (SceneLigtingSetup_t3397062989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[23] = 
{
	SceneLigtingSetup_t3397062989::get_offset_of_cameraBackground_2(),
	SceneLigtingSetup_t3397062989::get_offset_of_globalMaterial_3(),
	SceneLigtingSetup_t3397062989::get_offset_of_vegetationMaterial_4(),
	SceneLigtingSetup_t3397062989::get_offset_of_vegetationRoot_5(),
	SceneLigtingSetup_t3397062989::get_offset_of_bridgeMaterial_6(),
	SceneLigtingSetup_t3397062989::get_offset_of_bridgeRoot_7(),
	SceneLigtingSetup_t3397062989::get_offset_of_lookoutMaterial_8(),
	SceneLigtingSetup_t3397062989::get_offset_of_lookoutRoot_9(),
	SceneLigtingSetup_t3397062989::get_offset_of_towerMaterial_10(),
	SceneLigtingSetup_t3397062989::get_offset_of_towerRoot_11(),
	SceneLigtingSetup_t3397062989::get_offset_of_deadTreeMaterial_12(),
	SceneLigtingSetup_t3397062989::get_offset_of_deadTreeRoot_13(),
	SceneLigtingSetup_t3397062989::get_offset_of_rocksMaterial_14(),
	SceneLigtingSetup_t3397062989::get_offset_of_rocksTreeRoot_15(),
	SceneLigtingSetup_t3397062989::get_offset_of_objectsToEnable_16(),
	SceneLigtingSetup_t3397062989::get_offset_of_sceneRenderers_17(),
	SceneLigtingSetup_t3397062989::get_offset_of_vegetationRenderers_18(),
	SceneLigtingSetup_t3397062989::get_offset_of_bridgeRenderers_19(),
	SceneLigtingSetup_t3397062989::get_offset_of_lookoutRenderers_20(),
	SceneLigtingSetup_t3397062989::get_offset_of_towerRenderers_21(),
	SceneLigtingSetup_t3397062989::get_offset_of_deadTreeRenderers_22(),
	SceneLigtingSetup_t3397062989::get_offset_of_boatRenderers_23(),
	SceneLigtingSetup_t3397062989::get_offset_of_rocksRenderers_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (MaterialBlender_t4195463282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[4] = 
{
	MaterialBlender_t4195463282::get_offset_of_durationSeconds_2(),
	MaterialBlender_t4195463282::get_offset_of_loop_3(),
	MaterialBlender_t4195463282::get_offset_of_materials_4(),
	MaterialBlender_t4195463282::get_offset_of_internalMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U3CUpdateBlendingU3Ec__Iterator0_t3841162924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[10] = 
{
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U3CcurrentMaterialIndexU3E__0_0(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U3CnextMaterialIndexU3E__0_1(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U3CiU3E__1_2(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U3CcurrentMaterialU3E__2_3(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U3CnextMaterialU3E__2_4(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U3CtU3E__3_5(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U24this_6(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U24current_7(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U24disposing_8(),
	U3CUpdateBlendingU3Ec__Iterator0_t3841162924::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (Constants_t439474454), -1, sizeof(Constants_t439474454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2165[4] = 
{
	Constants_t439474454_StaticFields::get_offset_of_FlatLightingShaderPath_0(),
	Constants_t439474454_StaticFields::get_offset_of_FlatLightingTag_1(),
	Constants_t439474454_StaticFields::get_offset_of_FlatLightingBakedTag_2(),
	Constants_t439474454_StaticFields::get_offset_of_GizmoIconsPath_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (Shader_t1748525893), -1, sizeof(Shader_t1748525893_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2166[46] = 
{
	Shader_t1748525893_StaticFields::get_offset_of_AXIS_COLORS_LOCAL_0(),
	Shader_t1748525893_StaticFields::get_offset_of_AXIS_COLORS_GLOBAL_1(),
	Shader_t1748525893_StaticFields::get_offset_of_SYMETRIC_COLORS_ON_KEYWORD_2(),
	Shader_t1748525893_StaticFields::get_offset_of_SYMETRIC_COLORS_OFF_KEYWORD_3(),
	Shader_t1748525893_StaticFields::get_offset_of_AXIS_GRADIENT_ON_X_KEYWORD_4(),
	Shader_t1748525893_StaticFields::get_offset_of_AXIS_GRADIENT_ON_Y_KEYWORD_5(),
	Shader_t1748525893_StaticFields::get_offset_of_AXIS_GRADIENT_ON_Z_KEYWORD_6(),
	Shader_t1748525893_StaticFields::get_offset_of_AXIS_GRADIENT_OFF_KEYWORD_7(),
	Shader_t1748525893_StaticFields::get_offset_of_VERTEX_COLOR_KEYWORD_8(),
	Shader_t1748525893_StaticFields::get_offset_of_AMBIENT_LIGHT_KEYWORD_9(),
	Shader_t1748525893_StaticFields::get_offset_of_DIRECT_LIGHT_KEYWORD_10(),
	Shader_t1748525893_StaticFields::get_offset_of_SPOT_LIGHT_KEYWORD_11(),
	Shader_t1748525893_StaticFields::get_offset_of_POINT_LIGHT_KEYWORD_12(),
	Shader_t1748525893_StaticFields::get_offset_of_BLEND_LIGHT_SOURCES_KEYWORD_13(),
	Shader_t1748525893_StaticFields::get_offset_of_GRADIENT_LOCAL_KEYWORD_14(),
	Shader_t1748525893_StaticFields::get_offset_of_GRADIENT_WORLD_KEYWORD_15(),
	Shader_t1748525893_StaticFields::get_offset_of_CUSTOM_LIGHTMAPPING_KEYWORD_16(),
	Shader_t1748525893_StaticFields::get_offset_of_UNITY_LIGHTMAPPING_KEYWORD_17(),
	Shader_t1748525893_StaticFields::get_offset_of_RECEIVE_CUSTOM_SHADOW_KEYWORD_18(),
	Shader_t1748525893_StaticFields::get_offset_of_CAST_CUSTOM_SHADOW_ON_KEYWORD_19(),
	Shader_t1748525893_StaticFields::get_offset_of_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20(),
	Shader_t1748525893_StaticFields::get_offset_of_USE_MAIN_TEXTURE_KEYWORD_21(),
	Shader_t1748525893_StaticFields::get_offset_of_LightPositiveX_22(),
	Shader_t1748525893_StaticFields::get_offset_of_LightPositiveY_23(),
	Shader_t1748525893_StaticFields::get_offset_of_LightPositiveZ_24(),
	Shader_t1748525893_StaticFields::get_offset_of_LightNegativeX_25(),
	Shader_t1748525893_StaticFields::get_offset_of_LightNegativeY_26(),
	Shader_t1748525893_StaticFields::get_offset_of_LightNegativeZ_27(),
	Shader_t1748525893_StaticFields::get_offset_of_LightPositive2X_28(),
	Shader_t1748525893_StaticFields::get_offset_of_LightPositive2Y_29(),
	Shader_t1748525893_StaticFields::get_offset_of_LightPositive2Z_30(),
	Shader_t1748525893_StaticFields::get_offset_of_LightNegative2X_31(),
	Shader_t1748525893_StaticFields::get_offset_of_LightNegative2Y_32(),
	Shader_t1748525893_StaticFields::get_offset_of_LightNegative2Z_33(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientWidthPositiveX_34(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientWidthPositiveY_35(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientWidthPositiveZ_36(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientWidthNegativeX_37(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientWidthNegativeY_38(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientWidthNegativeZ_39(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientOriginOffsetPositiveX_40(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientOriginOffsetPositiveY_41(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientOriginOffsetPositiveZ_42(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientOriginOffsetNegativeX_43(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientOriginOffsetNegativeY_44(),
	Shader_t1748525893_StaticFields::get_offset_of_GradientOriginOffsetNegativeZ_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (DirectionalLight_t3737659866), -1, sizeof(DirectionalLight_t3737659866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2167[8] = 
{
	DirectionalLight_t3737659866_StaticFields::get_offset_of_directionalLightCountProperty_7(),
	DirectionalLight_t3737659866_StaticFields::get_offset_of_directionalLightColorProperty_8(),
	DirectionalLight_t3737659866_StaticFields::get_offset_of_directionalLightForwardProperty_9(),
	DirectionalLight_t3737659866::get_offset_of_isRealTime_10(),
	DirectionalLight_t3737659866::get_offset_of_LightColor_11(),
	DirectionalLight_t3737659866::get_offset_of_isFirstPass_12(),
	DirectionalLight_t3737659866_StaticFields::get_offset_of_forward_13(),
	DirectionalLight_t3737659866_StaticFields::get_offset_of_color_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (PointLight_t2259825046), -1, sizeof(PointLight_t2259825046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2170[18] = 
{
	PointLight_t2259825046_StaticFields::get_offset_of_pointLightCountProperty_7(),
	PointLight_t2259825046_StaticFields::get_offset_of_pointLight0WorldToModelProperty_8(),
	PointLight_t2259825046_StaticFields::get_offset_of_pointLightColorProperty_9(),
	PointLight_t2259825046_StaticFields::get_offset_of_pointLightDistancesProperty_10(),
	PointLight_t2259825046_StaticFields::get_offset_of_pointLightIntensitiesProperty_11(),
	PointLight_t2259825046_StaticFields::get_offset_of_pointLightSmoothnessProperty_12(),
	PointLight_t2259825046::get_offset_of_Range_13(),
	PointLight_t2259825046::get_offset_of_LightColor_14(),
	PointLight_t2259825046::get_offset_of_LightDistances_15(),
	PointLight_t2259825046::get_offset_of_LightIntensities_16(),
	PointLight_t2259825046::get_offset_of_Smooth_17(),
	PointLight_t2259825046::get_offset_of_isRealTime_18(),
	PointLight_t2259825046::get_offset_of_isFirstPass_19(),
	PointLight_t2259825046_StaticFields::get_offset_of_worldToModel_20(),
	PointLight_t2259825046_StaticFields::get_offset_of_distances_21(),
	PointLight_t2259825046_StaticFields::get_offset_of_intensities_22(),
	PointLight_t2259825046_StaticFields::get_offset_of_smoothness_23(),
	PointLight_t2259825046_StaticFields::get_offset_of_color_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (SpotLight_t3810368972), -1, sizeof(SpotLight_t3810368972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[25] = 
{
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightCountProperty_7(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightWorldToModelProperty_8(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightForwardProperty_9(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightColorProperty_10(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightBaseRadiusProperty_11(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightHeightProperty_12(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightDistancesProperty_13(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightIntensitiesProperty_14(),
	SpotLight_t3810368972_StaticFields::get_offset_of_spotLightSmoothnessProperty_15(),
	SpotLight_t3810368972::get_offset_of_BaseRadius_16(),
	SpotLight_t3810368972::get_offset_of_Height_17(),
	SpotLight_t3810368972::get_offset_of_LightColor_18(),
	SpotLight_t3810368972::get_offset_of_LightDistances_19(),
	SpotLight_t3810368972::get_offset_of_LightIntensities_20(),
	SpotLight_t3810368972::get_offset_of_Smooth_21(),
	SpotLight_t3810368972::get_offset_of_isRealTime_22(),
	SpotLight_t3810368972::get_offset_of_isFirstPass_23(),
	SpotLight_t3810368972_StaticFields::get_offset_of_worldToModel_24(),
	SpotLight_t3810368972_StaticFields::get_offset_of_forward_25(),
	SpotLight_t3810368972_StaticFields::get_offset_of_baseRadius_26(),
	SpotLight_t3810368972_StaticFields::get_offset_of_height_27(),
	SpotLight_t3810368972_StaticFields::get_offset_of_distances_28(),
	SpotLight_t3810368972_StaticFields::get_offset_of_intensities_29(),
	SpotLight_t3810368972_StaticFields::get_offset_of_smoothness_30(),
	SpotLight_t3810368972_StaticFields::get_offset_of_color_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (VectorAsSlidersAttribute_t2873057700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[4] = 
{
	VectorAsSlidersAttribute_t2873057700::get_offset_of_label_0(),
	VectorAsSlidersAttribute_t2873057700::get_offset_of_min_1(),
	VectorAsSlidersAttribute_t2873057700::get_offset_of_max_2(),
	VectorAsSlidersAttribute_t2873057700::get_offset_of_dimensions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (RangeWithStepAttribute_t2191810823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	RangeWithStepAttribute_t2191810823::get_offset_of_min_0(),
	RangeWithStepAttribute_t2191810823::get_offset_of_max_1(),
	RangeWithStepAttribute_t2191810823::get_offset_of_step_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (ShadowProjector_t191975869), -1, sizeof(ShadowProjector_t191975869_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2174[27] = 
{
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowProjectorCountProperty_7(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowMapMatrixProperty_8(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowModelToViewProperty_9(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowColorProperty_10(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowBlurProperty_11(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowCameraSettingsProperty_12(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowTextureProperty_13(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14(),
	ShadowProjector_t191975869::get_offset_of_ShadowColor_15(),
	ShadowProjector_t191975869::get_offset_of_ShadowMapShader_16(),
	ShadowProjector_t191975869::get_offset_of_Bias_17(),
	ShadowProjector_t191975869::get_offset_of_ShadowBlur_18(),
	ShadowProjector_t191975869::get_offset_of_MemoryToUse_19(),
	ShadowProjector_t191975869::get_offset_of_isRealTime_20(),
	ShadowProjector_t191975869::get_offset_of_DebugShadowMappingTexture_21(),
	ShadowProjector_t191975869::get_offset_of_isFirstPass_22(),
	ShadowProjector_t191975869::get_offset_of_debugObject_23(),
	ShadowProjector_t191975869::get_offset_of_debugMaterial_24(),
	ShadowProjector_t191975869::get_offset_of_ShadowCamera_25(),
	ShadowProjector_t191975869::get_offset_of_ShadowMapRequested_26(),
	ShadowProjector_t191975869::get_offset_of_ShadowMapOffset_27(),
	ShadowProjector_t191975869::get_offset_of_ShadowMapTexture_28(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_mapMatrix_29(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_modelToView_30(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_cameraSettings_31(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_blur_32(),
	ShadowProjector_t191975869_StaticFields::get_offset_of_shadowColor_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ActiveTCellScript_t3652656283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[2] = 
{
	ActiveTCellScript_t3652656283::get_offset_of_ActivCell_2(),
	ActiveTCellScript_t3652656283::get_offset_of_AntiGen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (AudioManager_t3267510698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[2] = 
{
	AudioManager_t3267510698::get_offset_of_audiosource_3(),
	AudioManager_t3267510698::get_offset_of_soundEffects_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (GameManager_t1536523654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[23] = 
{
	GameManager_t1536523654::get_offset_of_Collectables_3(),
	GameManager_t1536523654::get_offset_of_goal_4(),
	GameManager_t1536523654::get_offset_of_numberOfCellstokill_5(),
	GameManager_t1536523654::get_offset_of_player_6(),
	GameManager_t1536523654::get_offset_of_framesText_7(),
	GameManager_t1536523654::get_offset_of_framesIterator_8(),
	GameManager_t1536523654::get_offset_of_fireworks_9(),
	GameManager_t1536523654::get_offset_of_TutorialFrames_10(),
	GameManager_t1536523654::get_offset_of_GameFrames_11(),
	GameManager_t1536523654::get_offset_of_TrackedItem_12(),
	GameManager_t1536523654::get_offset_of_FakeSphere_13(),
	GameManager_t1536523654::get_offset_of_Camera_14(),
	GameManager_t1536523654::get_offset_of_BloodSplatter_15(),
	GameManager_t1536523654::get_offset_of_RealSphere_16(),
	GameManager_t1536523654::get_offset_of_am_17(),
	GameManager_t1536523654::get_offset_of_SlidesON_18(),
	GameManager_t1536523654::get_offset_of_MovementTutorialStarted_19(),
	GameManager_t1536523654::get_offset_of_MovementTutorialFinished_20(),
	GameManager_t1536523654::get_offset_of_GameON_21(),
	GameManager_t1536523654::get_offset_of_ResetPlayer_22(),
	GameManager_t1536523654::get_offset_of_StartButton_23(),
	GameManager_t1536523654::get_offset_of_ResetBallButton_24(),
	GameManager_t1536523654::get_offset_of_ArrowsButton_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (U3CScaleOverTimeU3Ec__Iterator0_t1452294761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[8] = 
{
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U3CoriginalScaleU3E__0_0(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U3CdestinationScaleU3E__0_1(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U3CcurrentTimeU3E__0_2(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_time_3(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U24this_4(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U24current_5(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U24disposing_6(),
	U3CScaleOverTimeU3Ec__Iterator0_t1452294761::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (LoopManager_t2087799844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[6] = 
{
	LoopManager_t2087799844::get_offset_of_speed_2(),
	LoopManager_t2087799844::get_offset_of_Sphere_3(),
	LoopManager_t2087799844::get_offset_of_StartPoint_4(),
	LoopManager_t2087799844::get_offset_of_SpawnTime_5(),
	LoopManager_t2087799844::get_offset_of_Cooldown_6(),
	LoopManager_t2087799844::get_offset_of_StartSpawn_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (PlanetScript_t2937373613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[1] = 
{
	PlanetScript_t2937373613::get_offset_of_gravity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (PlayerGravityBody_t2486658507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[2] = 
{
	PlayerGravityBody_t2486658507::get_offset_of_attractorPlanet_2(),
	PlayerGravityBody_t2486658507::get_offset_of_playerTransform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (PlayerMovementScript_t179248481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[15] = 
{
	PlayerMovementScript_t179248481::get_offset_of_moveSpeed_2(),
	PlayerMovementScript_t179248481::get_offset_of_TrackedItem_3(),
	PlayerMovementScript_t179248481::get_offset_of_moveDirection_4(),
	PlayerMovementScript_t179248481::get_offset_of_Camera_5(),
	PlayerMovementScript_t179248481::get_offset_of_Player_6(),
	PlayerMovementScript_t179248481::get_offset_of_scorePivot_7(),
	PlayerMovementScript_t179248481::get_offset_of_smooth_8(),
	PlayerMovementScript_t179248481::get_offset_of_horizontalAxis_9(),
	PlayerMovementScript_t179248481::get_offset_of_verticalAxis_10(),
	PlayerMovementScript_t179248481::get_offset_of_inputHorizontal_11(),
	PlayerMovementScript_t179248481::get_offset_of_inputVertical_12(),
	PlayerMovementScript_t179248481::get_offset_of_dir_13(),
	PlayerMovementScript_t179248481::get_offset_of_dir2_14(),
	PlayerMovementScript_t179248481::get_offset_of_am_15(),
	PlayerMovementScript_t179248481::get_offset_of_gm_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (Rotate_t1850091912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[4] = 
{
	Rotate_t1850091912::get_offset_of_cam_2(),
	Rotate_t1850091912::get_offset_of_target_3(),
	Rotate_t1850091912::get_offset_of_dir_4(),
	Rotate_t1850091912::get_offset_of_dir2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (StaticItem_t47160647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[5] = 
{
	StaticItem_t47160647::get_offset_of_Camera_2(),
	StaticItem_t47160647::get_offset_of_positionToFollow_3(),
	StaticItem_t47160647::get_offset_of_speed_4(),
	StaticItem_t47160647::get_offset_of_startTime_5(),
	StaticItem_t47160647::get_offset_of_journeyLength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (AsymmetricFrustum_t2453563558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[12] = 
{
	AsymmetricFrustum_t2453563558::get_offset_of_left_2(),
	AsymmetricFrustum_t2453563558::get_offset_of_right_3(),
	AsymmetricFrustum_t2453563558::get_offset_of_top_4(),
	AsymmetricFrustum_t2453563558::get_offset_of_bottom_5(),
	AsymmetricFrustum_t2453563558::get_offset_of_near_6(),
	AsymmetricFrustum_t2453563558::get_offset_of_far_7(),
	AsymmetricFrustum_t2453563558::get_offset_of_fov_8(),
	AsymmetricFrustum_t2453563558::get_offset_of_calibratedCamNear_9(),
	AsymmetricFrustum_t2453563558::get_offset_of_slideX_10(),
	AsymmetricFrustum_t2453563558::get_offset_of_isLeftCam_11(),
	AsymmetricFrustum_t2453563558::get_offset_of_cam_12(),
	AsymmetricFrustum_t2453563558::get_offset_of_asymmetricFrust_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (ConnectToPreviousRemote_t2022260273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[2] = 
{
	ConnectToPreviousRemote_t2022260273::get_offset_of_lastRemoteID_2(),
	ConnectToPreviousRemote_t2022260273::get_offset_of_activelySearching_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[4] = 
{
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361::get_offset_of_U24this_0(),
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361::get_offset_of_U24current_1(),
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361::get_offset_of_U24disposing_2(),
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1411973361::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (U3CCheckRemotesU3Ec__Iterator1_t2653031284), -1, sizeof(U3CCheckRemotesU3Ec__Iterator1_t2653031284_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2189[5] = 
{
	U3CCheckRemotesU3Ec__Iterator1_t2653031284::get_offset_of_U24this_0(),
	U3CCheckRemotesU3Ec__Iterator1_t2653031284::get_offset_of_U24current_1(),
	U3CCheckRemotesU3Ec__Iterator1_t2653031284::get_offset_of_U24disposing_2(),
	U3CCheckRemotesU3Ec__Iterator1_t2653031284::get_offset_of_U24PC_3(),
	U3CCheckRemotesU3Ec__Iterator1_t2653031284_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (MiraRemoteErrorCode_t1406415247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2190[7] = 
{
	MiraRemoteErrorCode_t1406415247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (MiraRemoteException_t3723149839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[1] = 
{
	MiraRemoteException_t3723149839::get_offset_of_U3CerrorCodeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (NativeBridge_t1552383108), -1, sizeof(NativeBridge_t1552383108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2192[28] = 
{
	NativeBridge_t1552383108_StaticFields::get_offset_of_emptyActions_0(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_boolActions_1(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_intActions_2(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_floatActions_3(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_exceptionActions_4(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_remoteActions_5(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_remoteMotionActions_6(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_11(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_12(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_13(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_14(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_15(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_16(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_17(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_18(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_19(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_20(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_21(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_22(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_23(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_24(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_25(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_26(),
	NativeBridge_t1552383108_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (MREmptyCallback_t625280627), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (MRBoolCallback_t3968806430), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (MRIntCallback_t3089559991), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (MRFloatCallback_t806206182), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (MRErrorCallback_t1000734509), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (MRDiscoveredRemoteCallback_t179447917), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (MRConnectedRemoteCallback_t1852255652), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
