﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mira.RotationalTrackingManager
struct RotationalTrackingManager_t1723746904;
// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler
struct RemoteMotionSensorInputEventHandler_t2314235825;
// RemoteOrientationInput/RemoteOrientationInputEventHandler
struct RemoteOrientationInputEventHandler_t1727624703;
// System.Action`1<MiraRemoteException>
struct Action_1_t3895617434;
// RemoteManager
struct RemoteManager_t1941324022;
// Remote
struct Remote_t1018512123;
// MiraWikitudeManager
struct MiraWikitudeManager_t2444513627;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// RemoteTouchInput/RemoteTouchInputEventHandler
struct RemoteTouchInputEventHandler_t3839927206;
// MiraController
struct MiraController_t2033339498;
// RemoteBase
struct RemoteBase_t3398252275;
// VirtualRemote
struct VirtualRemote_t2507984974;
// SettingsManager
struct SettingsManager_t2083239687;
// System.Action`1<Remote>
struct Action_1_t1190979718;
// System.String
struct String_t;
// RemoteAxisInput/RemoteAxisInputEventHandler
struct RemoteAxisInputEventHandler_t4129402086;
// MiraRemoteException
struct MiraRemoteException_t3723149839;
// RemoteButtonInput
struct RemoteButtonInput_t3750858021;
// RemoteTouchPadInput
struct RemoteTouchPadInput_t3817316988;
// RemoteMotionInput
struct RemoteMotionInput_t177155726;
// RemoteButtonInput/RemoteButtonInputEventHandler
struct RemoteButtonInputEventHandler_t2005612063;
// RemoteTouchPadInput/RemoteTouchPadInputEventHandler
struct RemoteTouchPadInputEventHandler_t3280214852;
// RemoteAxisInput
struct RemoteAxisInput_t1550365229;
// RemoteTouchInput
struct RemoteTouchInput_t3055303244;
// RemoteMotionInput/RemoteMotionInputEventHandler
struct RemoteMotionInputEventHandler_t3928406934;
// RemoteOrientationInput
struct RemoteOrientationInput_t714724027;
// RemoteMotionSensorInput
struct RemoteMotionSensorInput_t407189685;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// Remote/RemoteRefreshedEventHandler
struct RemoteRefreshedEventHandler_t1529686812;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Material
struct Material_t340375123;
// System.EventArgs
struct EventArgs_t3591816995;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2328697118;
// MiraBTRemoteInput
struct MiraBTRemoteInput_t3160068026;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Collections.Generic.List`1<System.Func`1<System.Boolean>>
struct List_1_t999109354;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// RemoteManager/RemoteConnectedEventHandler
struct RemoteConnectedEventHandler_t306622352;
// RemoteManager/RemoteDisconnectedEventHandler
struct RemoteDisconnectedEventHandler_t2506463716;
// System.Collections.Generic.List`1<Remote>
struct List_1_t2490586865;
// Mira.MiraViewer
struct MiraViewer_t94414402;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.EventSystems.MiraInputModule
struct MiraInputModule_t1698371845;
// MiraReticlePointer
struct MiraReticlePointer_t3058549069;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// DistortionEquation
struct DistortionEquation_t32189124;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3081694049;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// MiraReticle
struct MiraReticle_t910965139;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// RemotesController
struct RemotesController_t1084025038;
// MiraBasePointer
struct MiraBasePointer_t2409431493;
// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t3568572958;
// System.Collections.Generic.Queue`1<UnityEngine.Quaternion>
struct Queue_1_t2148187825;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// Wikitude.ImageTrackable
struct ImageTrackable_t2462741734;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t384203932;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t3132410353;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t1435266790;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t830933145;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CROTATIONALSETUPU3EC__ITERATOR0_T1515946677_H
#define U3CROTATIONALSETUPU3EC__ITERATOR0_T1515946677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0
struct  U3CRotationalSetupU3Ec__Iterator0_t1515946677  : public RuntimeObject
{
public:
	// Mira.RotationalTrackingManager Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$this
	RotationalTrackingManager_t1723746904 * ___U24this_0;
	// System.Object Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t1515946677, ___U24this_0)); }
	inline RotationalTrackingManager_t1723746904 * get_U24this_0() const { return ___U24this_0; }
	inline RotationalTrackingManager_t1723746904 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RotationalTrackingManager_t1723746904 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t1515946677, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t1515946677, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t1515946677, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CROTATIONALSETUPU3EC__ITERATOR0_T1515946677_H
#ifndef REMOTEMOTIONSENSORINPUT_T407189685_H
#define REMOTEMOTIONSENSORINPUT_T407189685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionSensorInput
struct  RemoteMotionSensorInput_t407189685  : public RuntimeObject
{
public:
	// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler RemoteMotionSensorInput::OnValueChanged
	RemoteMotionSensorInputEventHandler_t2314235825 * ___OnValueChanged_0;
	// System.Single RemoteMotionSensorInput::<x>k__BackingField
	float ___U3CxU3Ek__BackingField_1;
	// System.Single RemoteMotionSensorInput::<y>k__BackingField
	float ___U3CyU3Ek__BackingField_2;
	// System.Single RemoteMotionSensorInput::<z>k__BackingField
	float ___U3CzU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t407189685, ___OnValueChanged_0)); }
	inline RemoteMotionSensorInputEventHandler_t2314235825 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteMotionSensorInputEventHandler_t2314235825 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteMotionSensorInputEventHandler_t2314235825 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of_U3CxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t407189685, ___U3CxU3Ek__BackingField_1)); }
	inline float get_U3CxU3Ek__BackingField_1() const { return ___U3CxU3Ek__BackingField_1; }
	inline float* get_address_of_U3CxU3Ek__BackingField_1() { return &___U3CxU3Ek__BackingField_1; }
	inline void set_U3CxU3Ek__BackingField_1(float value)
	{
		___U3CxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t407189685, ___U3CyU3Ek__BackingField_2)); }
	inline float get_U3CyU3Ek__BackingField_2() const { return ___U3CyU3Ek__BackingField_2; }
	inline float* get_address_of_U3CyU3Ek__BackingField_2() { return &___U3CyU3Ek__BackingField_2; }
	inline void set_U3CyU3Ek__BackingField_2(float value)
	{
		___U3CyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CzU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t407189685, ___U3CzU3Ek__BackingField_3)); }
	inline float get_U3CzU3Ek__BackingField_3() const { return ___U3CzU3Ek__BackingField_3; }
	inline float* get_address_of_U3CzU3Ek__BackingField_3() { return &___U3CzU3Ek__BackingField_3; }
	inline void set_U3CzU3Ek__BackingField_3(float value)
	{
		___U3CzU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONSENSORINPUT_T407189685_H
#ifndef U3CBUFFERPOSITIONU3EC__ITERATOR1_T3875555976_H
#define U3CBUFFERPOSITIONU3EC__ITERATOR1_T3875555976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1
struct  U3CBufferPositionU3Ec__Iterator1_t3875555976  : public RuntimeObject
{
public:
	// Mira.RotationalTrackingManager Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$this
	RotationalTrackingManager_t1723746904 * ___U24this_0;
	// System.Object Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t3875555976, ___U24this_0)); }
	inline RotationalTrackingManager_t1723746904 * get_U24this_0() const { return ___U24this_0; }
	inline RotationalTrackingManager_t1723746904 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RotationalTrackingManager_t1723746904 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t3875555976, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t3875555976, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t3875555976, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUFFERPOSITIONU3EC__ITERATOR1_T3875555976_H
#ifndef REMOTEORIENTATIONINPUT_T714724027_H
#define REMOTEORIENTATIONINPUT_T714724027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteOrientationInput
struct  RemoteOrientationInput_t714724027  : public RuntimeObject
{
public:
	// RemoteOrientationInput/RemoteOrientationInputEventHandler RemoteOrientationInput::OnValueChanged
	RemoteOrientationInputEventHandler_t1727624703 * ___OnValueChanged_0;
	// System.Single RemoteOrientationInput::<pitch>k__BackingField
	float ___U3CpitchU3Ek__BackingField_1;
	// System.Single RemoteOrientationInput::<yaw>k__BackingField
	float ___U3CyawU3Ek__BackingField_2;
	// System.Single RemoteOrientationInput::<roll>k__BackingField
	float ___U3CrollU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t714724027, ___OnValueChanged_0)); }
	inline RemoteOrientationInputEventHandler_t1727624703 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteOrientationInputEventHandler_t1727624703 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteOrientationInputEventHandler_t1727624703 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of_U3CpitchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t714724027, ___U3CpitchU3Ek__BackingField_1)); }
	inline float get_U3CpitchU3Ek__BackingField_1() const { return ___U3CpitchU3Ek__BackingField_1; }
	inline float* get_address_of_U3CpitchU3Ek__BackingField_1() { return &___U3CpitchU3Ek__BackingField_1; }
	inline void set_U3CpitchU3Ek__BackingField_1(float value)
	{
		___U3CpitchU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CyawU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t714724027, ___U3CyawU3Ek__BackingField_2)); }
	inline float get_U3CyawU3Ek__BackingField_2() const { return ___U3CyawU3Ek__BackingField_2; }
	inline float* get_address_of_U3CyawU3Ek__BackingField_2() { return &___U3CyawU3Ek__BackingField_2; }
	inline void set_U3CyawU3Ek__BackingField_2(float value)
	{
		___U3CyawU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CrollU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t714724027, ___U3CrollU3Ek__BackingField_3)); }
	inline float get_U3CrollU3Ek__BackingField_3() const { return ___U3CrollU3Ek__BackingField_3; }
	inline float* get_address_of_U3CrollU3Ek__BackingField_3() { return &___U3CrollU3Ek__BackingField_3; }
	inline void set_U3CrollU3Ek__BackingField_3(float value)
	{
		___U3CrollU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEORIENTATIONINPUT_T714724027_H
#ifndef U3CDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY2_T620331307_H
#define U3CDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY2_T620331307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/<DisconnectConnectedRemote>c__AnonStorey2
struct  U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t620331307  : public RuntimeObject
{
public:
	// System.Action`1<MiraRemoteException> RemoteManager/<DisconnectConnectedRemote>c__AnonStorey2::action
	Action_1_t3895617434 * ___action_0;
	// RemoteManager RemoteManager/<DisconnectConnectedRemote>c__AnonStorey2::$this
	RemoteManager_t1941324022 * ___U24this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t620331307, ___action_0)); }
	inline Action_1_t3895617434 * get_action_0() const { return ___action_0; }
	inline Action_1_t3895617434 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3895617434 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t620331307, ___U24this_1)); }
	inline RemoteManager_t1941324022 * get_U24this_1() const { return ___U24this_1; }
	inline RemoteManager_t1941324022 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RemoteManager_t1941324022 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY2_T620331307_H
#ifndef U3CCONNECTREMOTEU3EC__ANONSTOREY1_T2642683055_H
#define U3CCONNECTREMOTEU3EC__ANONSTOREY1_T2642683055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/<ConnectRemote>c__AnonStorey1
struct  U3CConnectRemoteU3Ec__AnonStorey1_t2642683055  : public RuntimeObject
{
public:
	// Remote RemoteManager/<ConnectRemote>c__AnonStorey1::remote
	Remote_t1018512123 * ___remote_0;
	// System.Action`1<MiraRemoteException> RemoteManager/<ConnectRemote>c__AnonStorey1::action
	Action_1_t3895617434 * ___action_1;
	// RemoteManager RemoteManager/<ConnectRemote>c__AnonStorey1::$this
	RemoteManager_t1941324022 * ___U24this_2;

public:
	inline static int32_t get_offset_of_remote_0() { return static_cast<int32_t>(offsetof(U3CConnectRemoteU3Ec__AnonStorey1_t2642683055, ___remote_0)); }
	inline Remote_t1018512123 * get_remote_0() const { return ___remote_0; }
	inline Remote_t1018512123 ** get_address_of_remote_0() { return &___remote_0; }
	inline void set_remote_0(Remote_t1018512123 * value)
	{
		___remote_0 = value;
		Il2CppCodeGenWriteBarrier((&___remote_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CConnectRemoteU3Ec__AnonStorey1_t2642683055, ___action_1)); }
	inline Action_1_t3895617434 * get_action_1() const { return ___action_1; }
	inline Action_1_t3895617434 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3895617434 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CConnectRemoteU3Ec__AnonStorey1_t2642683055, ___U24this_2)); }
	inline RemoteManager_t1941324022 * get_U24this_2() const { return ___U24this_2; }
	inline RemoteManager_t1941324022 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(RemoteManager_t1941324022 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONNECTREMOTEU3EC__ANONSTOREY1_T2642683055_H
#ifndef U3CACTIVATETRACKINGU3EC__ITERATOR0_T1610460162_H
#define U3CACTIVATETRACKINGU3EC__ITERATOR0_T1610460162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraWikitudeManager/<ActivateTracking>c__Iterator0
struct  U3CActivateTrackingU3Ec__Iterator0_t1610460162  : public RuntimeObject
{
public:
	// MiraWikitudeManager MiraWikitudeManager/<ActivateTracking>c__Iterator0::$this
	MiraWikitudeManager_t2444513627 * ___U24this_0;
	// System.Object MiraWikitudeManager/<ActivateTracking>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MiraWikitudeManager/<ActivateTracking>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MiraWikitudeManager/<ActivateTracking>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1610460162, ___U24this_0)); }
	inline MiraWikitudeManager_t2444513627 * get_U24this_0() const { return ___U24this_0; }
	inline MiraWikitudeManager_t2444513627 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MiraWikitudeManager_t2444513627 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1610460162, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1610460162, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1610460162, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATETRACKINGU3EC__ITERATOR0_T1610460162_H
#ifndef SERIALIZABLEPOINTCLOUD_T455238287_H
#define SERIALIZABLEPOINTCLOUD_T455238287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializablePointCloud
struct  serializablePointCloud_t455238287  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_t4116647657* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_t455238287, ___pointCloudData_0)); }
	inline ByteU5BU5D_t4116647657* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_t4116647657* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_T455238287_H
#ifndef REMOTETOUCHINPUT_T3055303244_H
#define REMOTETOUCHINPUT_T3055303244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchInput
struct  RemoteTouchInput_t3055303244  : public RuntimeObject
{
public:
	// RemoteTouchInput/RemoteTouchInputEventHandler RemoteTouchInput::OnActiveChanged
	RemoteTouchInputEventHandler_t3839927206 * ___OnActiveChanged_0;
	// System.Boolean RemoteTouchInput::_isActive
	bool ____isActive_1;

public:
	inline static int32_t get_offset_of_OnActiveChanged_0() { return static_cast<int32_t>(offsetof(RemoteTouchInput_t3055303244, ___OnActiveChanged_0)); }
	inline RemoteTouchInputEventHandler_t3839927206 * get_OnActiveChanged_0() const { return ___OnActiveChanged_0; }
	inline RemoteTouchInputEventHandler_t3839927206 ** get_address_of_OnActiveChanged_0() { return &___OnActiveChanged_0; }
	inline void set_OnActiveChanged_0(RemoteTouchInputEventHandler_t3839927206 * value)
	{
		___OnActiveChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnActiveChanged_0), value);
	}

	inline static int32_t get_offset_of__isActive_1() { return static_cast<int32_t>(offsetof(RemoteTouchInput_t3055303244, ____isActive_1)); }
	inline bool get__isActive_1() const { return ____isActive_1; }
	inline bool* get_address_of__isActive_1() { return &____isActive_1; }
	inline void set__isActive_1(bool value)
	{
		____isActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHINPUT_T3055303244_H
#ifndef U3CCHECKFORCHANGEU3EC__ITERATOR0_T3804541347_H
#define U3CCHECKFORCHANGEU3EC__ITERATOR0_T3804541347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceOrientationChange/<CheckForChange>c__Iterator0
struct  U3CCheckForChangeU3Ec__Iterator0_t3804541347  : public RuntimeObject
{
public:
	// System.Object DeviceOrientationChange/<CheckForChange>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean DeviceOrientationChange/<CheckForChange>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 DeviceOrientationChange/<CheckForChange>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ec__Iterator0_t3804541347, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ec__Iterator0_t3804541347, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ec__Iterator0_t3804541347, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORCHANGEU3EC__ITERATOR0_T3804541347_H
#ifndef U3CSTARTCONTROLLERU3EC__ITERATOR0_T3465610773_H
#define U3CSTARTCONTROLLERU3EC__ITERATOR0_T3465610773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<StartController>c__Iterator0
struct  U3CStartControllerU3Ec__Iterator0_t3465610773  : public RuntimeObject
{
public:
	// MiraController MiraController/<StartController>c__Iterator0::$this
	MiraController_t2033339498 * ___U24this_0;
	// System.Object MiraController/<StartController>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MiraController/<StartController>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MiraController/<StartController>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3465610773, ___U24this_0)); }
	inline MiraController_t2033339498 * get_U24this_0() const { return ___U24this_0; }
	inline MiraController_t2033339498 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MiraController_t2033339498 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3465610773, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3465610773, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3465610773, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTCONTROLLERU3EC__ITERATOR0_T3465610773_H
#ifndef MIRABASEPOINTER_T2409431493_H
#define MIRABASEPOINTER_T2409431493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBasePointer
struct  MiraBasePointer_t2409431493  : public RuntimeObject
{
public:
	// System.Single MiraBasePointer::<maxDistance>k__BackingField
	float ___U3CmaxDistanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmaxDistanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MiraBasePointer_t2409431493, ___U3CmaxDistanceU3Ek__BackingField_0)); }
	inline float get_U3CmaxDistanceU3Ek__BackingField_0() const { return ___U3CmaxDistanceU3Ek__BackingField_0; }
	inline float* get_address_of_U3CmaxDistanceU3Ek__BackingField_0() { return &___U3CmaxDistanceU3Ek__BackingField_0; }
	inline void set_U3CmaxDistanceU3Ek__BackingField_0(float value)
	{
		___U3CmaxDistanceU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASEPOINTER_T2409431493_H
#ifndef MIRABTREMOTEINPUT_T3160068026_H
#define MIRABTREMOTEINPUT_T3160068026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBTRemoteInput
struct  MiraBTRemoteInput_t3160068026  : public RuntimeObject
{
public:
	// RemoteBase MiraBTRemoteInput::controller
	RemoteBase_t3398252275 * ___controller_0;
	// VirtualRemote MiraBTRemoteInput::_virtualRemote
	VirtualRemote_t2507984974 * ____virtualRemote_1;
	// Remote MiraBTRemoteInput::_connectedRemote
	Remote_t1018512123 * ____connectedRemote_2;
	// Remote MiraBTRemoteInput::connectedRemote
	Remote_t1018512123 * ___connectedRemote_3;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t3160068026, ___controller_0)); }
	inline RemoteBase_t3398252275 * get_controller_0() const { return ___controller_0; }
	inline RemoteBase_t3398252275 ** get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(RemoteBase_t3398252275 * value)
	{
		___controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___controller_0), value);
	}

	inline static int32_t get_offset_of__virtualRemote_1() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t3160068026, ____virtualRemote_1)); }
	inline VirtualRemote_t2507984974 * get__virtualRemote_1() const { return ____virtualRemote_1; }
	inline VirtualRemote_t2507984974 ** get_address_of__virtualRemote_1() { return &____virtualRemote_1; }
	inline void set__virtualRemote_1(VirtualRemote_t2507984974 * value)
	{
		____virtualRemote_1 = value;
		Il2CppCodeGenWriteBarrier((&____virtualRemote_1), value);
	}

	inline static int32_t get_offset_of__connectedRemote_2() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t3160068026, ____connectedRemote_2)); }
	inline Remote_t1018512123 * get__connectedRemote_2() const { return ____connectedRemote_2; }
	inline Remote_t1018512123 ** get_address_of__connectedRemote_2() { return &____connectedRemote_2; }
	inline void set__connectedRemote_2(Remote_t1018512123 * value)
	{
		____connectedRemote_2 = value;
		Il2CppCodeGenWriteBarrier((&____connectedRemote_2), value);
	}

	inline static int32_t get_offset_of_connectedRemote_3() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t3160068026, ___connectedRemote_3)); }
	inline Remote_t1018512123 * get_connectedRemote_3() const { return ___connectedRemote_3; }
	inline Remote_t1018512123 ** get_address_of_connectedRemote_3() { return &___connectedRemote_3; }
	inline void set_connectedRemote_3(Remote_t1018512123 * value)
	{
		___connectedRemote_3 = value;
		Il2CppCodeGenWriteBarrier((&___connectedRemote_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABTREMOTEINPUT_T3160068026_H
#ifndef U3CREMOTEDISCONNECTEDNOTIFICATIONU3EC__ITERATOR1_T3913456614_H
#define U3CREMOTEDISCONNECTEDNOTIFICATIONU3EC__ITERATOR1_T3913456614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager/<RemoteDisconnectedNotification>c__Iterator1
struct  U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614  : public RuntimeObject
{
public:
	// SettingsManager SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$this
	SettingsManager_t2083239687 * ___U24this_0;
	// System.Object SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614, ___U24this_0)); }
	inline SettingsManager_t2083239687 * get_U24this_0() const { return ___U24this_0; }
	inline SettingsManager_t2083239687 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SettingsManager_t2083239687 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEDISCONNECTEDNOTIFICATIONU3EC__ITERATOR1_T3913456614_H
#ifndef U3CREMOTECONNECTEDNOTIFICATIONU3EC__ITERATOR0_T555146399_H
#define U3CREMOTECONNECTEDNOTIFICATIONU3EC__ITERATOR0_T555146399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager/<RemoteConnectedNotification>c__Iterator0
struct  U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399  : public RuntimeObject
{
public:
	// SettingsManager SettingsManager/<RemoteConnectedNotification>c__Iterator0::$this
	SettingsManager_t2083239687 * ___U24this_0;
	// System.Object SettingsManager/<RemoteConnectedNotification>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SettingsManager/<RemoteConnectedNotification>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SettingsManager/<RemoteConnectedNotification>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399, ___U24this_0)); }
	inline SettingsManager_t2083239687 * get_U24this_0() const { return ___U24this_0; }
	inline SettingsManager_t2083239687 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SettingsManager_t2083239687 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTECONNECTEDNOTIFICATIONU3EC__ITERATOR0_T555146399_H
#ifndef U3CDELAYEDRECENTERU3EC__ITERATOR1_T3514494614_H
#define U3CDELAYEDRECENTERU3EC__ITERATOR1_T3514494614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<DelayedRecenter>c__Iterator1
struct  U3CDelayedRecenterU3Ec__Iterator1_t3514494614  : public RuntimeObject
{
public:
	// MiraController MiraController/<DelayedRecenter>c__Iterator1::$this
	MiraController_t2033339498 * ___U24this_0;
	// System.Object MiraController/<DelayedRecenter>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MiraController/<DelayedRecenter>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 MiraController/<DelayedRecenter>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3514494614, ___U24this_0)); }
	inline MiraController_t2033339498 * get_U24this_0() const { return ___U24this_0; }
	inline MiraController_t2033339498 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MiraController_t2033339498 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3514494614, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3514494614, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3514494614, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDRECENTERU3EC__ITERATOR1_T3514494614_H
#ifndef U3CRETURNTOHOMEU3EC__ITERATOR2_T2347054426_H
#define U3CRETURNTOHOMEU3EC__ITERATOR2_T2347054426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<ReturnToHome>c__Iterator2
struct  U3CReturnToHomeU3Ec__Iterator2_t2347054426  : public RuntimeObject
{
public:
	// System.Single MiraController/<ReturnToHome>c__Iterator2::<timer>__0
	float ___U3CtimerU3E__0_0;
	// System.Boolean MiraController/<ReturnToHome>c__Iterator2::<animStarted>__0
	bool ___U3CanimStartedU3E__0_1;
	// System.Single MiraController/<ReturnToHome>c__Iterator2::<animProgress>__0
	float ___U3CanimProgressU3E__0_2;
	// MiraController MiraController/<ReturnToHome>c__Iterator2::$this
	MiraController_t2033339498 * ___U24this_3;
	// System.Object MiraController/<ReturnToHome>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MiraController/<ReturnToHome>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 MiraController/<ReturnToHome>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U3CtimerU3E__0_0)); }
	inline float get_U3CtimerU3E__0_0() const { return ___U3CtimerU3E__0_0; }
	inline float* get_address_of_U3CtimerU3E__0_0() { return &___U3CtimerU3E__0_0; }
	inline void set_U3CtimerU3E__0_0(float value)
	{
		___U3CtimerU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CanimStartedU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U3CanimStartedU3E__0_1)); }
	inline bool get_U3CanimStartedU3E__0_1() const { return ___U3CanimStartedU3E__0_1; }
	inline bool* get_address_of_U3CanimStartedU3E__0_1() { return &___U3CanimStartedU3E__0_1; }
	inline void set_U3CanimStartedU3E__0_1(bool value)
	{
		___U3CanimStartedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CanimProgressU3E__0_2() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U3CanimProgressU3E__0_2)); }
	inline float get_U3CanimProgressU3E__0_2() const { return ___U3CanimProgressU3E__0_2; }
	inline float* get_address_of_U3CanimProgressU3E__0_2() { return &___U3CanimProgressU3E__0_2; }
	inline void set_U3CanimProgressU3E__0_2(float value)
	{
		___U3CanimProgressU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U24this_3)); }
	inline MiraController_t2033339498 * get_U24this_3() const { return ___U24this_3; }
	inline MiraController_t2033339498 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MiraController_t2033339498 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t2347054426, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRETURNTOHOMEU3EC__ITERATOR2_T2347054426_H
#ifndef U3CSTARTREMOTEDISCOVERYU3EC__ANONSTOREY0_T1952101060_H
#define U3CSTARTREMOTEDISCOVERYU3EC__ANONSTOREY0_T1952101060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/<StartRemoteDiscovery>c__AnonStorey0
struct  U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1952101060  : public RuntimeObject
{
public:
	// System.Action`1<Remote> RemoteManager/<StartRemoteDiscovery>c__AnonStorey0::action
	Action_1_t1190979718 * ___action_0;
	// RemoteManager RemoteManager/<StartRemoteDiscovery>c__AnonStorey0::$this
	RemoteManager_t1941324022 * ___U24this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1952101060, ___action_0)); }
	inline Action_1_t1190979718 * get_action_0() const { return ___action_0; }
	inline Action_1_t1190979718 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1190979718 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1952101060, ___U24this_1)); }
	inline RemoteManager_t1941324022 * get_U24this_1() const { return ___U24this_1; }
	inline RemoteManager_t1941324022 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RemoteManager_t1941324022 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTREMOTEDISCOVERYU3EC__ANONSTOREY0_T1952101060_H
#ifndef U3CREMOTEMANAGERDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY4_T685188581_H
#define U3CREMOTEMANAGERDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY4_T685188581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerDisconnectConnectedRemote>c__AnonStorey4
struct  U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t685188581  : public RuntimeObject
{
public:
	// System.Action`1<MiraRemoteException> NativeBridge/<RemoteManagerDisconnectConnectedRemote>c__AnonStorey4::action
	Action_1_t3895617434 * ___action_0;
	// System.String NativeBridge/<RemoteManagerDisconnectConnectedRemote>c__AnonStorey4::identifier
	String_t* ___identifier_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t685188581, ___action_0)); }
	inline Action_1_t3895617434 * get_action_0() const { return ___action_0; }
	inline Action_1_t3895617434 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3895617434 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_identifier_1() { return static_cast<int32_t>(offsetof(U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t685188581, ___identifier_1)); }
	inline String_t* get_identifier_1() const { return ___identifier_1; }
	inline String_t** get_address_of_identifier_1() { return &___identifier_1; }
	inline void set_identifier_1(String_t* value)
	{
		___identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY4_T685188581_H
#ifndef U3CREMOTEREFRESHU3EC__ANONSTOREY5_T769484850_H
#define U3CREMOTEREFRESHU3EC__ANONSTOREY5_T769484850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteRefresh>c__AnonStorey5
struct  U3CRemoteRefreshU3Ec__AnonStorey5_t769484850  : public RuntimeObject
{
public:
	// Remote NativeBridge/<RemoteRefresh>c__AnonStorey5::remote
	Remote_t1018512123 * ___remote_0;
	// System.Action`1<MiraRemoteException> NativeBridge/<RemoteRefresh>c__AnonStorey5::action
	Action_1_t3895617434 * ___action_1;
	// System.String NativeBridge/<RemoteRefresh>c__AnonStorey5::identifier
	String_t* ___identifier_2;

public:
	inline static int32_t get_offset_of_remote_0() { return static_cast<int32_t>(offsetof(U3CRemoteRefreshU3Ec__AnonStorey5_t769484850, ___remote_0)); }
	inline Remote_t1018512123 * get_remote_0() const { return ___remote_0; }
	inline Remote_t1018512123 ** get_address_of_remote_0() { return &___remote_0; }
	inline void set_remote_0(Remote_t1018512123 * value)
	{
		___remote_0 = value;
		Il2CppCodeGenWriteBarrier((&___remote_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CRemoteRefreshU3Ec__AnonStorey5_t769484850, ___action_1)); }
	inline Action_1_t3895617434 * get_action_1() const { return ___action_1; }
	inline Action_1_t3895617434 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3895617434 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(U3CRemoteRefreshU3Ec__AnonStorey5_t769484850, ___identifier_2)); }
	inline String_t* get_identifier_2() const { return ___identifier_2; }
	inline String_t** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(String_t* value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEREFRESHU3EC__ANONSTOREY5_T769484850_H
#ifndef REMOTEAXISINPUT_T1550365229_H
#define REMOTEAXISINPUT_T1550365229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteAxisInput
struct  RemoteAxisInput_t1550365229  : public RuntimeObject
{
public:
	// RemoteAxisInput/RemoteAxisInputEventHandler RemoteAxisInput::OnValueChanged
	RemoteAxisInputEventHandler_t4129402086 * ___OnValueChanged_0;
	// System.Single RemoteAxisInput::_value
	float ____value_1;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteAxisInput_t1550365229, ___OnValueChanged_0)); }
	inline RemoteAxisInputEventHandler_t4129402086 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteAxisInputEventHandler_t4129402086 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteAxisInputEventHandler_t4129402086 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(RemoteAxisInput_t1550365229, ____value_1)); }
	inline float get__value_1() const { return ____value_1; }
	inline float* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(float value)
	{
		____value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEAXISINPUT_T1550365229_H
#ifndef U3CREMOTEMANAGERCONNECTREMOTEU3EC__ANONSTOREY3_T300449441_H
#define U3CREMOTEMANAGERCONNECTREMOTEU3EC__ANONSTOREY3_T300449441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3
struct  U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441  : public RuntimeObject
{
public:
	// Remote NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3::remote
	Remote_t1018512123 * ___remote_0;
	// System.Action`1<MiraRemoteException> NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3::action
	Action_1_t3895617434 * ___action_1;
	// System.String NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3::identifier
	String_t* ___identifier_2;

public:
	inline static int32_t get_offset_of_remote_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441, ___remote_0)); }
	inline Remote_t1018512123 * get_remote_0() const { return ___remote_0; }
	inline Remote_t1018512123 ** get_address_of_remote_0() { return &___remote_0; }
	inline void set_remote_0(Remote_t1018512123 * value)
	{
		___remote_0 = value;
		Il2CppCodeGenWriteBarrier((&___remote_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441, ___action_1)); }
	inline Action_1_t3895617434 * get_action_1() const { return ___action_1; }
	inline Action_1_t3895617434 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3895617434 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441, ___identifier_2)); }
	inline String_t* get_identifier_2() const { return ___identifier_2; }
	inline String_t** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(String_t* value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERCONNECTREMOTEU3EC__ANONSTOREY3_T300449441_H
#ifndef SERIALIZABLEVECTOR4_T1862640084_H
#define SERIALIZABLEVECTOR4_T1862640084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector4
struct  SerializableVector4_t1862640084  : public RuntimeObject
{
public:
	// System.Single Utils.SerializableVector4::x
	float ___x_0;
	// System.Single Utils.SerializableVector4::y
	float ___y_1;
	// System.Single Utils.SerializableVector4::z
	float ___z_2;
	// System.Single Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_T1862640084_H
#ifndef U3CREMOTEMANAGERAUTOMATICALLYCONNECTSTOPREVIOUSCONNECTEDREMOTEU3EC__ANONSTOREY1_T1357687005_H
#define U3CREMOTEMANAGERAUTOMATICALLYCONNECTSTOPREVIOUSCONNECTEDREMOTEU3EC__ANONSTOREY1_T1357687005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerAutomaticallyConnectsToPreviousConnectedRemote>c__AnonStorey1
struct  U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t1357687005  : public RuntimeObject
{
public:
	// System.Boolean NativeBridge/<RemoteManagerAutomaticallyConnectsToPreviousConnectedRemote>c__AnonStorey1::value
	bool ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t1357687005, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERAUTOMATICALLYCONNECTSTOPREVIOUSCONNECTEDREMOTEU3EC__ANONSTOREY1_T1357687005_H
#ifndef U3CREMOTEMANAGERSTARTREMOTEDISCOVERYU3EC__ANONSTOREY2_T3008845974_H
#define U3CREMOTEMANAGERSTARTREMOTEDISCOVERYU3EC__ANONSTOREY2_T3008845974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2
struct  U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974  : public RuntimeObject
{
public:
	// System.Action`1<Remote> NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2::action
	Action_1_t1190979718 * ___action_0;
	// System.String NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2::identifier
	String_t* ___identifier_1;
	// MiraRemoteException NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2::discoveryException
	MiraRemoteException_t3723149839 * ___discoveryException_2;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974, ___action_0)); }
	inline Action_1_t1190979718 * get_action_0() const { return ___action_0; }
	inline Action_1_t1190979718 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1190979718 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_identifier_1() { return static_cast<int32_t>(offsetof(U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974, ___identifier_1)); }
	inline String_t* get_identifier_1() const { return ___identifier_1; }
	inline String_t** get_address_of_identifier_1() { return &___identifier_1; }
	inline void set_identifier_1(String_t* value)
	{
		___identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_1), value);
	}

	inline static int32_t get_offset_of_discoveryException_2() { return static_cast<int32_t>(offsetof(U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974, ___discoveryException_2)); }
	inline MiraRemoteException_t3723149839 * get_discoveryException_2() const { return ___discoveryException_2; }
	inline MiraRemoteException_t3723149839 ** get_address_of_discoveryException_2() { return &___discoveryException_2; }
	inline void set_discoveryException_2(MiraRemoteException_t3723149839 * value)
	{
		___discoveryException_2 = value;
		Il2CppCodeGenWriteBarrier((&___discoveryException_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERSTARTREMOTEDISCOVERYU3EC__ANONSTOREY2_T3008845974_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T1046383205_H
#define OBJECTSERIALIZATIONEXTENSION_T1046383205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t1046383205  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T1046383205_H
#ifndef SERIALIZABLEFLOAT_T3568121741_H
#define SERIALIZABLEFLOAT_T3568121741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFloat
struct  serializableFloat_t3568121741  : public RuntimeObject
{
public:
	// System.Single Utils.serializableFloat::x
	float ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(serializableFloat_t3568121741, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFLOAT_T3568121741_H
#ifndef SERIALIZABLEBTREMOTEBUTTONS_T3417182756_H
#define SERIALIZABLEBTREMOTEBUTTONS_T3417182756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemoteButtons
struct  serializableBTRemoteButtons_t3417182756  : public RuntimeObject
{
public:
	// System.Boolean Utils.serializableBTRemoteButtons::startButton
	bool ___startButton_0;
	// System.Boolean Utils.serializableBTRemoteButtons::backButton
	bool ___backButton_1;
	// System.Boolean Utils.serializableBTRemoteButtons::triggerButton
	bool ___triggerButton_2;

public:
	inline static int32_t get_offset_of_startButton_0() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t3417182756, ___startButton_0)); }
	inline bool get_startButton_0() const { return ___startButton_0; }
	inline bool* get_address_of_startButton_0() { return &___startButton_0; }
	inline void set_startButton_0(bool value)
	{
		___startButton_0 = value;
	}

	inline static int32_t get_offset_of_backButton_1() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t3417182756, ___backButton_1)); }
	inline bool get_backButton_1() const { return ___backButton_1; }
	inline bool* get_address_of_backButton_1() { return &___backButton_1; }
	inline void set_backButton_1(bool value)
	{
		___backButton_1 = value;
	}

	inline static int32_t get_offset_of_triggerButton_2() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t3417182756, ___triggerButton_2)); }
	inline bool get_triggerButton_2() const { return ___triggerButton_2; }
	inline bool* get_address_of_triggerButton_2() { return &___triggerButton_2; }
	inline void set_triggerButton_2(bool value)
	{
		___triggerButton_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTEBUTTONS_T3417182756_H
#ifndef MIRASUBMESSAGEIDS_T4040701080_H
#define MIRASUBMESSAGEIDS_T4040701080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraSubMessageIds
struct  MiraSubMessageIds_t4040701080  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRASUBMESSAGEIDS_T4040701080_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MIRACONNECTIONMESSAGEIDS_T116877937_H
#define MIRACONNECTIONMESSAGEIDS_T116877937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraConnectionMessageIds
struct  MiraConnectionMessageIds_t116877937  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRACONNECTIONMESSAGEIDS_T116877937_H
#ifndef REMOTEBASE_T3398252275_H
#define REMOTEBASE_T3398252275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteBase
struct  RemoteBase_t3398252275  : public RuntimeObject
{
public:
	// RemoteButtonInput RemoteBase::<menuButton>k__BackingField
	RemoteButtonInput_t3750858021 * ___U3CmenuButtonU3Ek__BackingField_0;
	// RemoteButtonInput RemoteBase::<homeButton>k__BackingField
	RemoteButtonInput_t3750858021 * ___U3ChomeButtonU3Ek__BackingField_1;
	// RemoteButtonInput RemoteBase::<trigger>k__BackingField
	RemoteButtonInput_t3750858021 * ___U3CtriggerU3Ek__BackingField_2;
	// RemoteTouchPadInput RemoteBase::<touchPad>k__BackingField
	RemoteTouchPadInput_t3817316988 * ___U3CtouchPadU3Ek__BackingField_3;
	// RemoteMotionInput RemoteBase::<motion>k__BackingField
	RemoteMotionInput_t177155726 * ___U3CmotionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CmenuButtonU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RemoteBase_t3398252275, ___U3CmenuButtonU3Ek__BackingField_0)); }
	inline RemoteButtonInput_t3750858021 * get_U3CmenuButtonU3Ek__BackingField_0() const { return ___U3CmenuButtonU3Ek__BackingField_0; }
	inline RemoteButtonInput_t3750858021 ** get_address_of_U3CmenuButtonU3Ek__BackingField_0() { return &___U3CmenuButtonU3Ek__BackingField_0; }
	inline void set_U3CmenuButtonU3Ek__BackingField_0(RemoteButtonInput_t3750858021 * value)
	{
		___U3CmenuButtonU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmenuButtonU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ChomeButtonU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteBase_t3398252275, ___U3ChomeButtonU3Ek__BackingField_1)); }
	inline RemoteButtonInput_t3750858021 * get_U3ChomeButtonU3Ek__BackingField_1() const { return ___U3ChomeButtonU3Ek__BackingField_1; }
	inline RemoteButtonInput_t3750858021 ** get_address_of_U3ChomeButtonU3Ek__BackingField_1() { return &___U3ChomeButtonU3Ek__BackingField_1; }
	inline void set_U3ChomeButtonU3Ek__BackingField_1(RemoteButtonInput_t3750858021 * value)
	{
		___U3ChomeButtonU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChomeButtonU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtriggerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteBase_t3398252275, ___U3CtriggerU3Ek__BackingField_2)); }
	inline RemoteButtonInput_t3750858021 * get_U3CtriggerU3Ek__BackingField_2() const { return ___U3CtriggerU3Ek__BackingField_2; }
	inline RemoteButtonInput_t3750858021 ** get_address_of_U3CtriggerU3Ek__BackingField_2() { return &___U3CtriggerU3Ek__BackingField_2; }
	inline void set_U3CtriggerU3Ek__BackingField_2(RemoteButtonInput_t3750858021 * value)
	{
		___U3CtriggerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtriggerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtouchPadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteBase_t3398252275, ___U3CtouchPadU3Ek__BackingField_3)); }
	inline RemoteTouchPadInput_t3817316988 * get_U3CtouchPadU3Ek__BackingField_3() const { return ___U3CtouchPadU3Ek__BackingField_3; }
	inline RemoteTouchPadInput_t3817316988 ** get_address_of_U3CtouchPadU3Ek__BackingField_3() { return &___U3CtouchPadU3Ek__BackingField_3; }
	inline void set_U3CtouchPadU3Ek__BackingField_3(RemoteTouchPadInput_t3817316988 * value)
	{
		___U3CtouchPadU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchPadU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CmotionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteBase_t3398252275, ___U3CmotionU3Ek__BackingField_4)); }
	inline RemoteMotionInput_t177155726 * get_U3CmotionU3Ek__BackingField_4() const { return ___U3CmotionU3Ek__BackingField_4; }
	inline RemoteMotionInput_t177155726 ** get_address_of_U3CmotionU3Ek__BackingField_4() { return &___U3CmotionU3Ek__BackingField_4; }
	inline void set_U3CmotionU3Ek__BackingField_4(RemoteMotionInput_t177155726 * value)
	{
		___U3CmotionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmotionU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBASE_T3398252275_H
#ifndef REMOTEBUTTONINPUT_T3750858021_H
#define REMOTEBUTTONINPUT_T3750858021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteButtonInput
struct  RemoteButtonInput_t3750858021  : public RuntimeObject
{
public:
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnPressChanged
	RemoteButtonInputEventHandler_t2005612063 * ___OnPressChanged_0;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnHeldState
	RemoteButtonInputEventHandler_t2005612063 * ___OnHeldState_1;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnPresseddState
	RemoteButtonInputEventHandler_t2005612063 * ___OnPresseddState_2;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnReleasedState
	RemoteButtonInputEventHandler_t2005612063 * ___OnReleasedState_3;
	// System.Int32 RemoteButtonInput::m_PrevFrameCount
	int32_t ___m_PrevFrameCount_4;
	// System.Boolean RemoteButtonInput::m_PrevState
	bool ___m_PrevState_5;
	// System.Boolean RemoteButtonInput::m_State
	bool ___m_State_6;
	// System.Boolean RemoteButtonInput::_isPressed
	bool ____isPressed_7;
	// System.Boolean RemoteButtonInput::_onPressed
	bool ____onPressed_8;
	// System.Boolean RemoteButtonInput::_onReleased
	bool ____onReleased_9;
	// System.Boolean RemoteButtonInput::_onHeld
	bool ____onHeld_10;

public:
	inline static int32_t get_offset_of_OnPressChanged_0() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___OnPressChanged_0)); }
	inline RemoteButtonInputEventHandler_t2005612063 * get_OnPressChanged_0() const { return ___OnPressChanged_0; }
	inline RemoteButtonInputEventHandler_t2005612063 ** get_address_of_OnPressChanged_0() { return &___OnPressChanged_0; }
	inline void set_OnPressChanged_0(RemoteButtonInputEventHandler_t2005612063 * value)
	{
		___OnPressChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressChanged_0), value);
	}

	inline static int32_t get_offset_of_OnHeldState_1() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___OnHeldState_1)); }
	inline RemoteButtonInputEventHandler_t2005612063 * get_OnHeldState_1() const { return ___OnHeldState_1; }
	inline RemoteButtonInputEventHandler_t2005612063 ** get_address_of_OnHeldState_1() { return &___OnHeldState_1; }
	inline void set_OnHeldState_1(RemoteButtonInputEventHandler_t2005612063 * value)
	{
		___OnHeldState_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeldState_1), value);
	}

	inline static int32_t get_offset_of_OnPresseddState_2() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___OnPresseddState_2)); }
	inline RemoteButtonInputEventHandler_t2005612063 * get_OnPresseddState_2() const { return ___OnPresseddState_2; }
	inline RemoteButtonInputEventHandler_t2005612063 ** get_address_of_OnPresseddState_2() { return &___OnPresseddState_2; }
	inline void set_OnPresseddState_2(RemoteButtonInputEventHandler_t2005612063 * value)
	{
		___OnPresseddState_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnPresseddState_2), value);
	}

	inline static int32_t get_offset_of_OnReleasedState_3() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___OnReleasedState_3)); }
	inline RemoteButtonInputEventHandler_t2005612063 * get_OnReleasedState_3() const { return ___OnReleasedState_3; }
	inline RemoteButtonInputEventHandler_t2005612063 ** get_address_of_OnReleasedState_3() { return &___OnReleasedState_3; }
	inline void set_OnReleasedState_3(RemoteButtonInputEventHandler_t2005612063 * value)
	{
		___OnReleasedState_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnReleasedState_3), value);
	}

	inline static int32_t get_offset_of_m_PrevFrameCount_4() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___m_PrevFrameCount_4)); }
	inline int32_t get_m_PrevFrameCount_4() const { return ___m_PrevFrameCount_4; }
	inline int32_t* get_address_of_m_PrevFrameCount_4() { return &___m_PrevFrameCount_4; }
	inline void set_m_PrevFrameCount_4(int32_t value)
	{
		___m_PrevFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_5() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___m_PrevState_5)); }
	inline bool get_m_PrevState_5() const { return ___m_PrevState_5; }
	inline bool* get_address_of_m_PrevState_5() { return &___m_PrevState_5; }
	inline void set_m_PrevState_5(bool value)
	{
		___m_PrevState_5 = value;
	}

	inline static int32_t get_offset_of_m_State_6() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ___m_State_6)); }
	inline bool get_m_State_6() const { return ___m_State_6; }
	inline bool* get_address_of_m_State_6() { return &___m_State_6; }
	inline void set_m_State_6(bool value)
	{
		___m_State_6 = value;
	}

	inline static int32_t get_offset_of__isPressed_7() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ____isPressed_7)); }
	inline bool get__isPressed_7() const { return ____isPressed_7; }
	inline bool* get_address_of__isPressed_7() { return &____isPressed_7; }
	inline void set__isPressed_7(bool value)
	{
		____isPressed_7 = value;
	}

	inline static int32_t get_offset_of__onPressed_8() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ____onPressed_8)); }
	inline bool get__onPressed_8() const { return ____onPressed_8; }
	inline bool* get_address_of__onPressed_8() { return &____onPressed_8; }
	inline void set__onPressed_8(bool value)
	{
		____onPressed_8 = value;
	}

	inline static int32_t get_offset_of__onReleased_9() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ____onReleased_9)); }
	inline bool get__onReleased_9() const { return ____onReleased_9; }
	inline bool* get_address_of__onReleased_9() { return &____onReleased_9; }
	inline void set__onReleased_9(bool value)
	{
		____onReleased_9 = value;
	}

	inline static int32_t get_offset_of__onHeld_10() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t3750858021, ____onHeld_10)); }
	inline bool get__onHeld_10() const { return ____onHeld_10; }
	inline bool* get_address_of__onHeld_10() { return &____onHeld_10; }
	inline void set__onHeld_10(bool value)
	{
		____onHeld_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBUTTONINPUT_T3750858021_H
#ifndef MIRARETICLEPOINTER_T3058549069_H
#define MIRARETICLEPOINTER_T3058549069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraReticlePointer
struct  MiraReticlePointer_t3058549069  : public MiraBasePointer_t2409431493
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRARETICLEPOINTER_T3058549069_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SERIALIZABLEVECTOR2_T1862640090_H
#define SERIALIZABLEVECTOR2_T1862640090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector2
struct  SerializableVector2_t1862640090 
{
public:
	// System.Single Utils.SerializableVector2::x
	float ___x_0;
	// System.Single Utils.SerializableVector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector2_t1862640090, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector2_t1862640090, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR2_T1862640090_H
#ifndef SERIALIZABLEVECTOR3_T1862640089_H
#define SERIALIZABLEVECTOR3_T1862640089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector3
struct  SerializableVector3_t1862640089 
{
public:
	// System.Single Utils.SerializableVector3::x
	float ___x_0;
	// System.Single Utils.SerializableVector3::y
	float ___y_1;
	// System.Single Utils.SerializableVector3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector3_t1862640089, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector3_t1862640089, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector3_t1862640089, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR3_T1862640089_H
#ifndef REMOTETOUCHPADINPUT_T3817316988_H
#define REMOTETOUCHPADINPUT_T3817316988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchPadInput
struct  RemoteTouchPadInput_t3817316988  : public RemoteTouchInput_t3055303244
{
public:
	// RemoteTouchPadInput/RemoteTouchPadInputEventHandler RemoteTouchPadInput::OnValueChanged
	RemoteTouchPadInputEventHandler_t3280214852 * ___OnValueChanged_2;
	// RemoteButtonInput RemoteTouchPadInput::<button>k__BackingField
	RemoteButtonInput_t3750858021 * ___U3CbuttonU3Ek__BackingField_3;
	// RemoteButtonInput RemoteTouchPadInput::<touchActive>k__BackingField
	RemoteButtonInput_t3750858021 * ___U3CtouchActiveU3Ek__BackingField_4;
	// RemoteAxisInput RemoteTouchPadInput::<xAxis>k__BackingField
	RemoteAxisInput_t1550365229 * ___U3CxAxisU3Ek__BackingField_5;
	// RemoteAxisInput RemoteTouchPadInput::<yAxis>k__BackingField
	RemoteAxisInput_t1550365229 * ___U3CyAxisU3Ek__BackingField_6;
	// RemoteTouchInput RemoteTouchPadInput::<up>k__BackingField
	RemoteTouchInput_t3055303244 * ___U3CupU3Ek__BackingField_7;
	// RemoteTouchInput RemoteTouchPadInput::<down>k__BackingField
	RemoteTouchInput_t3055303244 * ___U3CdownU3Ek__BackingField_8;
	// RemoteTouchInput RemoteTouchPadInput::<left>k__BackingField
	RemoteTouchInput_t3055303244 * ___U3CleftU3Ek__BackingField_9;
	// RemoteTouchInput RemoteTouchPadInput::<right>k__BackingField
	RemoteTouchInput_t3055303244 * ___U3CrightU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_OnValueChanged_2() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___OnValueChanged_2)); }
	inline RemoteTouchPadInputEventHandler_t3280214852 * get_OnValueChanged_2() const { return ___OnValueChanged_2; }
	inline RemoteTouchPadInputEventHandler_t3280214852 ** get_address_of_OnValueChanged_2() { return &___OnValueChanged_2; }
	inline void set_OnValueChanged_2(RemoteTouchPadInputEventHandler_t3280214852 * value)
	{
		___OnValueChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CbuttonU3Ek__BackingField_3)); }
	inline RemoteButtonInput_t3750858021 * get_U3CbuttonU3Ek__BackingField_3() const { return ___U3CbuttonU3Ek__BackingField_3; }
	inline RemoteButtonInput_t3750858021 ** get_address_of_U3CbuttonU3Ek__BackingField_3() { return &___U3CbuttonU3Ek__BackingField_3; }
	inline void set_U3CbuttonU3Ek__BackingField_3(RemoteButtonInput_t3750858021 * value)
	{
		___U3CbuttonU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuttonU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtouchActiveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CtouchActiveU3Ek__BackingField_4)); }
	inline RemoteButtonInput_t3750858021 * get_U3CtouchActiveU3Ek__BackingField_4() const { return ___U3CtouchActiveU3Ek__BackingField_4; }
	inline RemoteButtonInput_t3750858021 ** get_address_of_U3CtouchActiveU3Ek__BackingField_4() { return &___U3CtouchActiveU3Ek__BackingField_4; }
	inline void set_U3CtouchActiveU3Ek__BackingField_4(RemoteButtonInput_t3750858021 * value)
	{
		___U3CtouchActiveU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchActiveU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CxAxisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CxAxisU3Ek__BackingField_5)); }
	inline RemoteAxisInput_t1550365229 * get_U3CxAxisU3Ek__BackingField_5() const { return ___U3CxAxisU3Ek__BackingField_5; }
	inline RemoteAxisInput_t1550365229 ** get_address_of_U3CxAxisU3Ek__BackingField_5() { return &___U3CxAxisU3Ek__BackingField_5; }
	inline void set_U3CxAxisU3Ek__BackingField_5(RemoteAxisInput_t1550365229 * value)
	{
		___U3CxAxisU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxAxisU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CyAxisU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CyAxisU3Ek__BackingField_6)); }
	inline RemoteAxisInput_t1550365229 * get_U3CyAxisU3Ek__BackingField_6() const { return ___U3CyAxisU3Ek__BackingField_6; }
	inline RemoteAxisInput_t1550365229 ** get_address_of_U3CyAxisU3Ek__BackingField_6() { return &___U3CyAxisU3Ek__BackingField_6; }
	inline void set_U3CyAxisU3Ek__BackingField_6(RemoteAxisInput_t1550365229 * value)
	{
		___U3CyAxisU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CyAxisU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CupU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CupU3Ek__BackingField_7)); }
	inline RemoteTouchInput_t3055303244 * get_U3CupU3Ek__BackingField_7() const { return ___U3CupU3Ek__BackingField_7; }
	inline RemoteTouchInput_t3055303244 ** get_address_of_U3CupU3Ek__BackingField_7() { return &___U3CupU3Ek__BackingField_7; }
	inline void set_U3CupU3Ek__BackingField_7(RemoteTouchInput_t3055303244 * value)
	{
		___U3CupU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdownU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CdownU3Ek__BackingField_8)); }
	inline RemoteTouchInput_t3055303244 * get_U3CdownU3Ek__BackingField_8() const { return ___U3CdownU3Ek__BackingField_8; }
	inline RemoteTouchInput_t3055303244 ** get_address_of_U3CdownU3Ek__BackingField_8() { return &___U3CdownU3Ek__BackingField_8; }
	inline void set_U3CdownU3Ek__BackingField_8(RemoteTouchInput_t3055303244 * value)
	{
		___U3CdownU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CleftU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CleftU3Ek__BackingField_9)); }
	inline RemoteTouchInput_t3055303244 * get_U3CleftU3Ek__BackingField_9() const { return ___U3CleftU3Ek__BackingField_9; }
	inline RemoteTouchInput_t3055303244 ** get_address_of_U3CleftU3Ek__BackingField_9() { return &___U3CleftU3Ek__BackingField_9; }
	inline void set_U3CleftU3Ek__BackingField_9(RemoteTouchInput_t3055303244 * value)
	{
		___U3CleftU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CleftU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CrightU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t3817316988, ___U3CrightU3Ek__BackingField_10)); }
	inline RemoteTouchInput_t3055303244 * get_U3CrightU3Ek__BackingField_10() const { return ___U3CrightU3Ek__BackingField_10; }
	inline RemoteTouchInput_t3055303244 ** get_address_of_U3CrightU3Ek__BackingField_10() { return &___U3CrightU3Ek__BackingField_10; }
	inline void set_U3CrightU3Ek__BackingField_10(RemoteTouchInput_t3055303244 * value)
	{
		___U3CrightU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrightU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHPADINPUT_T3817316988_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VIRTUALREMOTE_T2507984974_H
#define VIRTUALREMOTE_T2507984974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VirtualRemote
struct  VirtualRemote_t2507984974  : public RemoteBase_t3398252275
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALREMOTE_T2507984974_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef REMOTEMOTIONINPUT_T177155726_H
#define REMOTEMOTIONINPUT_T177155726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionInput
struct  RemoteMotionInput_t177155726  : public RemoteTouchInput_t3055303244
{
public:
	// RemoteMotionInput/RemoteMotionInputEventHandler RemoteMotionInput::OnValueChanged
	RemoteMotionInputEventHandler_t3928406934 * ___OnValueChanged_2;
	// RemoteOrientationInput RemoteMotionInput::<orientation>k__BackingField
	RemoteOrientationInput_t714724027 * ___U3CorientationU3Ek__BackingField_3;
	// RemoteMotionSensorInput RemoteMotionInput::<acceleration>k__BackingField
	RemoteMotionSensorInput_t407189685 * ___U3CaccelerationU3Ek__BackingField_4;
	// RemoteMotionSensorInput RemoteMotionInput::<rotationRate>k__BackingField
	RemoteMotionSensorInput_t407189685 * ___U3CrotationRateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_OnValueChanged_2() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t177155726, ___OnValueChanged_2)); }
	inline RemoteMotionInputEventHandler_t3928406934 * get_OnValueChanged_2() const { return ___OnValueChanged_2; }
	inline RemoteMotionInputEventHandler_t3928406934 ** get_address_of_OnValueChanged_2() { return &___OnValueChanged_2; }
	inline void set_OnValueChanged_2(RemoteMotionInputEventHandler_t3928406934 * value)
	{
		___OnValueChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CorientationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t177155726, ___U3CorientationU3Ek__BackingField_3)); }
	inline RemoteOrientationInput_t714724027 * get_U3CorientationU3Ek__BackingField_3() const { return ___U3CorientationU3Ek__BackingField_3; }
	inline RemoteOrientationInput_t714724027 ** get_address_of_U3CorientationU3Ek__BackingField_3() { return &___U3CorientationU3Ek__BackingField_3; }
	inline void set_U3CorientationU3Ek__BackingField_3(RemoteOrientationInput_t714724027 * value)
	{
		___U3CorientationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CorientationU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CaccelerationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t177155726, ___U3CaccelerationU3Ek__BackingField_4)); }
	inline RemoteMotionSensorInput_t407189685 * get_U3CaccelerationU3Ek__BackingField_4() const { return ___U3CaccelerationU3Ek__BackingField_4; }
	inline RemoteMotionSensorInput_t407189685 ** get_address_of_U3CaccelerationU3Ek__BackingField_4() { return &___U3CaccelerationU3Ek__BackingField_4; }
	inline void set_U3CaccelerationU3Ek__BackingField_4(RemoteMotionSensorInput_t407189685 * value)
	{
		___U3CaccelerationU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaccelerationU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrotationRateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t177155726, ___U3CrotationRateU3Ek__BackingField_5)); }
	inline RemoteMotionSensorInput_t407189685 * get_U3CrotationRateU3Ek__BackingField_5() const { return ___U3CrotationRateU3Ek__BackingField_5; }
	inline RemoteMotionSensorInput_t407189685 ** get_address_of_U3CrotationRateU3Ek__BackingField_5() { return &___U3CrotationRateU3Ek__BackingField_5; }
	inline void set_U3CrotationRateU3Ek__BackingField_5(RemoteMotionSensorInput_t407189685 * value)
	{
		___U3CrotationRateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrotationRateU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONINPUT_T177155726_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SERIALIZABLEQUATERNION_T1284387000_H
#define SERIALIZABLEQUATERNION_T1284387000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableQuaternion
struct  SerializableQuaternion_t1284387000 
{
public:
	// System.Single Utils.SerializableQuaternion::x
	float ___x_0;
	// System.Single Utils.SerializableQuaternion::y
	float ___y_1;
	// System.Single Utils.SerializableQuaternion::z
	float ___z_2;
	// System.Single Utils.SerializableQuaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t1284387000, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t1284387000, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t1284387000, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t1284387000, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEQUATERNION_T1284387000_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef SERIALIZABLEBTREMOTETOUCHPAD_T415748006_H
#define SERIALIZABLEBTREMOTETOUCHPAD_T415748006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemoteTouchPad
struct  serializableBTRemoteTouchPad_t415748006  : public RuntimeObject
{
public:
	// System.Boolean Utils.serializableBTRemoteTouchPad::touchActive
	bool ___touchActive_0;
	// System.Boolean Utils.serializableBTRemoteTouchPad::touchButton
	bool ___touchButton_1;
	// Utils.SerializableVector2 Utils.serializableBTRemoteTouchPad::touchPos
	SerializableVector2_t1862640090  ___touchPos_2;
	// System.Boolean Utils.serializableBTRemoteTouchPad::upButton
	bool ___upButton_3;
	// System.Boolean Utils.serializableBTRemoteTouchPad::downButton
	bool ___downButton_4;
	// System.Boolean Utils.serializableBTRemoteTouchPad::leftButton
	bool ___leftButton_5;
	// System.Boolean Utils.serializableBTRemoteTouchPad::rightButton
	bool ___rightButton_6;

public:
	inline static int32_t get_offset_of_touchActive_0() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___touchActive_0)); }
	inline bool get_touchActive_0() const { return ___touchActive_0; }
	inline bool* get_address_of_touchActive_0() { return &___touchActive_0; }
	inline void set_touchActive_0(bool value)
	{
		___touchActive_0 = value;
	}

	inline static int32_t get_offset_of_touchButton_1() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___touchButton_1)); }
	inline bool get_touchButton_1() const { return ___touchButton_1; }
	inline bool* get_address_of_touchButton_1() { return &___touchButton_1; }
	inline void set_touchButton_1(bool value)
	{
		___touchButton_1 = value;
	}

	inline static int32_t get_offset_of_touchPos_2() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___touchPos_2)); }
	inline SerializableVector2_t1862640090  get_touchPos_2() const { return ___touchPos_2; }
	inline SerializableVector2_t1862640090 * get_address_of_touchPos_2() { return &___touchPos_2; }
	inline void set_touchPos_2(SerializableVector2_t1862640090  value)
	{
		___touchPos_2 = value;
	}

	inline static int32_t get_offset_of_upButton_3() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___upButton_3)); }
	inline bool get_upButton_3() const { return ___upButton_3; }
	inline bool* get_address_of_upButton_3() { return &___upButton_3; }
	inline void set_upButton_3(bool value)
	{
		___upButton_3 = value;
	}

	inline static int32_t get_offset_of_downButton_4() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___downButton_4)); }
	inline bool get_downButton_4() const { return ___downButton_4; }
	inline bool* get_address_of_downButton_4() { return &___downButton_4; }
	inline void set_downButton_4(bool value)
	{
		___downButton_4 = value;
	}

	inline static int32_t get_offset_of_leftButton_5() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___leftButton_5)); }
	inline bool get_leftButton_5() const { return ___leftButton_5; }
	inline bool* get_address_of_leftButton_5() { return &___leftButton_5; }
	inline void set_leftButton_5(bool value)
	{
		___leftButton_5 = value;
	}

	inline static int32_t get_offset_of_rightButton_6() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t415748006, ___rightButton_6)); }
	inline bool get_rightButton_6() const { return ___rightButton_6; }
	inline bool* get_address_of_rightButton_6() { return &___rightButton_6; }
	inline void set_rightButton_6(bool value)
	{
		___rightButton_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTETOUCHPAD_T415748006_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef REMOTE_T1018512123_H
#define REMOTE_T1018512123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Remote
struct  Remote_t1018512123  : public RemoteBase_t3398252275
{
public:
	// Remote/RemoteRefreshedEventHandler Remote::OnRefresh
	RemoteRefreshedEventHandler_t1529686812 * ___OnRefresh_5;
	// System.Guid Remote::identifier
	Guid_t  ___identifier_6;
	// System.String Remote::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_7;
	// System.String Remote::<productName>k__BackingField
	String_t* ___U3CproductNameU3Ek__BackingField_8;
	// System.String Remote::<serialNumber>k__BackingField
	String_t* ___U3CserialNumberU3Ek__BackingField_9;
	// System.String Remote::<hardwareIdentifier>k__BackingField
	String_t* ___U3ChardwareIdentifierU3Ek__BackingField_10;
	// System.String Remote::<firmwareVersion>k__BackingField
	String_t* ___U3CfirmwareVersionU3Ek__BackingField_11;
	// System.Nullable`1<System.Single> Remote::<batteryPercentage>k__BackingField
	Nullable_1_t3119828856  ___U3CbatteryPercentageU3Ek__BackingField_12;
	// System.Nullable`1<System.Int32> Remote::<rssi>k__BackingField
	Nullable_1_t378540539  ___U3CrssiU3Ek__BackingField_13;
	// System.Boolean Remote::<isConnected>k__BackingField
	bool ___U3CisConnectedU3Ek__BackingField_14;
	// System.Boolean Remote::<isPreferred>k__BackingField
	bool ___U3CisPreferredU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_OnRefresh_5() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___OnRefresh_5)); }
	inline RemoteRefreshedEventHandler_t1529686812 * get_OnRefresh_5() const { return ___OnRefresh_5; }
	inline RemoteRefreshedEventHandler_t1529686812 ** get_address_of_OnRefresh_5() { return &___OnRefresh_5; }
	inline void set_OnRefresh_5(RemoteRefreshedEventHandler_t1529686812 * value)
	{
		___OnRefresh_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnRefresh_5), value);
	}

	inline static int32_t get_offset_of_identifier_6() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___identifier_6)); }
	inline Guid_t  get_identifier_6() const { return ___identifier_6; }
	inline Guid_t * get_address_of_identifier_6() { return &___identifier_6; }
	inline void set_identifier_6(Guid_t  value)
	{
		___identifier_6 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CnameU3Ek__BackingField_7)); }
	inline String_t* get_U3CnameU3Ek__BackingField_7() const { return ___U3CnameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_7() { return &___U3CnameU3Ek__BackingField_7; }
	inline void set_U3CnameU3Ek__BackingField_7(String_t* value)
	{
		___U3CnameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CproductNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CproductNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CproductNameU3Ek__BackingField_8() const { return ___U3CproductNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CproductNameU3Ek__BackingField_8() { return &___U3CproductNameU3Ek__BackingField_8; }
	inline void set_U3CproductNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CproductNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductNameU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CserialNumberU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CserialNumberU3Ek__BackingField_9)); }
	inline String_t* get_U3CserialNumberU3Ek__BackingField_9() const { return ___U3CserialNumberU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CserialNumberU3Ek__BackingField_9() { return &___U3CserialNumberU3Ek__BackingField_9; }
	inline void set_U3CserialNumberU3Ek__BackingField_9(String_t* value)
	{
		___U3CserialNumberU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CserialNumberU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3ChardwareIdentifierU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3ChardwareIdentifierU3Ek__BackingField_10)); }
	inline String_t* get_U3ChardwareIdentifierU3Ek__BackingField_10() const { return ___U3ChardwareIdentifierU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3ChardwareIdentifierU3Ek__BackingField_10() { return &___U3ChardwareIdentifierU3Ek__BackingField_10; }
	inline void set_U3ChardwareIdentifierU3Ek__BackingField_10(String_t* value)
	{
		___U3ChardwareIdentifierU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChardwareIdentifierU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CfirmwareVersionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CfirmwareVersionU3Ek__BackingField_11)); }
	inline String_t* get_U3CfirmwareVersionU3Ek__BackingField_11() const { return ___U3CfirmwareVersionU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CfirmwareVersionU3Ek__BackingField_11() { return &___U3CfirmwareVersionU3Ek__BackingField_11; }
	inline void set_U3CfirmwareVersionU3Ek__BackingField_11(String_t* value)
	{
		___U3CfirmwareVersionU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfirmwareVersionU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CbatteryPercentageU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CbatteryPercentageU3Ek__BackingField_12)); }
	inline Nullable_1_t3119828856  get_U3CbatteryPercentageU3Ek__BackingField_12() const { return ___U3CbatteryPercentageU3Ek__BackingField_12; }
	inline Nullable_1_t3119828856 * get_address_of_U3CbatteryPercentageU3Ek__BackingField_12() { return &___U3CbatteryPercentageU3Ek__BackingField_12; }
	inline void set_U3CbatteryPercentageU3Ek__BackingField_12(Nullable_1_t3119828856  value)
	{
		___U3CbatteryPercentageU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CrssiU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CrssiU3Ek__BackingField_13)); }
	inline Nullable_1_t378540539  get_U3CrssiU3Ek__BackingField_13() const { return ___U3CrssiU3Ek__BackingField_13; }
	inline Nullable_1_t378540539 * get_address_of_U3CrssiU3Ek__BackingField_13() { return &___U3CrssiU3Ek__BackingField_13; }
	inline void set_U3CrssiU3Ek__BackingField_13(Nullable_1_t378540539  value)
	{
		___U3CrssiU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CisConnectedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CisConnectedU3Ek__BackingField_14)); }
	inline bool get_U3CisConnectedU3Ek__BackingField_14() const { return ___U3CisConnectedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisConnectedU3Ek__BackingField_14() { return &___U3CisConnectedU3Ek__BackingField_14; }
	inline void set_U3CisConnectedU3Ek__BackingField_14(bool value)
	{
		___U3CisConnectedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPreferredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Remote_t1018512123, ___U3CisPreferredU3Ek__BackingField_15)); }
	inline bool get_U3CisPreferredU3Ek__BackingField_15() const { return ___U3CisPreferredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPreferredU3Ek__BackingField_15() { return &___U3CisPreferredU3Ek__BackingField_15; }
	inline void set_U3CisPreferredU3Ek__BackingField_15(bool value)
	{
		___U3CisPreferredU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTE_T1018512123_H
#ifndef BLOCKINGOBJECTS_T612090948_H
#define BLOCKINGOBJECTS_T612090948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GraphicRaycaster/BlockingObjects
struct  BlockingObjects_t612090948 
{
public:
	// System.Int32 UnityEngine.UI.GraphicRaycaster/BlockingObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockingObjects_t612090948, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_T612090948_H
#ifndef SERIALIZABLEBTREMOTE_T2102349873_H
#define SERIALIZABLEBTREMOTE_T2102349873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemote
struct  serializableBTRemote_t2102349873  : public RuntimeObject
{
public:
	// Utils.SerializableVector3 Utils.serializableBTRemote::orientation
	SerializableVector3_t1862640089  ___orientation_0;
	// Utils.SerializableVector3 Utils.serializableBTRemote::rotationRate
	SerializableVector3_t1862640089  ___rotationRate_1;
	// Utils.SerializableVector3 Utils.serializableBTRemote::acceleration
	SerializableVector3_t1862640089  ___acceleration_2;

public:
	inline static int32_t get_offset_of_orientation_0() { return static_cast<int32_t>(offsetof(serializableBTRemote_t2102349873, ___orientation_0)); }
	inline SerializableVector3_t1862640089  get_orientation_0() const { return ___orientation_0; }
	inline SerializableVector3_t1862640089 * get_address_of_orientation_0() { return &___orientation_0; }
	inline void set_orientation_0(SerializableVector3_t1862640089  value)
	{
		___orientation_0 = value;
	}

	inline static int32_t get_offset_of_rotationRate_1() { return static_cast<int32_t>(offsetof(serializableBTRemote_t2102349873, ___rotationRate_1)); }
	inline SerializableVector3_t1862640089  get_rotationRate_1() const { return ___rotationRate_1; }
	inline SerializableVector3_t1862640089 * get_address_of_rotationRate_1() { return &___rotationRate_1; }
	inline void set_rotationRate_1(SerializableVector3_t1862640089  value)
	{
		___rotationRate_1 = value;
	}

	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(serializableBTRemote_t2102349873, ___acceleration_2)); }
	inline SerializableVector3_t1862640089  get_acceleration_2() const { return ___acceleration_2; }
	inline SerializableVector3_t1862640089 * get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(SerializableVector3_t1862640089  value)
	{
		___acceleration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTE_T2102349873_H
#ifndef BLUETOOTHSTATE_T589450068_H
#define BLUETOOTHSTATE_T589450068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothState
struct  BluetoothState_t589450068 
{
public:
	// System.Int32 BluetoothState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BluetoothState_t589450068, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUETOOTHSTATE_T589450068_H
#ifndef SERIALIZABLEGYROSCOPE_T1801835429_H
#define SERIALIZABLEGYROSCOPE_T1801835429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableGyroscope
struct  serializableGyroscope_t1801835429  : public RuntimeObject
{
public:
	// Utils.SerializableQuaternion Utils.serializableGyroscope::attitude
	SerializableQuaternion_t1284387000  ___attitude_0;
	// Utils.SerializableVector3 Utils.serializableGyroscope::userAcceleration
	SerializableVector3_t1862640089  ___userAcceleration_1;

public:
	inline static int32_t get_offset_of_attitude_0() { return static_cast<int32_t>(offsetof(serializableGyroscope_t1801835429, ___attitude_0)); }
	inline SerializableQuaternion_t1284387000  get_attitude_0() const { return ___attitude_0; }
	inline SerializableQuaternion_t1284387000 * get_address_of_attitude_0() { return &___attitude_0; }
	inline void set_attitude_0(SerializableQuaternion_t1284387000  value)
	{
		___attitude_0 = value;
	}

	inline static int32_t get_offset_of_userAcceleration_1() { return static_cast<int32_t>(offsetof(serializableGyroscope_t1801835429, ___userAcceleration_1)); }
	inline SerializableVector3_t1862640089  get_userAcceleration_1() const { return ___userAcceleration_1; }
	inline SerializableVector3_t1862640089 * get_address_of_userAcceleration_1() { return &___userAcceleration_1; }
	inline void set_userAcceleration_1(SerializableVector3_t1862640089  value)
	{
		___userAcceleration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEGYROSCOPE_T1801835429_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SERIALIZABLETRANSFORM_T510777870_H
#define SERIALIZABLETRANSFORM_T510777870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableTransform
struct  serializableTransform_t510777870  : public RuntimeObject
{
public:
	// Utils.SerializableVector3 Utils.serializableTransform::position
	SerializableVector3_t1862640089  ___position_0;
	// Utils.SerializableQuaternion Utils.serializableTransform::rotation
	SerializableQuaternion_t1284387000  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(serializableTransform_t510777870, ___position_0)); }
	inline SerializableVector3_t1862640089  get_position_0() const { return ___position_0; }
	inline SerializableVector3_t1862640089 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(SerializableVector3_t1862640089  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(serializableTransform_t510777870, ___rotation_1)); }
	inline SerializableQuaternion_t1284387000  get_rotation_1() const { return ___rotation_1; }
	inline SerializableQuaternion_t1284387000 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(SerializableQuaternion_t1284387000  value)
	{
		___rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLETRANSFORM_T510777870_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_T3245497382_H
#define SERIALIZABLEFROMEDITORMESSAGE_T3245497382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_t3245497382  : public RuntimeObject
{
public:
	// System.Guid Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// System.Byte[] Utils.serializableFromEditorMessage::bytes
	ByteU5BU5D_t4116647657* ___bytes_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t3245497382, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t3245497382, ___bytes_1)); }
	inline ByteU5BU5D_t4116647657* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t4116647657* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_T3245497382_H
#ifndef CLICKCHOICES_T1305082988_H
#define CLICKCHOICES_T1305082988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/ClickChoices
struct  ClickChoices_t1305082988 
{
public:
	// System.Int32 MiraController/ClickChoices::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClickChoices_t1305082988, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKCHOICES_T1305082988_H
#ifndef U3CRECENTERANIMU3EC__ITERATOR3_T1200937000_H
#define U3CRECENTERANIMU3EC__ITERATOR3_T1200937000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<RecenterAnim>c__Iterator3
struct  U3CRecenterAnimU3Ec__Iterator3_t1200937000  : public RuntimeObject
{
public:
	// System.Single MiraController/<RecenterAnim>c__Iterator3::<animTime>__0
	float ___U3CanimTimeU3E__0_0;
	// UnityEngine.Transform MiraController/<RecenterAnim>c__Iterator3::<reticle>__0
	Transform_t3600365921 * ___U3CreticleU3E__0_1;
	// UnityEngine.Material MiraController/<RecenterAnim>c__Iterator3::<reticleVis>__0
	Material_t340375123 * ___U3CreticleVisU3E__0_2;
	// UnityEngine.Color MiraController/<RecenterAnim>c__Iterator3::<originalColor>__0
	Color_t2555686324  ___U3CoriginalColorU3E__0_3;
	// UnityEngine.Color MiraController/<RecenterAnim>c__Iterator3::<fadeColor>__0
	Color_t2555686324  ___U3CfadeColorU3E__0_4;
	// System.Single MiraController/<RecenterAnim>c__Iterator3::<timer>__0
	float ___U3CtimerU3E__0_5;
	// System.Object MiraController/<RecenterAnim>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MiraController/<RecenterAnim>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 MiraController/<RecenterAnim>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CanimTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U3CanimTimeU3E__0_0)); }
	inline float get_U3CanimTimeU3E__0_0() const { return ___U3CanimTimeU3E__0_0; }
	inline float* get_address_of_U3CanimTimeU3E__0_0() { return &___U3CanimTimeU3E__0_0; }
	inline void set_U3CanimTimeU3E__0_0(float value)
	{
		___U3CanimTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CreticleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U3CreticleU3E__0_1)); }
	inline Transform_t3600365921 * get_U3CreticleU3E__0_1() const { return ___U3CreticleU3E__0_1; }
	inline Transform_t3600365921 ** get_address_of_U3CreticleU3E__0_1() { return &___U3CreticleU3E__0_1; }
	inline void set_U3CreticleU3E__0_1(Transform_t3600365921 * value)
	{
		___U3CreticleU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreticleU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CreticleVisU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U3CreticleVisU3E__0_2)); }
	inline Material_t340375123 * get_U3CreticleVisU3E__0_2() const { return ___U3CreticleVisU3E__0_2; }
	inline Material_t340375123 ** get_address_of_U3CreticleVisU3E__0_2() { return &___U3CreticleVisU3E__0_2; }
	inline void set_U3CreticleVisU3E__0_2(Material_t340375123 * value)
	{
		___U3CreticleVisU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreticleVisU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CoriginalColorU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U3CoriginalColorU3E__0_3)); }
	inline Color_t2555686324  get_U3CoriginalColorU3E__0_3() const { return ___U3CoriginalColorU3E__0_3; }
	inline Color_t2555686324 * get_address_of_U3CoriginalColorU3E__0_3() { return &___U3CoriginalColorU3E__0_3; }
	inline void set_U3CoriginalColorU3E__0_3(Color_t2555686324  value)
	{
		___U3CoriginalColorU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CfadeColorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U3CfadeColorU3E__0_4)); }
	inline Color_t2555686324  get_U3CfadeColorU3E__0_4() const { return ___U3CfadeColorU3E__0_4; }
	inline Color_t2555686324 * get_address_of_U3CfadeColorU3E__0_4() { return &___U3CfadeColorU3E__0_4; }
	inline void set_U3CfadeColorU3E__0_4(Color_t2555686324  value)
	{
		___U3CfadeColorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U3CtimerU3E__0_5)); }
	inline float get_U3CtimerU3E__0_5() const { return ___U3CtimerU3E__0_5; }
	inline float* get_address_of_U3CtimerU3E__0_5() { return &___U3CtimerU3E__0_5; }
	inline void set_U3CtimerU3E__0_5(float value)
	{
		___U3CtimerU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1200937000, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECENTERANIMU3EC__ITERATOR3_T1200937000_H
#ifndef HANDEDNESS_T3015040191_H
#define HANDEDNESS_T3015040191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/Handedness
struct  Handedness_t3015040191 
{
public:
	// System.Int32 MiraController/Handedness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Handedness_t3015040191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDEDNESS_T3015040191_H
#ifndef RAYCASTSTYLE_T3505768865_H
#define RAYCASTSTYLE_T3505768865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster/RaycastStyle
struct  RaycastStyle_t3505768865 
{
public:
	// System.Int32 MiraBaseRaycaster/RaycastStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RaycastStyle_t3505768865, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTSTYLE_T3505768865_H
#ifndef CONTROLLERTYPE_T929894547_H
#define CONTROLLERTYPE_T929894547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/ControllerType
struct  ControllerType_t929894547 
{
public:
	// System.Int32 MiraController/ControllerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControllerType_t929894547, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T929894547_H
#ifndef EYE_T1833640780_H
#define EYE_T1833640780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraPostRender/Eye
struct  Eye_t1833640780 
{
public:
	// System.Int32 Mira.MiraPostRender/Eye::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Eye_t1833640780, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYE_T1833640780_H
#ifndef CAMERANAMES_T2553488640_H
#define CAMERANAMES_T2553488640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraViewer/CameraNames
struct  CameraNames_t2553488640 
{
public:
	// System.Int32 Mira.MiraViewer/CameraNames::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraNames_t2553488640, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERANAMES_T2553488640_H
#ifndef U3CSWITCHTOCAMERAGYROU3EC__ITERATOR2_T2238231814_H
#define U3CSWITCHTOCAMERAGYROU3EC__ITERATOR2_T2238231814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2
struct  U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::<offsetPosition>__0
	Vector3_t3722313464  ___U3CoffsetPositionU3E__0_0;
	// Mira.RotationalTrackingManager Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$this
	RotationalTrackingManager_t1723746904 * ___U24this_1;
	// System.Object Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoffsetPositionU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814, ___U3CoffsetPositionU3E__0_0)); }
	inline Vector3_t3722313464  get_U3CoffsetPositionU3E__0_0() const { return ___U3CoffsetPositionU3E__0_0; }
	inline Vector3_t3722313464 * get_address_of_U3CoffsetPositionU3E__0_0() { return &___U3CoffsetPositionU3E__0_0; }
	inline void set_U3CoffsetPositionU3E__0_0(Vector3_t3722313464  value)
	{
		___U3CoffsetPositionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814, ___U24this_1)); }
	inline RotationalTrackingManager_t1723746904 * get_U24this_1() const { return ___U24this_1; }
	inline RotationalTrackingManager_t1723746904 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RotationalTrackingManager_t1723746904 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSWITCHTOCAMERAGYROU3EC__ITERATOR2_T2238231814_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef INPUTMODE_T2966778961_H
#define INPUTMODE_T2966778961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MiraInputModule/InputMode
struct  InputMode_t2966778961 
{
public:
	// System.Int32 UnityEngine.EventSystems.MiraInputModule/InputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputMode_t2966778961, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMODE_T2966778961_H
#ifndef U3CREMOTEMANAGERBLUETOOTHSTATEU3EC__ANONSTOREY0_T249708700_H
#define U3CREMOTEMANAGERBLUETOOTHSTATEU3EC__ANONSTOREY0_T249708700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerBluetoothState>c__AnonStorey0
struct  U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t249708700  : public RuntimeObject
{
public:
	// BluetoothState NativeBridge/<RemoteManagerBluetoothState>c__AnonStorey0::bluetoothState
	int32_t ___bluetoothState_0;

public:
	inline static int32_t get_offset_of_bluetoothState_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t249708700, ___bluetoothState_0)); }
	inline int32_t get_bluetoothState_0() const { return ___bluetoothState_0; }
	inline int32_t* get_address_of_bluetoothState_0() { return &___bluetoothState_0; }
	inline void set_bluetoothState_0(int32_t value)
	{
		___bluetoothState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERBLUETOOTHSTATEU3EC__ANONSTOREY0_T249708700_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef REMOTEMOTIONINPUTEVENTHANDLER_T3928406934_H
#define REMOTEMOTIONINPUTEVENTHANDLER_T3928406934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionInput/RemoteMotionInputEventHandler
struct  RemoteMotionInputEventHandler_t3928406934  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONINPUTEVENTHANDLER_T3928406934_H
#ifndef REMOTEDISCONNECTEDEVENTHANDLER_T2506463716_H
#define REMOTEDISCONNECTEDEVENTHANDLER_T2506463716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/RemoteDisconnectedEventHandler
struct  RemoteDisconnectedEventHandler_t2506463716  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEDISCONNECTEDEVENTHANDLER_T2506463716_H
#ifndef REMOTECONNECTEDEVENTHANDLER_T306622352_H
#define REMOTECONNECTEDEVENTHANDLER_T306622352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/RemoteConnectedEventHandler
struct  RemoteConnectedEventHandler_t306622352  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTECONNECTEDEVENTHANDLER_T306622352_H
#ifndef REMOTEMOTIONSENSORINPUTEVENTHANDLER_T2314235825_H
#define REMOTEMOTIONSENSORINPUTEVENTHANDLER_T2314235825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler
struct  RemoteMotionSensorInputEventHandler_t2314235825  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONSENSORINPUTEVENTHANDLER_T2314235825_H
#ifndef REMOTETOUCHPADINPUTEVENTHANDLER_T3280214852_H
#define REMOTETOUCHPADINPUTEVENTHANDLER_T3280214852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchPadInput/RemoteTouchPadInputEventHandler
struct  RemoteTouchPadInputEventHandler_t3280214852  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHPADINPUTEVENTHANDLER_T3280214852_H
#ifndef REMOTETOUCHINPUTEVENTHANDLER_T3839927206_H
#define REMOTETOUCHINPUTEVENTHANDLER_T3839927206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchInput/RemoteTouchInputEventHandler
struct  RemoteTouchInputEventHandler_t3839927206  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHINPUTEVENTHANDLER_T3839927206_H
#ifndef REMOTEORIENTATIONINPUTEVENTHANDLER_T1727624703_H
#define REMOTEORIENTATIONINPUTEVENTHANDLER_T1727624703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteOrientationInput/RemoteOrientationInputEventHandler
struct  RemoteOrientationInputEventHandler_t1727624703  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEORIENTATIONINPUTEVENTHANDLER_T1727624703_H
#ifndef REMOTEBUTTONINPUTEVENTHANDLER_T2005612063_H
#define REMOTEBUTTONINPUTEVENTHANDLER_T2005612063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteButtonInput/RemoteButtonInputEventHandler
struct  RemoteButtonInputEventHandler_t2005612063  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBUTTONINPUTEVENTHANDLER_T2005612063_H
#ifndef REMOTEAXISINPUTEVENTHANDLER_T4129402086_H
#define REMOTEAXISINPUTEVENTHANDLER_T4129402086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteAxisInput/RemoteAxisInputEventHandler
struct  RemoteAxisInputEventHandler_t4129402086  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEAXISINPUTEVENTHANDLER_T4129402086_H
#ifndef REMOTEREFRESHEDEVENTHANDLER_T1529686812_H
#define REMOTEREFRESHEDEVENTHANDLER_T1529686812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Remote/RemoteRefreshedEventHandler
struct  RemoteRefreshedEventHandler_t1529686812  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEREFRESHEDEVENTHANDLER_T1529686812_H
#ifndef MRREMOTEMOTIONCALLBACK_T366018504_H
#define MRREMOTEMOTIONCALLBACK_T366018504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRRemoteMotionCallback
struct  MRRemoteMotionCallback_t366018504  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRREMOTEMOTIONCALLBACK_T366018504_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEBUGMIRACONTROLLER_T1049664957_H
#define DEBUGMIRACONTROLLER_T1049664957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugMiraController
struct  DebugMiraController_t1049664957  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DebugMiraController::debugOutputText
	Text_t1901882714 * ___debugOutputText_2;
	// System.String DebugMiraController::output
	String_t* ___output_3;
	// System.Boolean DebugMiraController::didPointerEnter
	bool ___didPointerEnter_4;
	// System.Boolean DebugMiraController::didPointerExit
	bool ___didPointerExit_5;
	// System.Boolean DebugMiraController::didPointerClick
	bool ___didPointerClick_6;
	// System.Boolean DebugMiraController::didPointerDown
	bool ___didPointerDown_7;
	// System.Boolean DebugMiraController::didPointerUp
	bool ___didPointerUp_8;

public:
	inline static int32_t get_offset_of_debugOutputText_2() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___debugOutputText_2)); }
	inline Text_t1901882714 * get_debugOutputText_2() const { return ___debugOutputText_2; }
	inline Text_t1901882714 ** get_address_of_debugOutputText_2() { return &___debugOutputText_2; }
	inline void set_debugOutputText_2(Text_t1901882714 * value)
	{
		___debugOutputText_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugOutputText_2), value);
	}

	inline static int32_t get_offset_of_output_3() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___output_3)); }
	inline String_t* get_output_3() const { return ___output_3; }
	inline String_t** get_address_of_output_3() { return &___output_3; }
	inline void set_output_3(String_t* value)
	{
		___output_3 = value;
		Il2CppCodeGenWriteBarrier((&___output_3), value);
	}

	inline static int32_t get_offset_of_didPointerEnter_4() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___didPointerEnter_4)); }
	inline bool get_didPointerEnter_4() const { return ___didPointerEnter_4; }
	inline bool* get_address_of_didPointerEnter_4() { return &___didPointerEnter_4; }
	inline void set_didPointerEnter_4(bool value)
	{
		___didPointerEnter_4 = value;
	}

	inline static int32_t get_offset_of_didPointerExit_5() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___didPointerExit_5)); }
	inline bool get_didPointerExit_5() const { return ___didPointerExit_5; }
	inline bool* get_address_of_didPointerExit_5() { return &___didPointerExit_5; }
	inline void set_didPointerExit_5(bool value)
	{
		___didPointerExit_5 = value;
	}

	inline static int32_t get_offset_of_didPointerClick_6() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___didPointerClick_6)); }
	inline bool get_didPointerClick_6() const { return ___didPointerClick_6; }
	inline bool* get_address_of_didPointerClick_6() { return &___didPointerClick_6; }
	inline void set_didPointerClick_6(bool value)
	{
		___didPointerClick_6 = value;
	}

	inline static int32_t get_offset_of_didPointerDown_7() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___didPointerDown_7)); }
	inline bool get_didPointerDown_7() const { return ___didPointerDown_7; }
	inline bool* get_address_of_didPointerDown_7() { return &___didPointerDown_7; }
	inline void set_didPointerDown_7(bool value)
	{
		___didPointerDown_7 = value;
	}

	inline static int32_t get_offset_of_didPointerUp_8() { return static_cast<int32_t>(offsetof(DebugMiraController_t1049664957, ___didPointerUp_8)); }
	inline bool get_didPointerUp_8() const { return ___didPointerUp_8; }
	inline bool* get_address_of_didPointerUp_8() { return &___didPointerUp_8; }
	inline void set_didPointerUp_8(bool value)
	{
		___didPointerUp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGMIRACONTROLLER_T1049664957_H
#ifndef DEVICEORIENTATIONCHANGE_T2350204370_H
#define DEVICEORIENTATIONCHANGE_T2350204370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceOrientationChange
struct  DeviceOrientationChange_t2350204370  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct DeviceOrientationChange_t2350204370_StaticFields
{
public:
	// System.Action`1<UnityEngine.Vector2> DeviceOrientationChange::OnResolutionChange
	Action_1_t2328697118 * ___OnResolutionChange_2;
	// System.Single DeviceOrientationChange::CheckDelay
	float ___CheckDelay_3;
	// UnityEngine.Vector2 DeviceOrientationChange::resolution
	Vector2_t2156229523  ___resolution_4;
	// System.Boolean DeviceOrientationChange::isAlive
	bool ___isAlive_5;

public:
	inline static int32_t get_offset_of_OnResolutionChange_2() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2350204370_StaticFields, ___OnResolutionChange_2)); }
	inline Action_1_t2328697118 * get_OnResolutionChange_2() const { return ___OnResolutionChange_2; }
	inline Action_1_t2328697118 ** get_address_of_OnResolutionChange_2() { return &___OnResolutionChange_2; }
	inline void set_OnResolutionChange_2(Action_1_t2328697118 * value)
	{
		___OnResolutionChange_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnResolutionChange_2), value);
	}

	inline static int32_t get_offset_of_CheckDelay_3() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2350204370_StaticFields, ___CheckDelay_3)); }
	inline float get_CheckDelay_3() const { return ___CheckDelay_3; }
	inline float* get_address_of_CheckDelay_3() { return &___CheckDelay_3; }
	inline void set_CheckDelay_3(float value)
	{
		___CheckDelay_3 = value;
	}

	inline static int32_t get_offset_of_resolution_4() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2350204370_StaticFields, ___resolution_4)); }
	inline Vector2_t2156229523  get_resolution_4() const { return ___resolution_4; }
	inline Vector2_t2156229523 * get_address_of_resolution_4() { return &___resolution_4; }
	inline void set_resolution_4(Vector2_t2156229523  value)
	{
		___resolution_4 = value;
	}

	inline static int32_t get_offset_of_isAlive_5() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2350204370_StaticFields, ___isAlive_5)); }
	inline bool get_isAlive_5() const { return ___isAlive_5; }
	inline bool* get_address_of_isAlive_5() { return &___isAlive_5; }
	inline void set_isAlive_5(bool value)
	{
		___isAlive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEORIENTATIONCHANGE_T2350204370_H
#ifndef MIRAIOSBRIDGE_T1203351363_H
#define MIRAIOSBRIDGE_T1203351363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraiOSBridge
struct  MiraiOSBridge_t1203351363  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAIOSBRIDGE_T1203351363_H
#ifndef MIRACONTROLLER_T2033339498_H
#define MIRACONTROLLER_T2033339498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController
struct  MiraController_t2033339498  : public MonoBehaviour_t3962482529
{
public:
	// MiraController/Handedness MiraController::handedness
	int32_t ___handedness_3;
	// MiraController/ControllerType MiraController::controllerType
	int32_t ___controllerType_4;
	// MiraController/ClickChoices MiraController::WhatButtonIsClick
	int32_t ___WhatButtonIsClick_5;
	// UnityEngine.Transform MiraController::cameraRig
	Transform_t3600365921 * ___cameraRig_7;
	// UnityEngine.Transform MiraController::rotationalTracking
	Transform_t3600365921 * ___rotationalTracking_8;
	// System.Boolean MiraController::trackPosition
	bool ___trackPosition_9;
	// System.Boolean MiraController::shouldFollowHead
	bool ___shouldFollowHead_10;
	// System.Single MiraController::sceneScaleMult
	float ___sceneScaleMult_11;
	// System.Single MiraController::armOffsetX
	float ___armOffsetX_12;
	// System.Single MiraController::armOffsetY
	float ___armOffsetY_13;
	// System.Single MiraController::armOffsetZ
	float ___armOffsetZ_14;
	// UnityEngine.Quaternion MiraController::baseController
	Quaternion_t2301928331  ___baseController_15;
	// UnityEngine.Vector3 MiraController::offsetPos
	Vector3_t3722313464  ___offsetPos_16;
	// System.Boolean MiraController::isRotationalMode
	bool ___isRotationalMode_17;
	// System.Single MiraController::angleTiltX
	float ___angleTiltX_18;
	// System.Single MiraController::angleTiltY
	float ___angleTiltY_19;
	// UnityEngine.GameObject MiraController::returnToHome
	GameObject_t1113636619 * ___returnToHome_20;
	// UnityEngine.UI.Image MiraController::radialTimer
	Image_t2670269651 * ___radialTimer_21;

public:
	inline static int32_t get_offset_of_handedness_3() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___handedness_3)); }
	inline int32_t get_handedness_3() const { return ___handedness_3; }
	inline int32_t* get_address_of_handedness_3() { return &___handedness_3; }
	inline void set_handedness_3(int32_t value)
	{
		___handedness_3 = value;
	}

	inline static int32_t get_offset_of_controllerType_4() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___controllerType_4)); }
	inline int32_t get_controllerType_4() const { return ___controllerType_4; }
	inline int32_t* get_address_of_controllerType_4() { return &___controllerType_4; }
	inline void set_controllerType_4(int32_t value)
	{
		___controllerType_4 = value;
	}

	inline static int32_t get_offset_of_WhatButtonIsClick_5() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___WhatButtonIsClick_5)); }
	inline int32_t get_WhatButtonIsClick_5() const { return ___WhatButtonIsClick_5; }
	inline int32_t* get_address_of_WhatButtonIsClick_5() { return &___WhatButtonIsClick_5; }
	inline void set_WhatButtonIsClick_5(int32_t value)
	{
		___WhatButtonIsClick_5 = value;
	}

	inline static int32_t get_offset_of_cameraRig_7() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___cameraRig_7)); }
	inline Transform_t3600365921 * get_cameraRig_7() const { return ___cameraRig_7; }
	inline Transform_t3600365921 ** get_address_of_cameraRig_7() { return &___cameraRig_7; }
	inline void set_cameraRig_7(Transform_t3600365921 * value)
	{
		___cameraRig_7 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_7), value);
	}

	inline static int32_t get_offset_of_rotationalTracking_8() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___rotationalTracking_8)); }
	inline Transform_t3600365921 * get_rotationalTracking_8() const { return ___rotationalTracking_8; }
	inline Transform_t3600365921 ** get_address_of_rotationalTracking_8() { return &___rotationalTracking_8; }
	inline void set_rotationalTracking_8(Transform_t3600365921 * value)
	{
		___rotationalTracking_8 = value;
		Il2CppCodeGenWriteBarrier((&___rotationalTracking_8), value);
	}

	inline static int32_t get_offset_of_trackPosition_9() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___trackPosition_9)); }
	inline bool get_trackPosition_9() const { return ___trackPosition_9; }
	inline bool* get_address_of_trackPosition_9() { return &___trackPosition_9; }
	inline void set_trackPosition_9(bool value)
	{
		___trackPosition_9 = value;
	}

	inline static int32_t get_offset_of_shouldFollowHead_10() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___shouldFollowHead_10)); }
	inline bool get_shouldFollowHead_10() const { return ___shouldFollowHead_10; }
	inline bool* get_address_of_shouldFollowHead_10() { return &___shouldFollowHead_10; }
	inline void set_shouldFollowHead_10(bool value)
	{
		___shouldFollowHead_10 = value;
	}

	inline static int32_t get_offset_of_sceneScaleMult_11() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___sceneScaleMult_11)); }
	inline float get_sceneScaleMult_11() const { return ___sceneScaleMult_11; }
	inline float* get_address_of_sceneScaleMult_11() { return &___sceneScaleMult_11; }
	inline void set_sceneScaleMult_11(float value)
	{
		___sceneScaleMult_11 = value;
	}

	inline static int32_t get_offset_of_armOffsetX_12() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___armOffsetX_12)); }
	inline float get_armOffsetX_12() const { return ___armOffsetX_12; }
	inline float* get_address_of_armOffsetX_12() { return &___armOffsetX_12; }
	inline void set_armOffsetX_12(float value)
	{
		___armOffsetX_12 = value;
	}

	inline static int32_t get_offset_of_armOffsetY_13() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___armOffsetY_13)); }
	inline float get_armOffsetY_13() const { return ___armOffsetY_13; }
	inline float* get_address_of_armOffsetY_13() { return &___armOffsetY_13; }
	inline void set_armOffsetY_13(float value)
	{
		___armOffsetY_13 = value;
	}

	inline static int32_t get_offset_of_armOffsetZ_14() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___armOffsetZ_14)); }
	inline float get_armOffsetZ_14() const { return ___armOffsetZ_14; }
	inline float* get_address_of_armOffsetZ_14() { return &___armOffsetZ_14; }
	inline void set_armOffsetZ_14(float value)
	{
		___armOffsetZ_14 = value;
	}

	inline static int32_t get_offset_of_baseController_15() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___baseController_15)); }
	inline Quaternion_t2301928331  get_baseController_15() const { return ___baseController_15; }
	inline Quaternion_t2301928331 * get_address_of_baseController_15() { return &___baseController_15; }
	inline void set_baseController_15(Quaternion_t2301928331  value)
	{
		___baseController_15 = value;
	}

	inline static int32_t get_offset_of_offsetPos_16() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___offsetPos_16)); }
	inline Vector3_t3722313464  get_offsetPos_16() const { return ___offsetPos_16; }
	inline Vector3_t3722313464 * get_address_of_offsetPos_16() { return &___offsetPos_16; }
	inline void set_offsetPos_16(Vector3_t3722313464  value)
	{
		___offsetPos_16 = value;
	}

	inline static int32_t get_offset_of_isRotationalMode_17() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___isRotationalMode_17)); }
	inline bool get_isRotationalMode_17() const { return ___isRotationalMode_17; }
	inline bool* get_address_of_isRotationalMode_17() { return &___isRotationalMode_17; }
	inline void set_isRotationalMode_17(bool value)
	{
		___isRotationalMode_17 = value;
	}

	inline static int32_t get_offset_of_angleTiltX_18() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___angleTiltX_18)); }
	inline float get_angleTiltX_18() const { return ___angleTiltX_18; }
	inline float* get_address_of_angleTiltX_18() { return &___angleTiltX_18; }
	inline void set_angleTiltX_18(float value)
	{
		___angleTiltX_18 = value;
	}

	inline static int32_t get_offset_of_angleTiltY_19() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___angleTiltY_19)); }
	inline float get_angleTiltY_19() const { return ___angleTiltY_19; }
	inline float* get_address_of_angleTiltY_19() { return &___angleTiltY_19; }
	inline void set_angleTiltY_19(float value)
	{
		___angleTiltY_19 = value;
	}

	inline static int32_t get_offset_of_returnToHome_20() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___returnToHome_20)); }
	inline GameObject_t1113636619 * get_returnToHome_20() const { return ___returnToHome_20; }
	inline GameObject_t1113636619 ** get_address_of_returnToHome_20() { return &___returnToHome_20; }
	inline void set_returnToHome_20(GameObject_t1113636619 * value)
	{
		___returnToHome_20 = value;
		Il2CppCodeGenWriteBarrier((&___returnToHome_20), value);
	}

	inline static int32_t get_offset_of_radialTimer_21() { return static_cast<int32_t>(offsetof(MiraController_t2033339498, ___radialTimer_21)); }
	inline Image_t2670269651 * get_radialTimer_21() const { return ___radialTimer_21; }
	inline Image_t2670269651 ** get_address_of_radialTimer_21() { return &___radialTimer_21; }
	inline void set_radialTimer_21(Image_t2670269651 * value)
	{
		___radialTimer_21 = value;
		Il2CppCodeGenWriteBarrier((&___radialTimer_21), value);
	}
};

struct MiraController_t2033339498_StaticFields
{
public:
	// MiraController MiraController::_instance
	MiraController_t2033339498 * ____instance_2;
	// MiraBTRemoteInput MiraController::_userInput
	MiraBTRemoteInput_t3160068026 * ____userInput_6;
	// System.Collections.Generic.List`1<System.Func`1<System.Boolean>> MiraController::<evaluateClickButton>k__BackingField
	List_1_t999109354 * ___U3CevaluateClickButtonU3Ek__BackingField_22;
	// System.Collections.Generic.List`1<System.Func`1<System.Boolean>> MiraController::<evaluateClickButtonPressed>k__BackingField
	List_1_t999109354 * ___U3CevaluateClickButtonPressedU3Ek__BackingField_23;
	// System.Collections.Generic.List`1<System.Func`1<System.Boolean>> MiraController::<evaluateClickButtonReleased>k__BackingField
	List_1_t999109354 * ___U3CevaluateClickButtonReleasedU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MiraController_t2033339498_StaticFields, ____instance_2)); }
	inline MiraController_t2033339498 * get__instance_2() const { return ____instance_2; }
	inline MiraController_t2033339498 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(MiraController_t2033339498 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of__userInput_6() { return static_cast<int32_t>(offsetof(MiraController_t2033339498_StaticFields, ____userInput_6)); }
	inline MiraBTRemoteInput_t3160068026 * get__userInput_6() const { return ____userInput_6; }
	inline MiraBTRemoteInput_t3160068026 ** get_address_of__userInput_6() { return &____userInput_6; }
	inline void set__userInput_6(MiraBTRemoteInput_t3160068026 * value)
	{
		____userInput_6 = value;
		Il2CppCodeGenWriteBarrier((&____userInput_6), value);
	}

	inline static int32_t get_offset_of_U3CevaluateClickButtonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(MiraController_t2033339498_StaticFields, ___U3CevaluateClickButtonU3Ek__BackingField_22)); }
	inline List_1_t999109354 * get_U3CevaluateClickButtonU3Ek__BackingField_22() const { return ___U3CevaluateClickButtonU3Ek__BackingField_22; }
	inline List_1_t999109354 ** get_address_of_U3CevaluateClickButtonU3Ek__BackingField_22() { return &___U3CevaluateClickButtonU3Ek__BackingField_22; }
	inline void set_U3CevaluateClickButtonU3Ek__BackingField_22(List_1_t999109354 * value)
	{
		___U3CevaluateClickButtonU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevaluateClickButtonU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CevaluateClickButtonPressedU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MiraController_t2033339498_StaticFields, ___U3CevaluateClickButtonPressedU3Ek__BackingField_23)); }
	inline List_1_t999109354 * get_U3CevaluateClickButtonPressedU3Ek__BackingField_23() const { return ___U3CevaluateClickButtonPressedU3Ek__BackingField_23; }
	inline List_1_t999109354 ** get_address_of_U3CevaluateClickButtonPressedU3Ek__BackingField_23() { return &___U3CevaluateClickButtonPressedU3Ek__BackingField_23; }
	inline void set_U3CevaluateClickButtonPressedU3Ek__BackingField_23(List_1_t999109354 * value)
	{
		___U3CevaluateClickButtonPressedU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevaluateClickButtonPressedU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CevaluateClickButtonReleasedU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MiraController_t2033339498_StaticFields, ___U3CevaluateClickButtonReleasedU3Ek__BackingField_24)); }
	inline List_1_t999109354 * get_U3CevaluateClickButtonReleasedU3Ek__BackingField_24() const { return ___U3CevaluateClickButtonReleasedU3Ek__BackingField_24; }
	inline List_1_t999109354 ** get_address_of_U3CevaluateClickButtonReleasedU3Ek__BackingField_24() { return &___U3CevaluateClickButtonReleasedU3Ek__BackingField_24; }
	inline void set_U3CevaluateClickButtonReleasedU3Ek__BackingField_24(List_1_t999109354 * value)
	{
		___U3CevaluateClickButtonReleasedU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevaluateClickButtonReleasedU3Ek__BackingField_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRACONTROLLER_T2033339498_H
#ifndef DISTORTIONCAMERA_T4266954139_H
#define DISTORTIONCAMERA_T4266954139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.DistortionCamera
struct  DistortionCamera_t4266954139  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.DistortionCamera::stereoFov
	float ___stereoFov_2;

public:
	inline static int32_t get_offset_of_stereoFov_2() { return static_cast<int32_t>(offsetof(DistortionCamera_t4266954139, ___stereoFov_2)); }
	inline float get_stereoFov_2() const { return ___stereoFov_2; }
	inline float* get_address_of_stereoFov_2() { return &___stereoFov_2; }
	inline void set_stereoFov_2(float value)
	{
		___stereoFov_2 = value;
	}
};

struct DistortionCamera_t4266954139_StaticFields
{
public:
	// Mira.DistortionCamera Mira.DistortionCamera::instance
	DistortionCamera_t4266954139 * ___instance_3;
	// UnityEngine.Camera Mira.DistortionCamera::m_dcLeft
	Camera_t4157153871 * ___m_dcLeft_4;
	// UnityEngine.Camera Mira.DistortionCamera::m_dcRight
	Camera_t4157153871 * ___m_dcRight_5;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(DistortionCamera_t4266954139_StaticFields, ___instance_3)); }
	inline DistortionCamera_t4266954139 * get_instance_3() const { return ___instance_3; }
	inline DistortionCamera_t4266954139 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(DistortionCamera_t4266954139 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_dcLeft_4() { return static_cast<int32_t>(offsetof(DistortionCamera_t4266954139_StaticFields, ___m_dcLeft_4)); }
	inline Camera_t4157153871 * get_m_dcLeft_4() const { return ___m_dcLeft_4; }
	inline Camera_t4157153871 ** get_address_of_m_dcLeft_4() { return &___m_dcLeft_4; }
	inline void set_m_dcLeft_4(Camera_t4157153871 * value)
	{
		___m_dcLeft_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_dcLeft_4), value);
	}

	inline static int32_t get_offset_of_m_dcRight_5() { return static_cast<int32_t>(offsetof(DistortionCamera_t4266954139_StaticFields, ___m_dcRight_5)); }
	inline Camera_t4157153871 * get_m_dcRight_5() const { return ___m_dcRight_5; }
	inline Camera_t4157153871 ** get_address_of_m_dcRight_5() { return &___m_dcRight_5; }
	inline void set_m_dcRight_5(Camera_t4157153871 * value)
	{
		___m_dcRight_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_dcRight_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONCAMERA_T4266954139_H
#ifndef REMOTESCONTROLLER_T1084025038_H
#define REMOTESCONTROLLER_T1084025038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemotesController
struct  RemotesController_t1084025038  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] RemotesController::remoteLabels
	GameObjectU5BU5D_t3328599146* ___remoteLabels_2;
	// UnityEngine.Color RemotesController::defaultColor
	Color_t2555686324  ___defaultColor_3;
	// UnityEngine.Color RemotesController::highlightColor
	Color_t2555686324  ___highlightColor_4;

public:
	inline static int32_t get_offset_of_remoteLabels_2() { return static_cast<int32_t>(offsetof(RemotesController_t1084025038, ___remoteLabels_2)); }
	inline GameObjectU5BU5D_t3328599146* get_remoteLabels_2() const { return ___remoteLabels_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_remoteLabels_2() { return &___remoteLabels_2; }
	inline void set_remoteLabels_2(GameObjectU5BU5D_t3328599146* value)
	{
		___remoteLabels_2 = value;
		Il2CppCodeGenWriteBarrier((&___remoteLabels_2), value);
	}

	inline static int32_t get_offset_of_defaultColor_3() { return static_cast<int32_t>(offsetof(RemotesController_t1084025038, ___defaultColor_3)); }
	inline Color_t2555686324  get_defaultColor_3() const { return ___defaultColor_3; }
	inline Color_t2555686324 * get_address_of_defaultColor_3() { return &___defaultColor_3; }
	inline void set_defaultColor_3(Color_t2555686324  value)
	{
		___defaultColor_3 = value;
	}

	inline static int32_t get_offset_of_highlightColor_4() { return static_cast<int32_t>(offsetof(RemotesController_t1084025038, ___highlightColor_4)); }
	inline Color_t2555686324  get_highlightColor_4() const { return ___highlightColor_4; }
	inline Color_t2555686324 * get_address_of_highlightColor_4() { return &___highlightColor_4; }
	inline void set_highlightColor_4(Color_t2555686324  value)
	{
		___highlightColor_4 = value;
	}
};

struct RemotesController_t1084025038_StaticFields
{
public:
	// System.Action`1<MiraRemoteException> RemotesController::<>f__am$cache0
	Action_1_t3895617434 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(RemotesController_t1084025038_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t3895617434 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t3895617434 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t3895617434 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESCONTROLLER_T1084025038_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TRANSFORMOVERRIDE_T3828400655_H
#define TRANSFORMOVERRIDE_T3828400655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformOverride
struct  TransformOverride_t3828400655  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMOVERRIDE_T3828400655_H
#ifndef CAMGYRO_T931366934_H
#define CAMGYRO_T931366934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.CamGyro
struct  CamGyro_t931366934  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Mira.CamGyro::isSpectator
	bool ___isSpectator_2;
	// UnityEngine.Quaternion Mira.CamGyro::camGyroOffset
	Quaternion_t2301928331  ___camGyroOffset_3;
	// System.Boolean Mira.CamGyro::camGyroActive
	bool ___camGyroActive_4;

public:
	inline static int32_t get_offset_of_isSpectator_2() { return static_cast<int32_t>(offsetof(CamGyro_t931366934, ___isSpectator_2)); }
	inline bool get_isSpectator_2() const { return ___isSpectator_2; }
	inline bool* get_address_of_isSpectator_2() { return &___isSpectator_2; }
	inline void set_isSpectator_2(bool value)
	{
		___isSpectator_2 = value;
	}

	inline static int32_t get_offset_of_camGyroOffset_3() { return static_cast<int32_t>(offsetof(CamGyro_t931366934, ___camGyroOffset_3)); }
	inline Quaternion_t2301928331  get_camGyroOffset_3() const { return ___camGyroOffset_3; }
	inline Quaternion_t2301928331 * get_address_of_camGyroOffset_3() { return &___camGyroOffset_3; }
	inline void set_camGyroOffset_3(Quaternion_t2301928331  value)
	{
		___camGyroOffset_3 = value;
	}

	inline static int32_t get_offset_of_camGyroActive_4() { return static_cast<int32_t>(offsetof(CamGyro_t931366934, ___camGyroActive_4)); }
	inline bool get_camGyroActive_4() const { return ___camGyroActive_4; }
	inline bool* get_address_of_camGyroActive_4() { return &___camGyroActive_4; }
	inline void set_camGyroActive_4(bool value)
	{
		___camGyroActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMGYRO_T931366934_H
#ifndef REMOTEMANAGER_T1941324022_H
#define REMOTEMANAGER_T1941324022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager
struct  RemoteManager_t1941324022  : public MonoBehaviour_t3962482529
{
public:
	// RemoteManager/RemoteConnectedEventHandler RemoteManager::OnRemoteConnected
	RemoteConnectedEventHandler_t306622352 * ___OnRemoteConnected_2;
	// RemoteManager/RemoteDisconnectedEventHandler RemoteManager::OnRemoteDisconnected
	RemoteDisconnectedEventHandler_t2506463716 * ___OnRemoteDisconnected_3;
	// System.Boolean RemoteManager::<isStarted>k__BackingField
	bool ___U3CisStartedU3Ek__BackingField_5;
	// System.Boolean RemoteManager::<isDiscoveringRemotes>k__BackingField
	bool ___U3CisDiscoveringRemotesU3Ek__BackingField_6;
	// Remote RemoteManager::_connectedRemote
	Remote_t1018512123 * ____connectedRemote_7;
	// System.Collections.Generic.List`1<Remote> RemoteManager::_discoveredRemotes
	List_1_t2490586865 * ____discoveredRemotes_8;

public:
	inline static int32_t get_offset_of_OnRemoteConnected_2() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022, ___OnRemoteConnected_2)); }
	inline RemoteConnectedEventHandler_t306622352 * get_OnRemoteConnected_2() const { return ___OnRemoteConnected_2; }
	inline RemoteConnectedEventHandler_t306622352 ** get_address_of_OnRemoteConnected_2() { return &___OnRemoteConnected_2; }
	inline void set_OnRemoteConnected_2(RemoteConnectedEventHandler_t306622352 * value)
	{
		___OnRemoteConnected_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteConnected_2), value);
	}

	inline static int32_t get_offset_of_OnRemoteDisconnected_3() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022, ___OnRemoteDisconnected_3)); }
	inline RemoteDisconnectedEventHandler_t2506463716 * get_OnRemoteDisconnected_3() const { return ___OnRemoteDisconnected_3; }
	inline RemoteDisconnectedEventHandler_t2506463716 ** get_address_of_OnRemoteDisconnected_3() { return &___OnRemoteDisconnected_3; }
	inline void set_OnRemoteDisconnected_3(RemoteDisconnectedEventHandler_t2506463716 * value)
	{
		___OnRemoteDisconnected_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteDisconnected_3), value);
	}

	inline static int32_t get_offset_of_U3CisStartedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022, ___U3CisStartedU3Ek__BackingField_5)); }
	inline bool get_U3CisStartedU3Ek__BackingField_5() const { return ___U3CisStartedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CisStartedU3Ek__BackingField_5() { return &___U3CisStartedU3Ek__BackingField_5; }
	inline void set_U3CisStartedU3Ek__BackingField_5(bool value)
	{
		___U3CisStartedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CisDiscoveringRemotesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022, ___U3CisDiscoveringRemotesU3Ek__BackingField_6)); }
	inline bool get_U3CisDiscoveringRemotesU3Ek__BackingField_6() const { return ___U3CisDiscoveringRemotesU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisDiscoveringRemotesU3Ek__BackingField_6() { return &___U3CisDiscoveringRemotesU3Ek__BackingField_6; }
	inline void set_U3CisDiscoveringRemotesU3Ek__BackingField_6(bool value)
	{
		___U3CisDiscoveringRemotesU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__connectedRemote_7() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022, ____connectedRemote_7)); }
	inline Remote_t1018512123 * get__connectedRemote_7() const { return ____connectedRemote_7; }
	inline Remote_t1018512123 ** get_address_of__connectedRemote_7() { return &____connectedRemote_7; }
	inline void set__connectedRemote_7(Remote_t1018512123 * value)
	{
		____connectedRemote_7 = value;
		Il2CppCodeGenWriteBarrier((&____connectedRemote_7), value);
	}

	inline static int32_t get_offset_of__discoveredRemotes_8() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022, ____discoveredRemotes_8)); }
	inline List_1_t2490586865 * get__discoveredRemotes_8() const { return ____discoveredRemotes_8; }
	inline List_1_t2490586865 ** get_address_of__discoveredRemotes_8() { return &____discoveredRemotes_8; }
	inline void set__discoveredRemotes_8(List_1_t2490586865 * value)
	{
		____discoveredRemotes_8 = value;
		Il2CppCodeGenWriteBarrier((&____discoveredRemotes_8), value);
	}
};

struct RemoteManager_t1941324022_StaticFields
{
public:
	// RemoteManager RemoteManager::Instance
	RemoteManager_t1941324022 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(RemoteManager_t1941324022_StaticFields, ___Instance_4)); }
	inline RemoteManager_t1941324022 * get_Instance_4() const { return ___Instance_4; }
	inline RemoteManager_t1941324022 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(RemoteManager_t1941324022 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMANAGER_T1941324022_H
#ifndef MIRAPRERENDER_T3151757564_H
#define MIRAPRERENDER_T3151757564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPreRender
struct  MiraPreRender_t3151757564  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera MiraPreRender::<cam>k__BackingField
	Camera_t4157153871 * ___U3CcamU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MiraPreRender_t3151757564, ___U3CcamU3Ek__BackingField_2)); }
	inline Camera_t4157153871 * get_U3CcamU3Ek__BackingField_2() const { return ___U3CcamU3Ek__BackingField_2; }
	inline Camera_t4157153871 ** get_address_of_U3CcamU3Ek__BackingField_2() { return &___U3CcamU3Ek__BackingField_2; }
	inline void set_U3CcamU3Ek__BackingField_2(Camera_t4157153871 * value)
	{
		___U3CcamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcamU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPRERENDER_T3151757564_H
#ifndef MIRAARCONTROLLER_T2798564687_H
#define MIRAARCONTROLLER_T2798564687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraArController
struct  MiraArController_t2798564687  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.MiraArController::IPD
	float ___IPD_2;
	// UnityEngine.GameObject Mira.MiraArController::leftCam
	GameObject_t1113636619 * ___leftCam_3;
	// UnityEngine.GameObject Mira.MiraArController::rightCam
	GameObject_t1113636619 * ___rightCam_4;
	// UnityEngine.GameObject Mira.MiraArController::cameraRig
	GameObject_t1113636619 * ___cameraRig_5;
	// System.Boolean Mira.MiraArController::isRotationalOnly
	bool ___isRotationalOnly_6;
	// System.Boolean Mira.MiraArController::isSpectator
	bool ___isSpectator_7;
	// System.Single Mira.MiraArController::nearClipPlane
	float ___nearClipPlane_8;
	// System.Single Mira.MiraArController::farClipPlane
	float ___farClipPlane_9;
	// System.Boolean Mira.MiraArController::defaultScale
	bool ___defaultScale_10;
	// System.Single Mira.MiraArController::setScaleMultiplier
	float ___setScaleMultiplier_11;
	// System.Single Mira.MiraArController::stereoCamFov
	float ___stereoCamFov_13;
	// Mira.MiraViewer Mira.MiraArController::mv
	MiraViewer_t94414402 * ___mv_14;
	// UnityEngine.Texture Mira.MiraArController::btnTexture
	Texture_t3661962703 * ___btnTexture_16;
	// System.Single Mira.MiraArController::buttonHeight
	float ___buttonHeight_17;
	// System.Single Mira.MiraArController::buttonWidth
	float ___buttonWidth_18;
	// UnityEngine.GUISkin Mira.MiraArController::MiraGuiSkin
	GUISkin_t1244372282 * ___MiraGuiSkin_19;
	// UnityEngine.EventSystems.MiraInputModule Mira.MiraArController::miraInputModule
	MiraInputModule_t1698371845 * ___miraInputModule_20;
	// System.Boolean Mira.MiraArController::GUIEnabled
	bool ___GUIEnabled_21;
	// UnityEngine.GameObject Mira.MiraArController::settingsMenu
	GameObject_t1113636619 * ___settingsMenu_22;

public:
	inline static int32_t get_offset_of_IPD_2() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___IPD_2)); }
	inline float get_IPD_2() const { return ___IPD_2; }
	inline float* get_address_of_IPD_2() { return &___IPD_2; }
	inline void set_IPD_2(float value)
	{
		___IPD_2 = value;
	}

	inline static int32_t get_offset_of_leftCam_3() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___leftCam_3)); }
	inline GameObject_t1113636619 * get_leftCam_3() const { return ___leftCam_3; }
	inline GameObject_t1113636619 ** get_address_of_leftCam_3() { return &___leftCam_3; }
	inline void set_leftCam_3(GameObject_t1113636619 * value)
	{
		___leftCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___leftCam_3), value);
	}

	inline static int32_t get_offset_of_rightCam_4() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___rightCam_4)); }
	inline GameObject_t1113636619 * get_rightCam_4() const { return ___rightCam_4; }
	inline GameObject_t1113636619 ** get_address_of_rightCam_4() { return &___rightCam_4; }
	inline void set_rightCam_4(GameObject_t1113636619 * value)
	{
		___rightCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___rightCam_4), value);
	}

	inline static int32_t get_offset_of_cameraRig_5() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___cameraRig_5)); }
	inline GameObject_t1113636619 * get_cameraRig_5() const { return ___cameraRig_5; }
	inline GameObject_t1113636619 ** get_address_of_cameraRig_5() { return &___cameraRig_5; }
	inline void set_cameraRig_5(GameObject_t1113636619 * value)
	{
		___cameraRig_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_5), value);
	}

	inline static int32_t get_offset_of_isRotationalOnly_6() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___isRotationalOnly_6)); }
	inline bool get_isRotationalOnly_6() const { return ___isRotationalOnly_6; }
	inline bool* get_address_of_isRotationalOnly_6() { return &___isRotationalOnly_6; }
	inline void set_isRotationalOnly_6(bool value)
	{
		___isRotationalOnly_6 = value;
	}

	inline static int32_t get_offset_of_isSpectator_7() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___isSpectator_7)); }
	inline bool get_isSpectator_7() const { return ___isSpectator_7; }
	inline bool* get_address_of_isSpectator_7() { return &___isSpectator_7; }
	inline void set_isSpectator_7(bool value)
	{
		___isSpectator_7 = value;
	}

	inline static int32_t get_offset_of_nearClipPlane_8() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___nearClipPlane_8)); }
	inline float get_nearClipPlane_8() const { return ___nearClipPlane_8; }
	inline float* get_address_of_nearClipPlane_8() { return &___nearClipPlane_8; }
	inline void set_nearClipPlane_8(float value)
	{
		___nearClipPlane_8 = value;
	}

	inline static int32_t get_offset_of_farClipPlane_9() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___farClipPlane_9)); }
	inline float get_farClipPlane_9() const { return ___farClipPlane_9; }
	inline float* get_address_of_farClipPlane_9() { return &___farClipPlane_9; }
	inline void set_farClipPlane_9(float value)
	{
		___farClipPlane_9 = value;
	}

	inline static int32_t get_offset_of_defaultScale_10() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___defaultScale_10)); }
	inline bool get_defaultScale_10() const { return ___defaultScale_10; }
	inline bool* get_address_of_defaultScale_10() { return &___defaultScale_10; }
	inline void set_defaultScale_10(bool value)
	{
		___defaultScale_10 = value;
	}

	inline static int32_t get_offset_of_setScaleMultiplier_11() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___setScaleMultiplier_11)); }
	inline float get_setScaleMultiplier_11() const { return ___setScaleMultiplier_11; }
	inline float* get_address_of_setScaleMultiplier_11() { return &___setScaleMultiplier_11; }
	inline void set_setScaleMultiplier_11(float value)
	{
		___setScaleMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_stereoCamFov_13() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___stereoCamFov_13)); }
	inline float get_stereoCamFov_13() const { return ___stereoCamFov_13; }
	inline float* get_address_of_stereoCamFov_13() { return &___stereoCamFov_13; }
	inline void set_stereoCamFov_13(float value)
	{
		___stereoCamFov_13 = value;
	}

	inline static int32_t get_offset_of_mv_14() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___mv_14)); }
	inline MiraViewer_t94414402 * get_mv_14() const { return ___mv_14; }
	inline MiraViewer_t94414402 ** get_address_of_mv_14() { return &___mv_14; }
	inline void set_mv_14(MiraViewer_t94414402 * value)
	{
		___mv_14 = value;
		Il2CppCodeGenWriteBarrier((&___mv_14), value);
	}

	inline static int32_t get_offset_of_btnTexture_16() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___btnTexture_16)); }
	inline Texture_t3661962703 * get_btnTexture_16() const { return ___btnTexture_16; }
	inline Texture_t3661962703 ** get_address_of_btnTexture_16() { return &___btnTexture_16; }
	inline void set_btnTexture_16(Texture_t3661962703 * value)
	{
		___btnTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___btnTexture_16), value);
	}

	inline static int32_t get_offset_of_buttonHeight_17() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___buttonHeight_17)); }
	inline float get_buttonHeight_17() const { return ___buttonHeight_17; }
	inline float* get_address_of_buttonHeight_17() { return &___buttonHeight_17; }
	inline void set_buttonHeight_17(float value)
	{
		___buttonHeight_17 = value;
	}

	inline static int32_t get_offset_of_buttonWidth_18() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___buttonWidth_18)); }
	inline float get_buttonWidth_18() const { return ___buttonWidth_18; }
	inline float* get_address_of_buttonWidth_18() { return &___buttonWidth_18; }
	inline void set_buttonWidth_18(float value)
	{
		___buttonWidth_18 = value;
	}

	inline static int32_t get_offset_of_MiraGuiSkin_19() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___MiraGuiSkin_19)); }
	inline GUISkin_t1244372282 * get_MiraGuiSkin_19() const { return ___MiraGuiSkin_19; }
	inline GUISkin_t1244372282 ** get_address_of_MiraGuiSkin_19() { return &___MiraGuiSkin_19; }
	inline void set_MiraGuiSkin_19(GUISkin_t1244372282 * value)
	{
		___MiraGuiSkin_19 = value;
		Il2CppCodeGenWriteBarrier((&___MiraGuiSkin_19), value);
	}

	inline static int32_t get_offset_of_miraInputModule_20() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___miraInputModule_20)); }
	inline MiraInputModule_t1698371845 * get_miraInputModule_20() const { return ___miraInputModule_20; }
	inline MiraInputModule_t1698371845 ** get_address_of_miraInputModule_20() { return &___miraInputModule_20; }
	inline void set_miraInputModule_20(MiraInputModule_t1698371845 * value)
	{
		___miraInputModule_20 = value;
		Il2CppCodeGenWriteBarrier((&___miraInputModule_20), value);
	}

	inline static int32_t get_offset_of_GUIEnabled_21() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___GUIEnabled_21)); }
	inline bool get_GUIEnabled_21() const { return ___GUIEnabled_21; }
	inline bool* get_address_of_GUIEnabled_21() { return &___GUIEnabled_21; }
	inline void set_GUIEnabled_21(bool value)
	{
		___GUIEnabled_21 = value;
	}

	inline static int32_t get_offset_of_settingsMenu_22() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687, ___settingsMenu_22)); }
	inline GameObject_t1113636619 * get_settingsMenu_22() const { return ___settingsMenu_22; }
	inline GameObject_t1113636619 ** get_address_of_settingsMenu_22() { return &___settingsMenu_22; }
	inline void set_settingsMenu_22(GameObject_t1113636619 * value)
	{
		___settingsMenu_22 = value;
		Il2CppCodeGenWriteBarrier((&___settingsMenu_22), value);
	}
};

struct MiraArController_t2798564687_StaticFields
{
public:
	// System.Single Mira.MiraArController::scaleMultiplier
	float ___scaleMultiplier_12;
	// Mira.MiraArController Mira.MiraArController::instance
	MiraArController_t2798564687 * ___instance_15;

public:
	inline static int32_t get_offset_of_scaleMultiplier_12() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687_StaticFields, ___scaleMultiplier_12)); }
	inline float get_scaleMultiplier_12() const { return ___scaleMultiplier_12; }
	inline float* get_address_of_scaleMultiplier_12() { return &___scaleMultiplier_12; }
	inline void set_scaleMultiplier_12(float value)
	{
		___scaleMultiplier_12 = value;
	}

	inline static int32_t get_offset_of_instance_15() { return static_cast<int32_t>(offsetof(MiraArController_t2798564687_StaticFields, ___instance_15)); }
	inline MiraArController_t2798564687 * get_instance_15() const { return ___instance_15; }
	inline MiraArController_t2798564687 ** get_address_of_instance_15() { return &___instance_15; }
	inline void set_instance_15(MiraArController_t2798564687 * value)
	{
		___instance_15 = value;
		Il2CppCodeGenWriteBarrier((&___instance_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAARCONTROLLER_T2798564687_H
#ifndef MIRARETICLE_T910965139_H
#define MIRARETICLE_T910965139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraReticle
struct  MiraReticle_t910965139  : public MonoBehaviour_t3962482529
{
public:
	// MiraReticlePointer MiraReticle::reticlepointer
	MiraReticlePointer_t3058549069 * ___reticlepointer_2;
	// UnityEngine.Transform MiraReticle::cameraRig
	Transform_t3600365921 * ___cameraRig_4;
	// System.Boolean MiraReticle::onlyVisibleOnHover
	bool ___onlyVisibleOnHover_5;
	// System.Single MiraReticle::maxDistance
	float ___maxDistance_6;
	// System.Single MiraReticle::minDistance
	float ___minDistance_7;
	// System.Single MiraReticle::minScale
	float ___minScale_8;
	// System.Single MiraReticle::maxScale
	float ___maxScale_9;
	// System.Single MiraReticle::lastDistance
	float ___lastDistance_10;
	// System.Single MiraReticle::externalMultiplier
	float ___externalMultiplier_11;
	// UnityEngine.Color MiraReticle::reticleHoverColor
	Color_t2555686324  ___reticleHoverColor_12;
	// UnityEngine.Color MiraReticle::reticleIdleColor
	Color_t2555686324  ___reticleIdleColor_13;
	// UnityEngine.SpriteRenderer MiraReticle::reticleRenderer
	SpriteRenderer_t3235626157 * ___reticleRenderer_14;
	// UnityEngine.Vector3 MiraReticle::reticleOriginalScale
	Vector3_t3722313464  ___reticleOriginalScale_15;

public:
	inline static int32_t get_offset_of_reticlepointer_2() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___reticlepointer_2)); }
	inline MiraReticlePointer_t3058549069 * get_reticlepointer_2() const { return ___reticlepointer_2; }
	inline MiraReticlePointer_t3058549069 ** get_address_of_reticlepointer_2() { return &___reticlepointer_2; }
	inline void set_reticlepointer_2(MiraReticlePointer_t3058549069 * value)
	{
		___reticlepointer_2 = value;
		Il2CppCodeGenWriteBarrier((&___reticlepointer_2), value);
	}

	inline static int32_t get_offset_of_cameraRig_4() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___cameraRig_4)); }
	inline Transform_t3600365921 * get_cameraRig_4() const { return ___cameraRig_4; }
	inline Transform_t3600365921 ** get_address_of_cameraRig_4() { return &___cameraRig_4; }
	inline void set_cameraRig_4(Transform_t3600365921 * value)
	{
		___cameraRig_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_4), value);
	}

	inline static int32_t get_offset_of_onlyVisibleOnHover_5() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___onlyVisibleOnHover_5)); }
	inline bool get_onlyVisibleOnHover_5() const { return ___onlyVisibleOnHover_5; }
	inline bool* get_address_of_onlyVisibleOnHover_5() { return &___onlyVisibleOnHover_5; }
	inline void set_onlyVisibleOnHover_5(bool value)
	{
		___onlyVisibleOnHover_5 = value;
	}

	inline static int32_t get_offset_of_maxDistance_6() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___maxDistance_6)); }
	inline float get_maxDistance_6() const { return ___maxDistance_6; }
	inline float* get_address_of_maxDistance_6() { return &___maxDistance_6; }
	inline void set_maxDistance_6(float value)
	{
		___maxDistance_6 = value;
	}

	inline static int32_t get_offset_of_minDistance_7() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___minDistance_7)); }
	inline float get_minDistance_7() const { return ___minDistance_7; }
	inline float* get_address_of_minDistance_7() { return &___minDistance_7; }
	inline void set_minDistance_7(float value)
	{
		___minDistance_7 = value;
	}

	inline static int32_t get_offset_of_minScale_8() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___minScale_8)); }
	inline float get_minScale_8() const { return ___minScale_8; }
	inline float* get_address_of_minScale_8() { return &___minScale_8; }
	inline void set_minScale_8(float value)
	{
		___minScale_8 = value;
	}

	inline static int32_t get_offset_of_maxScale_9() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___maxScale_9)); }
	inline float get_maxScale_9() const { return ___maxScale_9; }
	inline float* get_address_of_maxScale_9() { return &___maxScale_9; }
	inline void set_maxScale_9(float value)
	{
		___maxScale_9 = value;
	}

	inline static int32_t get_offset_of_lastDistance_10() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___lastDistance_10)); }
	inline float get_lastDistance_10() const { return ___lastDistance_10; }
	inline float* get_address_of_lastDistance_10() { return &___lastDistance_10; }
	inline void set_lastDistance_10(float value)
	{
		___lastDistance_10 = value;
	}

	inline static int32_t get_offset_of_externalMultiplier_11() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___externalMultiplier_11)); }
	inline float get_externalMultiplier_11() const { return ___externalMultiplier_11; }
	inline float* get_address_of_externalMultiplier_11() { return &___externalMultiplier_11; }
	inline void set_externalMultiplier_11(float value)
	{
		___externalMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_reticleHoverColor_12() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___reticleHoverColor_12)); }
	inline Color_t2555686324  get_reticleHoverColor_12() const { return ___reticleHoverColor_12; }
	inline Color_t2555686324 * get_address_of_reticleHoverColor_12() { return &___reticleHoverColor_12; }
	inline void set_reticleHoverColor_12(Color_t2555686324  value)
	{
		___reticleHoverColor_12 = value;
	}

	inline static int32_t get_offset_of_reticleIdleColor_13() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___reticleIdleColor_13)); }
	inline Color_t2555686324  get_reticleIdleColor_13() const { return ___reticleIdleColor_13; }
	inline Color_t2555686324 * get_address_of_reticleIdleColor_13() { return &___reticleIdleColor_13; }
	inline void set_reticleIdleColor_13(Color_t2555686324  value)
	{
		___reticleIdleColor_13 = value;
	}

	inline static int32_t get_offset_of_reticleRenderer_14() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___reticleRenderer_14)); }
	inline SpriteRenderer_t3235626157 * get_reticleRenderer_14() const { return ___reticleRenderer_14; }
	inline SpriteRenderer_t3235626157 ** get_address_of_reticleRenderer_14() { return &___reticleRenderer_14; }
	inline void set_reticleRenderer_14(SpriteRenderer_t3235626157 * value)
	{
		___reticleRenderer_14 = value;
		Il2CppCodeGenWriteBarrier((&___reticleRenderer_14), value);
	}

	inline static int32_t get_offset_of_reticleOriginalScale_15() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139, ___reticleOriginalScale_15)); }
	inline Vector3_t3722313464  get_reticleOriginalScale_15() const { return ___reticleOriginalScale_15; }
	inline Vector3_t3722313464 * get_address_of_reticleOriginalScale_15() { return &___reticleOriginalScale_15; }
	inline void set_reticleOriginalScale_15(Vector3_t3722313464  value)
	{
		___reticleOriginalScale_15 = value;
	}
};

struct MiraReticle_t910965139_StaticFields
{
public:
	// MiraReticle MiraReticle::_instance
	MiraReticle_t910965139 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(MiraReticle_t910965139_StaticFields, ____instance_3)); }
	inline MiraReticle_t910965139 * get__instance_3() const { return ____instance_3; }
	inline MiraReticle_t910965139 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(MiraReticle_t910965139 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRARETICLE_T910965139_H
#ifndef MIRALIVEPREVIEWVIDEO_T1040645816_H
#define MIRALIVEPREVIEWVIDEO_T1040645816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewVideo
struct  MiraLivePreviewVideo_t1040645816  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWVIDEO_T1040645816_H
#ifndef MIRALIVEPREVIEWWIKICONFIG_T2494817680_H
#define MIRALIVEPREVIEWWIKICONFIG_T2494817680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraLivePreviewWikiConfig
struct  MiraLivePreviewWikiConfig_t2494817680  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.WikitudeCamera MiraLivePreviewWikiConfig::ArCam
	WikitudeCamera_t2188881370 * ___ArCam_2;

public:
	inline static int32_t get_offset_of_ArCam_2() { return static_cast<int32_t>(offsetof(MiraLivePreviewWikiConfig_t2494817680, ___ArCam_2)); }
	inline WikitudeCamera_t2188881370 * get_ArCam_2() const { return ___ArCam_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_ArCam_2() { return &___ArCam_2; }
	inline void set_ArCam_2(WikitudeCamera_t2188881370 * value)
	{
		___ArCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___ArCam_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWWIKICONFIG_T2494817680_H
#ifndef MIRAPOSTRENDER_T1363872762_H
#define MIRAPOSTRENDER_T1363872762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraPostRender
struct  MiraPostRender_t1363872762  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.MiraPostRender::stereoCamFov
	float ___stereoCamFov_2;
	// System.Boolean Mira.MiraPostRender::noDistortion
	bool ___noDistortion_3;
	// System.Boolean Mira.MiraPostRender::postReduceDistortion
	bool ___postReduceDistortion_4;
	// System.Single Mira.MiraPostRender::xDistAmount
	float ___xDistAmount_5;
	// System.Single Mira.MiraPostRender::yDistAmount
	float ___yDistAmount_6;
	// System.Boolean Mira.MiraPostRender::tanCoordinates
	bool ___tanCoordinates_7;
	// Mira.MiraPostRender/Eye Mira.MiraPostRender::eye
	int32_t ___eye_8;
	// System.Int32 Mira.MiraPostRender::xSize
	int32_t ___xSize_9;
	// System.Int32 Mira.MiraPostRender::ySize
	int32_t ___ySize_10;
	// System.Single Mira.MiraPostRender::zOffset
	float ___zOffset_11;
	// System.Single Mira.MiraPostRender::xScalar
	float ___xScalar_12;
	// System.Single Mira.MiraPostRender::yScalar
	float ___yScalar_13;
	// System.Single Mira.MiraPostRender::tanConstant
	float ___tanConstant_14;
	// System.Single Mira.MiraPostRender::desiredParallaxDist
	float ___desiredParallaxDist_15;
	// UnityEngine.Mesh Mira.MiraPostRender::mesh
	Mesh_t3648964284 * ___mesh_16;
	// UnityEngine.Material Mira.MiraPostRender::renderTextureMaterial
	Material_t340375123 * ___renderTextureMaterial_17;
	// System.Single Mira.MiraPostRender::IPD
	float ___IPD_18;
	// System.Single Mira.MiraPostRender::ParallaxShift
	float ___ParallaxShift_19;
	// System.Double[] Mira.MiraPostRender::coefficientsXLeft
	DoubleU5BU5D_t3413330114* ___coefficientsXLeft_20;
	// System.Double[] Mira.MiraPostRender::coefficientsYLeft
	DoubleU5BU5D_t3413330114* ___coefficientsYLeft_21;
	// System.Double[] Mira.MiraPostRender::coefficientsXRight
	DoubleU5BU5D_t3413330114* ___coefficientsXRight_22;
	// System.Double[] Mira.MiraPostRender::coefficientsYRight
	DoubleU5BU5D_t3413330114* ___coefficientsYRight_23;
	// DistortionEquation Mira.MiraPostRender::distortion
	DistortionEquation_t32189124 * ___distortion_24;

public:
	inline static int32_t get_offset_of_stereoCamFov_2() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___stereoCamFov_2)); }
	inline float get_stereoCamFov_2() const { return ___stereoCamFov_2; }
	inline float* get_address_of_stereoCamFov_2() { return &___stereoCamFov_2; }
	inline void set_stereoCamFov_2(float value)
	{
		___stereoCamFov_2 = value;
	}

	inline static int32_t get_offset_of_noDistortion_3() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___noDistortion_3)); }
	inline bool get_noDistortion_3() const { return ___noDistortion_3; }
	inline bool* get_address_of_noDistortion_3() { return &___noDistortion_3; }
	inline void set_noDistortion_3(bool value)
	{
		___noDistortion_3 = value;
	}

	inline static int32_t get_offset_of_postReduceDistortion_4() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___postReduceDistortion_4)); }
	inline bool get_postReduceDistortion_4() const { return ___postReduceDistortion_4; }
	inline bool* get_address_of_postReduceDistortion_4() { return &___postReduceDistortion_4; }
	inline void set_postReduceDistortion_4(bool value)
	{
		___postReduceDistortion_4 = value;
	}

	inline static int32_t get_offset_of_xDistAmount_5() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___xDistAmount_5)); }
	inline float get_xDistAmount_5() const { return ___xDistAmount_5; }
	inline float* get_address_of_xDistAmount_5() { return &___xDistAmount_5; }
	inline void set_xDistAmount_5(float value)
	{
		___xDistAmount_5 = value;
	}

	inline static int32_t get_offset_of_yDistAmount_6() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___yDistAmount_6)); }
	inline float get_yDistAmount_6() const { return ___yDistAmount_6; }
	inline float* get_address_of_yDistAmount_6() { return &___yDistAmount_6; }
	inline void set_yDistAmount_6(float value)
	{
		___yDistAmount_6 = value;
	}

	inline static int32_t get_offset_of_tanCoordinates_7() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___tanCoordinates_7)); }
	inline bool get_tanCoordinates_7() const { return ___tanCoordinates_7; }
	inline bool* get_address_of_tanCoordinates_7() { return &___tanCoordinates_7; }
	inline void set_tanCoordinates_7(bool value)
	{
		___tanCoordinates_7 = value;
	}

	inline static int32_t get_offset_of_eye_8() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___eye_8)); }
	inline int32_t get_eye_8() const { return ___eye_8; }
	inline int32_t* get_address_of_eye_8() { return &___eye_8; }
	inline void set_eye_8(int32_t value)
	{
		___eye_8 = value;
	}

	inline static int32_t get_offset_of_xSize_9() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___xSize_9)); }
	inline int32_t get_xSize_9() const { return ___xSize_9; }
	inline int32_t* get_address_of_xSize_9() { return &___xSize_9; }
	inline void set_xSize_9(int32_t value)
	{
		___xSize_9 = value;
	}

	inline static int32_t get_offset_of_ySize_10() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___ySize_10)); }
	inline int32_t get_ySize_10() const { return ___ySize_10; }
	inline int32_t* get_address_of_ySize_10() { return &___ySize_10; }
	inline void set_ySize_10(int32_t value)
	{
		___ySize_10 = value;
	}

	inline static int32_t get_offset_of_zOffset_11() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___zOffset_11)); }
	inline float get_zOffset_11() const { return ___zOffset_11; }
	inline float* get_address_of_zOffset_11() { return &___zOffset_11; }
	inline void set_zOffset_11(float value)
	{
		___zOffset_11 = value;
	}

	inline static int32_t get_offset_of_xScalar_12() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___xScalar_12)); }
	inline float get_xScalar_12() const { return ___xScalar_12; }
	inline float* get_address_of_xScalar_12() { return &___xScalar_12; }
	inline void set_xScalar_12(float value)
	{
		___xScalar_12 = value;
	}

	inline static int32_t get_offset_of_yScalar_13() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___yScalar_13)); }
	inline float get_yScalar_13() const { return ___yScalar_13; }
	inline float* get_address_of_yScalar_13() { return &___yScalar_13; }
	inline void set_yScalar_13(float value)
	{
		___yScalar_13 = value;
	}

	inline static int32_t get_offset_of_tanConstant_14() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___tanConstant_14)); }
	inline float get_tanConstant_14() const { return ___tanConstant_14; }
	inline float* get_address_of_tanConstant_14() { return &___tanConstant_14; }
	inline void set_tanConstant_14(float value)
	{
		___tanConstant_14 = value;
	}

	inline static int32_t get_offset_of_desiredParallaxDist_15() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___desiredParallaxDist_15)); }
	inline float get_desiredParallaxDist_15() const { return ___desiredParallaxDist_15; }
	inline float* get_address_of_desiredParallaxDist_15() { return &___desiredParallaxDist_15; }
	inline void set_desiredParallaxDist_15(float value)
	{
		___desiredParallaxDist_15 = value;
	}

	inline static int32_t get_offset_of_mesh_16() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___mesh_16)); }
	inline Mesh_t3648964284 * get_mesh_16() const { return ___mesh_16; }
	inline Mesh_t3648964284 ** get_address_of_mesh_16() { return &___mesh_16; }
	inline void set_mesh_16(Mesh_t3648964284 * value)
	{
		___mesh_16 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_16), value);
	}

	inline static int32_t get_offset_of_renderTextureMaterial_17() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___renderTextureMaterial_17)); }
	inline Material_t340375123 * get_renderTextureMaterial_17() const { return ___renderTextureMaterial_17; }
	inline Material_t340375123 ** get_address_of_renderTextureMaterial_17() { return &___renderTextureMaterial_17; }
	inline void set_renderTextureMaterial_17(Material_t340375123 * value)
	{
		___renderTextureMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureMaterial_17), value);
	}

	inline static int32_t get_offset_of_IPD_18() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___IPD_18)); }
	inline float get_IPD_18() const { return ___IPD_18; }
	inline float* get_address_of_IPD_18() { return &___IPD_18; }
	inline void set_IPD_18(float value)
	{
		___IPD_18 = value;
	}

	inline static int32_t get_offset_of_ParallaxShift_19() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___ParallaxShift_19)); }
	inline float get_ParallaxShift_19() const { return ___ParallaxShift_19; }
	inline float* get_address_of_ParallaxShift_19() { return &___ParallaxShift_19; }
	inline void set_ParallaxShift_19(float value)
	{
		___ParallaxShift_19 = value;
	}

	inline static int32_t get_offset_of_coefficientsXLeft_20() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___coefficientsXLeft_20)); }
	inline DoubleU5BU5D_t3413330114* get_coefficientsXLeft_20() const { return ___coefficientsXLeft_20; }
	inline DoubleU5BU5D_t3413330114** get_address_of_coefficientsXLeft_20() { return &___coefficientsXLeft_20; }
	inline void set_coefficientsXLeft_20(DoubleU5BU5D_t3413330114* value)
	{
		___coefficientsXLeft_20 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsXLeft_20), value);
	}

	inline static int32_t get_offset_of_coefficientsYLeft_21() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___coefficientsYLeft_21)); }
	inline DoubleU5BU5D_t3413330114* get_coefficientsYLeft_21() const { return ___coefficientsYLeft_21; }
	inline DoubleU5BU5D_t3413330114** get_address_of_coefficientsYLeft_21() { return &___coefficientsYLeft_21; }
	inline void set_coefficientsYLeft_21(DoubleU5BU5D_t3413330114* value)
	{
		___coefficientsYLeft_21 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsYLeft_21), value);
	}

	inline static int32_t get_offset_of_coefficientsXRight_22() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___coefficientsXRight_22)); }
	inline DoubleU5BU5D_t3413330114* get_coefficientsXRight_22() const { return ___coefficientsXRight_22; }
	inline DoubleU5BU5D_t3413330114** get_address_of_coefficientsXRight_22() { return &___coefficientsXRight_22; }
	inline void set_coefficientsXRight_22(DoubleU5BU5D_t3413330114* value)
	{
		___coefficientsXRight_22 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsXRight_22), value);
	}

	inline static int32_t get_offset_of_coefficientsYRight_23() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___coefficientsYRight_23)); }
	inline DoubleU5BU5D_t3413330114* get_coefficientsYRight_23() const { return ___coefficientsYRight_23; }
	inline DoubleU5BU5D_t3413330114** get_address_of_coefficientsYRight_23() { return &___coefficientsYRight_23; }
	inline void set_coefficientsYRight_23(DoubleU5BU5D_t3413330114* value)
	{
		___coefficientsYRight_23 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsYRight_23), value);
	}

	inline static int32_t get_offset_of_distortion_24() { return static_cast<int32_t>(offsetof(MiraPostRender_t1363872762, ___distortion_24)); }
	inline DistortionEquation_t32189124 * get_distortion_24() const { return ___distortion_24; }
	inline DistortionEquation_t32189124 ** get_address_of_distortion_24() { return &___distortion_24; }
	inline void set_distortion_24(DistortionEquation_t32189124 * value)
	{
		___distortion_24 = value;
		Il2CppCodeGenWriteBarrier((&___distortion_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPOSTRENDER_T1363872762_H
#ifndef MIRALIVEPREVIEWEDITOR_T1492853446_H
#define MIRALIVEPREVIEWEDITOR_T1492853446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewEditor
struct  MiraLivePreviewEditor_t1492853446  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWEDITOR_T1492853446_H
#ifndef MIRALIVEPREVIEWPLAYER_T291622437_H
#define MIRALIVEPREVIEWPLAYER_T291622437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewPlayer
struct  MiraLivePreviewPlayer_t291622437  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.MiraLivePreviewPlayer::playerConnection
	PlayerConnection_t3081694049 * ___playerConnection_2;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::bSessionActive
	bool ___bSessionActive_3;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::editorID
	int32_t ___editorID_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.MiraLivePreviewPlayer::liveViewScreenTex
	Texture2D_t3840446185 * ___liveViewScreenTex_5;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::bTexturesInitialized
	bool ___bTexturesInitialized_6;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::isTracking
	bool ___isTracking_7;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::btFrameCounter
	int32_t ___btFrameCounter_9;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::btSendRate
	int32_t ___btSendRate_10;

public:
	inline static int32_t get_offset_of_playerConnection_2() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___playerConnection_2)); }
	inline PlayerConnection_t3081694049 * get_playerConnection_2() const { return ___playerConnection_2; }
	inline PlayerConnection_t3081694049 ** get_address_of_playerConnection_2() { return &___playerConnection_2; }
	inline void set_playerConnection_2(PlayerConnection_t3081694049 * value)
	{
		___playerConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_2), value);
	}

	inline static int32_t get_offset_of_bSessionActive_3() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___bSessionActive_3)); }
	inline bool get_bSessionActive_3() const { return ___bSessionActive_3; }
	inline bool* get_address_of_bSessionActive_3() { return &___bSessionActive_3; }
	inline void set_bSessionActive_3(bool value)
	{
		___bSessionActive_3 = value;
	}

	inline static int32_t get_offset_of_editorID_4() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___editorID_4)); }
	inline int32_t get_editorID_4() const { return ___editorID_4; }
	inline int32_t* get_address_of_editorID_4() { return &___editorID_4; }
	inline void set_editorID_4(int32_t value)
	{
		___editorID_4 = value;
	}

	inline static int32_t get_offset_of_liveViewScreenTex_5() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___liveViewScreenTex_5)); }
	inline Texture2D_t3840446185 * get_liveViewScreenTex_5() const { return ___liveViewScreenTex_5; }
	inline Texture2D_t3840446185 ** get_address_of_liveViewScreenTex_5() { return &___liveViewScreenTex_5; }
	inline void set_liveViewScreenTex_5(Texture2D_t3840446185 * value)
	{
		___liveViewScreenTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___liveViewScreenTex_5), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_6() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___bTexturesInitialized_6)); }
	inline bool get_bTexturesInitialized_6() const { return ___bTexturesInitialized_6; }
	inline bool* get_address_of_bTexturesInitialized_6() { return &___bTexturesInitialized_6; }
	inline void set_bTexturesInitialized_6(bool value)
	{
		___bTexturesInitialized_6 = value;
	}

	inline static int32_t get_offset_of_isTracking_7() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___isTracking_7)); }
	inline bool get_isTracking_7() const { return ___isTracking_7; }
	inline bool* get_address_of_isTracking_7() { return &___isTracking_7; }
	inline void set_isTracking_7(bool value)
	{
		___isTracking_7 = value;
	}

	inline static int32_t get_offset_of_btFrameCounter_9() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___btFrameCounter_9)); }
	inline int32_t get_btFrameCounter_9() const { return ___btFrameCounter_9; }
	inline int32_t* get_address_of_btFrameCounter_9() { return &___btFrameCounter_9; }
	inline void set_btFrameCounter_9(int32_t value)
	{
		___btFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_btSendRate_10() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437, ___btSendRate_10)); }
	inline int32_t get_btSendRate_10() const { return ___btSendRate_10; }
	inline int32_t* get_address_of_btSendRate_10() { return &___btSendRate_10; }
	inline void set_btSendRate_10(int32_t value)
	{
		___btSendRate_10 = value;
	}
};

struct MiraLivePreviewPlayer_t291622437_StaticFields
{
public:
	// MiraBTRemoteInput UnityEngine.XR.iOS.MiraLivePreviewPlayer::m_userInput
	MiraBTRemoteInput_t3160068026 * ___m_userInput_8;

public:
	inline static int32_t get_offset_of_m_userInput_8() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t291622437_StaticFields, ___m_userInput_8)); }
	inline MiraBTRemoteInput_t3160068026 * get_m_userInput_8() const { return ___m_userInput_8; }
	inline MiraBTRemoteInput_t3160068026 ** get_address_of_m_userInput_8() { return &___m_userInput_8; }
	inline void set_m_userInput_8(MiraBTRemoteInput_t3160068026 * value)
	{
		___m_userInput_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_userInput_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWPLAYER_T291622437_H
#ifndef MIRAARVIDEO_T2088478125_H
#define MIRAARVIDEO_T2088478125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraARVideo
struct  MiraARVideo_t2088478125  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D MiraARVideo::m_clearTexture
	Texture2D_t3840446185 * ___m_clearTexture_2;

public:
	inline static int32_t get_offset_of_m_clearTexture_2() { return static_cast<int32_t>(offsetof(MiraARVideo_t2088478125, ___m_clearTexture_2)); }
	inline Texture2D_t3840446185 * get_m_clearTexture_2() const { return ___m_clearTexture_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_clearTexture_2() { return &___m_clearTexture_2; }
	inline void set_m_clearTexture_2(Texture2D_t3840446185 * value)
	{
		___m_clearTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_clearTexture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAARVIDEO_T2088478125_H
#ifndef MIRAEDITORPREVIEW_T1088319792_H
#define MIRAEDITORPREVIEW_T1088319792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraEditorPreview
struct  MiraEditorPreview_t1088319792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAEDITORPREVIEW_T1088319792_H
#ifndef MIRALASERPOINTERLENGTH_T2739907205_H
#define MIRALASERPOINTERLENGTH_T2739907205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraLaserPointerLength
struct  MiraLaserPointerLength_t2739907205  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MiraLaserPointerLength::widthMultiplier
	float ___widthMultiplier_2;
	// MiraReticle MiraLaserPointerLength::reticle
	MiraReticle_t910965139 * ___reticle_3;
	// UnityEngine.LineRenderer MiraLaserPointerLength::rend
	LineRenderer_t3154350270 * ___rend_4;

public:
	inline static int32_t get_offset_of_widthMultiplier_2() { return static_cast<int32_t>(offsetof(MiraLaserPointerLength_t2739907205, ___widthMultiplier_2)); }
	inline float get_widthMultiplier_2() const { return ___widthMultiplier_2; }
	inline float* get_address_of_widthMultiplier_2() { return &___widthMultiplier_2; }
	inline void set_widthMultiplier_2(float value)
	{
		___widthMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_reticle_3() { return static_cast<int32_t>(offsetof(MiraLaserPointerLength_t2739907205, ___reticle_3)); }
	inline MiraReticle_t910965139 * get_reticle_3() const { return ___reticle_3; }
	inline MiraReticle_t910965139 ** get_address_of_reticle_3() { return &___reticle_3; }
	inline void set_reticle_3(MiraReticle_t910965139 * value)
	{
		___reticle_3 = value;
		Il2CppCodeGenWriteBarrier((&___reticle_3), value);
	}

	inline static int32_t get_offset_of_rend_4() { return static_cast<int32_t>(offsetof(MiraLaserPointerLength_t2739907205, ___rend_4)); }
	inline LineRenderer_t3154350270 * get_rend_4() const { return ___rend_4; }
	inline LineRenderer_t3154350270 ** get_address_of_rend_4() { return &___rend_4; }
	inline void set_rend_4(LineRenderer_t3154350270 * value)
	{
		___rend_4 = value;
		Il2CppCodeGenWriteBarrier((&___rend_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALASERPOINTERLENGTH_T2739907205_H
#ifndef LASERLERP_T1945677441_H
#define LASERLERP_T1945677441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.LaserLerp
struct  LaserLerp_t1945677441  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.LaserLerp::laserLerpSpeed
	float ___laserLerpSpeed_2;

public:
	inline static int32_t get_offset_of_laserLerpSpeed_2() { return static_cast<int32_t>(offsetof(LaserLerp_t1945677441, ___laserLerpSpeed_2)); }
	inline float get_laserLerpSpeed_2() const { return ___laserLerpSpeed_2; }
	inline float* get_address_of_laserLerpSpeed_2() { return &___laserLerpSpeed_2; }
	inline void set_laserLerpSpeed_2(float value)
	{
		___laserLerpSpeed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LASERLERP_T1945677441_H
#ifndef FOLLOWHEADAUTOMATIC_T1017553725_H
#define FOLLOWHEADAUTOMATIC_T1017553725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.FollowHeadAutomatic
struct  FollowHeadAutomatic_t1017553725  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 Mira.FollowHeadAutomatic::headFollowPositionCM
	Vector3_t3722313464  ___headFollowPositionCM_2;
	// System.Single Mira.FollowHeadAutomatic::lerpSpeed
	float ___lerpSpeed_3;
	// UnityEngine.Transform Mira.FollowHeadAutomatic::head
	Transform_t3600365921 * ___head_4;

public:
	inline static int32_t get_offset_of_headFollowPositionCM_2() { return static_cast<int32_t>(offsetof(FollowHeadAutomatic_t1017553725, ___headFollowPositionCM_2)); }
	inline Vector3_t3722313464  get_headFollowPositionCM_2() const { return ___headFollowPositionCM_2; }
	inline Vector3_t3722313464 * get_address_of_headFollowPositionCM_2() { return &___headFollowPositionCM_2; }
	inline void set_headFollowPositionCM_2(Vector3_t3722313464  value)
	{
		___headFollowPositionCM_2 = value;
	}

	inline static int32_t get_offset_of_lerpSpeed_3() { return static_cast<int32_t>(offsetof(FollowHeadAutomatic_t1017553725, ___lerpSpeed_3)); }
	inline float get_lerpSpeed_3() const { return ___lerpSpeed_3; }
	inline float* get_address_of_lerpSpeed_3() { return &___lerpSpeed_3; }
	inline void set_lerpSpeed_3(float value)
	{
		___lerpSpeed_3 = value;
	}

	inline static int32_t get_offset_of_head_4() { return static_cast<int32_t>(offsetof(FollowHeadAutomatic_t1017553725, ___head_4)); }
	inline Transform_t3600365921 * get_head_4() const { return ___head_4; }
	inline Transform_t3600365921 ** get_address_of_head_4() { return &___head_4; }
	inline void set_head_4(Transform_t3600365921 * value)
	{
		___head_4 = value;
		Il2CppCodeGenWriteBarrier((&___head_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWHEADAUTOMATIC_T1017553725_H
#ifndef SETTINGSMANAGER_T2083239687_H
#define SETTINGSMANAGER_T2083239687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager
struct  SettingsManager_t2083239687  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SettingsManager::MainSettingsMenu
	GameObject_t1113636619 * ___MainSettingsMenu_2;
	// UnityEngine.GameObject SettingsManager::RemoteMenu
	GameObject_t1113636619 * ___RemoteMenu_3;
	// UnityEngine.GameObject SettingsManager::connectedNotification
	GameObject_t1113636619 * ___connectedNotification_4;
	// UnityEngine.GameObject SettingsManager::disconnectedNotification
	GameObject_t1113636619 * ___disconnectedNotification_5;
	// RemotesController SettingsManager::remotesController
	RemotesController_t1084025038 * ___remotesController_7;
	// UnityEngine.GameObject SettingsManager::settingsButton
	GameObject_t1113636619 * ___settingsButton_8;

public:
	inline static int32_t get_offset_of_MainSettingsMenu_2() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687, ___MainSettingsMenu_2)); }
	inline GameObject_t1113636619 * get_MainSettingsMenu_2() const { return ___MainSettingsMenu_2; }
	inline GameObject_t1113636619 ** get_address_of_MainSettingsMenu_2() { return &___MainSettingsMenu_2; }
	inline void set_MainSettingsMenu_2(GameObject_t1113636619 * value)
	{
		___MainSettingsMenu_2 = value;
		Il2CppCodeGenWriteBarrier((&___MainSettingsMenu_2), value);
	}

	inline static int32_t get_offset_of_RemoteMenu_3() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687, ___RemoteMenu_3)); }
	inline GameObject_t1113636619 * get_RemoteMenu_3() const { return ___RemoteMenu_3; }
	inline GameObject_t1113636619 ** get_address_of_RemoteMenu_3() { return &___RemoteMenu_3; }
	inline void set_RemoteMenu_3(GameObject_t1113636619 * value)
	{
		___RemoteMenu_3 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteMenu_3), value);
	}

	inline static int32_t get_offset_of_connectedNotification_4() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687, ___connectedNotification_4)); }
	inline GameObject_t1113636619 * get_connectedNotification_4() const { return ___connectedNotification_4; }
	inline GameObject_t1113636619 ** get_address_of_connectedNotification_4() { return &___connectedNotification_4; }
	inline void set_connectedNotification_4(GameObject_t1113636619 * value)
	{
		___connectedNotification_4 = value;
		Il2CppCodeGenWriteBarrier((&___connectedNotification_4), value);
	}

	inline static int32_t get_offset_of_disconnectedNotification_5() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687, ___disconnectedNotification_5)); }
	inline GameObject_t1113636619 * get_disconnectedNotification_5() const { return ___disconnectedNotification_5; }
	inline GameObject_t1113636619 ** get_address_of_disconnectedNotification_5() { return &___disconnectedNotification_5; }
	inline void set_disconnectedNotification_5(GameObject_t1113636619 * value)
	{
		___disconnectedNotification_5 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectedNotification_5), value);
	}

	inline static int32_t get_offset_of_remotesController_7() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687, ___remotesController_7)); }
	inline RemotesController_t1084025038 * get_remotesController_7() const { return ___remotesController_7; }
	inline RemotesController_t1084025038 ** get_address_of_remotesController_7() { return &___remotesController_7; }
	inline void set_remotesController_7(RemotesController_t1084025038 * value)
	{
		___remotesController_7 = value;
		Il2CppCodeGenWriteBarrier((&___remotesController_7), value);
	}

	inline static int32_t get_offset_of_settingsButton_8() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687, ___settingsButton_8)); }
	inline GameObject_t1113636619 * get_settingsButton_8() const { return ___settingsButton_8; }
	inline GameObject_t1113636619 ** get_address_of_settingsButton_8() { return &___settingsButton_8; }
	inline void set_settingsButton_8(GameObject_t1113636619 * value)
	{
		___settingsButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___settingsButton_8), value);
	}
};

struct SettingsManager_t2083239687_StaticFields
{
public:
	// SettingsManager SettingsManager::Instance
	SettingsManager_t2083239687 * ___Instance_6;

public:
	inline static int32_t get_offset_of_Instance_6() { return static_cast<int32_t>(offsetof(SettingsManager_t2083239687_StaticFields, ___Instance_6)); }
	inline SettingsManager_t2083239687 * get_Instance_6() const { return ___Instance_6; }
	inline SettingsManager_t2083239687 ** get_address_of_Instance_6() { return &___Instance_6; }
	inline void set_Instance_6(SettingsManager_t2083239687 * value)
	{
		___Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMANAGER_T2083239687_H
#ifndef MIRAPOINTERMANAGER_T842607068_H
#define MIRAPOINTERMANAGER_T842607068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPointerManager
struct  MiraPointerManager_t842607068  : public MonoBehaviour_t3962482529
{
public:
	// MiraBasePointer MiraPointerManager::_pointer
	MiraBasePointer_t2409431493 * ____pointer_3;
	// UnityEngine.GameObject MiraPointerManager::_pointerGameObject
	GameObject_t1113636619 * ____pointerGameObject_4;

public:
	inline static int32_t get_offset_of__pointer_3() { return static_cast<int32_t>(offsetof(MiraPointerManager_t842607068, ____pointer_3)); }
	inline MiraBasePointer_t2409431493 * get__pointer_3() const { return ____pointer_3; }
	inline MiraBasePointer_t2409431493 ** get_address_of__pointer_3() { return &____pointer_3; }
	inline void set__pointer_3(MiraBasePointer_t2409431493 * value)
	{
		____pointer_3 = value;
		Il2CppCodeGenWriteBarrier((&____pointer_3), value);
	}

	inline static int32_t get_offset_of__pointerGameObject_4() { return static_cast<int32_t>(offsetof(MiraPointerManager_t842607068, ____pointerGameObject_4)); }
	inline GameObject_t1113636619 * get__pointerGameObject_4() const { return ____pointerGameObject_4; }
	inline GameObject_t1113636619 ** get_address_of__pointerGameObject_4() { return &____pointerGameObject_4; }
	inline void set__pointerGameObject_4(GameObject_t1113636619 * value)
	{
		____pointerGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____pointerGameObject_4), value);
	}
};

struct MiraPointerManager_t842607068_StaticFields
{
public:
	// MiraPointerManager MiraPointerManager::_instance
	MiraPointerManager_t842607068 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MiraPointerManager_t842607068_StaticFields, ____instance_2)); }
	inline MiraPointerManager_t842607068 * get__instance_2() const { return ____instance_2; }
	inline MiraPointerManager_t842607068 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(MiraPointerManager_t842607068 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPOINTERMANAGER_T842607068_H
#ifndef MIRAVIEWER_T94414402_H
#define MIRAVIEWER_T94414402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraViewer
struct  MiraViewer_t94414402  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mira.MiraViewer::stereoCamFov
	float ___stereoCamFov_2;
	// UnityEngine.GameObject Mira.MiraViewer::cameraRig
	GameObject_t1113636619 * ___cameraRig_4;
	// UnityEngine.GameObject Mira.MiraViewer::Left_Eye
	GameObject_t1113636619 * ___Left_Eye_5;
	// UnityEngine.GameObject Mira.MiraViewer::Right_Eye
	GameObject_t1113636619 * ___Right_Eye_6;

public:
	inline static int32_t get_offset_of_stereoCamFov_2() { return static_cast<int32_t>(offsetof(MiraViewer_t94414402, ___stereoCamFov_2)); }
	inline float get_stereoCamFov_2() const { return ___stereoCamFov_2; }
	inline float* get_address_of_stereoCamFov_2() { return &___stereoCamFov_2; }
	inline void set_stereoCamFov_2(float value)
	{
		___stereoCamFov_2 = value;
	}

	inline static int32_t get_offset_of_cameraRig_4() { return static_cast<int32_t>(offsetof(MiraViewer_t94414402, ___cameraRig_4)); }
	inline GameObject_t1113636619 * get_cameraRig_4() const { return ___cameraRig_4; }
	inline GameObject_t1113636619 ** get_address_of_cameraRig_4() { return &___cameraRig_4; }
	inline void set_cameraRig_4(GameObject_t1113636619 * value)
	{
		___cameraRig_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_4), value);
	}

	inline static int32_t get_offset_of_Left_Eye_5() { return static_cast<int32_t>(offsetof(MiraViewer_t94414402, ___Left_Eye_5)); }
	inline GameObject_t1113636619 * get_Left_Eye_5() const { return ___Left_Eye_5; }
	inline GameObject_t1113636619 ** get_address_of_Left_Eye_5() { return &___Left_Eye_5; }
	inline void set_Left_Eye_5(GameObject_t1113636619 * value)
	{
		___Left_Eye_5 = value;
		Il2CppCodeGenWriteBarrier((&___Left_Eye_5), value);
	}

	inline static int32_t get_offset_of_Right_Eye_6() { return static_cast<int32_t>(offsetof(MiraViewer_t94414402, ___Right_Eye_6)); }
	inline GameObject_t1113636619 * get_Right_Eye_6() const { return ___Right_Eye_6; }
	inline GameObject_t1113636619 ** get_address_of_Right_Eye_6() { return &___Right_Eye_6; }
	inline void set_Right_Eye_6(GameObject_t1113636619 * value)
	{
		___Right_Eye_6 = value;
		Il2CppCodeGenWriteBarrier((&___Right_Eye_6), value);
	}
};

struct MiraViewer_t94414402_StaticFields
{
public:
	// UnityEngine.GameObject Mira.MiraViewer::viewer
	GameObject_t1113636619 * ___viewer_3;
	// Mira.MiraViewer Mira.MiraViewer::Instance
	MiraViewer_t94414402 * ___Instance_7;

public:
	inline static int32_t get_offset_of_viewer_3() { return static_cast<int32_t>(offsetof(MiraViewer_t94414402_StaticFields, ___viewer_3)); }
	inline GameObject_t1113636619 * get_viewer_3() const { return ___viewer_3; }
	inline GameObject_t1113636619 ** get_address_of_viewer_3() { return &___viewer_3; }
	inline void set_viewer_3(GameObject_t1113636619 * value)
	{
		___viewer_3 = value;
		Il2CppCodeGenWriteBarrier((&___viewer_3), value);
	}

	inline static int32_t get_offset_of_Instance_7() { return static_cast<int32_t>(offsetof(MiraViewer_t94414402_StaticFields, ___Instance_7)); }
	inline MiraViewer_t94414402 * get_Instance_7() const { return ___Instance_7; }
	inline MiraViewer_t94414402 ** get_address_of_Instance_7() { return &___Instance_7; }
	inline void set_Instance_7(MiraViewer_t94414402 * value)
	{
		___Instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAVIEWER_T94414402_H
#ifndef INVERSEGYROCONTROLLER_T592264774_H
#define INVERSEGYROCONTROLLER_T592264774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.InverseGyroController
struct  InverseGyroController_t592264774  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Mira.InverseGyroController::stereoCamRig
	Transform_t3600365921 * ___stereoCamRig_2;

public:
	inline static int32_t get_offset_of_stereoCamRig_2() { return static_cast<int32_t>(offsetof(InverseGyroController_t592264774, ___stereoCamRig_2)); }
	inline Transform_t3600365921 * get_stereoCamRig_2() const { return ___stereoCamRig_2; }
	inline Transform_t3600365921 ** get_address_of_stereoCamRig_2() { return &___stereoCamRig_2; }
	inline void set_stereoCamRig_2(Transform_t3600365921 * value)
	{
		___stereoCamRig_2 = value;
		Il2CppCodeGenWriteBarrier((&___stereoCamRig_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVERSEGYROCONTROLLER_T592264774_H
#ifndef FOLLOWHEADAUTOMATICSCALE_T4137849354_H
#define FOLLOWHEADAUTOMATICSCALE_T4137849354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.FollowHeadAutomaticScale
struct  FollowHeadAutomaticScale_t4137849354  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 Mira.FollowHeadAutomaticScale::headFollowPositionM
	Vector3_t3722313464  ___headFollowPositionM_2;
	// System.Single Mira.FollowHeadAutomaticScale::lerpSpeed
	float ___lerpSpeed_3;
	// UnityEngine.Transform Mira.FollowHeadAutomaticScale::head
	Transform_t3600365921 * ___head_4;

public:
	inline static int32_t get_offset_of_headFollowPositionM_2() { return static_cast<int32_t>(offsetof(FollowHeadAutomaticScale_t4137849354, ___headFollowPositionM_2)); }
	inline Vector3_t3722313464  get_headFollowPositionM_2() const { return ___headFollowPositionM_2; }
	inline Vector3_t3722313464 * get_address_of_headFollowPositionM_2() { return &___headFollowPositionM_2; }
	inline void set_headFollowPositionM_2(Vector3_t3722313464  value)
	{
		___headFollowPositionM_2 = value;
	}

	inline static int32_t get_offset_of_lerpSpeed_3() { return static_cast<int32_t>(offsetof(FollowHeadAutomaticScale_t4137849354, ___lerpSpeed_3)); }
	inline float get_lerpSpeed_3() const { return ___lerpSpeed_3; }
	inline float* get_address_of_lerpSpeed_3() { return &___lerpSpeed_3; }
	inline void set_lerpSpeed_3(float value)
	{
		___lerpSpeed_3 = value;
	}

	inline static int32_t get_offset_of_head_4() { return static_cast<int32_t>(offsetof(FollowHeadAutomaticScale_t4137849354, ___head_4)); }
	inline Transform_t3600365921 * get_head_4() const { return ___head_4; }
	inline Transform_t3600365921 ** get_address_of_head_4() { return &___head_4; }
	inline void set_head_4(Transform_t3600365921 * value)
	{
		___head_4 = value;
		Il2CppCodeGenWriteBarrier((&___head_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWHEADAUTOMATICSCALE_T4137849354_H
#ifndef GYROCONTROLLER_T3407913731_H
#define GYROCONTROLLER_T3407913731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroController
struct  GyroController_t3407913731  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Quaternion GyroController::gyroRotation
	Quaternion_t2301928331  ___gyroRotation_3;
	// System.Boolean GyroController::gyroEnabled
	bool ___gyroEnabled_4;
	// System.Single GyroController::lowPassFilterFactor
	float ___lowPassFilterFactor_5;
	// UnityEngine.Quaternion GyroController::frontCamera
	Quaternion_t2301928331  ___frontCamera_6;
	// UnityEngine.Quaternion GyroController::cameraBase
	Quaternion_t2301928331  ___cameraBase_7;
	// UnityEngine.Quaternion GyroController::calibration
	Quaternion_t2301928331  ___calibration_8;
	// UnityEngine.Quaternion GyroController::baseOrientation
	Quaternion_t2301928331  ___baseOrientation_9;
	// UnityEngine.Quaternion GyroController::referenceRotation
	Quaternion_t2301928331  ___referenceRotation_10;
	// System.Boolean GyroController::inverseGyroSetup
	bool ___inverseGyroSetup_11;
	// System.Boolean GyroController::debugGUI
	bool ___debugGUI_12;
	// System.Boolean GyroController::useFrontCamera
	bool ___useFrontCamera_13;
	// System.Boolean GyroController::miraRemoteMode
	bool ___miraRemoteMode_14;
	// UnityEngine.Quaternion GyroController::rotationOffset
	Quaternion_t2301928331  ___rotationOffset_15;

public:
	inline static int32_t get_offset_of_gyroRotation_3() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___gyroRotation_3)); }
	inline Quaternion_t2301928331  get_gyroRotation_3() const { return ___gyroRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_gyroRotation_3() { return &___gyroRotation_3; }
	inline void set_gyroRotation_3(Quaternion_t2301928331  value)
	{
		___gyroRotation_3 = value;
	}

	inline static int32_t get_offset_of_gyroEnabled_4() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___gyroEnabled_4)); }
	inline bool get_gyroEnabled_4() const { return ___gyroEnabled_4; }
	inline bool* get_address_of_gyroEnabled_4() { return &___gyroEnabled_4; }
	inline void set_gyroEnabled_4(bool value)
	{
		___gyroEnabled_4 = value;
	}

	inline static int32_t get_offset_of_lowPassFilterFactor_5() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___lowPassFilterFactor_5)); }
	inline float get_lowPassFilterFactor_5() const { return ___lowPassFilterFactor_5; }
	inline float* get_address_of_lowPassFilterFactor_5() { return &___lowPassFilterFactor_5; }
	inline void set_lowPassFilterFactor_5(float value)
	{
		___lowPassFilterFactor_5 = value;
	}

	inline static int32_t get_offset_of_frontCamera_6() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___frontCamera_6)); }
	inline Quaternion_t2301928331  get_frontCamera_6() const { return ___frontCamera_6; }
	inline Quaternion_t2301928331 * get_address_of_frontCamera_6() { return &___frontCamera_6; }
	inline void set_frontCamera_6(Quaternion_t2301928331  value)
	{
		___frontCamera_6 = value;
	}

	inline static int32_t get_offset_of_cameraBase_7() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___cameraBase_7)); }
	inline Quaternion_t2301928331  get_cameraBase_7() const { return ___cameraBase_7; }
	inline Quaternion_t2301928331 * get_address_of_cameraBase_7() { return &___cameraBase_7; }
	inline void set_cameraBase_7(Quaternion_t2301928331  value)
	{
		___cameraBase_7 = value;
	}

	inline static int32_t get_offset_of_calibration_8() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___calibration_8)); }
	inline Quaternion_t2301928331  get_calibration_8() const { return ___calibration_8; }
	inline Quaternion_t2301928331 * get_address_of_calibration_8() { return &___calibration_8; }
	inline void set_calibration_8(Quaternion_t2301928331  value)
	{
		___calibration_8 = value;
	}

	inline static int32_t get_offset_of_baseOrientation_9() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___baseOrientation_9)); }
	inline Quaternion_t2301928331  get_baseOrientation_9() const { return ___baseOrientation_9; }
	inline Quaternion_t2301928331 * get_address_of_baseOrientation_9() { return &___baseOrientation_9; }
	inline void set_baseOrientation_9(Quaternion_t2301928331  value)
	{
		___baseOrientation_9 = value;
	}

	inline static int32_t get_offset_of_referenceRotation_10() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___referenceRotation_10)); }
	inline Quaternion_t2301928331  get_referenceRotation_10() const { return ___referenceRotation_10; }
	inline Quaternion_t2301928331 * get_address_of_referenceRotation_10() { return &___referenceRotation_10; }
	inline void set_referenceRotation_10(Quaternion_t2301928331  value)
	{
		___referenceRotation_10 = value;
	}

	inline static int32_t get_offset_of_inverseGyroSetup_11() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___inverseGyroSetup_11)); }
	inline bool get_inverseGyroSetup_11() const { return ___inverseGyroSetup_11; }
	inline bool* get_address_of_inverseGyroSetup_11() { return &___inverseGyroSetup_11; }
	inline void set_inverseGyroSetup_11(bool value)
	{
		___inverseGyroSetup_11 = value;
	}

	inline static int32_t get_offset_of_debugGUI_12() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___debugGUI_12)); }
	inline bool get_debugGUI_12() const { return ___debugGUI_12; }
	inline bool* get_address_of_debugGUI_12() { return &___debugGUI_12; }
	inline void set_debugGUI_12(bool value)
	{
		___debugGUI_12 = value;
	}

	inline static int32_t get_offset_of_useFrontCamera_13() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___useFrontCamera_13)); }
	inline bool get_useFrontCamera_13() const { return ___useFrontCamera_13; }
	inline bool* get_address_of_useFrontCamera_13() { return &___useFrontCamera_13; }
	inline void set_useFrontCamera_13(bool value)
	{
		___useFrontCamera_13 = value;
	}

	inline static int32_t get_offset_of_miraRemoteMode_14() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___miraRemoteMode_14)); }
	inline bool get_miraRemoteMode_14() const { return ___miraRemoteMode_14; }
	inline bool* get_address_of_miraRemoteMode_14() { return &___miraRemoteMode_14; }
	inline void set_miraRemoteMode_14(bool value)
	{
		___miraRemoteMode_14 = value;
	}

	inline static int32_t get_offset_of_rotationOffset_15() { return static_cast<int32_t>(offsetof(GyroController_t3407913731, ___rotationOffset_15)); }
	inline Quaternion_t2301928331  get_rotationOffset_15() const { return ___rotationOffset_15; }
	inline Quaternion_t2301928331 * get_address_of_rotationOffset_15() { return &___rotationOffset_15; }
	inline void set_rotationOffset_15(Quaternion_t2301928331  value)
	{
		___rotationOffset_15 = value;
	}
};

struct GyroController_t3407913731_StaticFields
{
public:
	// GyroController GyroController::instance
	GyroController_t3407913731 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GyroController_t3407913731_StaticFields, ___instance_2)); }
	inline GyroController_t3407913731 * get_instance_2() const { return ___instance_2; }
	inline GyroController_t3407913731 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GyroController_t3407913731 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROCONTROLLER_T3407913731_H
#ifndef ROTATIONALTRACKINGMANAGER_T1723746904_H
#define ROTATIONALTRACKINGMANAGER_T1723746904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager
struct  RotationalTrackingManager_t1723746904  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Mira.RotationalTrackingManager::mainCamera
	Transform_t3600365921 * ___mainCamera_2;
	// UnityEngine.Transform Mira.RotationalTrackingManager::cameraRig
	Transform_t3600365921 * ___cameraRig_3;
	// System.Boolean Mira.RotationalTrackingManager::isRotational
	bool ___isRotational_4;
	// System.Collections.Generic.Queue`1<UnityEngine.Vector3> Mira.RotationalTrackingManager::positionBuffer
	Queue_1_t3568572958 * ___positionBuffer_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Quaternion> Mira.RotationalTrackingManager::rotationBuffer
	Queue_1_t2148187825 * ___rotationBuffer_6;
	// System.Single Mira.RotationalTrackingManager::delay
	float ___delay_7;
	// System.Int32 Mira.RotationalTrackingManager::bufferSize
	int32_t ___bufferSize_8;
	// System.Int32 Mira.RotationalTrackingManager::bufferDiscardLast
	int32_t ___bufferDiscardLast_9;
	// UnityEngine.Vector3 Mira.RotationalTrackingManager::camRigStartPosition
	Vector3_t3722313464  ___camRigStartPosition_10;
	// System.Boolean Mira.RotationalTrackingManager::isSpectator
	bool ___isSpectator_11;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___mainCamera_2)); }
	inline Transform_t3600365921 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Transform_t3600365921 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Transform_t3600365921 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}

	inline static int32_t get_offset_of_cameraRig_3() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___cameraRig_3)); }
	inline Transform_t3600365921 * get_cameraRig_3() const { return ___cameraRig_3; }
	inline Transform_t3600365921 ** get_address_of_cameraRig_3() { return &___cameraRig_3; }
	inline void set_cameraRig_3(Transform_t3600365921 * value)
	{
		___cameraRig_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_3), value);
	}

	inline static int32_t get_offset_of_isRotational_4() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___isRotational_4)); }
	inline bool get_isRotational_4() const { return ___isRotational_4; }
	inline bool* get_address_of_isRotational_4() { return &___isRotational_4; }
	inline void set_isRotational_4(bool value)
	{
		___isRotational_4 = value;
	}

	inline static int32_t get_offset_of_positionBuffer_5() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___positionBuffer_5)); }
	inline Queue_1_t3568572958 * get_positionBuffer_5() const { return ___positionBuffer_5; }
	inline Queue_1_t3568572958 ** get_address_of_positionBuffer_5() { return &___positionBuffer_5; }
	inline void set_positionBuffer_5(Queue_1_t3568572958 * value)
	{
		___positionBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___positionBuffer_5), value);
	}

	inline static int32_t get_offset_of_rotationBuffer_6() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___rotationBuffer_6)); }
	inline Queue_1_t2148187825 * get_rotationBuffer_6() const { return ___rotationBuffer_6; }
	inline Queue_1_t2148187825 ** get_address_of_rotationBuffer_6() { return &___rotationBuffer_6; }
	inline void set_rotationBuffer_6(Queue_1_t2148187825 * value)
	{
		___rotationBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotationBuffer_6), value);
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_bufferSize_8() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___bufferSize_8)); }
	inline int32_t get_bufferSize_8() const { return ___bufferSize_8; }
	inline int32_t* get_address_of_bufferSize_8() { return &___bufferSize_8; }
	inline void set_bufferSize_8(int32_t value)
	{
		___bufferSize_8 = value;
	}

	inline static int32_t get_offset_of_bufferDiscardLast_9() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___bufferDiscardLast_9)); }
	inline int32_t get_bufferDiscardLast_9() const { return ___bufferDiscardLast_9; }
	inline int32_t* get_address_of_bufferDiscardLast_9() { return &___bufferDiscardLast_9; }
	inline void set_bufferDiscardLast_9(int32_t value)
	{
		___bufferDiscardLast_9 = value;
	}

	inline static int32_t get_offset_of_camRigStartPosition_10() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___camRigStartPosition_10)); }
	inline Vector3_t3722313464  get_camRigStartPosition_10() const { return ___camRigStartPosition_10; }
	inline Vector3_t3722313464 * get_address_of_camRigStartPosition_10() { return &___camRigStartPosition_10; }
	inline void set_camRigStartPosition_10(Vector3_t3722313464  value)
	{
		___camRigStartPosition_10 = value;
	}

	inline static int32_t get_offset_of_isSpectator_11() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1723746904, ___isSpectator_11)); }
	inline bool get_isSpectator_11() const { return ___isSpectator_11; }
	inline bool* get_address_of_isSpectator_11() { return &___isSpectator_11; }
	inline void set_isSpectator_11(bool value)
	{
		___isSpectator_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALTRACKINGMANAGER_T1723746904_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef MIRAWIKITUDEMANAGER_T2444513627_H
#define MIRAWIKITUDEMANAGER_T2444513627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraWikitudeManager
struct  MiraWikitudeManager_t2444513627  : public TransformOverride_t3828400655
{
public:
	// Wikitude.WikitudeCamera MiraWikitudeManager::ArCam
	WikitudeCamera_t2188881370 * ___ArCam_2;
	// UnityEngine.GameObject MiraWikitudeManager::headOrientation3DOF
	GameObject_t1113636619 * ___headOrientation3DOF_3;
	// Wikitude.ImageTrackable MiraWikitudeManager::imageTracker
	ImageTrackable_t2462741734 * ___imageTracker_4;
	// System.Single MiraWikitudeManager::scaleMultiplier
	float ___scaleMultiplier_6;
	// UnityEngine.Quaternion MiraWikitudeManager::rotationalOffset
	Quaternion_t2301928331  ___rotationalOffset_7;
	// UnityEngine.Vector3 MiraWikitudeManager::positionalOffset
	Vector3_t3722313464  ___positionalOffset_8;
	// Mira.RotationalTrackingManager MiraWikitudeManager::rotationalTracking
	RotationalTrackingManager_t1723746904 * ___rotationalTracking_9;
	// System.Boolean MiraWikitudeManager::flag
	bool ___flag_10;

public:
	inline static int32_t get_offset_of_ArCam_2() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___ArCam_2)); }
	inline WikitudeCamera_t2188881370 * get_ArCam_2() const { return ___ArCam_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_ArCam_2() { return &___ArCam_2; }
	inline void set_ArCam_2(WikitudeCamera_t2188881370 * value)
	{
		___ArCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___ArCam_2), value);
	}

	inline static int32_t get_offset_of_headOrientation3DOF_3() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___headOrientation3DOF_3)); }
	inline GameObject_t1113636619 * get_headOrientation3DOF_3() const { return ___headOrientation3DOF_3; }
	inline GameObject_t1113636619 ** get_address_of_headOrientation3DOF_3() { return &___headOrientation3DOF_3; }
	inline void set_headOrientation3DOF_3(GameObject_t1113636619 * value)
	{
		___headOrientation3DOF_3 = value;
		Il2CppCodeGenWriteBarrier((&___headOrientation3DOF_3), value);
	}

	inline static int32_t get_offset_of_imageTracker_4() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___imageTracker_4)); }
	inline ImageTrackable_t2462741734 * get_imageTracker_4() const { return ___imageTracker_4; }
	inline ImageTrackable_t2462741734 ** get_address_of_imageTracker_4() { return &___imageTracker_4; }
	inline void set_imageTracker_4(ImageTrackable_t2462741734 * value)
	{
		___imageTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageTracker_4), value);
	}

	inline static int32_t get_offset_of_scaleMultiplier_6() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___scaleMultiplier_6)); }
	inline float get_scaleMultiplier_6() const { return ___scaleMultiplier_6; }
	inline float* get_address_of_scaleMultiplier_6() { return &___scaleMultiplier_6; }
	inline void set_scaleMultiplier_6(float value)
	{
		___scaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_rotationalOffset_7() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___rotationalOffset_7)); }
	inline Quaternion_t2301928331  get_rotationalOffset_7() const { return ___rotationalOffset_7; }
	inline Quaternion_t2301928331 * get_address_of_rotationalOffset_7() { return &___rotationalOffset_7; }
	inline void set_rotationalOffset_7(Quaternion_t2301928331  value)
	{
		___rotationalOffset_7 = value;
	}

	inline static int32_t get_offset_of_positionalOffset_8() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___positionalOffset_8)); }
	inline Vector3_t3722313464  get_positionalOffset_8() const { return ___positionalOffset_8; }
	inline Vector3_t3722313464 * get_address_of_positionalOffset_8() { return &___positionalOffset_8; }
	inline void set_positionalOffset_8(Vector3_t3722313464  value)
	{
		___positionalOffset_8 = value;
	}

	inline static int32_t get_offset_of_rotationalTracking_9() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___rotationalTracking_9)); }
	inline RotationalTrackingManager_t1723746904 * get_rotationalTracking_9() const { return ___rotationalTracking_9; }
	inline RotationalTrackingManager_t1723746904 ** get_address_of_rotationalTracking_9() { return &___rotationalTracking_9; }
	inline void set_rotationalTracking_9(RotationalTrackingManager_t1723746904 * value)
	{
		___rotationalTracking_9 = value;
		Il2CppCodeGenWriteBarrier((&___rotationalTracking_9), value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627, ___flag_10)); }
	inline bool get_flag_10() const { return ___flag_10; }
	inline bool* get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(bool value)
	{
		___flag_10 = value;
	}
};

struct MiraWikitudeManager_t2444513627_StaticFields
{
public:
	// MiraWikitudeManager MiraWikitudeManager::instance
	MiraWikitudeManager_t2444513627 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2444513627_StaticFields, ___instance_5)); }
	inline MiraWikitudeManager_t2444513627 * get_instance_5() const { return ___instance_5; }
	inline MiraWikitudeManager_t2444513627 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(MiraWikitudeManager_t2444513627 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAWIKITUDEMANAGER_T2444513627_H
#ifndef BASERAYCASTER_T4150874583_H
#define BASERAYCASTER_T4150874583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t4150874583  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T4150874583_H
#ifndef POINTERINPUTMODULE_T3453173740_H
#define POINTERINPUTMODULE_T3453173740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t3453173740  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t2696614423 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_PointerData_12)); }
	inline Dictionary_2_t2696614423 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t2696614423 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t2696614423 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_MouseState_13)); }
	inline MouseState_t384203932 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t384203932 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T3453173740_H
#ifndef MIRABASERAYCASTER_T368298697_H
#define MIRABASERAYCASTER_T368298697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster
struct  MiraBaseRaycaster_t368298697  : public BaseRaycaster_t4150874583
{
public:
	// MiraBaseRaycaster/RaycastStyle MiraBaseRaycaster::raycastStyle
	int32_t ___raycastStyle_2;
	// UnityEngine.Ray MiraBaseRaycaster::lastray
	Ray_t3785851493  ___lastray_3;

public:
	inline static int32_t get_offset_of_raycastStyle_2() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t368298697, ___raycastStyle_2)); }
	inline int32_t get_raycastStyle_2() const { return ___raycastStyle_2; }
	inline int32_t* get_address_of_raycastStyle_2() { return &___raycastStyle_2; }
	inline void set_raycastStyle_2(int32_t value)
	{
		___raycastStyle_2 = value;
	}

	inline static int32_t get_offset_of_lastray_3() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t368298697, ___lastray_3)); }
	inline Ray_t3785851493  get_lastray_3() const { return ___lastray_3; }
	inline Ray_t3785851493 * get_address_of_lastray_3() { return &___lastray_3; }
	inline void set_lastray_3(Ray_t3785851493  value)
	{
		___lastray_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASERAYCASTER_T368298697_H
#ifndef MIRAGRAPHICRAYCAST_T892254387_H
#define MIRAGRAPHICRAYCAST_T892254387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraGraphicRaycast
struct  MiraGraphicRaycast_t892254387  : public MiraBaseRaycaster_t368298697
{
public:
	// UnityEngine.LayerMask MiraGraphicRaycast::blockingMask
	LayerMask_t3493934918  ___blockingMask_5;
	// System.Boolean MiraGraphicRaycast::ignoreReversedGraphics
	bool ___ignoreReversedGraphics_6;
	// UnityEngine.UI.GraphicRaycaster/BlockingObjects MiraGraphicRaycast::blockingObjects
	int32_t ___blockingObjects_7;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::raycastResults
	List_1_t3132410353 * ___raycastResults_8;
	// UnityEngine.Canvas MiraGraphicRaycast::_canvas
	Canvas_t3310196443 * ____canvas_9;

public:
	inline static int32_t get_offset_of_blockingMask_5() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387, ___blockingMask_5)); }
	inline LayerMask_t3493934918  get_blockingMask_5() const { return ___blockingMask_5; }
	inline LayerMask_t3493934918 * get_address_of_blockingMask_5() { return &___blockingMask_5; }
	inline void set_blockingMask_5(LayerMask_t3493934918  value)
	{
		___blockingMask_5 = value;
	}

	inline static int32_t get_offset_of_ignoreReversedGraphics_6() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387, ___ignoreReversedGraphics_6)); }
	inline bool get_ignoreReversedGraphics_6() const { return ___ignoreReversedGraphics_6; }
	inline bool* get_address_of_ignoreReversedGraphics_6() { return &___ignoreReversedGraphics_6; }
	inline void set_ignoreReversedGraphics_6(bool value)
	{
		___ignoreReversedGraphics_6 = value;
	}

	inline static int32_t get_offset_of_blockingObjects_7() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387, ___blockingObjects_7)); }
	inline int32_t get_blockingObjects_7() const { return ___blockingObjects_7; }
	inline int32_t* get_address_of_blockingObjects_7() { return &___blockingObjects_7; }
	inline void set_blockingObjects_7(int32_t value)
	{
		___blockingObjects_7 = value;
	}

	inline static int32_t get_offset_of_raycastResults_8() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387, ___raycastResults_8)); }
	inline List_1_t3132410353 * get_raycastResults_8() const { return ___raycastResults_8; }
	inline List_1_t3132410353 ** get_address_of_raycastResults_8() { return &___raycastResults_8; }
	inline void set_raycastResults_8(List_1_t3132410353 * value)
	{
		___raycastResults_8 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_8), value);
	}

	inline static int32_t get_offset_of__canvas_9() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387, ____canvas_9)); }
	inline Canvas_t3310196443 * get__canvas_9() const { return ____canvas_9; }
	inline Canvas_t3310196443 ** get_address_of__canvas_9() { return &____canvas_9; }
	inline void set__canvas_9(Canvas_t3310196443 * value)
	{
		____canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_9), value);
	}
};

struct MiraGraphicRaycast_t892254387_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::sortedGraphics
	List_1_t3132410353 * ___sortedGraphics_10;
	// System.Comparison`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::<>f__am$cache0
	Comparison_1_t1435266790 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_sortedGraphics_10() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387_StaticFields, ___sortedGraphics_10)); }
	inline List_1_t3132410353 * get_sortedGraphics_10() const { return ___sortedGraphics_10; }
	inline List_1_t3132410353 ** get_address_of_sortedGraphics_10() { return &___sortedGraphics_10; }
	inline void set_sortedGraphics_10(List_1_t3132410353 * value)
	{
		___sortedGraphics_10 = value;
		Il2CppCodeGenWriteBarrier((&___sortedGraphics_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t892254387_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Comparison_1_t1435266790 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Comparison_1_t1435266790 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Comparison_1_t1435266790 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAGRAPHICRAYCAST_T892254387_H
#ifndef MIRAINPUTMODULE_T1698371845_H
#define MIRAINPUTMODULE_T1698371845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MiraInputModule
struct  MiraInputModule_t1698371845  : public PointerInputModule_t3453173740
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.MiraInputModule::pointerData
	PointerEventData_t3807901092 * ___pointerData_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::lastPose
	Vector2_t2156229523  ___lastPose_15;
	// System.Boolean UnityEngine.EventSystems.MiraInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_16;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_PrevActionTime
	float ___m_PrevActionTime_17;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_LastMoveVector
	Vector2_t2156229523  ___m_LastMoveVector_18;
	// System.Int32 UnityEngine.EventSystems.MiraInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_20;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_21;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_22;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_23;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_24;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_CancelButton
	String_t* ___m_CancelButton_25;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_26;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_RepeatDelay
	float ___m_RepeatDelay_27;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.MiraInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_28;

public:
	inline static int32_t get_offset_of_pointerData_14() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___pointerData_14)); }
	inline PointerEventData_t3807901092 * get_pointerData_14() const { return ___pointerData_14; }
	inline PointerEventData_t3807901092 ** get_address_of_pointerData_14() { return &___pointerData_14; }
	inline void set_pointerData_14(PointerEventData_t3807901092 * value)
	{
		___pointerData_14 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_14), value);
	}

	inline static int32_t get_offset_of_lastPose_15() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___lastPose_15)); }
	inline Vector2_t2156229523  get_lastPose_15() const { return ___lastPose_15; }
	inline Vector2_t2156229523 * get_address_of_lastPose_15() { return &___lastPose_15; }
	inline void set_lastPose_15(Vector2_t2156229523  value)
	{
		___lastPose_15 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_16() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_ForceModuleActive_16)); }
	inline bool get_m_ForceModuleActive_16() const { return ___m_ForceModuleActive_16; }
	inline bool* get_address_of_m_ForceModuleActive_16() { return &___m_ForceModuleActive_16; }
	inline void set_m_ForceModuleActive_16(bool value)
	{
		___m_ForceModuleActive_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevActionTime_17() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_PrevActionTime_17)); }
	inline float get_m_PrevActionTime_17() const { return ___m_PrevActionTime_17; }
	inline float* get_address_of_m_PrevActionTime_17() { return &___m_PrevActionTime_17; }
	inline void set_m_PrevActionTime_17(float value)
	{
		___m_PrevActionTime_17 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_18() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_LastMoveVector_18)); }
	inline Vector2_t2156229523  get_m_LastMoveVector_18() const { return ___m_LastMoveVector_18; }
	inline Vector2_t2156229523 * get_address_of_m_LastMoveVector_18() { return &___m_LastMoveVector_18; }
	inline void set_m_LastMoveVector_18(Vector2_t2156229523  value)
	{
		___m_LastMoveVector_18 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_19() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_ConsecutiveMoveCount_19)); }
	inline int32_t get_m_ConsecutiveMoveCount_19() const { return ___m_ConsecutiveMoveCount_19; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_19() { return &___m_ConsecutiveMoveCount_19; }
	inline void set_m_ConsecutiveMoveCount_19(int32_t value)
	{
		___m_ConsecutiveMoveCount_19 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_20() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_LastMousePosition_20)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_20() const { return ___m_LastMousePosition_20; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_20() { return &___m_LastMousePosition_20; }
	inline void set_m_LastMousePosition_20(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_20 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_21() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_MousePosition_21)); }
	inline Vector2_t2156229523  get_m_MousePosition_21() const { return ___m_MousePosition_21; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_21() { return &___m_MousePosition_21; }
	inline void set_m_MousePosition_21(Vector2_t2156229523  value)
	{
		___m_MousePosition_21 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_22() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_HorizontalAxis_22)); }
	inline String_t* get_m_HorizontalAxis_22() const { return ___m_HorizontalAxis_22; }
	inline String_t** get_address_of_m_HorizontalAxis_22() { return &___m_HorizontalAxis_22; }
	inline void set_m_HorizontalAxis_22(String_t* value)
	{
		___m_HorizontalAxis_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_22), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_23() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_VerticalAxis_23)); }
	inline String_t* get_m_VerticalAxis_23() const { return ___m_VerticalAxis_23; }
	inline String_t** get_address_of_m_VerticalAxis_23() { return &___m_VerticalAxis_23; }
	inline void set_m_VerticalAxis_23(String_t* value)
	{
		___m_VerticalAxis_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_23), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_24() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_SubmitButton_24)); }
	inline String_t* get_m_SubmitButton_24() const { return ___m_SubmitButton_24; }
	inline String_t** get_address_of_m_SubmitButton_24() { return &___m_SubmitButton_24; }
	inline void set_m_SubmitButton_24(String_t* value)
	{
		___m_SubmitButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_24), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_25() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_CancelButton_25)); }
	inline String_t* get_m_CancelButton_25() const { return ___m_CancelButton_25; }
	inline String_t** get_address_of_m_CancelButton_25() { return &___m_CancelButton_25; }
	inline void set_m_CancelButton_25(String_t* value)
	{
		___m_CancelButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_25), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_26() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_InputActionsPerSecond_26)); }
	inline float get_m_InputActionsPerSecond_26() const { return ___m_InputActionsPerSecond_26; }
	inline float* get_address_of_m_InputActionsPerSecond_26() { return &___m_InputActionsPerSecond_26; }
	inline void set_m_InputActionsPerSecond_26(float value)
	{
		___m_InputActionsPerSecond_26 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_27() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_RepeatDelay_27)); }
	inline float get_m_RepeatDelay_27() const { return ___m_RepeatDelay_27; }
	inline float* get_address_of_m_RepeatDelay_27() { return &___m_RepeatDelay_27; }
	inline void set_m_RepeatDelay_27(float value)
	{
		___m_RepeatDelay_27 = value;
	}

	inline static int32_t get_offset_of_m_MouseState_28() { return static_cast<int32_t>(offsetof(MiraInputModule_t1698371845, ___m_MouseState_28)); }
	inline MouseState_t384203932 * get_m_MouseState_28() const { return ___m_MouseState_28; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_28() { return &___m_MouseState_28; }
	inline void set_m_MouseState_28(MouseState_t384203932 * value)
	{
		___m_MouseState_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAINPUTMODULE_T1698371845_H
#ifndef MIRAPHYSICSRAYCAST_T1299684474_H
#define MIRAPHYSICSRAYCAST_T1299684474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsRaycast
struct  MiraPhysicsRaycast_t1299684474  : public MiraBaseRaycaster_t368298697
{
public:
	// UnityEngine.Camera MiraPhysicsRaycast::_EventCamera
	Camera_t4157153871 * ____EventCamera_5;
	// UnityEngine.LayerMask MiraPhysicsRaycast::_EventMask
	LayerMask_t3493934918  ____EventMask_6;

public:
	inline static int32_t get_offset_of__EventCamera_5() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1299684474, ____EventCamera_5)); }
	inline Camera_t4157153871 * get__EventCamera_5() const { return ____EventCamera_5; }
	inline Camera_t4157153871 ** get_address_of__EventCamera_5() { return &____EventCamera_5; }
	inline void set__EventCamera_5(Camera_t4157153871 * value)
	{
		____EventCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&____EventCamera_5), value);
	}

	inline static int32_t get_offset_of__EventMask_6() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1299684474, ____EventMask_6)); }
	inline LayerMask_t3493934918  get__EventMask_6() const { return ____EventMask_6; }
	inline LayerMask_t3493934918 * get_address_of__EventMask_6() { return &____EventMask_6; }
	inline void set__EventMask_6(LayerMask_t3493934918  value)
	{
		____EventMask_6 = value;
	}
};

struct MiraPhysicsRaycast_t1299684474_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.RaycastHit> MiraPhysicsRaycast::<>f__am$cache0
	Comparison_1_t830933145 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1299684474_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Comparison_1_t830933145 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Comparison_1_t830933145 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Comparison_1_t830933145 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSRAYCAST_T1299684474_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (MRRemoteMotionCallback_t366018504), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t249708700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t249708700::get_offset_of_bluetoothState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t1357687005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[1] = 
{
	U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t1357687005::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974::get_offset_of_action_0(),
	U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974::get_offset_of_identifier_1(),
	U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t3008845974::get_offset_of_discoveryException_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441::get_offset_of_remote_0(),
	U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441::get_offset_of_action_1(),
	U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t300449441::get_offset_of_identifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t685188581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[2] = 
{
	U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t685188581::get_offset_of_action_0(),
	U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t685188581::get_offset_of_identifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (U3CRemoteRefreshU3Ec__AnonStorey5_t769484850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[3] = 
{
	U3CRemoteRefreshU3Ec__AnonStorey5_t769484850::get_offset_of_remote_0(),
	U3CRemoteRefreshU3Ec__AnonStorey5_t769484850::get_offset_of_action_1(),
	U3CRemoteRefreshU3Ec__AnonStorey5_t769484850::get_offset_of_identifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (Remote_t1018512123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[11] = 
{
	Remote_t1018512123::get_offset_of_OnRefresh_5(),
	Remote_t1018512123::get_offset_of_identifier_6(),
	Remote_t1018512123::get_offset_of_U3CnameU3Ek__BackingField_7(),
	Remote_t1018512123::get_offset_of_U3CproductNameU3Ek__BackingField_8(),
	Remote_t1018512123::get_offset_of_U3CserialNumberU3Ek__BackingField_9(),
	Remote_t1018512123::get_offset_of_U3ChardwareIdentifierU3Ek__BackingField_10(),
	Remote_t1018512123::get_offset_of_U3CfirmwareVersionU3Ek__BackingField_11(),
	Remote_t1018512123::get_offset_of_U3CbatteryPercentageU3Ek__BackingField_12(),
	Remote_t1018512123::get_offset_of_U3CrssiU3Ek__BackingField_13(),
	Remote_t1018512123::get_offset_of_U3CisConnectedU3Ek__BackingField_14(),
	Remote_t1018512123::get_offset_of_U3CisPreferredU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (RemoteRefreshedEventHandler_t1529686812), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (RemoteAxisInput_t1550365229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[2] = 
{
	RemoteAxisInput_t1550365229::get_offset_of_OnValueChanged_0(),
	RemoteAxisInput_t1550365229::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (RemoteAxisInputEventHandler_t4129402086), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (RemoteBase_t3398252275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	RemoteBase_t3398252275::get_offset_of_U3CmenuButtonU3Ek__BackingField_0(),
	RemoteBase_t3398252275::get_offset_of_U3ChomeButtonU3Ek__BackingField_1(),
	RemoteBase_t3398252275::get_offset_of_U3CtriggerU3Ek__BackingField_2(),
	RemoteBase_t3398252275::get_offset_of_U3CtouchPadU3Ek__BackingField_3(),
	RemoteBase_t3398252275::get_offset_of_U3CmotionU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (RemoteButtonInput_t3750858021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[11] = 
{
	RemoteButtonInput_t3750858021::get_offset_of_OnPressChanged_0(),
	RemoteButtonInput_t3750858021::get_offset_of_OnHeldState_1(),
	RemoteButtonInput_t3750858021::get_offset_of_OnPresseddState_2(),
	RemoteButtonInput_t3750858021::get_offset_of_OnReleasedState_3(),
	RemoteButtonInput_t3750858021::get_offset_of_m_PrevFrameCount_4(),
	RemoteButtonInput_t3750858021::get_offset_of_m_PrevState_5(),
	RemoteButtonInput_t3750858021::get_offset_of_m_State_6(),
	RemoteButtonInput_t3750858021::get_offset_of__isPressed_7(),
	RemoteButtonInput_t3750858021::get_offset_of__onPressed_8(),
	RemoteButtonInput_t3750858021::get_offset_of__onReleased_9(),
	RemoteButtonInput_t3750858021::get_offset_of__onHeld_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (RemoteButtonInputEventHandler_t2005612063), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (BluetoothState_t589450068)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2214[7] = 
{
	BluetoothState_t589450068::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (RemoteManager_t1941324022), -1, sizeof(RemoteManager_t1941324022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2215[7] = 
{
	RemoteManager_t1941324022::get_offset_of_OnRemoteConnected_2(),
	RemoteManager_t1941324022::get_offset_of_OnRemoteDisconnected_3(),
	RemoteManager_t1941324022_StaticFields::get_offset_of_Instance_4(),
	RemoteManager_t1941324022::get_offset_of_U3CisStartedU3Ek__BackingField_5(),
	RemoteManager_t1941324022::get_offset_of_U3CisDiscoveringRemotesU3Ek__BackingField_6(),
	RemoteManager_t1941324022::get_offset_of__connectedRemote_7(),
	RemoteManager_t1941324022::get_offset_of__discoveredRemotes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (RemoteConnectedEventHandler_t306622352), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (RemoteDisconnectedEventHandler_t2506463716), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1952101060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[2] = 
{
	U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1952101060::get_offset_of_action_0(),
	U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1952101060::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (U3CConnectRemoteU3Ec__AnonStorey1_t2642683055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	U3CConnectRemoteU3Ec__AnonStorey1_t2642683055::get_offset_of_remote_0(),
	U3CConnectRemoteU3Ec__AnonStorey1_t2642683055::get_offset_of_action_1(),
	U3CConnectRemoteU3Ec__AnonStorey1_t2642683055::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t620331307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[2] = 
{
	U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t620331307::get_offset_of_action_0(),
	U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t620331307::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (RemoteMotionInput_t177155726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[4] = 
{
	RemoteMotionInput_t177155726::get_offset_of_OnValueChanged_2(),
	RemoteMotionInput_t177155726::get_offset_of_U3CorientationU3Ek__BackingField_3(),
	RemoteMotionInput_t177155726::get_offset_of_U3CaccelerationU3Ek__BackingField_4(),
	RemoteMotionInput_t177155726::get_offset_of_U3CrotationRateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (RemoteMotionInputEventHandler_t3928406934), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (RemoteMotionSensorInput_t407189685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[4] = 
{
	RemoteMotionSensorInput_t407189685::get_offset_of_OnValueChanged_0(),
	RemoteMotionSensorInput_t407189685::get_offset_of_U3CxU3Ek__BackingField_1(),
	RemoteMotionSensorInput_t407189685::get_offset_of_U3CyU3Ek__BackingField_2(),
	RemoteMotionSensorInput_t407189685::get_offset_of_U3CzU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (RemoteMotionSensorInputEventHandler_t2314235825), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (RemoteOrientationInput_t714724027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[4] = 
{
	RemoteOrientationInput_t714724027::get_offset_of_OnValueChanged_0(),
	RemoteOrientationInput_t714724027::get_offset_of_U3CpitchU3Ek__BackingField_1(),
	RemoteOrientationInput_t714724027::get_offset_of_U3CyawU3Ek__BackingField_2(),
	RemoteOrientationInput_t714724027::get_offset_of_U3CrollU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (RemoteOrientationInputEventHandler_t1727624703), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (RemotesController_t1084025038), -1, sizeof(RemotesController_t1084025038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	RemotesController_t1084025038::get_offset_of_remoteLabels_2(),
	RemotesController_t1084025038::get_offset_of_defaultColor_3(),
	RemotesController_t1084025038::get_offset_of_highlightColor_4(),
	RemotesController_t1084025038_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (RemoteTouchInput_t3055303244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[2] = 
{
	RemoteTouchInput_t3055303244::get_offset_of_OnActiveChanged_0(),
	RemoteTouchInput_t3055303244::get_offset_of__isActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (RemoteTouchInputEventHandler_t3839927206), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (RemoteTouchPadInput_t3817316988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[9] = 
{
	RemoteTouchPadInput_t3817316988::get_offset_of_OnValueChanged_2(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CbuttonU3Ek__BackingField_3(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CtouchActiveU3Ek__BackingField_4(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CxAxisU3Ek__BackingField_5(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CyAxisU3Ek__BackingField_6(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CupU3Ek__BackingField_7(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CdownU3Ek__BackingField_8(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CleftU3Ek__BackingField_9(),
	RemoteTouchPadInput_t3817316988::get_offset_of_U3CrightU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (RemoteTouchPadInputEventHandler_t3280214852), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (VirtualRemote_t2507984974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (CamGyro_t931366934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[3] = 
{
	CamGyro_t931366934::get_offset_of_isSpectator_2(),
	CamGyro_t931366934::get_offset_of_camGyroOffset_3(),
	CamGyro_t931366934::get_offset_of_camGyroActive_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (DebugMiraController_t1049664957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[7] = 
{
	DebugMiraController_t1049664957::get_offset_of_debugOutputText_2(),
	DebugMiraController_t1049664957::get_offset_of_output_3(),
	DebugMiraController_t1049664957::get_offset_of_didPointerEnter_4(),
	DebugMiraController_t1049664957::get_offset_of_didPointerExit_5(),
	DebugMiraController_t1049664957::get_offset_of_didPointerClick_6(),
	DebugMiraController_t1049664957::get_offset_of_didPointerDown_7(),
	DebugMiraController_t1049664957::get_offset_of_didPointerUp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (DeviceOrientationChange_t2350204370), -1, sizeof(DeviceOrientationChange_t2350204370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	DeviceOrientationChange_t2350204370_StaticFields::get_offset_of_OnResolutionChange_2(),
	DeviceOrientationChange_t2350204370_StaticFields::get_offset_of_CheckDelay_3(),
	DeviceOrientationChange_t2350204370_StaticFields::get_offset_of_resolution_4(),
	DeviceOrientationChange_t2350204370_StaticFields::get_offset_of_isAlive_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3CCheckForChangeU3Ec__Iterator0_t3804541347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[3] = 
{
	U3CCheckForChangeU3Ec__Iterator0_t3804541347::get_offset_of_U24current_0(),
	U3CCheckForChangeU3Ec__Iterator0_t3804541347::get_offset_of_U24disposing_1(),
	U3CCheckForChangeU3Ec__Iterator0_t3804541347::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (DistortionCamera_t4266954139), -1, sizeof(DistortionCamera_t4266954139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	DistortionCamera_t4266954139::get_offset_of_stereoFov_2(),
	DistortionCamera_t4266954139_StaticFields::get_offset_of_instance_3(),
	DistortionCamera_t4266954139_StaticFields::get_offset_of_m_dcLeft_4(),
	DistortionCamera_t4266954139_StaticFields::get_offset_of_m_dcRight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (MiraBasePointer_t2409431493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	MiraBasePointer_t2409431493::get_offset_of_U3CmaxDistanceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (MiraBaseRaycaster_t368298697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[2] = 
{
	MiraBaseRaycaster_t368298697::get_offset_of_raycastStyle_2(),
	MiraBaseRaycaster_t368298697::get_offset_of_lastray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (RaycastStyle_t3505768865)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2240[3] = 
{
	RaycastStyle_t3505768865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (MiraBTRemoteInput_t3160068026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[4] = 
{
	MiraBTRemoteInput_t3160068026::get_offset_of_controller_0(),
	MiraBTRemoteInput_t3160068026::get_offset_of__virtualRemote_1(),
	MiraBTRemoteInput_t3160068026::get_offset_of__connectedRemote_2(),
	MiraBTRemoteInput_t3160068026::get_offset_of_connectedRemote_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (MiraController_t2033339498), -1, sizeof(MiraController_t2033339498_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2242[23] = 
{
	MiraController_t2033339498_StaticFields::get_offset_of__instance_2(),
	MiraController_t2033339498::get_offset_of_handedness_3(),
	MiraController_t2033339498::get_offset_of_controllerType_4(),
	MiraController_t2033339498::get_offset_of_WhatButtonIsClick_5(),
	MiraController_t2033339498_StaticFields::get_offset_of__userInput_6(),
	MiraController_t2033339498::get_offset_of_cameraRig_7(),
	MiraController_t2033339498::get_offset_of_rotationalTracking_8(),
	MiraController_t2033339498::get_offset_of_trackPosition_9(),
	MiraController_t2033339498::get_offset_of_shouldFollowHead_10(),
	MiraController_t2033339498::get_offset_of_sceneScaleMult_11(),
	MiraController_t2033339498::get_offset_of_armOffsetX_12(),
	MiraController_t2033339498::get_offset_of_armOffsetY_13(),
	MiraController_t2033339498::get_offset_of_armOffsetZ_14(),
	MiraController_t2033339498::get_offset_of_baseController_15(),
	MiraController_t2033339498::get_offset_of_offsetPos_16(),
	MiraController_t2033339498::get_offset_of_isRotationalMode_17(),
	MiraController_t2033339498::get_offset_of_angleTiltX_18(),
	MiraController_t2033339498::get_offset_of_angleTiltY_19(),
	MiraController_t2033339498::get_offset_of_returnToHome_20(),
	MiraController_t2033339498::get_offset_of_radialTimer_21(),
	MiraController_t2033339498_StaticFields::get_offset_of_U3CevaluateClickButtonU3Ek__BackingField_22(),
	MiraController_t2033339498_StaticFields::get_offset_of_U3CevaluateClickButtonPressedU3Ek__BackingField_23(),
	MiraController_t2033339498_StaticFields::get_offset_of_U3CevaluateClickButtonReleasedU3Ek__BackingField_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ControllerType_t929894547)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	ControllerType_t929894547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Handedness_t3015040191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[3] = 
{
	Handedness_t3015040191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (ClickChoices_t1305082988)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2245[4] = 
{
	ClickChoices_t1305082988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (U3CStartControllerU3Ec__Iterator0_t3465610773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[4] = 
{
	U3CStartControllerU3Ec__Iterator0_t3465610773::get_offset_of_U24this_0(),
	U3CStartControllerU3Ec__Iterator0_t3465610773::get_offset_of_U24current_1(),
	U3CStartControllerU3Ec__Iterator0_t3465610773::get_offset_of_U24disposing_2(),
	U3CStartControllerU3Ec__Iterator0_t3465610773::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (U3CDelayedRecenterU3Ec__Iterator1_t3514494614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[4] = 
{
	U3CDelayedRecenterU3Ec__Iterator1_t3514494614::get_offset_of_U24this_0(),
	U3CDelayedRecenterU3Ec__Iterator1_t3514494614::get_offset_of_U24current_1(),
	U3CDelayedRecenterU3Ec__Iterator1_t3514494614::get_offset_of_U24disposing_2(),
	U3CDelayedRecenterU3Ec__Iterator1_t3514494614::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U3CReturnToHomeU3Ec__Iterator2_t2347054426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[7] = 
{
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U3CtimerU3E__0_0(),
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U3CanimStartedU3E__0_1(),
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U3CanimProgressU3E__0_2(),
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U24this_3(),
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U24current_4(),
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U24disposing_5(),
	U3CReturnToHomeU3Ec__Iterator2_t2347054426::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CRecenterAnimU3Ec__Iterator3_t1200937000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[9] = 
{
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U3CanimTimeU3E__0_0(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U3CreticleU3E__0_1(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U3CreticleVisU3E__0_2(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U3CoriginalColorU3E__0_3(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U3CfadeColorU3E__0_4(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U3CtimerU3E__0_5(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U24current_6(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U24disposing_7(),
	U3CRecenterAnimU3Ec__Iterator3_t1200937000::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (MiraGraphicRaycast_t892254387), -1, sizeof(MiraGraphicRaycast_t892254387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2250[8] = 
{
	0,
	MiraGraphicRaycast_t892254387::get_offset_of_blockingMask_5(),
	MiraGraphicRaycast_t892254387::get_offset_of_ignoreReversedGraphics_6(),
	MiraGraphicRaycast_t892254387::get_offset_of_blockingObjects_7(),
	MiraGraphicRaycast_t892254387::get_offset_of_raycastResults_8(),
	MiraGraphicRaycast_t892254387::get_offset_of__canvas_9(),
	MiraGraphicRaycast_t892254387_StaticFields::get_offset_of_sortedGraphics_10(),
	MiraGraphicRaycast_t892254387_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (MiraInputModule_t1698371845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[15] = 
{
	MiraInputModule_t1698371845::get_offset_of_pointerData_14(),
	MiraInputModule_t1698371845::get_offset_of_lastPose_15(),
	MiraInputModule_t1698371845::get_offset_of_m_ForceModuleActive_16(),
	MiraInputModule_t1698371845::get_offset_of_m_PrevActionTime_17(),
	MiraInputModule_t1698371845::get_offset_of_m_LastMoveVector_18(),
	MiraInputModule_t1698371845::get_offset_of_m_ConsecutiveMoveCount_19(),
	MiraInputModule_t1698371845::get_offset_of_m_LastMousePosition_20(),
	MiraInputModule_t1698371845::get_offset_of_m_MousePosition_21(),
	MiraInputModule_t1698371845::get_offset_of_m_HorizontalAxis_22(),
	MiraInputModule_t1698371845::get_offset_of_m_VerticalAxis_23(),
	MiraInputModule_t1698371845::get_offset_of_m_SubmitButton_24(),
	MiraInputModule_t1698371845::get_offset_of_m_CancelButton_25(),
	MiraInputModule_t1698371845::get_offset_of_m_InputActionsPerSecond_26(),
	MiraInputModule_t1698371845::get_offset_of_m_RepeatDelay_27(),
	MiraInputModule_t1698371845::get_offset_of_m_MouseState_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (InputMode_t2966778961)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2252[3] = 
{
	InputMode_t2966778961::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (MiraPhysicsRaycast_t1299684474), -1, sizeof(MiraPhysicsRaycast_t1299684474_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2253[4] = 
{
	0,
	MiraPhysicsRaycast_t1299684474::get_offset_of__EventCamera_5(),
	MiraPhysicsRaycast_t1299684474::get_offset_of__EventMask_6(),
	MiraPhysicsRaycast_t1299684474_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (MiraPointerManager_t842607068), -1, sizeof(MiraPointerManager_t842607068_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2254[3] = 
{
	MiraPointerManager_t842607068_StaticFields::get_offset_of__instance_2(),
	MiraPointerManager_t842607068::get_offset_of__pointer_3(),
	MiraPointerManager_t842607068::get_offset_of__pointerGameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (MiraReticlePointer_t3058549069), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (FollowHeadAutomatic_t1017553725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[3] = 
{
	FollowHeadAutomatic_t1017553725::get_offset_of_headFollowPositionCM_2(),
	FollowHeadAutomatic_t1017553725::get_offset_of_lerpSpeed_3(),
	FollowHeadAutomatic_t1017553725::get_offset_of_head_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (FollowHeadAutomaticScale_t4137849354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[3] = 
{
	FollowHeadAutomaticScale_t4137849354::get_offset_of_headFollowPositionM_2(),
	FollowHeadAutomaticScale_t4137849354::get_offset_of_lerpSpeed_3(),
	FollowHeadAutomaticScale_t4137849354::get_offset_of_head_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (GyroController_t3407913731), -1, sizeof(GyroController_t3407913731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2258[14] = 
{
	GyroController_t3407913731_StaticFields::get_offset_of_instance_2(),
	GyroController_t3407913731::get_offset_of_gyroRotation_3(),
	GyroController_t3407913731::get_offset_of_gyroEnabled_4(),
	GyroController_t3407913731::get_offset_of_lowPassFilterFactor_5(),
	GyroController_t3407913731::get_offset_of_frontCamera_6(),
	GyroController_t3407913731::get_offset_of_cameraBase_7(),
	GyroController_t3407913731::get_offset_of_calibration_8(),
	GyroController_t3407913731::get_offset_of_baseOrientation_9(),
	GyroController_t3407913731::get_offset_of_referenceRotation_10(),
	GyroController_t3407913731::get_offset_of_inverseGyroSetup_11(),
	GyroController_t3407913731::get_offset_of_debugGUI_12(),
	GyroController_t3407913731::get_offset_of_useFrontCamera_13(),
	GyroController_t3407913731::get_offset_of_miraRemoteMode_14(),
	GyroController_t3407913731::get_offset_of_rotationOffset_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (InverseGyroController_t592264774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	InverseGyroController_t592264774::get_offset_of_stereoCamRig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (LaserLerp_t1945677441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[1] = 
{
	LaserLerp_t1945677441::get_offset_of_laserLerpSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (MiraArController_t2798564687), -1, sizeof(MiraArController_t2798564687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2261[21] = 
{
	MiraArController_t2798564687::get_offset_of_IPD_2(),
	MiraArController_t2798564687::get_offset_of_leftCam_3(),
	MiraArController_t2798564687::get_offset_of_rightCam_4(),
	MiraArController_t2798564687::get_offset_of_cameraRig_5(),
	MiraArController_t2798564687::get_offset_of_isRotationalOnly_6(),
	MiraArController_t2798564687::get_offset_of_isSpectator_7(),
	MiraArController_t2798564687::get_offset_of_nearClipPlane_8(),
	MiraArController_t2798564687::get_offset_of_farClipPlane_9(),
	MiraArController_t2798564687::get_offset_of_defaultScale_10(),
	MiraArController_t2798564687::get_offset_of_setScaleMultiplier_11(),
	MiraArController_t2798564687_StaticFields::get_offset_of_scaleMultiplier_12(),
	MiraArController_t2798564687::get_offset_of_stereoCamFov_13(),
	MiraArController_t2798564687::get_offset_of_mv_14(),
	MiraArController_t2798564687_StaticFields::get_offset_of_instance_15(),
	MiraArController_t2798564687::get_offset_of_btnTexture_16(),
	MiraArController_t2798564687::get_offset_of_buttonHeight_17(),
	MiraArController_t2798564687::get_offset_of_buttonWidth_18(),
	MiraArController_t2798564687::get_offset_of_MiraGuiSkin_19(),
	MiraArController_t2798564687::get_offset_of_miraInputModule_20(),
	MiraArController_t2798564687::get_offset_of_GUIEnabled_21(),
	MiraArController_t2798564687::get_offset_of_settingsMenu_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (MiraEditorPreview_t1088319792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (MiraiOSBridge_t1203351363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (MiraLaserPointerLength_t2739907205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[3] = 
{
	MiraLaserPointerLength_t2739907205::get_offset_of_widthMultiplier_2(),
	MiraLaserPointerLength_t2739907205::get_offset_of_reticle_3(),
	MiraLaserPointerLength_t2739907205::get_offset_of_rend_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (MiraARVideo_t2088478125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	MiraARVideo_t2088478125::get_offset_of_m_clearTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (MiraConnectionMessageIds_t116877937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (MiraSubMessageIds_t4040701080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (MiraLivePreviewEditor_t1492853446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (MiraLivePreviewPlayer_t291622437), -1, sizeof(MiraLivePreviewPlayer_t291622437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2269[9] = 
{
	MiraLivePreviewPlayer_t291622437::get_offset_of_playerConnection_2(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_bSessionActive_3(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_editorID_4(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_liveViewScreenTex_5(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_bTexturesInitialized_6(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_isTracking_7(),
	MiraLivePreviewPlayer_t291622437_StaticFields::get_offset_of_m_userInput_8(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_btFrameCounter_9(),
	MiraLivePreviewPlayer_t291622437::get_offset_of_btSendRate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (MiraLivePreviewVideo_t1040645816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (MiraLivePreviewWikiConfig_t2494817680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[1] = 
{
	MiraLivePreviewWikiConfig_t2494817680::get_offset_of_ArCam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (ObjectSerializationExtension_t1046383205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (SerializableVector3_t1862640089)+ sizeof (RuntimeObject), sizeof(SerializableVector3_t1862640089 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2273[3] = 
{
	SerializableVector3_t1862640089::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector3_t1862640089::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector3_t1862640089::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (SerializableVector2_t1862640090)+ sizeof (RuntimeObject), sizeof(SerializableVector2_t1862640090 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2274[2] = 
{
	SerializableVector2_t1862640090::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector2_t1862640090::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (SerializableVector4_t1862640084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[4] = 
{
	SerializableVector4_t1862640084::get_offset_of_x_0(),
	SerializableVector4_t1862640084::get_offset_of_y_1(),
	SerializableVector4_t1862640084::get_offset_of_z_2(),
	SerializableVector4_t1862640084::get_offset_of_w_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (SerializableQuaternion_t1284387000)+ sizeof (RuntimeObject), sizeof(SerializableQuaternion_t1284387000 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[4] = 
{
	SerializableQuaternion_t1284387000::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableQuaternion_t1284387000::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableQuaternion_t1284387000::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableQuaternion_t1284387000::get_offset_of_w_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (serializableFloat_t3568121741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[1] = 
{
	serializableFloat_t3568121741::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (serializableGyroscope_t1801835429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	serializableGyroscope_t1801835429::get_offset_of_attitude_0(),
	serializableGyroscope_t1801835429::get_offset_of_userAcceleration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (serializableTransform_t510777870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[2] = 
{
	serializableTransform_t510777870::get_offset_of_position_0(),
	serializableTransform_t510777870::get_offset_of_rotation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (serializableBTRemote_t2102349873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[3] = 
{
	serializableBTRemote_t2102349873::get_offset_of_orientation_0(),
	serializableBTRemote_t2102349873::get_offset_of_rotationRate_1(),
	serializableBTRemote_t2102349873::get_offset_of_acceleration_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (serializableBTRemoteTouchPad_t415748006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[7] = 
{
	serializableBTRemoteTouchPad_t415748006::get_offset_of_touchActive_0(),
	serializableBTRemoteTouchPad_t415748006::get_offset_of_touchButton_1(),
	serializableBTRemoteTouchPad_t415748006::get_offset_of_touchPos_2(),
	serializableBTRemoteTouchPad_t415748006::get_offset_of_upButton_3(),
	serializableBTRemoteTouchPad_t415748006::get_offset_of_downButton_4(),
	serializableBTRemoteTouchPad_t415748006::get_offset_of_leftButton_5(),
	serializableBTRemoteTouchPad_t415748006::get_offset_of_rightButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (serializableBTRemoteButtons_t3417182756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[3] = 
{
	serializableBTRemoteButtons_t3417182756::get_offset_of_startButton_0(),
	serializableBTRemoteButtons_t3417182756::get_offset_of_backButton_1(),
	serializableBTRemoteButtons_t3417182756::get_offset_of_triggerButton_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (serializablePointCloud_t455238287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[1] = 
{
	serializablePointCloud_t455238287::get_offset_of_pointCloudData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (serializableFromEditorMessage_t3245497382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[2] = 
{
	serializableFromEditorMessage_t3245497382::get_offset_of_subMessageId_0(),
	serializableFromEditorMessage_t3245497382::get_offset_of_bytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (MiraPostRender_t1363872762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[23] = 
{
	MiraPostRender_t1363872762::get_offset_of_stereoCamFov_2(),
	MiraPostRender_t1363872762::get_offset_of_noDistortion_3(),
	MiraPostRender_t1363872762::get_offset_of_postReduceDistortion_4(),
	MiraPostRender_t1363872762::get_offset_of_xDistAmount_5(),
	MiraPostRender_t1363872762::get_offset_of_yDistAmount_6(),
	MiraPostRender_t1363872762::get_offset_of_tanCoordinates_7(),
	MiraPostRender_t1363872762::get_offset_of_eye_8(),
	MiraPostRender_t1363872762::get_offset_of_xSize_9(),
	MiraPostRender_t1363872762::get_offset_of_ySize_10(),
	MiraPostRender_t1363872762::get_offset_of_zOffset_11(),
	MiraPostRender_t1363872762::get_offset_of_xScalar_12(),
	MiraPostRender_t1363872762::get_offset_of_yScalar_13(),
	MiraPostRender_t1363872762::get_offset_of_tanConstant_14(),
	MiraPostRender_t1363872762::get_offset_of_desiredParallaxDist_15(),
	MiraPostRender_t1363872762::get_offset_of_mesh_16(),
	MiraPostRender_t1363872762::get_offset_of_renderTextureMaterial_17(),
	MiraPostRender_t1363872762::get_offset_of_IPD_18(),
	MiraPostRender_t1363872762::get_offset_of_ParallaxShift_19(),
	MiraPostRender_t1363872762::get_offset_of_coefficientsXLeft_20(),
	MiraPostRender_t1363872762::get_offset_of_coefficientsYLeft_21(),
	MiraPostRender_t1363872762::get_offset_of_coefficientsXRight_22(),
	MiraPostRender_t1363872762::get_offset_of_coefficientsYRight_23(),
	MiraPostRender_t1363872762::get_offset_of_distortion_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (Eye_t1833640780)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2286[3] = 
{
	Eye_t1833640780::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (MiraPreRender_t3151757564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[1] = 
{
	MiraPreRender_t3151757564::get_offset_of_U3CcamU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (MiraReticle_t910965139), -1, sizeof(MiraReticle_t910965139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2288[14] = 
{
	MiraReticle_t910965139::get_offset_of_reticlepointer_2(),
	MiraReticle_t910965139_StaticFields::get_offset_of__instance_3(),
	MiraReticle_t910965139::get_offset_of_cameraRig_4(),
	MiraReticle_t910965139::get_offset_of_onlyVisibleOnHover_5(),
	MiraReticle_t910965139::get_offset_of_maxDistance_6(),
	MiraReticle_t910965139::get_offset_of_minDistance_7(),
	MiraReticle_t910965139::get_offset_of_minScale_8(),
	MiraReticle_t910965139::get_offset_of_maxScale_9(),
	MiraReticle_t910965139::get_offset_of_lastDistance_10(),
	MiraReticle_t910965139::get_offset_of_externalMultiplier_11(),
	MiraReticle_t910965139::get_offset_of_reticleHoverColor_12(),
	MiraReticle_t910965139::get_offset_of_reticleIdleColor_13(),
	MiraReticle_t910965139::get_offset_of_reticleRenderer_14(),
	MiraReticle_t910965139::get_offset_of_reticleOriginalScale_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (MiraViewer_t94414402), -1, sizeof(MiraViewer_t94414402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2289[6] = 
{
	MiraViewer_t94414402::get_offset_of_stereoCamFov_2(),
	MiraViewer_t94414402_StaticFields::get_offset_of_viewer_3(),
	MiraViewer_t94414402::get_offset_of_cameraRig_4(),
	MiraViewer_t94414402::get_offset_of_Left_Eye_5(),
	MiraViewer_t94414402::get_offset_of_Right_Eye_6(),
	MiraViewer_t94414402_StaticFields::get_offset_of_Instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (CameraNames_t2553488640)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2290[3] = 
{
	CameraNames_t2553488640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (MiraWikitudeManager_t2444513627), -1, sizeof(MiraWikitudeManager_t2444513627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2291[9] = 
{
	MiraWikitudeManager_t2444513627::get_offset_of_ArCam_2(),
	MiraWikitudeManager_t2444513627::get_offset_of_headOrientation3DOF_3(),
	MiraWikitudeManager_t2444513627::get_offset_of_imageTracker_4(),
	MiraWikitudeManager_t2444513627_StaticFields::get_offset_of_instance_5(),
	MiraWikitudeManager_t2444513627::get_offset_of_scaleMultiplier_6(),
	MiraWikitudeManager_t2444513627::get_offset_of_rotationalOffset_7(),
	MiraWikitudeManager_t2444513627::get_offset_of_positionalOffset_8(),
	MiraWikitudeManager_t2444513627::get_offset_of_rotationalTracking_9(),
	MiraWikitudeManager_t2444513627::get_offset_of_flag_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (U3CActivateTrackingU3Ec__Iterator0_t1610460162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[4] = 
{
	U3CActivateTrackingU3Ec__Iterator0_t1610460162::get_offset_of_U24this_0(),
	U3CActivateTrackingU3Ec__Iterator0_t1610460162::get_offset_of_U24current_1(),
	U3CActivateTrackingU3Ec__Iterator0_t1610460162::get_offset_of_U24disposing_2(),
	U3CActivateTrackingU3Ec__Iterator0_t1610460162::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (RotationalTrackingManager_t1723746904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[10] = 
{
	RotationalTrackingManager_t1723746904::get_offset_of_mainCamera_2(),
	RotationalTrackingManager_t1723746904::get_offset_of_cameraRig_3(),
	RotationalTrackingManager_t1723746904::get_offset_of_isRotational_4(),
	RotationalTrackingManager_t1723746904::get_offset_of_positionBuffer_5(),
	RotationalTrackingManager_t1723746904::get_offset_of_rotationBuffer_6(),
	RotationalTrackingManager_t1723746904::get_offset_of_delay_7(),
	RotationalTrackingManager_t1723746904::get_offset_of_bufferSize_8(),
	RotationalTrackingManager_t1723746904::get_offset_of_bufferDiscardLast_9(),
	RotationalTrackingManager_t1723746904::get_offset_of_camRigStartPosition_10(),
	RotationalTrackingManager_t1723746904::get_offset_of_isSpectator_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U3CRotationalSetupU3Ec__Iterator0_t1515946677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[4] = 
{
	U3CRotationalSetupU3Ec__Iterator0_t1515946677::get_offset_of_U24this_0(),
	U3CRotationalSetupU3Ec__Iterator0_t1515946677::get_offset_of_U24current_1(),
	U3CRotationalSetupU3Ec__Iterator0_t1515946677::get_offset_of_U24disposing_2(),
	U3CRotationalSetupU3Ec__Iterator0_t1515946677::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (U3CBufferPositionU3Ec__Iterator1_t3875555976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[4] = 
{
	U3CBufferPositionU3Ec__Iterator1_t3875555976::get_offset_of_U24this_0(),
	U3CBufferPositionU3Ec__Iterator1_t3875555976::get_offset_of_U24current_1(),
	U3CBufferPositionU3Ec__Iterator1_t3875555976::get_offset_of_U24disposing_2(),
	U3CBufferPositionU3Ec__Iterator1_t3875555976::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[5] = 
{
	U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814::get_offset_of_U3CoffsetPositionU3E__0_0(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814::get_offset_of_U24this_1(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814::get_offset_of_U24current_2(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814::get_offset_of_U24disposing_3(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t2238231814::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (SettingsManager_t2083239687), -1, sizeof(SettingsManager_t2083239687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2297[7] = 
{
	SettingsManager_t2083239687::get_offset_of_MainSettingsMenu_2(),
	SettingsManager_t2083239687::get_offset_of_RemoteMenu_3(),
	SettingsManager_t2083239687::get_offset_of_connectedNotification_4(),
	SettingsManager_t2083239687::get_offset_of_disconnectedNotification_5(),
	SettingsManager_t2083239687_StaticFields::get_offset_of_Instance_6(),
	SettingsManager_t2083239687::get_offset_of_remotesController_7(),
	SettingsManager_t2083239687::get_offset_of_settingsButton_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[4] = 
{
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399::get_offset_of_U24this_0(),
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399::get_offset_of_U24current_1(),
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399::get_offset_of_U24disposing_2(),
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t555146399::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[4] = 
{
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614::get_offset_of_U24this_0(),
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614::get_offset_of_U24current_1(),
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614::get_offset_of_U24disposing_2(),
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t3913456614::get_offset_of_U24PC_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
