﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// ExamplePlayerController
struct ExamplePlayerController_t740376480;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888;
// SimpleInput
struct SimpleInput_t4265260572;
// SimpleInput/UpdateCallback
struct UpdateCallback_t3991193291;
// System.Delegate
struct Delegate_t1188392813;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2165061829;
// System.Collections.Generic.List`1<SimpleInput/AxisInput>
struct List_1_t2274532392;
// SimpleInput/AxisInput
struct AxisInput_t802457650;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// SimpleInputNamespace.BaseInput`2<System.String,System.Single>
struct BaseInput_2_t1381185473;
// SimpleInputNamespace.BaseInput`2<System.Object,System.Single>
struct BaseInput_2_t2943335458;
// System.Collections.Generic.List`1<SimpleInput/ButtonInput>
struct List_1_t4291026645;
// SimpleInput/ButtonInput
struct ButtonInput_t2818951903;
// SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>
struct BaseInput_2_t81206664;
// SimpleInputNamespace.BaseInput`2<System.Object,System.Boolean>
struct BaseInput_2_t1643356649;
// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>
struct List_1_t45323880;
// SimpleInput/MouseButtonInput
struct MouseButtonInput_t2868216434;
// SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>
struct BaseInput_2_t3479630992;
// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>
struct Dictionary_2_t1968145125;
// SimpleInput/Axis
struct Axis_t2182888826;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>
struct Dictionary_2_t2716226479;
// SimpleInput/Button
struct Button_t2930970180;
// System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>
struct Dictionary_2_t4181968371;
// SimpleInput/MouseButton
struct MouseButton_t998287744;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>
struct Dictionary_2_t316807927;
// SimpleInput/Key
struct Key_t3885074208;
// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Object>
struct Dictionary_2_t3806807179;
// System.Collections.Generic.List`1<SimpleInput/Axis>
struct List_1_t3654963568;
// System.Collections.Generic.List`1<SimpleInput/Button>
struct List_1_t108077626;
// System.Collections.Generic.List`1<SimpleInput/MouseButton>
struct List_1_t2470362486;
// SimpleInput/KeyInput
struct KeyInput_t3393349320;
// SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>
struct BaseInput_2_t1022651380;
// System.Collections.Generic.List`1<SimpleInput/Key>
struct List_1_t1062181654;
// System.Collections.Generic.List`1<SimpleInput/KeyInput>
struct List_1_t570456766;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// SimpleInputNamespace.AxisInputKeyboard
struct AxisInputKeyboard_t1690261069;
// SimpleInputNamespace.AxisInputMouse
struct AxisInputMouse_t3174651034;
// SimpleInputNamespace.AxisInputUI
struct AxisInputUI_t3093488737;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// SimpleInputNamespace.ButtonInputKeyboard
struct ButtonInputKeyboard_t3640535414;
// SimpleInputNamespace.ButtonInputUI
struct ButtonInputUI_t1749738659;
// SimpleInputNamespace.Dpad
struct Dpad_t1007306070;
// SimpleInputNamespace.SimpleInputDragListener
struct SimpleInputDragListener_t3058349199;
// SimpleInputNamespace.ISimpleInputDraggable
struct ISimpleInputDraggable_t3592242264;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// SimpleInputNamespace.Joystick
struct Joystick_t1225167045;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// UnityEngine.Sprite
struct Sprite_t280657092;
// SimpleInputNamespace.KeyInputKeyboard
struct KeyInputKeyboard_t2444570555;
// SimpleInputNamespace.KeyInputUI
struct KeyInputUI_t1141642093;
// SimpleInputNamespace.MouseButtonInputKeyboard
struct MouseButtonInputKeyboard_t4221569981;
// SimpleInputNamespace.MouseButtonInputUI
struct MouseButtonInputUI_t1438106573;
// SimpleInputNamespace.SteeringWheel
struct SteeringWheel_t1692308694;
// SimpleInputNamespace.Touchpad
struct Touchpad_t3466294403;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// SimpleInput/MouseButtonInput[]
struct MouseButtonInputU5BU5D_t2011609991;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.String[]
struct StringU5BU5D_t1281789340;
// SimpleInput/Axis[]
struct AxisU5BU5D_t2385759071;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,SimpleInput/Axis,System.Collections.DictionaryEntry>
struct Transform_1_t1236170787;
// SimpleInput/AxisInput[]
struct AxisInputU5BU5D_t3788777159;
// SimpleInput/ButtonInput[]
struct ButtonInputU5BU5D_t3910904518;
// SimpleInput/Button[]
struct ButtonU5BU5D_t547109933;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,SimpleInput/Button,System.Collections.DictionaryEntry>
struct Transform_1_t3692488945;
// SimpleInput/KeyInput[]
struct KeyInputU5BU5D_t1260944665;
// SimpleInput/Key[]
struct KeyU5BU5D_t2789267553;
// SimpleInput/MouseButton[]
struct MouseButtonU5BU5D_t3646161025;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.KeyCode>
struct IEqualityComparer_1_t411658999;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.KeyCode,SimpleInput/Key,System.Collections.DictionaryEntry>
struct Transform_1_t1407579017;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SimpleInput/MouseButton,System.Collections.DictionaryEntry>
struct Transform_1_t3250499869;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// UnityEngine.Collider
struct Collider_t1773347010;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// SimpleInputNamespace.Touchpad/MouseButton[]
struct MouseButtonU5BU5D_t3311405085;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t3491343620;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t2019268878;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2475741330;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;

extern String_t* _stringLiteral1828639942;
extern String_t* _stringLiteral2984908384;
extern String_t* _stringLiteral1930566815;
extern const uint32_t ExamplePlayerController__ctor_m649646620_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t2627027031_m3991588524_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t3916780224_m4049400462_RuntimeMethod_var;
extern const uint32_t ExamplePlayerController_Awake_m384171372_MetadataUsageId;
extern RuntimeClass* SimpleInput_t4265260572_il2cpp_TypeInfo_var;
extern const uint32_t ExamplePlayerController_Update_m1857921205_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t ExamplePlayerController_FixedUpdate_m2865340580_MetadataUsageId;
extern String_t* _stringLiteral2261822918;
extern const uint32_t ExamplePlayerController_OnCollisionEnter_m4084468661_MetadataUsageId;
extern const uint32_t ExamplePlayerController_IsGrounded_m1847996145_MetadataUsageId;
extern RuntimeClass* UpdateCallback_t3991193291_il2cpp_TypeInfo_var;
extern const uint32_t SimpleInput_add_OnUpdate_m521018632_MetadataUsageId;
extern const uint32_t SimpleInput_remove_OnUpdate_m4020446573_MetadataUsageId;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisSimpleInput_t4265260572_m2077851558_RuntimeMethod_var;
extern String_t* _stringLiteral3260436178;
extern const uint32_t SimpleInput_Init_m124578375_MetadataUsageId;
extern RuntimeClass* UnityAction_2_t2165061829_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SimpleInput_OnSceneChanged_m3517309758_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_2__ctor_m4122454247_RuntimeMethod_var;
extern const uint32_t SimpleInput_Awake_m215113224_MetadataUsageId;
extern const uint32_t SimpleInput_Start_m925969886_MetadataUsageId;
extern const uint32_t SimpleInput_OnDestroy_m3382805764_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m4255429017_RuntimeMethod_var;
extern const RuntimeMethod* BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1495589272_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2913088823_RuntimeMethod_var;
extern const RuntimeMethod* BaseInput_2_StopTracking_m1977248712_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m828575477_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2247891234_RuntimeMethod_var;
extern const RuntimeMethod* BaseInput_2_StopTracking_m3193694964_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1109486988_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m3184015962_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1732670702_RuntimeMethod_var;
extern const uint32_t SimpleInput_OnSceneChanged_m3517309758_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1425023151_RuntimeMethod_var;
extern const uint32_t SimpleInput_GetAxis_m2070570208_MetadataUsageId;
extern const uint32_t SimpleInput_GetAxisRaw_m3854594848_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1636010796_RuntimeMethod_var;
extern const uint32_t SimpleInput_GetButtonDown_m2622109342_MetadataUsageId;
extern const uint32_t SimpleInput_GetButton_m2783520745_MetadataUsageId;
extern const uint32_t SimpleInput_GetButtonUp_m4050978607_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m579209773_RuntimeMethod_var;
extern const uint32_t SimpleInput_GetMouseButtonDown_m4152109214_MetadataUsageId;
extern const uint32_t SimpleInput_GetMouseButton_m3558780159_MetadataUsageId;
extern const uint32_t SimpleInput_GetMouseButtonUp_m3042222655_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m822832064_RuntimeMethod_var;
extern const uint32_t SimpleInput_GetKeyDown_m41503968_MetadataUsageId;
extern const uint32_t SimpleInput_GetKey_m2675519645_MetadataUsageId;
extern const uint32_t SimpleInput_GetKeyUp_m3571899093_MetadataUsageId;
extern RuntimeClass* Axis_t2182888826_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_get_Key_m784409413_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m2398515240_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1209423649_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3590501126_RuntimeMethod_var;
extern const uint32_t SimpleInput_RegisterAxis_m496276425_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m466206242_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m2962106831_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m2391931732_RuntimeMethod_var;
extern const uint32_t SimpleInput_UnregisterAxis_m2397270711_MetadataUsageId;
extern RuntimeClass* Button_t2930970180_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_get_Key_m1061843947_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m232276741_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1978629791_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m577318652_RuntimeMethod_var;
extern const uint32_t SimpleInput_RegisterButton_m3324302350_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m1939984325_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m1980553089_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m3740224469_RuntimeMethod_var;
extern const uint32_t SimpleInput_UnregisterButton_m1428040134_MetadataUsageId;
extern RuntimeClass* MouseButton_t998287744_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_get_Key_m3054695495_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m3172288348_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1746165051_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1188574635_RuntimeMethod_var;
extern const uint32_t SimpleInput_RegisterMouseButton_m4261908703_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m2942097792_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3237777153_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m2637628108_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m2631527114_RuntimeMethod_var;
extern const uint32_t SimpleInput_UnregisterMouseButton_m1845058510_MetadataUsageId;
extern RuntimeClass* Key_t3885074208_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_get_Key_m870532662_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m1481711019_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2761925027_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2434912339_RuntimeMethod_var;
extern const uint32_t SimpleInput_RegisterKey_m2116541909_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m3715497796_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3615089172_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m1979789194_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m372239913_RuntimeMethod_var;
extern const uint32_t SimpleInput_UnregisterKey_m3562015723_MetadataUsageId;
extern RuntimeClass* AxisInput_t802457650_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var;
extern const uint32_t SimpleInput_TrackAxis_m1843737917_MetadataUsageId;
extern RuntimeClass* ButtonInput_t2818951903_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_StartTracking_m3481582819_RuntimeMethod_var;
extern const uint32_t SimpleInput_TrackButton_m2675713569_MetadataUsageId;
extern RuntimeClass* MouseButtonInput_t2868216434_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BaseInput_2_StartTracking_m985837305_RuntimeMethod_var;
extern const uint32_t SimpleInput_TrackMouseButton_m959883669_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m2001209520_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1399466053_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m182785779_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m377990033_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3336004944_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m326005465_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1284462838_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1602638289_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m108667433_RuntimeMethod_var;
extern const uint32_t SimpleInput_Update_m3384111285_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1968145125_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3654963568_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2274532392_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2716226479_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t108077626_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4291026645_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t4181968371_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2470362486_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t45323880_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t316807927_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1062181654_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1219153817_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3492050649_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2629046078_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1157345819_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m4217368305_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m770714582_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1685523410_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2002102647_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m256120419_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2001987697_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2112490249_RuntimeMethod_var;
extern const uint32_t SimpleInput__cctor_m942092536_MetadataUsageId;
extern const uint32_t Axis__ctor_m2063451953_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m1791511703_RuntimeMethod_var;
extern const uint32_t AxisInput__ctor_m143791316_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m449874524_RuntimeMethod_var;
extern const uint32_t AxisInput__ctor_m635020897_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AxisInput_IsKeyValid_m4176685907_MetadataUsageId;
extern const uint32_t AxisInput_KeysEqual_m3941979874_MetadataUsageId;
extern const uint32_t AxisInput_RegisterInput_m3849587313_MetadataUsageId;
extern const uint32_t AxisInput_UnregisterInput_m2227498125_MetadataUsageId;
extern const uint32_t Button__ctor_m1639548820_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m3207761156_RuntimeMethod_var;
extern const uint32_t ButtonInput__ctor_m2907722220_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m3206253858_RuntimeMethod_var;
extern const uint32_t ButtonInput__ctor_m1973775960_MetadataUsageId;
extern const uint32_t ButtonInput_IsKeyValid_m1643963182_MetadataUsageId;
extern const uint32_t ButtonInput_KeysEqual_m549903569_MetadataUsageId;
extern const uint32_t ButtonInput_RegisterInput_m2741498365_MetadataUsageId;
extern const uint32_t ButtonInput_UnregisterInput_m1873494997_MetadataUsageId;
extern RuntimeClass* List_1_t570456766_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2757187041_RuntimeMethod_var;
extern const uint32_t Key__ctor_m707106284_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m4093196004_RuntimeMethod_var;
extern const uint32_t KeyInput__ctor_m3791521000_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m3612960250_RuntimeMethod_var;
extern const uint32_t KeyInput__ctor_m134535775_MetadataUsageId;
extern const uint32_t KeyInput_RegisterInput_m3140085273_MetadataUsageId;
extern const uint32_t KeyInput_UnregisterInput_m2413266091_MetadataUsageId;
extern const uint32_t MouseButton__ctor_m2922259096_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m1777816748_RuntimeMethod_var;
extern const uint32_t MouseButtonInput__ctor_m247368948_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2__ctor_m3806450130_RuntimeMethod_var;
extern const uint32_t MouseButtonInput__ctor_m1842993350_MetadataUsageId;
extern const uint32_t MouseButtonInput_RegisterInput_m1050424758_MetadataUsageId;
extern const uint32_t MouseButtonInput_UnregisterInput_m584781875_MetadataUsageId;
extern const uint32_t AxisInputKeyboard__ctor_m1434635288_MetadataUsageId;
extern const RuntimeMethod* AxisInputKeyboard_OnUpdate_m773120620_RuntimeMethod_var;
extern const uint32_t AxisInputKeyboard_OnEnable_m697364291_MetadataUsageId;
extern const uint32_t AxisInputKeyboard_OnDisable_m3983922163_MetadataUsageId;
extern const uint32_t AxisInputKeyboard_OnUpdate_m773120620_MetadataUsageId;
extern const uint32_t AxisInputMouse__ctor_m996829082_MetadataUsageId;
extern const uint32_t AxisInputUI__ctor_m3583187817_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var;
extern const uint32_t AxisInputUI_Awake_m2387327640_MetadataUsageId;
extern const uint32_t AxisInputUI_OnEnable_m3469068147_MetadataUsageId;
extern const uint32_t AxisInputUI_OnDisable_m1473024516_MetadataUsageId;
extern const uint32_t ButtonInputKeyboard__ctor_m1168318295_MetadataUsageId;
extern const RuntimeMethod* ButtonInputKeyboard_OnUpdate_m3691304227_RuntimeMethod_var;
extern const uint32_t ButtonInputKeyboard_OnEnable_m508473374_MetadataUsageId;
extern const uint32_t ButtonInputKeyboard_OnDisable_m1528174353_MetadataUsageId;
extern const uint32_t ButtonInputKeyboard_OnUpdate_m3691304227_MetadataUsageId;
extern const uint32_t ButtonInputUI__ctor_m1861433565_MetadataUsageId;
extern const uint32_t ButtonInputUI_Awake_m4178634406_MetadataUsageId;
extern const uint32_t ButtonInputUI_OnEnable_m2872570879_MetadataUsageId;
extern const uint32_t ButtonInputUI_OnDisable_m1423996615_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t Dpad__ctor_m1388007440_MetadataUsageId;
extern RuntimeClass* RectTransform_t3704657025_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994_RuntimeMethod_var;
extern const uint32_t Dpad_Awake_m629104672_MetadataUsageId;
extern const uint32_t Dpad_OnEnable_m3687405751_MetadataUsageId;
extern const uint32_t Dpad_OnDisable_m3832001405_MetadataUsageId;
extern const uint32_t Dpad_OnPointerUp_m1024412546_MetadataUsageId;
extern RuntimeClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern const uint32_t Dpad_CalculateInput_m4151013375_MetadataUsageId;
extern const uint32_t Joystick__ctor_m170445243_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const uint32_t Joystick_Awake_m4093454875_MetadataUsageId;
extern const RuntimeType* RectTransform_t3704657025_0_0_0_var;
extern const RuntimeType* Image_t2670269651_0_0_0_var;
extern RuntimeClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var;
extern String_t* _stringLiteral2150701781;
extern const uint32_t Joystick_Start_m1419705193_MetadataUsageId;
extern const RuntimeMethod* Joystick_OnUpdate_m3745511237_RuntimeMethod_var;
extern const uint32_t Joystick_OnEnable_m2515348506_MetadataUsageId;
extern const uint32_t Joystick_OnDisable_m4097605840_MetadataUsageId;
extern const uint32_t Joystick_OnPointerDown_m1051375263_MetadataUsageId;
extern const uint32_t Joystick_OnDrag_m3054587775_MetadataUsageId;
extern const uint32_t Joystick_OnPointerUp_m1184069125_MetadataUsageId;
extern const uint32_t Joystick_OnUpdate_m3745511237_MetadataUsageId;
extern RuntimeClass* KeyInput_t3393349320_il2cpp_TypeInfo_var;
extern const uint32_t KeyInputKeyboard__ctor_m1037973947_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2_StartTracking_m1519213419_RuntimeMethod_var;
extern const RuntimeMethod* KeyInputKeyboard_OnUpdate_m1091070468_RuntimeMethod_var;
extern const uint32_t KeyInputKeyboard_OnEnable_m2127914007_MetadataUsageId;
extern const RuntimeMethod* BaseInput_2_StopTracking_m3686500270_RuntimeMethod_var;
extern const uint32_t KeyInputKeyboard_OnDisable_m996401503_MetadataUsageId;
extern const uint32_t KeyInputKeyboard_OnUpdate_m1091070468_MetadataUsageId;
extern const uint32_t KeyInputUI__ctor_m4057530322_MetadataUsageId;
extern const uint32_t KeyInputUI_Awake_m174052282_MetadataUsageId;
extern const uint32_t KeyInputUI_OnEnable_m2983933856_MetadataUsageId;
extern const uint32_t KeyInputUI_OnDisable_m3849322508_MetadataUsageId;
extern const uint32_t MouseButtonInputKeyboard__ctor_m2031118237_MetadataUsageId;
extern const RuntimeMethod* MouseButtonInputKeyboard_OnUpdate_m988355275_RuntimeMethod_var;
extern const uint32_t MouseButtonInputKeyboard_OnEnable_m4117763566_MetadataUsageId;
extern const uint32_t MouseButtonInputKeyboard_OnDisable_m451127520_MetadataUsageId;
extern const uint32_t MouseButtonInputKeyboard_OnUpdate_m988355275_MetadataUsageId;
extern const uint32_t MouseButtonInputUI__ctor_m546969997_MetadataUsageId;
extern const uint32_t MouseButtonInputUI_Awake_m464842101_MetadataUsageId;
extern const uint32_t MouseButtonInputUI_OnEnable_m400322805_MetadataUsageId;
extern const uint32_t MouseButtonInputUI_OnDisable_m2301120173_MetadataUsageId;
extern RuntimeClass* ISimpleInputDraggable_t3592242264_il2cpp_TypeInfo_var;
extern const uint32_t SimpleInputDragListener_OnPointerDown_m625974936_MetadataUsageId;
extern const uint32_t SimpleInputDragListener_OnDrag_m505829966_MetadataUsageId;
extern const uint32_t SimpleInputDragListener_OnPointerUp_m3725929757_MetadataUsageId;
extern const uint32_t SteeringWheel__ctor_m3581698376_MetadataUsageId;
extern const uint32_t SteeringWheel_Awake_m1459047363_MetadataUsageId;
extern const RuntimeMethod* SteeringWheel_OnUpdate_m2737091337_RuntimeMethod_var;
extern const uint32_t SteeringWheel_OnEnable_m1912695944_MetadataUsageId;
extern const uint32_t SteeringWheel_OnDisable_m1546151817_MetadataUsageId;
extern const uint32_t SteeringWheel_OnUpdate_m2737091337_MetadataUsageId;
extern const uint32_t SteeringWheel_OnPointerDown_m2240246608_MetadataUsageId;
extern const uint32_t SteeringWheel_OnDrag_m1834180620_MetadataUsageId;
extern String_t* _stringLiteral3403559637;
extern String_t* _stringLiteral674676282;
extern const uint32_t Touchpad__ctor_m243273444_MetadataUsageId;
extern const uint32_t Touchpad_Awake_m3382882084_MetadataUsageId;
extern const RuntimeMethod* Touchpad_OnUpdate_m3810730052_RuntimeMethod_var;
extern const uint32_t Touchpad_OnEnable_m4042567201_MetadataUsageId;
extern const uint32_t Touchpad_OnDisable_m3422720687_MetadataUsageId;
extern RuntimeClass* EventSystem_t1003666588_il2cpp_TypeInfo_var;
extern const uint32_t Touchpad_OnUpdate_m3810730052_MetadataUsageId;
extern const uint32_t Touchpad_GetMouseButtonDown_m102808409_MetadataUsageId;
extern const uint32_t Touchpad_GetMouseButton_m3020692098_MetadataUsageId;
struct ContactPoint_t3758755253 ;

struct ContactPointU5BU5D_t872956888;
struct TypeU5BU5D_t3940880105;
struct MouseButtonU5BU5D_t3311405085;


#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BASEINPUT_2_T3479630992_H
#define BASEINPUT_2_T3479630992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>
struct  BaseInput_2_t3479630992  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	int32_t ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	bool ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t3479630992, ___m_key_0)); }
	inline int32_t get_m_key_0() const { return ___m_key_0; }
	inline int32_t* get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(int32_t value)
	{
		___m_key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t3479630992, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t3479630992, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T3479630992_H
#ifndef LIST_1_T45323880_H
#define LIST_1_T45323880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>
struct  List_1_t45323880  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MouseButtonInputU5BU5D_t2011609991* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t45323880, ____items_1)); }
	inline MouseButtonInputU5BU5D_t2011609991* get__items_1() const { return ____items_1; }
	inline MouseButtonInputU5BU5D_t2011609991** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MouseButtonInputU5BU5D_t2011609991* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t45323880, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t45323880, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t45323880_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MouseButtonInputU5BU5D_t2011609991* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t45323880_StaticFields, ___EmptyArray_4)); }
	inline MouseButtonInputU5BU5D_t2011609991* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MouseButtonInputU5BU5D_t2011609991** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MouseButtonInputU5BU5D_t2011609991* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T45323880_H
#ifndef DICTIONARY_2_T1968145125_H
#define DICTIONARY_2_T1968145125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>
struct  Dictionary_2_t1968145125  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	AxisU5BU5D_t2385759071* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___valueSlots_7)); }
	inline AxisU5BU5D_t2385759071* get_valueSlots_7() const { return ___valueSlots_7; }
	inline AxisU5BU5D_t2385759071** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(AxisU5BU5D_t2385759071* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1968145125_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1236170787 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1968145125_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1236170787 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1236170787 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1236170787 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1968145125_H
#ifndef AXIS_T2182888826_H
#define AXIS_T2182888826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/Axis
struct  Axis_t2182888826  : public RuntimeObject
{
public:
	// System.String SimpleInput/Axis::name
	String_t* ___name_0;
	// System.Collections.Generic.List`1<SimpleInput/AxisInput> SimpleInput/Axis::inputs
	List_1_t2274532392 * ___inputs_1;
	// System.Single SimpleInput/Axis::value
	float ___value_2;
	// System.Single SimpleInput/Axis::valueRaw
	float ___valueRaw_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___inputs_1)); }
	inline List_1_t2274532392 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t2274532392 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t2274532392 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___value_2)); }
	inline float get_value_2() const { return ___value_2; }
	inline float* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(float value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_valueRaw_3() { return static_cast<int32_t>(offsetof(Axis_t2182888826, ___valueRaw_3)); }
	inline float get_valueRaw_3() const { return ___valueRaw_3; }
	inline float* get_address_of_valueRaw_3() { return &___valueRaw_3; }
	inline void set_valueRaw_3(float value)
	{
		___valueRaw_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T2182888826_H
#ifndef BASEINPUT_2_T81206664_H
#define BASEINPUT_2_T81206664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>
struct  BaseInput_2_t81206664  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	String_t* ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	bool ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t81206664, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t81206664, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t81206664, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T81206664_H
#ifndef LIST_1_T2274532392_H
#define LIST_1_T2274532392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/AxisInput>
struct  List_1_t2274532392  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AxisInputU5BU5D_t3788777159* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2274532392, ____items_1)); }
	inline AxisInputU5BU5D_t3788777159* get__items_1() const { return ____items_1; }
	inline AxisInputU5BU5D_t3788777159** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AxisInputU5BU5D_t3788777159* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2274532392, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2274532392, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2274532392_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	AxisInputU5BU5D_t3788777159* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2274532392_StaticFields, ___EmptyArray_4)); }
	inline AxisInputU5BU5D_t3788777159* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline AxisInputU5BU5D_t3788777159** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(AxisInputU5BU5D_t3788777159* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2274532392_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef LIST_1_T4291026645_H
#define LIST_1_T4291026645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/ButtonInput>
struct  List_1_t4291026645  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ButtonInputU5BU5D_t3910904518* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4291026645, ____items_1)); }
	inline ButtonInputU5BU5D_t3910904518* get__items_1() const { return ____items_1; }
	inline ButtonInputU5BU5D_t3910904518** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ButtonInputU5BU5D_t3910904518* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4291026645, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4291026645, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4291026645_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ButtonInputU5BU5D_t3910904518* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4291026645_StaticFields, ___EmptyArray_4)); }
	inline ButtonInputU5BU5D_t3910904518* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ButtonInputU5BU5D_t3910904518** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ButtonInputU5BU5D_t3910904518* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4291026645_H
#ifndef BASEINPUT_2_T1381185473_H
#define BASEINPUT_2_T1381185473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<System.String,System.Single>
struct  BaseInput_2_t1381185473  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	String_t* ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	float ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t1381185473, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t1381185473, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t1381185473, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T1381185473_H
#ifndef DICTIONARY_2_T2716226479_H
#define DICTIONARY_2_T2716226479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>
struct  Dictionary_2_t2716226479  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ButtonU5BU5D_t547109933* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___valueSlots_7)); }
	inline ButtonU5BU5D_t547109933* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ButtonU5BU5D_t547109933** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ButtonU5BU5D_t547109933* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2716226479_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3692488945 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2716226479_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3692488945 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3692488945 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3692488945 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2716226479_H
#ifndef LIST_1_T570456766_H
#define LIST_1_T570456766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/KeyInput>
struct  List_1_t570456766  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyInputU5BU5D_t1260944665* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t570456766, ____items_1)); }
	inline KeyInputU5BU5D_t1260944665* get__items_1() const { return ____items_1; }
	inline KeyInputU5BU5D_t1260944665** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyInputU5BU5D_t1260944665* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t570456766, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t570456766, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t570456766_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyInputU5BU5D_t1260944665* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t570456766_StaticFields, ___EmptyArray_4)); }
	inline KeyInputU5BU5D_t1260944665* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyInputU5BU5D_t1260944665** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyInputU5BU5D_t1260944665* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T570456766_H
#ifndef LIST_1_T1062181654_H
#define LIST_1_T1062181654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/Key>
struct  List_1_t1062181654  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyU5BU5D_t2789267553* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1062181654, ____items_1)); }
	inline KeyU5BU5D_t2789267553* get__items_1() const { return ____items_1; }
	inline KeyU5BU5D_t2789267553** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyU5BU5D_t2789267553* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1062181654, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1062181654, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1062181654_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyU5BU5D_t2789267553* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1062181654_StaticFields, ___EmptyArray_4)); }
	inline KeyU5BU5D_t2789267553* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyU5BU5D_t2789267553** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyU5BU5D_t2789267553* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1062181654_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LIST_1_T2470362486_H
#define LIST_1_T2470362486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/MouseButton>
struct  List_1_t2470362486  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MouseButtonU5BU5D_t3646161025* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2470362486, ____items_1)); }
	inline MouseButtonU5BU5D_t3646161025* get__items_1() const { return ____items_1; }
	inline MouseButtonU5BU5D_t3646161025** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MouseButtonU5BU5D_t3646161025* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2470362486, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2470362486, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2470362486_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MouseButtonU5BU5D_t3646161025* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2470362486_StaticFields, ___EmptyArray_4)); }
	inline MouseButtonU5BU5D_t3646161025* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MouseButtonU5BU5D_t3646161025** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MouseButtonU5BU5D_t3646161025* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2470362486_H
#ifndef DICTIONARY_2_T316807927_H
#define DICTIONARY_2_T316807927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>
struct  Dictionary_2_t316807927  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	KeyCodeU5BU5D_t2223234056* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	KeyU5BU5D_t2789267553* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___keySlots_6)); }
	inline KeyCodeU5BU5D_t2223234056* get_keySlots_6() const { return ___keySlots_6; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(KeyCodeU5BU5D_t2223234056* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___valueSlots_7)); }
	inline KeyU5BU5D_t2789267553* get_valueSlots_7() const { return ___valueSlots_7; }
	inline KeyU5BU5D_t2789267553** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(KeyU5BU5D_t2789267553* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t316807927_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1407579017 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t316807927_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1407579017 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1407579017 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1407579017 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T316807927_H
#ifndef DICTIONARY_2_T4181968371_H
#define DICTIONARY_2_T4181968371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>
struct  Dictionary_2_t4181968371  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t385246372* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	MouseButtonU5BU5D_t3646161025* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___keySlots_6)); }
	inline Int32U5BU5D_t385246372* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t385246372** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t385246372* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___valueSlots_7)); }
	inline MouseButtonU5BU5D_t3646161025* get_valueSlots_7() const { return ___valueSlots_7; }
	inline MouseButtonU5BU5D_t3646161025** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(MouseButtonU5BU5D_t3646161025* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t4181968371_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3250499869 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t4181968371_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3250499869 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3250499869 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3250499869 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4181968371_H
#ifndef LIST_1_T108077626_H
#define LIST_1_T108077626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/Button>
struct  List_1_t108077626  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ButtonU5BU5D_t547109933* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t108077626, ____items_1)); }
	inline ButtonU5BU5D_t547109933* get__items_1() const { return ____items_1; }
	inline ButtonU5BU5D_t547109933** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ButtonU5BU5D_t547109933* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t108077626, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t108077626, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t108077626_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ButtonU5BU5D_t547109933* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t108077626_StaticFields, ___EmptyArray_4)); }
	inline ButtonU5BU5D_t547109933* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ButtonU5BU5D_t547109933** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ButtonU5BU5D_t547109933* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T108077626_H
#ifndef LIST_1_T3654963568_H
#define LIST_1_T3654963568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SimpleInput/Axis>
struct  List_1_t3654963568  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AxisU5BU5D_t2385759071* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3654963568, ____items_1)); }
	inline AxisU5BU5D_t2385759071* get__items_1() const { return ____items_1; }
	inline AxisU5BU5D_t2385759071** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AxisU5BU5D_t2385759071* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3654963568, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3654963568, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3654963568_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	AxisU5BU5D_t2385759071* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3654963568_StaticFields, ___EmptyArray_4)); }
	inline AxisU5BU5D_t2385759071* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline AxisU5BU5D_t2385759071** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(AxisU5BU5D_t2385759071* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3654963568_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef BUTTONINPUT_T2818951903_H
#define BUTTONINPUT_T2818951903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/ButtonInput
struct  ButtonInput_t2818951903  : public BaseInput_2_t81206664
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUT_T2818951903_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef AXISINPUT_T802457650_H
#define AXISINPUT_T802457650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/AxisInput
struct  AxisInput_t802457650  : public BaseInput_2_t1381185473
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUT_T802457650_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef MOUSEBUTTONINPUT_T2868216434_H
#define MOUSEBUTTONINPUT_T2868216434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/MouseButtonInput
struct  MouseButtonInput_t2868216434  : public BaseInput_2_t3479630992
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINPUT_T2868216434_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef MOUSEBUTTON_T3761347860_H
#define MOUSEBUTTON_T3761347860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Touchpad/MouseButton
struct  MouseButton_t3761347860 
{
public:
	// System.Int32 SimpleInputNamespace.Touchpad/MouseButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseButton_t3761347860, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_T3761347860_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef MOVEMENTAXES_T2821595006_H
#define MOVEMENTAXES_T2821595006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Joystick/MovementAxes
struct  MovementAxes_t2821595006 
{
public:
	// System.Int32 SimpleInputNamespace.Joystick/MovementAxes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementAxes_t2821595006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTAXES_T2821595006_H
#ifndef CONTACTPOINT_T3758755253_H
#define CONTACTPOINT_T3758755253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContactPoint
struct  ContactPoint_t3758755253 
{
public:
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisColliderInstanceID_2() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_ThisColliderInstanceID_2)); }
	inline int32_t get_m_ThisColliderInstanceID_2() const { return ___m_ThisColliderInstanceID_2; }
	inline int32_t* get_address_of_m_ThisColliderInstanceID_2() { return &___m_ThisColliderInstanceID_2; }
	inline void set_m_ThisColliderInstanceID_2(int32_t value)
	{
		___m_ThisColliderInstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_OtherColliderInstanceID_3)); }
	inline int32_t get_m_OtherColliderInstanceID_3() const { return ___m_OtherColliderInstanceID_3; }
	inline int32_t* get_address_of_m_OtherColliderInstanceID_3() { return &___m_OtherColliderInstanceID_3; }
	inline void set_m_OtherColliderInstanceID_3(int32_t value)
	{
		___m_OtherColliderInstanceID_3 = value;
	}

	inline static int32_t get_offset_of_m_Separation_4() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_Separation_4)); }
	inline float get_m_Separation_4() const { return ___m_Separation_4; }
	inline float* get_address_of_m_Separation_4() { return &___m_Separation_4; }
	inline void set_m_Separation_4(float value)
	{
		___m_Separation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTPOINT_T3758755253_H
#ifndef COLLISION_T4262080450_H
#define COLLISION_T4262080450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision
struct  Collision_t4262080450  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t3722313464  ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	// UnityEngine.Rigidbody UnityEngine.Collision::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1773347010 * ___m_Collider_3;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_Contacts
	ContactPointU5BU5D_t872956888* ___m_Contacts_4;

public:
	inline static int32_t get_offset_of_m_Impulse_0() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Impulse_0)); }
	inline Vector3_t3722313464  get_m_Impulse_0() const { return ___m_Impulse_0; }
	inline Vector3_t3722313464 * get_address_of_m_Impulse_0() { return &___m_Impulse_0; }
	inline void set_m_Impulse_0(Vector3_t3722313464  value)
	{
		___m_Impulse_0 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_1() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_RelativeVelocity_1)); }
	inline Vector3_t3722313464  get_m_RelativeVelocity_1() const { return ___m_RelativeVelocity_1; }
	inline Vector3_t3722313464 * get_address_of_m_RelativeVelocity_1() { return &___m_RelativeVelocity_1; }
	inline void set_m_RelativeVelocity_1(Vector3_t3722313464  value)
	{
		___m_RelativeVelocity_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Rigidbody_2)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_2), value);
	}

	inline static int32_t get_offset_of_m_Collider_3() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Collider_3)); }
	inline Collider_t1773347010 * get_m_Collider_3() const { return ___m_Collider_3; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_3() { return &___m_Collider_3; }
	inline void set_m_Collider_3(Collider_t1773347010 * value)
	{
		___m_Collider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_3), value);
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Contacts_4)); }
	inline ContactPointU5BU5D_t872956888* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPointU5BU5D_t872956888** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPointU5BU5D_t872956888* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_com
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
#endif // COLLISION_T4262080450_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef LOADSCENEMODE_T3251202195_H
#define LOADSCENEMODE_T3251202195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t3251202195 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t3251202195, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T3251202195_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef FORCEMODE_T3656391766_H
#define FORCEMODE_T3656391766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode
struct  ForceMode_t3656391766 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode_t3656391766, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE_T3656391766_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef INPUTSTATE_T3726306521_H
#define INPUTSTATE_T3726306521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/InputState
struct  InputState_t3726306521 
{
public:
	// System.Int32 SimpleInput/InputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputState_t3726306521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T3726306521_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef MOUSEBUTTON_T998287744_H
#define MOUSEBUTTON_T998287744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/MouseButton
struct  MouseButton_t998287744  : public RuntimeObject
{
public:
	// System.Int32 SimpleInput/MouseButton::button
	int32_t ___button_0;
	// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput> SimpleInput/MouseButton::inputs
	List_1_t45323880 * ___inputs_1;
	// SimpleInput/InputState SimpleInput/MouseButton::state
	int32_t ___state_2;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(MouseButton_t998287744, ___button_0)); }
	inline int32_t get_button_0() const { return ___button_0; }
	inline int32_t* get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(int32_t value)
	{
		___button_0 = value;
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(MouseButton_t998287744, ___inputs_1)); }
	inline List_1_t45323880 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t45323880 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t45323880 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(MouseButton_t998287744, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_T998287744_H
#ifndef BUTTON_T2930970180_H
#define BUTTON_T2930970180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/Button
struct  Button_t2930970180  : public RuntimeObject
{
public:
	// System.String SimpleInput/Button::button
	String_t* ___button_0;
	// System.Collections.Generic.List`1<SimpleInput/ButtonInput> SimpleInput/Button::inputs
	List_1_t4291026645 * ___inputs_1;
	// SimpleInput/InputState SimpleInput/Button::state
	int32_t ___state_2;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(Button_t2930970180, ___button_0)); }
	inline String_t* get_button_0() const { return ___button_0; }
	inline String_t** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(String_t* value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier((&___button_0), value);
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Button_t2930970180, ___inputs_1)); }
	inline List_1_t4291026645 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t4291026645 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t4291026645 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Button_t2930970180, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T2930970180_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef KEY_T3885074208_H
#define KEY_T3885074208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/Key
struct  Key_t3885074208  : public RuntimeObject
{
public:
	// UnityEngine.KeyCode SimpleInput/Key::key
	int32_t ___key_0;
	// System.Collections.Generic.List`1<SimpleInput/KeyInput> SimpleInput/Key::inputs
	List_1_t570456766 * ___inputs_1;
	// SimpleInput/InputState SimpleInput/Key::state
	int32_t ___state_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(Key_t3885074208, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Key_t3885074208, ___inputs_1)); }
	inline List_1_t570456766 * get_inputs_1() const { return ___inputs_1; }
	inline List_1_t570456766 ** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(List_1_t570456766 * value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Key_t3885074208, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEY_T3885074208_H
#ifndef BASEINPUT_2_T1022651380_H
#define BASEINPUT_2_T1022651380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>
struct  BaseInput_2_t1022651380  : public RuntimeObject
{
public:
	// K SimpleInputNamespace.BaseInput`2::m_key
	int32_t ___m_key_0;
	// V SimpleInputNamespace.BaseInput`2::value
	bool ___value_1;
	// System.Boolean SimpleInputNamespace.BaseInput`2::isTracking
	bool ___isTracking_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(BaseInput_2_t1022651380, ___m_key_0)); }
	inline int32_t get_m_key_0() const { return ___m_key_0; }
	inline int32_t* get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(int32_t value)
	{
		___m_key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BaseInput_2_t1022651380, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_isTracking_2() { return static_cast<int32_t>(offsetof(BaseInput_2_t1022651380, ___isTracking_2)); }
	inline bool get_isTracking_2() const { return ___isTracking_2; }
	inline bool* get_address_of_isTracking_2() { return &___isTracking_2; }
	inline void set_isTracking_2(bool value)
	{
		___isTracking_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_2_T1022651380_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UNITYACTION_2_T2165061829_H
#define UNITYACTION_2_T2165061829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_t2165061829  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T2165061829_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef UPDATECALLBACK_T3991193291_H
#define UPDATECALLBACK_T3991193291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/UpdateCallback
struct  UpdateCallback_t3991193291  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECALLBACK_T3991193291_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef KEYINPUT_T3393349320_H
#define KEYINPUT_T3393349320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput/KeyInput
struct  KeyInput_t3393349320  : public BaseInput_2_t1022651380
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINPUT_T3393349320_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef AXISINPUTUI_T3093488737_H
#define AXISINPUTUI_T3093488737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.AxisInputUI
struct  AxisInputUI_t3093488737  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SimpleInputNamespace.AxisInputUI::value
	float ___value_2;
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputUI::axis
	AxisInput_t802457650 * ___axis_3;

public:
	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(AxisInputUI_t3093488737, ___value_2)); }
	inline float get_value_2() const { return ___value_2; }
	inline float* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(float value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_axis_3() { return static_cast<int32_t>(offsetof(AxisInputUI_t3093488737, ___axis_3)); }
	inline AxisInput_t802457650 * get_axis_3() const { return ___axis_3; }
	inline AxisInput_t802457650 ** get_address_of_axis_3() { return &___axis_3; }
	inline void set_axis_3(AxisInput_t802457650 * value)
	{
		___axis_3 = value;
		Il2CppCodeGenWriteBarrier((&___axis_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTUI_T3093488737_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef AXISINPUTMOUSE_T3174651034_H
#define AXISINPUTMOUSE_T3174651034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.AxisInputMouse
struct  AxisInputMouse_t3174651034  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputMouse::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputMouse::yAxis
	AxisInput_t802457650 * ___yAxis_3;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(AxisInputMouse_t3174651034, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(AxisInputMouse_t3174651034, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTMOUSE_T3174651034_H
#ifndef EXAMPLEPLAYERCONTROLLER_T740376480_H
#define EXAMPLEPLAYERCONTROLLER_T740376480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExamplePlayerController
struct  ExamplePlayerController_t740376480  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color ExamplePlayerController::materialColor
	Color_t2555686324  ___materialColor_2;
	// UnityEngine.Rigidbody ExamplePlayerController::m_rigidbody
	Rigidbody_t3916780224 * ___m_rigidbody_3;
	// System.String ExamplePlayerController::horizontalAxis
	String_t* ___horizontalAxis_4;
	// System.String ExamplePlayerController::verticalAxis
	String_t* ___verticalAxis_5;
	// System.String ExamplePlayerController::jumpButton
	String_t* ___jumpButton_6;
	// System.Single ExamplePlayerController::inputHorizontal
	float ___inputHorizontal_7;
	// System.Single ExamplePlayerController::inputVertical
	float ___inputVertical_8;

public:
	inline static int32_t get_offset_of_materialColor_2() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___materialColor_2)); }
	inline Color_t2555686324  get_materialColor_2() const { return ___materialColor_2; }
	inline Color_t2555686324 * get_address_of_materialColor_2() { return &___materialColor_2; }
	inline void set_materialColor_2(Color_t2555686324  value)
	{
		___materialColor_2 = value;
	}

	inline static int32_t get_offset_of_m_rigidbody_3() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___m_rigidbody_3)); }
	inline Rigidbody_t3916780224 * get_m_rigidbody_3() const { return ___m_rigidbody_3; }
	inline Rigidbody_t3916780224 ** get_address_of_m_rigidbody_3() { return &___m_rigidbody_3; }
	inline void set_m_rigidbody_3(Rigidbody_t3916780224 * value)
	{
		___m_rigidbody_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_rigidbody_3), value);
	}

	inline static int32_t get_offset_of_horizontalAxis_4() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___horizontalAxis_4)); }
	inline String_t* get_horizontalAxis_4() const { return ___horizontalAxis_4; }
	inline String_t** get_address_of_horizontalAxis_4() { return &___horizontalAxis_4; }
	inline void set_horizontalAxis_4(String_t* value)
	{
		___horizontalAxis_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_4), value);
	}

	inline static int32_t get_offset_of_verticalAxis_5() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___verticalAxis_5)); }
	inline String_t* get_verticalAxis_5() const { return ___verticalAxis_5; }
	inline String_t** get_address_of_verticalAxis_5() { return &___verticalAxis_5; }
	inline void set_verticalAxis_5(String_t* value)
	{
		___verticalAxis_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_5), value);
	}

	inline static int32_t get_offset_of_jumpButton_6() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___jumpButton_6)); }
	inline String_t* get_jumpButton_6() const { return ___jumpButton_6; }
	inline String_t** get_address_of_jumpButton_6() { return &___jumpButton_6; }
	inline void set_jumpButton_6(String_t* value)
	{
		___jumpButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___jumpButton_6), value);
	}

	inline static int32_t get_offset_of_inputHorizontal_7() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___inputHorizontal_7)); }
	inline float get_inputHorizontal_7() const { return ___inputHorizontal_7; }
	inline float* get_address_of_inputHorizontal_7() { return &___inputHorizontal_7; }
	inline void set_inputHorizontal_7(float value)
	{
		___inputHorizontal_7 = value;
	}

	inline static int32_t get_offset_of_inputVertical_8() { return static_cast<int32_t>(offsetof(ExamplePlayerController_t740376480, ___inputVertical_8)); }
	inline float get_inputVertical_8() const { return ___inputVertical_8; }
	inline float* get_address_of_inputVertical_8() { return &___inputVertical_8; }
	inline void set_inputVertical_8(float value)
	{
		___inputVertical_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEPLAYERCONTROLLER_T740376480_H
#ifndef AXISINPUTKEYBOARD_T1690261069_H
#define AXISINPUTKEYBOARD_T1690261069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.AxisInputKeyboard
struct  AxisInputKeyboard_t1690261069  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.AxisInputKeyboard::key
	int32_t ___key_2;
	// System.Single SimpleInputNamespace.AxisInputKeyboard::value
	float ___value_3;
	// SimpleInput/AxisInput SimpleInputNamespace.AxisInputKeyboard::axis
	AxisInput_t802457650 * ___axis_4;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(AxisInputKeyboard_t1690261069, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(AxisInputKeyboard_t1690261069, ___value_3)); }
	inline float get_value_3() const { return ___value_3; }
	inline float* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(float value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(AxisInputKeyboard_t1690261069, ___axis_4)); }
	inline AxisInput_t802457650 * get_axis_4() const { return ___axis_4; }
	inline AxisInput_t802457650 ** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(AxisInput_t802457650 * value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTKEYBOARD_T1690261069_H
#ifndef KEYINPUTKEYBOARD_T2444570555_H
#define KEYINPUTKEYBOARD_T2444570555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.KeyInputKeyboard
struct  KeyInputKeyboard_t2444570555  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.KeyInputKeyboard::realKey
	int32_t ___realKey_2;
	// SimpleInput/KeyInput SimpleInputNamespace.KeyInputKeyboard::key
	KeyInput_t3393349320 * ___key_3;

public:
	inline static int32_t get_offset_of_realKey_2() { return static_cast<int32_t>(offsetof(KeyInputKeyboard_t2444570555, ___realKey_2)); }
	inline int32_t get_realKey_2() const { return ___realKey_2; }
	inline int32_t* get_address_of_realKey_2() { return &___realKey_2; }
	inline void set_realKey_2(int32_t value)
	{
		___realKey_2 = value;
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(KeyInputKeyboard_t2444570555, ___key_3)); }
	inline KeyInput_t3393349320 * get_key_3() const { return ___key_3; }
	inline KeyInput_t3393349320 ** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(KeyInput_t3393349320 * value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier((&___key_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINPUTKEYBOARD_T2444570555_H
#ifndef KEYINPUTUI_T1141642093_H
#define KEYINPUTUI_T1141642093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.KeyInputUI
struct  KeyInputUI_t1141642093  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/KeyInput SimpleInputNamespace.KeyInputUI::key
	KeyInput_t3393349320 * ___key_2;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KeyInputUI_t1141642093, ___key_2)); }
	inline KeyInput_t3393349320 * get_key_2() const { return ___key_2; }
	inline KeyInput_t3393349320 ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(KeyInput_t3393349320 * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINPUTUI_T1141642093_H
#ifndef MOUSEBUTTONINPUTKEYBOARD_T4221569981_H
#define MOUSEBUTTONINPUTKEYBOARD_T4221569981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.MouseButtonInputKeyboard
struct  MouseButtonInputKeyboard_t4221569981  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.MouseButtonInputKeyboard::key
	int32_t ___key_2;
	// SimpleInput/MouseButtonInput SimpleInputNamespace.MouseButtonInputKeyboard::mouseButton
	MouseButtonInput_t2868216434 * ___mouseButton_3;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(MouseButtonInputKeyboard_t4221569981, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_mouseButton_3() { return static_cast<int32_t>(offsetof(MouseButtonInputKeyboard_t4221569981, ___mouseButton_3)); }
	inline MouseButtonInput_t2868216434 * get_mouseButton_3() const { return ___mouseButton_3; }
	inline MouseButtonInput_t2868216434 ** get_address_of_mouseButton_3() { return &___mouseButton_3; }
	inline void set_mouseButton_3(MouseButtonInput_t2868216434 * value)
	{
		___mouseButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButton_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINPUTKEYBOARD_T4221569981_H
#ifndef JOYSTICK_T1225167045_H
#define JOYSTICK_T1225167045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Joystick
struct  Joystick_t1225167045  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.Joystick::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.Joystick::yAxis
	AxisInput_t802457650 * ___yAxis_3;
	// UnityEngine.RectTransform SimpleInputNamespace.Joystick::joystickTR
	RectTransform_t3704657025 * ___joystickTR_4;
	// UnityEngine.UI.Image SimpleInputNamespace.Joystick::background
	Image_t2670269651 * ___background_5;
	// UnityEngine.UI.Image SimpleInputNamespace.Joystick::thumb
	Image_t2670269651 * ___thumb_6;
	// UnityEngine.RectTransform SimpleInputNamespace.Joystick::thumbTR
	RectTransform_t3704657025 * ___thumbTR_7;
	// SimpleInputNamespace.Joystick/MovementAxes SimpleInputNamespace.Joystick::movementAxes
	int32_t ___movementAxes_8;
	// System.Single SimpleInputNamespace.Joystick::movementAreaRadius
	float ___movementAreaRadius_9;
	// System.Boolean SimpleInputNamespace.Joystick::isDynamicJoystick
	bool ___isDynamicJoystick_10;
	// UnityEngine.RectTransform SimpleInputNamespace.Joystick::dynamicJoystickMovementArea
	RectTransform_t3704657025 * ___dynamicJoystickMovementArea_11;
	// System.Boolean SimpleInputNamespace.Joystick::joystickHeld
	bool ___joystickHeld_12;
	// UnityEngine.Vector2 SimpleInputNamespace.Joystick::pointerInitialPos
	Vector2_t2156229523  ___pointerInitialPos_13;
	// System.Single SimpleInputNamespace.Joystick::_1OverMovementAreaRadius
	float ____1OverMovementAreaRadius_14;
	// System.Single SimpleInputNamespace.Joystick::movementAreaRadiusSqr
	float ___movementAreaRadiusSqr_15;
	// System.Single SimpleInputNamespace.Joystick::opacity
	float ___opacity_16;
	// UnityEngine.Vector2 SimpleInputNamespace.Joystick::m_value
	Vector2_t2156229523  ___m_value_17;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}

	inline static int32_t get_offset_of_joystickTR_4() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___joystickTR_4)); }
	inline RectTransform_t3704657025 * get_joystickTR_4() const { return ___joystickTR_4; }
	inline RectTransform_t3704657025 ** get_address_of_joystickTR_4() { return &___joystickTR_4; }
	inline void set_joystickTR_4(RectTransform_t3704657025 * value)
	{
		___joystickTR_4 = value;
		Il2CppCodeGenWriteBarrier((&___joystickTR_4), value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___background_5)); }
	inline Image_t2670269651 * get_background_5() const { return ___background_5; }
	inline Image_t2670269651 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(Image_t2670269651 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier((&___background_5), value);
	}

	inline static int32_t get_offset_of_thumb_6() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___thumb_6)); }
	inline Image_t2670269651 * get_thumb_6() const { return ___thumb_6; }
	inline Image_t2670269651 ** get_address_of_thumb_6() { return &___thumb_6; }
	inline void set_thumb_6(Image_t2670269651 * value)
	{
		___thumb_6 = value;
		Il2CppCodeGenWriteBarrier((&___thumb_6), value);
	}

	inline static int32_t get_offset_of_thumbTR_7() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___thumbTR_7)); }
	inline RectTransform_t3704657025 * get_thumbTR_7() const { return ___thumbTR_7; }
	inline RectTransform_t3704657025 ** get_address_of_thumbTR_7() { return &___thumbTR_7; }
	inline void set_thumbTR_7(RectTransform_t3704657025 * value)
	{
		___thumbTR_7 = value;
		Il2CppCodeGenWriteBarrier((&___thumbTR_7), value);
	}

	inline static int32_t get_offset_of_movementAxes_8() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___movementAxes_8)); }
	inline int32_t get_movementAxes_8() const { return ___movementAxes_8; }
	inline int32_t* get_address_of_movementAxes_8() { return &___movementAxes_8; }
	inline void set_movementAxes_8(int32_t value)
	{
		___movementAxes_8 = value;
	}

	inline static int32_t get_offset_of_movementAreaRadius_9() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___movementAreaRadius_9)); }
	inline float get_movementAreaRadius_9() const { return ___movementAreaRadius_9; }
	inline float* get_address_of_movementAreaRadius_9() { return &___movementAreaRadius_9; }
	inline void set_movementAreaRadius_9(float value)
	{
		___movementAreaRadius_9 = value;
	}

	inline static int32_t get_offset_of_isDynamicJoystick_10() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___isDynamicJoystick_10)); }
	inline bool get_isDynamicJoystick_10() const { return ___isDynamicJoystick_10; }
	inline bool* get_address_of_isDynamicJoystick_10() { return &___isDynamicJoystick_10; }
	inline void set_isDynamicJoystick_10(bool value)
	{
		___isDynamicJoystick_10 = value;
	}

	inline static int32_t get_offset_of_dynamicJoystickMovementArea_11() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___dynamicJoystickMovementArea_11)); }
	inline RectTransform_t3704657025 * get_dynamicJoystickMovementArea_11() const { return ___dynamicJoystickMovementArea_11; }
	inline RectTransform_t3704657025 ** get_address_of_dynamicJoystickMovementArea_11() { return &___dynamicJoystickMovementArea_11; }
	inline void set_dynamicJoystickMovementArea_11(RectTransform_t3704657025 * value)
	{
		___dynamicJoystickMovementArea_11 = value;
		Il2CppCodeGenWriteBarrier((&___dynamicJoystickMovementArea_11), value);
	}

	inline static int32_t get_offset_of_joystickHeld_12() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___joystickHeld_12)); }
	inline bool get_joystickHeld_12() const { return ___joystickHeld_12; }
	inline bool* get_address_of_joystickHeld_12() { return &___joystickHeld_12; }
	inline void set_joystickHeld_12(bool value)
	{
		___joystickHeld_12 = value;
	}

	inline static int32_t get_offset_of_pointerInitialPos_13() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___pointerInitialPos_13)); }
	inline Vector2_t2156229523  get_pointerInitialPos_13() const { return ___pointerInitialPos_13; }
	inline Vector2_t2156229523 * get_address_of_pointerInitialPos_13() { return &___pointerInitialPos_13; }
	inline void set_pointerInitialPos_13(Vector2_t2156229523  value)
	{
		___pointerInitialPos_13 = value;
	}

	inline static int32_t get_offset_of__1OverMovementAreaRadius_14() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ____1OverMovementAreaRadius_14)); }
	inline float get__1OverMovementAreaRadius_14() const { return ____1OverMovementAreaRadius_14; }
	inline float* get_address_of__1OverMovementAreaRadius_14() { return &____1OverMovementAreaRadius_14; }
	inline void set__1OverMovementAreaRadius_14(float value)
	{
		____1OverMovementAreaRadius_14 = value;
	}

	inline static int32_t get_offset_of_movementAreaRadiusSqr_15() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___movementAreaRadiusSqr_15)); }
	inline float get_movementAreaRadiusSqr_15() const { return ___movementAreaRadiusSqr_15; }
	inline float* get_address_of_movementAreaRadiusSqr_15() { return &___movementAreaRadiusSqr_15; }
	inline void set_movementAreaRadiusSqr_15(float value)
	{
		___movementAreaRadiusSqr_15 = value;
	}

	inline static int32_t get_offset_of_opacity_16() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___opacity_16)); }
	inline float get_opacity_16() const { return ___opacity_16; }
	inline float* get_address_of_opacity_16() { return &___opacity_16; }
	inline void set_opacity_16(float value)
	{
		___opacity_16 = value;
	}

	inline static int32_t get_offset_of_m_value_17() { return static_cast<int32_t>(offsetof(Joystick_t1225167045, ___m_value_17)); }
	inline Vector2_t2156229523  get_m_value_17() const { return ___m_value_17; }
	inline Vector2_t2156229523 * get_address_of_m_value_17() { return &___m_value_17; }
	inline void set_m_value_17(Vector2_t2156229523  value)
	{
		___m_value_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T1225167045_H
#ifndef SIMPLEINPUTDRAGLISTENER_T3058349199_H
#define SIMPLEINPUTDRAGLISTENER_T3058349199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.SimpleInputDragListener
struct  SimpleInputDragListener_t3058349199  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInputNamespace.ISimpleInputDraggable SimpleInputNamespace.SimpleInputDragListener::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_2;
	// System.Int32 SimpleInputNamespace.SimpleInputDragListener::pointerId
	int32_t ___pointerId_3;

public:
	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SimpleInputDragListener_t3058349199, ___U3CListenerU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_2() const { return ___U3CListenerU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_2() { return &___U3CListenerU3Ek__BackingField_2; }
	inline void set_U3CListenerU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_pointerId_3() { return static_cast<int32_t>(offsetof(SimpleInputDragListener_t3058349199, ___pointerId_3)); }
	inline int32_t get_pointerId_3() const { return ___pointerId_3; }
	inline int32_t* get_address_of_pointerId_3() { return &___pointerId_3; }
	inline void set_pointerId_3(int32_t value)
	{
		___pointerId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEINPUTDRAGLISTENER_T3058349199_H
#ifndef DPAD_T1007306070_H
#define DPAD_T1007306070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Dpad
struct  Dpad_t1007306070  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.Dpad::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.Dpad::yAxis
	AxisInput_t802457650 * ___yAxis_3;
	// UnityEngine.RectTransform SimpleInputNamespace.Dpad::rectTransform
	RectTransform_t3704657025 * ___rectTransform_4;
	// UnityEngine.UI.Graphic SimpleInputNamespace.Dpad::background
	Graphic_t1660335611 * ___background_5;
	// UnityEngine.Vector2 SimpleInputNamespace.Dpad::m_value
	Vector2_t2156229523  ___m_value_6;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___rectTransform_4)); }
	inline RectTransform_t3704657025 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t3704657025 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_4), value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___background_5)); }
	inline Graphic_t1660335611 * get_background_5() const { return ___background_5; }
	inline Graphic_t1660335611 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(Graphic_t1660335611 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier((&___background_5), value);
	}

	inline static int32_t get_offset_of_m_value_6() { return static_cast<int32_t>(offsetof(Dpad_t1007306070, ___m_value_6)); }
	inline Vector2_t2156229523  get_m_value_6() const { return ___m_value_6; }
	inline Vector2_t2156229523 * get_address_of_m_value_6() { return &___m_value_6; }
	inline void set_m_value_6(Vector2_t2156229523  value)
	{
		___m_value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPAD_T1007306070_H
#ifndef BUTTONINPUTUI_T1749738659_H
#define BUTTONINPUTUI_T1749738659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.ButtonInputUI
struct  ButtonInputUI_t1749738659  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/ButtonInput SimpleInputNamespace.ButtonInputUI::button
	ButtonInput_t2818951903 * ___button_2;

public:
	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(ButtonInputUI_t1749738659, ___button_2)); }
	inline ButtonInput_t2818951903 * get_button_2() const { return ___button_2; }
	inline ButtonInput_t2818951903 ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(ButtonInput_t2818951903 * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUTUI_T1749738659_H
#ifndef BUTTONINPUTKEYBOARD_T3640535414_H
#define BUTTONINPUTKEYBOARD_T3640535414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.ButtonInputKeyboard
struct  ButtonInputKeyboard_t3640535414  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode SimpleInputNamespace.ButtonInputKeyboard::key
	int32_t ___key_2;
	// SimpleInput/ButtonInput SimpleInputNamespace.ButtonInputKeyboard::button
	ButtonInput_t2818951903 * ___button_3;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(ButtonInputKeyboard_t3640535414, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(ButtonInputKeyboard_t3640535414, ___button_3)); }
	inline ButtonInput_t2818951903 * get_button_3() const { return ___button_3; }
	inline ButtonInput_t2818951903 ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(ButtonInput_t2818951903 * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier((&___button_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUTKEYBOARD_T3640535414_H
#ifndef SIMPLEINPUT_T4265260572_H
#define SIMPLEINPUT_T4265260572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInput
struct  SimpleInput_t4265260572  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct SimpleInput_t4265260572_StaticFields
{
public:
	// SimpleInput SimpleInput::instance
	SimpleInput_t4265260572 * ___instance_3;
	// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis> SimpleInput::axes
	Dictionary_2_t1968145125 * ___axes_4;
	// System.Collections.Generic.List`1<SimpleInput/Axis> SimpleInput::axesList
	List_1_t3654963568 * ___axesList_5;
	// System.Collections.Generic.List`1<SimpleInput/AxisInput> SimpleInput::trackedUnityAxes
	List_1_t2274532392 * ___trackedUnityAxes_6;
	// System.Collections.Generic.List`1<SimpleInput/AxisInput> SimpleInput::trackedTemporaryAxes
	List_1_t2274532392 * ___trackedTemporaryAxes_7;
	// System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button> SimpleInput::buttons
	Dictionary_2_t2716226479 * ___buttons_8;
	// System.Collections.Generic.List`1<SimpleInput/Button> SimpleInput::buttonsList
	List_1_t108077626 * ___buttonsList_9;
	// System.Collections.Generic.List`1<SimpleInput/ButtonInput> SimpleInput::trackedUnityButtons
	List_1_t4291026645 * ___trackedUnityButtons_10;
	// System.Collections.Generic.List`1<SimpleInput/ButtonInput> SimpleInput::trackedTemporaryButtons
	List_1_t4291026645 * ___trackedTemporaryButtons_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton> SimpleInput::mouseButtons
	Dictionary_2_t4181968371 * ___mouseButtons_12;
	// System.Collections.Generic.List`1<SimpleInput/MouseButton> SimpleInput::mouseButtonsList
	List_1_t2470362486 * ___mouseButtonsList_13;
	// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput> SimpleInput::trackedUnityMouseButtons
	List_1_t45323880 * ___trackedUnityMouseButtons_14;
	// System.Collections.Generic.List`1<SimpleInput/MouseButtonInput> SimpleInput::trackedTemporaryMouseButtons
	List_1_t45323880 * ___trackedTemporaryMouseButtons_15;
	// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key> SimpleInput::keys
	Dictionary_2_t316807927 * ___keys_16;
	// System.Collections.Generic.List`1<SimpleInput/Key> SimpleInput::keysList
	List_1_t1062181654 * ___keysList_17;
	// SimpleInput/UpdateCallback SimpleInput::OnUpdate
	UpdateCallback_t3991193291 * ___OnUpdate_18;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> SimpleInput::<>f__mg$cache0
	UnityAction_2_t2165061829 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> SimpleInput::<>f__mg$cache1
	UnityAction_2_t2165061829 * ___U3CU3Ef__mgU24cache1_20;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___instance_3)); }
	inline SimpleInput_t4265260572 * get_instance_3() const { return ___instance_3; }
	inline SimpleInput_t4265260572 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(SimpleInput_t4265260572 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_axes_4() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___axes_4)); }
	inline Dictionary_2_t1968145125 * get_axes_4() const { return ___axes_4; }
	inline Dictionary_2_t1968145125 ** get_address_of_axes_4() { return &___axes_4; }
	inline void set_axes_4(Dictionary_2_t1968145125 * value)
	{
		___axes_4 = value;
		Il2CppCodeGenWriteBarrier((&___axes_4), value);
	}

	inline static int32_t get_offset_of_axesList_5() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___axesList_5)); }
	inline List_1_t3654963568 * get_axesList_5() const { return ___axesList_5; }
	inline List_1_t3654963568 ** get_address_of_axesList_5() { return &___axesList_5; }
	inline void set_axesList_5(List_1_t3654963568 * value)
	{
		___axesList_5 = value;
		Il2CppCodeGenWriteBarrier((&___axesList_5), value);
	}

	inline static int32_t get_offset_of_trackedUnityAxes_6() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedUnityAxes_6)); }
	inline List_1_t2274532392 * get_trackedUnityAxes_6() const { return ___trackedUnityAxes_6; }
	inline List_1_t2274532392 ** get_address_of_trackedUnityAxes_6() { return &___trackedUnityAxes_6; }
	inline void set_trackedUnityAxes_6(List_1_t2274532392 * value)
	{
		___trackedUnityAxes_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackedUnityAxes_6), value);
	}

	inline static int32_t get_offset_of_trackedTemporaryAxes_7() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedTemporaryAxes_7)); }
	inline List_1_t2274532392 * get_trackedTemporaryAxes_7() const { return ___trackedTemporaryAxes_7; }
	inline List_1_t2274532392 ** get_address_of_trackedTemporaryAxes_7() { return &___trackedTemporaryAxes_7; }
	inline void set_trackedTemporaryAxes_7(List_1_t2274532392 * value)
	{
		___trackedTemporaryAxes_7 = value;
		Il2CppCodeGenWriteBarrier((&___trackedTemporaryAxes_7), value);
	}

	inline static int32_t get_offset_of_buttons_8() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___buttons_8)); }
	inline Dictionary_2_t2716226479 * get_buttons_8() const { return ___buttons_8; }
	inline Dictionary_2_t2716226479 ** get_address_of_buttons_8() { return &___buttons_8; }
	inline void set_buttons_8(Dictionary_2_t2716226479 * value)
	{
		___buttons_8 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_8), value);
	}

	inline static int32_t get_offset_of_buttonsList_9() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___buttonsList_9)); }
	inline List_1_t108077626 * get_buttonsList_9() const { return ___buttonsList_9; }
	inline List_1_t108077626 ** get_address_of_buttonsList_9() { return &___buttonsList_9; }
	inline void set_buttonsList_9(List_1_t108077626 * value)
	{
		___buttonsList_9 = value;
		Il2CppCodeGenWriteBarrier((&___buttonsList_9), value);
	}

	inline static int32_t get_offset_of_trackedUnityButtons_10() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedUnityButtons_10)); }
	inline List_1_t4291026645 * get_trackedUnityButtons_10() const { return ___trackedUnityButtons_10; }
	inline List_1_t4291026645 ** get_address_of_trackedUnityButtons_10() { return &___trackedUnityButtons_10; }
	inline void set_trackedUnityButtons_10(List_1_t4291026645 * value)
	{
		___trackedUnityButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___trackedUnityButtons_10), value);
	}

	inline static int32_t get_offset_of_trackedTemporaryButtons_11() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedTemporaryButtons_11)); }
	inline List_1_t4291026645 * get_trackedTemporaryButtons_11() const { return ___trackedTemporaryButtons_11; }
	inline List_1_t4291026645 ** get_address_of_trackedTemporaryButtons_11() { return &___trackedTemporaryButtons_11; }
	inline void set_trackedTemporaryButtons_11(List_1_t4291026645 * value)
	{
		___trackedTemporaryButtons_11 = value;
		Il2CppCodeGenWriteBarrier((&___trackedTemporaryButtons_11), value);
	}

	inline static int32_t get_offset_of_mouseButtons_12() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___mouseButtons_12)); }
	inline Dictionary_2_t4181968371 * get_mouseButtons_12() const { return ___mouseButtons_12; }
	inline Dictionary_2_t4181968371 ** get_address_of_mouseButtons_12() { return &___mouseButtons_12; }
	inline void set_mouseButtons_12(Dictionary_2_t4181968371 * value)
	{
		___mouseButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtons_12), value);
	}

	inline static int32_t get_offset_of_mouseButtonsList_13() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___mouseButtonsList_13)); }
	inline List_1_t2470362486 * get_mouseButtonsList_13() const { return ___mouseButtonsList_13; }
	inline List_1_t2470362486 ** get_address_of_mouseButtonsList_13() { return &___mouseButtonsList_13; }
	inline void set_mouseButtonsList_13(List_1_t2470362486 * value)
	{
		___mouseButtonsList_13 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtonsList_13), value);
	}

	inline static int32_t get_offset_of_trackedUnityMouseButtons_14() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedUnityMouseButtons_14)); }
	inline List_1_t45323880 * get_trackedUnityMouseButtons_14() const { return ___trackedUnityMouseButtons_14; }
	inline List_1_t45323880 ** get_address_of_trackedUnityMouseButtons_14() { return &___trackedUnityMouseButtons_14; }
	inline void set_trackedUnityMouseButtons_14(List_1_t45323880 * value)
	{
		___trackedUnityMouseButtons_14 = value;
		Il2CppCodeGenWriteBarrier((&___trackedUnityMouseButtons_14), value);
	}

	inline static int32_t get_offset_of_trackedTemporaryMouseButtons_15() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___trackedTemporaryMouseButtons_15)); }
	inline List_1_t45323880 * get_trackedTemporaryMouseButtons_15() const { return ___trackedTemporaryMouseButtons_15; }
	inline List_1_t45323880 ** get_address_of_trackedTemporaryMouseButtons_15() { return &___trackedTemporaryMouseButtons_15; }
	inline void set_trackedTemporaryMouseButtons_15(List_1_t45323880 * value)
	{
		___trackedTemporaryMouseButtons_15 = value;
		Il2CppCodeGenWriteBarrier((&___trackedTemporaryMouseButtons_15), value);
	}

	inline static int32_t get_offset_of_keys_16() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___keys_16)); }
	inline Dictionary_2_t316807927 * get_keys_16() const { return ___keys_16; }
	inline Dictionary_2_t316807927 ** get_address_of_keys_16() { return &___keys_16; }
	inline void set_keys_16(Dictionary_2_t316807927 * value)
	{
		___keys_16 = value;
		Il2CppCodeGenWriteBarrier((&___keys_16), value);
	}

	inline static int32_t get_offset_of_keysList_17() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___keysList_17)); }
	inline List_1_t1062181654 * get_keysList_17() const { return ___keysList_17; }
	inline List_1_t1062181654 ** get_address_of_keysList_17() { return &___keysList_17; }
	inline void set_keysList_17(List_1_t1062181654 * value)
	{
		___keysList_17 = value;
		Il2CppCodeGenWriteBarrier((&___keysList_17), value);
	}

	inline static int32_t get_offset_of_OnUpdate_18() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___OnUpdate_18)); }
	inline UpdateCallback_t3991193291 * get_OnUpdate_18() const { return ___OnUpdate_18; }
	inline UpdateCallback_t3991193291 ** get_address_of_OnUpdate_18() { return &___OnUpdate_18; }
	inline void set_OnUpdate_18(UpdateCallback_t3991193291 * value)
	{
		___OnUpdate_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdate_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline UnityAction_2_t2165061829 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline UnityAction_2_t2165061829 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(UnityAction_2_t2165061829 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(SimpleInput_t4265260572_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline UnityAction_2_t2165061829 * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline UnityAction_2_t2165061829 ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(UnityAction_2_t2165061829 * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEINPUT_T4265260572_H
#ifndef MOUSEBUTTONINPUTUI_T1438106573_H
#define MOUSEBUTTONINPUTUI_T1438106573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.MouseButtonInputUI
struct  MouseButtonInputUI_t1438106573  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/MouseButtonInput SimpleInputNamespace.MouseButtonInputUI::mouseButton
	MouseButtonInput_t2868216434 * ___mouseButton_2;

public:
	inline static int32_t get_offset_of_mouseButton_2() { return static_cast<int32_t>(offsetof(MouseButtonInputUI_t1438106573, ___mouseButton_2)); }
	inline MouseButtonInput_t2868216434 * get_mouseButton_2() const { return ___mouseButton_2; }
	inline MouseButtonInput_t2868216434 ** get_address_of_mouseButton_2() { return &___mouseButton_2; }
	inline void set_mouseButton_2(MouseButtonInput_t2868216434 * value)
	{
		___mouseButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButton_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINPUTUI_T1438106573_H
#ifndef STEERINGWHEEL_T1692308694_H
#define STEERINGWHEEL_T1692308694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.SteeringWheel
struct  SteeringWheel_t1692308694  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.SteeringWheel::axis
	AxisInput_t802457650 * ___axis_2;
	// UnityEngine.UI.Graphic SimpleInputNamespace.SteeringWheel::wheel
	Graphic_t1660335611 * ___wheel_3;
	// UnityEngine.RectTransform SimpleInputNamespace.SteeringWheel::wheelTR
	RectTransform_t3704657025 * ___wheelTR_4;
	// UnityEngine.Vector2 SimpleInputNamespace.SteeringWheel::centerPoint
	Vector2_t2156229523  ___centerPoint_5;
	// System.Single SimpleInputNamespace.SteeringWheel::maximumSteeringAngle
	float ___maximumSteeringAngle_6;
	// System.Single SimpleInputNamespace.SteeringWheel::wheelReleasedSpeed
	float ___wheelReleasedSpeed_7;
	// System.Single SimpleInputNamespace.SteeringWheel::wheelAngle
	float ___wheelAngle_8;
	// System.Single SimpleInputNamespace.SteeringWheel::wheelPrevAngle
	float ___wheelPrevAngle_9;
	// System.Boolean SimpleInputNamespace.SteeringWheel::wheelBeingHeld
	bool ___wheelBeingHeld_10;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___axis_2)); }
	inline AxisInput_t802457650 * get_axis_2() const { return ___axis_2; }
	inline AxisInput_t802457650 ** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(AxisInput_t802457650 * value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}

	inline static int32_t get_offset_of_wheel_3() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheel_3)); }
	inline Graphic_t1660335611 * get_wheel_3() const { return ___wheel_3; }
	inline Graphic_t1660335611 ** get_address_of_wheel_3() { return &___wheel_3; }
	inline void set_wheel_3(Graphic_t1660335611 * value)
	{
		___wheel_3 = value;
		Il2CppCodeGenWriteBarrier((&___wheel_3), value);
	}

	inline static int32_t get_offset_of_wheelTR_4() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelTR_4)); }
	inline RectTransform_t3704657025 * get_wheelTR_4() const { return ___wheelTR_4; }
	inline RectTransform_t3704657025 ** get_address_of_wheelTR_4() { return &___wheelTR_4; }
	inline void set_wheelTR_4(RectTransform_t3704657025 * value)
	{
		___wheelTR_4 = value;
		Il2CppCodeGenWriteBarrier((&___wheelTR_4), value);
	}

	inline static int32_t get_offset_of_centerPoint_5() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___centerPoint_5)); }
	inline Vector2_t2156229523  get_centerPoint_5() const { return ___centerPoint_5; }
	inline Vector2_t2156229523 * get_address_of_centerPoint_5() { return &___centerPoint_5; }
	inline void set_centerPoint_5(Vector2_t2156229523  value)
	{
		___centerPoint_5 = value;
	}

	inline static int32_t get_offset_of_maximumSteeringAngle_6() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___maximumSteeringAngle_6)); }
	inline float get_maximumSteeringAngle_6() const { return ___maximumSteeringAngle_6; }
	inline float* get_address_of_maximumSteeringAngle_6() { return &___maximumSteeringAngle_6; }
	inline void set_maximumSteeringAngle_6(float value)
	{
		___maximumSteeringAngle_6 = value;
	}

	inline static int32_t get_offset_of_wheelReleasedSpeed_7() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelReleasedSpeed_7)); }
	inline float get_wheelReleasedSpeed_7() const { return ___wheelReleasedSpeed_7; }
	inline float* get_address_of_wheelReleasedSpeed_7() { return &___wheelReleasedSpeed_7; }
	inline void set_wheelReleasedSpeed_7(float value)
	{
		___wheelReleasedSpeed_7 = value;
	}

	inline static int32_t get_offset_of_wheelAngle_8() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelAngle_8)); }
	inline float get_wheelAngle_8() const { return ___wheelAngle_8; }
	inline float* get_address_of_wheelAngle_8() { return &___wheelAngle_8; }
	inline void set_wheelAngle_8(float value)
	{
		___wheelAngle_8 = value;
	}

	inline static int32_t get_offset_of_wheelPrevAngle_9() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelPrevAngle_9)); }
	inline float get_wheelPrevAngle_9() const { return ___wheelPrevAngle_9; }
	inline float* get_address_of_wheelPrevAngle_9() { return &___wheelPrevAngle_9; }
	inline void set_wheelPrevAngle_9(float value)
	{
		___wheelPrevAngle_9 = value;
	}

	inline static int32_t get_offset_of_wheelBeingHeld_10() { return static_cast<int32_t>(offsetof(SteeringWheel_t1692308694, ___wheelBeingHeld_10)); }
	inline bool get_wheelBeingHeld_10() const { return ___wheelBeingHeld_10; }
	inline bool* get_address_of_wheelBeingHeld_10() { return &___wheelBeingHeld_10; }
	inline void set_wheelBeingHeld_10(bool value)
	{
		___wheelBeingHeld_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEERINGWHEEL_T1692308694_H
#ifndef TOUCHPAD_T3466294403_H
#define TOUCHPAD_T3466294403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleInputNamespace.Touchpad
struct  Touchpad_t3466294403  : public MonoBehaviour_t3962482529
{
public:
	// SimpleInput/AxisInput SimpleInputNamespace.Touchpad::xAxis
	AxisInput_t802457650 * ___xAxis_2;
	// SimpleInput/AxisInput SimpleInputNamespace.Touchpad::yAxis
	AxisInput_t802457650 * ___yAxis_3;
	// UnityEngine.RectTransform SimpleInputNamespace.Touchpad::rectTransform
	RectTransform_t3704657025 * ___rectTransform_4;
	// System.Single SimpleInputNamespace.Touchpad::sensitivity
	float ___sensitivity_5;
	// System.Boolean SimpleInputNamespace.Touchpad::allowTouchInput
	bool ___allowTouchInput_6;
	// SimpleInputNamespace.Touchpad/MouseButton[] SimpleInputNamespace.Touchpad::allowedMouseButtons
	MouseButtonU5BU5D_t3311405085* ___allowedMouseButtons_7;
	// System.Boolean SimpleInputNamespace.Touchpad::ignoreUIElements
	bool ___ignoreUIElements_8;
	// System.Single SimpleInputNamespace.Touchpad::resolutionMultiplier
	float ___resolutionMultiplier_9;
	// System.Int32 SimpleInputNamespace.Touchpad::fingerId
	int32_t ___fingerId_10;
	// UnityEngine.Vector2 SimpleInputNamespace.Touchpad::prevMouseInputPos
	Vector2_t2156229523  ___prevMouseInputPos_11;
	// System.Boolean SimpleInputNamespace.Touchpad::trackMouseInput
	bool ___trackMouseInput_12;
	// UnityEngine.Vector2 SimpleInputNamespace.Touchpad::m_value
	Vector2_t2156229523  ___m_value_13;

public:
	inline static int32_t get_offset_of_xAxis_2() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___xAxis_2)); }
	inline AxisInput_t802457650 * get_xAxis_2() const { return ___xAxis_2; }
	inline AxisInput_t802457650 ** get_address_of_xAxis_2() { return &___xAxis_2; }
	inline void set_xAxis_2(AxisInput_t802457650 * value)
	{
		___xAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___xAxis_2), value);
	}

	inline static int32_t get_offset_of_yAxis_3() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___yAxis_3)); }
	inline AxisInput_t802457650 * get_yAxis_3() const { return ___yAxis_3; }
	inline AxisInput_t802457650 ** get_address_of_yAxis_3() { return &___yAxis_3; }
	inline void set_yAxis_3(AxisInput_t802457650 * value)
	{
		___yAxis_3 = value;
		Il2CppCodeGenWriteBarrier((&___yAxis_3), value);
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___rectTransform_4)); }
	inline RectTransform_t3704657025 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t3704657025 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_4), value);
	}

	inline static int32_t get_offset_of_sensitivity_5() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___sensitivity_5)); }
	inline float get_sensitivity_5() const { return ___sensitivity_5; }
	inline float* get_address_of_sensitivity_5() { return &___sensitivity_5; }
	inline void set_sensitivity_5(float value)
	{
		___sensitivity_5 = value;
	}

	inline static int32_t get_offset_of_allowTouchInput_6() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___allowTouchInput_6)); }
	inline bool get_allowTouchInput_6() const { return ___allowTouchInput_6; }
	inline bool* get_address_of_allowTouchInput_6() { return &___allowTouchInput_6; }
	inline void set_allowTouchInput_6(bool value)
	{
		___allowTouchInput_6 = value;
	}

	inline static int32_t get_offset_of_allowedMouseButtons_7() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___allowedMouseButtons_7)); }
	inline MouseButtonU5BU5D_t3311405085* get_allowedMouseButtons_7() const { return ___allowedMouseButtons_7; }
	inline MouseButtonU5BU5D_t3311405085** get_address_of_allowedMouseButtons_7() { return &___allowedMouseButtons_7; }
	inline void set_allowedMouseButtons_7(MouseButtonU5BU5D_t3311405085* value)
	{
		___allowedMouseButtons_7 = value;
		Il2CppCodeGenWriteBarrier((&___allowedMouseButtons_7), value);
	}

	inline static int32_t get_offset_of_ignoreUIElements_8() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___ignoreUIElements_8)); }
	inline bool get_ignoreUIElements_8() const { return ___ignoreUIElements_8; }
	inline bool* get_address_of_ignoreUIElements_8() { return &___ignoreUIElements_8; }
	inline void set_ignoreUIElements_8(bool value)
	{
		___ignoreUIElements_8 = value;
	}

	inline static int32_t get_offset_of_resolutionMultiplier_9() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___resolutionMultiplier_9)); }
	inline float get_resolutionMultiplier_9() const { return ___resolutionMultiplier_9; }
	inline float* get_address_of_resolutionMultiplier_9() { return &___resolutionMultiplier_9; }
	inline void set_resolutionMultiplier_9(float value)
	{
		___resolutionMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_fingerId_10() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___fingerId_10)); }
	inline int32_t get_fingerId_10() const { return ___fingerId_10; }
	inline int32_t* get_address_of_fingerId_10() { return &___fingerId_10; }
	inline void set_fingerId_10(int32_t value)
	{
		___fingerId_10 = value;
	}

	inline static int32_t get_offset_of_prevMouseInputPos_11() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___prevMouseInputPos_11)); }
	inline Vector2_t2156229523  get_prevMouseInputPos_11() const { return ___prevMouseInputPos_11; }
	inline Vector2_t2156229523 * get_address_of_prevMouseInputPos_11() { return &___prevMouseInputPos_11; }
	inline void set_prevMouseInputPos_11(Vector2_t2156229523  value)
	{
		___prevMouseInputPos_11 = value;
	}

	inline static int32_t get_offset_of_trackMouseInput_12() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___trackMouseInput_12)); }
	inline bool get_trackMouseInput_12() const { return ___trackMouseInput_12; }
	inline bool* get_address_of_trackMouseInput_12() { return &___trackMouseInput_12; }
	inline void set_trackMouseInput_12(bool value)
	{
		___trackMouseInput_12 = value;
	}

	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Touchpad_t3466294403, ___m_value_13)); }
	inline Vector2_t2156229523  get_m_value_13() const { return ___m_value_13; }
	inline Vector2_t2156229523 * get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(Vector2_t2156229523  value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T3466294403_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef EVENTSYSTEM_T1003666588_H
#define EVENTSYSTEM_T1003666588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t1003666588  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t3491343620 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t2019268878 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1113636619 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1113636619 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t3903027533 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SystemInputModules_2)); }
	inline List_1_t3491343620 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t3491343620 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t3491343620 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t2019268878 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t2019268878 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t2019268878 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_FirstSelected_5)); }
	inline GameObject_t1113636619 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1113636619 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1113636619 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentSelected_8)); }
	inline GameObject_t1113636619 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1113636619 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DummyData_11)); }
	inline BaseEventData_t3903027533 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t3903027533 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t3903027533 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t1003666588_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2475741330 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t3135238028 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2475741330 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2475741330 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2475741330 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t3135238028 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t3135238028 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t3135238028 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T1003666588_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ContactPoint_t3758755253  m_Items[1];

public:
	inline ContactPoint_t3758755253  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContactPoint_t3758755253 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContactPoint_t3758755253  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ContactPoint_t3758755253  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContactPoint_t3758755253 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContactPoint_t3758755253  value)
	{
		m_Items[index] = value;
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SimpleInputNamespace.Touchpad/MouseButton[]
struct MouseButtonU5BU5D_t3311405085  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m4122454247_gshared (UnityAction_2_t2165061829 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Single>::StopTracking()
extern "C"  void BaseInput_2_StopTracking_m339472839_gshared (BaseInput_2_t2943335458 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Boolean>::StopTracking()
extern "C"  void BaseInput_2_StopTracking_m2959303188_gshared (BaseInput_2_t1643356649 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::StopTracking()
extern "C"  void BaseInput_2_StopTracking_m3193694964_gshared (BaseInput_2_t3479630992 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3411363121_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m785745541_gshared (Dictionary_2_t3806807179 * __this, int32_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// K SimpleInputNamespace.BaseInput`2<System.Object,System.Single>::get_Key()
extern "C"  RuntimeObject * BaseInput_2_get_Key_m1282213269_gshared (BaseInput_2_t2943335458 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3474379962_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m1786738978_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// K SimpleInputNamespace.BaseInput`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * BaseInput_2_get_Key_m4006690616_gshared (BaseInput_2_t1643356649 * __this, const RuntimeMethod* method);
// K SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::get_Key()
extern "C"  int32_t BaseInput_2_get_Key_m3054695495_gshared (BaseInput_2_t3479630992 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3327106492_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m4193450060_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, const RuntimeMethod* method);
// K SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::get_Key()
extern "C"  int32_t BaseInput_2_get_Key_m870532662_gshared (BaseInput_2_t1022651380 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m182474683_gshared (Dictionary_2_t3806807179 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m2276339812_gshared (Dictionary_2_t3806807179 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Single>::StartTracking()
extern "C"  void BaseInput_2_StartTracking_m1023462373_gshared (BaseInput_2_t2943335458 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Boolean>::StartTracking()
extern "C"  void BaseInput_2_StartTracking_m1114219140_gshared (BaseInput_2_t1643356649 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::StartTracking()
extern "C"  void BaseInput_2_StartTracking_m985837305_gshared (BaseInput_2_t3479630992 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2601736566_gshared (Dictionary_2_t1968819495 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m4185219368_gshared (Dictionary_2_t3806807179 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Single>::.ctor()
extern "C"  void BaseInput_2__ctor_m865641853_gshared (BaseInput_2_t2943335458 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Single>::.ctor(K)
extern "C"  void BaseInput_2__ctor_m2023899163_gshared (BaseInput_2_t2943335458 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Boolean>::.ctor()
extern "C"  void BaseInput_2__ctor_m3354704284_gshared (BaseInput_2_t1643356649 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Object,System.Boolean>::.ctor(K)
extern "C"  void BaseInput_2__ctor_m3353065918_gshared (BaseInput_2_t1643356649 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::.ctor()
extern "C"  void BaseInput_2__ctor_m4093196004_gshared (BaseInput_2_t1022651380 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::.ctor(K)
extern "C"  void BaseInput_2__ctor_m3612960250_gshared (BaseInput_2_t1022651380 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::.ctor()
extern "C"  void BaseInput_2__ctor_m1777816748_gshared (BaseInput_2_t3479630992 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::.ctor(K)
extern "C"  void BaseInput_2__ctor_m3806450130_gshared (BaseInput_2_t3479630992 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::StartTracking()
extern "C"  void BaseInput_2_StartTracking_m1519213419_gshared (BaseInput_2_t1022651380 * __this, const RuntimeMethod* method);
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::StopTracking()
extern "C"  void BaseInput_2_StopTracking_m3686500270_gshared (BaseInput_2_t1022651380 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m3991588524(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m1794818007 (Material_t340375123 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3916780224_m4049400462(__this, method) ((  Rigidbody_t3916780224 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single SimpleInput::GetAxis(System.String)
extern "C"  float SimpleInput_GetAxis_m2070570208 (RuntimeObject * __this /* static, unused */, String_t* ___axis0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m1660364534 (Transform_t3600365921 * __this, float p0, float p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleInput::GetButtonDown(System.String)
extern "C"  bool SimpleInput_GetButtonDown_m2622109342 (RuntimeObject * __this /* static, unused */, String_t* ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExamplePlayerController::IsGrounded()
extern "C"  bool ExamplePlayerController_IsGrounded_m1847996145 (ExamplePlayerController_t740376480 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m467379572 (Rigidbody_t3916780224 * __this, float p0, float p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddRelativeForce_m438673559 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C"  GameObject_t1113636619 * Collision_get_gameObject_m3662658840 (Collision_t4262080450 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern "C"  bool GameObject_CompareTag_m3144439756 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern "C"  ContactPointU5BU5D_t872956888* Collision_get_contacts_m116535001 (Collision_t4262080450 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
extern "C"  Vector3_t3722313464  ContactPoint_get_normal_m691132423 (ContactPoint_t3758755253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m1059068722 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t3722313464  Vector3_get_down_m3781355428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_Raycast_m1896872038 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<SimpleInput>()
#define GameObject_AddComponent_TisSimpleInput_t4265260572_m2077851558(__this, method) ((  SimpleInput_t4265260572 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m4122454247(__this, p0, p1, method) ((  void (*) (UnityAction_2_t2165061829 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_2__ctor_m4122454247_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.SceneManagement.SceneManager::add_sceneLoaded(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>)
extern "C"  void SceneManager_add_sceneLoaded_m3678832055 (RuntimeObject * __this /* static, unused */, UnityAction_2_t2165061829 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::remove_sceneLoaded(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>)
extern "C"  void SceneManager_remove_sceneLoaded_m2345981904 (RuntimeObject * __this /* static, unused */, UnityAction_2_t2165061829 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<SimpleInput/AxisInput>::get_Item(System.Int32)
#define List_1_get_Item_m4255429017(__this, p0, method) ((  AxisInput_t802457650 * (*) (List_1_t2274532392 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Single>::StopTracking()
#define BaseInput_2_StopTracking_m2000325864(__this, method) ((  void (*) (BaseInput_2_t1381185473 *, const RuntimeMethod*))BaseInput_2_StopTracking_m339472839_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/AxisInput>::get_Count()
#define List_1_get_Count_m1495589272(__this, method) ((  int32_t (*) (List_1_t2274532392 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<SimpleInput/ButtonInput>::get_Item(System.Int32)
#define List_1_get_Item_m2913088823(__this, p0, method) ((  ButtonInput_t2818951903 * (*) (List_1_t4291026645 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>::StopTracking()
#define BaseInput_2_StopTracking_m1977248712(__this, method) ((  void (*) (BaseInput_2_t81206664 *, const RuntimeMethod*))BaseInput_2_StopTracking_m2959303188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/ButtonInput>::get_Count()
#define List_1_get_Count_m828575477(__this, method) ((  int32_t (*) (List_1_t4291026645 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>::get_Item(System.Int32)
#define List_1_get_Item_m2247891234(__this, p0, method) ((  MouseButtonInput_t2868216434 * (*) (List_1_t45323880 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::StopTracking()
#define BaseInput_2_StopTracking_m3193694964(__this, method) ((  void (*) (BaseInput_2_t3479630992 *, const RuntimeMethod*))BaseInput_2_StopTracking_m3193694964_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/AxisInput>::Clear()
#define List_1_Clear_m1109486988(__this, method) ((  void (*) (List_1_t2274532392 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/ButtonInput>::Clear()
#define List_1_Clear_m3184015962(__this, method) ((  void (*) (List_1_t4291026645 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>::Clear()
#define List_1_Clear_m1732670702(__this, method) ((  void (*) (List_1_t45323880 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1425023151(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1968145125 *, String_t*, Axis_t2182888826 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.Void SimpleInput::TrackAxis(System.String,System.Boolean)
extern "C"  void SimpleInput_TrackAxis_m1843737917 (RuntimeObject * __this /* static, unused */, String_t* ___axis0, bool ___trackUnityAxisOnly1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1636010796(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2716226479 *, String_t*, Button_t2930970180 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.Void SimpleInput::TrackButton(System.String,System.Boolean)
extern "C"  void SimpleInput_TrackButton_m2675713569 (RuntimeObject * __this /* static, unused */, String_t* ___button0, bool ___trackUnityButtonOnly1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m579209773(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t4181968371 *, int32_t, MouseButton_t998287744 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3411363121_gshared)(__this, p0, p1, method)
// System.Void SimpleInput::TrackMouseButton(System.Int32,System.Boolean)
extern "C"  void SimpleInput_TrackMouseButton_m959883669 (RuntimeObject * __this /* static, unused */, int32_t ___button0, bool ___trackUnityMouseButtonOnly1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m822832064(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t316807927 *, int32_t, Key_t3885074208 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m785745541_gshared)(__this, p0, p1, method)
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C"  bool Input_GetKey_m3736388334 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyUp_m2808015270 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// K SimpleInputNamespace.BaseInput`2<System.String,System.Single>::get_Key()
#define BaseInput_2_get_Key_m784409413(__this, method) ((  String_t* (*) (BaseInput_2_t1381185473 *, const RuntimeMethod*))BaseInput_2_get_Key_m1282213269_gshared)(__this, method)
// System.Void SimpleInput/Axis::.ctor(System.String)
extern "C"  void Axis__ctor_m2063451953 (Axis_t2182888826 * __this, String_t* ___axis0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2398515240(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1968145125 *, String_t*, Axis_t2182888826 *, const RuntimeMethod*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/Axis>::Add(!0)
#define List_1_Add_m1209423649(__this, p0, method) ((  void (*) (List_1_t3654963568 *, Axis_t2182888826 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/AxisInput>::Add(!0)
#define List_1_Add_m3590501126(__this, p0, method) ((  void (*) (List_1_t2274532392 *, AxisInput_t802457650 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/AxisInput>::Remove(!0)
#define List_1_Remove_m466206242(__this, p0, method) ((  bool (*) (List_1_t2274532392 *, AxisInput_t802457650 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>::Remove(!0)
#define Dictionary_2_Remove_m2962106831(__this, p0, method) ((  bool (*) (Dictionary_2_t1968145125 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m1786738978_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/Axis>::Remove(!0)
#define List_1_Remove_m2391931732(__this, p0, method) ((  bool (*) (List_1_t3654963568 *, Axis_t2182888826 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// K SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>::get_Key()
#define BaseInput_2_get_Key_m1061843947(__this, method) ((  String_t* (*) (BaseInput_2_t81206664 *, const RuntimeMethod*))BaseInput_2_get_Key_m4006690616_gshared)(__this, method)
// System.Void SimpleInput/Button::.ctor(System.String)
extern "C"  void Button__ctor_m1639548820 (Button_t2930970180 * __this, String_t* ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m232276741(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2716226479 *, String_t*, Button_t2930970180 *, const RuntimeMethod*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/Button>::Add(!0)
#define List_1_Add_m1978629791(__this, p0, method) ((  void (*) (List_1_t108077626 *, Button_t2930970180 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/ButtonInput>::Add(!0)
#define List_1_Add_m577318652(__this, p0, method) ((  void (*) (List_1_t4291026645 *, ButtonInput_t2818951903 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/ButtonInput>::Remove(!0)
#define List_1_Remove_m1939984325(__this, p0, method) ((  bool (*) (List_1_t4291026645 *, ButtonInput_t2818951903 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>::Remove(!0)
#define Dictionary_2_Remove_m1980553089(__this, p0, method) ((  bool (*) (Dictionary_2_t2716226479 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m1786738978_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/Button>::Remove(!0)
#define List_1_Remove_m3740224469(__this, p0, method) ((  bool (*) (List_1_t108077626 *, Button_t2930970180 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// K SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::get_Key()
#define BaseInput_2_get_Key_m3054695495(__this, method) ((  int32_t (*) (BaseInput_2_t3479630992 *, const RuntimeMethod*))BaseInput_2_get_Key_m3054695495_gshared)(__this, method)
// System.Void SimpleInput/MouseButton::.ctor(System.Int32)
extern "C"  void MouseButton__ctor_m2922259096 (MouseButton_t998287744 * __this, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3172288348(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4181968371 *, int32_t, MouseButton_t998287744 *, const RuntimeMethod*))Dictionary_2_set_Item_m3327106492_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/MouseButton>::Add(!0)
#define List_1_Add_m1746165051(__this, p0, method) ((  void (*) (List_1_t2470362486 *, MouseButton_t998287744 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>::Add(!0)
#define List_1_Add_m1188574635(__this, p0, method) ((  void (*) (List_1_t45323880 *, MouseButtonInput_t2868216434 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>::Remove(!0)
#define List_1_Remove_m2942097792(__this, p0, method) ((  bool (*) (List_1_t45323880 *, MouseButtonInput_t2868216434 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>::get_Count()
#define List_1_get_Count_m3237777153(__this, method) ((  int32_t (*) (List_1_t45323880 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>::Remove(!0)
#define Dictionary_2_Remove_m2637628108(__this, p0, method) ((  bool (*) (Dictionary_2_t4181968371 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m4193450060_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/MouseButton>::Remove(!0)
#define List_1_Remove_m2631527114(__this, p0, method) ((  bool (*) (List_1_t2470362486 *, MouseButton_t998287744 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// K SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::get_Key()
#define BaseInput_2_get_Key_m870532662(__this, method) ((  int32_t (*) (BaseInput_2_t1022651380 *, const RuntimeMethod*))BaseInput_2_get_Key_m870532662_gshared)(__this, method)
// System.Void SimpleInput/Key::.ctor(UnityEngine.KeyCode)
extern "C"  void Key__ctor_m707106284 (Key_t3885074208 * __this, int32_t ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1481711019(__this, p0, p1, method) ((  void (*) (Dictionary_2_t316807927 *, int32_t, Key_t3885074208 *, const RuntimeMethod*))Dictionary_2_set_Item_m182474683_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/Key>::Add(!0)
#define List_1_Add_m2761925027(__this, p0, method) ((  void (*) (List_1_t1062181654 *, Key_t3885074208 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/KeyInput>::Add(!0)
#define List_1_Add_m2434912339(__this, p0, method) ((  void (*) (List_1_t570456766 *, KeyInput_t3393349320 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/KeyInput>::Remove(!0)
#define List_1_Remove_m3715497796(__this, p0, method) ((  bool (*) (List_1_t570456766 *, KeyInput_t3393349320 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/KeyInput>::get_Count()
#define List_1_get_Count_m3615089172(__this, method) ((  int32_t (*) (List_1_t570456766 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>::Remove(!0)
#define Dictionary_2_Remove_m1979789194(__this, p0, method) ((  bool (*) (Dictionary_2_t316807927 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2276339812_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<SimpleInput/Key>::Remove(!0)
#define List_1_Remove_m372239913(__this, p0, method) ((  bool (*) (List_1_t1062181654 *, Key_t3885074208 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Void SimpleInput/AxisInput::.ctor(System.String)
extern "C"  void AxisInput__ctor_m635020897 (AxisInput_t802457650 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m2316819917 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Single>::StartTracking()
#define BaseInput_2_StartTracking_m3188229854(__this, method) ((  void (*) (BaseInput_2_t1381185473 *, const RuntimeMethod*))BaseInput_2_StartTracking_m1023462373_gshared)(__this, method)
// System.Void SimpleInput/ButtonInput::.ctor(System.String)
extern "C"  void ButtonInput__ctor_m1973775960 (ButtonInput_t2818951903 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C"  bool Input_GetButton_m2064261504 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>::StartTracking()
#define BaseInput_2_StartTracking_m3481582819(__this, method) ((  void (*) (BaseInput_2_t81206664 *, const RuntimeMethod*))BaseInput_2_StartTracking_m1114219140_gshared)(__this, method)
// System.Void SimpleInput/MouseButtonInput::.ctor(System.Int32)
extern "C"  void MouseButtonInput__ctor_m1842993350 (MouseButtonInput_t2868216434 * __this, int32_t ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m513753021 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::StartTracking()
#define BaseInput_2_StartTracking_m985837305(__this, method) ((  void (*) (BaseInput_2_t3479630992 *, const RuntimeMethod*))BaseInput_2_StartTracking_m985837305_gshared)(__this, method)
// System.Void SimpleInput/UpdateCallback::Invoke()
extern "C"  void UpdateCallback_Invoke_m894695456 (UpdateCallback_t3991193291 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<SimpleInput/Axis>::get_Item(System.Int32)
#define List_1_get_Item_m2001209520(__this, p0, method) ((  Axis_t2182888826 * (*) (List_1_t3654963568 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1004423579 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<SimpleInput/Axis>::get_Count()
#define List_1_get_Count_m1399466053(__this, method) ((  int32_t (*) (List_1_t3654963568 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<SimpleInput/Button>::get_Item(System.Int32)
#define List_1_get_Item_m182785779(__this, p0, method) ((  Button_t2930970180 * (*) (List_1_t108077626 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/Button>::get_Count()
#define List_1_get_Count_m377990033(__this, method) ((  int32_t (*) (List_1_t108077626 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<SimpleInput/MouseButton>::get_Item(System.Int32)
#define List_1_get_Item_m3336004944(__this, p0, method) ((  MouseButton_t998287744 * (*) (List_1_t2470362486 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/MouseButton>::get_Count()
#define List_1_get_Count_m326005465(__this, method) ((  int32_t (*) (List_1_t2470362486 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<SimpleInput/Key>::get_Item(System.Int32)
#define List_1_get_Item_m1284462838(__this, p0, method) ((  Key_t3885074208 * (*) (List_1_t1062181654 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<SimpleInput/KeyInput>::get_Item(System.Int32)
#define List_1_get_Item_m1602638289(__this, p0, method) ((  KeyInput_t3393349320 * (*) (List_1_t570456766 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<SimpleInput/Key>::get_Count()
#define List_1_get_Count_m108667433(__this, method) ((  int32_t (*) (List_1_t1062181654 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Axis>::.ctor()
#define Dictionary_2__ctor_m1219153817(__this, method) ((  void (*) (Dictionary_2_t1968145125 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/Axis>::.ctor()
#define List_1__ctor_m3492050649(__this, method) ((  void (*) (List_1_t3654963568 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/AxisInput>::.ctor()
#define List_1__ctor_m2629046078(__this, method) ((  void (*) (List_1_t2274532392 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleInput/Button>::.ctor()
#define Dictionary_2__ctor_m1157345819(__this, method) ((  void (*) (Dictionary_2_t2716226479 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/Button>::.ctor()
#define List_1__ctor_m4217368305(__this, method) ((  void (*) (List_1_t108077626 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/ButtonInput>::.ctor()
#define List_1__ctor_m770714582(__this, method) ((  void (*) (List_1_t4291026645 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SimpleInput/MouseButton>::.ctor()
#define Dictionary_2__ctor_m1685523410(__this, method) ((  void (*) (Dictionary_2_t4181968371 *, const RuntimeMethod*))Dictionary_2__ctor_m2601736566_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/MouseButton>::.ctor()
#define List_1__ctor_m2002102647(__this, method) ((  void (*) (List_1_t2470362486 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/MouseButtonInput>::.ctor()
#define List_1__ctor_m256120419(__this, method) ((  void (*) (List_1_t45323880 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,SimpleInput/Key>::.ctor()
#define Dictionary_2__ctor_m2001987697(__this, method) ((  void (*) (Dictionary_2_t316807927 *, const RuntimeMethod*))Dictionary_2__ctor_m4185219368_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SimpleInput/Key>::.ctor()
#define List_1__ctor_m2112490249(__this, method) ((  void (*) (List_1_t1062181654 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Single>::.ctor()
#define BaseInput_2__ctor_m1791511703(__this, method) ((  void (*) (BaseInput_2_t1381185473 *, const RuntimeMethod*))BaseInput_2__ctor_m865641853_gshared)(__this, method)
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Single>::.ctor(K)
#define BaseInput_2__ctor_m449874524(__this, p0, method) ((  void (*) (BaseInput_2_t1381185473 *, String_t*, const RuntimeMethod*))BaseInput_2__ctor_m2023899163_gshared)(__this, p0, method)
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::RegisterAxis(SimpleInput/AxisInput)
extern "C"  void SimpleInput_RegisterAxis_m496276425 (RuntimeObject * __this /* static, unused */, AxisInput_t802457650 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::UnregisterAxis(SimpleInput/AxisInput)
extern "C"  void SimpleInput_UnregisterAxis_m2397270711 (RuntimeObject * __this /* static, unused */, AxisInput_t802457650 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>::.ctor()
#define BaseInput_2__ctor_m3207761156(__this, method) ((  void (*) (BaseInput_2_t81206664 *, const RuntimeMethod*))BaseInput_2__ctor_m3354704284_gshared)(__this, method)
// System.Void SimpleInputNamespace.BaseInput`2<System.String,System.Boolean>::.ctor(K)
#define BaseInput_2__ctor_m3206253858(__this, p0, method) ((  void (*) (BaseInput_2_t81206664 *, String_t*, const RuntimeMethod*))BaseInput_2__ctor_m3353065918_gshared)(__this, p0, method)
// System.Void SimpleInput::RegisterButton(SimpleInput/ButtonInput)
extern "C"  void SimpleInput_RegisterButton_m3324302350 (RuntimeObject * __this /* static, unused */, ButtonInput_t2818951903 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::UnregisterButton(SimpleInput/ButtonInput)
extern "C"  void SimpleInput_UnregisterButton_m1428040134 (RuntimeObject * __this /* static, unused */, ButtonInput_t2818951903 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<SimpleInput/KeyInput>::.ctor()
#define List_1__ctor_m2757187041(__this, method) ((  void (*) (List_1_t570456766 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::.ctor()
#define BaseInput_2__ctor_m4093196004(__this, method) ((  void (*) (BaseInput_2_t1022651380 *, const RuntimeMethod*))BaseInput_2__ctor_m4093196004_gshared)(__this, method)
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::.ctor(K)
#define BaseInput_2__ctor_m3612960250(__this, p0, method) ((  void (*) (BaseInput_2_t1022651380 *, int32_t, const RuntimeMethod*))BaseInput_2__ctor_m3612960250_gshared)(__this, p0, method)
// System.Void SimpleInput::RegisterKey(SimpleInput/KeyInput)
extern "C"  void SimpleInput_RegisterKey_m2116541909 (RuntimeObject * __this /* static, unused */, KeyInput_t3393349320 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::UnregisterKey(SimpleInput/KeyInput)
extern "C"  void SimpleInput_UnregisterKey_m3562015723 (RuntimeObject * __this /* static, unused */, KeyInput_t3393349320 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::.ctor()
#define BaseInput_2__ctor_m1777816748(__this, method) ((  void (*) (BaseInput_2_t3479630992 *, const RuntimeMethod*))BaseInput_2__ctor_m1777816748_gshared)(__this, method)
// System.Void SimpleInputNamespace.BaseInput`2<System.Int32,System.Boolean>::.ctor(K)
#define BaseInput_2__ctor_m3806450130(__this, p0, method) ((  void (*) (BaseInput_2_t3479630992 *, int32_t, const RuntimeMethod*))BaseInput_2__ctor_m3806450130_gshared)(__this, p0, method)
// System.Void SimpleInput::RegisterMouseButton(SimpleInput/MouseButtonInput)
extern "C"  void SimpleInput_RegisterMouseButton_m4261908703 (RuntimeObject * __this /* static, unused */, MouseButtonInput_t2868216434 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::UnregisterMouseButton(SimpleInput/MouseButtonInput)
extern "C"  void SimpleInput_UnregisterMouseButton_m1845058510 (RuntimeObject * __this /* static, unused */, MouseButtonInput_t2868216434 * ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput/AxisInput::.ctor()
extern "C"  void AxisInput__ctor_m143791316 (AxisInput_t802457650 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput/UpdateCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCallback__ctor_m3190082255 (UpdateCallback_t3991193291 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::add_OnUpdate(SimpleInput/UpdateCallback)
extern "C"  void SimpleInput_add_OnUpdate_m521018632 (RuntimeObject * __this /* static, unused */, UpdateCallback_t3991193291 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput::remove_OnUpdate(SimpleInput/UpdateCallback)
extern "C"  void SimpleInput_remove_OnUpdate_m4020446573 (RuntimeObject * __this /* static, unused */, UpdateCallback_t3991193291 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
#define Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, method) ((  Graphic_t1660335611 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void SimpleInput/ButtonInput::.ctor()
extern "C"  void ButtonInput__ctor_m2907722220 (ButtonInput_t2818951903 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<SimpleInputNamespace.SimpleInputDragListener>()
#define GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994(__this, method) ((  SimpleInputDragListener_t3058349199 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void SimpleInputNamespace.SimpleInputDragListener::set_Listener(SimpleInputNamespace.ISimpleInputDraggable)
extern "C"  void SimpleInputDragListener_set_Listener_m3326121527 (SimpleInputDragListener_t3058349199 * __this, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.Dpad::CalculateInput(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Dpad_CalculateInput_m4151013375 (Dpad_t1007306070 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
extern "C"  Vector2_t2156229523  PointerEventData_get_position_m437660275 (PointerEventData_t3807901092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Canvas UnityEngine.UI.Graphic::get_canvas()
extern "C"  Canvas_t3310196443 * Graphic_get_canvas_m3320066409 (Graphic_t1660335611 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C"  Camera_t4157153871 * Canvas_get_worldCamera_m3516267897 (Canvas_t3310196443 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * p0, Vector2_t2156229523  p1, Camera_t4157153871 * p2, Vector2_t2156229523 * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m837837635 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t2156229523  Vector2_get_right_m1027081661 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Angle_m4105581454 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C"  RectTransform_t3704657025 * Graphic_get_rectTransform_m1167152468 (Graphic_t1660335611 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void SimpleInputNamespace.Joystick::OnUpdate()
extern "C"  void Joystick_OnUpdate_m3745511237 (Joystick_t1225167045 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m1350607670 (GameObject_t1113636619 * __this, String_t* p0, TypeU5BU5D_t3940880105* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(__this, method) ((  RectTransform_t3704657025 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m273471670 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m253439912 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4230103102 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2156229523  Vector2_get_one_m738793577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2998668828 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m3462269772 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Image>()
#define GameObject_AddComponent_TisImage_t2670269651_m1594579417(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// UnityEngine.Sprite UnityEngine.UI.Image::get_sprite()
extern "C"  Sprite_t280657092 * Image_get_sprite_m1811690853 (Image_t2670269651 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t2555686324  Color_get_clear_m1016382534 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t2156229523  Vector2_get_normalized_m2683665860 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInput/KeyInput::.ctor()
extern "C"  void KeyInput__ctor_m3791521000 (KeyInput_t3393349320 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::StartTracking()
#define BaseInput_2_StartTracking_m1519213419(__this, method) ((  void (*) (BaseInput_2_t1022651380 *, const RuntimeMethod*))BaseInput_2_StartTracking_m1519213419_gshared)(__this, method)
// System.Void SimpleInputNamespace.BaseInput`2<UnityEngine.KeyCode,System.Boolean>::StopTracking()
#define BaseInput_2_StopTracking_m3686500270(__this, method) ((  void (*) (BaseInput_2_t1022651380 *, const RuntimeMethod*))BaseInput_2_StopTracking_m3686500270_gshared)(__this, method)
// System.Void SimpleInput/MouseButtonInput::.ctor()
extern "C"  void MouseButtonInput__ctor_m247368948 (MouseButtonInput_t2868216434 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SimpleInputNamespace.ISimpleInputDraggable SimpleInputNamespace.SimpleInputDragListener::get_Listener()
extern "C"  RuntimeObject* SimpleInputDragListener_get_Listener_m1909111161 (SimpleInputDragListener_t3058349199 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.EventSystems.PointerEventData::get_pointerId()
extern "C"  int32_t PointerEventData_get_pointerId_m1200356155 (PointerEventData_t3807901092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_pointerDrag(UnityEngine.GameObject)
extern "C"  void PointerEventData_set_pointerDrag_m841976018 (PointerEventData_t3807901092 * __this, GameObject_t1113636619 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m4202601546 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single SimpleInputNamespace.SteeringWheel::get_Value()
extern "C"  float SteeringWheel_get_Value_m1547502318 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t2156229523  Vector2_get_up_m2647420593 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Distance_m3048868881 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleInputNamespace.SteeringWheel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SteeringWheel_OnDrag_m1834180620 (SteeringWheel_t1692308694 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
extern "C"  EventSystem_t1003666588 * EventSystem_get_current_m1416377559 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::Set(System.Single,System.Single)
extern "C"  void Vector2_Set_m3780194483 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m214549210 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2)
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m3246909541 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m859576425 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.EventSystem::IsPointerOverGameObject(System.Int32)
extern "C"  bool EventSystem_IsPointerOverGameObject_m301566329 (EventSystem_t1003666588 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t2156229523  Touch_get_deltaPosition_m2389653382 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleInputNamespace.Touchpad::GetMouseButtonDown()
extern "C"  bool Touchpad_GetMouseButtonDown_m102808409 (Touchpad_t3466294403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.EventSystem::IsPointerOverGameObject()
extern "C"  bool EventSystem_IsPointerOverGameObject_m1911785875 (EventSystem_t1003666588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleInputNamespace.Touchpad::GetMouseButton()
extern "C"  bool Touchpad_GetMouseButton_m3020692098 (Touchpad_t3466294403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2081676745 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ExamplePlayerController::.ctor()
extern "C"  void ExamplePlayerController__ctor_m649646620 (ExamplePlayerController_t740376480 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExamplePlayerController__ctor_m649646620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string horizontalAxis = "Horizontal";
		__this->set_horizontalAxis_4(_stringLiteral1828639942);
		// public string verticalAxis = "Vertical";
		__this->set_verticalAxis_5(_stringLiteral2984908384);
		// public string jumpButton = "Jump";
		__this->set_jumpButton_6(_stringLiteral1930566815);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExamplePlayerController::Awake()
extern "C"  void ExamplePlayerController_Awake_m384171372 (ExamplePlayerController_t740376480 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExamplePlayerController_Awake_m384171372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<Renderer>().material.color = materialColor;
		// GetComponent<Renderer>().material.color = materialColor;
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m3991588524(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m3991588524_RuntimeMethod_var);
		// GetComponent<Renderer>().material.color = materialColor;
		NullCheck(L_0);
		Material_t340375123 * L_1 = Renderer_get_material_m4171603682(L_0, /*hidden argument*/NULL);
		Color_t2555686324  L_2 = __this->get_materialColor_2();
		// GetComponent<Renderer>().material.color = materialColor;
		NullCheck(L_1);
		Material_set_color_m1794818007(L_1, L_2, /*hidden argument*/NULL);
		// m_rigidbody = GetComponent<Rigidbody>();
		// m_rigidbody = GetComponent<Rigidbody>();
		Rigidbody_t3916780224 * L_3 = Component_GetComponent_TisRigidbody_t3916780224_m4049400462(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3916780224_m4049400462_RuntimeMethod_var);
		__this->set_m_rigidbody_3(L_3);
		// }
		return;
	}
}
// System.Void ExamplePlayerController::Update()
extern "C"  void ExamplePlayerController_Update_m1857921205 (ExamplePlayerController_t740376480 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExamplePlayerController_Update_m1857921205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// inputHorizontal = SimpleInput.GetAxis( horizontalAxis );
		String_t* L_0 = __this->get_horizontalAxis_4();
		// inputHorizontal = SimpleInput.GetAxis( horizontalAxis );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		float L_1 = SimpleInput_GetAxis_m2070570208(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_inputHorizontal_7(L_1);
		// inputVertical = SimpleInput.GetAxis( verticalAxis );
		String_t* L_2 = __this->get_verticalAxis_5();
		// inputVertical = SimpleInput.GetAxis( verticalAxis );
		float L_3 = SimpleInput_GetAxis_m2070570208(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_inputVertical_8(L_3);
		// transform.Rotate( 0f, inputHorizontal * 5f, 0f, Space.World );
		// transform.Rotate( 0f, inputHorizontal * 5f, 0f, Space.World );
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_inputHorizontal_7();
		// transform.Rotate( 0f, inputHorizontal * 5f, 0f, Space.World );
		NullCheck(L_4);
		Transform_Rotate_m1660364534(L_4, (0.0f), ((float)il2cpp_codegen_multiply((float)L_5, (float)(5.0f))), (0.0f), 0, /*hidden argument*/NULL);
		// if( SimpleInput.GetButtonDown( jumpButton ) && IsGrounded() )
		String_t* L_6 = __this->get_jumpButton_6();
		// if( SimpleInput.GetButtonDown( jumpButton ) && IsGrounded() )
		bool L_7 = SimpleInput_GetButtonDown_m2622109342(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		// if( SimpleInput.GetButtonDown( jumpButton ) && IsGrounded() )
		bool L_8 = ExamplePlayerController_IsGrounded_m1847996145(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_007b;
		}
	}
	{
		// m_rigidbody.AddForce( 0f, 10f, 0f, ForceMode.Impulse );
		Rigidbody_t3916780224 * L_9 = __this->get_m_rigidbody_3();
		// m_rigidbody.AddForce( 0f, 10f, 0f, ForceMode.Impulse );
		NullCheck(L_9);
		Rigidbody_AddForce_m467379572(L_9, (0.0f), (10.0f), (0.0f), 1, /*hidden argument*/NULL);
	}

IL_007b:
	{
		// }
		return;
	}
}
// System.Void ExamplePlayerController::FixedUpdate()
extern "C"  void ExamplePlayerController_FixedUpdate_m2865340580 (ExamplePlayerController_t740376480 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExamplePlayerController_FixedUpdate_m2865340580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_rigidbody.AddRelativeForce( new Vector3( 0f, 0f, inputVertical ) * 20f );
		Rigidbody_t3916780224 * L_0 = __this->get_m_rigidbody_3();
		float L_1 = __this->get_inputVertical_8();
		// m_rigidbody.AddRelativeForce( new Vector3( 0f, 0f, inputVertical ) * 20f );
		Vector3_t3722313464  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m3353183577((&L_2), (0.0f), (0.0f), L_1, /*hidden argument*/NULL);
		// m_rigidbody.AddRelativeForce( new Vector3( 0f, 0f, inputVertical ) * 20f );
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_2, (20.0f), /*hidden argument*/NULL);
		// m_rigidbody.AddRelativeForce( new Vector3( 0f, 0f, inputVertical ) * 20f );
		NullCheck(L_0);
		Rigidbody_AddRelativeForce_m438673559(L_0, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ExamplePlayerController::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void ExamplePlayerController_OnCollisionEnter_m4084468661 (ExamplePlayerController_t740376480 * __this, Collision_t4262080450 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExamplePlayerController_OnCollisionEnter_m4084468661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( collision.gameObject.CompareTag( "Player" ) )
		Collision_t4262080450 * L_0 = ___collision0;
		// if( collision.gameObject.CompareTag( "Player" ) )
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Collision_get_gameObject_m3662658840(L_0, /*hidden argument*/NULL);
		// if( collision.gameObject.CompareTag( "Player" ) )
		NullCheck(L_1);
		bool L_2 = GameObject_CompareTag_m3144439756(L_1, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		// m_rigidbody.AddForce( collision.contacts[0].normal * 10f, ForceMode.Impulse );
		Rigidbody_t3916780224 * L_3 = __this->get_m_rigidbody_3();
		Collision_t4262080450 * L_4 = ___collision0;
		// m_rigidbody.AddForce( collision.contacts[0].normal * 10f, ForceMode.Impulse );
		NullCheck(L_4);
		ContactPointU5BU5D_t872956888* L_5 = Collision_get_contacts_m116535001(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		// m_rigidbody.AddForce( collision.contacts[0].normal * 10f, ForceMode.Impulse );
		Vector3_t3722313464  L_6 = ContactPoint_get_normal_m691132423(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		// m_rigidbody.AddForce( collision.contacts[0].normal * 10f, ForceMode.Impulse );
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, (10.0f), /*hidden argument*/NULL);
		// m_rigidbody.AddForce( collision.contacts[0].normal * 10f, ForceMode.Impulse );
		NullCheck(L_3);
		Rigidbody_AddForce_m1059068722(L_3, L_7, 1, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// }
		return;
	}
}
// System.Boolean ExamplePlayerController::IsGrounded()
extern "C"  bool ExamplePlayerController_IsGrounded_m1847996145 (ExamplePlayerController_t740376480 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExamplePlayerController_IsGrounded_m1847996145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// return Physics.Raycast( transform.position, Vector3.down, 1.75f );
		// return Physics.Raycast( transform.position, Vector3.down, 1.75f );
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// return Physics.Raycast( transform.position, Vector3.down, 1.75f );
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		// return Physics.Raycast( transform.position, Vector3.down, 1.75f );
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		// return Physics.Raycast( transform.position, Vector3.down, 1.75f );
		bool L_3 = Physics_Raycast_m1896872038(NULL /*static, unused*/, L_1, L_2, (1.75f), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		// }
		bool L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput::.ctor()
extern "C"  void SimpleInput__ctor_m2393977280 (SimpleInput_t4265260572 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInput::add_OnUpdate(SimpleInput/UpdateCallback)
extern "C"  void SimpleInput_add_OnUpdate_m521018632 (RuntimeObject * __this /* static, unused */, UpdateCallback_t3991193291 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_add_OnUpdate_m521018632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdateCallback_t3991193291 * V_0 = NULL;
	UpdateCallback_t3991193291 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UpdateCallback_t3991193291 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_OnUpdate_18();
		V_0 = L_0;
	}

IL_0006:
	{
		UpdateCallback_t3991193291 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UpdateCallback_t3991193291 * L_2 = V_1;
		UpdateCallback_t3991193291 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UpdateCallback_t3991193291 * L_5 = V_0;
		UpdateCallback_t3991193291 * L_6 = InterlockedCompareExchangeImpl<UpdateCallback_t3991193291 *>((((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_address_of_OnUpdate_18()), ((UpdateCallback_t3991193291 *)CastclassSealed((RuntimeObject*)L_4, UpdateCallback_t3991193291_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UpdateCallback_t3991193291 * L_7 = V_0;
		UpdateCallback_t3991193291 * L_8 = V_1;
		if ((!(((RuntimeObject*)(UpdateCallback_t3991193291 *)L_7) == ((RuntimeObject*)(UpdateCallback_t3991193291 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void SimpleInput::remove_OnUpdate(SimpleInput/UpdateCallback)
extern "C"  void SimpleInput_remove_OnUpdate_m4020446573 (RuntimeObject * __this /* static, unused */, UpdateCallback_t3991193291 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_remove_OnUpdate_m4020446573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdateCallback_t3991193291 * V_0 = NULL;
	UpdateCallback_t3991193291 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UpdateCallback_t3991193291 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_OnUpdate_18();
		V_0 = L_0;
	}

IL_0006:
	{
		UpdateCallback_t3991193291 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UpdateCallback_t3991193291 * L_2 = V_1;
		UpdateCallback_t3991193291 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UpdateCallback_t3991193291 * L_5 = V_0;
		UpdateCallback_t3991193291 * L_6 = InterlockedCompareExchangeImpl<UpdateCallback_t3991193291 *>((((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_address_of_OnUpdate_18()), ((UpdateCallback_t3991193291 *)CastclassSealed((RuntimeObject*)L_4, UpdateCallback_t3991193291_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UpdateCallback_t3991193291 * L_7 = V_0;
		UpdateCallback_t3991193291 * L_8 = V_1;
		if ((!(((RuntimeObject*)(UpdateCallback_t3991193291 *)L_7) == ((RuntimeObject*)(UpdateCallback_t3991193291 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void SimpleInput::Init()
extern "C"  void SimpleInput_Init_m124578375 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_Init_m124578375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// instance = new GameObject( "SimpleInput" ).AddComponent<SimpleInput>();
		// instance = new GameObject( "SimpleInput" ).AddComponent<SimpleInput>();
		GameObject_t1113636619 * L_0 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_0, _stringLiteral3260436178, /*hidden argument*/NULL);
		// instance = new GameObject( "SimpleInput" ).AddComponent<SimpleInput>();
		NullCheck(L_0);
		SimpleInput_t4265260572 * L_1 = GameObject_AddComponent_TisSimpleInput_t4265260572_m2077851558(L_0, /*hidden argument*/GameObject_AddComponent_TisSimpleInput_t4265260572_m2077851558_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_instance_3(L_1);
		// DontDestroyOnLoad( instance.gameObject );
		SimpleInput_t4265260572 * L_2 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_instance_3();
		// DontDestroyOnLoad( instance.gameObject );
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		// DontDestroyOnLoad( instance.gameObject );
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInput::Awake()
extern "C"  void SimpleInput_Awake_m215113224 (SimpleInput_t4265260572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_Awake_m215113224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.sceneLoaded += OnSceneChanged;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UnityAction_2_t2165061829 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_19();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		intptr_t L_1 = (intptr_t)SimpleInput_OnSceneChanged_m3517309758_RuntimeMethod_var;
		UnityAction_2_t2165061829 * L_2 = (UnityAction_2_t2165061829 *)il2cpp_codegen_object_new(UnityAction_2_t2165061829_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m4122454247(L_2, NULL, L_1, /*hidden argument*/UnityAction_2__ctor_m4122454247_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_19(L_2);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UnityAction_2_t2165061829 * L_3 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_19();
		// SceneManager.sceneLoaded += OnSceneChanged;
		SceneManager_add_sceneLoaded_m3678832055(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInput::Start()
extern "C"  void SimpleInput_Start_m925969886 (SimpleInput_t4265260572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_Start_m925969886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( this != instance )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_t4265260572 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_instance_3();
		// if( this != instance )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		// Destroy( this );
		// Destroy( this );
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::OnDestroy()
extern "C"  void SimpleInput_OnDestroy_m3382805764 (SimpleInput_t4265260572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_OnDestroy_m3382805764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.sceneLoaded -= OnSceneChanged;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UnityAction_2_t2165061829 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_20();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		intptr_t L_1 = (intptr_t)SimpleInput_OnSceneChanged_m3517309758_RuntimeMethod_var;
		UnityAction_2_t2165061829 * L_2 = (UnityAction_2_t2165061829 *)il2cpp_codegen_object_new(UnityAction_2_t2165061829_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m4122454247(L_2, NULL, L_1, /*hidden argument*/UnityAction_2__ctor_m4122454247_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_20(L_2);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UnityAction_2_t2165061829 * L_3 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_20();
		// SceneManager.sceneLoaded -= OnSceneChanged;
		SceneManager_remove_sceneLoaded_m2345981904(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInput::OnSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SimpleInput_OnSceneChanged_m3517309758 (RuntimeObject * __this /* static, unused */, Scene_t2348375561  ___scene0, int32_t ___loadSceneMode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_OnSceneChanged_m3517309758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// for( int i = 0; i < trackedTemporaryAxes.Count; i++ )
		V_0 = 0;
		goto IL_001c;
	}

IL_0008:
	{
		// trackedTemporaryAxes[i].StopTracking();
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2274532392 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryAxes_7();
		int32_t L_1 = V_0;
		// trackedTemporaryAxes[i].StopTracking();
		NullCheck(L_0);
		AxisInput_t802457650 * L_2 = List_1_get_Item_m4255429017(L_0, L_1, /*hidden argument*/List_1_get_Item_m4255429017_RuntimeMethod_var);
		// trackedTemporaryAxes[i].StopTracking();
		NullCheck(L_2);
		BaseInput_2_StopTracking_m2000325864(L_2, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// for( int i = 0; i < trackedTemporaryAxes.Count; i++ )
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_001c:
	{
		// for( int i = 0; i < trackedTemporaryAxes.Count; i++ )
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2274532392 * L_5 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryAxes_7();
		// for( int i = 0; i < trackedTemporaryAxes.Count; i++ )
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m1495589272(L_5, /*hidden argument*/List_1_get_Count_m1495589272_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0008;
		}
	}
	{
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		V_1 = 0;
		goto IL_0047;
	}

IL_0033:
	{
		// trackedTemporaryButtons[i].StopTracking();
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t4291026645 * L_7 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryButtons_11();
		int32_t L_8 = V_1;
		// trackedTemporaryButtons[i].StopTracking();
		NullCheck(L_7);
		ButtonInput_t2818951903 * L_9 = List_1_get_Item_m2913088823(L_7, L_8, /*hidden argument*/List_1_get_Item_m2913088823_RuntimeMethod_var);
		// trackedTemporaryButtons[i].StopTracking();
		NullCheck(L_9);
		BaseInput_2_StopTracking_m1977248712(L_9, /*hidden argument*/BaseInput_2_StopTracking_m1977248712_RuntimeMethod_var);
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0047:
	{
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t4291026645 * L_12 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryButtons_11();
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m828575477(L_12, /*hidden argument*/List_1_get_Count_m828575477_RuntimeMethod_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0033;
		}
	}
	{
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		V_2 = 0;
		goto IL_0072;
	}

IL_005e:
	{
		// trackedTemporaryMouseButtons[i].StopTracking();
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t45323880 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryMouseButtons_15();
		int32_t L_15 = V_2;
		// trackedTemporaryMouseButtons[i].StopTracking();
		NullCheck(L_14);
		MouseButtonInput_t2868216434 * L_16 = List_1_get_Item_m2247891234(L_14, L_15, /*hidden argument*/List_1_get_Item_m2247891234_RuntimeMethod_var);
		// trackedTemporaryMouseButtons[i].StopTracking();
		NullCheck(L_16);
		BaseInput_2_StopTracking_m3193694964(L_16, /*hidden argument*/BaseInput_2_StopTracking_m3193694964_RuntimeMethod_var);
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		int32_t L_17 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_0072:
	{
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t4291026645 * L_19 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryButtons_11();
		// for( int i = 0; i < trackedTemporaryButtons.Count; i++ )
		NullCheck(L_19);
		int32_t L_20 = List_1_get_Count_m828575477(L_19, /*hidden argument*/List_1_get_Count_m828575477_RuntimeMethod_var);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_005e;
		}
	}
	{
		// trackedTemporaryAxes.Clear();
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2274532392 * L_21 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryAxes_7();
		// trackedTemporaryAxes.Clear();
		NullCheck(L_21);
		List_1_Clear_m1109486988(L_21, /*hidden argument*/List_1_Clear_m1109486988_RuntimeMethod_var);
		// trackedTemporaryButtons.Clear();
		List_1_t4291026645 * L_22 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryButtons_11();
		// trackedTemporaryButtons.Clear();
		NullCheck(L_22);
		List_1_Clear_m3184015962(L_22, /*hidden argument*/List_1_Clear_m3184015962_RuntimeMethod_var);
		// trackedTemporaryMouseButtons.Clear();
		List_1_t45323880 * L_23 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryMouseButtons_15();
		// trackedTemporaryMouseButtons.Clear();
		NullCheck(L_23);
		List_1_Clear_m1732670702(L_23, /*hidden argument*/List_1_Clear_m1732670702_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Single SimpleInput::GetAxis(System.String)
extern "C"  float SimpleInput_GetAxis_m2070570208 (RuntimeObject * __this /* static, unused */, String_t* ___axis0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetAxis_m2070570208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Axis_t2182888826 * V_0 = NULL;
	float V_1 = 0.0f;
	{
		// if( axes.TryGetValue( axis, out axisInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t1968145125 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axes_4();
		String_t* L_1 = ___axis0;
		// if( axes.TryGetValue( axis, out axisInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1425023151(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1425023151_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// return axisInput.value;
		Axis_t2182888826 * L_3 = V_0;
		NullCheck(L_3);
		float L_4 = L_3->get_value_2();
		V_1 = L_4;
		goto IL_0031;
	}

IL_001f:
	{
		// TrackAxis( axis );
		String_t* L_5 = ___axis0;
		// TrackAxis( axis );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackAxis_m1843737917(NULL /*static, unused*/, L_5, (bool)0, /*hidden argument*/NULL);
		// return 0f;
		V_1 = (0.0f);
		goto IL_0031;
	}

IL_0031:
	{
		// }
		float L_6 = V_1;
		return L_6;
	}
}
// System.Single SimpleInput::GetAxisRaw(System.String)
extern "C"  float SimpleInput_GetAxisRaw_m3854594848 (RuntimeObject * __this /* static, unused */, String_t* ___axis0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetAxisRaw_m3854594848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Axis_t2182888826 * V_0 = NULL;
	float V_1 = 0.0f;
	{
		// if( axes.TryGetValue( axis, out axisInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t1968145125 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axes_4();
		String_t* L_1 = ___axis0;
		// if( axes.TryGetValue( axis, out axisInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1425023151(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1425023151_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// return axisInput.valueRaw;
		Axis_t2182888826 * L_3 = V_0;
		NullCheck(L_3);
		float L_4 = L_3->get_valueRaw_3();
		V_1 = L_4;
		goto IL_0031;
	}

IL_001f:
	{
		// TrackAxis( axis );
		String_t* L_5 = ___axis0;
		// TrackAxis( axis );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackAxis_m1843737917(NULL /*static, unused*/, L_5, (bool)0, /*hidden argument*/NULL);
		// return 0f;
		V_1 = (0.0f);
		goto IL_0031;
	}

IL_0031:
	{
		// }
		float L_6 = V_1;
		return L_6;
	}
}
// System.Boolean SimpleInput::GetButtonDown(System.String)
extern "C"  bool SimpleInput_GetButtonDown_m2622109342 (RuntimeObject * __this /* static, unused */, String_t* ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetButtonDown_m2622109342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2930970180 * V_0 = NULL;
	bool V_1 = false;
	{
		// if( buttons.TryGetValue( button, out buttonInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		String_t* L_1 = ___button0;
		// if( buttons.TryGetValue( button, out buttonInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1636010796(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1636010796_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		// return buttonInput.state == InputState.Pressed;
		Button_t2930970180 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		goto IL_0030;
	}

IL_0022:
	{
		// TrackButton( button );
		String_t* L_5 = ___button0;
		// TrackButton( button );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackButton_m2675713569(NULL /*static, unused*/, L_5, (bool)0, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_0030;
	}

IL_0030:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean SimpleInput::GetButton(System.String)
extern "C"  bool SimpleInput_GetButton_m2783520745 (RuntimeObject * __this /* static, unused */, String_t* ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetButton_m2783520745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2930970180 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		// if( buttons.TryGetValue( button, out buttonInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		String_t* L_1 = ___button0;
		// if( buttons.TryGetValue( button, out buttonInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1636010796(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1636010796_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		// return buttonInput.state == InputState.Held || buttonInput.state == InputState.Pressed;
		Button_t2930970180 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_002a;
		}
	}
	{
		Button_t2930970180 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_state_2();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		V_1 = (bool)G_B4_0;
		goto IL_003f;
	}

IL_0031:
	{
		// TrackButton( button );
		String_t* L_7 = ___button0;
		// TrackButton( button );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackButton_m2675713569(NULL /*static, unused*/, L_7, (bool)0, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_003f;
	}

IL_003f:
	{
		// }
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean SimpleInput::GetButtonUp(System.String)
extern "C"  bool SimpleInput_GetButtonUp_m4050978607 (RuntimeObject * __this /* static, unused */, String_t* ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetButtonUp_m4050978607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2930970180 * V_0 = NULL;
	bool V_1 = false;
	{
		// if( buttons.TryGetValue( button, out buttonInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		String_t* L_1 = ___button0;
		// if( buttons.TryGetValue( button, out buttonInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1636010796(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1636010796_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		// return buttonInput.state == InputState.Released;
		Button_t2930970180 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0030;
	}

IL_0022:
	{
		// TrackButton( button );
		String_t* L_5 = ___button0;
		// TrackButton( button );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackButton_m2675713569(NULL /*static, unused*/, L_5, (bool)0, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_0030;
	}

IL_0030:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean SimpleInput::GetMouseButtonDown(System.Int32)
extern "C"  bool SimpleInput_GetMouseButtonDown_m4152109214 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetMouseButtonDown_m4152109214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseButton_t998287744 * V_0 = NULL;
	bool V_1 = false;
	{
		// if( mouseButtons.TryGetValue( button, out mouseButtonInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		int32_t L_1 = ___button0;
		// if( mouseButtons.TryGetValue( button, out mouseButtonInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m579209773(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m579209773_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		// return mouseButtonInput.state == InputState.Pressed;
		MouseButton_t998287744 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		goto IL_0030;
	}

IL_0022:
	{
		// TrackMouseButton( button );
		int32_t L_5 = ___button0;
		// TrackMouseButton( button );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackMouseButton_m959883669(NULL /*static, unused*/, L_5, (bool)0, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_0030;
	}

IL_0030:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean SimpleInput::GetMouseButton(System.Int32)
extern "C"  bool SimpleInput_GetMouseButton_m3558780159 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetMouseButton_m3558780159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseButton_t998287744 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		// if( mouseButtons.TryGetValue( button, out mouseButtonInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		int32_t L_1 = ___button0;
		// if( mouseButtons.TryGetValue( button, out mouseButtonInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m579209773(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m579209773_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		// return mouseButtonInput.state == InputState.Held || mouseButtonInput.state == InputState.Pressed;
		MouseButton_t998287744 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_002a;
		}
	}
	{
		MouseButton_t998287744 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_state_2();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		V_1 = (bool)G_B4_0;
		goto IL_003f;
	}

IL_0031:
	{
		// TrackMouseButton( button );
		int32_t L_7 = ___button0;
		// TrackMouseButton( button );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackMouseButton_m959883669(NULL /*static, unused*/, L_7, (bool)0, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_003f;
	}

IL_003f:
	{
		// }
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean SimpleInput::GetMouseButtonUp(System.Int32)
extern "C"  bool SimpleInput_GetMouseButtonUp_m3042222655 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetMouseButtonUp_m3042222655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseButton_t998287744 * V_0 = NULL;
	bool V_1 = false;
	{
		// if( mouseButtons.TryGetValue( button, out mouseButtonInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		int32_t L_1 = ___button0;
		// if( mouseButtons.TryGetValue( button, out mouseButtonInput ) )
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m579209773(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m579209773_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		// return mouseButtonInput.state == InputState.Released;
		MouseButton_t998287744 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0030;
	}

IL_0022:
	{
		// TrackMouseButton( button );
		int32_t L_5 = ___button0;
		// TrackMouseButton( button );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_TrackMouseButton_m959883669(NULL /*static, unused*/, L_5, (bool)0, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_0030;
	}

IL_0030:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean SimpleInput::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool SimpleInput_GetKeyDown_m41503968 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetKeyDown_m41503968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Key_t3885074208 * V_1 = NULL;
	{
		// if( Input.GetKeyDown( key ) )
		int32_t L_0 = ___key0;
		// if( Input.GetKeyDown( key ) )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_003b;
	}

IL_0013:
	{
		// if( keys.TryGetValue( key, out keyInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_2 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		int32_t L_3 = ___key0;
		// if( keys.TryGetValue( key, out keyInput ) )
		NullCheck(L_2);
		bool L_4 = Dictionary_2_TryGetValue_m822832064(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m822832064_RuntimeMethod_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// return keyInput.state == InputState.Pressed;
		Key_t3885074208 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_state_2();
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		goto IL_003b;
	}

IL_0034:
	{
		// return false;
		V_0 = (bool)0;
		goto IL_003b;
	}

IL_003b:
	{
		// }
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean SimpleInput::GetKey(UnityEngine.KeyCode)
extern "C"  bool SimpleInput_GetKey_m2675519645 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetKey_m2675519645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Key_t3885074208 * V_1 = NULL;
	int32_t G_B6_0 = 0;
	{
		// if( Input.GetKey( key ) )
		int32_t L_0 = ___key0;
		// if( Input.GetKey( key ) )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_004a;
	}

IL_0013:
	{
		// if( keys.TryGetValue( key, out keyInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_2 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		int32_t L_3 = ___key0;
		// if( keys.TryGetValue( key, out keyInput ) )
		NullCheck(L_2);
		bool L_4 = Dictionary_2_TryGetValue_m822832064(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m822832064_RuntimeMethod_var);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		// return keyInput.state == InputState.Held || keyInput.state == InputState.Pressed;
		Key_t3885074208 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_state_2();
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_003c;
		}
	}
	{
		Key_t3885074208 * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_state_2();
		G_B6_0 = ((((int32_t)L_8) == ((int32_t)1))? 1 : 0);
		goto IL_003d;
	}

IL_003c:
	{
		G_B6_0 = 1;
	}

IL_003d:
	{
		V_0 = (bool)G_B6_0;
		goto IL_004a;
	}

IL_0043:
	{
		// return false;
		V_0 = (bool)0;
		goto IL_004a;
	}

IL_004a:
	{
		// }
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Boolean SimpleInput::GetKeyUp(UnityEngine.KeyCode)
extern "C"  bool SimpleInput_GetKeyUp_m3571899093 (RuntimeObject * __this /* static, unused */, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_GetKeyUp_m3571899093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Key_t3885074208 * V_1 = NULL;
	{
		// if( Input.GetKeyUp( key ) )
		int32_t L_0 = ___key0;
		// if( Input.GetKeyUp( key ) )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUp_m2808015270(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_003b;
	}

IL_0013:
	{
		// if( keys.TryGetValue( key, out keyInput ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_2 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		int32_t L_3 = ___key0;
		// if( keys.TryGetValue( key, out keyInput ) )
		NullCheck(L_2);
		bool L_4 = Dictionary_2_TryGetValue_m822832064(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m822832064_RuntimeMethod_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// return keyInput.state == InputState.Released;
		Key_t3885074208 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_state_2();
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)3))? 1 : 0);
		goto IL_003b;
	}

IL_0034:
	{
		// return false;
		V_0 = (bool)0;
		goto IL_003b;
	}

IL_003b:
	{
		// }
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void SimpleInput::RegisterAxis(SimpleInput/AxisInput)
extern "C"  void SimpleInput_RegisterAxis_m496276425 (RuntimeObject * __this /* static, unused */, AxisInput_t802457650 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_RegisterAxis_m496276425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Axis_t2182888826 * V_0 = NULL;
	{
		// if( !axes.TryGetValue( input.Key, out axisObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t1968145125 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axes_4();
		AxisInput_t802457650 * L_1 = ___input0;
		// if( !axes.TryGetValue( input.Key, out axisObj ) )
		NullCheck(L_1);
		String_t* L_2 = BaseInput_2_get_Key_m784409413(L_1, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// if( !axes.TryGetValue( input.Key, out axisObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m1425023151(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1425023151_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_004e;
		}
	}
	{
		// axisObj = new Axis( input.Key );
		AxisInput_t802457650 * L_4 = ___input0;
		// axisObj = new Axis( input.Key );
		NullCheck(L_4);
		String_t* L_5 = BaseInput_2_get_Key_m784409413(L_4, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// axisObj = new Axis( input.Key );
		Axis_t2182888826 * L_6 = (Axis_t2182888826 *)il2cpp_codegen_object_new(Axis_t2182888826_il2cpp_TypeInfo_var);
		Axis__ctor_m2063451953(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// axes[input.Key] = axisObj;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t1968145125 * L_7 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axes_4();
		AxisInput_t802457650 * L_8 = ___input0;
		// axes[input.Key] = axisObj;
		NullCheck(L_8);
		String_t* L_9 = BaseInput_2_get_Key_m784409413(L_8, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		Axis_t2182888826 * L_10 = V_0;
		// axes[input.Key] = axisObj;
		NullCheck(L_7);
		Dictionary_2_set_Item_m2398515240(L_7, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m2398515240_RuntimeMethod_var);
		// axesList.Add( axisObj );
		List_1_t3654963568 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axesList_5();
		Axis_t2182888826 * L_12 = V_0;
		// axesList.Add( axisObj );
		NullCheck(L_11);
		List_1_Add_m1209423649(L_11, L_12, /*hidden argument*/List_1_Add_m1209423649_RuntimeMethod_var);
		// TrackAxis( input.Key, true );
		AxisInput_t802457650 * L_13 = ___input0;
		// TrackAxis( input.Key, true );
		NullCheck(L_13);
		String_t* L_14 = BaseInput_2_get_Key_m784409413(L_13, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// TrackAxis( input.Key, true );
		SimpleInput_TrackAxis_m1843737917(NULL /*static, unused*/, L_14, (bool)1, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// axisObj.inputs.Add( input );
		Axis_t2182888826 * L_15 = V_0;
		NullCheck(L_15);
		List_1_t2274532392 * L_16 = L_15->get_inputs_1();
		AxisInput_t802457650 * L_17 = ___input0;
		// axisObj.inputs.Add( input );
		NullCheck(L_16);
		List_1_Add_m3590501126(L_16, L_17, /*hidden argument*/List_1_Add_m3590501126_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInput::UnregisterAxis(SimpleInput/AxisInput)
extern "C"  void SimpleInput_UnregisterAxis_m2397270711 (RuntimeObject * __this /* static, unused */, AxisInput_t802457650 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_UnregisterAxis_m2397270711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Axis_t2182888826 * V_0 = NULL;
	{
		// if( axes.TryGetValue( input.Key, out axisObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t1968145125 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axes_4();
		AxisInput_t802457650 * L_1 = ___input0;
		// if( axes.TryGetValue( input.Key, out axisObj ) )
		NullCheck(L_1);
		String_t* L_2 = BaseInput_2_get_Key_m784409413(L_1, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// if( axes.TryGetValue( input.Key, out axisObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m1425023151(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1425023151_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		// if( axisObj.inputs.Remove( input ) && axisObj.inputs.Count == 0 )
		Axis_t2182888826 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t2274532392 * L_5 = L_4->get_inputs_1();
		AxisInput_t802457650 * L_6 = ___input0;
		// if( axisObj.inputs.Remove( input ) && axisObj.inputs.Count == 0 )
		NullCheck(L_5);
		bool L_7 = List_1_Remove_m466206242(L_5, L_6, /*hidden argument*/List_1_Remove_m466206242_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		Axis_t2182888826 * L_8 = V_0;
		NullCheck(L_8);
		List_1_t2274532392 * L_9 = L_8->get_inputs_1();
		// if( axisObj.inputs.Remove( input ) && axisObj.inputs.Count == 0 )
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m1495589272(L_9, /*hidden argument*/List_1_get_Count_m1495589272_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_0059;
		}
	}
	{
		// axes.Remove( input.Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t1968145125 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axes_4();
		AxisInput_t802457650 * L_12 = ___input0;
		// axes.Remove( input.Key );
		NullCheck(L_12);
		String_t* L_13 = BaseInput_2_get_Key_m784409413(L_12, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// axes.Remove( input.Key );
		NullCheck(L_11);
		Dictionary_2_Remove_m2962106831(L_11, L_13, /*hidden argument*/Dictionary_2_Remove_m2962106831_RuntimeMethod_var);
		// axesList.Remove( axisObj );
		List_1_t3654963568 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axesList_5();
		Axis_t2182888826 * L_15 = V_0;
		// axesList.Remove( axisObj );
		NullCheck(L_14);
		List_1_Remove_m2391931732(L_14, L_15, /*hidden argument*/List_1_Remove_m2391931732_RuntimeMethod_var);
	}

IL_0059:
	{
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::RegisterButton(SimpleInput/ButtonInput)
extern "C"  void SimpleInput_RegisterButton_m3324302350 (RuntimeObject * __this /* static, unused */, ButtonInput_t2818951903 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_RegisterButton_m3324302350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2930970180 * V_0 = NULL;
	{
		// if( !buttons.TryGetValue( input.Key, out buttonObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		ButtonInput_t2818951903 * L_1 = ___input0;
		// if( !buttons.TryGetValue( input.Key, out buttonObj ) )
		NullCheck(L_1);
		String_t* L_2 = BaseInput_2_get_Key_m1061843947(L_1, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// if( !buttons.TryGetValue( input.Key, out buttonObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m1636010796(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1636010796_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_004e;
		}
	}
	{
		// buttonObj = new Button( input.Key );
		ButtonInput_t2818951903 * L_4 = ___input0;
		// buttonObj = new Button( input.Key );
		NullCheck(L_4);
		String_t* L_5 = BaseInput_2_get_Key_m1061843947(L_4, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// buttonObj = new Button( input.Key );
		Button_t2930970180 * L_6 = (Button_t2930970180 *)il2cpp_codegen_object_new(Button_t2930970180_il2cpp_TypeInfo_var);
		Button__ctor_m1639548820(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// buttons[input.Key] = buttonObj;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_7 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		ButtonInput_t2818951903 * L_8 = ___input0;
		// buttons[input.Key] = buttonObj;
		NullCheck(L_8);
		String_t* L_9 = BaseInput_2_get_Key_m1061843947(L_8, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		Button_t2930970180 * L_10 = V_0;
		// buttons[input.Key] = buttonObj;
		NullCheck(L_7);
		Dictionary_2_set_Item_m232276741(L_7, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m232276741_RuntimeMethod_var);
		// buttonsList.Add( buttonObj );
		List_1_t108077626 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttonsList_9();
		Button_t2930970180 * L_12 = V_0;
		// buttonsList.Add( buttonObj );
		NullCheck(L_11);
		List_1_Add_m1978629791(L_11, L_12, /*hidden argument*/List_1_Add_m1978629791_RuntimeMethod_var);
		// TrackButton( input.Key, true );
		ButtonInput_t2818951903 * L_13 = ___input0;
		// TrackButton( input.Key, true );
		NullCheck(L_13);
		String_t* L_14 = BaseInput_2_get_Key_m1061843947(L_13, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// TrackButton( input.Key, true );
		SimpleInput_TrackButton_m2675713569(NULL /*static, unused*/, L_14, (bool)1, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// buttonObj.inputs.Add( input );
		Button_t2930970180 * L_15 = V_0;
		NullCheck(L_15);
		List_1_t4291026645 * L_16 = L_15->get_inputs_1();
		ButtonInput_t2818951903 * L_17 = ___input0;
		// buttonObj.inputs.Add( input );
		NullCheck(L_16);
		List_1_Add_m577318652(L_16, L_17, /*hidden argument*/List_1_Add_m577318652_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInput::UnregisterButton(SimpleInput/ButtonInput)
extern "C"  void SimpleInput_UnregisterButton_m1428040134 (RuntimeObject * __this /* static, unused */, ButtonInput_t2818951903 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_UnregisterButton_m1428040134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2930970180 * V_0 = NULL;
	{
		// if( buttons.TryGetValue( input.Key, out buttonObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		ButtonInput_t2818951903 * L_1 = ___input0;
		// if( buttons.TryGetValue( input.Key, out buttonObj ) )
		NullCheck(L_1);
		String_t* L_2 = BaseInput_2_get_Key_m1061843947(L_1, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// if( buttons.TryGetValue( input.Key, out buttonObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m1636010796(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1636010796_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		// if( buttonObj.inputs.Remove( input ) && buttonObj.inputs.Count == 0 )
		Button_t2930970180 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t4291026645 * L_5 = L_4->get_inputs_1();
		ButtonInput_t2818951903 * L_6 = ___input0;
		// if( buttonObj.inputs.Remove( input ) && buttonObj.inputs.Count == 0 )
		NullCheck(L_5);
		bool L_7 = List_1_Remove_m1939984325(L_5, L_6, /*hidden argument*/List_1_Remove_m1939984325_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		Button_t2930970180 * L_8 = V_0;
		NullCheck(L_8);
		List_1_t4291026645 * L_9 = L_8->get_inputs_1();
		// if( buttonObj.inputs.Remove( input ) && buttonObj.inputs.Count == 0 )
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m828575477(L_9, /*hidden argument*/List_1_get_Count_m828575477_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_0059;
		}
	}
	{
		// buttons.Remove( input.Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t2716226479 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttons_8();
		ButtonInput_t2818951903 * L_12 = ___input0;
		// buttons.Remove( input.Key );
		NullCheck(L_12);
		String_t* L_13 = BaseInput_2_get_Key_m1061843947(L_12, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// buttons.Remove( input.Key );
		NullCheck(L_11);
		Dictionary_2_Remove_m1980553089(L_11, L_13, /*hidden argument*/Dictionary_2_Remove_m1980553089_RuntimeMethod_var);
		// buttonsList.Remove( buttonObj );
		List_1_t108077626 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttonsList_9();
		Button_t2930970180 * L_15 = V_0;
		// buttonsList.Remove( buttonObj );
		NullCheck(L_14);
		List_1_Remove_m3740224469(L_14, L_15, /*hidden argument*/List_1_Remove_m3740224469_RuntimeMethod_var);
	}

IL_0059:
	{
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::RegisterMouseButton(SimpleInput/MouseButtonInput)
extern "C"  void SimpleInput_RegisterMouseButton_m4261908703 (RuntimeObject * __this /* static, unused */, MouseButtonInput_t2868216434 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_RegisterMouseButton_m4261908703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseButton_t998287744 * V_0 = NULL;
	{
		// if( !mouseButtons.TryGetValue( input.Key, out mouseButtonObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		MouseButtonInput_t2868216434 * L_1 = ___input0;
		// if( !mouseButtons.TryGetValue( input.Key, out mouseButtonObj ) )
		NullCheck(L_1);
		int32_t L_2 = BaseInput_2_get_Key_m3054695495(L_1, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		// if( !mouseButtons.TryGetValue( input.Key, out mouseButtonObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m579209773(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m579209773_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_004e;
		}
	}
	{
		// mouseButtonObj = new MouseButton( input.Key );
		MouseButtonInput_t2868216434 * L_4 = ___input0;
		// mouseButtonObj = new MouseButton( input.Key );
		NullCheck(L_4);
		int32_t L_5 = BaseInput_2_get_Key_m3054695495(L_4, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		// mouseButtonObj = new MouseButton( input.Key );
		MouseButton_t998287744 * L_6 = (MouseButton_t998287744 *)il2cpp_codegen_object_new(MouseButton_t998287744_il2cpp_TypeInfo_var);
		MouseButton__ctor_m2922259096(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// mouseButtons[input.Key] = mouseButtonObj;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_7 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		MouseButtonInput_t2868216434 * L_8 = ___input0;
		// mouseButtons[input.Key] = mouseButtonObj;
		NullCheck(L_8);
		int32_t L_9 = BaseInput_2_get_Key_m3054695495(L_8, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		MouseButton_t998287744 * L_10 = V_0;
		// mouseButtons[input.Key] = mouseButtonObj;
		NullCheck(L_7);
		Dictionary_2_set_Item_m3172288348(L_7, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m3172288348_RuntimeMethod_var);
		// mouseButtonsList.Add( mouseButtonObj );
		List_1_t2470362486 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtonsList_13();
		MouseButton_t998287744 * L_12 = V_0;
		// mouseButtonsList.Add( mouseButtonObj );
		NullCheck(L_11);
		List_1_Add_m1746165051(L_11, L_12, /*hidden argument*/List_1_Add_m1746165051_RuntimeMethod_var);
		// TrackMouseButton( input.Key, true );
		MouseButtonInput_t2868216434 * L_13 = ___input0;
		// TrackMouseButton( input.Key, true );
		NullCheck(L_13);
		int32_t L_14 = BaseInput_2_get_Key_m3054695495(L_13, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		// TrackMouseButton( input.Key, true );
		SimpleInput_TrackMouseButton_m959883669(NULL /*static, unused*/, L_14, (bool)1, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// mouseButtonObj.inputs.Add( input );
		MouseButton_t998287744 * L_15 = V_0;
		NullCheck(L_15);
		List_1_t45323880 * L_16 = L_15->get_inputs_1();
		MouseButtonInput_t2868216434 * L_17 = ___input0;
		// mouseButtonObj.inputs.Add( input );
		NullCheck(L_16);
		List_1_Add_m1188574635(L_16, L_17, /*hidden argument*/List_1_Add_m1188574635_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInput::UnregisterMouseButton(SimpleInput/MouseButtonInput)
extern "C"  void SimpleInput_UnregisterMouseButton_m1845058510 (RuntimeObject * __this /* static, unused */, MouseButtonInput_t2868216434 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_UnregisterMouseButton_m1845058510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseButton_t998287744 * V_0 = NULL;
	{
		// if( mouseButtons.TryGetValue( input.Key, out mouseButtonObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		MouseButtonInput_t2868216434 * L_1 = ___input0;
		// if( mouseButtons.TryGetValue( input.Key, out mouseButtonObj ) )
		NullCheck(L_1);
		int32_t L_2 = BaseInput_2_get_Key_m3054695495(L_1, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		// if( mouseButtons.TryGetValue( input.Key, out mouseButtonObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m579209773(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m579209773_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		// if( mouseButtonObj.inputs.Remove( input ) && mouseButtonObj.inputs.Count == 0 )
		MouseButton_t998287744 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t45323880 * L_5 = L_4->get_inputs_1();
		MouseButtonInput_t2868216434 * L_6 = ___input0;
		// if( mouseButtonObj.inputs.Remove( input ) && mouseButtonObj.inputs.Count == 0 )
		NullCheck(L_5);
		bool L_7 = List_1_Remove_m2942097792(L_5, L_6, /*hidden argument*/List_1_Remove_m2942097792_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		MouseButton_t998287744 * L_8 = V_0;
		NullCheck(L_8);
		List_1_t45323880 * L_9 = L_8->get_inputs_1();
		// if( mouseButtonObj.inputs.Remove( input ) && mouseButtonObj.inputs.Count == 0 )
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m3237777153(L_9, /*hidden argument*/List_1_get_Count_m3237777153_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_0059;
		}
	}
	{
		// mouseButtons.Remove( input.Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t4181968371 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtons_12();
		MouseButtonInput_t2868216434 * L_12 = ___input0;
		// mouseButtons.Remove( input.Key );
		NullCheck(L_12);
		int32_t L_13 = BaseInput_2_get_Key_m3054695495(L_12, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		// mouseButtons.Remove( input.Key );
		NullCheck(L_11);
		Dictionary_2_Remove_m2637628108(L_11, L_13, /*hidden argument*/Dictionary_2_Remove_m2637628108_RuntimeMethod_var);
		// mouseButtonsList.Remove( mouseButtonObj );
		List_1_t2470362486 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtonsList_13();
		MouseButton_t998287744 * L_15 = V_0;
		// mouseButtonsList.Remove( mouseButtonObj );
		NullCheck(L_14);
		List_1_Remove_m2631527114(L_14, L_15, /*hidden argument*/List_1_Remove_m2631527114_RuntimeMethod_var);
	}

IL_0059:
	{
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::RegisterKey(SimpleInput/KeyInput)
extern "C"  void SimpleInput_RegisterKey_m2116541909 (RuntimeObject * __this /* static, unused */, KeyInput_t3393349320 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_RegisterKey_m2116541909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Key_t3885074208 * V_0 = NULL;
	{
		// if( !keys.TryGetValue( input.Key, out keyObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		KeyInput_t3393349320 * L_1 = ___input0;
		// if( !keys.TryGetValue( input.Key, out keyObj ) )
		NullCheck(L_1);
		int32_t L_2 = BaseInput_2_get_Key_m870532662(L_1, /*hidden argument*/BaseInput_2_get_Key_m870532662_RuntimeMethod_var);
		// if( !keys.TryGetValue( input.Key, out keyObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m822832064(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m822832064_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		// keyObj = new Key( input.Key );
		KeyInput_t3393349320 * L_4 = ___input0;
		// keyObj = new Key( input.Key );
		NullCheck(L_4);
		int32_t L_5 = BaseInput_2_get_Key_m870532662(L_4, /*hidden argument*/BaseInput_2_get_Key_m870532662_RuntimeMethod_var);
		// keyObj = new Key( input.Key );
		Key_t3885074208 * L_6 = (Key_t3885074208 *)il2cpp_codegen_object_new(Key_t3885074208_il2cpp_TypeInfo_var);
		Key__ctor_m707106284(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// keys[input.Key] = keyObj;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_7 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		KeyInput_t3393349320 * L_8 = ___input0;
		// keys[input.Key] = keyObj;
		NullCheck(L_8);
		int32_t L_9 = BaseInput_2_get_Key_m870532662(L_8, /*hidden argument*/BaseInput_2_get_Key_m870532662_RuntimeMethod_var);
		Key_t3885074208 * L_10 = V_0;
		// keys[input.Key] = keyObj;
		NullCheck(L_7);
		Dictionary_2_set_Item_m1481711019(L_7, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m1481711019_RuntimeMethod_var);
		// keysList.Add( keyObj );
		List_1_t1062181654 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keysList_17();
		Key_t3885074208 * L_12 = V_0;
		// keysList.Add( keyObj );
		NullCheck(L_11);
		List_1_Add_m2761925027(L_11, L_12, /*hidden argument*/List_1_Add_m2761925027_RuntimeMethod_var);
	}

IL_0042:
	{
		// keyObj.inputs.Add( input );
		Key_t3885074208 * L_13 = V_0;
		NullCheck(L_13);
		List_1_t570456766 * L_14 = L_13->get_inputs_1();
		KeyInput_t3393349320 * L_15 = ___input0;
		// keyObj.inputs.Add( input );
		NullCheck(L_14);
		List_1_Add_m2434912339(L_14, L_15, /*hidden argument*/List_1_Add_m2434912339_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInput::UnregisterKey(SimpleInput/KeyInput)
extern "C"  void SimpleInput_UnregisterKey_m3562015723 (RuntimeObject * __this /* static, unused */, KeyInput_t3393349320 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_UnregisterKey_m3562015723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Key_t3885074208 * V_0 = NULL;
	{
		// if( keys.TryGetValue( input.Key, out keyObj ) )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		KeyInput_t3393349320 * L_1 = ___input0;
		// if( keys.TryGetValue( input.Key, out keyObj ) )
		NullCheck(L_1);
		int32_t L_2 = BaseInput_2_get_Key_m870532662(L_1, /*hidden argument*/BaseInput_2_get_Key_m870532662_RuntimeMethod_var);
		// if( keys.TryGetValue( input.Key, out keyObj ) )
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m822832064(L_0, L_2, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m822832064_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		// if( keyObj.inputs.Remove( input ) && keyObj.inputs.Count == 0 )
		Key_t3885074208 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t570456766 * L_5 = L_4->get_inputs_1();
		KeyInput_t3393349320 * L_6 = ___input0;
		// if( keyObj.inputs.Remove( input ) && keyObj.inputs.Count == 0 )
		NullCheck(L_5);
		bool L_7 = List_1_Remove_m3715497796(L_5, L_6, /*hidden argument*/List_1_Remove_m3715497796_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		Key_t3885074208 * L_8 = V_0;
		NullCheck(L_8);
		List_1_t570456766 * L_9 = L_8->get_inputs_1();
		// if( keyObj.inputs.Remove( input ) && keyObj.inputs.Count == 0 )
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m3615089172(L_9, /*hidden argument*/List_1_get_Count_m3615089172_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_0059;
		}
	}
	{
		// keys.Remove( input.Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		Dictionary_2_t316807927 * L_11 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keys_16();
		KeyInput_t3393349320 * L_12 = ___input0;
		// keys.Remove( input.Key );
		NullCheck(L_12);
		int32_t L_13 = BaseInput_2_get_Key_m870532662(L_12, /*hidden argument*/BaseInput_2_get_Key_m870532662_RuntimeMethod_var);
		// keys.Remove( input.Key );
		NullCheck(L_11);
		Dictionary_2_Remove_m1979789194(L_11, L_13, /*hidden argument*/Dictionary_2_Remove_m1979789194_RuntimeMethod_var);
		// keysList.Remove( keyObj );
		List_1_t1062181654 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keysList_17();
		Key_t3885074208 * L_15 = V_0;
		// keysList.Remove( keyObj );
		NullCheck(L_14);
		List_1_Remove_m372239913(L_14, L_15, /*hidden argument*/List_1_Remove_m372239913_RuntimeMethod_var);
	}

IL_0059:
	{
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::TrackAxis(System.String,System.Boolean)
extern "C"  void SimpleInput_TrackAxis_m1843737917 (RuntimeObject * __this /* static, unused */, String_t* ___axis0, bool ___trackUnityAxisOnly1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_TrackAxis_m1843737917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AxisInput_t802457650 * V_0 = NULL;
	AxisInput_t802457650 * V_1 = NULL;
	AxisInput_t802457650 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		// AxisInput unityAxis = new AxisInput( axis ) { value = Input.GetAxisRaw( axis ) };
		String_t* L_0 = ___axis0;
		// AxisInput unityAxis = new AxisInput( axis ) { value = Input.GetAxisRaw( axis ) };
		AxisInput_t802457650 * L_1 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_1, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		// AxisInput unityAxis = new AxisInput( axis ) { value = Input.GetAxisRaw( axis ) };
		AxisInput_t802457650 * L_2 = V_1;
		String_t* L_3 = ___axis0;
		// AxisInput unityAxis = new AxisInput( axis ) { value = Input.GetAxisRaw( axis ) };
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		((BaseInput_2_t1381185473 *)L_2)->set_value_1(L_4);
		AxisInput_t802457650 * L_5 = V_1;
		V_0 = L_5;
		// trackedUnityAxes.Add( unityAxis );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2274532392 * L_6 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityAxes_6();
		AxisInput_t802457650 * L_7 = V_0;
		// trackedUnityAxes.Add( unityAxis );
		NullCheck(L_6);
		List_1_Add_m3590501126(L_6, L_7, /*hidden argument*/List_1_Add_m3590501126_RuntimeMethod_var);
		// unityAxis.StartTracking();
		AxisInput_t802457650 * L_8 = V_0;
		// unityAxis.StartTracking();
		NullCheck(L_8);
		BaseInput_2_StartTracking_m3188229854(L_8, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// catch
		goto IL_0063;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002e;
		throw e;
	}

CATCH_002e:
	{ // begin catch(System.Object)
		{
			// if( !trackUnityAxisOnly )
			bool L_9 = ___trackUnityAxisOnly1;
			if (L_9)
			{
				goto IL_005d;
			}
		}

IL_0036:
		{
			// AxisInput temporaryAxis = new AxisInput( axis ) { value = 0f };
			String_t* L_10 = ___axis0;
			// AxisInput temporaryAxis = new AxisInput( axis ) { value = 0f };
			AxisInput_t802457650 * L_11 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
			AxisInput__ctor_m635020897(L_11, L_10, /*hidden argument*/NULL);
			V_1 = L_11;
			// AxisInput temporaryAxis = new AxisInput( axis ) { value = 0f };
			AxisInput_t802457650 * L_12 = V_1;
			NullCheck(L_12);
			((BaseInput_2_t1381185473 *)L_12)->set_value_1((0.0f));
			AxisInput_t802457650 * L_13 = V_1;
			V_2 = L_13;
			// trackedTemporaryAxes.Add( temporaryAxis );
			IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
			List_1_t2274532392 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryAxes_7();
			AxisInput_t802457650 * L_15 = V_2;
			// trackedTemporaryAxes.Add( temporaryAxis );
			NullCheck(L_14);
			List_1_Add_m3590501126(L_14, L_15, /*hidden argument*/List_1_Add_m3590501126_RuntimeMethod_var);
			// temporaryAxis.StartTracking();
			AxisInput_t802457650 * L_16 = V_2;
			// temporaryAxis.StartTracking();
			NullCheck(L_16);
			BaseInput_2_StartTracking_m3188229854(L_16, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		}

IL_005d:
		{
			goto IL_0063;
		}
	} // end catch (depth: 1)

IL_0063:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::TrackButton(System.String,System.Boolean)
extern "C"  void SimpleInput_TrackButton_m2675713569 (RuntimeObject * __this /* static, unused */, String_t* ___button0, bool ___trackUnityButtonOnly1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_TrackButton_m2675713569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ButtonInput_t2818951903 * V_0 = NULL;
	ButtonInput_t2818951903 * V_1 = NULL;
	ButtonInput_t2818951903 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		// ButtonInput unityButton = new ButtonInput( button ) { value = Input.GetButton( button ) };
		String_t* L_0 = ___button0;
		// ButtonInput unityButton = new ButtonInput( button ) { value = Input.GetButton( button ) };
		ButtonInput_t2818951903 * L_1 = (ButtonInput_t2818951903 *)il2cpp_codegen_object_new(ButtonInput_t2818951903_il2cpp_TypeInfo_var);
		ButtonInput__ctor_m1973775960(L_1, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		// ButtonInput unityButton = new ButtonInput( button ) { value = Input.GetButton( button ) };
		ButtonInput_t2818951903 * L_2 = V_1;
		String_t* L_3 = ___button0;
		// ButtonInput unityButton = new ButtonInput( button ) { value = Input.GetButton( button ) };
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetButton_m2064261504(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		((BaseInput_2_t81206664 *)L_2)->set_value_1(L_4);
		ButtonInput_t2818951903 * L_5 = V_1;
		V_0 = L_5;
		// trackedUnityButtons.Add( unityButton );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t4291026645 * L_6 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityButtons_10();
		ButtonInput_t2818951903 * L_7 = V_0;
		// trackedUnityButtons.Add( unityButton );
		NullCheck(L_6);
		List_1_Add_m577318652(L_6, L_7, /*hidden argument*/List_1_Add_m577318652_RuntimeMethod_var);
		// unityButton.StartTracking();
		ButtonInput_t2818951903 * L_8 = V_0;
		// unityButton.StartTracking();
		NullCheck(L_8);
		BaseInput_2_StartTracking_m3481582819(L_8, /*hidden argument*/BaseInput_2_StartTracking_m3481582819_RuntimeMethod_var);
		// catch
		goto IL_005f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002e;
		throw e;
	}

CATCH_002e:
	{ // begin catch(System.Object)
		{
			// if( !trackUnityButtonOnly )
			bool L_9 = ___trackUnityButtonOnly1;
			if (L_9)
			{
				goto IL_0059;
			}
		}

IL_0036:
		{
			// ButtonInput temporaryButton = new ButtonInput( button ) { value = false };
			String_t* L_10 = ___button0;
			// ButtonInput temporaryButton = new ButtonInput( button ) { value = false };
			ButtonInput_t2818951903 * L_11 = (ButtonInput_t2818951903 *)il2cpp_codegen_object_new(ButtonInput_t2818951903_il2cpp_TypeInfo_var);
			ButtonInput__ctor_m1973775960(L_11, L_10, /*hidden argument*/NULL);
			V_1 = L_11;
			// ButtonInput temporaryButton = new ButtonInput( button ) { value = false };
			ButtonInput_t2818951903 * L_12 = V_1;
			NullCheck(L_12);
			((BaseInput_2_t81206664 *)L_12)->set_value_1((bool)0);
			ButtonInput_t2818951903 * L_13 = V_1;
			V_2 = L_13;
			// trackedTemporaryButtons.Add( temporaryButton );
			IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
			List_1_t4291026645 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryButtons_11();
			ButtonInput_t2818951903 * L_15 = V_2;
			// trackedTemporaryButtons.Add( temporaryButton );
			NullCheck(L_14);
			List_1_Add_m577318652(L_14, L_15, /*hidden argument*/List_1_Add_m577318652_RuntimeMethod_var);
			// temporaryButton.StartTracking();
			ButtonInput_t2818951903 * L_16 = V_2;
			// temporaryButton.StartTracking();
			NullCheck(L_16);
			BaseInput_2_StartTracking_m3481582819(L_16, /*hidden argument*/BaseInput_2_StartTracking_m3481582819_RuntimeMethod_var);
		}

IL_0059:
		{
			goto IL_005f;
		}
	} // end catch (depth: 1)

IL_005f:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::TrackMouseButton(System.Int32,System.Boolean)
extern "C"  void SimpleInput_TrackMouseButton_m959883669 (RuntimeObject * __this /* static, unused */, int32_t ___button0, bool ___trackUnityMouseButtonOnly1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_TrackMouseButton_m959883669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseButtonInput_t2868216434 * V_0 = NULL;
	MouseButtonInput_t2868216434 * V_1 = NULL;
	MouseButtonInput_t2868216434 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		// MouseButtonInput unityMouseButton = new MouseButtonInput( button ) { value = Input.GetMouseButton( button ) };
		int32_t L_0 = ___button0;
		// MouseButtonInput unityMouseButton = new MouseButtonInput( button ) { value = Input.GetMouseButton( button ) };
		MouseButtonInput_t2868216434 * L_1 = (MouseButtonInput_t2868216434 *)il2cpp_codegen_object_new(MouseButtonInput_t2868216434_il2cpp_TypeInfo_var);
		MouseButtonInput__ctor_m1842993350(L_1, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		// MouseButtonInput unityMouseButton = new MouseButtonInput( button ) { value = Input.GetMouseButton( button ) };
		MouseButtonInput_t2868216434 * L_2 = V_1;
		int32_t L_3 = ___button0;
		// MouseButtonInput unityMouseButton = new MouseButtonInput( button ) { value = Input.GetMouseButton( button ) };
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetMouseButton_m513753021(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		((BaseInput_2_t3479630992 *)L_2)->set_value_1(L_4);
		MouseButtonInput_t2868216434 * L_5 = V_1;
		V_0 = L_5;
		// trackedUnityMouseButtons.Add( unityMouseButton );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t45323880 * L_6 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityMouseButtons_14();
		MouseButtonInput_t2868216434 * L_7 = V_0;
		// trackedUnityMouseButtons.Add( unityMouseButton );
		NullCheck(L_6);
		List_1_Add_m1188574635(L_6, L_7, /*hidden argument*/List_1_Add_m1188574635_RuntimeMethod_var);
		// unityMouseButton.StartTracking();
		MouseButtonInput_t2868216434 * L_8 = V_0;
		// unityMouseButton.StartTracking();
		NullCheck(L_8);
		BaseInput_2_StartTracking_m985837305(L_8, /*hidden argument*/BaseInput_2_StartTracking_m985837305_RuntimeMethod_var);
		// catch
		goto IL_005f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002e;
		throw e;
	}

CATCH_002e:
	{ // begin catch(System.Object)
		{
			// if( !trackUnityMouseButtonOnly )
			bool L_9 = ___trackUnityMouseButtonOnly1;
			if (L_9)
			{
				goto IL_0059;
			}
		}

IL_0036:
		{
			// MouseButtonInput temporaryMouseButton = new MouseButtonInput( button ) { value = false };
			int32_t L_10 = ___button0;
			// MouseButtonInput temporaryMouseButton = new MouseButtonInput( button ) { value = false };
			MouseButtonInput_t2868216434 * L_11 = (MouseButtonInput_t2868216434 *)il2cpp_codegen_object_new(MouseButtonInput_t2868216434_il2cpp_TypeInfo_var);
			MouseButtonInput__ctor_m1842993350(L_11, L_10, /*hidden argument*/NULL);
			V_1 = L_11;
			// MouseButtonInput temporaryMouseButton = new MouseButtonInput( button ) { value = false };
			MouseButtonInput_t2868216434 * L_12 = V_1;
			NullCheck(L_12);
			((BaseInput_2_t3479630992 *)L_12)->set_value_1((bool)0);
			MouseButtonInput_t2868216434 * L_13 = V_1;
			V_2 = L_13;
			// trackedTemporaryMouseButtons.Add( temporaryMouseButton );
			IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
			List_1_t45323880 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedTemporaryMouseButtons_15();
			MouseButtonInput_t2868216434 * L_15 = V_2;
			// trackedTemporaryMouseButtons.Add( temporaryMouseButton );
			NullCheck(L_14);
			List_1_Add_m1188574635(L_14, L_15, /*hidden argument*/List_1_Add_m1188574635_RuntimeMethod_var);
			// temporaryMouseButton.StartTracking();
			MouseButtonInput_t2868216434 * L_16 = V_2;
			// temporaryMouseButton.StartTracking();
			NullCheck(L_16);
			BaseInput_2_StartTracking_m985837305(L_16, /*hidden argument*/BaseInput_2_StartTracking_m985837305_RuntimeMethod_var);
		}

IL_0059:
		{
			goto IL_005f;
		}
	} // end catch (depth: 1)

IL_005f:
	{
		// }
		return;
	}
}
// System.Void SimpleInput::Update()
extern "C"  void SimpleInput_Update_m3384111285 (SimpleInput_t4265260572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput_Update_m3384111285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	Axis_t2182888826 * V_5 = NULL;
	int32_t V_6 = 0;
	AxisInput_t802457650 * V_7 = NULL;
	int32_t V_8 = 0;
	Button_t2930970180 * V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	ButtonInput_t2818951903 * V_12 = NULL;
	int32_t V_13 = 0;
	MouseButton_t998287744 * V_14 = NULL;
	bool V_15 = false;
	int32_t V_16 = 0;
	MouseButtonInput_t2868216434 * V_17 = NULL;
	int32_t V_18 = 0;
	Key_t3885074208 * V_19 = NULL;
	bool V_20 = false;
	int32_t V_21 = 0;
	KeyInput_t3393349320 * V_22 = NULL;
	{
		// if( OnUpdate != null )
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UpdateCallback_t3991193291 * L_0 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_OnUpdate_18();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// OnUpdate();
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		UpdateCallback_t3991193291 * L_1 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_OnUpdate_18();
		// OnUpdate();
		NullCheck(L_1);
		UpdateCallback_Invoke_m894695456(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		// for( int i = 0; i < trackedUnityAxes.Count; i++ )
		V_0 = 0;
		goto IL_0045;
	}

IL_001c:
	{
		// trackedUnityAxes[i].value = Input.GetAxisRaw( trackedUnityAxes[i].Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2274532392 * L_2 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityAxes_6();
		int32_t L_3 = V_0;
		// trackedUnityAxes[i].value = Input.GetAxisRaw( trackedUnityAxes[i].Key );
		NullCheck(L_2);
		AxisInput_t802457650 * L_4 = List_1_get_Item_m4255429017(L_2, L_3, /*hidden argument*/List_1_get_Item_m4255429017_RuntimeMethod_var);
		List_1_t2274532392 * L_5 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityAxes_6();
		int32_t L_6 = V_0;
		// trackedUnityAxes[i].value = Input.GetAxisRaw( trackedUnityAxes[i].Key );
		NullCheck(L_5);
		AxisInput_t802457650 * L_7 = List_1_get_Item_m4255429017(L_5, L_6, /*hidden argument*/List_1_get_Item_m4255429017_RuntimeMethod_var);
		// trackedUnityAxes[i].value = Input.GetAxisRaw( trackedUnityAxes[i].Key );
		NullCheck(L_7);
		String_t* L_8 = BaseInput_2_get_Key_m784409413(L_7, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// trackedUnityAxes[i].value = Input.GetAxisRaw( trackedUnityAxes[i].Key );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_9 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		((BaseInput_2_t1381185473 *)L_4)->set_value_1(L_9);
		// for( int i = 0; i < trackedUnityAxes.Count; i++ )
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0045:
	{
		// for( int i = 0; i < trackedUnityAxes.Count; i++ )
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2274532392 * L_12 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityAxes_6();
		// for( int i = 0; i < trackedUnityAxes.Count; i++ )
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m1495589272(L_12, /*hidden argument*/List_1_get_Count_m1495589272_RuntimeMethod_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001c;
		}
	}
	{
		// for( int i = 0; i < trackedUnityButtons.Count; i++ )
		V_1 = 0;
		goto IL_0085;
	}

IL_005c:
	{
		// trackedUnityButtons[i].value = Input.GetButton( trackedUnityButtons[i].Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t4291026645 * L_14 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityButtons_10();
		int32_t L_15 = V_1;
		// trackedUnityButtons[i].value = Input.GetButton( trackedUnityButtons[i].Key );
		NullCheck(L_14);
		ButtonInput_t2818951903 * L_16 = List_1_get_Item_m2913088823(L_14, L_15, /*hidden argument*/List_1_get_Item_m2913088823_RuntimeMethod_var);
		List_1_t4291026645 * L_17 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityButtons_10();
		int32_t L_18 = V_1;
		// trackedUnityButtons[i].value = Input.GetButton( trackedUnityButtons[i].Key );
		NullCheck(L_17);
		ButtonInput_t2818951903 * L_19 = List_1_get_Item_m2913088823(L_17, L_18, /*hidden argument*/List_1_get_Item_m2913088823_RuntimeMethod_var);
		// trackedUnityButtons[i].value = Input.GetButton( trackedUnityButtons[i].Key );
		NullCheck(L_19);
		String_t* L_20 = BaseInput_2_get_Key_m1061843947(L_19, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// trackedUnityButtons[i].value = Input.GetButton( trackedUnityButtons[i].Key );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_21 = Input_GetButton_m2064261504(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_16);
		((BaseInput_2_t81206664 *)L_16)->set_value_1(L_21);
		// for( int i = 0; i < trackedUnityButtons.Count; i++ )
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0085:
	{
		// for( int i = 0; i < trackedUnityButtons.Count; i++ )
		int32_t L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t4291026645 * L_24 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityButtons_10();
		// for( int i = 0; i < trackedUnityButtons.Count; i++ )
		NullCheck(L_24);
		int32_t L_25 = List_1_get_Count_m828575477(L_24, /*hidden argument*/List_1_get_Count_m828575477_RuntimeMethod_var);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_005c;
		}
	}
	{
		// for( int i = 0; i < trackedUnityMouseButtons.Count; i++ )
		V_2 = 0;
		goto IL_00c5;
	}

IL_009c:
	{
		// trackedUnityMouseButtons[i].value = Input.GetMouseButton( trackedUnityMouseButtons[i].Key );
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t45323880 * L_26 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityMouseButtons_14();
		int32_t L_27 = V_2;
		// trackedUnityMouseButtons[i].value = Input.GetMouseButton( trackedUnityMouseButtons[i].Key );
		NullCheck(L_26);
		MouseButtonInput_t2868216434 * L_28 = List_1_get_Item_m2247891234(L_26, L_27, /*hidden argument*/List_1_get_Item_m2247891234_RuntimeMethod_var);
		List_1_t45323880 * L_29 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityMouseButtons_14();
		int32_t L_30 = V_2;
		// trackedUnityMouseButtons[i].value = Input.GetMouseButton( trackedUnityMouseButtons[i].Key );
		NullCheck(L_29);
		MouseButtonInput_t2868216434 * L_31 = List_1_get_Item_m2247891234(L_29, L_30, /*hidden argument*/List_1_get_Item_m2247891234_RuntimeMethod_var);
		// trackedUnityMouseButtons[i].value = Input.GetMouseButton( trackedUnityMouseButtons[i].Key );
		NullCheck(L_31);
		int32_t L_32 = BaseInput_2_get_Key_m3054695495(L_31, /*hidden argument*/BaseInput_2_get_Key_m3054695495_RuntimeMethod_var);
		// trackedUnityMouseButtons[i].value = Input.GetMouseButton( trackedUnityMouseButtons[i].Key );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_33 = Input_GetMouseButton_m513753021(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_28);
		((BaseInput_2_t3479630992 *)L_28)->set_value_1(L_33);
		// for( int i = 0; i < trackedUnityMouseButtons.Count; i++ )
		int32_t L_34 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
	}

IL_00c5:
	{
		// for( int i = 0; i < trackedUnityMouseButtons.Count; i++ )
		int32_t L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t45323880 * L_36 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_trackedUnityMouseButtons_14();
		// for( int i = 0; i < trackedUnityMouseButtons.Count; i++ )
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_m3237777153(L_36, /*hidden argument*/List_1_get_Count_m3237777153_RuntimeMethod_var);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_009c;
		}
	}
	{
		// float lerpModifier = AXIS_LERP_MODIFIER * Time.deltaTime;
		// float lerpModifier = AXIS_LERP_MODIFIER * Time.deltaTime;
		float L_38 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = ((float)il2cpp_codegen_multiply((float)(20.0f), (float)L_38));
		// for( int i = 0; i < axesList.Count; i++ )
		V_4 = 0;
		goto IL_01c6;
	}

IL_00e9:
	{
		// Axis axis = axesList[i];
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t3654963568 * L_39 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axesList_5();
		int32_t L_40 = V_4;
		// Axis axis = axesList[i];
		NullCheck(L_39);
		Axis_t2182888826 * L_41 = List_1_get_Item_m2001209520(L_39, L_40, /*hidden argument*/List_1_get_Item_m2001209520_RuntimeMethod_var);
		V_5 = L_41;
		// axis.valueRaw = 0f;
		Axis_t2182888826 * L_42 = V_5;
		NullCheck(L_42);
		L_42->set_valueRaw_3((0.0f));
		// for( int j = axis.inputs.Count - 1; j >= 0; j-- )
		Axis_t2182888826 * L_43 = V_5;
		NullCheck(L_43);
		List_1_t2274532392 * L_44 = L_43->get_inputs_1();
		// for( int j = axis.inputs.Count - 1; j >= 0; j-- )
		NullCheck(L_44);
		int32_t L_45 = List_1_get_Count_m1495589272(L_44, /*hidden argument*/List_1_get_Count_m1495589272_RuntimeMethod_var);
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)1));
		goto IL_0156;
	}

IL_0119:
	{
		// AxisInput input = axis.inputs[j];
		Axis_t2182888826 * L_46 = V_5;
		NullCheck(L_46);
		List_1_t2274532392 * L_47 = L_46->get_inputs_1();
		int32_t L_48 = V_6;
		// AxisInput input = axis.inputs[j];
		NullCheck(L_47);
		AxisInput_t802457650 * L_49 = List_1_get_Item_m4255429017(L_47, L_48, /*hidden argument*/List_1_get_Item_m4255429017_RuntimeMethod_var);
		V_7 = L_49;
		// if( input.value != 0f )
		AxisInput_t802457650 * L_50 = V_7;
		NullCheck(L_50);
		float L_51 = ((BaseInput_2_t1381185473 *)L_50)->get_value_1();
		if ((((float)L_51) == ((float)(0.0f))))
		{
			goto IL_014f;
		}
	}
	{
		// axis.valueRaw = input.value;
		Axis_t2182888826 * L_52 = V_5;
		AxisInput_t802457650 * L_53 = V_7;
		NullCheck(L_53);
		float L_54 = ((BaseInput_2_t1381185473 *)L_53)->get_value_1();
		NullCheck(L_52);
		L_52->set_valueRaw_3(L_54);
		// break;
		goto IL_015e;
	}

IL_014f:
	{
		// for( int j = axis.inputs.Count - 1; j >= 0; j-- )
		int32_t L_55 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)1));
	}

IL_0156:
	{
		// for( int j = axis.inputs.Count - 1; j >= 0; j-- )
		int32_t L_56 = V_6;
		if ((((int32_t)L_56) >= ((int32_t)0)))
		{
			goto IL_0119;
		}
	}

IL_015e:
	{
		// axis.value = Mathf.Lerp( axis.value, axis.valueRaw, lerpModifier );
		Axis_t2182888826 * L_57 = V_5;
		Axis_t2182888826 * L_58 = V_5;
		NullCheck(L_58);
		float L_59 = L_58->get_value_2();
		Axis_t2182888826 * L_60 = V_5;
		NullCheck(L_60);
		float L_61 = L_60->get_valueRaw_3();
		float L_62 = V_3;
		// axis.value = Mathf.Lerp( axis.value, axis.valueRaw, lerpModifier );
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_63 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_59, L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_57);
		L_57->set_value_2(L_63);
		// if( axis.valueRaw == 0f && axis.value != 0f )
		Axis_t2182888826 * L_64 = V_5;
		NullCheck(L_64);
		float L_65 = L_64->get_valueRaw_3();
		if ((!(((float)L_65) == ((float)(0.0f)))))
		{
			goto IL_01bf;
		}
	}
	{
		Axis_t2182888826 * L_66 = V_5;
		NullCheck(L_66);
		float L_67 = L_66->get_value_2();
		if ((((float)L_67) == ((float)(0.0f))))
		{
			goto IL_01bf;
		}
	}
	{
		// if( Mathf.Abs( axis.value ) < 0.025f )
		Axis_t2182888826 * L_68 = V_5;
		NullCheck(L_68);
		float L_69 = L_68->get_value_2();
		// if( Mathf.Abs( axis.value ) < 0.025f )
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_70 = fabsf(L_69);
		if ((!(((float)L_70) < ((float)(0.025f)))))
		{
			goto IL_01be;
		}
	}
	{
		// axis.value = 0f;
		Axis_t2182888826 * L_71 = V_5;
		NullCheck(L_71);
		L_71->set_value_2((0.0f));
	}

IL_01be:
	{
	}

IL_01bf:
	{
		// for( int i = 0; i < axesList.Count; i++ )
		int32_t L_72 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_72, (int32_t)1));
	}

IL_01c6:
	{
		// for( int i = 0; i < axesList.Count; i++ )
		int32_t L_73 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t3654963568 * L_74 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_axesList_5();
		// for( int i = 0; i < axesList.Count; i++ )
		NullCheck(L_74);
		int32_t L_75 = List_1_get_Count_m1399466053(L_74, /*hidden argument*/List_1_get_Count_m1399466053_RuntimeMethod_var);
		if ((((int32_t)L_73) < ((int32_t)L_75)))
		{
			goto IL_00e9;
		}
	}
	{
		// for( int i = 0; i < buttonsList.Count; i++ )
		V_8 = 0;
		goto IL_02af;
	}

IL_01df:
	{
		// Button button = buttonsList[i];
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t108077626 * L_76 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttonsList_9();
		int32_t L_77 = V_8;
		// Button button = buttonsList[i];
		NullCheck(L_76);
		Button_t2930970180 * L_78 = List_1_get_Item_m182785779(L_76, L_77, /*hidden argument*/List_1_get_Item_m182785779_RuntimeMethod_var);
		V_9 = L_78;
		// bool isDown = false;
		V_10 = (bool)0;
		// for( int j = button.inputs.Count - 1; j >= 0; j-- )
		Button_t2930970180 * L_79 = V_9;
		NullCheck(L_79);
		List_1_t4291026645 * L_80 = L_79->get_inputs_1();
		// for( int j = button.inputs.Count - 1; j >= 0; j-- )
		NullCheck(L_80);
		int32_t L_81 = List_1_get_Count_m828575477(L_80, /*hidden argument*/List_1_get_Count_m828575477_RuntimeMethod_var);
		V_11 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_81, (int32_t)1));
		goto IL_0233;
	}

IL_0206:
	{
		// ButtonInput input = button.inputs[j];
		Button_t2930970180 * L_82 = V_9;
		NullCheck(L_82);
		List_1_t4291026645 * L_83 = L_82->get_inputs_1();
		int32_t L_84 = V_11;
		// ButtonInput input = button.inputs[j];
		NullCheck(L_83);
		ButtonInput_t2818951903 * L_85 = List_1_get_Item_m2913088823(L_83, L_84, /*hidden argument*/List_1_get_Item_m2913088823_RuntimeMethod_var);
		V_12 = L_85;
		// if( input.value )
		ButtonInput_t2818951903 * L_86 = V_12;
		NullCheck(L_86);
		bool L_87 = ((BaseInput_2_t81206664 *)L_86)->get_value_1();
		if (!L_87)
		{
			goto IL_022c;
		}
	}
	{
		// isDown = true;
		V_10 = (bool)1;
		// break;
		goto IL_023b;
	}

IL_022c:
	{
		// for( int j = button.inputs.Count - 1; j >= 0; j-- )
		int32_t L_88 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_88, (int32_t)1));
	}

IL_0233:
	{
		// for( int j = button.inputs.Count - 1; j >= 0; j-- )
		int32_t L_89 = V_11;
		if ((((int32_t)L_89) >= ((int32_t)0)))
		{
			goto IL_0206;
		}
	}

IL_023b:
	{
		// if( isDown )
		bool L_90 = V_10;
		if (!L_90)
		{
			goto IL_0277;
		}
	}
	{
		// if( button.state == InputState.None || button.state == InputState.Released )
		Button_t2930970180 * L_91 = V_9;
		NullCheck(L_91);
		int32_t L_92 = L_91->get_state_2();
		if (!L_92)
		{
			goto IL_025c;
		}
	}
	{
		Button_t2930970180 * L_93 = V_9;
		NullCheck(L_93);
		int32_t L_94 = L_93->get_state_2();
		if ((!(((uint32_t)L_94) == ((uint32_t)3))))
		{
			goto IL_0269;
		}
	}

IL_025c:
	{
		// button.state = InputState.Pressed;
		Button_t2930970180 * L_95 = V_9;
		NullCheck(L_95);
		L_95->set_state_2(1);
		goto IL_0271;
	}

IL_0269:
	{
		// button.state = InputState.Held;
		Button_t2930970180 * L_96 = V_9;
		NullCheck(L_96);
		L_96->set_state_2(2);
	}

IL_0271:
	{
		goto IL_02a8;
	}

IL_0277:
	{
		// if( button.state == InputState.Pressed || button.state == InputState.Held )
		Button_t2930970180 * L_97 = V_9;
		NullCheck(L_97);
		int32_t L_98 = L_97->get_state_2();
		if ((((int32_t)L_98) == ((int32_t)1)))
		{
			goto IL_0292;
		}
	}
	{
		Button_t2930970180 * L_99 = V_9;
		NullCheck(L_99);
		int32_t L_100 = L_99->get_state_2();
		if ((!(((uint32_t)L_100) == ((uint32_t)2))))
		{
			goto IL_029f;
		}
	}

IL_0292:
	{
		// button.state = InputState.Released;
		Button_t2930970180 * L_101 = V_9;
		NullCheck(L_101);
		L_101->set_state_2(3);
		goto IL_02a7;
	}

IL_029f:
	{
		// button.state = InputState.None;
		Button_t2930970180 * L_102 = V_9;
		NullCheck(L_102);
		L_102->set_state_2(0);
	}

IL_02a7:
	{
	}

IL_02a8:
	{
		// for( int i = 0; i < buttonsList.Count; i++ )
		int32_t L_103 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_103, (int32_t)1));
	}

IL_02af:
	{
		// for( int i = 0; i < buttonsList.Count; i++ )
		int32_t L_104 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t108077626 * L_105 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_buttonsList_9();
		// for( int i = 0; i < buttonsList.Count; i++ )
		NullCheck(L_105);
		int32_t L_106 = List_1_get_Count_m377990033(L_105, /*hidden argument*/List_1_get_Count_m377990033_RuntimeMethod_var);
		if ((((int32_t)L_104) < ((int32_t)L_106)))
		{
			goto IL_01df;
		}
	}
	{
		// for( int i = 0; i < mouseButtonsList.Count; i++ )
		V_13 = 0;
		goto IL_0398;
	}

IL_02c8:
	{
		// MouseButton mouseButton = mouseButtonsList[i];
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2470362486 * L_107 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtonsList_13();
		int32_t L_108 = V_13;
		// MouseButton mouseButton = mouseButtonsList[i];
		NullCheck(L_107);
		MouseButton_t998287744 * L_109 = List_1_get_Item_m3336004944(L_107, L_108, /*hidden argument*/List_1_get_Item_m3336004944_RuntimeMethod_var);
		V_14 = L_109;
		// bool isDown = false;
		V_15 = (bool)0;
		// for( int j = mouseButton.inputs.Count - 1; j >= 0; j-- )
		MouseButton_t998287744 * L_110 = V_14;
		NullCheck(L_110);
		List_1_t45323880 * L_111 = L_110->get_inputs_1();
		// for( int j = mouseButton.inputs.Count - 1; j >= 0; j-- )
		NullCheck(L_111);
		int32_t L_112 = List_1_get_Count_m3237777153(L_111, /*hidden argument*/List_1_get_Count_m3237777153_RuntimeMethod_var);
		V_16 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_112, (int32_t)1));
		goto IL_031c;
	}

IL_02ef:
	{
		// MouseButtonInput input = mouseButton.inputs[j];
		MouseButton_t998287744 * L_113 = V_14;
		NullCheck(L_113);
		List_1_t45323880 * L_114 = L_113->get_inputs_1();
		int32_t L_115 = V_16;
		// MouseButtonInput input = mouseButton.inputs[j];
		NullCheck(L_114);
		MouseButtonInput_t2868216434 * L_116 = List_1_get_Item_m2247891234(L_114, L_115, /*hidden argument*/List_1_get_Item_m2247891234_RuntimeMethod_var);
		V_17 = L_116;
		// if( input.value )
		MouseButtonInput_t2868216434 * L_117 = V_17;
		NullCheck(L_117);
		bool L_118 = ((BaseInput_2_t3479630992 *)L_117)->get_value_1();
		if (!L_118)
		{
			goto IL_0315;
		}
	}
	{
		// isDown = true;
		V_15 = (bool)1;
		// break;
		goto IL_0324;
	}

IL_0315:
	{
		// for( int j = mouseButton.inputs.Count - 1; j >= 0; j-- )
		int32_t L_119 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_119, (int32_t)1));
	}

IL_031c:
	{
		// for( int j = mouseButton.inputs.Count - 1; j >= 0; j-- )
		int32_t L_120 = V_16;
		if ((((int32_t)L_120) >= ((int32_t)0)))
		{
			goto IL_02ef;
		}
	}

IL_0324:
	{
		// if( isDown )
		bool L_121 = V_15;
		if (!L_121)
		{
			goto IL_0360;
		}
	}
	{
		// if( mouseButton.state == InputState.None || mouseButton.state == InputState.Released )
		MouseButton_t998287744 * L_122 = V_14;
		NullCheck(L_122);
		int32_t L_123 = L_122->get_state_2();
		if (!L_123)
		{
			goto IL_0345;
		}
	}
	{
		MouseButton_t998287744 * L_124 = V_14;
		NullCheck(L_124);
		int32_t L_125 = L_124->get_state_2();
		if ((!(((uint32_t)L_125) == ((uint32_t)3))))
		{
			goto IL_0352;
		}
	}

IL_0345:
	{
		// mouseButton.state = InputState.Pressed;
		MouseButton_t998287744 * L_126 = V_14;
		NullCheck(L_126);
		L_126->set_state_2(1);
		goto IL_035a;
	}

IL_0352:
	{
		// mouseButton.state = InputState.Held;
		MouseButton_t998287744 * L_127 = V_14;
		NullCheck(L_127);
		L_127->set_state_2(2);
	}

IL_035a:
	{
		goto IL_0391;
	}

IL_0360:
	{
		// if( mouseButton.state == InputState.Pressed || mouseButton.state == InputState.Held )
		MouseButton_t998287744 * L_128 = V_14;
		NullCheck(L_128);
		int32_t L_129 = L_128->get_state_2();
		if ((((int32_t)L_129) == ((int32_t)1)))
		{
			goto IL_037b;
		}
	}
	{
		MouseButton_t998287744 * L_130 = V_14;
		NullCheck(L_130);
		int32_t L_131 = L_130->get_state_2();
		if ((!(((uint32_t)L_131) == ((uint32_t)2))))
		{
			goto IL_0388;
		}
	}

IL_037b:
	{
		// mouseButton.state = InputState.Released;
		MouseButton_t998287744 * L_132 = V_14;
		NullCheck(L_132);
		L_132->set_state_2(3);
		goto IL_0390;
	}

IL_0388:
	{
		// mouseButton.state = InputState.None;
		MouseButton_t998287744 * L_133 = V_14;
		NullCheck(L_133);
		L_133->set_state_2(0);
	}

IL_0390:
	{
	}

IL_0391:
	{
		// for( int i = 0; i < mouseButtonsList.Count; i++ )
		int32_t L_134 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_134, (int32_t)1));
	}

IL_0398:
	{
		// for( int i = 0; i < mouseButtonsList.Count; i++ )
		int32_t L_135 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t2470362486 * L_136 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_mouseButtonsList_13();
		// for( int i = 0; i < mouseButtonsList.Count; i++ )
		NullCheck(L_136);
		int32_t L_137 = List_1_get_Count_m326005465(L_136, /*hidden argument*/List_1_get_Count_m326005465_RuntimeMethod_var);
		if ((((int32_t)L_135) < ((int32_t)L_137)))
		{
			goto IL_02c8;
		}
	}
	{
		// for( int i = 0; i < keysList.Count; i++ )
		V_18 = 0;
		goto IL_0481;
	}

IL_03b1:
	{
		// Key key = keysList[i];
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t1062181654 * L_138 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keysList_17();
		int32_t L_139 = V_18;
		// Key key = keysList[i];
		NullCheck(L_138);
		Key_t3885074208 * L_140 = List_1_get_Item_m1284462838(L_138, L_139, /*hidden argument*/List_1_get_Item_m1284462838_RuntimeMethod_var);
		V_19 = L_140;
		// bool isDown = false;
		V_20 = (bool)0;
		// for( int j = key.inputs.Count - 1; j >= 0; j-- )
		Key_t3885074208 * L_141 = V_19;
		NullCheck(L_141);
		List_1_t570456766 * L_142 = L_141->get_inputs_1();
		// for( int j = key.inputs.Count - 1; j >= 0; j-- )
		NullCheck(L_142);
		int32_t L_143 = List_1_get_Count_m3615089172(L_142, /*hidden argument*/List_1_get_Count_m3615089172_RuntimeMethod_var);
		V_21 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_143, (int32_t)1));
		goto IL_0405;
	}

IL_03d8:
	{
		// KeyInput input = key.inputs[j];
		Key_t3885074208 * L_144 = V_19;
		NullCheck(L_144);
		List_1_t570456766 * L_145 = L_144->get_inputs_1();
		int32_t L_146 = V_21;
		// KeyInput input = key.inputs[j];
		NullCheck(L_145);
		KeyInput_t3393349320 * L_147 = List_1_get_Item_m1602638289(L_145, L_146, /*hidden argument*/List_1_get_Item_m1602638289_RuntimeMethod_var);
		V_22 = L_147;
		// if( input.value )
		KeyInput_t3393349320 * L_148 = V_22;
		NullCheck(L_148);
		bool L_149 = ((BaseInput_2_t1022651380 *)L_148)->get_value_1();
		if (!L_149)
		{
			goto IL_03fe;
		}
	}
	{
		// isDown = true;
		V_20 = (bool)1;
		// break;
		goto IL_040d;
	}

IL_03fe:
	{
		// for( int j = key.inputs.Count - 1; j >= 0; j-- )
		int32_t L_150 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_150, (int32_t)1));
	}

IL_0405:
	{
		// for( int j = key.inputs.Count - 1; j >= 0; j-- )
		int32_t L_151 = V_21;
		if ((((int32_t)L_151) >= ((int32_t)0)))
		{
			goto IL_03d8;
		}
	}

IL_040d:
	{
		// if( isDown )
		bool L_152 = V_20;
		if (!L_152)
		{
			goto IL_0449;
		}
	}
	{
		// if( key.state == InputState.None || key.state == InputState.Released )
		Key_t3885074208 * L_153 = V_19;
		NullCheck(L_153);
		int32_t L_154 = L_153->get_state_2();
		if (!L_154)
		{
			goto IL_042e;
		}
	}
	{
		Key_t3885074208 * L_155 = V_19;
		NullCheck(L_155);
		int32_t L_156 = L_155->get_state_2();
		if ((!(((uint32_t)L_156) == ((uint32_t)3))))
		{
			goto IL_043b;
		}
	}

IL_042e:
	{
		// key.state = InputState.Pressed;
		Key_t3885074208 * L_157 = V_19;
		NullCheck(L_157);
		L_157->set_state_2(1);
		goto IL_0443;
	}

IL_043b:
	{
		// key.state = InputState.Held;
		Key_t3885074208 * L_158 = V_19;
		NullCheck(L_158);
		L_158->set_state_2(2);
	}

IL_0443:
	{
		goto IL_047a;
	}

IL_0449:
	{
		// if( key.state == InputState.Pressed || key.state == InputState.Held )
		Key_t3885074208 * L_159 = V_19;
		NullCheck(L_159);
		int32_t L_160 = L_159->get_state_2();
		if ((((int32_t)L_160) == ((int32_t)1)))
		{
			goto IL_0464;
		}
	}
	{
		Key_t3885074208 * L_161 = V_19;
		NullCheck(L_161);
		int32_t L_162 = L_161->get_state_2();
		if ((!(((uint32_t)L_162) == ((uint32_t)2))))
		{
			goto IL_0471;
		}
	}

IL_0464:
	{
		// key.state = InputState.Released;
		Key_t3885074208 * L_163 = V_19;
		NullCheck(L_163);
		L_163->set_state_2(3);
		goto IL_0479;
	}

IL_0471:
	{
		// key.state = InputState.None;
		Key_t3885074208 * L_164 = V_19;
		NullCheck(L_164);
		L_164->set_state_2(0);
	}

IL_0479:
	{
	}

IL_047a:
	{
		// for( int i = 0; i < keysList.Count; i++ )
		int32_t L_165 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add((int32_t)L_165, (int32_t)1));
	}

IL_0481:
	{
		// for( int i = 0; i < keysList.Count; i++ )
		int32_t L_166 = V_18;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		List_1_t1062181654 * L_167 = ((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->get_keysList_17();
		// for( int i = 0; i < keysList.Count; i++ )
		NullCheck(L_167);
		int32_t L_168 = List_1_get_Count_m108667433(L_167, /*hidden argument*/List_1_get_Count_m108667433_RuntimeMethod_var);
		if ((((int32_t)L_166) < ((int32_t)L_168)))
		{
			goto IL_03b1;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SimpleInput::.cctor()
extern "C"  void SimpleInput__cctor_m942092536 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInput__cctor_m942092536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static Dictionary<string, Axis> axes = new Dictionary<string, Axis>();
		Dictionary_2_t1968145125 * L_0 = (Dictionary_2_t1968145125 *)il2cpp_codegen_object_new(Dictionary_2_t1968145125_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1219153817(L_0, /*hidden argument*/Dictionary_2__ctor_m1219153817_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_axes_4(L_0);
		// private static List<Axis> axesList = new List<Axis>();
		List_1_t3654963568 * L_1 = (List_1_t3654963568 *)il2cpp_codegen_object_new(List_1_t3654963568_il2cpp_TypeInfo_var);
		List_1__ctor_m3492050649(L_1, /*hidden argument*/List_1__ctor_m3492050649_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_axesList_5(L_1);
		// private static List<AxisInput> trackedUnityAxes = new List<AxisInput>();
		List_1_t2274532392 * L_2 = (List_1_t2274532392 *)il2cpp_codegen_object_new(List_1_t2274532392_il2cpp_TypeInfo_var);
		List_1__ctor_m2629046078(L_2, /*hidden argument*/List_1__ctor_m2629046078_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_trackedUnityAxes_6(L_2);
		// private static List<AxisInput> trackedTemporaryAxes = new List<AxisInput>();
		List_1_t2274532392 * L_3 = (List_1_t2274532392 *)il2cpp_codegen_object_new(List_1_t2274532392_il2cpp_TypeInfo_var);
		List_1__ctor_m2629046078(L_3, /*hidden argument*/List_1__ctor_m2629046078_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_trackedTemporaryAxes_7(L_3);
		// private static Dictionary<string, Button> buttons = new Dictionary<string, Button>();
		Dictionary_2_t2716226479 * L_4 = (Dictionary_2_t2716226479 *)il2cpp_codegen_object_new(Dictionary_2_t2716226479_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1157345819(L_4, /*hidden argument*/Dictionary_2__ctor_m1157345819_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_buttons_8(L_4);
		// private static List<Button> buttonsList = new List<Button>();
		List_1_t108077626 * L_5 = (List_1_t108077626 *)il2cpp_codegen_object_new(List_1_t108077626_il2cpp_TypeInfo_var);
		List_1__ctor_m4217368305(L_5, /*hidden argument*/List_1__ctor_m4217368305_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_buttonsList_9(L_5);
		// private static List<ButtonInput> trackedUnityButtons = new List<ButtonInput>();
		List_1_t4291026645 * L_6 = (List_1_t4291026645 *)il2cpp_codegen_object_new(List_1_t4291026645_il2cpp_TypeInfo_var);
		List_1__ctor_m770714582(L_6, /*hidden argument*/List_1__ctor_m770714582_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_trackedUnityButtons_10(L_6);
		// private static List<ButtonInput> trackedTemporaryButtons = new List<ButtonInput>();
		List_1_t4291026645 * L_7 = (List_1_t4291026645 *)il2cpp_codegen_object_new(List_1_t4291026645_il2cpp_TypeInfo_var);
		List_1__ctor_m770714582(L_7, /*hidden argument*/List_1__ctor_m770714582_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_trackedTemporaryButtons_11(L_7);
		// private static Dictionary<int, MouseButton> mouseButtons = new Dictionary<int, MouseButton>();
		Dictionary_2_t4181968371 * L_8 = (Dictionary_2_t4181968371 *)il2cpp_codegen_object_new(Dictionary_2_t4181968371_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1685523410(L_8, /*hidden argument*/Dictionary_2__ctor_m1685523410_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_mouseButtons_12(L_8);
		// private static List<MouseButton> mouseButtonsList = new List<MouseButton>();
		List_1_t2470362486 * L_9 = (List_1_t2470362486 *)il2cpp_codegen_object_new(List_1_t2470362486_il2cpp_TypeInfo_var);
		List_1__ctor_m2002102647(L_9, /*hidden argument*/List_1__ctor_m2002102647_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_mouseButtonsList_13(L_9);
		// private static List<MouseButtonInput> trackedUnityMouseButtons = new List<MouseButtonInput>();
		List_1_t45323880 * L_10 = (List_1_t45323880 *)il2cpp_codegen_object_new(List_1_t45323880_il2cpp_TypeInfo_var);
		List_1__ctor_m256120419(L_10, /*hidden argument*/List_1__ctor_m256120419_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_trackedUnityMouseButtons_14(L_10);
		// private static List<MouseButtonInput> trackedTemporaryMouseButtons = new List<MouseButtonInput>();
		List_1_t45323880 * L_11 = (List_1_t45323880 *)il2cpp_codegen_object_new(List_1_t45323880_il2cpp_TypeInfo_var);
		List_1__ctor_m256120419(L_11, /*hidden argument*/List_1__ctor_m256120419_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_trackedTemporaryMouseButtons_15(L_11);
		// private static Dictionary<KeyCode, Key> keys = new Dictionary<KeyCode, Key>();
		Dictionary_2_t316807927 * L_12 = (Dictionary_2_t316807927 *)il2cpp_codegen_object_new(Dictionary_2_t316807927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2001987697(L_12, /*hidden argument*/Dictionary_2__ctor_m2001987697_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_keys_16(L_12);
		// private static List<Key> keysList = new List<Key>();
		List_1_t1062181654 * L_13 = (List_1_t1062181654 *)il2cpp_codegen_object_new(List_1_t1062181654_il2cpp_TypeInfo_var);
		List_1__ctor_m2112490249(L_13, /*hidden argument*/List_1__ctor_m2112490249_RuntimeMethod_var);
		((SimpleInput_t4265260572_StaticFields*)il2cpp_codegen_static_fields_for(SimpleInput_t4265260572_il2cpp_TypeInfo_var))->set_keysList_17(L_13);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/Axis::.ctor(System.String)
extern "C"  void Axis__ctor_m2063451953 (Axis_t2182888826 * __this, String_t* ___axis0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Axis__ctor_m2063451953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float value = 0f;
		__this->set_value_2((0.0f));
		// public float valueRaw = 0f;
		__this->set_valueRaw_3((0.0f));
		// public Axis( string axis )
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// name = axis;
		String_t* L_0 = ___axis0;
		__this->set_name_0(L_0);
		// inputs = new List<AxisInput>();
		// inputs = new List<AxisInput>();
		List_1_t2274532392 * L_1 = (List_1_t2274532392 *)il2cpp_codegen_object_new(List_1_t2274532392_il2cpp_TypeInfo_var);
		List_1__ctor_m2629046078(L_1, /*hidden argument*/List_1__ctor_m2629046078_RuntimeMethod_var);
		__this->set_inputs_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/AxisInput::.ctor()
extern "C"  void AxisInput__ctor_m143791316 (AxisInput_t802457650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInput__ctor_m143791316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AxisInput() : base() { }
		BaseInput_2__ctor_m1791511703(__this, /*hidden argument*/BaseInput_2__ctor_m1791511703_RuntimeMethod_var);
		// public AxisInput() : base() { }
		return;
	}
}
// System.Void SimpleInput/AxisInput::.ctor(System.String)
extern "C"  void AxisInput__ctor_m635020897 (AxisInput_t802457650 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInput__ctor_m635020897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AxisInput( string key ) : base( key ) { }
		String_t* L_0 = ___key0;
		BaseInput_2__ctor_m449874524(__this, L_0, /*hidden argument*/BaseInput_2__ctor_m449874524_RuntimeMethod_var);
		// public AxisInput( string key ) : base( key ) { }
		return;
	}
}
// System.Boolean SimpleInput/AxisInput::IsKeyValid()
extern "C"  bool AxisInput_IsKeyValid_m4176685907 (AxisInput_t802457650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInput_IsKeyValid_m4176685907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		String_t* L_0 = BaseInput_2_get_Key_m784409413(__this, /*hidden argument*/BaseInput_2_get_Key_m784409413_RuntimeMethod_var);
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean SimpleInput/AxisInput::KeysEqual(System.String,System.String)
extern "C"  bool AxisInput_KeysEqual_m3941979874 (AxisInput_t802457650 * __this, String_t* ___key10, String_t* ___key21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInput_KeysEqual_m3941979874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// protected override bool KeysEqual( string key1, string key2 ) { return key1 == key2; }
		String_t* L_0 = ___key10;
		String_t* L_1 = ___key21;
		// protected override bool KeysEqual( string key1, string key2 ) { return key1 == key2; }
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		// protected override bool KeysEqual( string key1, string key2 ) { return key1 == key2; }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void SimpleInput/AxisInput::RegisterInput()
extern "C"  void AxisInput_RegisterInput_m3849587313 (AxisInput_t802457650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInput_RegisterInput_m3849587313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void RegisterInput() { RegisterAxis( this ); }
		// protected override void RegisterInput() { RegisterAxis( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_RegisterAxis_m496276425(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void RegisterInput() { RegisterAxis( this ); }
		return;
	}
}
// System.Void SimpleInput/AxisInput::UnregisterInput()
extern "C"  void AxisInput_UnregisterInput_m2227498125 (AxisInput_t802457650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInput_UnregisterInput_m2227498125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void UnregisterInput() { UnregisterAxis( this ); }
		// protected override void UnregisterInput() { UnregisterAxis( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_UnregisterAxis_m2397270711(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void UnregisterInput() { UnregisterAxis( this ); }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/Button::.ctor(System.String)
extern "C"  void Button__ctor_m1639548820 (Button_t2930970180 * __this, String_t* ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Button__ctor_m1639548820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public InputState state = InputState.None;
		__this->set_state_2(0);
		// public Button( string button )
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// this.button = button;
		String_t* L_0 = ___button0;
		__this->set_button_0(L_0);
		// inputs = new List<ButtonInput>();
		// inputs = new List<ButtonInput>();
		List_1_t4291026645 * L_1 = (List_1_t4291026645 *)il2cpp_codegen_object_new(List_1_t4291026645_il2cpp_TypeInfo_var);
		List_1__ctor_m770714582(L_1, /*hidden argument*/List_1__ctor_m770714582_RuntimeMethod_var);
		__this->set_inputs_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/ButtonInput::.ctor()
extern "C"  void ButtonInput__ctor_m2907722220 (ButtonInput_t2818951903 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInput__ctor_m2907722220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public ButtonInput() : base() { }
		BaseInput_2__ctor_m3207761156(__this, /*hidden argument*/BaseInput_2__ctor_m3207761156_RuntimeMethod_var);
		// public ButtonInput() : base() { }
		return;
	}
}
// System.Void SimpleInput/ButtonInput::.ctor(System.String)
extern "C"  void ButtonInput__ctor_m1973775960 (ButtonInput_t2818951903 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInput__ctor_m1973775960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public ButtonInput( string key ) : base( key ) { }
		String_t* L_0 = ___key0;
		BaseInput_2__ctor_m3206253858(__this, L_0, /*hidden argument*/BaseInput_2__ctor_m3206253858_RuntimeMethod_var);
		// public ButtonInput( string key ) : base( key ) { }
		return;
	}
}
// System.Boolean SimpleInput/ButtonInput::IsKeyValid()
extern "C"  bool ButtonInput_IsKeyValid_m1643963182 (ButtonInput_t2818951903 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInput_IsKeyValid_m1643963182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		String_t* L_0 = BaseInput_2_get_Key_m1061843947(__this, /*hidden argument*/BaseInput_2_get_Key_m1061843947_RuntimeMethod_var);
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		// public override bool IsKeyValid() { return !string.IsNullOrEmpty( Key ); }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean SimpleInput/ButtonInput::KeysEqual(System.String,System.String)
extern "C"  bool ButtonInput_KeysEqual_m549903569 (ButtonInput_t2818951903 * __this, String_t* ___key10, String_t* ___key21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInput_KeysEqual_m549903569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// protected override bool KeysEqual( string key1, string key2 ) { return key1 == key2; }
		String_t* L_0 = ___key10;
		String_t* L_1 = ___key21;
		// protected override bool KeysEqual( string key1, string key2 ) { return key1 == key2; }
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		// protected override bool KeysEqual( string key1, string key2 ) { return key1 == key2; }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void SimpleInput/ButtonInput::RegisterInput()
extern "C"  void ButtonInput_RegisterInput_m2741498365 (ButtonInput_t2818951903 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInput_RegisterInput_m2741498365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void RegisterInput() { RegisterButton( this ); }
		// protected override void RegisterInput() { RegisterButton( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_RegisterButton_m3324302350(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void RegisterInput() { RegisterButton( this ); }
		return;
	}
}
// System.Void SimpleInput/ButtonInput::UnregisterInput()
extern "C"  void ButtonInput_UnregisterInput_m1873494997 (ButtonInput_t2818951903 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInput_UnregisterInput_m1873494997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void UnregisterInput() { UnregisterButton( this ); }
		// protected override void UnregisterInput() { UnregisterButton( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_UnregisterButton_m1428040134(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void UnregisterInput() { UnregisterButton( this ); }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/Key::.ctor(UnityEngine.KeyCode)
extern "C"  void Key__ctor_m707106284 (Key_t3885074208 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Key__ctor_m707106284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public InputState state = InputState.None;
		__this->set_state_2(0);
		// public Key( KeyCode key )
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// this.key = key;
		int32_t L_0 = ___key0;
		__this->set_key_0(L_0);
		// inputs = new List<KeyInput>();
		// inputs = new List<KeyInput>();
		List_1_t570456766 * L_1 = (List_1_t570456766 *)il2cpp_codegen_object_new(List_1_t570456766_il2cpp_TypeInfo_var);
		List_1__ctor_m2757187041(L_1, /*hidden argument*/List_1__ctor_m2757187041_RuntimeMethod_var);
		__this->set_inputs_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/KeyInput::.ctor()
extern "C"  void KeyInput__ctor_m3791521000 (KeyInput_t3393349320 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInput__ctor_m3791521000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public KeyInput() : base() { }
		BaseInput_2__ctor_m4093196004(__this, /*hidden argument*/BaseInput_2__ctor_m4093196004_RuntimeMethod_var);
		// public KeyInput() : base() { }
		return;
	}
}
// System.Void SimpleInput/KeyInput::.ctor(UnityEngine.KeyCode)
extern "C"  void KeyInput__ctor_m134535775 (KeyInput_t3393349320 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInput__ctor_m134535775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public KeyInput( KeyCode key ) : base( key ) { }
		int32_t L_0 = ___key0;
		BaseInput_2__ctor_m3612960250(__this, L_0, /*hidden argument*/BaseInput_2__ctor_m3612960250_RuntimeMethod_var);
		// public KeyInput( KeyCode key ) : base( key ) { }
		return;
	}
}
// System.Boolean SimpleInput/KeyInput::KeysEqual(UnityEngine.KeyCode,UnityEngine.KeyCode)
extern "C"  bool KeyInput_KeysEqual_m2239269678 (KeyInput_t3393349320 * __this, int32_t ___key10, int32_t ___key21, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// protected override bool KeysEqual( KeyCode key1, KeyCode key2 ) { return key1 == key2; }
		int32_t L_0 = ___key10;
		int32_t L_1 = ___key21;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_000b;
	}

IL_000b:
	{
		// protected override bool KeysEqual( KeyCode key1, KeyCode key2 ) { return key1 == key2; }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void SimpleInput/KeyInput::RegisterInput()
extern "C"  void KeyInput_RegisterInput_m3140085273 (KeyInput_t3393349320 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInput_RegisterInput_m3140085273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void RegisterInput() { RegisterKey( this ); }
		// protected override void RegisterInput() { RegisterKey( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_RegisterKey_m2116541909(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void RegisterInput() { RegisterKey( this ); }
		return;
	}
}
// System.Void SimpleInput/KeyInput::UnregisterInput()
extern "C"  void KeyInput_UnregisterInput_m2413266091 (KeyInput_t3393349320 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInput_UnregisterInput_m2413266091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void UnregisterInput() { UnregisterKey( this ); }
		// protected override void UnregisterInput() { UnregisterKey( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_UnregisterKey_m3562015723(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void UnregisterInput() { UnregisterKey( this ); }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/MouseButton::.ctor(System.Int32)
extern "C"  void MouseButton__ctor_m2922259096 (MouseButton_t998287744 * __this, int32_t ___button0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButton__ctor_m2922259096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public InputState state = InputState.None;
		__this->set_state_2(0);
		// public MouseButton( int button )
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// this.button = button;
		int32_t L_0 = ___button0;
		__this->set_button_0(L_0);
		// inputs = new List<MouseButtonInput>();
		// inputs = new List<MouseButtonInput>();
		List_1_t45323880 * L_1 = (List_1_t45323880 *)il2cpp_codegen_object_new(List_1_t45323880_il2cpp_TypeInfo_var);
		List_1__ctor_m256120419(L_1, /*hidden argument*/List_1__ctor_m256120419_RuntimeMethod_var);
		__this->set_inputs_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInput/MouseButtonInput::.ctor()
extern "C"  void MouseButtonInput__ctor_m247368948 (MouseButtonInput_t2868216434 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInput__ctor_m247368948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public MouseButtonInput() : base() { }
		BaseInput_2__ctor_m1777816748(__this, /*hidden argument*/BaseInput_2__ctor_m1777816748_RuntimeMethod_var);
		// public MouseButtonInput() : base() { }
		return;
	}
}
// System.Void SimpleInput/MouseButtonInput::.ctor(System.Int32)
extern "C"  void MouseButtonInput__ctor_m1842993350 (MouseButtonInput_t2868216434 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInput__ctor_m1842993350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public MouseButtonInput( int key ) : base( key ) { }
		int32_t L_0 = ___key0;
		BaseInput_2__ctor_m3806450130(__this, L_0, /*hidden argument*/BaseInput_2__ctor_m3806450130_RuntimeMethod_var);
		// public MouseButtonInput( int key ) : base( key ) { }
		return;
	}
}
// System.Boolean SimpleInput/MouseButtonInput::KeysEqual(System.Int32,System.Int32)
extern "C"  bool MouseButtonInput_KeysEqual_m2733265914 (MouseButtonInput_t2868216434 * __this, int32_t ___key10, int32_t ___key21, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// protected override bool KeysEqual( int key1, int key2 ) { return key1 == key2; }
		int32_t L_0 = ___key10;
		int32_t L_1 = ___key21;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_000b;
	}

IL_000b:
	{
		// protected override bool KeysEqual( int key1, int key2 ) { return key1 == key2; }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void SimpleInput/MouseButtonInput::RegisterInput()
extern "C"  void MouseButtonInput_RegisterInput_m1050424758 (MouseButtonInput_t2868216434 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInput_RegisterInput_m1050424758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void RegisterInput() { RegisterMouseButton( this ); }
		// protected override void RegisterInput() { RegisterMouseButton( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_RegisterMouseButton_m4261908703(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void RegisterInput() { RegisterMouseButton( this ); }
		return;
	}
}
// System.Void SimpleInput/MouseButtonInput::UnregisterInput()
extern "C"  void MouseButtonInput_UnregisterInput_m584781875 (MouseButtonInput_t2868216434 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInput_UnregisterInput_m584781875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected override void UnregisterInput() { UnregisterMouseButton( this ); }
		// protected override void UnregisterInput() { UnregisterMouseButton( this ); }
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_UnregisterMouseButton_m1845058510(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// protected override void UnregisterInput() { UnregisterMouseButton( this ); }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_UpdateCallback_t3991193291 (UpdateCallback_t3991193291 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void SimpleInput/UpdateCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCallback__ctor_m3190082255 (UpdateCallback_t3991193291 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleInput/UpdateCallback::Invoke()
extern "C"  void UpdateCallback_Invoke_m894695456 (UpdateCallback_t3991193291 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdateCallback_Invoke_m894695456((UpdateCallback_t3991193291 *)__this->get_prev_9(), method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
		{
			// open
			typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
		}
		else
		{
			// closed
			typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
		}
	}
	else
	{
		{
			// closed
			typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
		}
	}
}
// System.IAsyncResult SimpleInput/UpdateCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UpdateCallback_BeginInvoke_m3727983531 (UpdateCallback_t3991193291 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void SimpleInput/UpdateCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateCallback_EndInvoke_m2276539723 (UpdateCallback_t3991193291 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.AxisInputKeyboard::.ctor()
extern "C"  void AxisInputKeyboard__ctor_m1434635288 (AxisInputKeyboard_t1690261069 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputKeyboard__ctor_m1434635288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput axis = new SimpleInput.AxisInput();
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m143791316(L_0, /*hidden argument*/NULL);
		__this->set_axis_4(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputKeyboard::OnEnable()
extern "C"  void AxisInputKeyboard_OnEnable_m697364291 (AxisInputKeyboard_t1690261069 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputKeyboard_OnEnable_m697364291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// axis.StartTracking();
		AxisInput_t802457650 * L_0 = __this->get_axis_4();
		// axis.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3188229854(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_1 = (intptr_t)AxisInputKeyboard_OnUpdate_m773120620_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputKeyboard::OnDisable()
extern "C"  void AxisInputKeyboard_OnDisable_m3983922163 (AxisInputKeyboard_t1690261069 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputKeyboard_OnDisable_m3983922163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// axis.StopTracking();
		AxisInput_t802457650 * L_0 = __this->get_axis_4();
		// axis.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m2000325864(L_0, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_1 = (intptr_t)AxisInputKeyboard_OnUpdate_m773120620_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputKeyboard::OnUpdate()
extern "C"  void AxisInputKeyboard_OnUpdate_m773120620 (AxisInputKeyboard_t1690261069 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputKeyboard_OnUpdate_m773120620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AxisInput_t802457650 * G_B2_0 = NULL;
	AxisInput_t802457650 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	AxisInput_t802457650 * G_B3_1 = NULL;
	{
		// axis.value = Input.GetKey( key ) ? value : 0f;
		AxisInput_t802457650 * L_0 = __this->get_axis_4();
		int32_t L_1 = __this->get_key_2();
		// axis.value = Input.GetKey( key ) ? value : 0f;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0022;
		}
	}
	{
		float L_3 = __this->get_value_3();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0022:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		NullCheck(G_B3_1);
		((BaseInput_2_t1381185473 *)G_B3_1)->set_value_1(G_B3_0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.AxisInputMouse::.ctor()
extern "C"  void AxisInputMouse__ctor_m996829082 (AxisInputMouse_t3174651034 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputMouse__ctor_m996829082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput xAxis = new SimpleInput.AxisInput();
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m143791316(L_0, /*hidden argument*/NULL);
		__this->set_xAxis_2(L_0);
		// public SimpleInput.AxisInput yAxis = new SimpleInput.AxisInput();
		AxisInput_t802457650 * L_1 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m143791316(L_1, /*hidden argument*/NULL);
		__this->set_yAxis_3(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.AxisInputUI::.ctor()
extern "C"  void AxisInputUI__ctor_m3583187817 (AxisInputUI_t3093488737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputUI__ctor_m3583187817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput axis = new SimpleInput.AxisInput();
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m143791316(L_0, /*hidden argument*/NULL);
		__this->set_axis_3(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputUI::Awake()
extern "C"  void AxisInputUI_Awake_m2387327640 (AxisInputUI_t3093488737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputUI_Awake_m2387327640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	{
		// Graphic graphic = GetComponent<Graphic>();
		// Graphic graphic = GetComponent<Graphic>();
		Graphic_t1660335611 * L_0 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		V_0 = L_0;
		// if( graphic != null )
		Graphic_t1660335611 * L_1 = V_0;
		// if( graphic != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// graphic.raycastTarget = true;
		Graphic_t1660335611 * L_3 = V_0;
		// graphic.raycastTarget = true;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_3, (bool)1);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputUI::OnEnable()
extern "C"  void AxisInputUI_OnEnable_m3469068147 (AxisInputUI_t3093488737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputUI_OnEnable_m3469068147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// axis.StartTracking();
		AxisInput_t802457650 * L_0 = __this->get_axis_3();
		// axis.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3188229854(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputUI::OnDisable()
extern "C"  void AxisInputUI_OnDisable_m1473024516 (AxisInputUI_t3093488737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AxisInputUI_OnDisable_m1473024516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// axis.StopTracking();
		AxisInput_t802457650 * L_0 = __this->get_axis_3();
		// axis.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m2000325864(L_0, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputUI::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void AxisInputUI_OnPointerDown_m3175300798 (AxisInputUI_t3093488737 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// axis.value = value;
		AxisInput_t802457650 * L_0 = __this->get_axis_3();
		float L_1 = __this->get_value_2();
		NullCheck(L_0);
		((BaseInput_2_t1381185473 *)L_0)->set_value_1(L_1);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.AxisInputUI::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void AxisInputUI_OnPointerUp_m1541387311 (AxisInputUI_t3093488737 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// axis.value = 0f;
		AxisInput_t802457650 * L_0 = __this->get_axis_3();
		NullCheck(L_0);
		((BaseInput_2_t1381185473 *)L_0)->set_value_1((0.0f));
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.ButtonInputKeyboard::.ctor()
extern "C"  void ButtonInputKeyboard__ctor_m1168318295 (ButtonInputKeyboard_t3640535414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputKeyboard__ctor_m1168318295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.ButtonInput button = new SimpleInput.ButtonInput();
		ButtonInput_t2818951903 * L_0 = (ButtonInput_t2818951903 *)il2cpp_codegen_object_new(ButtonInput_t2818951903_il2cpp_TypeInfo_var);
		ButtonInput__ctor_m2907722220(L_0, /*hidden argument*/NULL);
		__this->set_button_3(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputKeyboard::OnEnable()
extern "C"  void ButtonInputKeyboard_OnEnable_m508473374 (ButtonInputKeyboard_t3640535414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputKeyboard_OnEnable_m508473374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.StartTracking();
		ButtonInput_t2818951903 * L_0 = __this->get_button_3();
		// button.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3481582819(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3481582819_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_1 = (intptr_t)ButtonInputKeyboard_OnUpdate_m3691304227_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputKeyboard::OnDisable()
extern "C"  void ButtonInputKeyboard_OnDisable_m1528174353 (ButtonInputKeyboard_t3640535414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputKeyboard_OnDisable_m1528174353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.StopTracking();
		ButtonInput_t2818951903 * L_0 = __this->get_button_3();
		// button.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m1977248712(L_0, /*hidden argument*/BaseInput_2_StopTracking_m1977248712_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_1 = (intptr_t)ButtonInputKeyboard_OnUpdate_m3691304227_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputKeyboard::OnUpdate()
extern "C"  void ButtonInputKeyboard_OnUpdate_m3691304227 (ButtonInputKeyboard_t3640535414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputKeyboard_OnUpdate_m3691304227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.value = Input.GetKey( key );
		ButtonInput_t2818951903 * L_0 = __this->get_button_3();
		int32_t L_1 = __this->get_key_2();
		// button.value = Input.GetKey( key );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		((BaseInput_2_t81206664 *)L_0)->set_value_1(L_2);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.ButtonInputUI::.ctor()
extern "C"  void ButtonInputUI__ctor_m1861433565 (ButtonInputUI_t1749738659 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputUI__ctor_m1861433565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.ButtonInput button = new SimpleInput.ButtonInput();
		ButtonInput_t2818951903 * L_0 = (ButtonInput_t2818951903 *)il2cpp_codegen_object_new(ButtonInput_t2818951903_il2cpp_TypeInfo_var);
		ButtonInput__ctor_m2907722220(L_0, /*hidden argument*/NULL);
		__this->set_button_2(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputUI::Awake()
extern "C"  void ButtonInputUI_Awake_m4178634406 (ButtonInputUI_t1749738659 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputUI_Awake_m4178634406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	{
		// Graphic graphic = GetComponent<Graphic>();
		// Graphic graphic = GetComponent<Graphic>();
		Graphic_t1660335611 * L_0 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		V_0 = L_0;
		// if( graphic != null )
		Graphic_t1660335611 * L_1 = V_0;
		// if( graphic != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// graphic.raycastTarget = true;
		Graphic_t1660335611 * L_3 = V_0;
		// graphic.raycastTarget = true;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_3, (bool)1);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputUI::OnEnable()
extern "C"  void ButtonInputUI_OnEnable_m2872570879 (ButtonInputUI_t1749738659 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputUI_OnEnable_m2872570879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.StartTracking();
		ButtonInput_t2818951903 * L_0 = __this->get_button_2();
		// button.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3481582819(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3481582819_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputUI::OnDisable()
extern "C"  void ButtonInputUI_OnDisable_m1423996615 (ButtonInputUI_t1749738659 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonInputUI_OnDisable_m1423996615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.StopTracking();
		ButtonInput_t2818951903 * L_0 = __this->get_button_2();
		// button.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m1977248712(L_0, /*hidden argument*/BaseInput_2_StopTracking_m1977248712_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputUI::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ButtonInputUI_OnPointerDown_m1322390184 (ButtonInputUI_t1749738659 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// button.value = true;
		ButtonInput_t2818951903 * L_0 = __this->get_button_2();
		NullCheck(L_0);
		((BaseInput_2_t81206664 *)L_0)->set_value_1((bool)1);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.ButtonInputUI::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ButtonInputUI_OnPointerUp_m2500598661 (ButtonInputUI_t1749738659 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// button.value = false;
		ButtonInput_t2818951903 * L_0 = __this->get_button_2();
		NullCheck(L_0);
		((BaseInput_2_t81206664 *)L_0)->set_value_1((bool)0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.Dpad::.ctor()
extern "C"  void Dpad__ctor_m1388007440 (Dpad_t1007306070 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dpad__ctor_m1388007440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput xAxis = new SimpleInput.AxisInput( "Horizontal" );
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_0, _stringLiteral1828639942, /*hidden argument*/NULL);
		__this->set_xAxis_2(L_0);
		// public SimpleInput.AxisInput yAxis = new SimpleInput.AxisInput( "Vertical" );
		AxisInput_t802457650 * L_1 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_1, _stringLiteral2984908384, /*hidden argument*/NULL);
		__this->set_yAxis_3(L_1);
		// private Vector2 m_value = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_value_6(L_2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 SimpleInputNamespace.Dpad::get_Value()
extern "C"  Vector2_t2156229523  Dpad_get_Value_m3074965257 (Dpad_t1007306070 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public Vector2 Value { get { return m_value; } }
		Vector2_t2156229523  L_0 = __this->get_m_value_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// public Vector2 Value { get { return m_value; } }
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
// System.Void SimpleInputNamespace.Dpad::Awake()
extern "C"  void Dpad_Awake_m629104672 (Dpad_t1007306070 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dpad_Awake_m629104672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SimpleInputDragListener_t3058349199 * V_0 = NULL;
	{
		// rectTransform = (RectTransform) transform;
		// rectTransform = (RectTransform) transform;
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_rectTransform_4(((RectTransform_t3704657025 *)CastclassSealed((RuntimeObject*)L_0, RectTransform_t3704657025_il2cpp_TypeInfo_var)));
		// background = GetComponent<Graphic>();
		// background = GetComponent<Graphic>();
		Graphic_t1660335611 * L_1 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		__this->set_background_5(L_1);
		// if( background != null )
		Graphic_t1660335611 * L_2 = __this->get_background_5();
		// if( background != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		// background.raycastTarget = true;
		Graphic_t1660335611 * L_4 = __this->get_background_5();
		// background.raycastTarget = true;
		NullCheck(L_4);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_4, (bool)1);
	}

IL_003b:
	{
		// SimpleInputDragListener eventReceiver = gameObject.AddComponent<SimpleInputDragListener>();
		// SimpleInputDragListener eventReceiver = gameObject.AddComponent<SimpleInputDragListener>();
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		// SimpleInputDragListener eventReceiver = gameObject.AddComponent<SimpleInputDragListener>();
		NullCheck(L_5);
		SimpleInputDragListener_t3058349199 * L_6 = GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994(L_5, /*hidden argument*/GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994_RuntimeMethod_var);
		V_0 = L_6;
		// eventReceiver.Listener = this;
		SimpleInputDragListener_t3058349199 * L_7 = V_0;
		// eventReceiver.Listener = this;
		NullCheck(L_7);
		SimpleInputDragListener_set_Listener_m3326121527(L_7, __this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Dpad::OnEnable()
extern "C"  void Dpad_OnEnable_m3687405751 (Dpad_t1007306070 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dpad_OnEnable_m3687405751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// xAxis.StartTracking();
		AxisInput_t802457650 * L_0 = __this->get_xAxis_2();
		// xAxis.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3188229854(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// yAxis.StartTracking();
		AxisInput_t802457650 * L_1 = __this->get_yAxis_3();
		// yAxis.StartTracking();
		NullCheck(L_1);
		BaseInput_2_StartTracking_m3188229854(L_1, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Dpad::OnDisable()
extern "C"  void Dpad_OnDisable_m3832001405 (Dpad_t1007306070 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dpad_OnDisable_m3832001405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// xAxis.StopTracking();
		AxisInput_t802457650 * L_0 = __this->get_xAxis_2();
		// xAxis.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m2000325864(L_0, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// yAxis.StopTracking();
		AxisInput_t802457650 * L_1 = __this->get_yAxis_3();
		// yAxis.StopTracking();
		NullCheck(L_1);
		BaseInput_2_StopTracking_m2000325864(L_1, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Dpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Dpad_OnPointerDown_m3237108463 (Dpad_t1007306070 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// CalculateInput( eventData );
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		// CalculateInput( eventData );
		Dpad_CalculateInput_m4151013375(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Dpad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Dpad_OnDrag_m2373604601 (Dpad_t1007306070 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// CalculateInput( eventData );
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		// CalculateInput( eventData );
		Dpad_CalculateInput_m4151013375(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Dpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Dpad_OnPointerUp_m1024412546 (Dpad_t1007306070 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dpad_OnPointerUp_m1024412546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_value = Vector2.zero;
		// m_value = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_value_6(L_0);
		// xAxis.value = 0f;
		AxisInput_t802457650 * L_1 = __this->get_xAxis_2();
		NullCheck(L_1);
		((BaseInput_2_t1381185473 *)L_1)->set_value_1((0.0f));
		// yAxis.value = 0f;
		AxisInput_t802457650 * L_2 = __this->get_yAxis_3();
		NullCheck(L_2);
		((BaseInput_2_t1381185473 *)L_2)->set_value_1((0.0f));
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Dpad::CalculateInput(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Dpad_CalculateInput_m4151013375 (Dpad_t1007306070 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dpad_CalculateInput_m4151013375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( rectTransform, eventData.position, background.canvas.worldCamera, out pointerPos );
		RectTransform_t3704657025 * L_0 = __this->get_rectTransform_4();
		PointerEventData_t3807901092 * L_1 = ___eventData0;
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( rectTransform, eventData.position, background.canvas.worldCamera, out pointerPos );
		NullCheck(L_1);
		Vector2_t2156229523  L_2 = PointerEventData_get_position_m437660275(L_1, /*hidden argument*/NULL);
		Graphic_t1660335611 * L_3 = __this->get_background_5();
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( rectTransform, eventData.position, background.canvas.worldCamera, out pointerPos );
		NullCheck(L_3);
		Canvas_t3310196443 * L_4 = Graphic_get_canvas_m3320066409(L_3, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( rectTransform, eventData.position, background.canvas.worldCamera, out pointerPos );
		NullCheck(L_4);
		Camera_t4157153871 * L_5 = Canvas_get_worldCamera_m3516267897(L_4, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( rectTransform, eventData.position, background.canvas.worldCamera, out pointerPos );
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187(NULL /*static, unused*/, L_0, L_2, L_5, (&V_0), /*hidden argument*/NULL);
		// if( pointerPos.sqrMagnitude <= 400f )
		// if( pointerPos.sqrMagnitude <= 400f )
		float L_6 = Vector2_get_sqrMagnitude_m837837635((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_6) <= ((float)(400.0f)))))
		{
			goto IL_003b;
		}
	}
	{
		// return;
		goto IL_0158;
	}

IL_003b:
	{
		// float angle = Vector2.Angle( pointerPos, Vector2.right );
		Vector2_t2156229523  L_7 = V_0;
		// float angle = Vector2.Angle( pointerPos, Vector2.right );
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_8 = Vector2_get_right_m1027081661(NULL /*static, unused*/, /*hidden argument*/NULL);
		// float angle = Vector2.Angle( pointerPos, Vector2.right );
		float L_9 = Vector2_Angle_m4105581454(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		// if( pointerPos.y < 0f )
		float L_10 = (&V_0)->get_y_1();
		if ((!(((float)L_10) < ((float)(0.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		// angle = 360f - angle;
		float L_11 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract((float)(360.0f), (float)L_11));
	}

IL_0060:
	{
		// if( angle >= 25f && angle <= 155f )
		float L_12 = V_1;
		if ((!(((float)L_12) >= ((float)(25.0f)))))
		{
			goto IL_008b;
		}
	}
	{
		float L_13 = V_1;
		if ((!(((float)L_13) <= ((float)(155.0f)))))
		{
			goto IL_008b;
		}
	}
	{
		// m_value.y = 1f;
		Vector2_t2156229523 * L_14 = __this->get_address_of_m_value_6();
		L_14->set_y_1((1.0f));
		goto IL_00c6;
	}

IL_008b:
	{
		// else if( angle >= 205f && angle <= 335f )
		float L_15 = V_1;
		if ((!(((float)L_15) >= ((float)(205.0f)))))
		{
			goto IL_00b6;
		}
	}
	{
		float L_16 = V_1;
		if ((!(((float)L_16) <= ((float)(335.0f)))))
		{
			goto IL_00b6;
		}
	}
	{
		// m_value.y = -1f;
		Vector2_t2156229523 * L_17 = __this->get_address_of_m_value_6();
		L_17->set_y_1((-1.0f));
		goto IL_00c6;
	}

IL_00b6:
	{
		// m_value.y = 0f;
		Vector2_t2156229523 * L_18 = __this->get_address_of_m_value_6();
		L_18->set_y_1((0.0f));
	}

IL_00c6:
	{
		// if( angle <= 65f || angle >= 295f )
		float L_19 = V_1;
		if ((((float)L_19) <= ((float)(65.0f))))
		{
			goto IL_00dc;
		}
	}
	{
		float L_20 = V_1;
		if ((!(((float)L_20) >= ((float)(295.0f)))))
		{
			goto IL_00f1;
		}
	}

IL_00dc:
	{
		// m_value.x = 1f;
		Vector2_t2156229523 * L_21 = __this->get_address_of_m_value_6();
		L_21->set_x_0((1.0f));
		goto IL_012c;
	}

IL_00f1:
	{
		// else if( angle >= 115f && angle <= 245f )
		float L_22 = V_1;
		if ((!(((float)L_22) >= ((float)(115.0f)))))
		{
			goto IL_011c;
		}
	}
	{
		float L_23 = V_1;
		if ((!(((float)L_23) <= ((float)(245.0f)))))
		{
			goto IL_011c;
		}
	}
	{
		// m_value.x = -1f;
		Vector2_t2156229523 * L_24 = __this->get_address_of_m_value_6();
		L_24->set_x_0((-1.0f));
		goto IL_012c;
	}

IL_011c:
	{
		// m_value.x = 0f;
		Vector2_t2156229523 * L_25 = __this->get_address_of_m_value_6();
		L_25->set_x_0((0.0f));
	}

IL_012c:
	{
		// xAxis.value = m_value.x;
		AxisInput_t802457650 * L_26 = __this->get_xAxis_2();
		Vector2_t2156229523 * L_27 = __this->get_address_of_m_value_6();
		float L_28 = L_27->get_x_0();
		NullCheck(L_26);
		((BaseInput_2_t1381185473 *)L_26)->set_value_1(L_28);
		// yAxis.value = m_value.y;
		AxisInput_t802457650 * L_29 = __this->get_yAxis_3();
		Vector2_t2156229523 * L_30 = __this->get_address_of_m_value_6();
		float L_31 = L_30->get_y_1();
		NullCheck(L_29);
		((BaseInput_2_t1381185473 *)L_29)->set_value_1(L_31);
	}

IL_0158:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.Joystick::.ctor()
extern "C"  void Joystick__ctor_m170445243 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick__ctor_m170445243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput xAxis = new SimpleInput.AxisInput( "Horizontal" );
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_0, _stringLiteral1828639942, /*hidden argument*/NULL);
		__this->set_xAxis_2(L_0);
		// public SimpleInput.AxisInput yAxis = new SimpleInput.AxisInput( "Vertical" );
		AxisInput_t802457650 * L_1 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_1, _stringLiteral2984908384, /*hidden argument*/NULL);
		__this->set_yAxis_3(L_1);
		// public MovementAxes movementAxes = MovementAxes.XandY;
		__this->set_movementAxes_8(0);
		// private float movementAreaRadius = 75f;
		__this->set_movementAreaRadius_9((75.0f));
		// private bool isDynamicJoystick = false;
		__this->set_isDynamicJoystick_10((bool)0);
		// private bool joystickHeld = false;
		__this->set_joystickHeld_12((bool)0);
		// private float opacity = 1f;
		__this->set_opacity_16((1.0f));
		// private Vector2 m_value = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_value_17(L_2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 SimpleInputNamespace.Joystick::get_Value()
extern "C"  Vector2_t2156229523  Joystick_get_Value_m2067427618 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public Vector2 Value { get { return m_value; } }
		Vector2_t2156229523  L_0 = __this->get_m_value_17();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// public Vector2 Value { get { return m_value; } }
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
// System.Void SimpleInputNamespace.Joystick::Awake()
extern "C"  void Joystick_Awake_m4093454875 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Awake_m4093454875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2670269651 * V_0 = NULL;
	{
		// joystickTR = (RectTransform) transform;
		// joystickTR = (RectTransform) transform;
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_joystickTR_4(((RectTransform_t3704657025 *)CastclassSealed((RuntimeObject*)L_0, RectTransform_t3704657025_il2cpp_TypeInfo_var)));
		// thumbTR = thumb.rectTransform;
		Image_t2670269651 * L_1 = __this->get_thumb_6();
		// thumbTR = thumb.rectTransform;
		NullCheck(L_1);
		RectTransform_t3704657025 * L_2 = Graphic_get_rectTransform_m1167152468(L_1, /*hidden argument*/NULL);
		__this->set_thumbTR_7(L_2);
		// Image bgImage = GetComponent<Image>();
		// Image bgImage = GetComponent<Image>();
		Image_t2670269651 * L_3 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		V_0 = L_3;
		// if( bgImage != null )
		Image_t2670269651 * L_4 = V_0;
		// if( bgImage != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		// background = bgImage;
		Image_t2670269651 * L_6 = V_0;
		__this->set_background_5(L_6);
		// background.raycastTarget = false;
		Image_t2670269651 * L_7 = __this->get_background_5();
		// background.raycastTarget = false;
		NullCheck(L_7);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_7, (bool)0);
	}

IL_004b:
	{
		// if( isDynamicJoystick )
		bool L_8 = __this->get_isDynamicJoystick_10();
		if (!L_8)
		{
			goto IL_007a;
		}
	}
	{
		// opacity = 0f;
		__this->set_opacity_16((0.0f));
		// thumb.raycastTarget = false;
		Image_t2670269651 * L_9 = __this->get_thumb_6();
		// thumb.raycastTarget = false;
		NullCheck(L_9);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_9, (bool)0);
		// OnUpdate();
		// OnUpdate();
		Joystick_OnUpdate_m3745511237(__this, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_007a:
	{
		// thumb.raycastTarget = true;
		Image_t2670269651 * L_10 = __this->get_thumb_6();
		// thumb.raycastTarget = true;
		NullCheck(L_10);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_10, (bool)1);
	}

IL_0086:
	{
		// _1OverMovementAreaRadius = 1f / movementAreaRadius;
		float L_11 = __this->get_movementAreaRadius_9();
		__this->set__1OverMovementAreaRadius_14(((float)((float)(1.0f)/(float)L_11)));
		// movementAreaRadiusSqr = movementAreaRadius * movementAreaRadius;
		float L_12 = __this->get_movementAreaRadius_9();
		float L_13 = __this->get_movementAreaRadius_9();
		__this->set_movementAreaRadiusSqr_15(((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)));
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::Start()
extern "C"  void Joystick_Start_m1419705193 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Start_m1419705193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SimpleInputDragListener_t3058349199 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	Image_t2670269651 * V_2 = NULL;
	{
		// if( !isDynamicJoystick )
		bool L_0 = __this->get_isDynamicJoystick_10();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		// eventReceiver = thumbTR.gameObject.AddComponent<SimpleInputDragListener>();
		RectTransform_t3704657025 * L_1 = __this->get_thumbTR_7();
		// eventReceiver = thumbTR.gameObject.AddComponent<SimpleInputDragListener>();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		// eventReceiver = thumbTR.gameObject.AddComponent<SimpleInputDragListener>();
		NullCheck(L_2);
		SimpleInputDragListener_t3058349199 * L_3 = GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994(L_2, /*hidden argument*/GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994_RuntimeMethod_var);
		V_0 = L_3;
		goto IL_0132;
	}

IL_0022:
	{
		// if( dynamicJoystickMovementArea == null )
		RectTransform_t3704657025 * L_4 = __this->get_dynamicJoystickMovementArea_11();
		// if( dynamicJoystickMovementArea == null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00d4;
		}
	}
	{
		// Transform canvasTransform = thumb.canvas.transform;
		Image_t2670269651 * L_6 = __this->get_thumb_6();
		// Transform canvasTransform = thumb.canvas.transform;
		NullCheck(L_6);
		Canvas_t3310196443 * L_7 = Graphic_get_canvas_m3320066409(L_6, /*hidden argument*/NULL);
		// Transform canvasTransform = thumb.canvas.transform;
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		// dynamicJoystickMovementArea = new GameObject( "Dynamic Joystick Movement Area", typeof( RectTransform ), typeof( Image ) ).GetComponent<RectTransform>();
		TypeU5BU5D_t3940880105* L_9 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)2));
		RuntimeTypeHandle_t3027515415  L_10 = { reinterpret_cast<intptr_t> (RectTransform_t3704657025_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_11);
		TypeU5BU5D_t3940880105* L_12 = L_9;
		RuntimeTypeHandle_t3027515415  L_13 = { reinterpret_cast<intptr_t> (Image_t2670269651_0_0_0_var) };
		Type_t * L_14 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_14);
		// dynamicJoystickMovementArea = new GameObject( "Dynamic Joystick Movement Area", typeof( RectTransform ), typeof( Image ) ).GetComponent<RectTransform>();
		GameObject_t1113636619 * L_15 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_15, _stringLiteral2150701781, L_12, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea = new GameObject( "Dynamic Joystick Movement Area", typeof( RectTransform ), typeof( Image ) ).GetComponent<RectTransform>();
		NullCheck(L_15);
		RectTransform_t3704657025 * L_16 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_15, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		__this->set_dynamicJoystickMovementArea_11(L_16);
		// dynamicJoystickMovementArea.SetParent( canvasTransform, false );
		RectTransform_t3704657025 * L_17 = __this->get_dynamicJoystickMovementArea_11();
		Transform_t3600365921 * L_18 = V_1;
		// dynamicJoystickMovementArea.SetParent( canvasTransform, false );
		NullCheck(L_17);
		Transform_SetParent_m273471670(L_17, L_18, (bool)0, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.SetAsFirstSibling();
		RectTransform_t3704657025 * L_19 = __this->get_dynamicJoystickMovementArea_11();
		// dynamicJoystickMovementArea.SetAsFirstSibling();
		NullCheck(L_19);
		Transform_SetAsFirstSibling_m253439912(L_19, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.anchorMin = Vector2.zero;
		RectTransform_t3704657025 * L_20 = __this->get_dynamicJoystickMovementArea_11();
		// dynamicJoystickMovementArea.anchorMin = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_21 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.anchorMin = Vector2.zero;
		NullCheck(L_20);
		RectTransform_set_anchorMin_m4230103102(L_20, L_21, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.anchorMax = Vector2.one;
		RectTransform_t3704657025 * L_22 = __this->get_dynamicJoystickMovementArea_11();
		// dynamicJoystickMovementArea.anchorMax = Vector2.one;
		Vector2_t2156229523  L_23 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.anchorMax = Vector2.one;
		NullCheck(L_22);
		RectTransform_set_anchorMax_m2998668828(L_22, L_23, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.sizeDelta = Vector2.zero;
		RectTransform_t3704657025 * L_24 = __this->get_dynamicJoystickMovementArea_11();
		// dynamicJoystickMovementArea.sizeDelta = Vector2.zero;
		Vector2_t2156229523  L_25 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.sizeDelta = Vector2.zero;
		NullCheck(L_24);
		RectTransform_set_sizeDelta_m3462269772(L_24, L_25, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.anchoredPosition = Vector2.zero;
		RectTransform_t3704657025 * L_26 = __this->get_dynamicJoystickMovementArea_11();
		// dynamicJoystickMovementArea.anchoredPosition = Vector2.zero;
		Vector2_t2156229523  L_27 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		// dynamicJoystickMovementArea.anchoredPosition = Vector2.zero;
		NullCheck(L_26);
		RectTransform_set_anchoredPosition_m4126691837(L_26, L_27, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		// Image dynamicJoystickMovementAreaRaycastTarget = dynamicJoystickMovementArea.GetComponent<Image>();
		RectTransform_t3704657025 * L_28 = __this->get_dynamicJoystickMovementArea_11();
		// Image dynamicJoystickMovementAreaRaycastTarget = dynamicJoystickMovementArea.GetComponent<Image>();
		NullCheck(L_28);
		Image_t2670269651 * L_29 = Component_GetComponent_TisImage_t2670269651_m980647750(L_28, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		V_2 = L_29;
		// if( dynamicJoystickMovementAreaRaycastTarget == null )
		Image_t2670269651 * L_30 = V_2;
		// if( dynamicJoystickMovementAreaRaycastTarget == null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_31 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_30, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00fd;
		}
	}
	{
		// dynamicJoystickMovementAreaRaycastTarget = dynamicJoystickMovementArea.gameObject.AddComponent<Image>();
		RectTransform_t3704657025 * L_32 = __this->get_dynamicJoystickMovementArea_11();
		// dynamicJoystickMovementAreaRaycastTarget = dynamicJoystickMovementArea.gameObject.AddComponent<Image>();
		NullCheck(L_32);
		GameObject_t1113636619 * L_33 = Component_get_gameObject_m442555142(L_32, /*hidden argument*/NULL);
		// dynamicJoystickMovementAreaRaycastTarget = dynamicJoystickMovementArea.gameObject.AddComponent<Image>();
		NullCheck(L_33);
		Image_t2670269651 * L_34 = GameObject_AddComponent_TisImage_t2670269651_m1594579417(L_33, /*hidden argument*/GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var);
		V_2 = L_34;
	}

IL_00fd:
	{
		// dynamicJoystickMovementAreaRaycastTarget.sprite = thumb.sprite;
		Image_t2670269651 * L_35 = V_2;
		Image_t2670269651 * L_36 = __this->get_thumb_6();
		// dynamicJoystickMovementAreaRaycastTarget.sprite = thumb.sprite;
		NullCheck(L_36);
		Sprite_t280657092 * L_37 = Image_get_sprite_m1811690853(L_36, /*hidden argument*/NULL);
		// dynamicJoystickMovementAreaRaycastTarget.sprite = thumb.sprite;
		NullCheck(L_35);
		Image_set_sprite_m2369174689(L_35, L_37, /*hidden argument*/NULL);
		// dynamicJoystickMovementAreaRaycastTarget.color = Color.clear;
		Image_t2670269651 * L_38 = V_2;
		// dynamicJoystickMovementAreaRaycastTarget.color = Color.clear;
		Color_t2555686324  L_39 = Color_get_clear_m1016382534(NULL /*static, unused*/, /*hidden argument*/NULL);
		// dynamicJoystickMovementAreaRaycastTarget.color = Color.clear;
		NullCheck(L_38);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_38, L_39);
		// dynamicJoystickMovementAreaRaycastTarget.raycastTarget = true;
		Image_t2670269651 * L_40 = V_2;
		// dynamicJoystickMovementAreaRaycastTarget.raycastTarget = true;
		NullCheck(L_40);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_40, (bool)1);
		// eventReceiver = dynamicJoystickMovementArea.gameObject.AddComponent<SimpleInputDragListener>();
		RectTransform_t3704657025 * L_41 = __this->get_dynamicJoystickMovementArea_11();
		// eventReceiver = dynamicJoystickMovementArea.gameObject.AddComponent<SimpleInputDragListener>();
		NullCheck(L_41);
		GameObject_t1113636619 * L_42 = Component_get_gameObject_m442555142(L_41, /*hidden argument*/NULL);
		// eventReceiver = dynamicJoystickMovementArea.gameObject.AddComponent<SimpleInputDragListener>();
		NullCheck(L_42);
		SimpleInputDragListener_t3058349199 * L_43 = GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994(L_42, /*hidden argument*/GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994_RuntimeMethod_var);
		V_0 = L_43;
	}

IL_0132:
	{
		// eventReceiver.Listener = this;
		SimpleInputDragListener_t3058349199 * L_44 = V_0;
		// eventReceiver.Listener = this;
		NullCheck(L_44);
		SimpleInputDragListener_set_Listener_m3326121527(L_44, __this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::OnEnable()
extern "C"  void Joystick_OnEnable_m2515348506 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_OnEnable_m2515348506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// xAxis.StartTracking();
		AxisInput_t802457650 * L_0 = __this->get_xAxis_2();
		// xAxis.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3188229854(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// yAxis.StartTracking();
		AxisInput_t802457650 * L_1 = __this->get_yAxis_3();
		// yAxis.StartTracking();
		NullCheck(L_1);
		BaseInput_2_StartTracking_m3188229854(L_1, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_2 = (intptr_t)Joystick_OnUpdate_m3745511237_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_3 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_3, __this, L_2, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::OnDisable()
extern "C"  void Joystick_OnDisable_m4097605840 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_OnDisable_m4097605840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// xAxis.StopTracking();
		AxisInput_t802457650 * L_0 = __this->get_xAxis_2();
		// xAxis.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m2000325864(L_0, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// yAxis.StopTracking();
		AxisInput_t802457650 * L_1 = __this->get_yAxis_3();
		// yAxis.StopTracking();
		NullCheck(L_1);
		BaseInput_2_StopTracking_m2000325864(L_1, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_2 = (intptr_t)Joystick_OnUpdate_m3745511237_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_3 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_3, __this, L_2, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Joystick_OnPointerDown_m1051375263 (Joystick_t1225167045 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_OnPointerDown_m1051375263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// joystickHeld = true;
		__this->set_joystickHeld_12((bool)1);
		// if( isDynamicJoystick )
		bool L_0 = __this->get_isDynamicJoystick_10();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		// pointerInitialPos = Vector2.zero;
		// pointerInitialPos = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_pointerInitialPos_13(L_1);
		// joystickTR.position = eventData.position;
		RectTransform_t3704657025 * L_2 = __this->get_joystickTR_4();
		PointerEventData_t3807901092 * L_3 = ___eventData0;
		// joystickTR.position = eventData.position;
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = PointerEventData_get_position_m437660275(L_3, /*hidden argument*/NULL);
		// joystickTR.position = eventData.position;
		Vector3_t3722313464  L_5 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// joystickTR.position = eventData.position;
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_5, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_003b:
	{
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerInitialPos );
		RectTransform_t3704657025 * L_6 = __this->get_joystickTR_4();
		PointerEventData_t3807901092 * L_7 = ___eventData0;
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerInitialPos );
		NullCheck(L_7);
		Vector2_t2156229523  L_8 = PointerEventData_get_position_m437660275(L_7, /*hidden argument*/NULL);
		Image_t2670269651 * L_9 = __this->get_thumb_6();
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerInitialPos );
		NullCheck(L_9);
		Canvas_t3310196443 * L_10 = Graphic_get_canvas_m3320066409(L_9, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerInitialPos );
		NullCheck(L_10);
		Camera_t4157153871 * L_11 = Canvas_get_worldCamera_m3516267897(L_10, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_12 = __this->get_address_of_pointerInitialPos_13();
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerInitialPos );
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187(NULL /*static, unused*/, L_6, L_8, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0063:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Joystick_OnDrag_m3054587775 (Joystick_t1225167045 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_OnDrag_m3054587775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerPos );
		RectTransform_t3704657025 * L_0 = __this->get_joystickTR_4();
		PointerEventData_t3807901092 * L_1 = ___eventData0;
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerPos );
		NullCheck(L_1);
		Vector2_t2156229523  L_2 = PointerEventData_get_position_m437660275(L_1, /*hidden argument*/NULL);
		Image_t2670269651 * L_3 = __this->get_thumb_6();
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerPos );
		NullCheck(L_3);
		Canvas_t3310196443 * L_4 = Graphic_get_canvas_m3320066409(L_3, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerPos );
		NullCheck(L_4);
		Camera_t4157153871 * L_5 = Canvas_get_worldCamera_m3516267897(L_4, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToLocalPointInRectangle( joystickTR, eventData.position, thumb.canvas.worldCamera, out pointerPos );
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187(NULL /*static, unused*/, L_0, L_2, L_5, (&V_0), /*hidden argument*/NULL);
		// Vector2 direction = pointerPos - pointerInitialPos;
		Vector2_t2156229523  L_6 = V_0;
		Vector2_t2156229523  L_7 = __this->get_pointerInitialPos_13();
		// Vector2 direction = pointerPos - pointerInitialPos;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_8 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		// if( movementAxes == MovementAxes.X )
		int32_t L_9 = __this->get_movementAxes_8();
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		// direction.y = 0f;
		(&V_1)->set_y_1((0.0f));
		goto IL_0067;
	}

IL_004f:
	{
		// else if( movementAxes == MovementAxes.Y )
		int32_t L_10 = __this->get_movementAxes_8();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0067;
		}
	}
	{
		// direction.x = 0f;
		(&V_1)->set_x_0((0.0f));
	}

IL_0067:
	{
		// if( direction.sqrMagnitude > movementAreaRadiusSqr )
		// if( direction.sqrMagnitude > movementAreaRadiusSqr )
		float L_11 = Vector2_get_sqrMagnitude_m837837635((&V_1), /*hidden argument*/NULL);
		float L_12 = __this->get_movementAreaRadiusSqr_15();
		if ((!(((float)L_11) > ((float)L_12))))
		{
			goto IL_008c;
		}
	}
	{
		// direction = direction.normalized * movementAreaRadius;
		// direction = direction.normalized * movementAreaRadius;
		Vector2_t2156229523  L_13 = Vector2_get_normalized_m2683665860((&V_1), /*hidden argument*/NULL);
		float L_14 = __this->get_movementAreaRadius_9();
		// direction = direction.normalized * movementAreaRadius;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_15 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_008c:
	{
		// m_value = direction * _1OverMovementAreaRadius;
		Vector2_t2156229523  L_16 = V_1;
		float L_17 = __this->get__1OverMovementAreaRadius_14();
		// m_value = direction * _1OverMovementAreaRadius;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_18 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		__this->set_m_value_17(L_18);
		// thumbTR.localPosition = direction;
		RectTransform_t3704657025 * L_19 = __this->get_thumbTR_7();
		Vector2_t2156229523  L_20 = V_1;
		// thumbTR.localPosition = direction;
		Vector3_t3722313464  L_21 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		// thumbTR.localPosition = direction;
		NullCheck(L_19);
		Transform_set_localPosition_m4128471975(L_19, L_21, /*hidden argument*/NULL);
		// xAxis.value = m_value.x;
		AxisInput_t802457650 * L_22 = __this->get_xAxis_2();
		Vector2_t2156229523 * L_23 = __this->get_address_of_m_value_17();
		float L_24 = L_23->get_x_0();
		NullCheck(L_22);
		((BaseInput_2_t1381185473 *)L_22)->set_value_1(L_24);
		// yAxis.value = m_value.y;
		AxisInput_t802457650 * L_25 = __this->get_yAxis_3();
		Vector2_t2156229523 * L_26 = __this->get_address_of_m_value_17();
		float L_27 = L_26->get_y_1();
		NullCheck(L_25);
		((BaseInput_2_t1381185473 *)L_25)->set_value_1(L_27);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Joystick_OnPointerUp_m1184069125 (Joystick_t1225167045 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_OnPointerUp_m1184069125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// joystickHeld = false;
		__this->set_joystickHeld_12((bool)0);
		// thumbTR.localPosition = Vector3.zero;
		RectTransform_t3704657025 * L_0 = __this->get_thumbTR_7();
		// thumbTR.localPosition = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		// thumbTR.localPosition = Vector3.zero;
		NullCheck(L_0);
		Transform_set_localPosition_m4128471975(L_0, L_1, /*hidden argument*/NULL);
		// m_value = Vector2.zero;
		// m_value = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_value_17(L_2);
		// xAxis.value = 0f;
		AxisInput_t802457650 * L_3 = __this->get_xAxis_2();
		NullCheck(L_3);
		((BaseInput_2_t1381185473 *)L_3)->set_value_1((0.0f));
		// yAxis.value = 0f;
		AxisInput_t802457650 * L_4 = __this->get_yAxis_3();
		NullCheck(L_4);
		((BaseInput_2_t1381185473 *)L_4)->set_value_1((0.0f));
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Joystick::OnUpdate()
extern "C"  void Joystick_OnUpdate_m3745511237 (Joystick_t1225167045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_OnUpdate_m3745511237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// if( !isDynamicJoystick )
		bool L_0 = __this->get_isDynamicJoystick_10();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		goto IL_00c2;
	}

IL_0011:
	{
		// if( joystickHeld )
		bool L_1 = __this->get_joystickHeld_12();
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		// opacity = Mathf.Min( 1f, opacity + Time.deltaTime * 4f );
		float L_2 = __this->get_opacity_16();
		// opacity = Mathf.Min( 1f, opacity + Time.deltaTime * 4f );
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		// opacity = Mathf.Min( 1f, opacity + Time.deltaTime * 4f );
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Min_m1073399594(NULL /*static, unused*/, (1.0f), ((float)il2cpp_codegen_add((float)L_2, (float)((float)il2cpp_codegen_multiply((float)L_3, (float)(4.0f))))), /*hidden argument*/NULL);
		__this->set_opacity_16(L_4);
		goto IL_0065;
	}

IL_0043:
	{
		// opacity = Mathf.Max( 0f, opacity - Time.deltaTime * 4f );
		float L_5 = __this->get_opacity_16();
		// opacity = Mathf.Max( 0f, opacity - Time.deltaTime * 4f );
		float L_6 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		// opacity = Mathf.Max( 0f, opacity - Time.deltaTime * 4f );
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Max_m3146388979(NULL /*static, unused*/, (0.0f), ((float)il2cpp_codegen_subtract((float)L_5, (float)((float)il2cpp_codegen_multiply((float)L_6, (float)(4.0f))))), /*hidden argument*/NULL);
		__this->set_opacity_16(L_7);
	}

IL_0065:
	{
		// Color c = thumb.color;
		Image_t2670269651 * L_8 = __this->get_thumb_6();
		// Color c = thumb.color;
		NullCheck(L_8);
		Color_t2555686324  L_9 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_8);
		V_0 = L_9;
		// c.a = opacity;
		float L_10 = __this->get_opacity_16();
		(&V_0)->set_a_3(L_10);
		// thumb.color = c;
		Image_t2670269651 * L_11 = __this->get_thumb_6();
		Color_t2555686324  L_12 = V_0;
		// thumb.color = c;
		NullCheck(L_11);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_12);
		// if( background != null )
		Image_t2670269651 * L_13 = __this->get_background_5();
		// if( background != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c2;
		}
	}
	{
		// c = background.color;
		Image_t2670269651 * L_15 = __this->get_background_5();
		// c = background.color;
		NullCheck(L_15);
		Color_t2555686324  L_16 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_15);
		V_0 = L_16;
		// c.a = opacity;
		float L_17 = __this->get_opacity_16();
		(&V_0)->set_a_3(L_17);
		// background.color = c;
		Image_t2670269651 * L_18 = __this->get_background_5();
		Color_t2555686324  L_19 = V_0;
		// background.color = c;
		NullCheck(L_18);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_18, L_19);
	}

IL_00c2:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.KeyInputKeyboard::.ctor()
extern "C"  void KeyInputKeyboard__ctor_m1037973947 (KeyInputKeyboard_t2444570555 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputKeyboard__ctor_m1037973947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.KeyInput key = new SimpleInput.KeyInput();
		KeyInput_t3393349320 * L_0 = (KeyInput_t3393349320 *)il2cpp_codegen_object_new(KeyInput_t3393349320_il2cpp_TypeInfo_var);
		KeyInput__ctor_m3791521000(L_0, /*hidden argument*/NULL);
		__this->set_key_3(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputKeyboard::OnEnable()
extern "C"  void KeyInputKeyboard_OnEnable_m2127914007 (KeyInputKeyboard_t2444570555 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputKeyboard_OnEnable_m2127914007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// key.StartTracking();
		KeyInput_t3393349320 * L_0 = __this->get_key_3();
		// key.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m1519213419(L_0, /*hidden argument*/BaseInput_2_StartTracking_m1519213419_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_1 = (intptr_t)KeyInputKeyboard_OnUpdate_m1091070468_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputKeyboard::OnDisable()
extern "C"  void KeyInputKeyboard_OnDisable_m996401503 (KeyInputKeyboard_t2444570555 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputKeyboard_OnDisable_m996401503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// key.StopTracking();
		KeyInput_t3393349320 * L_0 = __this->get_key_3();
		// key.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m3686500270(L_0, /*hidden argument*/BaseInput_2_StopTracking_m3686500270_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_1 = (intptr_t)KeyInputKeyboard_OnUpdate_m1091070468_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputKeyboard::OnUpdate()
extern "C"  void KeyInputKeyboard_OnUpdate_m1091070468 (KeyInputKeyboard_t2444570555 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputKeyboard_OnUpdate_m1091070468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// key.value = Input.GetKey( realKey );
		KeyInput_t3393349320 * L_0 = __this->get_key_3();
		int32_t L_1 = __this->get_realKey_2();
		// key.value = Input.GetKey( realKey );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		((BaseInput_2_t1022651380 *)L_0)->set_value_1(L_2);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.KeyInputUI::.ctor()
extern "C"  void KeyInputUI__ctor_m4057530322 (KeyInputUI_t1141642093 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputUI__ctor_m4057530322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.KeyInput key = new SimpleInput.KeyInput();
		KeyInput_t3393349320 * L_0 = (KeyInput_t3393349320 *)il2cpp_codegen_object_new(KeyInput_t3393349320_il2cpp_TypeInfo_var);
		KeyInput__ctor_m3791521000(L_0, /*hidden argument*/NULL);
		__this->set_key_2(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputUI::Awake()
extern "C"  void KeyInputUI_Awake_m174052282 (KeyInputUI_t1141642093 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputUI_Awake_m174052282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	{
		// Graphic graphic = GetComponent<Graphic>();
		// Graphic graphic = GetComponent<Graphic>();
		Graphic_t1660335611 * L_0 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		V_0 = L_0;
		// if( graphic != null )
		Graphic_t1660335611 * L_1 = V_0;
		// if( graphic != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// graphic.raycastTarget = true;
		Graphic_t1660335611 * L_3 = V_0;
		// graphic.raycastTarget = true;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_3, (bool)1);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputUI::OnEnable()
extern "C"  void KeyInputUI_OnEnable_m2983933856 (KeyInputUI_t1141642093 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputUI_OnEnable_m2983933856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// key.StartTracking();
		KeyInput_t3393349320 * L_0 = __this->get_key_2();
		// key.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m1519213419(L_0, /*hidden argument*/BaseInput_2_StartTracking_m1519213419_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputUI::OnDisable()
extern "C"  void KeyInputUI_OnDisable_m3849322508 (KeyInputUI_t1141642093 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyInputUI_OnDisable_m3849322508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// key.StopTracking();
		KeyInput_t3393349320 * L_0 = __this->get_key_2();
		// key.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m3686500270(L_0, /*hidden argument*/BaseInput_2_StopTracking_m3686500270_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputUI::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void KeyInputUI_OnPointerDown_m2367402574 (KeyInputUI_t1141642093 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// key.value = true;
		KeyInput_t3393349320 * L_0 = __this->get_key_2();
		NullCheck(L_0);
		((BaseInput_2_t1022651380 *)L_0)->set_value_1((bool)1);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.KeyInputUI::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void KeyInputUI_OnPointerUp_m4084381151 (KeyInputUI_t1141642093 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// key.value = false;
		KeyInput_t3393349320 * L_0 = __this->get_key_2();
		NullCheck(L_0);
		((BaseInput_2_t1022651380 *)L_0)->set_value_1((bool)0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.MouseButtonInputKeyboard::.ctor()
extern "C"  void MouseButtonInputKeyboard__ctor_m2031118237 (MouseButtonInputKeyboard_t4221569981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputKeyboard__ctor_m2031118237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.MouseButtonInput mouseButton = new SimpleInput.MouseButtonInput();
		MouseButtonInput_t2868216434 * L_0 = (MouseButtonInput_t2868216434 *)il2cpp_codegen_object_new(MouseButtonInput_t2868216434_il2cpp_TypeInfo_var);
		MouseButtonInput__ctor_m247368948(L_0, /*hidden argument*/NULL);
		__this->set_mouseButton_3(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputKeyboard::OnEnable()
extern "C"  void MouseButtonInputKeyboard_OnEnable_m4117763566 (MouseButtonInputKeyboard_t4221569981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputKeyboard_OnEnable_m4117763566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mouseButton.StartTracking();
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_3();
		// mouseButton.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m985837305(L_0, /*hidden argument*/BaseInput_2_StartTracking_m985837305_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_1 = (intptr_t)MouseButtonInputKeyboard_OnUpdate_m988355275_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputKeyboard::OnDisable()
extern "C"  void MouseButtonInputKeyboard_OnDisable_m451127520 (MouseButtonInputKeyboard_t4221569981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputKeyboard_OnDisable_m451127520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mouseButton.StopTracking();
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_3();
		// mouseButton.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m3193694964(L_0, /*hidden argument*/BaseInput_2_StopTracking_m3193694964_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_1 = (intptr_t)MouseButtonInputKeyboard_OnUpdate_m988355275_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputKeyboard::OnUpdate()
extern "C"  void MouseButtonInputKeyboard_OnUpdate_m988355275 (MouseButtonInputKeyboard_t4221569981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputKeyboard_OnUpdate_m988355275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mouseButton.value = Input.GetKey( key );
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_3();
		int32_t L_1 = __this->get_key_2();
		// mouseButton.value = Input.GetKey( key );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		((BaseInput_2_t3479630992 *)L_0)->set_value_1(L_2);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.MouseButtonInputUI::.ctor()
extern "C"  void MouseButtonInputUI__ctor_m546969997 (MouseButtonInputUI_t1438106573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputUI__ctor_m546969997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.MouseButtonInput mouseButton = new SimpleInput.MouseButtonInput();
		MouseButtonInput_t2868216434 * L_0 = (MouseButtonInput_t2868216434 *)il2cpp_codegen_object_new(MouseButtonInput_t2868216434_il2cpp_TypeInfo_var);
		MouseButtonInput__ctor_m247368948(L_0, /*hidden argument*/NULL);
		__this->set_mouseButton_2(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputUI::Awake()
extern "C"  void MouseButtonInputUI_Awake_m464842101 (MouseButtonInputUI_t1438106573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputUI_Awake_m464842101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	{
		// Graphic graphic = GetComponent<Graphic>();
		// Graphic graphic = GetComponent<Graphic>();
		Graphic_t1660335611 * L_0 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		V_0 = L_0;
		// if( graphic != null )
		Graphic_t1660335611 * L_1 = V_0;
		// if( graphic != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// graphic.raycastTarget = true;
		Graphic_t1660335611 * L_3 = V_0;
		// graphic.raycastTarget = true;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_3, (bool)1);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputUI::OnEnable()
extern "C"  void MouseButtonInputUI_OnEnable_m400322805 (MouseButtonInputUI_t1438106573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputUI_OnEnable_m400322805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mouseButton.StartTracking();
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_2();
		// mouseButton.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m985837305(L_0, /*hidden argument*/BaseInput_2_StartTracking_m985837305_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputUI::OnDisable()
extern "C"  void MouseButtonInputUI_OnDisable_m2301120173 (MouseButtonInputUI_t1438106573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseButtonInputUI_OnDisable_m2301120173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mouseButton.StopTracking();
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_2();
		// mouseButton.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m3193694964(L_0, /*hidden argument*/BaseInput_2_StopTracking_m3193694964_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputUI::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MouseButtonInputUI_OnPointerDown_m1187747589 (MouseButtonInputUI_t1438106573 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// mouseButton.value = true;
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_2();
		NullCheck(L_0);
		((BaseInput_2_t3479630992 *)L_0)->set_value_1((bool)1);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.MouseButtonInputUI::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MouseButtonInputUI_OnPointerUp_m1240294046 (MouseButtonInputUI_t1438106573 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// mouseButton.value = false;
		MouseButtonInput_t2868216434 * L_0 = __this->get_mouseButton_2();
		NullCheck(L_0);
		((BaseInput_2_t3479630992 *)L_0)->set_value_1((bool)0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.SimpleInputDragListener::.ctor()
extern "C"  void SimpleInputDragListener__ctor_m1366940289 (SimpleInputDragListener_t3058349199 * __this, const RuntimeMethod* method)
{
	{
		// private int pointerId = -99;
		__this->set_pointerId_3(((int32_t)-99));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleInputNamespace.ISimpleInputDraggable SimpleInputNamespace.SimpleInputDragListener::get_Listener()
extern "C"  RuntimeObject* SimpleInputDragListener_get_Listener_m1909111161 (SimpleInputDragListener_t3058349199 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public ISimpleInputDraggable Listener { get; set; }
		RuntimeObject* L_0 = __this->get_U3CListenerU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void SimpleInputNamespace.SimpleInputDragListener::set_Listener(SimpleInputNamespace.ISimpleInputDraggable)
extern "C"  void SimpleInputDragListener_set_Listener_m3326121527 (SimpleInputDragListener_t3058349199 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public ISimpleInputDraggable Listener { get; set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CListenerU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void SimpleInputNamespace.SimpleInputDragListener::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleInputDragListener_OnPointerDown_m625974936 (SimpleInputDragListener_t3058349199 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInputDragListener_OnPointerDown_m625974936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( pointerId != -99 )
		int32_t L_0 = __this->get_pointerId_3();
		if ((((int32_t)L_0) == ((int32_t)((int32_t)-99))))
		{
			goto IL_0013;
		}
	}
	{
		// return;
		goto IL_002b;
	}

IL_0013:
	{
		// Listener.OnPointerDown( eventData );
		// Listener.OnPointerDown( eventData );
		RuntimeObject* L_1 = SimpleInputDragListener_get_Listener_m1909111161(__this, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_2 = ___eventData0;
		// Listener.OnPointerDown( eventData );
		NullCheck(L_1);
		InterfaceActionInvoker1< PointerEventData_t3807901092 * >::Invoke(0 /* System.Void SimpleInputNamespace.ISimpleInputDraggable::OnPointerDown(UnityEngine.EventSystems.PointerEventData) */, ISimpleInputDraggable_t3592242264_il2cpp_TypeInfo_var, L_1, L_2);
		// pointerId = eventData.pointerId;
		PointerEventData_t3807901092 * L_3 = ___eventData0;
		// pointerId = eventData.pointerId;
		NullCheck(L_3);
		int32_t L_4 = PointerEventData_get_pointerId_m1200356155(L_3, /*hidden argument*/NULL);
		__this->set_pointerId_3(L_4);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SimpleInputDragListener::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleInputDragListener_OnDrag_m505829966 (SimpleInputDragListener_t3058349199 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInputDragListener_OnDrag_m505829966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( pointerId != eventData.pointerId )
		int32_t L_0 = __this->get_pointerId_3();
		PointerEventData_t3807901092 * L_1 = ___eventData0;
		// if( pointerId != eventData.pointerId )
		NullCheck(L_1);
		int32_t L_2 = PointerEventData_get_pointerId_m1200356155(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001f;
		}
	}
	{
		// eventData.pointerDrag = null;
		PointerEventData_t3807901092 * L_3 = ___eventData0;
		// eventData.pointerDrag = null;
		NullCheck(L_3);
		PointerEventData_set_pointerDrag_m841976018(L_3, (GameObject_t1113636619 *)NULL, /*hidden argument*/NULL);
		// return;
		goto IL_002b;
	}

IL_001f:
	{
		// Listener.OnDrag( eventData );
		// Listener.OnDrag( eventData );
		RuntimeObject* L_4 = SimpleInputDragListener_get_Listener_m1909111161(__this, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_5 = ___eventData0;
		// Listener.OnDrag( eventData );
		NullCheck(L_4);
		InterfaceActionInvoker1< PointerEventData_t3807901092 * >::Invoke(1 /* System.Void SimpleInputNamespace.ISimpleInputDraggable::OnDrag(UnityEngine.EventSystems.PointerEventData) */, ISimpleInputDraggable_t3592242264_il2cpp_TypeInfo_var, L_4, L_5);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SimpleInputDragListener::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleInputDragListener_OnPointerUp_m3725929757 (SimpleInputDragListener_t3058349199 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleInputDragListener_OnPointerUp_m3725929757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( pointerId != eventData.pointerId )
		int32_t L_0 = __this->get_pointerId_3();
		PointerEventData_t3807901092 * L_1 = ___eventData0;
		// if( pointerId != eventData.pointerId )
		NullCheck(L_1);
		int32_t L_2 = PointerEventData_get_pointerId_m1200356155(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0017;
		}
	}
	{
		// return;
		goto IL_002b;
	}

IL_0017:
	{
		// Listener.OnPointerUp( eventData );
		// Listener.OnPointerUp( eventData );
		RuntimeObject* L_3 = SimpleInputDragListener_get_Listener_m1909111161(__this, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_4 = ___eventData0;
		// Listener.OnPointerUp( eventData );
		NullCheck(L_3);
		InterfaceActionInvoker1< PointerEventData_t3807901092 * >::Invoke(2 /* System.Void SimpleInputNamespace.ISimpleInputDraggable::OnPointerUp(UnityEngine.EventSystems.PointerEventData) */, ISimpleInputDraggable_t3592242264_il2cpp_TypeInfo_var, L_3, L_4);
		// pointerId = -99;
		__this->set_pointerId_3(((int32_t)-99));
	}

IL_002b:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.SteeringWheel::.ctor()
extern "C"  void SteeringWheel__ctor_m3581698376 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel__ctor_m3581698376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput axis = new SimpleInput.AxisInput( "Horizontal" );
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_0, _stringLiteral1828639942, /*hidden argument*/NULL);
		__this->set_axis_2(L_0);
		// public float maximumSteeringAngle = 200f;
		__this->set_maximumSteeringAngle_6((200.0f));
		// public float wheelReleasedSpeed = 350f;
		__this->set_wheelReleasedSpeed_7((350.0f));
		// private float wheelAngle = 0f;
		__this->set_wheelAngle_8((0.0f));
		// private float wheelPrevAngle = 0f;
		__this->set_wheelPrevAngle_9((0.0f));
		// private bool wheelBeingHeld = false;
		__this->set_wheelBeingHeld_10((bool)0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single SimpleInputNamespace.SteeringWheel::get_Value()
extern "C"  float SteeringWheel_get_Value_m1547502318 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// public float Value { get { return wheelAngle / maximumSteeringAngle; } }
		float L_0 = __this->get_wheelAngle_8();
		float L_1 = __this->get_maximumSteeringAngle_6();
		V_0 = ((float)((float)L_0/(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		// public float Value { get { return wheelAngle / maximumSteeringAngle; } }
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single SimpleInputNamespace.SteeringWheel::get_Angle()
extern "C"  float SteeringWheel_get_Angle_m46809554 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// public float Angle { get { return wheelAngle; } }
		float L_0 = __this->get_wheelAngle_8();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// public float Angle { get { return wheelAngle; } }
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::Awake()
extern "C"  void SteeringWheel_Awake_m1459047363 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel_Awake_m1459047363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SimpleInputDragListener_t3058349199 * V_0 = NULL;
	{
		// wheel = GetComponent<Graphic>();
		// wheel = GetComponent<Graphic>();
		Graphic_t1660335611 * L_0 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		__this->set_wheel_3(L_0);
		// wheelTR = wheel.rectTransform;
		Graphic_t1660335611 * L_1 = __this->get_wheel_3();
		// wheelTR = wheel.rectTransform;
		NullCheck(L_1);
		RectTransform_t3704657025 * L_2 = Graphic_get_rectTransform_m1167152468(L_1, /*hidden argument*/NULL);
		__this->set_wheelTR_4(L_2);
		// wheel.raycastTarget = true;
		Graphic_t1660335611 * L_3 = __this->get_wheel_3();
		// wheel.raycastTarget = true;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_3, (bool)1);
		// SimpleInputDragListener eventReceiver = gameObject.AddComponent<SimpleInputDragListener>();
		// SimpleInputDragListener eventReceiver = gameObject.AddComponent<SimpleInputDragListener>();
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		// SimpleInputDragListener eventReceiver = gameObject.AddComponent<SimpleInputDragListener>();
		NullCheck(L_4);
		SimpleInputDragListener_t3058349199 * L_5 = GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994(L_4, /*hidden argument*/GameObject_AddComponent_TisSimpleInputDragListener_t3058349199_m2113259994_RuntimeMethod_var);
		V_0 = L_5;
		// eventReceiver.Listener = this;
		SimpleInputDragListener_t3058349199 * L_6 = V_0;
		// eventReceiver.Listener = this;
		NullCheck(L_6);
		SimpleInputDragListener_set_Listener_m3326121527(L_6, __this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::OnEnable()
extern "C"  void SteeringWheel_OnEnable_m1912695944 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel_OnEnable_m1912695944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// axis.StartTracking();
		AxisInput_t802457650 * L_0 = __this->get_axis_2();
		// axis.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3188229854(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_1 = (intptr_t)SteeringWheel_OnUpdate_m2737091337_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::OnDisable()
extern "C"  void SteeringWheel_OnDisable_m1546151817 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel_OnDisable_m1546151817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// axis.StopTracking();
		AxisInput_t802457650 * L_0 = __this->get_axis_2();
		// axis.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m2000325864(L_0, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_1 = (intptr_t)SteeringWheel_OnUpdate_m2737091337_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_2 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_2, __this, L_1, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::OnUpdate()
extern "C"  void SteeringWheel_OnUpdate_m2737091337 (SteeringWheel_t1692308694 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel_OnUpdate_m2737091337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if( !wheelBeingHeld && wheelAngle != 0f )
		bool L_0 = __this->get_wheelBeingHeld_10();
		if (L_0)
		{
			goto IL_0082;
		}
	}
	{
		float L_1 = __this->get_wheelAngle_8();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0082;
		}
	}
	{
		// float deltaAngle = wheelReleasedSpeed * Time.deltaTime;
		float L_2 = __this->get_wheelReleasedSpeed_7();
		// float deltaAngle = wheelReleasedSpeed * Time.deltaTime;
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_multiply((float)L_2, (float)L_3));
		// if( Mathf.Abs( deltaAngle ) > Mathf.Abs( wheelAngle ) )
		float L_4 = V_0;
		// if( Mathf.Abs( deltaAngle ) > Mathf.Abs( wheelAngle ) )
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_5 = fabsf(L_4);
		float L_6 = __this->get_wheelAngle_8();
		// if( Mathf.Abs( deltaAngle ) > Mathf.Abs( wheelAngle ) )
		float L_7 = fabsf(L_6);
		if ((!(((float)L_5) > ((float)L_7))))
		{
			goto IL_0050;
		}
	}
	{
		// wheelAngle = 0f;
		__this->set_wheelAngle_8((0.0f));
		goto IL_0081;
	}

IL_0050:
	{
		// else if( wheelAngle > 0f )
		float L_8 = __this->get_wheelAngle_8();
		if ((!(((float)L_8) > ((float)(0.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		// wheelAngle -= deltaAngle;
		float L_9 = __this->get_wheelAngle_8();
		float L_10 = V_0;
		__this->set_wheelAngle_8(((float)il2cpp_codegen_subtract((float)L_9, (float)L_10)));
		goto IL_0081;
	}

IL_0073:
	{
		// wheelAngle += deltaAngle;
		float L_11 = __this->get_wheelAngle_8();
		float L_12 = V_0;
		__this->set_wheelAngle_8(((float)il2cpp_codegen_add((float)L_11, (float)L_12)));
	}

IL_0081:
	{
	}

IL_0082:
	{
		// wheelTR.localEulerAngles = new Vector3( 0f, 0f, -wheelAngle );
		RectTransform_t3704657025 * L_13 = __this->get_wheelTR_4();
		float L_14 = __this->get_wheelAngle_8();
		// wheelTR.localEulerAngles = new Vector3( 0f, 0f, -wheelAngle );
		Vector3_t3722313464  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector3__ctor_m3353183577((&L_15), (0.0f), (0.0f), ((-L_14)), /*hidden argument*/NULL);
		// wheelTR.localEulerAngles = new Vector3( 0f, 0f, -wheelAngle );
		NullCheck(L_13);
		Transform_set_localEulerAngles_m4202601546(L_13, L_15, /*hidden argument*/NULL);
		// axis.value = Value;
		AxisInput_t802457650 * L_16 = __this->get_axis_2();
		// axis.value = Value;
		float L_17 = SteeringWheel_get_Value_m1547502318(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		((BaseInput_2_t1381185473 *)L_16)->set_value_1(L_17);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SteeringWheel_OnPointerDown_m2240246608 (SteeringWheel_t1692308694 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel_OnPointerDown_m2240246608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// wheelBeingHeld = true;
		__this->set_wheelBeingHeld_10((bool)1);
		// centerPoint = wheelTR.position;
		RectTransform_t3704657025 * L_0 = __this->get_wheelTR_4();
		// centerPoint = wheelTR.position;
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		// centerPoint = wheelTR.position;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_centerPoint_5(L_2);
		// wheelPrevAngle = Vector2.Angle( Vector2.up, eventData.position - centerPoint );
		// wheelPrevAngle = Vector2.Angle( Vector2.up, eventData.position - centerPoint );
		Vector2_t2156229523  L_3 = Vector2_get_up_m2647420593(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_4 = ___eventData0;
		// wheelPrevAngle = Vector2.Angle( Vector2.up, eventData.position - centerPoint );
		NullCheck(L_4);
		Vector2_t2156229523  L_5 = PointerEventData_get_position_m437660275(L_4, /*hidden argument*/NULL);
		Vector2_t2156229523  L_6 = __this->get_centerPoint_5();
		// wheelPrevAngle = Vector2.Angle( Vector2.up, eventData.position - centerPoint );
		Vector2_t2156229523  L_7 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		// wheelPrevAngle = Vector2.Angle( Vector2.up, eventData.position - centerPoint );
		float L_8 = Vector2_Angle_m4105581454(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		__this->set_wheelPrevAngle_9(L_8);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SteeringWheel_OnDrag_m1834180620 (SteeringWheel_t1692308694 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SteeringWheel_OnDrag_m1834180620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// Vector2 pointerPos = eventData.position;
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		// Vector2 pointerPos = eventData.position;
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = PointerEventData_get_position_m437660275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// float wheelNewAngle = Vector2.Angle( Vector2.up, pointerPos - centerPoint );
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_up_m2647420593(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = V_0;
		Vector2_t2156229523  L_4 = __this->get_centerPoint_5();
		// float wheelNewAngle = Vector2.Angle( Vector2.up, pointerPos - centerPoint );
		Vector2_t2156229523  L_5 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		// float wheelNewAngle = Vector2.Angle( Vector2.up, pointerPos - centerPoint );
		float L_6 = Vector2_Angle_m4105581454(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// if( Vector2.Distance( pointerPos, centerPoint ) > 20f )
		Vector2_t2156229523  L_7 = V_0;
		Vector2_t2156229523  L_8 = __this->get_centerPoint_5();
		// if( Vector2.Distance( pointerPos, centerPoint ) > 20f )
		float L_9 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) > ((float)(20.0f)))))
		{
			goto IL_007d;
		}
	}
	{
		// if( pointerPos.x > centerPoint.x )
		float L_10 = (&V_0)->get_x_0();
		Vector2_t2156229523 * L_11 = __this->get_address_of_centerPoint_5();
		float L_12 = L_11->get_x_0();
		if ((!(((float)L_10) > ((float)L_12))))
		{
			goto IL_0067;
		}
	}
	{
		// wheelAngle += wheelNewAngle - wheelPrevAngle;
		float L_13 = __this->get_wheelAngle_8();
		float L_14 = V_1;
		float L_15 = __this->get_wheelPrevAngle_9();
		__this->set_wheelAngle_8(((float)il2cpp_codegen_add((float)L_13, (float)((float)il2cpp_codegen_subtract((float)L_14, (float)L_15)))));
		goto IL_007c;
	}

IL_0067:
	{
		// wheelAngle -= wheelNewAngle - wheelPrevAngle;
		float L_16 = __this->get_wheelAngle_8();
		float L_17 = V_1;
		float L_18 = __this->get_wheelPrevAngle_9();
		__this->set_wheelAngle_8(((float)il2cpp_codegen_subtract((float)L_16, (float)((float)il2cpp_codegen_subtract((float)L_17, (float)L_18)))));
	}

IL_007c:
	{
	}

IL_007d:
	{
		// wheelAngle = Mathf.Clamp( wheelAngle, -maximumSteeringAngle, maximumSteeringAngle );
		float L_19 = __this->get_wheelAngle_8();
		float L_20 = __this->get_maximumSteeringAngle_6();
		float L_21 = __this->get_maximumSteeringAngle_6();
		// wheelAngle = Mathf.Clamp( wheelAngle, -maximumSteeringAngle, maximumSteeringAngle );
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_19, ((-L_20)), L_21, /*hidden argument*/NULL);
		__this->set_wheelAngle_8(L_22);
		// wheelPrevAngle = wheelNewAngle;
		float L_23 = V_1;
		__this->set_wheelPrevAngle_9(L_23);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.SteeringWheel::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SteeringWheel_OnPointerUp_m3754624503 (SteeringWheel_t1692308694 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// OnDrag( eventData );
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		// OnDrag( eventData );
		SteeringWheel_OnDrag_m1834180620(__this, L_0, /*hidden argument*/NULL);
		// wheelBeingHeld = false;
		__this->set_wheelBeingHeld_10((bool)0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleInputNamespace.Touchpad::.ctor()
extern "C"  void Touchpad__ctor_m243273444 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad__ctor_m243273444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public SimpleInput.AxisInput xAxis = new SimpleInput.AxisInput( "Mouse X" );
		AxisInput_t802457650 * L_0 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_0, _stringLiteral3403559637, /*hidden argument*/NULL);
		__this->set_xAxis_2(L_0);
		// public SimpleInput.AxisInput yAxis = new SimpleInput.AxisInput( "Mouse Y" );
		AxisInput_t802457650 * L_1 = (AxisInput_t802457650 *)il2cpp_codegen_object_new(AxisInput_t802457650_il2cpp_TypeInfo_var);
		AxisInput__ctor_m635020897(L_1, _stringLiteral674676282, /*hidden argument*/NULL);
		__this->set_yAxis_3(L_1);
		// public float sensitivity = 1f;
		__this->set_sensitivity_5((1.0f));
		// public bool allowTouchInput = true;
		__this->set_allowTouchInput_6((bool)1);
		// public bool ignoreUIElements = false;
		__this->set_ignoreUIElements_8((bool)0);
		// private int fingerId = -1;
		__this->set_fingerId_10((-1));
		// private bool trackMouseInput = false;
		__this->set_trackMouseInput_12((bool)0);
		// private Vector2 m_value = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_value_13(L_2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 SimpleInputNamespace.Touchpad::get_Value()
extern "C"  Vector2_t2156229523  Touchpad_get_Value_m596491985 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public Vector2 Value { get { return m_value; } }
		Vector2_t2156229523  L_0 = __this->get_m_value_13();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// public Vector2 Value { get { return m_value; } }
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
// System.Void SimpleInputNamespace.Touchpad::Awake()
extern "C"  void Touchpad_Awake_m3382882084 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_Awake_m3382882084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	{
		// rectTransform = transform as RectTransform;
		// rectTransform = transform as RectTransform;
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_rectTransform_4(((RectTransform_t3704657025 *)IsInstSealed((RuntimeObject*)L_0, RectTransform_t3704657025_il2cpp_TypeInfo_var)));
		// resolutionMultiplier = 100f / ( Screen.width + Screen.height );
		// resolutionMultiplier = 100f / ( Screen.width + Screen.height );
		int32_t L_1 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		// resolutionMultiplier = 100f / ( Screen.width + Screen.height );
		int32_t L_2 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_resolutionMultiplier_9(((float)((float)(100.0f)/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_2))))))));
		// Graphic graphic = GetComponent<Graphic>();
		// Graphic graphic = GetComponent<Graphic>();
		Graphic_t1660335611 * L_3 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		V_0 = L_3;
		// if( graphic != null )
		Graphic_t1660335611 * L_4 = V_0;
		// if( graphic != null )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		// graphic.raycastTarget = false;
		Graphic_t1660335611 * L_6 = V_0;
		// graphic.raycastTarget = false;
		NullCheck(L_6);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_6, (bool)0);
	}

IL_0044:
	{
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Touchpad::OnEnable()
extern "C"  void Touchpad_OnEnable_m4042567201 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_OnEnable_m4042567201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// xAxis.StartTracking();
		AxisInput_t802457650 * L_0 = __this->get_xAxis_2();
		// xAxis.StartTracking();
		NullCheck(L_0);
		BaseInput_2_StartTracking_m3188229854(L_0, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// yAxis.StartTracking();
		AxisInput_t802457650 * L_1 = __this->get_yAxis_3();
		// yAxis.StartTracking();
		NullCheck(L_1);
		BaseInput_2_StartTracking_m3188229854(L_1, /*hidden argument*/BaseInput_2_StartTracking_m3188229854_RuntimeMethod_var);
		// SimpleInput.OnUpdate += OnUpdate;
		intptr_t L_2 = (intptr_t)Touchpad_OnUpdate_m3810730052_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_3 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_3, __this, L_2, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate += OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_add_OnUpdate_m521018632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Touchpad::OnDisable()
extern "C"  void Touchpad_OnDisable_m3422720687 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_OnDisable_m3422720687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// xAxis.StopTracking();
		AxisInput_t802457650 * L_0 = __this->get_xAxis_2();
		// xAxis.StopTracking();
		NullCheck(L_0);
		BaseInput_2_StopTracking_m2000325864(L_0, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// yAxis.StopTracking();
		AxisInput_t802457650 * L_1 = __this->get_yAxis_3();
		// yAxis.StopTracking();
		NullCheck(L_1);
		BaseInput_2_StopTracking_m2000325864(L_1, /*hidden argument*/BaseInput_2_StopTracking_m2000325864_RuntimeMethod_var);
		// SimpleInput.OnUpdate -= OnUpdate;
		intptr_t L_2 = (intptr_t)Touchpad_OnUpdate_m3810730052_RuntimeMethod_var;
		UpdateCallback_t3991193291 * L_3 = (UpdateCallback_t3991193291 *)il2cpp_codegen_object_new(UpdateCallback_t3991193291_il2cpp_TypeInfo_var);
		UpdateCallback__ctor_m3190082255(L_3, __this, L_2, /*hidden argument*/NULL);
		// SimpleInput.OnUpdate -= OnUpdate;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleInput_t4265260572_il2cpp_TypeInfo_var);
		SimpleInput_remove_OnUpdate_m4020446573(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleInputNamespace.Touchpad::OnUpdate()
extern "C"  void Touchpad_OnUpdate_m3810730052 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_OnUpdate_m3810730052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventSystem_t1003666588 * V_0 = NULL;
	int32_t V_1 = 0;
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		// EventSystem eventSystem = EventSystem.current;
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t1003666588_il2cpp_TypeInfo_var);
		EventSystem_t1003666588 * L_0 = EventSystem_get_current_m1416377559(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		// m_value.Set( 0f, 0f );
		Vector2_t2156229523 * L_1 = __this->get_address_of_m_value_13();
		// m_value.Set( 0f, 0f );
		Vector2_Set_m3780194483(L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		// if( allowTouchInput && Input.touchCount > 0 )
		bool L_2 = __this->get_allowTouchInput_6();
		if (!L_2)
		{
			goto IL_0137;
		}
	}
	{
		// if( allowTouchInput && Input.touchCount > 0 )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0137;
		}
	}
	{
		// for( int i = 0; i < Input.touchCount; i++ )
		V_1 = 0;
		goto IL_0126;
	}

IL_003a:
	{
		// Touch touch = Input.GetTouch( i );
		int32_t L_4 = V_1;
		// Touch touch = Input.GetTouch( i );
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_5 = Input_GetTouch_m2192712756(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		// if( fingerId == -1 )
		int32_t L_6 = __this->get_fingerId_10();
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_00c5;
		}
	}
	{
		// if( touch.phase == TouchPhase.Began &&
		// if( touch.phase == TouchPhase.Began &&
		int32_t L_7 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_00bf;
		}
	}
	{
		RectTransform_t3704657025 * L_8 = __this->get_rectTransform_4();
		// ( rectTransform == null || RectTransformUtility.RectangleContainsScreenPoint( rectTransform, touch.position ) ) &&
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0083;
		}
	}
	{
		RectTransform_t3704657025 * L_10 = __this->get_rectTransform_4();
		// ( rectTransform == null || RectTransformUtility.RectangleContainsScreenPoint( rectTransform, touch.position ) ) &&
		Vector2_t2156229523  L_11 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		// ( rectTransform == null || RectTransformUtility.RectangleContainsScreenPoint( rectTransform, touch.position ) ) &&
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		bool L_12 = RectTransformUtility_RectangleContainsScreenPoint_m3246909541(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00bf;
		}
	}

IL_0083:
	{
		bool L_13 = __this->get_ignoreUIElements_8();
		if (L_13)
		{
			goto IL_00ac;
		}
	}
	{
		EventSystem_t1003666588 * L_14 = V_0;
		// ( ignoreUIElements || eventSystem == null || !eventSystem.IsPointerOverGameObject( touch.fingerId ) ) )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_14, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00ac;
		}
	}
	{
		EventSystem_t1003666588 * L_16 = V_0;
		// ( ignoreUIElements || eventSystem == null || !eventSystem.IsPointerOverGameObject( touch.fingerId ) ) )
		int32_t L_17 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		// ( ignoreUIElements || eventSystem == null || !eventSystem.IsPointerOverGameObject( touch.fingerId ) ) )
		NullCheck(L_16);
		bool L_18 = EventSystem_IsPointerOverGameObject_m301566329(L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00bf;
		}
	}

IL_00ac:
	{
		// fingerId = touch.fingerId;
		// fingerId = touch.fingerId;
		int32_t L_19 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		__this->set_fingerId_10(L_19);
		// break;
		goto IL_0131;
	}

IL_00bf:
	{
		goto IL_0121;
	}

IL_00c5:
	{
		// else if( touch.fingerId == fingerId )
		// else if( touch.fingerId == fingerId )
		int32_t L_20 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		int32_t L_21 = __this->get_fingerId_10();
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			goto IL_0121;
		}
	}
	{
		// m_value = touch.deltaPosition * resolutionMultiplier * sensitivity;
		// m_value = touch.deltaPosition * resolutionMultiplier * sensitivity;
		Vector2_t2156229523  L_22 = Touch_get_deltaPosition_m2389653382((&V_2), /*hidden argument*/NULL);
		float L_23 = __this->get_resolutionMultiplier_9();
		// m_value = touch.deltaPosition * resolutionMultiplier * sensitivity;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_24 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		float L_25 = __this->get_sensitivity_5();
		// m_value = touch.deltaPosition * resolutionMultiplier * sensitivity;
		Vector2_t2156229523  L_26 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		__this->set_m_value_13(L_26);
		// if( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled )
		// if( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled )
		int32_t L_27 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)3)))
		{
			goto IL_0115;
		}
	}
	{
		// if( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled )
		int32_t L_28 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)4))))
		{
			goto IL_011c;
		}
	}

IL_0115:
	{
		// fingerId = -1;
		__this->set_fingerId_10((-1));
	}

IL_011c:
	{
		// break;
		goto IL_0131;
	}

IL_0121:
	{
		// for( int i = 0; i < Input.touchCount; i++ )
		int32_t L_29 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_0126:
	{
		// for( int i = 0; i < Input.touchCount; i++ )
		int32_t L_30 = V_1;
		// for( int i = 0; i < Input.touchCount; i++ )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_31 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_003a;
		}
	}

IL_0131:
	{
		goto IL_0214;
	}

IL_0137:
	{
		// if( GetMouseButtonDown() )
		// if( GetMouseButtonDown() )
		bool L_32 = Touchpad_GetMouseButtonDown_m102808409(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_01be;
		}
	}
	{
		// Vector2 mousePos = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_33 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Vector2 mousePos = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_34 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		V_3 = L_34;
		// if( ( rectTransform == null || RectTransformUtility.RectangleContainsScreenPoint( rectTransform, mousePos ) ) &&
		RectTransform_t3704657025 * L_35 = __this->get_rectTransform_4();
		// if( ( rectTransform == null || RectTransformUtility.RectangleContainsScreenPoint( rectTransform, mousePos ) ) &&
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_35, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_0171;
		}
	}
	{
		RectTransform_t3704657025 * L_37 = __this->get_rectTransform_4();
		Vector2_t2156229523  L_38 = V_3;
		// if( ( rectTransform == null || RectTransformUtility.RectangleContainsScreenPoint( rectTransform, mousePos ) ) &&
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		bool L_39 = RectTransformUtility_RectangleContainsScreenPoint_m3246909541(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_01b1;
		}
	}

IL_0171:
	{
		bool L_40 = __this->get_ignoreUIElements_8();
		if (L_40)
		{
			goto IL_0193;
		}
	}
	{
		EventSystem_t1003666588 * L_41 = V_0;
		// ( ignoreUIElements || eventSystem == null || !eventSystem.IsPointerOverGameObject() ) )
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_41, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_0193;
		}
	}
	{
		EventSystem_t1003666588 * L_43 = V_0;
		// ( ignoreUIElements || eventSystem == null || !eventSystem.IsPointerOverGameObject() ) )
		NullCheck(L_43);
		bool L_44 = EventSystem_IsPointerOverGameObject_m1911785875(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_01b1;
		}
	}

IL_0193:
	{
		// trackMouseInput = true;
		__this->set_trackMouseInput_12((bool)1);
		// prevMouseInputPos = Input.mousePosition;
		// prevMouseInputPos = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_45 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		// prevMouseInputPos = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_46 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		__this->set_prevMouseInputPos_11(L_46);
		goto IL_01b8;
	}

IL_01b1:
	{
		// trackMouseInput = false;
		__this->set_trackMouseInput_12((bool)0);
	}

IL_01b8:
	{
		goto IL_0213;
	}

IL_01be:
	{
		// else if( trackMouseInput && GetMouseButton() )
		bool L_47 = __this->get_trackMouseInput_12();
		if (!L_47)
		{
			goto IL_0213;
		}
	}
	{
		// else if( trackMouseInput && GetMouseButton() )
		bool L_48 = Touchpad_GetMouseButton_m3020692098(__this, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0213;
		}
	}
	{
		// Vector2 mousePos = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_49 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Vector2 mousePos = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_50 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		V_4 = L_50;
		// m_value = ( mousePos - prevMouseInputPos ) * resolutionMultiplier * sensitivity;
		Vector2_t2156229523  L_51 = V_4;
		Vector2_t2156229523  L_52 = __this->get_prevMouseInputPos_11();
		// m_value = ( mousePos - prevMouseInputPos ) * resolutionMultiplier * sensitivity;
		Vector2_t2156229523  L_53 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		float L_54 = __this->get_resolutionMultiplier_9();
		// m_value = ( mousePos - prevMouseInputPos ) * resolutionMultiplier * sensitivity;
		Vector2_t2156229523  L_55 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		float L_56 = __this->get_sensitivity_5();
		// m_value = ( mousePos - prevMouseInputPos ) * resolutionMultiplier * sensitivity;
		Vector2_t2156229523  L_57 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		__this->set_m_value_13(L_57);
		// prevMouseInputPos = mousePos;
		Vector2_t2156229523  L_58 = V_4;
		__this->set_prevMouseInputPos_11(L_58);
	}

IL_0213:
	{
	}

IL_0214:
	{
		// xAxis.value = m_value.x;
		AxisInput_t802457650 * L_59 = __this->get_xAxis_2();
		Vector2_t2156229523 * L_60 = __this->get_address_of_m_value_13();
		float L_61 = L_60->get_x_0();
		NullCheck(L_59);
		((BaseInput_2_t1381185473 *)L_59)->set_value_1(L_61);
		// yAxis.value = m_value.y;
		AxisInput_t802457650 * L_62 = __this->get_yAxis_3();
		Vector2_t2156229523 * L_63 = __this->get_address_of_m_value_13();
		float L_64 = L_63->get_y_1();
		NullCheck(L_62);
		((BaseInput_2_t1381185473 *)L_62)->set_value_1(L_64);
		// }
		return;
	}
}
// System.Boolean SimpleInputNamespace.Touchpad::GetMouseButtonDown()
extern "C"  bool Touchpad_GetMouseButtonDown_m102808409 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_GetMouseButtonDown_m102808409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		// for( int i = 0; i < allowedMouseButtons.Length; i++ )
		V_0 = 0;
		goto IL_0027;
	}

IL_0008:
	{
		// if( Input.GetMouseButtonDown( (int) allowedMouseButtons[i] ) )
		MouseButtonU5BU5D_t3311405085* L_0 = __this->get_allowedMouseButtons_7();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		// if( Input.GetMouseButtonDown( (int) allowedMouseButtons[i] ) )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		// return true;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0022:
	{
		// for( int i = 0; i < allowedMouseButtons.Length; i++ )
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0027:
	{
		// for( int i = 0; i < allowedMouseButtons.Length; i++ )
		int32_t L_6 = V_0;
		MouseButtonU5BU5D_t3311405085* L_7 = __this->get_allowedMouseButtons_7();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))))))
		{
			goto IL_0008;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		// }
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean SimpleInputNamespace.Touchpad::GetMouseButton()
extern "C"  bool Touchpad_GetMouseButton_m3020692098 (Touchpad_t3466294403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_GetMouseButton_m3020692098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		// for( int i = 0; i < allowedMouseButtons.Length; i++ )
		V_0 = 0;
		goto IL_0027;
	}

IL_0008:
	{
		// if( Input.GetMouseButtonDown( (int) allowedMouseButtons[i] ) )
		MouseButtonU5BU5D_t3311405085* L_0 = __this->get_allowedMouseButtons_7();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		// if( Input.GetMouseButtonDown( (int) allowedMouseButtons[i] ) )
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		// return true;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0022:
	{
		// for( int i = 0; i < allowedMouseButtons.Length; i++ )
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0027:
	{
		// for( int i = 0; i < allowedMouseButtons.Length; i++ )
		int32_t L_6 = V_0;
		MouseButtonU5BU5D_t3311405085* L_7 = __this->get_allowedMouseButtons_7();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))))))
		{
			goto IL_0008;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		// }
		bool L_8 = V_1;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
