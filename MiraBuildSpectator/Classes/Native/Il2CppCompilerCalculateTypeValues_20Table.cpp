﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3593973608;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t4071693616;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t96558379;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.List`1<Wikitude.UnityEditorSimulator/SimulatedTarget>
struct List_1_t3036245690;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// Wikitude.TrackerManager
struct TrackerManager_t2102804711;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t225505033;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// UnityEngine.Material
struct Material_t340375123;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2496920137;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t1930184704;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3766301798;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3558768157;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t4120149533;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t2463031060;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// Wikitude.CloudRecognitionService/OnInitializedEvent
struct OnInitializedEvent_t1242699385;
// Wikitude.CloudRecognitionService/OnInitializationErrorEvent
struct OnInitializationErrorEvent_t511199451;
// Wikitude.CloudRecognitionService/OnRecognitionResponseEvent
struct OnRecognitionResponseEvent_t3712700845;
// Wikitude.CloudRecognitionService/OnRecognitionErrorEvent
struct OnRecognitionErrorEvent_t2212464566;
// Wikitude.CloudRecognitionService/OnInterruptionEvent
struct OnInterruptionEvent_t1530367979;
// System.Collections.Generic.List`1<TMPro.TMP_Style>
struct List_1_t656488210;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style>
struct Dictionary_2_t2368094095;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// Wikitude.PluginManager/OnCameraFrameAvailableEvent
struct OnCameraFrameAvailableEvent_t3843633858;
// Wikitude.PluginManager/OnPluginFailureEvent
struct OnPluginFailureEvent_t2376488508;
// Wikitude.IPlatformBridge
struct IPlatformBridge_t1124789254;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// Wikitude.WikitudeCamera/OnInputPluginFailureEvent
struct OnInputPluginFailureEvent_t818001259;
// Wikitude.WikitudeCamera/OnCameraFailureEvent
struct OnCameraFailureEvent_t2345510068;
// Wikitude.WikitudeBridge
struct WikitudeBridge_t4110292378;
// Wikitude.TransformOverride
struct TransformOverride_t3828400655;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// Wikitude.TrackerBehaviour
struct TrackerBehaviour_t921360922;
// System.Collections.Generic.Dictionary`2<Wikitude.RecognizedTarget,UnityEngine.GameObject>
struct Dictionary_2_t2728509719;
// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.TargetCollectionResource>
struct Dictionary_2_t1128681183;
// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.CloudRecognitionService>
struct Dictionary_2_t634242530;
// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>
struct HashSet_1_t2094861142;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.HashSet`1<Wikitude.TrackerBehaviour>
struct HashSet_1_t3781277692;
// System.Collections.Generic.Dictionary`2<Wikitude.Trackable,System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>>
struct Dictionary_2_t1769459038;
// Wikitude.RecognizedTarget
struct RecognizedTarget_t3529911668;
// System.Collections.Generic.HashSet`1<Wikitude.TrackableBehaviour>
struct HashSet_1_t1034764117;
// UnityEngine.Camera
struct Camera_t4157153871;
// Wikitude.TrackerBehaviour/OnTargetsLoadedEvent
struct OnTargetsLoadedEvent_t1820866191;
// Wikitude.TrackerBehaviour/OnErrorLoadingTargetsEvent
struct OnErrorLoadingTargetsEvent_t394526247;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// Wikitude.TargetCollectionResource
struct TargetCollectionResource_t66041399;
// Wikitude.InstantTrackable/OnInitializationStartedEvent
struct OnInitializationStartedEvent_t272217273;
// Wikitude.InstantTrackable/OnInitializationStoppedEvent
struct OnInitializationStoppedEvent_t4207911327;
// Wikitude.InstantTrackable/OnSceneRecognizedEvent
struct OnSceneRecognizedEvent_t2416994635;
// Wikitude.InstantTrackable/OnSceneLostEvent
struct OnSceneLostEvent_t389101256;
// Wikitude.ImageTracker/OnExtendedTrackingQualityChangedEvent
struct OnExtendedTrackingQualityChangedEvent_t2560195974;
// Wikitude.CloudRecognitionService
struct CloudRecognitionService_t3866570042;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;
// Wikitude.ObjectTrackable/OnObjectRecognizedEvent
struct OnObjectRecognizedEvent_t3443511796;
// Wikitude.ObjectTrackable/OnObjectLostEvent
struct OnObjectLostEvent_t460465675;
// Wikitude.MapPointCloud
struct MapPointCloud_t955983077;
// WikitudeEditor.PointCloudRenderer
struct PointCloudRenderer_t3664016937;
// Wikitude.ImageTrackable/OnImageRecognizedEvent
struct OnImageRecognizedEvent_t773132985;
// Wikitude.ImageTrackable/OnImageLostEvent
struct OnImageLostEvent_t966623312;
// Wikitude.InstantTracker/OnStateChangedEvent
struct OnStateChangedEvent_t1359267737;
// Wikitude.InstantTracker/OnScreenConversionComputedEvent
struct OnScreenConversionComputedEvent_t608500589;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Transform
struct Transform_t3600365921;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t284240280;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TMP_Glyph
struct TMP_Glyph_t581847833;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef BRIDGEERROR_T1304078858_H
#define BRIDGEERROR_T1304078858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerManager/BridgeError
struct  BridgeError_t1304078858  : public RuntimeObject
{
public:
	// System.Int32 Wikitude.TrackerManager/BridgeError::<Code>k__BackingField
	int32_t ___U3CCodeU3Ek__BackingField_0;
	// System.String Wikitude.TrackerManager/BridgeError::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;
	// System.Int64 Wikitude.TrackerManager/BridgeError::<Identifier>k__BackingField
	int64_t ___U3CIdentifierU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BridgeError_t1304078858, ___U3CCodeU3Ek__BackingField_0)); }
	inline int32_t get_U3CCodeU3Ek__BackingField_0() const { return ___U3CCodeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCodeU3Ek__BackingField_0() { return &___U3CCodeU3Ek__BackingField_0; }
	inline void set_U3CCodeU3Ek__BackingField_0(int32_t value)
	{
		___U3CCodeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BridgeError_t1304078858, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BridgeError_t1304078858, ___U3CIdentifierU3Ek__BackingField_2)); }
	inline int64_t get_U3CIdentifierU3Ek__BackingField_2() const { return ___U3CIdentifierU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3CIdentifierU3Ek__BackingField_2() { return &___U3CIdentifierU3Ek__BackingField_2; }
	inline void set_U3CIdentifierU3Ek__BackingField_2(int64_t value)
	{
		___U3CIdentifierU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIDGEERROR_T1304078858_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef TMP_UPDATEREGISTRY_T461608481_H
#define TMP_UPDATEREGISTRY_T461608481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_t461608481  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_t3593973608 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_t3593973608 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t3593973608 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t3593973608 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t3593973608 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t3593973608 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t3593973608 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t3593973608 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_t461608481_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_t461608481 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_t461608481 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_t461608481 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_t461608481 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_T461608481_H
#ifndef TMP_TEXTUTILITIES_T2105690005_H
#define TMP_TEXTUTILITIES_T2105690005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t2105690005  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t2105690005_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_t1718750761* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t2105690005_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T2105690005_H
#ifndef TMP_UPDATEMANAGER_T4114267509_H
#define TMP_UPDATEMANAGER_T4114267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t4114267509  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t4071693616 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t4071693616 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t4071693616 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t4071693616 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t4071693616 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t4071693616 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t4071693616 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t4071693616 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateManager_t4114267509_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t4114267509 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t4114267509 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t4114267509 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t4114267509 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T4114267509_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CLOUDRECOGNITIONSERVICERESPONSE_T1770101589_H
#define CLOUDRECOGNITIONSERVICERESPONSE_T1770101589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionServiceResponse
struct  CloudRecognitionServiceResponse_t1770101589  : public RuntimeObject
{
public:
	// System.Boolean Wikitude.CloudRecognitionServiceResponse::<Recognized>k__BackingField
	bool ___U3CRecognizedU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Wikitude.CloudRecognitionServiceResponse::<Info>k__BackingField
	RuntimeObject* ___U3CInfoU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Wikitude.CloudRecognitionServiceResponse::<Metadata>k__BackingField
	RuntimeObject* ___U3CMetadataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRecognizedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CloudRecognitionServiceResponse_t1770101589, ___U3CRecognizedU3Ek__BackingField_0)); }
	inline bool get_U3CRecognizedU3Ek__BackingField_0() const { return ___U3CRecognizedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CRecognizedU3Ek__BackingField_0() { return &___U3CRecognizedU3Ek__BackingField_0; }
	inline void set_U3CRecognizedU3Ek__BackingField_0(bool value)
	{
		___U3CRecognizedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CloudRecognitionServiceResponse_t1770101589, ___U3CInfoU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CInfoU3Ek__BackingField_1() const { return ___U3CInfoU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CInfoU3Ek__BackingField_1() { return &___U3CInfoU3Ek__BackingField_1; }
	inline void set_U3CInfoU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CloudRecognitionServiceResponse_t1770101589, ___U3CMetadataU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CMetadataU3Ek__BackingField_2() const { return ___U3CMetadataU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CMetadataU3Ek__BackingField_2() { return &___U3CMetadataU3Ek__BackingField_2; }
	inline void set_U3CMetadataU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CMetadataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetadataU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOGNITIONSERVICERESPONSE_T1770101589_H
#ifndef UNITYEDITORSIMULATOR_T2033215626_H
#define UNITYEDITORSIMULATOR_T2033215626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityEditorSimulator
struct  UnityEditorSimulator_t2033215626  : public RuntimeObject
{
public:

public:
};

struct UnityEditorSimulator_t2033215626_StaticFields
{
public:
	// System.Single[] Wikitude.UnityEditorSimulator::_defaultTrackingMatrix
	SingleU5BU5D_t1444911251* ____defaultTrackingMatrix_0;
	// System.Collections.Generic.List`1<Wikitude.UnityEditorSimulator/SimulatedTarget> Wikitude.UnityEditorSimulator::_currentTargets
	List_1_t3036245690 * ____currentTargets_1;

public:
	inline static int32_t get_offset_of__defaultTrackingMatrix_0() { return static_cast<int32_t>(offsetof(UnityEditorSimulator_t2033215626_StaticFields, ____defaultTrackingMatrix_0)); }
	inline SingleU5BU5D_t1444911251* get__defaultTrackingMatrix_0() const { return ____defaultTrackingMatrix_0; }
	inline SingleU5BU5D_t1444911251** get_address_of__defaultTrackingMatrix_0() { return &____defaultTrackingMatrix_0; }
	inline void set__defaultTrackingMatrix_0(SingleU5BU5D_t1444911251* value)
	{
		____defaultTrackingMatrix_0 = value;
		Il2CppCodeGenWriteBarrier((&____defaultTrackingMatrix_0), value);
	}

	inline static int32_t get_offset_of__currentTargets_1() { return static_cast<int32_t>(offsetof(UnityEditorSimulator_t2033215626_StaticFields, ____currentTargets_1)); }
	inline List_1_t3036245690 * get__currentTargets_1() const { return ____currentTargets_1; }
	inline List_1_t3036245690 ** get_address_of__currentTargets_1() { return &____currentTargets_1; }
	inline void set__currentTargets_1(List_1_t3036245690 * value)
	{
		____currentTargets_1 = value;
		Il2CppCodeGenWriteBarrier((&____currentTargets_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEDITORSIMULATOR_T2033215626_H
#ifndef UNITYBRIDGE_T2504510425_H
#define UNITYBRIDGE_T2504510425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityBridge
struct  UnityBridge_t2504510425  : public RuntimeObject
{
public:
	// System.Single[] Wikitude.UnityBridge::GenericTrackingMatrices
	SingleU5BU5D_t1444911251* ___GenericTrackingMatrices_0;
	// System.Single[] Wikitude.UnityBridge::GenericProjectionMatrix
	SingleU5BU5D_t1444911251* ___GenericProjectionMatrix_1;
	// System.Single[] Wikitude.UnityBridge::ObjectTrackingMatrix
	SingleU5BU5D_t1444911251* ___ObjectTrackingMatrix_2;
	// System.Single[] Wikitude.UnityBridge::ObjectProjectionMatrix
	SingleU5BU5D_t1444911251* ___ObjectProjectionMatrix_3;
	// System.Single[] Wikitude.UnityBridge::PhysicalTargetHeights
	SingleU5BU5D_t1444911251* ___PhysicalTargetHeights_4;
	// System.Int64[] Wikitude.UnityBridge::TargetIDs
	Int64U5BU5D_t2559172825* ___TargetIDs_5;
	// System.String Wikitude.UnityBridge::TrackedTargets
	String_t* ___TrackedTargets_6;
	// System.Int32 Wikitude.UnityBridge::TargetCount
	int32_t ___TargetCount_7;
	// System.Boolean Wikitude.UnityBridge::_isObjectTrackingRunning
	bool ____isObjectTrackingRunning_8;
	// System.String Wikitude.UnityBridge::_trackerManagerName
	String_t* ____trackerManagerName_9;

public:
	inline static int32_t get_offset_of_GenericTrackingMatrices_0() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___GenericTrackingMatrices_0)); }
	inline SingleU5BU5D_t1444911251* get_GenericTrackingMatrices_0() const { return ___GenericTrackingMatrices_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_GenericTrackingMatrices_0() { return &___GenericTrackingMatrices_0; }
	inline void set_GenericTrackingMatrices_0(SingleU5BU5D_t1444911251* value)
	{
		___GenericTrackingMatrices_0 = value;
		Il2CppCodeGenWriteBarrier((&___GenericTrackingMatrices_0), value);
	}

	inline static int32_t get_offset_of_GenericProjectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___GenericProjectionMatrix_1)); }
	inline SingleU5BU5D_t1444911251* get_GenericProjectionMatrix_1() const { return ___GenericProjectionMatrix_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_GenericProjectionMatrix_1() { return &___GenericProjectionMatrix_1; }
	inline void set_GenericProjectionMatrix_1(SingleU5BU5D_t1444911251* value)
	{
		___GenericProjectionMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___GenericProjectionMatrix_1), value);
	}

	inline static int32_t get_offset_of_ObjectTrackingMatrix_2() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___ObjectTrackingMatrix_2)); }
	inline SingleU5BU5D_t1444911251* get_ObjectTrackingMatrix_2() const { return ___ObjectTrackingMatrix_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_ObjectTrackingMatrix_2() { return &___ObjectTrackingMatrix_2; }
	inline void set_ObjectTrackingMatrix_2(SingleU5BU5D_t1444911251* value)
	{
		___ObjectTrackingMatrix_2 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectTrackingMatrix_2), value);
	}

	inline static int32_t get_offset_of_ObjectProjectionMatrix_3() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___ObjectProjectionMatrix_3)); }
	inline SingleU5BU5D_t1444911251* get_ObjectProjectionMatrix_3() const { return ___ObjectProjectionMatrix_3; }
	inline SingleU5BU5D_t1444911251** get_address_of_ObjectProjectionMatrix_3() { return &___ObjectProjectionMatrix_3; }
	inline void set_ObjectProjectionMatrix_3(SingleU5BU5D_t1444911251* value)
	{
		___ObjectProjectionMatrix_3 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectProjectionMatrix_3), value);
	}

	inline static int32_t get_offset_of_PhysicalTargetHeights_4() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___PhysicalTargetHeights_4)); }
	inline SingleU5BU5D_t1444911251* get_PhysicalTargetHeights_4() const { return ___PhysicalTargetHeights_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_PhysicalTargetHeights_4() { return &___PhysicalTargetHeights_4; }
	inline void set_PhysicalTargetHeights_4(SingleU5BU5D_t1444911251* value)
	{
		___PhysicalTargetHeights_4 = value;
		Il2CppCodeGenWriteBarrier((&___PhysicalTargetHeights_4), value);
	}

	inline static int32_t get_offset_of_TargetIDs_5() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___TargetIDs_5)); }
	inline Int64U5BU5D_t2559172825* get_TargetIDs_5() const { return ___TargetIDs_5; }
	inline Int64U5BU5D_t2559172825** get_address_of_TargetIDs_5() { return &___TargetIDs_5; }
	inline void set_TargetIDs_5(Int64U5BU5D_t2559172825* value)
	{
		___TargetIDs_5 = value;
		Il2CppCodeGenWriteBarrier((&___TargetIDs_5), value);
	}

	inline static int32_t get_offset_of_TrackedTargets_6() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___TrackedTargets_6)); }
	inline String_t* get_TrackedTargets_6() const { return ___TrackedTargets_6; }
	inline String_t** get_address_of_TrackedTargets_6() { return &___TrackedTargets_6; }
	inline void set_TrackedTargets_6(String_t* value)
	{
		___TrackedTargets_6 = value;
		Il2CppCodeGenWriteBarrier((&___TrackedTargets_6), value);
	}

	inline static int32_t get_offset_of_TargetCount_7() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___TargetCount_7)); }
	inline int32_t get_TargetCount_7() const { return ___TargetCount_7; }
	inline int32_t* get_address_of_TargetCount_7() { return &___TargetCount_7; }
	inline void set_TargetCount_7(int32_t value)
	{
		___TargetCount_7 = value;
	}

	inline static int32_t get_offset_of__isObjectTrackingRunning_8() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ____isObjectTrackingRunning_8)); }
	inline bool get__isObjectTrackingRunning_8() const { return ____isObjectTrackingRunning_8; }
	inline bool* get_address_of__isObjectTrackingRunning_8() { return &____isObjectTrackingRunning_8; }
	inline void set__isObjectTrackingRunning_8(bool value)
	{
		____isObjectTrackingRunning_8 = value;
	}

	inline static int32_t get_offset_of__trackerManagerName_9() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ____trackerManagerName_9)); }
	inline String_t* get__trackerManagerName_9() const { return ____trackerManagerName_9; }
	inline String_t** get_address_of__trackerManagerName_9() { return &____trackerManagerName_9; }
	inline void set__trackerManagerName_9(String_t* value)
	{
		____trackerManagerName_9 = value;
		Il2CppCodeGenWriteBarrier((&____trackerManagerName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYBRIDGE_T2504510425_H
#ifndef WIKITUDEBRIDGE_T4110292378_H
#define WIKITUDEBRIDGE_T4110292378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeBridge
struct  WikitudeBridge_t4110292378  : public RuntimeObject
{
public:
	// Wikitude.TrackerManager Wikitude.WikitudeBridge::_trackerManager
	TrackerManager_t2102804711 * ____trackerManager_0;
	// UnityEngine.GameObject Wikitude.WikitudeBridge::_trackerManagerGameObject
	GameObject_t1113636619 * ____trackerManagerGameObject_1;

public:
	inline static int32_t get_offset_of__trackerManager_0() { return static_cast<int32_t>(offsetof(WikitudeBridge_t4110292378, ____trackerManager_0)); }
	inline TrackerManager_t2102804711 * get__trackerManager_0() const { return ____trackerManager_0; }
	inline TrackerManager_t2102804711 ** get_address_of__trackerManager_0() { return &____trackerManager_0; }
	inline void set__trackerManager_0(TrackerManager_t2102804711 * value)
	{
		____trackerManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____trackerManager_0), value);
	}

	inline static int32_t get_offset_of__trackerManagerGameObject_1() { return static_cast<int32_t>(offsetof(WikitudeBridge_t4110292378, ____trackerManagerGameObject_1)); }
	inline GameObject_t1113636619 * get__trackerManagerGameObject_1() const { return ____trackerManagerGameObject_1; }
	inline GameObject_t1113636619 ** get_address_of__trackerManagerGameObject_1() { return &____trackerManagerGameObject_1; }
	inline void set__trackerManagerGameObject_1(GameObject_t1113636619 * value)
	{
		____trackerManagerGameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____trackerManagerGameObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIKITUDEBRIDGE_T4110292378_H
#ifndef TARGETSOURCE_T2046190744_H
#define TARGETSOURCE_T2046190744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetSource
struct  TargetSource_t2046190744  : public RuntimeObject
{
public:
	// System.Int64 Wikitude.TargetSource::_identifier
	int64_t ____identifier_0;
	// System.Boolean Wikitude.TargetSource::<IsRegistered>k__BackingField
	bool ___U3CIsRegisteredU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__identifier_0() { return static_cast<int32_t>(offsetof(TargetSource_t2046190744, ____identifier_0)); }
	inline int64_t get__identifier_0() const { return ____identifier_0; }
	inline int64_t* get_address_of__identifier_0() { return &____identifier_0; }
	inline void set__identifier_0(int64_t value)
	{
		____identifier_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsRegisteredU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TargetSource_t2046190744, ___U3CIsRegisteredU3Ek__BackingField_1)); }
	inline bool get_U3CIsRegisteredU3Ek__BackingField_1() const { return ___U3CIsRegisteredU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsRegisteredU3Ek__BackingField_1() { return &___U3CIsRegisteredU3Ek__BackingField_1; }
	inline void set_U3CIsRegisteredU3Ek__BackingField_1(bool value)
	{
		___U3CIsRegisteredU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETSOURCE_T2046190744_H
#ifndef SPRITEDATAOBJECT_T308163541_H
#define SPRITEDATAOBJECT_T308163541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject
struct  SpriteDataObject_t308163541  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData> TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject::frames
	List_1_t225505033 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t308163541, ___frames_0)); }
	inline List_1_t225505033 * get_frames_0() const { return ___frames_0; }
	inline List_1_t225505033 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t225505033 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T308163541_H
#ifndef TMP_STYLE_T3479380764_H
#define TMP_STYLE_T3479380764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Style
struct  TMP_Style_t3479380764  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Style::m_Name
	String_t* ___m_Name_0;
	// System.Int32 TMPro.TMP_Style::m_HashCode
	int32_t ___m_HashCode_1;
	// System.String TMPro.TMP_Style::m_OpeningDefinition
	String_t* ___m_OpeningDefinition_2;
	// System.String TMPro.TMP_Style::m_ClosingDefinition
	String_t* ___m_ClosingDefinition_3;
	// System.Int32[] TMPro.TMP_Style::m_OpeningTagArray
	Int32U5BU5D_t385246372* ___m_OpeningTagArray_4;
	// System.Int32[] TMPro.TMP_Style::m_ClosingTagArray
	Int32U5BU5D_t385246372* ___m_ClosingTagArray_5;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_0), value);
	}

	inline static int32_t get_offset_of_m_HashCode_1() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_HashCode_1)); }
	inline int32_t get_m_HashCode_1() const { return ___m_HashCode_1; }
	inline int32_t* get_address_of_m_HashCode_1() { return &___m_HashCode_1; }
	inline void set_m_HashCode_1(int32_t value)
	{
		___m_HashCode_1 = value;
	}

	inline static int32_t get_offset_of_m_OpeningDefinition_2() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_OpeningDefinition_2)); }
	inline String_t* get_m_OpeningDefinition_2() const { return ___m_OpeningDefinition_2; }
	inline String_t** get_address_of_m_OpeningDefinition_2() { return &___m_OpeningDefinition_2; }
	inline void set_m_OpeningDefinition_2(String_t* value)
	{
		___m_OpeningDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningDefinition_2), value);
	}

	inline static int32_t get_offset_of_m_ClosingDefinition_3() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_ClosingDefinition_3)); }
	inline String_t* get_m_ClosingDefinition_3() const { return ___m_ClosingDefinition_3; }
	inline String_t** get_address_of_m_ClosingDefinition_3() { return &___m_ClosingDefinition_3; }
	inline void set_m_ClosingDefinition_3(String_t* value)
	{
		___m_ClosingDefinition_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingDefinition_3), value);
	}

	inline static int32_t get_offset_of_m_OpeningTagArray_4() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_OpeningTagArray_4)); }
	inline Int32U5BU5D_t385246372* get_m_OpeningTagArray_4() const { return ___m_OpeningTagArray_4; }
	inline Int32U5BU5D_t385246372** get_address_of_m_OpeningTagArray_4() { return &___m_OpeningTagArray_4; }
	inline void set_m_OpeningTagArray_4(Int32U5BU5D_t385246372* value)
	{
		___m_OpeningTagArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningTagArray_4), value);
	}

	inline static int32_t get_offset_of_m_ClosingTagArray_5() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_ClosingTagArray_5)); }
	inline Int32U5BU5D_t385246372* get_m_ClosingTagArray_5() const { return ___m_ClosingTagArray_5; }
	inline Int32U5BU5D_t385246372** get_address_of_m_ClosingTagArray_5() { return &___m_ClosingTagArray_5; }
	inline void set_m_ClosingTagArray_5(Int32U5BU5D_t385246372* value)
	{
		___m_ClosingTagArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingTagArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLE_T3479380764_H
#ifndef TEXTUREPACKER_T3148178657_H
#define TEXTUREPACKER_T3148178657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t3148178657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T3148178657_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef SPRITESIZE_T3355290999_H
#define SPRITESIZE_T3355290999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize
struct  SpriteSize_t3355290999 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T3355290999_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U24ARRAYTYPEU3D40_T2865632059_H
#define U24ARRAYTYPEU3D40_T2865632059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2865632059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2865632059__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2865632059_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef FRAMESTRIDES_T352336826_H
#define FRAMESTRIDES_T352336826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.FrameStrides
struct  FrameStrides_t352336826 
{
public:
	// System.Int32 Wikitude.FrameStrides::LuminancePixelStride
	int32_t ___LuminancePixelStride_0;
	// System.Int32 Wikitude.FrameStrides::LuminanceRowStride
	int32_t ___LuminanceRowStride_1;
	// System.Int32 Wikitude.FrameStrides::ChrominanceRedPixelStride
	int32_t ___ChrominanceRedPixelStride_2;
	// System.Int32 Wikitude.FrameStrides::ChrominanceRedRowStride
	int32_t ___ChrominanceRedRowStride_3;
	// System.Int32 Wikitude.FrameStrides::ChrominanceBluePixelStride
	int32_t ___ChrominanceBluePixelStride_4;
	// System.Int32 Wikitude.FrameStrides::ChrominanceBlueRowStride
	int32_t ___ChrominanceBlueRowStride_5;

public:
	inline static int32_t get_offset_of_LuminancePixelStride_0() { return static_cast<int32_t>(offsetof(FrameStrides_t352336826, ___LuminancePixelStride_0)); }
	inline int32_t get_LuminancePixelStride_0() const { return ___LuminancePixelStride_0; }
	inline int32_t* get_address_of_LuminancePixelStride_0() { return &___LuminancePixelStride_0; }
	inline void set_LuminancePixelStride_0(int32_t value)
	{
		___LuminancePixelStride_0 = value;
	}

	inline static int32_t get_offset_of_LuminanceRowStride_1() { return static_cast<int32_t>(offsetof(FrameStrides_t352336826, ___LuminanceRowStride_1)); }
	inline int32_t get_LuminanceRowStride_1() const { return ___LuminanceRowStride_1; }
	inline int32_t* get_address_of_LuminanceRowStride_1() { return &___LuminanceRowStride_1; }
	inline void set_LuminanceRowStride_1(int32_t value)
	{
		___LuminanceRowStride_1 = value;
	}

	inline static int32_t get_offset_of_ChrominanceRedPixelStride_2() { return static_cast<int32_t>(offsetof(FrameStrides_t352336826, ___ChrominanceRedPixelStride_2)); }
	inline int32_t get_ChrominanceRedPixelStride_2() const { return ___ChrominanceRedPixelStride_2; }
	inline int32_t* get_address_of_ChrominanceRedPixelStride_2() { return &___ChrominanceRedPixelStride_2; }
	inline void set_ChrominanceRedPixelStride_2(int32_t value)
	{
		___ChrominanceRedPixelStride_2 = value;
	}

	inline static int32_t get_offset_of_ChrominanceRedRowStride_3() { return static_cast<int32_t>(offsetof(FrameStrides_t352336826, ___ChrominanceRedRowStride_3)); }
	inline int32_t get_ChrominanceRedRowStride_3() const { return ___ChrominanceRedRowStride_3; }
	inline int32_t* get_address_of_ChrominanceRedRowStride_3() { return &___ChrominanceRedRowStride_3; }
	inline void set_ChrominanceRedRowStride_3(int32_t value)
	{
		___ChrominanceRedRowStride_3 = value;
	}

	inline static int32_t get_offset_of_ChrominanceBluePixelStride_4() { return static_cast<int32_t>(offsetof(FrameStrides_t352336826, ___ChrominanceBluePixelStride_4)); }
	inline int32_t get_ChrominanceBluePixelStride_4() const { return ___ChrominanceBluePixelStride_4; }
	inline int32_t* get_address_of_ChrominanceBluePixelStride_4() { return &___ChrominanceBluePixelStride_4; }
	inline void set_ChrominanceBluePixelStride_4(int32_t value)
	{
		___ChrominanceBluePixelStride_4 = value;
	}

	inline static int32_t get_offset_of_ChrominanceBlueRowStride_5() { return static_cast<int32_t>(offsetof(FrameStrides_t352336826, ___ChrominanceBlueRowStride_5)); }
	inline int32_t get_ChrominanceBlueRowStride_5() const { return ___ChrominanceBlueRowStride_5; }
	inline int32_t* get_address_of_ChrominanceBlueRowStride_5() { return &___ChrominanceBlueRowStride_5; }
	inline void set_ChrominanceBlueRowStride_5(int32_t value)
	{
		___ChrominanceBlueRowStride_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESTRIDES_T352336826_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef CAMERAINFO_T2490161545_H
#define CAMERAINFO_T2490161545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CameraInfo
struct  CameraInfo_t2490161545 
{
public:
	// System.Int32 Wikitude.CameraInfo::Width
	int32_t ___Width_0;
	// System.Int32 Wikitude.CameraInfo::Height
	int32_t ___Height_1;
	// System.Int32 Wikitude.CameraInfo::Framerate
	int32_t ___Framerate_2;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(CameraInfo_t2490161545, ___Width_0)); }
	inline int32_t get_Width_0() const { return ___Width_0; }
	inline int32_t* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(int32_t value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Height_1() { return static_cast<int32_t>(offsetof(CameraInfo_t2490161545, ___Height_1)); }
	inline int32_t get_Height_1() const { return ___Height_1; }
	inline int32_t* get_address_of_Height_1() { return &___Height_1; }
	inline void set_Height_1(int32_t value)
	{
		___Height_1 = value;
	}

	inline static int32_t get_offset_of_Framerate_2() { return static_cast<int32_t>(offsetof(CameraInfo_t2490161545, ___Framerate_2)); }
	inline int32_t get_Framerate_2() const { return ___Framerate_2; }
	inline int32_t* get_address_of_Framerate_2() { return &___Framerate_2; }
	inline void set_Framerate_2(int32_t value)
	{
		___Framerate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAINFO_T2490161545_H
#ifndef SPRITEFRAME_T3912389194_H
#define SPRITEFRAME_T3912389194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame
struct  SpriteFrame_t3912389194 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_T3912389194_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_1_T1476324867_H
#define UNITYEVENT_1_T1476324867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Double>
struct  UnityEvent_1_t1476324867  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1476324867, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1476324867_H
#ifndef UNITYEVENT_1_T4195527445_H
#define UNITYEVENT_1_T4195527445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.InstantTarget>
struct  UnityEvent_1_t4195527445  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4195527445, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4195527445_H
#ifndef UNITYEVENT_1_T560102991_H
#define UNITYEVENT_1_T560102991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.InstantTrackingState>
struct  UnityEvent_1_t560102991  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t560102991, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T560102991_H
#ifndef UNITYEVENT_1_T3204000826_H
#define UNITYEVENT_1_T3204000826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.ImageTarget>
struct  UnityEvent_1_t3204000826  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3204000826, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3204000826_H
#ifndef UNITYEVENT_2_T631475969_H
#define UNITYEVENT_2_T631475969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,Wikitude.ExtendedTrackingQuality>
struct  UnityEvent_2_t631475969  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t631475969, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T631475969_H
#ifndef UNITYEVENT_3_T1153900776_H
#define UNITYEVENT_3_T1153900776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Boolean,UnityEngine.Vector2,UnityEngine.Vector3>
struct  UnityEvent_3_t1153900776  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1153900776, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1153900776_H
#ifndef UNITYEVENT_2_T1217887265_H
#define UNITYEVENT_2_T1217887265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.String>
struct  UnityEvent_2_t1217887265  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1217887265, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1217887265_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef UNITYEVENT_1_T1569884548_H
#define UNITYEVENT_1_T1569884548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.ObjectTarget>
struct  UnityEvent_1_t1569884548  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1569884548, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1569884548_H
#ifndef UNITYEVENT_1_T1944436728_H
#define UNITYEVENT_1_T1944436728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.Frame>
struct  UnityEvent_1_t1944436728  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1944436728, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1944436728_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef TMP_XMLTAGSTACK_1_T3241710312_H
#define TMP_XMLTAGSTACK_1_T3241710312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t3241710312 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2496920137* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t3678055768 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2496920137* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2496920137** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2496920137* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t3678055768 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t3678055768 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3241710312_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef UNITYEVENT_1_T2651761093_H
#define UNITYEVENT_1_T2651761093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.CloudRecognitionServiceResponse>
struct  UnityEvent_1_t2651761093  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2651761093, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2651761093_H
#ifndef CAPTUREDEVICEFRAMERATE_T2114957004_H
#define CAPTUREDEVICEFRAMERATE_T2114957004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureDeviceFramerate
struct  CaptureDeviceFramerate_t2114957004 
{
public:
	// System.Int32 Wikitude.CaptureDeviceFramerate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureDeviceFramerate_t2114957004, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREDEVICEFRAMERATE_T2114957004_H
#ifndef CAPTUREDEVICERESOLUTION_T2690595878_H
#define CAPTUREDEVICERESOLUTION_T2690595878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureDeviceResolution
struct  CaptureDeviceResolution_t2690595878 
{
public:
	// System.Int32 Wikitude.CaptureDeviceResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureDeviceResolution_t2690595878, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREDEVICERESOLUTION_T2690595878_H
#ifndef CAPTUREEXPOSUREMODE_T1315396563_H
#define CAPTUREEXPOSUREMODE_T1315396563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureExposureMode
struct  CaptureExposureMode_t1315396563 
{
public:
	// System.Int32 Wikitude.CaptureExposureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureExposureMode_t1315396563, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREEXPOSUREMODE_T1315396563_H
#ifndef ONINPUTPLUGINFAILUREEVENT_T818001259_H
#define ONINPUTPLUGINFAILUREEVENT_T818001259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeCamera/OnInputPluginFailureEvent
struct  OnInputPluginFailureEvent_t818001259  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINPUTPLUGINFAILUREEVENT_T818001259_H
#ifndef ONCAMERAFAILUREEVENT_T2345510068_H
#define ONCAMERAFAILUREEVENT_T2345510068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeCamera/OnCameraFailureEvent
struct  OnCameraFailureEvent_t2345510068  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCAMERAFAILUREEVENT_T2345510068_H
#ifndef CAPTUREDEVICEPOSITION_T3635123692_H
#define CAPTUREDEVICEPOSITION_T3635123692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureDevicePosition
struct  CaptureDevicePosition_t3635123692 
{
public:
	// System.Int32 Wikitude.CaptureDevicePosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureDevicePosition_t3635123692, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREDEVICEPOSITION_T3635123692_H
#ifndef CLOUDRECOGNITIONSERVERREGION_T2146108836_H
#define CLOUDRECOGNITIONSERVERREGION_T2146108836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerManager/CloudRecognitionServerRegion
struct  CloudRecognitionServerRegion_t2146108836 
{
public:
	// System.Int32 Wikitude.TrackerManager/CloudRecognitionServerRegion::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRecognitionServerRegion_t2146108836, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOGNITIONSERVERREGION_T2146108836_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef CAPTUREFLASHMODE_T2444581339_H
#define CAPTUREFLASHMODE_T2444581339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureFlashMode
struct  CaptureFlashMode_t2444581339 
{
public:
	// System.Int32 Wikitude.CaptureFlashMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureFlashMode_t2444581339, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFLASHMODE_T2444581339_H
#ifndef CAPTUREAUTOFOCUSRESTRICTION_T4132688021_H
#define CAPTUREAUTOFOCUSRESTRICTION_T4132688021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureAutoFocusRestriction
struct  CaptureAutoFocusRestriction_t4132688021 
{
public:
	// System.Int32 Wikitude.CaptureAutoFocusRestriction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureAutoFocusRestriction_t4132688021, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREAUTOFOCUSRESTRICTION_T4132688021_H
#ifndef CAPTUREFOCUSMODE_T3452326368_H
#define CAPTUREFOCUSMODE_T3452326368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CaptureFocusMode
struct  CaptureFocusMode_t3452326368 
{
public:
	// System.Int32 Wikitude.CaptureFocusMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureFocusMode_t3452326368, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFOCUSMODE_T3452326368_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef TARGETSOURCETYPE_T830527111_H
#define TARGETSOURCETYPE_T830527111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TargetSourceType
struct  TargetSourceType_t830527111 
{
public:
	// System.Int32 Wikitude.TargetSourceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetSourceType_t830527111, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETSOURCETYPE_T830527111_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef ONRECOGNITIONRESPONSEEVENT_T3712700845_H
#define ONRECOGNITIONRESPONSEEVENT_T3712700845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService/OnRecognitionResponseEvent
struct  OnRecognitionResponseEvent_t3712700845  : public UnityEvent_1_t2651761093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECOGNITIONRESPONSEEVENT_T3712700845_H
#ifndef ONINITIALIZATIONERROREVENT_T511199451_H
#define ONINITIALIZATIONERROREVENT_T511199451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService/OnInitializationErrorEvent
struct  OnInitializationErrorEvent_t511199451  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINITIALIZATIONERROREVENT_T511199451_H
#ifndef ONINITIALIZEDEVENT_T1242699385_H
#define ONINITIALIZEDEVENT_T1242699385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService/OnInitializedEvent
struct  OnInitializedEvent_t1242699385  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINITIALIZEDEVENT_T1242699385_H
#ifndef FRAMECOLORSPACE_T1906248721_H
#define FRAMECOLORSPACE_T1906248721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.FrameColorSpace
struct  FrameColorSpace_t1906248721 
{
public:
	// System.Int32 Wikitude.FrameColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameColorSpace_t1906248721, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMECOLORSPACE_T1906248721_H
#ifndef ONINTERRUPTIONEVENT_T1530367979_H
#define ONINTERRUPTIONEVENT_T1530367979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService/OnInterruptionEvent
struct  OnInterruptionEvent_t1530367979  : public UnityEvent_1_t1476324867
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINTERRUPTIONEVENT_T1530367979_H
#ifndef ONRECOGNITIONERROREVENT_T2212464566_H
#define ONRECOGNITIONERROREVENT_T2212464566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService/OnRecognitionErrorEvent
struct  OnRecognitionErrorEvent_t2212464566  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECOGNITIONERROREVENT_T2212464566_H
#ifndef ONIMAGERECOGNIZEDEVENT_T773132985_H
#define ONIMAGERECOGNIZEDEVENT_T773132985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable/OnImageRecognizedEvent
struct  OnImageRecognizedEvent_t773132985  : public UnityEvent_1_t3204000826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONIMAGERECOGNIZEDEVENT_T773132985_H
#ifndef ONIMAGELOSTEVENT_T966623312_H
#define ONIMAGELOSTEVENT_T966623312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable/OnImageLostEvent
struct  OnImageLostEvent_t966623312  : public UnityEvent_1_t3204000826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONIMAGELOSTEVENT_T966623312_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef ANDROIDBRIDGE_T2441337961_H
#define ANDROIDBRIDGE_T2441337961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.AndroidBridge
struct  AndroidBridge_t2441337961  : public RuntimeObject
{
public:
	// System.Object Wikitude.AndroidBridge::androidClass
	RuntimeObject * ___androidClass_0;
	// System.Type Wikitude.AndroidBridge::androidJNIType
	Type_t * ___androidJNIType_1;
	// System.Object Wikitude.AndroidBridge::androidActivity
	RuntimeObject * ___androidActivity_2;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callMethod
	MethodInfo_t * ____callMethod_3;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callStringMethod
	MethodInfo_t * ____callStringMethod_4;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callFloatArrayMethod
	MethodInfo_t * ____callFloatArrayMethod_5;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callLongArrayMethod
	MethodInfo_t * ____callLongArrayMethod_6;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callIntMethod
	MethodInfo_t * ____callIntMethod_7;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callFloatMethod
	MethodInfo_t * ____callFloatMethod_8;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callBoolMethod
	MethodInfo_t * ____callBoolMethod_9;
	// System.Reflection.MethodInfo Wikitude.AndroidBridge::_callLongMethod
	MethodInfo_t * ____callLongMethod_10;
	// System.IntPtr Wikitude.AndroidBridge::_nativeFramePtr
	intptr_t ____nativeFramePtr_11;

public:
	inline static int32_t get_offset_of_androidClass_0() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ___androidClass_0)); }
	inline RuntimeObject * get_androidClass_0() const { return ___androidClass_0; }
	inline RuntimeObject ** get_address_of_androidClass_0() { return &___androidClass_0; }
	inline void set_androidClass_0(RuntimeObject * value)
	{
		___androidClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___androidClass_0), value);
	}

	inline static int32_t get_offset_of_androidJNIType_1() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ___androidJNIType_1)); }
	inline Type_t * get_androidJNIType_1() const { return ___androidJNIType_1; }
	inline Type_t ** get_address_of_androidJNIType_1() { return &___androidJNIType_1; }
	inline void set_androidJNIType_1(Type_t * value)
	{
		___androidJNIType_1 = value;
		Il2CppCodeGenWriteBarrier((&___androidJNIType_1), value);
	}

	inline static int32_t get_offset_of_androidActivity_2() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ___androidActivity_2)); }
	inline RuntimeObject * get_androidActivity_2() const { return ___androidActivity_2; }
	inline RuntimeObject ** get_address_of_androidActivity_2() { return &___androidActivity_2; }
	inline void set_androidActivity_2(RuntimeObject * value)
	{
		___androidActivity_2 = value;
		Il2CppCodeGenWriteBarrier((&___androidActivity_2), value);
	}

	inline static int32_t get_offset_of__callMethod_3() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callMethod_3)); }
	inline MethodInfo_t * get__callMethod_3() const { return ____callMethod_3; }
	inline MethodInfo_t ** get_address_of__callMethod_3() { return &____callMethod_3; }
	inline void set__callMethod_3(MethodInfo_t * value)
	{
		____callMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&____callMethod_3), value);
	}

	inline static int32_t get_offset_of__callStringMethod_4() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callStringMethod_4)); }
	inline MethodInfo_t * get__callStringMethod_4() const { return ____callStringMethod_4; }
	inline MethodInfo_t ** get_address_of__callStringMethod_4() { return &____callStringMethod_4; }
	inline void set__callStringMethod_4(MethodInfo_t * value)
	{
		____callStringMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&____callStringMethod_4), value);
	}

	inline static int32_t get_offset_of__callFloatArrayMethod_5() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callFloatArrayMethod_5)); }
	inline MethodInfo_t * get__callFloatArrayMethod_5() const { return ____callFloatArrayMethod_5; }
	inline MethodInfo_t ** get_address_of__callFloatArrayMethod_5() { return &____callFloatArrayMethod_5; }
	inline void set__callFloatArrayMethod_5(MethodInfo_t * value)
	{
		____callFloatArrayMethod_5 = value;
		Il2CppCodeGenWriteBarrier((&____callFloatArrayMethod_5), value);
	}

	inline static int32_t get_offset_of__callLongArrayMethod_6() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callLongArrayMethod_6)); }
	inline MethodInfo_t * get__callLongArrayMethod_6() const { return ____callLongArrayMethod_6; }
	inline MethodInfo_t ** get_address_of__callLongArrayMethod_6() { return &____callLongArrayMethod_6; }
	inline void set__callLongArrayMethod_6(MethodInfo_t * value)
	{
		____callLongArrayMethod_6 = value;
		Il2CppCodeGenWriteBarrier((&____callLongArrayMethod_6), value);
	}

	inline static int32_t get_offset_of__callIntMethod_7() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callIntMethod_7)); }
	inline MethodInfo_t * get__callIntMethod_7() const { return ____callIntMethod_7; }
	inline MethodInfo_t ** get_address_of__callIntMethod_7() { return &____callIntMethod_7; }
	inline void set__callIntMethod_7(MethodInfo_t * value)
	{
		____callIntMethod_7 = value;
		Il2CppCodeGenWriteBarrier((&____callIntMethod_7), value);
	}

	inline static int32_t get_offset_of__callFloatMethod_8() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callFloatMethod_8)); }
	inline MethodInfo_t * get__callFloatMethod_8() const { return ____callFloatMethod_8; }
	inline MethodInfo_t ** get_address_of__callFloatMethod_8() { return &____callFloatMethod_8; }
	inline void set__callFloatMethod_8(MethodInfo_t * value)
	{
		____callFloatMethod_8 = value;
		Il2CppCodeGenWriteBarrier((&____callFloatMethod_8), value);
	}

	inline static int32_t get_offset_of__callBoolMethod_9() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callBoolMethod_9)); }
	inline MethodInfo_t * get__callBoolMethod_9() const { return ____callBoolMethod_9; }
	inline MethodInfo_t ** get_address_of__callBoolMethod_9() { return &____callBoolMethod_9; }
	inline void set__callBoolMethod_9(MethodInfo_t * value)
	{
		____callBoolMethod_9 = value;
		Il2CppCodeGenWriteBarrier((&____callBoolMethod_9), value);
	}

	inline static int32_t get_offset_of__callLongMethod_10() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____callLongMethod_10)); }
	inline MethodInfo_t * get__callLongMethod_10() const { return ____callLongMethod_10; }
	inline MethodInfo_t ** get_address_of__callLongMethod_10() { return &____callLongMethod_10; }
	inline void set__callLongMethod_10(MethodInfo_t * value)
	{
		____callLongMethod_10 = value;
		Il2CppCodeGenWriteBarrier((&____callLongMethod_10), value);
	}

	inline static int32_t get_offset_of__nativeFramePtr_11() { return static_cast<int32_t>(offsetof(AndroidBridge_t2441337961, ____nativeFramePtr_11)); }
	inline intptr_t get__nativeFramePtr_11() const { return ____nativeFramePtr_11; }
	inline intptr_t* get_address_of__nativeFramePtr_11() { return &____nativeFramePtr_11; }
	inline void set__nativeFramePtr_11(intptr_t value)
	{
		____nativeFramePtr_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDBRIDGE_T2441337961_H
#ifndef SIMULATEDTARGET_T1564170948_H
#define SIMULATEDTARGET_T1564170948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityEditorSimulator/SimulatedTarget
struct  SimulatedTarget_t1564170948 
{
public:
	// System.String Wikitude.UnityEditorSimulator/SimulatedTarget::Name
	String_t* ___Name_0;
	// System.Int32 Wikitude.UnityEditorSimulator/SimulatedTarget::UniqueId
	int32_t ___UniqueId_1;
	// UnityEngine.Vector3 Wikitude.UnityEditorSimulator/SimulatedTarget::Offset
	Vector3_t3722313464  ___Offset_2;
	// System.Single Wikitude.UnityEditorSimulator/SimulatedTarget::PhysicalTargetHeight
	float ___PhysicalTargetHeight_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_UniqueId_1() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___UniqueId_1)); }
	inline int32_t get_UniqueId_1() const { return ___UniqueId_1; }
	inline int32_t* get_address_of_UniqueId_1() { return &___UniqueId_1; }
	inline void set_UniqueId_1(int32_t value)
	{
		___UniqueId_1 = value;
	}

	inline static int32_t get_offset_of_Offset_2() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___Offset_2)); }
	inline Vector3_t3722313464  get_Offset_2() const { return ___Offset_2; }
	inline Vector3_t3722313464 * get_address_of_Offset_2() { return &___Offset_2; }
	inline void set_Offset_2(Vector3_t3722313464  value)
	{
		___Offset_2 = value;
	}

	inline static int32_t get_offset_of_PhysicalTargetHeight_3() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___PhysicalTargetHeight_3)); }
	inline float get_PhysicalTargetHeight_3() const { return ___PhysicalTargetHeight_3; }
	inline float* get_address_of_PhysicalTargetHeight_3() { return &___PhysicalTargetHeight_3; }
	inline void set_PhysicalTargetHeight_3(float value)
	{
		___PhysicalTargetHeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wikitude.UnityEditorSimulator/SimulatedTarget
struct SimulatedTarget_t1564170948_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___UniqueId_1;
	Vector3_t3722313464  ___Offset_2;
	float ___PhysicalTargetHeight_3;
};
// Native definition for COM marshalling of Wikitude.UnityEditorSimulator/SimulatedTarget
struct SimulatedTarget_t1564170948_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___UniqueId_1;
	Vector3_t3722313464  ___Offset_2;
	float ___PhysicalTargetHeight_3;
};
#endif // SIMULATEDTARGET_T1564170948_H
#ifndef EXTENDEDTRACKINGQUALITY_T364496425_H
#define EXTENDEDTRACKINGQUALITY_T364496425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ExtendedTrackingQuality
struct  ExtendedTrackingQuality_t364496425 
{
public:
	// System.Int32 Wikitude.ExtendedTrackingQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ExtendedTrackingQuality_t364496425, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDTRACKINGQUALITY_T364496425_H
#ifndef ONEXTENDEDTRACKINGQUALITYCHANGEDEVENT_T2560195974_H
#define ONEXTENDEDTRACKINGQUALITYCHANGEDEVENT_T2560195974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTracker/OnExtendedTrackingQualityChangedEvent
struct  OnExtendedTrackingQualityChangedEvent_t2560195974  : public UnityEvent_2_t631475969
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEXTENDEDTRACKINGQUALITYCHANGEDEVENT_T2560195974_H
#ifndef _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#define _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._HorizontalAlignmentOptions
struct  _HorizontalAlignmentOptions_t2379014378 
{
public:
	// System.Int32 TMPro._HorizontalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_HorizontalAlignmentOptions_t2379014378, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef IMAGERECOGNITIONRANGEEXTENSION_T604843042_H
#define IMAGERECOGNITIONRANGEEXTENSION_T604843042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageRecognitionRangeExtension
struct  ImageRecognitionRangeExtension_t604843042 
{
public:
	// System.Int32 Wikitude.ImageRecognitionRangeExtension::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageRecognitionRangeExtension_t604843042, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGERECOGNITIONRANGEEXTENSION_T604843042_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef FONTWEIGHTS_T3122883458_H
#define FONTWEIGHTS_T3122883458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeights
struct  FontWeights_t3122883458 
{
public:
	// System.Int32 TMPro.FontWeights::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontWeights_t3122883458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHTS_T3122883458_H
#ifndef TAGUNITS_T1169424683_H
#define TAGUNITS_T1169424683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnits
struct  TagUnits_t1169424683 
{
public:
	// System.Int32 TMPro.TagUnits::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagUnits_t1169424683, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITS_T1169424683_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef MASKINGOFFSETMODE_T2266644590_H
#define MASKINGOFFSETMODE_T2266644590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2266644590 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2266644590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2266644590_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef CARETPOSITION_T3997512201_H
#define CARETPOSITION_T3997512201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t3997512201 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaretPosition_t3997512201, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T3997512201_H
#ifndef LINESEGMENT_T1526544958_H
#define LINESEGMENT_T1526544958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities/LineSegment
struct  LineSegment_t1526544958 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point1
	Vector3_t3722313464  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point2
	Vector3_t3722313464  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point1_0)); }
	inline Vector3_t3722313464  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t3722313464 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t3722313464  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point2_1)); }
	inline Vector3_t3722313464  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t3722313464 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t3722313464  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T1526544958_H
#ifndef TMP_TEXTINFO_T3598145122_H
#define TMP_TEXTINFO_T3598145122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t3598145122  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t3766301798* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t3558768157* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t4120149533* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t2463031060* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___textComponent_2)); }
	inline TMP_Text_t2599618874 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t2599618874 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t3766301798* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t3766301798** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t3766301798* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t3558768157* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t3558768157** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t3558768157* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t4120149533* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t4120149533** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t4120149533* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_t2463031060* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_t2463031060** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_t2463031060* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_t3598145122_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_t2156229523  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_t2156229523  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_t2156229523  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_t2156229523  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_t2156229523  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_t2156229523  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_T3598145122_H
#ifndef MASKINGTYPES_T3687969768_H
#define MASKINGTYPES_T3687969768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t3687969768 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t3687969768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T3687969768_H
#ifndef TEXTINPUTSOURCES_T1522115805_H
#define TEXTINPUTSOURCES_T1522115805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t1522115805 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t1522115805, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T1522115805_H
#ifndef ONINITIALIZATIONSTARTEDEVENT_T272217273_H
#define ONINITIALIZATIONSTARTEDEVENT_T272217273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackable/OnInitializationStartedEvent
struct  OnInitializationStartedEvent_t272217273  : public UnityEvent_1_t4195527445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINITIALIZATIONSTARTEDEVENT_T272217273_H
#ifndef ONSCREENCONVERSIONCOMPUTEDEVENT_T608500589_H
#define ONSCREENCONVERSIONCOMPUTEDEVENT_T608500589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTracker/OnScreenConversionComputedEvent
struct  OnScreenConversionComputedEvent_t608500589  : public UnityEvent_3_t1153900776
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCREENCONVERSIONCOMPUTEDEVENT_T608500589_H
#ifndef ONPLUGINFAILUREEVENT_T2376488508_H
#define ONPLUGINFAILUREEVENT_T2376488508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PluginManager/OnPluginFailureEvent
struct  OnPluginFailureEvent_t2376488508  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPLUGINFAILUREEVENT_T2376488508_H
#ifndef ONSTATECHANGEDEVENT_T1359267737_H
#define ONSTATECHANGEDEVENT_T1359267737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTracker/OnStateChangedEvent
struct  OnStateChangedEvent_t1359267737  : public UnityEvent_1_t560102991
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATECHANGEDEVENT_T1359267737_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef ONOBJECTLOSTEVENT_T460465675_H
#define ONOBJECTLOSTEVENT_T460465675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTrackable/OnObjectLostEvent
struct  OnObjectLostEvent_t460465675  : public UnityEvent_1_t1569884548
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONOBJECTLOSTEVENT_T460465675_H
#ifndef ONOBJECTRECOGNIZEDEVENT_T3443511796_H
#define ONOBJECTRECOGNIZEDEVENT_T3443511796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTrackable/OnObjectRecognizedEvent
struct  OnObjectRecognizedEvent_t3443511796  : public UnityEvent_1_t1569884548
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONOBJECTRECOGNIZEDEVENT_T3443511796_H
#ifndef ONCAMERAFRAMEAVAILABLEEVENT_T3843633858_H
#define ONCAMERAFRAMEAVAILABLEEVENT_T3843633858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PluginManager/OnCameraFrameAvailableEvent
struct  OnCameraFrameAvailableEvent_t3843633858  : public UnityEvent_1_t1944436728
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCAMERAFRAMEAVAILABLEEVENT_T3843633858_H
#ifndef SPRITEDATA_T3048397587_H
#define SPRITEDATA_T3048397587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct  SpriteData_t3048397587 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::frame
	SpriteFrame_t3912389194  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::spriteSourceSize
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::sourceSize
	SpriteSize_t3355290999  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::pivot
	Vector2_t2156229523  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___frame_1)); }
	inline SpriteFrame_t3912389194  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_t3912389194 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_t3912389194  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___spriteSourceSize_4)); }
	inline SpriteFrame_t3912389194  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_t3912389194 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_t3912389194  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___sourceSize_5)); }
	inline SpriteSize_t3355290999  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t3355290999 * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t3355290999  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___pivot_6)); }
	inline Vector2_t2156229523  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_t2156229523 * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_t2156229523  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
#endif // SPRITEDATA_T3048397587_H
#ifndef SPRITEASSETIMPORTFORMATS_T116390639_H
#define SPRITEASSETIMPORTFORMATS_T116390639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t116390639 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t116390639, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T116390639_H
#ifndef ONSCENELOSTEVENT_T389101256_H
#define ONSCENELOSTEVENT_T389101256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackable/OnSceneLostEvent
struct  OnSceneLostEvent_t389101256  : public UnityEvent_1_t4195527445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCENELOSTEVENT_T389101256_H
#ifndef _VERTICALALIGNMENTOPTIONS_T2825528260_H
#define _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._VerticalAlignmentOptions
struct  _VerticalAlignmentOptions_t2825528260 
{
public:
	// System.Int32 TMPro._VerticalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_VerticalAlignmentOptions_t2825528260, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifndef ONINITIALIZATIONSTOPPEDEVENT_T4207911327_H
#define ONINITIALIZATIONSTOPPEDEVENT_T4207911327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackable/OnInitializationStoppedEvent
struct  OnInitializationStoppedEvent_t4207911327  : public UnityEvent_1_t4195527445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINITIALIZATIONSTOPPEDEVENT_T4207911327_H
#ifndef ONSCENERECOGNIZEDEVENT_T2416994635_H
#define ONSCENERECOGNIZEDEVENT_T2416994635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackable/OnSceneRecognizedEvent
struct  OnSceneRecognizedEvent_t2416994635  : public UnityEvent_1_t4195527445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCENERECOGNIZEDEVENT_T2416994635_H
#ifndef INSTANTTRACKINGSTATE_T3973410783_H
#define INSTANTTRACKINGSTATE_T3973410783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackingState
struct  InstantTrackingState_t3973410783 
{
public:
	// System.Int32 Wikitude.InstantTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstantTrackingState_t3973410783, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTTRACKINGSTATE_T3973410783_H
#ifndef INSTANTTRACKINGPLANEORIENTATION_T1439566444_H
#define INSTANTTRACKINGPLANEORIENTATION_T1439566444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackingPlaneOrientation
struct  InstantTrackingPlaneOrientation_t1439566444 
{
public:
	// System.Int32 Wikitude.InstantTrackingPlaneOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstantTrackingPlaneOrientation_t1439566444, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTTRACKINGPLANEORIENTATION_T1439566444_H
#ifndef ONERRORLOADINGTARGETSEVENT_T394526247_H
#define ONERRORLOADINGTARGETSEVENT_T394526247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerBehaviour/OnErrorLoadingTargetsEvent
struct  OnErrorLoadingTargetsEvent_t394526247  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORLOADINGTARGETSEVENT_T394526247_H
#ifndef ONTARGETSLOADEDEVENT_T1820866191_H
#define ONTARGETSLOADEDEVENT_T1820866191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerBehaviour/OnTargetsLoadedEvent
struct  OnTargetsLoadedEvent_t1820866191  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTARGETSLOADEDEVENT_T1820866191_H
#ifndef CARETINFO_T841780893_H
#define CARETINFO_T841780893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_t841780893 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_T841780893_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_18)); }
	inline Extents_t3837212874  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3837212874 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3837212874  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef NULLABLE_1_T1062718478_H
#define NULLABLE_1_T1062718478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Wikitude.CaptureDevicePosition>
struct  Nullable_1_t1062718478 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1062718478, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1062718478, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1062718478_H
#ifndef CLOUDRECOGNITIONSERVICE_T3866570042_H
#define CLOUDRECOGNITIONSERVICE_T3866570042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService
struct  CloudRecognitionService_t3866570042  : public TargetSource_t2046190744
{
public:
	// System.String Wikitude.CloudRecognitionService::_clientToken
	String_t* ____clientToken_2;
	// System.String Wikitude.CloudRecognitionService::_targetCollectionId
	String_t* ____targetCollectionId_3;
	// Wikitude.TrackerManager/CloudRecognitionServerRegion Wikitude.CloudRecognitionService::_serverRegion
	int32_t ____serverRegion_4;
	// System.String Wikitude.CloudRecognitionService::_customServerURL
	String_t* ____customServerURL_5;
	// Wikitude.CloudRecognitionService/OnInitializedEvent Wikitude.CloudRecognitionService::OnInitialized
	OnInitializedEvent_t1242699385 * ___OnInitialized_6;
	// Wikitude.CloudRecognitionService/OnInitializationErrorEvent Wikitude.CloudRecognitionService::OnInitializationError
	OnInitializationErrorEvent_t511199451 * ___OnInitializationError_7;
	// Wikitude.CloudRecognitionService/OnRecognitionResponseEvent Wikitude.CloudRecognitionService::OnRecognitionResponse
	OnRecognitionResponseEvent_t3712700845 * ___OnRecognitionResponse_8;
	// Wikitude.CloudRecognitionService/OnRecognitionErrorEvent Wikitude.CloudRecognitionService::OnRecognitionError
	OnRecognitionErrorEvent_t2212464566 * ___OnRecognitionError_9;
	// Wikitude.CloudRecognitionService/OnInterruptionEvent Wikitude.CloudRecognitionService::OnInterruption
	OnInterruptionEvent_t1530367979 * ___OnInterruption_10;
	// System.Boolean Wikitude.CloudRecognitionService::<IsContinuousRecognitionRunning>k__BackingField
	bool ___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__clientToken_2() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____clientToken_2)); }
	inline String_t* get__clientToken_2() const { return ____clientToken_2; }
	inline String_t** get_address_of__clientToken_2() { return &____clientToken_2; }
	inline void set__clientToken_2(String_t* value)
	{
		____clientToken_2 = value;
		Il2CppCodeGenWriteBarrier((&____clientToken_2), value);
	}

	inline static int32_t get_offset_of__targetCollectionId_3() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____targetCollectionId_3)); }
	inline String_t* get__targetCollectionId_3() const { return ____targetCollectionId_3; }
	inline String_t** get_address_of__targetCollectionId_3() { return &____targetCollectionId_3; }
	inline void set__targetCollectionId_3(String_t* value)
	{
		____targetCollectionId_3 = value;
		Il2CppCodeGenWriteBarrier((&____targetCollectionId_3), value);
	}

	inline static int32_t get_offset_of__serverRegion_4() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____serverRegion_4)); }
	inline int32_t get__serverRegion_4() const { return ____serverRegion_4; }
	inline int32_t* get_address_of__serverRegion_4() { return &____serverRegion_4; }
	inline void set__serverRegion_4(int32_t value)
	{
		____serverRegion_4 = value;
	}

	inline static int32_t get_offset_of__customServerURL_5() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____customServerURL_5)); }
	inline String_t* get__customServerURL_5() const { return ____customServerURL_5; }
	inline String_t** get_address_of__customServerURL_5() { return &____customServerURL_5; }
	inline void set__customServerURL_5(String_t* value)
	{
		____customServerURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____customServerURL_5), value);
	}

	inline static int32_t get_offset_of_OnInitialized_6() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnInitialized_6)); }
	inline OnInitializedEvent_t1242699385 * get_OnInitialized_6() const { return ___OnInitialized_6; }
	inline OnInitializedEvent_t1242699385 ** get_address_of_OnInitialized_6() { return &___OnInitialized_6; }
	inline void set_OnInitialized_6(OnInitializedEvent_t1242699385 * value)
	{
		___OnInitialized_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnInitialized_6), value);
	}

	inline static int32_t get_offset_of_OnInitializationError_7() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnInitializationError_7)); }
	inline OnInitializationErrorEvent_t511199451 * get_OnInitializationError_7() const { return ___OnInitializationError_7; }
	inline OnInitializationErrorEvent_t511199451 ** get_address_of_OnInitializationError_7() { return &___OnInitializationError_7; }
	inline void set_OnInitializationError_7(OnInitializationErrorEvent_t511199451 * value)
	{
		___OnInitializationError_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnInitializationError_7), value);
	}

	inline static int32_t get_offset_of_OnRecognitionResponse_8() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnRecognitionResponse_8)); }
	inline OnRecognitionResponseEvent_t3712700845 * get_OnRecognitionResponse_8() const { return ___OnRecognitionResponse_8; }
	inline OnRecognitionResponseEvent_t3712700845 ** get_address_of_OnRecognitionResponse_8() { return &___OnRecognitionResponse_8; }
	inline void set_OnRecognitionResponse_8(OnRecognitionResponseEvent_t3712700845 * value)
	{
		___OnRecognitionResponse_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnRecognitionResponse_8), value);
	}

	inline static int32_t get_offset_of_OnRecognitionError_9() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnRecognitionError_9)); }
	inline OnRecognitionErrorEvent_t2212464566 * get_OnRecognitionError_9() const { return ___OnRecognitionError_9; }
	inline OnRecognitionErrorEvent_t2212464566 ** get_address_of_OnRecognitionError_9() { return &___OnRecognitionError_9; }
	inline void set_OnRecognitionError_9(OnRecognitionErrorEvent_t2212464566 * value)
	{
		___OnRecognitionError_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnRecognitionError_9), value);
	}

	inline static int32_t get_offset_of_OnInterruption_10() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnInterruption_10)); }
	inline OnInterruptionEvent_t1530367979 * get_OnInterruption_10() const { return ___OnInterruption_10; }
	inline OnInterruptionEvent_t1530367979 ** get_address_of_OnInterruption_10() { return &___OnInterruption_10; }
	inline void set_OnInterruption_10(OnInterruptionEvent_t1530367979 * value)
	{
		___OnInterruption_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterruption_10), value);
	}

	inline static int32_t get_offset_of_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11)); }
	inline bool get_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11() const { return ___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11() { return &___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11; }
	inline void set_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11(bool value)
	{
		___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOGNITIONSERVICE_T3866570042_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef IOSBRIDGE_T3849146092_H
#define IOSBRIDGE_T3849146092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.iOSBridge
struct  iOSBridge_t3849146092  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Wikitude.iOSBridge::_screenOrientation
	int32_t ____screenOrientation_0;
	// System.Int64[] Wikitude.iOSBridge::_cachedTargetIdBuffer
	Int64U5BU5D_t2559172825* ____cachedTargetIdBuffer_1;
	// System.Single[] Wikitude.iOSBridge::_cachedProjectionMatrix
	SingleU5BU5D_t1444911251* ____cachedProjectionMatrix_2;
	// System.Single[] Wikitude.iOSBridge::_cachedModelViewMatrices
	SingleU5BU5D_t1444911251* ____cachedModelViewMatrices_3;
	// System.IntPtr Wikitude.iOSBridge::_nativeFramePtr
	intptr_t ____nativeFramePtr_4;

public:
	inline static int32_t get_offset_of__screenOrientation_0() { return static_cast<int32_t>(offsetof(iOSBridge_t3849146092, ____screenOrientation_0)); }
	inline int32_t get__screenOrientation_0() const { return ____screenOrientation_0; }
	inline int32_t* get_address_of__screenOrientation_0() { return &____screenOrientation_0; }
	inline void set__screenOrientation_0(int32_t value)
	{
		____screenOrientation_0 = value;
	}

	inline static int32_t get_offset_of__cachedTargetIdBuffer_1() { return static_cast<int32_t>(offsetof(iOSBridge_t3849146092, ____cachedTargetIdBuffer_1)); }
	inline Int64U5BU5D_t2559172825* get__cachedTargetIdBuffer_1() const { return ____cachedTargetIdBuffer_1; }
	inline Int64U5BU5D_t2559172825** get_address_of__cachedTargetIdBuffer_1() { return &____cachedTargetIdBuffer_1; }
	inline void set__cachedTargetIdBuffer_1(Int64U5BU5D_t2559172825* value)
	{
		____cachedTargetIdBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____cachedTargetIdBuffer_1), value);
	}

	inline static int32_t get_offset_of__cachedProjectionMatrix_2() { return static_cast<int32_t>(offsetof(iOSBridge_t3849146092, ____cachedProjectionMatrix_2)); }
	inline SingleU5BU5D_t1444911251* get__cachedProjectionMatrix_2() const { return ____cachedProjectionMatrix_2; }
	inline SingleU5BU5D_t1444911251** get_address_of__cachedProjectionMatrix_2() { return &____cachedProjectionMatrix_2; }
	inline void set__cachedProjectionMatrix_2(SingleU5BU5D_t1444911251* value)
	{
		____cachedProjectionMatrix_2 = value;
		Il2CppCodeGenWriteBarrier((&____cachedProjectionMatrix_2), value);
	}

	inline static int32_t get_offset_of__cachedModelViewMatrices_3() { return static_cast<int32_t>(offsetof(iOSBridge_t3849146092, ____cachedModelViewMatrices_3)); }
	inline SingleU5BU5D_t1444911251* get__cachedModelViewMatrices_3() const { return ____cachedModelViewMatrices_3; }
	inline SingleU5BU5D_t1444911251** get_address_of__cachedModelViewMatrices_3() { return &____cachedModelViewMatrices_3; }
	inline void set__cachedModelViewMatrices_3(SingleU5BU5D_t1444911251* value)
	{
		____cachedModelViewMatrices_3 = value;
		Il2CppCodeGenWriteBarrier((&____cachedModelViewMatrices_3), value);
	}

	inline static int32_t get_offset_of__nativeFramePtr_4() { return static_cast<int32_t>(offsetof(iOSBridge_t3849146092, ____nativeFramePtr_4)); }
	inline intptr_t get__nativeFramePtr_4() const { return ____nativeFramePtr_4; }
	inline intptr_t* get_address_of__nativeFramePtr_4() { return &____nativeFramePtr_4; }
	inline void set__nativeFramePtr_4(intptr_t value)
	{
		____nativeFramePtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSBRIDGE_T3849146092_H
#ifndef FRAME_T1062777224_H
#define FRAME_T1062777224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.Frame
struct  Frame_t1062777224 
{
public:
	// System.IntPtr Wikitude.Frame::Data
	intptr_t ___Data_0;
	// System.Int32 Wikitude.Frame::DataSize
	int32_t ___DataSize_1;
	// System.Int32 Wikitude.Frame::Width
	int32_t ___Width_2;
	// System.Int32 Wikitude.Frame::Height
	int32_t ___Height_3;
	// Wikitude.FrameColorSpace Wikitude.Frame::ColorSpace
	int32_t ___ColorSpace_4;
	// System.Boolean Wikitude.Frame::HasStrides
	bool ___HasStrides_5;
	// Wikitude.FrameStrides Wikitude.Frame::Strides
	FrameStrides_t352336826  ___Strides_6;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___Data_0)); }
	inline intptr_t get_Data_0() const { return ___Data_0; }
	inline intptr_t* get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(intptr_t value)
	{
		___Data_0 = value;
	}

	inline static int32_t get_offset_of_DataSize_1() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___DataSize_1)); }
	inline int32_t get_DataSize_1() const { return ___DataSize_1; }
	inline int32_t* get_address_of_DataSize_1() { return &___DataSize_1; }
	inline void set_DataSize_1(int32_t value)
	{
		___DataSize_1 = value;
	}

	inline static int32_t get_offset_of_Width_2() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___Width_2)); }
	inline int32_t get_Width_2() const { return ___Width_2; }
	inline int32_t* get_address_of_Width_2() { return &___Width_2; }
	inline void set_Width_2(int32_t value)
	{
		___Width_2 = value;
	}

	inline static int32_t get_offset_of_Height_3() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___Height_3)); }
	inline int32_t get_Height_3() const { return ___Height_3; }
	inline int32_t* get_address_of_Height_3() { return &___Height_3; }
	inline void set_Height_3(int32_t value)
	{
		___Height_3 = value;
	}

	inline static int32_t get_offset_of_ColorSpace_4() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___ColorSpace_4)); }
	inline int32_t get_ColorSpace_4() const { return ___ColorSpace_4; }
	inline int32_t* get_address_of_ColorSpace_4() { return &___ColorSpace_4; }
	inline void set_ColorSpace_4(int32_t value)
	{
		___ColorSpace_4 = value;
	}

	inline static int32_t get_offset_of_HasStrides_5() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___HasStrides_5)); }
	inline bool get_HasStrides_5() const { return ___HasStrides_5; }
	inline bool* get_address_of_HasStrides_5() { return &___HasStrides_5; }
	inline void set_HasStrides_5(bool value)
	{
		___HasStrides_5 = value;
	}

	inline static int32_t get_offset_of_Strides_6() { return static_cast<int32_t>(offsetof(Frame_t1062777224, ___Strides_6)); }
	inline FrameStrides_t352336826  get_Strides_6() const { return ___Strides_6; }
	inline FrameStrides_t352336826 * get_address_of_Strides_6() { return &___Strides_6; }
	inline void set_Strides_6(FrameStrides_t352336826  value)
	{
		___Strides_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wikitude.Frame
struct Frame_t1062777224_marshaled_pinvoke
{
	intptr_t ___Data_0;
	int32_t ___DataSize_1;
	int32_t ___Width_2;
	int32_t ___Height_3;
	int32_t ___ColorSpace_4;
	int32_t ___HasStrides_5;
	FrameStrides_t352336826  ___Strides_6;
};
// Native definition for COM marshalling of Wikitude.Frame
struct Frame_t1062777224_marshaled_com
{
	intptr_t ___Data_0;
	int32_t ___DataSize_1;
	int32_t ___Width_2;
	int32_t ___Height_3;
	int32_t ___ColorSpace_4;
	int32_t ___HasStrides_5;
	FrameStrides_t352336826  ___Strides_6;
};
#endif // FRAME_T1062777224_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TMP_STYLESHEET_T917564226_H
#define TMP_STYLESHEET_T917564226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_StyleSheet
struct  TMP_StyleSheet_t917564226  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleList
	List_1_t656488210 * ___m_StyleList_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleDictionary
	Dictionary_2_t2368094095 * ___m_StyleDictionary_4;

public:
	inline static int32_t get_offset_of_m_StyleList_3() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226, ___m_StyleList_3)); }
	inline List_1_t656488210 * get_m_StyleList_3() const { return ___m_StyleList_3; }
	inline List_1_t656488210 ** get_address_of_m_StyleList_3() { return &___m_StyleList_3; }
	inline void set_m_StyleList_3(List_1_t656488210 * value)
	{
		___m_StyleList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleList_3), value);
	}

	inline static int32_t get_offset_of_m_StyleDictionary_4() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226, ___m_StyleDictionary_4)); }
	inline Dictionary_2_t2368094095 * get_m_StyleDictionary_4() const { return ___m_StyleDictionary_4; }
	inline Dictionary_2_t2368094095 ** get_address_of_m_StyleDictionary_4() { return &___m_StyleDictionary_4; }
	inline void set_m_StyleDictionary_4(Dictionary_2_t2368094095 * value)
	{
		___m_StyleDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleDictionary_4), value);
	}
};

struct TMP_StyleSheet_t917564226_StaticFields
{
public:
	// TMPro.TMP_StyleSheet TMPro.TMP_StyleSheet::s_Instance
	TMP_StyleSheet_t917564226 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226_StaticFields, ___s_Instance_2)); }
	inline TMP_StyleSheet_t917564226 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_StyleSheet_t917564226 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_StyleSheet_t917564226 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLESHEET_T917564226_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t3241710312  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t3241710312  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_50)); }
	inline Material_t340375123 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t340375123 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t340375123 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_52)); }
	inline Extents_t3837212874  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3837212874 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3837212874  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TMP_SUBMESH_T2613037997_H
#define TMP_SUBMESH_T2613037997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMesh
struct  TMP_SubMesh_t2613037997  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMesh::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMesh::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_3;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_material
	Material_t340375123 * ___m_material_4;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_5;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackMaterial
	Material_t340375123 * ___m_fallbackMaterial_6;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackSourceMaterial
	Material_t340375123 * ___m_fallbackSourceMaterial_7;
	// System.Boolean TMPro.TMP_SubMesh::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_8;
	// System.Single TMPro.TMP_SubMesh::m_padding
	float ___m_padding_9;
	// UnityEngine.Renderer TMPro.TMP_SubMesh::m_renderer
	Renderer_t2627027031 * ___m_renderer_10;
	// UnityEngine.MeshFilter TMPro.TMP_SubMesh::m_meshFilter
	MeshFilter_t3523625662 * ___m_meshFilter_11;
	// UnityEngine.Mesh TMPro.TMP_SubMesh::m_mesh
	Mesh_t3648964284 * ___m_mesh_12;
	// UnityEngine.BoxCollider TMPro.TMP_SubMesh::m_boxCollider
	BoxCollider_t1640800422 * ___m_boxCollider_13;
	// TMPro.TextMeshPro TMPro.TMP_SubMesh::m_TextComponent
	TextMeshPro_t2393593166 * ___m_TextComponent_14;
	// System.Boolean TMPro.TMP_SubMesh::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_15;

public:
	inline static int32_t get_offset_of_m_fontAsset_2() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fontAsset_2)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_2() const { return ___m_fontAsset_2; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_2() { return &___m_fontAsset_2; }
	inline void set_m_fontAsset_2(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_2), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_3() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_spriteAsset_3)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_3() const { return ___m_spriteAsset_3; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_3() { return &___m_spriteAsset_3; }
	inline void set_m_spriteAsset_3(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_5() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_sharedMaterial_5)); }
	inline Material_t340375123 * get_m_sharedMaterial_5() const { return ___m_sharedMaterial_5; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_5() { return &___m_sharedMaterial_5; }
	inline void set_m_sharedMaterial_5(Material_t340375123 * value)
	{
		___m_sharedMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_5), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fallbackMaterial_6)); }
	inline Material_t340375123 * get_m_fallbackMaterial_6() const { return ___m_fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_m_fallbackMaterial_6() { return &___m_fallbackMaterial_6; }
	inline void set_m_fallbackMaterial_6(Material_t340375123 * value)
	{
		___m_fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_7() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fallbackSourceMaterial_7)); }
	inline Material_t340375123 * get_m_fallbackSourceMaterial_7() const { return ___m_fallbackSourceMaterial_7; }
	inline Material_t340375123 ** get_address_of_m_fallbackSourceMaterial_7() { return &___m_fallbackSourceMaterial_7; }
	inline void set_m_fallbackSourceMaterial_7(Material_t340375123 * value)
	{
		___m_fallbackSourceMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_8() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_isDefaultMaterial_8)); }
	inline bool get_m_isDefaultMaterial_8() const { return ___m_isDefaultMaterial_8; }
	inline bool* get_address_of_m_isDefaultMaterial_8() { return &___m_isDefaultMaterial_8; }
	inline void set_m_isDefaultMaterial_8(bool value)
	{
		___m_isDefaultMaterial_8 = value;
	}

	inline static int32_t get_offset_of_m_padding_9() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_padding_9)); }
	inline float get_m_padding_9() const { return ___m_padding_9; }
	inline float* get_address_of_m_padding_9() { return &___m_padding_9; }
	inline void set_m_padding_9(float value)
	{
		___m_padding_9 = value;
	}

	inline static int32_t get_offset_of_m_renderer_10() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_renderer_10)); }
	inline Renderer_t2627027031 * get_m_renderer_10() const { return ___m_renderer_10; }
	inline Renderer_t2627027031 ** get_address_of_m_renderer_10() { return &___m_renderer_10; }
	inline void set_m_renderer_10(Renderer_t2627027031 * value)
	{
		___m_renderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_10), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_11() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_meshFilter_11)); }
	inline MeshFilter_t3523625662 * get_m_meshFilter_11() const { return ___m_meshFilter_11; }
	inline MeshFilter_t3523625662 ** get_address_of_m_meshFilter_11() { return &___m_meshFilter_11; }
	inline void set_m_meshFilter_11(MeshFilter_t3523625662 * value)
	{
		___m_meshFilter_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_11), value);
	}

	inline static int32_t get_offset_of_m_mesh_12() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_mesh_12)); }
	inline Mesh_t3648964284 * get_m_mesh_12() const { return ___m_mesh_12; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_12() { return &___m_mesh_12; }
	inline void set_m_mesh_12(Mesh_t3648964284 * value)
	{
		___m_mesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_12), value);
	}

	inline static int32_t get_offset_of_m_boxCollider_13() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_boxCollider_13)); }
	inline BoxCollider_t1640800422 * get_m_boxCollider_13() const { return ___m_boxCollider_13; }
	inline BoxCollider_t1640800422 ** get_address_of_m_boxCollider_13() { return &___m_boxCollider_13; }
	inline void set_m_boxCollider_13(BoxCollider_t1640800422 * value)
	{
		___m_boxCollider_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_boxCollider_13), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_14() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_TextComponent_14)); }
	inline TextMeshPro_t2393593166 * get_m_TextComponent_14() const { return ___m_TextComponent_14; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextComponent_14() { return &___m_TextComponent_14; }
	inline void set_m_TextComponent_14(TextMeshPro_t2393593166 * value)
	{
		___m_TextComponent_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_14), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_15() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_isRegisteredForEvents_15)); }
	inline bool get_m_isRegisteredForEvents_15() const { return ___m_isRegisteredForEvents_15; }
	inline bool* get_address_of_m_isRegisteredForEvents_15() { return &___m_isRegisteredForEvents_15; }
	inline void set_m_isRegisteredForEvents_15(bool value)
	{
		___m_isRegisteredForEvents_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESH_T2613037997_H
#ifndef PLUGINMANAGER_T1017991214_H
#define PLUGINMANAGER_T1017991214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PluginManager
struct  PluginManager_t1017991214  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.PluginManager/OnCameraFrameAvailableEvent Wikitude.PluginManager::OnCameraFrameAvailable
	OnCameraFrameAvailableEvent_t3843633858 * ___OnCameraFrameAvailable_2;
	// Wikitude.PluginManager/OnPluginFailureEvent Wikitude.PluginManager::OnPluginFailure
	OnPluginFailureEvent_t2376488508 * ___OnPluginFailure_3;
	// Wikitude.IPlatformBridge Wikitude.PluginManager::_bridge
	RuntimeObject* ____bridge_4;

public:
	inline static int32_t get_offset_of_OnCameraFrameAvailable_2() { return static_cast<int32_t>(offsetof(PluginManager_t1017991214, ___OnCameraFrameAvailable_2)); }
	inline OnCameraFrameAvailableEvent_t3843633858 * get_OnCameraFrameAvailable_2() const { return ___OnCameraFrameAvailable_2; }
	inline OnCameraFrameAvailableEvent_t3843633858 ** get_address_of_OnCameraFrameAvailable_2() { return &___OnCameraFrameAvailable_2; }
	inline void set_OnCameraFrameAvailable_2(OnCameraFrameAvailableEvent_t3843633858 * value)
	{
		___OnCameraFrameAvailable_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnCameraFrameAvailable_2), value);
	}

	inline static int32_t get_offset_of_OnPluginFailure_3() { return static_cast<int32_t>(offsetof(PluginManager_t1017991214, ___OnPluginFailure_3)); }
	inline OnPluginFailureEvent_t2376488508 * get_OnPluginFailure_3() const { return ___OnPluginFailure_3; }
	inline OnPluginFailureEvent_t2376488508 ** get_address_of_OnPluginFailure_3() { return &___OnPluginFailure_3; }
	inline void set_OnPluginFailure_3(OnPluginFailureEvent_t2376488508 * value)
	{
		___OnPluginFailure_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPluginFailure_3), value);
	}

	inline static int32_t get_offset_of__bridge_4() { return static_cast<int32_t>(offsetof(PluginManager_t1017991214, ____bridge_4)); }
	inline RuntimeObject* get__bridge_4() const { return ____bridge_4; }
	inline RuntimeObject** get_address_of__bridge_4() { return &____bridge_4; }
	inline void set__bridge_4(RuntimeObject* value)
	{
		____bridge_4 = value;
		Il2CppCodeGenWriteBarrier((&____bridge_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINMANAGER_T1017991214_H
#ifndef BACKGROUNDCAMERA_T2368011250_H
#define BACKGROUNDCAMERA_T2368011250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.BackgroundCamera
struct  BackgroundCamera_t2368011250  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.WikitudeCamera Wikitude.BackgroundCamera::WikitudeCamera
	WikitudeCamera_t2188881370 * ___WikitudeCamera_2;

public:
	inline static int32_t get_offset_of_WikitudeCamera_2() { return static_cast<int32_t>(offsetof(BackgroundCamera_t2368011250, ___WikitudeCamera_2)); }
	inline WikitudeCamera_t2188881370 * get_WikitudeCamera_2() const { return ___WikitudeCamera_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_WikitudeCamera_2() { return &___WikitudeCamera_2; }
	inline void set_WikitudeCamera_2(WikitudeCamera_t2188881370 * value)
	{
		___WikitudeCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___WikitudeCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDCAMERA_T2368011250_H
#ifndef WIKITUDECAMERA_T2188881370_H
#define WIKITUDECAMERA_T2188881370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeCamera
struct  WikitudeCamera_t2188881370  : public MonoBehaviour_t3962482529
{
public:
	// System.String Wikitude.WikitudeCamera::_WikitudeLicenseKey
	String_t* ____WikitudeLicenseKey_2;
	// UnityEngine.Texture2D Wikitude.WikitudeCamera::_cameraTexture
	Texture2D_t3840446185 * ____cameraTexture_3;
	// System.Nullable`1<Wikitude.CaptureDevicePosition> Wikitude.WikitudeCamera::_cachedDevicePosition
	Nullable_1_t1062718478  ____cachedDevicePosition_4;
	// Wikitude.CaptureFocusMode Wikitude.WikitudeCamera::_cachedFocusMode
	int32_t ____cachedFocusMode_5;
	// Wikitude.CaptureAutoFocusRestriction Wikitude.WikitudeCamera::_cachedAutoFocusRestriction
	int32_t ____cachedAutoFocusRestriction_6;
	// System.Single Wikitude.WikitudeCamera::_cachedZoomLevel
	float ____cachedZoomLevel_7;
	// Wikitude.CaptureFlashMode Wikitude.WikitudeCamera::_cachedFlashMode
	int32_t ____cachedFlashMode_8;
	// Wikitude.CaptureDeviceResolution Wikitude.WikitudeCamera::_desiredDeviceResolution
	int32_t ____desiredDeviceResolution_9;
	// Wikitude.CaptureDeviceFramerate Wikitude.WikitudeCamera::_desiredDeviceFramerate
	int32_t ____desiredDeviceFramerate_10;
	// System.Boolean Wikitude.WikitudeCamera::_enableCamera2
	bool ____enableCamera2_11;
	// System.Boolean Wikitude.WikitudeCamera::_enableCameraRendering
	bool ____enableCameraRendering_12;
	// System.Boolean Wikitude.WikitudeCamera::_staticCamera
	bool ____staticCamera_13;
	// System.Boolean Wikitude.WikitudeCamera::_enableInputPlugin
	bool ____enableInputPlugin_14;
	// System.Boolean Wikitude.WikitudeCamera::_enableMirroring
	bool ____enableMirroring_15;
	// System.Boolean Wikitude.WikitudeCamera::_invertedFrame
	bool ____invertedFrame_16;
	// System.Boolean Wikitude.WikitudeCamera::_mirroredFrame
	bool ____mirroredFrame_17;
	// Wikitude.FrameColorSpace Wikitude.WikitudeCamera::_inputFrameColorSpace
	int32_t ____inputFrameColorSpace_18;
	// System.Single Wikitude.WikitudeCamera::_horizontalAngle
	float ____horizontalAngle_19;
	// System.Int32 Wikitude.WikitudeCamera::_inputFrameWidth
	int32_t ____inputFrameWidth_20;
	// System.Int32 Wikitude.WikitudeCamera::_inputFrameHeight
	int32_t ____inputFrameHeight_21;
	// System.Boolean Wikitude.WikitudeCamera::_requestInputFrameRendering
	bool ____requestInputFrameRendering_22;
	// UnityEngine.Events.UnityEvent Wikitude.WikitudeCamera::OnInputPluginRegistered
	UnityEvent_t2581268647 * ___OnInputPluginRegistered_23;
	// Wikitude.WikitudeCamera/OnInputPluginFailureEvent Wikitude.WikitudeCamera::OnInputPluginFailure
	OnInputPluginFailureEvent_t818001259 * ___OnInputPluginFailure_24;
	// Wikitude.WikitudeCamera/OnCameraFailureEvent Wikitude.WikitudeCamera::OnCameraFailure
	OnCameraFailureEvent_t2345510068 * ___OnCameraFailure_25;
	// System.Boolean Wikitude.WikitudeCamera::_inputPluginRegistered
	bool ____inputPluginRegistered_26;
	// System.Boolean Wikitude.WikitudeCamera::_cameraOpened
	bool ____cameraOpened_27;
	// Wikitude.WikitudeBridge Wikitude.WikitudeCamera::_bridge
	WikitudeBridge_t4110292378 * ____bridge_28;
	// UnityEngine.GameObject Wikitude.WikitudeCamera::_backgroundCamera
	GameObject_t1113636619 * ____backgroundCamera_29;
	// UnityEngine.Color32[] Wikitude.WikitudeCamera::_blackPixels
	Color32U5BU5D_t3850468773* ____blackPixels_30;
	// UnityEngine.Vector3 Wikitude.WikitudeCamera::_calibrationPositionOffset
	Vector3_t3722313464  ____calibrationPositionOffset_31;
	// UnityEngine.Quaternion Wikitude.WikitudeCamera::_calibrationRotationOffset
	Quaternion_t2301928331  ____calibrationRotationOffset_32;
	// System.Boolean Wikitude.WikitudeCamera::_ignoreTrackableScale
	bool ____ignoreTrackableScale_33;
	// Wikitude.TransformOverride Wikitude.WikitudeCamera::ActiveOverride
	TransformOverride_t3828400655 * ___ActiveOverride_34;

public:
	inline static int32_t get_offset_of__WikitudeLicenseKey_2() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____WikitudeLicenseKey_2)); }
	inline String_t* get__WikitudeLicenseKey_2() const { return ____WikitudeLicenseKey_2; }
	inline String_t** get_address_of__WikitudeLicenseKey_2() { return &____WikitudeLicenseKey_2; }
	inline void set__WikitudeLicenseKey_2(String_t* value)
	{
		____WikitudeLicenseKey_2 = value;
		Il2CppCodeGenWriteBarrier((&____WikitudeLicenseKey_2), value);
	}

	inline static int32_t get_offset_of__cameraTexture_3() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cameraTexture_3)); }
	inline Texture2D_t3840446185 * get__cameraTexture_3() const { return ____cameraTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of__cameraTexture_3() { return &____cameraTexture_3; }
	inline void set__cameraTexture_3(Texture2D_t3840446185 * value)
	{
		____cameraTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&____cameraTexture_3), value);
	}

	inline static int32_t get_offset_of__cachedDevicePosition_4() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cachedDevicePosition_4)); }
	inline Nullable_1_t1062718478  get__cachedDevicePosition_4() const { return ____cachedDevicePosition_4; }
	inline Nullable_1_t1062718478 * get_address_of__cachedDevicePosition_4() { return &____cachedDevicePosition_4; }
	inline void set__cachedDevicePosition_4(Nullable_1_t1062718478  value)
	{
		____cachedDevicePosition_4 = value;
	}

	inline static int32_t get_offset_of__cachedFocusMode_5() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cachedFocusMode_5)); }
	inline int32_t get__cachedFocusMode_5() const { return ____cachedFocusMode_5; }
	inline int32_t* get_address_of__cachedFocusMode_5() { return &____cachedFocusMode_5; }
	inline void set__cachedFocusMode_5(int32_t value)
	{
		____cachedFocusMode_5 = value;
	}

	inline static int32_t get_offset_of__cachedAutoFocusRestriction_6() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cachedAutoFocusRestriction_6)); }
	inline int32_t get__cachedAutoFocusRestriction_6() const { return ____cachedAutoFocusRestriction_6; }
	inline int32_t* get_address_of__cachedAutoFocusRestriction_6() { return &____cachedAutoFocusRestriction_6; }
	inline void set__cachedAutoFocusRestriction_6(int32_t value)
	{
		____cachedAutoFocusRestriction_6 = value;
	}

	inline static int32_t get_offset_of__cachedZoomLevel_7() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cachedZoomLevel_7)); }
	inline float get__cachedZoomLevel_7() const { return ____cachedZoomLevel_7; }
	inline float* get_address_of__cachedZoomLevel_7() { return &____cachedZoomLevel_7; }
	inline void set__cachedZoomLevel_7(float value)
	{
		____cachedZoomLevel_7 = value;
	}

	inline static int32_t get_offset_of__cachedFlashMode_8() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cachedFlashMode_8)); }
	inline int32_t get__cachedFlashMode_8() const { return ____cachedFlashMode_8; }
	inline int32_t* get_address_of__cachedFlashMode_8() { return &____cachedFlashMode_8; }
	inline void set__cachedFlashMode_8(int32_t value)
	{
		____cachedFlashMode_8 = value;
	}

	inline static int32_t get_offset_of__desiredDeviceResolution_9() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____desiredDeviceResolution_9)); }
	inline int32_t get__desiredDeviceResolution_9() const { return ____desiredDeviceResolution_9; }
	inline int32_t* get_address_of__desiredDeviceResolution_9() { return &____desiredDeviceResolution_9; }
	inline void set__desiredDeviceResolution_9(int32_t value)
	{
		____desiredDeviceResolution_9 = value;
	}

	inline static int32_t get_offset_of__desiredDeviceFramerate_10() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____desiredDeviceFramerate_10)); }
	inline int32_t get__desiredDeviceFramerate_10() const { return ____desiredDeviceFramerate_10; }
	inline int32_t* get_address_of__desiredDeviceFramerate_10() { return &____desiredDeviceFramerate_10; }
	inline void set__desiredDeviceFramerate_10(int32_t value)
	{
		____desiredDeviceFramerate_10 = value;
	}

	inline static int32_t get_offset_of__enableCamera2_11() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____enableCamera2_11)); }
	inline bool get__enableCamera2_11() const { return ____enableCamera2_11; }
	inline bool* get_address_of__enableCamera2_11() { return &____enableCamera2_11; }
	inline void set__enableCamera2_11(bool value)
	{
		____enableCamera2_11 = value;
	}

	inline static int32_t get_offset_of__enableCameraRendering_12() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____enableCameraRendering_12)); }
	inline bool get__enableCameraRendering_12() const { return ____enableCameraRendering_12; }
	inline bool* get_address_of__enableCameraRendering_12() { return &____enableCameraRendering_12; }
	inline void set__enableCameraRendering_12(bool value)
	{
		____enableCameraRendering_12 = value;
	}

	inline static int32_t get_offset_of__staticCamera_13() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____staticCamera_13)); }
	inline bool get__staticCamera_13() const { return ____staticCamera_13; }
	inline bool* get_address_of__staticCamera_13() { return &____staticCamera_13; }
	inline void set__staticCamera_13(bool value)
	{
		____staticCamera_13 = value;
	}

	inline static int32_t get_offset_of__enableInputPlugin_14() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____enableInputPlugin_14)); }
	inline bool get__enableInputPlugin_14() const { return ____enableInputPlugin_14; }
	inline bool* get_address_of__enableInputPlugin_14() { return &____enableInputPlugin_14; }
	inline void set__enableInputPlugin_14(bool value)
	{
		____enableInputPlugin_14 = value;
	}

	inline static int32_t get_offset_of__enableMirroring_15() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____enableMirroring_15)); }
	inline bool get__enableMirroring_15() const { return ____enableMirroring_15; }
	inline bool* get_address_of__enableMirroring_15() { return &____enableMirroring_15; }
	inline void set__enableMirroring_15(bool value)
	{
		____enableMirroring_15 = value;
	}

	inline static int32_t get_offset_of__invertedFrame_16() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____invertedFrame_16)); }
	inline bool get__invertedFrame_16() const { return ____invertedFrame_16; }
	inline bool* get_address_of__invertedFrame_16() { return &____invertedFrame_16; }
	inline void set__invertedFrame_16(bool value)
	{
		____invertedFrame_16 = value;
	}

	inline static int32_t get_offset_of__mirroredFrame_17() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____mirroredFrame_17)); }
	inline bool get__mirroredFrame_17() const { return ____mirroredFrame_17; }
	inline bool* get_address_of__mirroredFrame_17() { return &____mirroredFrame_17; }
	inline void set__mirroredFrame_17(bool value)
	{
		____mirroredFrame_17 = value;
	}

	inline static int32_t get_offset_of__inputFrameColorSpace_18() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____inputFrameColorSpace_18)); }
	inline int32_t get__inputFrameColorSpace_18() const { return ____inputFrameColorSpace_18; }
	inline int32_t* get_address_of__inputFrameColorSpace_18() { return &____inputFrameColorSpace_18; }
	inline void set__inputFrameColorSpace_18(int32_t value)
	{
		____inputFrameColorSpace_18 = value;
	}

	inline static int32_t get_offset_of__horizontalAngle_19() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____horizontalAngle_19)); }
	inline float get__horizontalAngle_19() const { return ____horizontalAngle_19; }
	inline float* get_address_of__horizontalAngle_19() { return &____horizontalAngle_19; }
	inline void set__horizontalAngle_19(float value)
	{
		____horizontalAngle_19 = value;
	}

	inline static int32_t get_offset_of__inputFrameWidth_20() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____inputFrameWidth_20)); }
	inline int32_t get__inputFrameWidth_20() const { return ____inputFrameWidth_20; }
	inline int32_t* get_address_of__inputFrameWidth_20() { return &____inputFrameWidth_20; }
	inline void set__inputFrameWidth_20(int32_t value)
	{
		____inputFrameWidth_20 = value;
	}

	inline static int32_t get_offset_of__inputFrameHeight_21() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____inputFrameHeight_21)); }
	inline int32_t get__inputFrameHeight_21() const { return ____inputFrameHeight_21; }
	inline int32_t* get_address_of__inputFrameHeight_21() { return &____inputFrameHeight_21; }
	inline void set__inputFrameHeight_21(int32_t value)
	{
		____inputFrameHeight_21 = value;
	}

	inline static int32_t get_offset_of__requestInputFrameRendering_22() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____requestInputFrameRendering_22)); }
	inline bool get__requestInputFrameRendering_22() const { return ____requestInputFrameRendering_22; }
	inline bool* get_address_of__requestInputFrameRendering_22() { return &____requestInputFrameRendering_22; }
	inline void set__requestInputFrameRendering_22(bool value)
	{
		____requestInputFrameRendering_22 = value;
	}

	inline static int32_t get_offset_of_OnInputPluginRegistered_23() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ___OnInputPluginRegistered_23)); }
	inline UnityEvent_t2581268647 * get_OnInputPluginRegistered_23() const { return ___OnInputPluginRegistered_23; }
	inline UnityEvent_t2581268647 ** get_address_of_OnInputPluginRegistered_23() { return &___OnInputPluginRegistered_23; }
	inline void set_OnInputPluginRegistered_23(UnityEvent_t2581268647 * value)
	{
		___OnInputPluginRegistered_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnInputPluginRegistered_23), value);
	}

	inline static int32_t get_offset_of_OnInputPluginFailure_24() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ___OnInputPluginFailure_24)); }
	inline OnInputPluginFailureEvent_t818001259 * get_OnInputPluginFailure_24() const { return ___OnInputPluginFailure_24; }
	inline OnInputPluginFailureEvent_t818001259 ** get_address_of_OnInputPluginFailure_24() { return &___OnInputPluginFailure_24; }
	inline void set_OnInputPluginFailure_24(OnInputPluginFailureEvent_t818001259 * value)
	{
		___OnInputPluginFailure_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnInputPluginFailure_24), value);
	}

	inline static int32_t get_offset_of_OnCameraFailure_25() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ___OnCameraFailure_25)); }
	inline OnCameraFailureEvent_t2345510068 * get_OnCameraFailure_25() const { return ___OnCameraFailure_25; }
	inline OnCameraFailureEvent_t2345510068 ** get_address_of_OnCameraFailure_25() { return &___OnCameraFailure_25; }
	inline void set_OnCameraFailure_25(OnCameraFailureEvent_t2345510068 * value)
	{
		___OnCameraFailure_25 = value;
		Il2CppCodeGenWriteBarrier((&___OnCameraFailure_25), value);
	}

	inline static int32_t get_offset_of__inputPluginRegistered_26() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____inputPluginRegistered_26)); }
	inline bool get__inputPluginRegistered_26() const { return ____inputPluginRegistered_26; }
	inline bool* get_address_of__inputPluginRegistered_26() { return &____inputPluginRegistered_26; }
	inline void set__inputPluginRegistered_26(bool value)
	{
		____inputPluginRegistered_26 = value;
	}

	inline static int32_t get_offset_of__cameraOpened_27() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____cameraOpened_27)); }
	inline bool get__cameraOpened_27() const { return ____cameraOpened_27; }
	inline bool* get_address_of__cameraOpened_27() { return &____cameraOpened_27; }
	inline void set__cameraOpened_27(bool value)
	{
		____cameraOpened_27 = value;
	}

	inline static int32_t get_offset_of__bridge_28() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____bridge_28)); }
	inline WikitudeBridge_t4110292378 * get__bridge_28() const { return ____bridge_28; }
	inline WikitudeBridge_t4110292378 ** get_address_of__bridge_28() { return &____bridge_28; }
	inline void set__bridge_28(WikitudeBridge_t4110292378 * value)
	{
		____bridge_28 = value;
		Il2CppCodeGenWriteBarrier((&____bridge_28), value);
	}

	inline static int32_t get_offset_of__backgroundCamera_29() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____backgroundCamera_29)); }
	inline GameObject_t1113636619 * get__backgroundCamera_29() const { return ____backgroundCamera_29; }
	inline GameObject_t1113636619 ** get_address_of__backgroundCamera_29() { return &____backgroundCamera_29; }
	inline void set__backgroundCamera_29(GameObject_t1113636619 * value)
	{
		____backgroundCamera_29 = value;
		Il2CppCodeGenWriteBarrier((&____backgroundCamera_29), value);
	}

	inline static int32_t get_offset_of__blackPixels_30() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____blackPixels_30)); }
	inline Color32U5BU5D_t3850468773* get__blackPixels_30() const { return ____blackPixels_30; }
	inline Color32U5BU5D_t3850468773** get_address_of__blackPixels_30() { return &____blackPixels_30; }
	inline void set__blackPixels_30(Color32U5BU5D_t3850468773* value)
	{
		____blackPixels_30 = value;
		Il2CppCodeGenWriteBarrier((&____blackPixels_30), value);
	}

	inline static int32_t get_offset_of__calibrationPositionOffset_31() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____calibrationPositionOffset_31)); }
	inline Vector3_t3722313464  get__calibrationPositionOffset_31() const { return ____calibrationPositionOffset_31; }
	inline Vector3_t3722313464 * get_address_of__calibrationPositionOffset_31() { return &____calibrationPositionOffset_31; }
	inline void set__calibrationPositionOffset_31(Vector3_t3722313464  value)
	{
		____calibrationPositionOffset_31 = value;
	}

	inline static int32_t get_offset_of__calibrationRotationOffset_32() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____calibrationRotationOffset_32)); }
	inline Quaternion_t2301928331  get__calibrationRotationOffset_32() const { return ____calibrationRotationOffset_32; }
	inline Quaternion_t2301928331 * get_address_of__calibrationRotationOffset_32() { return &____calibrationRotationOffset_32; }
	inline void set__calibrationRotationOffset_32(Quaternion_t2301928331  value)
	{
		____calibrationRotationOffset_32 = value;
	}

	inline static int32_t get_offset_of__ignoreTrackableScale_33() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ____ignoreTrackableScale_33)); }
	inline bool get__ignoreTrackableScale_33() const { return ____ignoreTrackableScale_33; }
	inline bool* get_address_of__ignoreTrackableScale_33() { return &____ignoreTrackableScale_33; }
	inline void set__ignoreTrackableScale_33(bool value)
	{
		____ignoreTrackableScale_33 = value;
	}

	inline static int32_t get_offset_of_ActiveOverride_34() { return static_cast<int32_t>(offsetof(WikitudeCamera_t2188881370, ___ActiveOverride_34)); }
	inline TransformOverride_t3828400655 * get_ActiveOverride_34() const { return ___ActiveOverride_34; }
	inline TransformOverride_t3828400655 ** get_address_of_ActiveOverride_34() { return &___ActiveOverride_34; }
	inline void set_ActiveOverride_34(TransformOverride_t3828400655 * value)
	{
		___ActiveOverride_34 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveOverride_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIKITUDECAMERA_T2188881370_H
#ifndef TRACKABLE_T347424808_H
#define TRACKABLE_T347424808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.Trackable
struct  Trackable_t347424808  : public MonoBehaviour_t3962482529
{
public:
	// System.String Wikitude.Trackable::_targetPattern
	String_t* ____targetPattern_2;
	// System.Text.RegularExpressions.Regex Wikitude.Trackable::_targetPatternRegex
	Regex_t3657309853 * ____targetPatternRegex_3;
	// UnityEngine.GameObject Wikitude.Trackable::_drawable
	GameObject_t1113636619 * ____drawable_4;
	// System.Boolean Wikitude.Trackable::_autoToggleVisibility
	bool ____autoToggleVisibility_5;
	// System.Boolean Wikitude.Trackable::_registeredToTracker
	bool ____registeredToTracker_6;
	// Wikitude.TrackerBehaviour Wikitude.Trackable::_tracker
	TrackerBehaviour_t921360922 * ____tracker_7;
	// System.Collections.Generic.Dictionary`2<Wikitude.RecognizedTarget,UnityEngine.GameObject> Wikitude.Trackable::_activeDrawables
	Dictionary_2_t2728509719 * ____activeDrawables_8;
	// System.Boolean Wikitude.Trackable::UpdatedTransform
	bool ___UpdatedTransform_9;

public:
	inline static int32_t get_offset_of__targetPattern_2() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____targetPattern_2)); }
	inline String_t* get__targetPattern_2() const { return ____targetPattern_2; }
	inline String_t** get_address_of__targetPattern_2() { return &____targetPattern_2; }
	inline void set__targetPattern_2(String_t* value)
	{
		____targetPattern_2 = value;
		Il2CppCodeGenWriteBarrier((&____targetPattern_2), value);
	}

	inline static int32_t get_offset_of__targetPatternRegex_3() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____targetPatternRegex_3)); }
	inline Regex_t3657309853 * get__targetPatternRegex_3() const { return ____targetPatternRegex_3; }
	inline Regex_t3657309853 ** get_address_of__targetPatternRegex_3() { return &____targetPatternRegex_3; }
	inline void set__targetPatternRegex_3(Regex_t3657309853 * value)
	{
		____targetPatternRegex_3 = value;
		Il2CppCodeGenWriteBarrier((&____targetPatternRegex_3), value);
	}

	inline static int32_t get_offset_of__drawable_4() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____drawable_4)); }
	inline GameObject_t1113636619 * get__drawable_4() const { return ____drawable_4; }
	inline GameObject_t1113636619 ** get_address_of__drawable_4() { return &____drawable_4; }
	inline void set__drawable_4(GameObject_t1113636619 * value)
	{
		____drawable_4 = value;
		Il2CppCodeGenWriteBarrier((&____drawable_4), value);
	}

	inline static int32_t get_offset_of__autoToggleVisibility_5() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____autoToggleVisibility_5)); }
	inline bool get__autoToggleVisibility_5() const { return ____autoToggleVisibility_5; }
	inline bool* get_address_of__autoToggleVisibility_5() { return &____autoToggleVisibility_5; }
	inline void set__autoToggleVisibility_5(bool value)
	{
		____autoToggleVisibility_5 = value;
	}

	inline static int32_t get_offset_of__registeredToTracker_6() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____registeredToTracker_6)); }
	inline bool get__registeredToTracker_6() const { return ____registeredToTracker_6; }
	inline bool* get_address_of__registeredToTracker_6() { return &____registeredToTracker_6; }
	inline void set__registeredToTracker_6(bool value)
	{
		____registeredToTracker_6 = value;
	}

	inline static int32_t get_offset_of__tracker_7() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____tracker_7)); }
	inline TrackerBehaviour_t921360922 * get__tracker_7() const { return ____tracker_7; }
	inline TrackerBehaviour_t921360922 ** get_address_of__tracker_7() { return &____tracker_7; }
	inline void set__tracker_7(TrackerBehaviour_t921360922 * value)
	{
		____tracker_7 = value;
		Il2CppCodeGenWriteBarrier((&____tracker_7), value);
	}

	inline static int32_t get_offset_of__activeDrawables_8() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____activeDrawables_8)); }
	inline Dictionary_2_t2728509719 * get__activeDrawables_8() const { return ____activeDrawables_8; }
	inline Dictionary_2_t2728509719 ** get_address_of__activeDrawables_8() { return &____activeDrawables_8; }
	inline void set__activeDrawables_8(Dictionary_2_t2728509719 * value)
	{
		____activeDrawables_8 = value;
		Il2CppCodeGenWriteBarrier((&____activeDrawables_8), value);
	}

	inline static int32_t get_offset_of_UpdatedTransform_9() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ___UpdatedTransform_9)); }
	inline bool get_UpdatedTransform_9() const { return ___UpdatedTransform_9; }
	inline bool* get_address_of_UpdatedTransform_9() { return &___UpdatedTransform_9; }
	inline void set_UpdatedTransform_9(bool value)
	{
		___UpdatedTransform_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLE_T347424808_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TRACKERMANAGER_T2102804711_H
#define TRACKERMANAGER_T2102804711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerManager
struct  TrackerManager_t2102804711  : public MonoBehaviour_t3962482529
{
public:
	// System.Int64 Wikitude.TrackerManager::_targetCollectionResourceId
	int64_t ____targetCollectionResourceId_2;
	// System.Int64 Wikitude.TrackerManager::_cloudRecognitionServiceId
	int64_t ____cloudRecognitionServiceId_3;
	// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.TargetCollectionResource> Wikitude.TrackerManager::_registeredResources
	Dictionary_2_t1128681183 * ____registeredResources_4;
	// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.CloudRecognitionService> Wikitude.TrackerManager::_registeredCloudRecognitionServices
	Dictionary_2_t634242530 * ____registeredCloudRecognitionServices_5;
	// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget> Wikitude.TrackerManager::_recognizedTargets
	HashSet_1_t2094861142 * ____recognizedTargets_6;
	// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget> Wikitude.TrackerManager::_updatedTargets
	HashSet_1_t2094861142 * ____updatedTargets_7;
	// System.String[] Wikitude.TrackerManager::_cachedSplitString
	StringU5BU5D_t1281789340* ____cachedSplitString_8;
	// Wikitude.IPlatformBridge Wikitude.TrackerManager::_bridge
	RuntimeObject* ____bridge_9;
	// System.Collections.Generic.HashSet`1<Wikitude.TrackerBehaviour> Wikitude.TrackerManager::_registeredTrackers
	HashSet_1_t3781277692 * ____registeredTrackers_10;
	// Wikitude.TrackerBehaviour Wikitude.TrackerManager::_activeTracker
	TrackerBehaviour_t921360922 * ____activeTracker_11;
	// Wikitude.WikitudeCamera Wikitude.TrackerManager::_wikitudeCamera
	WikitudeCamera_t2188881370 * ____wikitudeCamera_12;

public:
	inline static int32_t get_offset_of__targetCollectionResourceId_2() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____targetCollectionResourceId_2)); }
	inline int64_t get__targetCollectionResourceId_2() const { return ____targetCollectionResourceId_2; }
	inline int64_t* get_address_of__targetCollectionResourceId_2() { return &____targetCollectionResourceId_2; }
	inline void set__targetCollectionResourceId_2(int64_t value)
	{
		____targetCollectionResourceId_2 = value;
	}

	inline static int32_t get_offset_of__cloudRecognitionServiceId_3() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____cloudRecognitionServiceId_3)); }
	inline int64_t get__cloudRecognitionServiceId_3() const { return ____cloudRecognitionServiceId_3; }
	inline int64_t* get_address_of__cloudRecognitionServiceId_3() { return &____cloudRecognitionServiceId_3; }
	inline void set__cloudRecognitionServiceId_3(int64_t value)
	{
		____cloudRecognitionServiceId_3 = value;
	}

	inline static int32_t get_offset_of__registeredResources_4() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____registeredResources_4)); }
	inline Dictionary_2_t1128681183 * get__registeredResources_4() const { return ____registeredResources_4; }
	inline Dictionary_2_t1128681183 ** get_address_of__registeredResources_4() { return &____registeredResources_4; }
	inline void set__registeredResources_4(Dictionary_2_t1128681183 * value)
	{
		____registeredResources_4 = value;
		Il2CppCodeGenWriteBarrier((&____registeredResources_4), value);
	}

	inline static int32_t get_offset_of__registeredCloudRecognitionServices_5() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____registeredCloudRecognitionServices_5)); }
	inline Dictionary_2_t634242530 * get__registeredCloudRecognitionServices_5() const { return ____registeredCloudRecognitionServices_5; }
	inline Dictionary_2_t634242530 ** get_address_of__registeredCloudRecognitionServices_5() { return &____registeredCloudRecognitionServices_5; }
	inline void set__registeredCloudRecognitionServices_5(Dictionary_2_t634242530 * value)
	{
		____registeredCloudRecognitionServices_5 = value;
		Il2CppCodeGenWriteBarrier((&____registeredCloudRecognitionServices_5), value);
	}

	inline static int32_t get_offset_of__recognizedTargets_6() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____recognizedTargets_6)); }
	inline HashSet_1_t2094861142 * get__recognizedTargets_6() const { return ____recognizedTargets_6; }
	inline HashSet_1_t2094861142 ** get_address_of__recognizedTargets_6() { return &____recognizedTargets_6; }
	inline void set__recognizedTargets_6(HashSet_1_t2094861142 * value)
	{
		____recognizedTargets_6 = value;
		Il2CppCodeGenWriteBarrier((&____recognizedTargets_6), value);
	}

	inline static int32_t get_offset_of__updatedTargets_7() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____updatedTargets_7)); }
	inline HashSet_1_t2094861142 * get__updatedTargets_7() const { return ____updatedTargets_7; }
	inline HashSet_1_t2094861142 ** get_address_of__updatedTargets_7() { return &____updatedTargets_7; }
	inline void set__updatedTargets_7(HashSet_1_t2094861142 * value)
	{
		____updatedTargets_7 = value;
		Il2CppCodeGenWriteBarrier((&____updatedTargets_7), value);
	}

	inline static int32_t get_offset_of__cachedSplitString_8() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____cachedSplitString_8)); }
	inline StringU5BU5D_t1281789340* get__cachedSplitString_8() const { return ____cachedSplitString_8; }
	inline StringU5BU5D_t1281789340** get_address_of__cachedSplitString_8() { return &____cachedSplitString_8; }
	inline void set__cachedSplitString_8(StringU5BU5D_t1281789340* value)
	{
		____cachedSplitString_8 = value;
		Il2CppCodeGenWriteBarrier((&____cachedSplitString_8), value);
	}

	inline static int32_t get_offset_of__bridge_9() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____bridge_9)); }
	inline RuntimeObject* get__bridge_9() const { return ____bridge_9; }
	inline RuntimeObject** get_address_of__bridge_9() { return &____bridge_9; }
	inline void set__bridge_9(RuntimeObject* value)
	{
		____bridge_9 = value;
		Il2CppCodeGenWriteBarrier((&____bridge_9), value);
	}

	inline static int32_t get_offset_of__registeredTrackers_10() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____registeredTrackers_10)); }
	inline HashSet_1_t3781277692 * get__registeredTrackers_10() const { return ____registeredTrackers_10; }
	inline HashSet_1_t3781277692 ** get_address_of__registeredTrackers_10() { return &____registeredTrackers_10; }
	inline void set__registeredTrackers_10(HashSet_1_t3781277692 * value)
	{
		____registeredTrackers_10 = value;
		Il2CppCodeGenWriteBarrier((&____registeredTrackers_10), value);
	}

	inline static int32_t get_offset_of__activeTracker_11() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____activeTracker_11)); }
	inline TrackerBehaviour_t921360922 * get__activeTracker_11() const { return ____activeTracker_11; }
	inline TrackerBehaviour_t921360922 ** get_address_of__activeTracker_11() { return &____activeTracker_11; }
	inline void set__activeTracker_11(TrackerBehaviour_t921360922 * value)
	{
		____activeTracker_11 = value;
		Il2CppCodeGenWriteBarrier((&____activeTracker_11), value);
	}

	inline static int32_t get_offset_of__wikitudeCamera_12() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____wikitudeCamera_12)); }
	inline WikitudeCamera_t2188881370 * get__wikitudeCamera_12() const { return ____wikitudeCamera_12; }
	inline WikitudeCamera_t2188881370 ** get_address_of__wikitudeCamera_12() { return &____wikitudeCamera_12; }
	inline void set__wikitudeCamera_12(WikitudeCamera_t2188881370 * value)
	{
		____wikitudeCamera_12 = value;
		Il2CppCodeGenWriteBarrier((&____wikitudeCamera_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T2102804711_H
#ifndef TRACKERBEHAVIOUR_T921360922_H
#define TRACKERBEHAVIOUR_T921360922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerBehaviour
struct  TrackerBehaviour_t921360922  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.TrackerManager Wikitude.TrackerBehaviour::_manager
	TrackerManager_t2102804711 * ____manager_2;
	// System.Collections.Generic.Dictionary`2<Wikitude.Trackable,System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>> Wikitude.TrackerBehaviour::_registeredTrackables
	Dictionary_2_t1769459038 * ____registeredTrackables_3;
	// Wikitude.RecognizedTarget Wikitude.TrackerBehaviour::_anchorTarget
	RecognizedTarget_t3529911668 * ____anchorTarget_4;
	// UnityEngine.Vector3 Wikitude.TrackerBehaviour::_anchorPositionOffset
	Vector3_t3722313464  ____anchorPositionOffset_5;
	// UnityEngine.Quaternion Wikitude.TrackerBehaviour::_anchorRotationOffset
	Quaternion_t2301928331  ____anchorRotationOffset_6;
	// System.Collections.Generic.HashSet`1<Wikitude.TrackableBehaviour> Wikitude.TrackerBehaviour::_legacyTrackables
	HashSet_1_t1034764117 * ____legacyTrackables_7;
	// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget> Wikitude.TrackerBehaviour::_cachedTrackedTargets
	HashSet_1_t2094861142 * ____cachedTrackedTargets_10;
	// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget> Wikitude.TrackerBehaviour::_cachedTargetsToAdd
	HashSet_1_t2094861142 * ____cachedTargetsToAdd_11;
	// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget> Wikitude.TrackerBehaviour::_cachedTargetsToRemove
	HashSet_1_t2094861142 * ____cachedTargetsToRemove_12;
	// System.Boolean Wikitude.TrackerBehaviour::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_13;
	// UnityEngine.Camera Wikitude.TrackerBehaviour::_sceneCamera
	Camera_t4157153871 * ____sceneCamera_14;
	// UnityEngine.Vector3 Wikitude.TrackerBehaviour::_initialSceneCameraPosition
	Vector3_t3722313464  ____initialSceneCameraPosition_15;
	// UnityEngine.Quaternion Wikitude.TrackerBehaviour::_initialSceneCameraRotation
	Quaternion_t2301928331  ____initialSceneCameraRotation_16;
	// Wikitude.WikitudeCamera Wikitude.TrackerBehaviour::_wikitudeCamera
	WikitudeCamera_t2188881370 * ____wikitudeCamera_17;
	// Wikitude.TrackerBehaviour/OnTargetsLoadedEvent Wikitude.TrackerBehaviour::OnTargetsLoaded
	OnTargetsLoadedEvent_t1820866191 * ___OnTargetsLoaded_18;
	// Wikitude.TrackerBehaviour/OnErrorLoadingTargetsEvent Wikitude.TrackerBehaviour::OnErrorLoadingTargets
	OnErrorLoadingTargetsEvent_t394526247 * ___OnErrorLoadingTargets_19;

public:
	inline static int32_t get_offset_of__manager_2() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____manager_2)); }
	inline TrackerManager_t2102804711 * get__manager_2() const { return ____manager_2; }
	inline TrackerManager_t2102804711 ** get_address_of__manager_2() { return &____manager_2; }
	inline void set__manager_2(TrackerManager_t2102804711 * value)
	{
		____manager_2 = value;
		Il2CppCodeGenWriteBarrier((&____manager_2), value);
	}

	inline static int32_t get_offset_of__registeredTrackables_3() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____registeredTrackables_3)); }
	inline Dictionary_2_t1769459038 * get__registeredTrackables_3() const { return ____registeredTrackables_3; }
	inline Dictionary_2_t1769459038 ** get_address_of__registeredTrackables_3() { return &____registeredTrackables_3; }
	inline void set__registeredTrackables_3(Dictionary_2_t1769459038 * value)
	{
		____registeredTrackables_3 = value;
		Il2CppCodeGenWriteBarrier((&____registeredTrackables_3), value);
	}

	inline static int32_t get_offset_of__anchorTarget_4() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____anchorTarget_4)); }
	inline RecognizedTarget_t3529911668 * get__anchorTarget_4() const { return ____anchorTarget_4; }
	inline RecognizedTarget_t3529911668 ** get_address_of__anchorTarget_4() { return &____anchorTarget_4; }
	inline void set__anchorTarget_4(RecognizedTarget_t3529911668 * value)
	{
		____anchorTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&____anchorTarget_4), value);
	}

	inline static int32_t get_offset_of__anchorPositionOffset_5() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____anchorPositionOffset_5)); }
	inline Vector3_t3722313464  get__anchorPositionOffset_5() const { return ____anchorPositionOffset_5; }
	inline Vector3_t3722313464 * get_address_of__anchorPositionOffset_5() { return &____anchorPositionOffset_5; }
	inline void set__anchorPositionOffset_5(Vector3_t3722313464  value)
	{
		____anchorPositionOffset_5 = value;
	}

	inline static int32_t get_offset_of__anchorRotationOffset_6() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____anchorRotationOffset_6)); }
	inline Quaternion_t2301928331  get__anchorRotationOffset_6() const { return ____anchorRotationOffset_6; }
	inline Quaternion_t2301928331 * get_address_of__anchorRotationOffset_6() { return &____anchorRotationOffset_6; }
	inline void set__anchorRotationOffset_6(Quaternion_t2301928331  value)
	{
		____anchorRotationOffset_6 = value;
	}

	inline static int32_t get_offset_of__legacyTrackables_7() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____legacyTrackables_7)); }
	inline HashSet_1_t1034764117 * get__legacyTrackables_7() const { return ____legacyTrackables_7; }
	inline HashSet_1_t1034764117 ** get_address_of__legacyTrackables_7() { return &____legacyTrackables_7; }
	inline void set__legacyTrackables_7(HashSet_1_t1034764117 * value)
	{
		____legacyTrackables_7 = value;
		Il2CppCodeGenWriteBarrier((&____legacyTrackables_7), value);
	}

	inline static int32_t get_offset_of__cachedTrackedTargets_10() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____cachedTrackedTargets_10)); }
	inline HashSet_1_t2094861142 * get__cachedTrackedTargets_10() const { return ____cachedTrackedTargets_10; }
	inline HashSet_1_t2094861142 ** get_address_of__cachedTrackedTargets_10() { return &____cachedTrackedTargets_10; }
	inline void set__cachedTrackedTargets_10(HashSet_1_t2094861142 * value)
	{
		____cachedTrackedTargets_10 = value;
		Il2CppCodeGenWriteBarrier((&____cachedTrackedTargets_10), value);
	}

	inline static int32_t get_offset_of__cachedTargetsToAdd_11() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____cachedTargetsToAdd_11)); }
	inline HashSet_1_t2094861142 * get__cachedTargetsToAdd_11() const { return ____cachedTargetsToAdd_11; }
	inline HashSet_1_t2094861142 ** get_address_of__cachedTargetsToAdd_11() { return &____cachedTargetsToAdd_11; }
	inline void set__cachedTargetsToAdd_11(HashSet_1_t2094861142 * value)
	{
		____cachedTargetsToAdd_11 = value;
		Il2CppCodeGenWriteBarrier((&____cachedTargetsToAdd_11), value);
	}

	inline static int32_t get_offset_of__cachedTargetsToRemove_12() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____cachedTargetsToRemove_12)); }
	inline HashSet_1_t2094861142 * get__cachedTargetsToRemove_12() const { return ____cachedTargetsToRemove_12; }
	inline HashSet_1_t2094861142 ** get_address_of__cachedTargetsToRemove_12() { return &____cachedTargetsToRemove_12; }
	inline void set__cachedTargetsToRemove_12(HashSet_1_t2094861142 * value)
	{
		____cachedTargetsToRemove_12 = value;
		Il2CppCodeGenWriteBarrier((&____cachedTargetsToRemove_12), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ___U3CInitializedU3Ek__BackingField_13)); }
	inline bool get_U3CInitializedU3Ek__BackingField_13() const { return ___U3CInitializedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_13() { return &___U3CInitializedU3Ek__BackingField_13; }
	inline void set_U3CInitializedU3Ek__BackingField_13(bool value)
	{
		___U3CInitializedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__sceneCamera_14() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____sceneCamera_14)); }
	inline Camera_t4157153871 * get__sceneCamera_14() const { return ____sceneCamera_14; }
	inline Camera_t4157153871 ** get_address_of__sceneCamera_14() { return &____sceneCamera_14; }
	inline void set__sceneCamera_14(Camera_t4157153871 * value)
	{
		____sceneCamera_14 = value;
		Il2CppCodeGenWriteBarrier((&____sceneCamera_14), value);
	}

	inline static int32_t get_offset_of__initialSceneCameraPosition_15() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____initialSceneCameraPosition_15)); }
	inline Vector3_t3722313464  get__initialSceneCameraPosition_15() const { return ____initialSceneCameraPosition_15; }
	inline Vector3_t3722313464 * get_address_of__initialSceneCameraPosition_15() { return &____initialSceneCameraPosition_15; }
	inline void set__initialSceneCameraPosition_15(Vector3_t3722313464  value)
	{
		____initialSceneCameraPosition_15 = value;
	}

	inline static int32_t get_offset_of__initialSceneCameraRotation_16() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____initialSceneCameraRotation_16)); }
	inline Quaternion_t2301928331  get__initialSceneCameraRotation_16() const { return ____initialSceneCameraRotation_16; }
	inline Quaternion_t2301928331 * get_address_of__initialSceneCameraRotation_16() { return &____initialSceneCameraRotation_16; }
	inline void set__initialSceneCameraRotation_16(Quaternion_t2301928331  value)
	{
		____initialSceneCameraRotation_16 = value;
	}

	inline static int32_t get_offset_of__wikitudeCamera_17() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____wikitudeCamera_17)); }
	inline WikitudeCamera_t2188881370 * get__wikitudeCamera_17() const { return ____wikitudeCamera_17; }
	inline WikitudeCamera_t2188881370 ** get_address_of__wikitudeCamera_17() { return &____wikitudeCamera_17; }
	inline void set__wikitudeCamera_17(WikitudeCamera_t2188881370 * value)
	{
		____wikitudeCamera_17 = value;
		Il2CppCodeGenWriteBarrier((&____wikitudeCamera_17), value);
	}

	inline static int32_t get_offset_of_OnTargetsLoaded_18() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ___OnTargetsLoaded_18)); }
	inline OnTargetsLoadedEvent_t1820866191 * get_OnTargetsLoaded_18() const { return ___OnTargetsLoaded_18; }
	inline OnTargetsLoadedEvent_t1820866191 ** get_address_of_OnTargetsLoaded_18() { return &___OnTargetsLoaded_18; }
	inline void set_OnTargetsLoaded_18(OnTargetsLoadedEvent_t1820866191 * value)
	{
		___OnTargetsLoaded_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetsLoaded_18), value);
	}

	inline static int32_t get_offset_of_OnErrorLoadingTargets_19() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ___OnErrorLoadingTargets_19)); }
	inline OnErrorLoadingTargetsEvent_t394526247 * get_OnErrorLoadingTargets_19() const { return ___OnErrorLoadingTargets_19; }
	inline OnErrorLoadingTargetsEvent_t394526247 ** get_address_of_OnErrorLoadingTargets_19() { return &___OnErrorLoadingTargets_19; }
	inline void set_OnErrorLoadingTargets_19(OnErrorLoadingTargetsEvent_t394526247 * value)
	{
		___OnErrorLoadingTargets_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnErrorLoadingTargets_19), value);
	}
};

struct TrackerBehaviour_t921360922_StaticFields
{
public:
	// UnityEngine.Matrix4x4 Wikitude.TrackerBehaviour::_conversion
	Matrix4x4_t1817901843  ____conversion_8;
	// UnityEngine.Matrix4x4 Wikitude.TrackerBehaviour::_modelMatrix
	Matrix4x4_t1817901843  ____modelMatrix_9;

public:
	inline static int32_t get_offset_of__conversion_8() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922_StaticFields, ____conversion_8)); }
	inline Matrix4x4_t1817901843  get__conversion_8() const { return ____conversion_8; }
	inline Matrix4x4_t1817901843 * get_address_of__conversion_8() { return &____conversion_8; }
	inline void set__conversion_8(Matrix4x4_t1817901843  value)
	{
		____conversion_8 = value;
	}

	inline static int32_t get_offset_of__modelMatrix_9() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922_StaticFields, ____modelMatrix_9)); }
	inline Matrix4x4_t1817901843  get__modelMatrix_9() const { return ____modelMatrix_9; }
	inline Matrix4x4_t1817901843 * get_address_of__modelMatrix_9() { return &____modelMatrix_9; }
	inline void set__modelMatrix_9(Matrix4x4_t1817901843  value)
	{
		____modelMatrix_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERBEHAVIOUR_T921360922_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef OBJECTTRACKER_T3006704158_H
#define OBJECTTRACKER_T3006704158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTracker
struct  ObjectTracker_t3006704158  : public TrackerBehaviour_t921360922
{
public:
	// Wikitude.TargetCollectionResource Wikitude.ObjectTracker::_targetCollectionResource
	TargetCollectionResource_t66041399 * ____targetCollectionResource_20;
	// System.Boolean Wikitude.ObjectTracker::<IsRegistered>k__BackingField
	bool ___U3CIsRegisteredU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of__targetCollectionResource_20() { return static_cast<int32_t>(offsetof(ObjectTracker_t3006704158, ____targetCollectionResource_20)); }
	inline TargetCollectionResource_t66041399 * get__targetCollectionResource_20() const { return ____targetCollectionResource_20; }
	inline TargetCollectionResource_t66041399 ** get_address_of__targetCollectionResource_20() { return &____targetCollectionResource_20; }
	inline void set__targetCollectionResource_20(TargetCollectionResource_t66041399 * value)
	{
		____targetCollectionResource_20 = value;
		Il2CppCodeGenWriteBarrier((&____targetCollectionResource_20), value);
	}

	inline static int32_t get_offset_of_U3CIsRegisteredU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ObjectTracker_t3006704158, ___U3CIsRegisteredU3Ek__BackingField_21)); }
	inline bool get_U3CIsRegisteredU3Ek__BackingField_21() const { return ___U3CIsRegisteredU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CIsRegisteredU3Ek__BackingField_21() { return &___U3CIsRegisteredU3Ek__BackingField_21; }
	inline void set_U3CIsRegisteredU3Ek__BackingField_21(bool value)
	{
		___U3CIsRegisteredU3Ek__BackingField_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_T3006704158_H
#ifndef INSTANTTRACKABLE_T3187174337_H
#define INSTANTTRACKABLE_T3187174337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackable
struct  InstantTrackable_t3187174337  : public Trackable_t347424808
{
public:
	// Wikitude.InstantTrackable/OnInitializationStartedEvent Wikitude.InstantTrackable::OnInitializationStarted
	OnInitializationStartedEvent_t272217273 * ___OnInitializationStarted_10;
	// Wikitude.InstantTrackable/OnInitializationStoppedEvent Wikitude.InstantTrackable::OnInitializationStopped
	OnInitializationStoppedEvent_t4207911327 * ___OnInitializationStopped_11;
	// Wikitude.InstantTrackable/OnSceneRecognizedEvent Wikitude.InstantTrackable::OnSceneRecognized
	OnSceneRecognizedEvent_t2416994635 * ___OnSceneRecognized_12;
	// Wikitude.InstantTrackable/OnSceneLostEvent Wikitude.InstantTrackable::OnSceneLost
	OnSceneLostEvent_t389101256 * ___OnSceneLost_13;

public:
	inline static int32_t get_offset_of_OnInitializationStarted_10() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnInitializationStarted_10)); }
	inline OnInitializationStartedEvent_t272217273 * get_OnInitializationStarted_10() const { return ___OnInitializationStarted_10; }
	inline OnInitializationStartedEvent_t272217273 ** get_address_of_OnInitializationStarted_10() { return &___OnInitializationStarted_10; }
	inline void set_OnInitializationStarted_10(OnInitializationStartedEvent_t272217273 * value)
	{
		___OnInitializationStarted_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnInitializationStarted_10), value);
	}

	inline static int32_t get_offset_of_OnInitializationStopped_11() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnInitializationStopped_11)); }
	inline OnInitializationStoppedEvent_t4207911327 * get_OnInitializationStopped_11() const { return ___OnInitializationStopped_11; }
	inline OnInitializationStoppedEvent_t4207911327 ** get_address_of_OnInitializationStopped_11() { return &___OnInitializationStopped_11; }
	inline void set_OnInitializationStopped_11(OnInitializationStoppedEvent_t4207911327 * value)
	{
		___OnInitializationStopped_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnInitializationStopped_11), value);
	}

	inline static int32_t get_offset_of_OnSceneRecognized_12() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnSceneRecognized_12)); }
	inline OnSceneRecognizedEvent_t2416994635 * get_OnSceneRecognized_12() const { return ___OnSceneRecognized_12; }
	inline OnSceneRecognizedEvent_t2416994635 ** get_address_of_OnSceneRecognized_12() { return &___OnSceneRecognized_12; }
	inline void set_OnSceneRecognized_12(OnSceneRecognizedEvent_t2416994635 * value)
	{
		___OnSceneRecognized_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnSceneRecognized_12), value);
	}

	inline static int32_t get_offset_of_OnSceneLost_13() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnSceneLost_13)); }
	inline OnSceneLostEvent_t389101256 * get_OnSceneLost_13() const { return ___OnSceneLost_13; }
	inline OnSceneLostEvent_t389101256 ** get_address_of_OnSceneLost_13() { return &___OnSceneLost_13; }
	inline void set_OnSceneLost_13(OnSceneLostEvent_t389101256 * value)
	{
		___OnSceneLost_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnSceneLost_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTTRACKABLE_T3187174337_H
#ifndef IMAGETRACKER_T271395264_H
#define IMAGETRACKER_T271395264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTracker
struct  ImageTracker_t271395264  : public TrackerBehaviour_t921360922
{
public:
	// Wikitude.ImageTracker/OnExtendedTrackingQualityChangedEvent Wikitude.ImageTracker::OnExtendedTrackingQualityChanged
	OnExtendedTrackingQualityChangedEvent_t2560195974 * ___OnExtendedTrackingQualityChanged_20;
	// Wikitude.TargetSourceType Wikitude.ImageTracker::_targetSourceType
	int32_t ____targetSourceType_21;
	// Wikitude.TargetCollectionResource Wikitude.ImageTracker::_targetCollectionResource
	TargetCollectionResource_t66041399 * ____targetCollectionResource_22;
	// Wikitude.CloudRecognitionService Wikitude.ImageTracker::_cloudRecognitionService
	CloudRecognitionService_t3866570042 * ____cloudRecognitionService_23;
	// System.Int32 Wikitude.ImageTracker::_maximumNumberOfConcurrentTrackableTargets
	int32_t ____maximumNumberOfConcurrentTrackableTargets_24;
	// Wikitude.ImageRecognitionRangeExtension Wikitude.ImageTracker::_rangeExtension
	int32_t ____rangeExtension_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> Wikitude.ImageTracker::<PhysicalTargetImageHeights>k__BackingField
	Dictionary_2_t1182523073 * ___U3CPhysicalTargetImageHeightsU3Ek__BackingField_26;
	// System.Boolean Wikitude.ImageTracker::<IsRegistered>k__BackingField
	bool ___U3CIsRegisteredU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_OnExtendedTrackingQualityChanged_20() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ___OnExtendedTrackingQualityChanged_20)); }
	inline OnExtendedTrackingQualityChangedEvent_t2560195974 * get_OnExtendedTrackingQualityChanged_20() const { return ___OnExtendedTrackingQualityChanged_20; }
	inline OnExtendedTrackingQualityChangedEvent_t2560195974 ** get_address_of_OnExtendedTrackingQualityChanged_20() { return &___OnExtendedTrackingQualityChanged_20; }
	inline void set_OnExtendedTrackingQualityChanged_20(OnExtendedTrackingQualityChangedEvent_t2560195974 * value)
	{
		___OnExtendedTrackingQualityChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnExtendedTrackingQualityChanged_20), value);
	}

	inline static int32_t get_offset_of__targetSourceType_21() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____targetSourceType_21)); }
	inline int32_t get__targetSourceType_21() const { return ____targetSourceType_21; }
	inline int32_t* get_address_of__targetSourceType_21() { return &____targetSourceType_21; }
	inline void set__targetSourceType_21(int32_t value)
	{
		____targetSourceType_21 = value;
	}

	inline static int32_t get_offset_of__targetCollectionResource_22() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____targetCollectionResource_22)); }
	inline TargetCollectionResource_t66041399 * get__targetCollectionResource_22() const { return ____targetCollectionResource_22; }
	inline TargetCollectionResource_t66041399 ** get_address_of__targetCollectionResource_22() { return &____targetCollectionResource_22; }
	inline void set__targetCollectionResource_22(TargetCollectionResource_t66041399 * value)
	{
		____targetCollectionResource_22 = value;
		Il2CppCodeGenWriteBarrier((&____targetCollectionResource_22), value);
	}

	inline static int32_t get_offset_of__cloudRecognitionService_23() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____cloudRecognitionService_23)); }
	inline CloudRecognitionService_t3866570042 * get__cloudRecognitionService_23() const { return ____cloudRecognitionService_23; }
	inline CloudRecognitionService_t3866570042 ** get_address_of__cloudRecognitionService_23() { return &____cloudRecognitionService_23; }
	inline void set__cloudRecognitionService_23(CloudRecognitionService_t3866570042 * value)
	{
		____cloudRecognitionService_23 = value;
		Il2CppCodeGenWriteBarrier((&____cloudRecognitionService_23), value);
	}

	inline static int32_t get_offset_of__maximumNumberOfConcurrentTrackableTargets_24() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____maximumNumberOfConcurrentTrackableTargets_24)); }
	inline int32_t get__maximumNumberOfConcurrentTrackableTargets_24() const { return ____maximumNumberOfConcurrentTrackableTargets_24; }
	inline int32_t* get_address_of__maximumNumberOfConcurrentTrackableTargets_24() { return &____maximumNumberOfConcurrentTrackableTargets_24; }
	inline void set__maximumNumberOfConcurrentTrackableTargets_24(int32_t value)
	{
		____maximumNumberOfConcurrentTrackableTargets_24 = value;
	}

	inline static int32_t get_offset_of__rangeExtension_25() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____rangeExtension_25)); }
	inline int32_t get__rangeExtension_25() const { return ____rangeExtension_25; }
	inline int32_t* get_address_of__rangeExtension_25() { return &____rangeExtension_25; }
	inline void set__rangeExtension_25(int32_t value)
	{
		____rangeExtension_25 = value;
	}

	inline static int32_t get_offset_of_U3CPhysicalTargetImageHeightsU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ___U3CPhysicalTargetImageHeightsU3Ek__BackingField_26)); }
	inline Dictionary_2_t1182523073 * get_U3CPhysicalTargetImageHeightsU3Ek__BackingField_26() const { return ___U3CPhysicalTargetImageHeightsU3Ek__BackingField_26; }
	inline Dictionary_2_t1182523073 ** get_address_of_U3CPhysicalTargetImageHeightsU3Ek__BackingField_26() { return &___U3CPhysicalTargetImageHeightsU3Ek__BackingField_26; }
	inline void set_U3CPhysicalTargetImageHeightsU3Ek__BackingField_26(Dictionary_2_t1182523073 * value)
	{
		___U3CPhysicalTargetImageHeightsU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPhysicalTargetImageHeightsU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CIsRegisteredU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ___U3CIsRegisteredU3Ek__BackingField_27)); }
	inline bool get_U3CIsRegisteredU3Ek__BackingField_27() const { return ___U3CIsRegisteredU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CIsRegisteredU3Ek__BackingField_27() { return &___U3CIsRegisteredU3Ek__BackingField_27; }
	inline void set_U3CIsRegisteredU3Ek__BackingField_27(bool value)
	{
		___U3CIsRegisteredU3Ek__BackingField_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETRACKER_T271395264_H
#ifndef OBJECTTRACKABLE_T3378627079_H
#define OBJECTTRACKABLE_T3378627079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTrackable
struct  ObjectTrackable_t3378627079  : public Trackable_t347424808
{
public:
	// Wikitude.ObjectTrackable/OnObjectRecognizedEvent Wikitude.ObjectTrackable::OnObjectRecognized
	OnObjectRecognizedEvent_t3443511796 * ___OnObjectRecognized_10;
	// Wikitude.ObjectTrackable/OnObjectLostEvent Wikitude.ObjectTrackable::OnObjectLost
	OnObjectLostEvent_t460465675 * ___OnObjectLost_11;
	// Wikitude.MapPointCloud Wikitude.ObjectTrackable::_pointCloud
	MapPointCloud_t955983077 * ____pointCloud_12;
	// WikitudeEditor.PointCloudRenderer Wikitude.ObjectTrackable::_mapRenderer
	PointCloudRenderer_t3664016937 * ____mapRenderer_13;

public:
	inline static int32_t get_offset_of_OnObjectRecognized_10() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ___OnObjectRecognized_10)); }
	inline OnObjectRecognizedEvent_t3443511796 * get_OnObjectRecognized_10() const { return ___OnObjectRecognized_10; }
	inline OnObjectRecognizedEvent_t3443511796 ** get_address_of_OnObjectRecognized_10() { return &___OnObjectRecognized_10; }
	inline void set_OnObjectRecognized_10(OnObjectRecognizedEvent_t3443511796 * value)
	{
		___OnObjectRecognized_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnObjectRecognized_10), value);
	}

	inline static int32_t get_offset_of_OnObjectLost_11() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ___OnObjectLost_11)); }
	inline OnObjectLostEvent_t460465675 * get_OnObjectLost_11() const { return ___OnObjectLost_11; }
	inline OnObjectLostEvent_t460465675 ** get_address_of_OnObjectLost_11() { return &___OnObjectLost_11; }
	inline void set_OnObjectLost_11(OnObjectLostEvent_t460465675 * value)
	{
		___OnObjectLost_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnObjectLost_11), value);
	}

	inline static int32_t get_offset_of__pointCloud_12() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ____pointCloud_12)); }
	inline MapPointCloud_t955983077 * get__pointCloud_12() const { return ____pointCloud_12; }
	inline MapPointCloud_t955983077 ** get_address_of__pointCloud_12() { return &____pointCloud_12; }
	inline void set__pointCloud_12(MapPointCloud_t955983077 * value)
	{
		____pointCloud_12 = value;
		Il2CppCodeGenWriteBarrier((&____pointCloud_12), value);
	}

	inline static int32_t get_offset_of__mapRenderer_13() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ____mapRenderer_13)); }
	inline PointCloudRenderer_t3664016937 * get__mapRenderer_13() const { return ____mapRenderer_13; }
	inline PointCloudRenderer_t3664016937 ** get_address_of__mapRenderer_13() { return &____mapRenderer_13; }
	inline void set__mapRenderer_13(PointCloudRenderer_t3664016937 * value)
	{
		____mapRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((&____mapRenderer_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKABLE_T3378627079_H
#ifndef IMAGETRACKABLE_T2462741734_H
#define IMAGETRACKABLE_T2462741734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable
struct  ImageTrackable_t2462741734  : public Trackable_t347424808
{
public:
	// System.Boolean Wikitude.ImageTrackable::_extendedTracking
	bool ____extendedTracking_10;
	// System.String[] Wikitude.ImageTrackable::_targetsForExtendedTracking
	StringU5BU5D_t1281789340* ____targetsForExtendedTracking_11;
	// Wikitude.ImageTrackable/OnImageRecognizedEvent Wikitude.ImageTrackable::OnImageRecognized
	OnImageRecognizedEvent_t773132985 * ___OnImageRecognized_12;
	// Wikitude.ImageTrackable/OnImageLostEvent Wikitude.ImageTrackable::OnImageLost
	OnImageLostEvent_t966623312 * ___OnImageLost_13;
	// UnityEngine.Texture2D Wikitude.ImageTrackable::<Preview>k__BackingField
	Texture2D_t3840446185 * ___U3CPreviewU3Ek__BackingField_14;
	// System.Single Wikitude.ImageTrackable::<ImageTargetHeight>k__BackingField
	float ___U3CImageTargetHeightU3Ek__BackingField_15;
	// UnityEngine.Material Wikitude.ImageTrackable::_previewMaterial
	Material_t340375123 * ____previewMaterial_16;
	// UnityEngine.Mesh Wikitude.ImageTrackable::_previewMesh
	Mesh_t3648964284 * ____previewMesh_17;
	// Wikitude.WikitudeCamera Wikitude.ImageTrackable::_wikitudeCamera
	WikitudeCamera_t2188881370 * ____wikitudeCamera_18;

public:
	inline static int32_t get_offset_of__extendedTracking_10() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____extendedTracking_10)); }
	inline bool get__extendedTracking_10() const { return ____extendedTracking_10; }
	inline bool* get_address_of__extendedTracking_10() { return &____extendedTracking_10; }
	inline void set__extendedTracking_10(bool value)
	{
		____extendedTracking_10 = value;
	}

	inline static int32_t get_offset_of__targetsForExtendedTracking_11() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____targetsForExtendedTracking_11)); }
	inline StringU5BU5D_t1281789340* get__targetsForExtendedTracking_11() const { return ____targetsForExtendedTracking_11; }
	inline StringU5BU5D_t1281789340** get_address_of__targetsForExtendedTracking_11() { return &____targetsForExtendedTracking_11; }
	inline void set__targetsForExtendedTracking_11(StringU5BU5D_t1281789340* value)
	{
		____targetsForExtendedTracking_11 = value;
		Il2CppCodeGenWriteBarrier((&____targetsForExtendedTracking_11), value);
	}

	inline static int32_t get_offset_of_OnImageRecognized_12() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ___OnImageRecognized_12)); }
	inline OnImageRecognizedEvent_t773132985 * get_OnImageRecognized_12() const { return ___OnImageRecognized_12; }
	inline OnImageRecognizedEvent_t773132985 ** get_address_of_OnImageRecognized_12() { return &___OnImageRecognized_12; }
	inline void set_OnImageRecognized_12(OnImageRecognizedEvent_t773132985 * value)
	{
		___OnImageRecognized_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageRecognized_12), value);
	}

	inline static int32_t get_offset_of_OnImageLost_13() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ___OnImageLost_13)); }
	inline OnImageLostEvent_t966623312 * get_OnImageLost_13() const { return ___OnImageLost_13; }
	inline OnImageLostEvent_t966623312 ** get_address_of_OnImageLost_13() { return &___OnImageLost_13; }
	inline void set_OnImageLost_13(OnImageLostEvent_t966623312 * value)
	{
		___OnImageLost_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageLost_13), value);
	}

	inline static int32_t get_offset_of_U3CPreviewU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ___U3CPreviewU3Ek__BackingField_14)); }
	inline Texture2D_t3840446185 * get_U3CPreviewU3Ek__BackingField_14() const { return ___U3CPreviewU3Ek__BackingField_14; }
	inline Texture2D_t3840446185 ** get_address_of_U3CPreviewU3Ek__BackingField_14() { return &___U3CPreviewU3Ek__BackingField_14; }
	inline void set_U3CPreviewU3Ek__BackingField_14(Texture2D_t3840446185 * value)
	{
		___U3CPreviewU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviewU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CImageTargetHeightU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ___U3CImageTargetHeightU3Ek__BackingField_15)); }
	inline float get_U3CImageTargetHeightU3Ek__BackingField_15() const { return ___U3CImageTargetHeightU3Ek__BackingField_15; }
	inline float* get_address_of_U3CImageTargetHeightU3Ek__BackingField_15() { return &___U3CImageTargetHeightU3Ek__BackingField_15; }
	inline void set_U3CImageTargetHeightU3Ek__BackingField_15(float value)
	{
		___U3CImageTargetHeightU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of__previewMaterial_16() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____previewMaterial_16)); }
	inline Material_t340375123 * get__previewMaterial_16() const { return ____previewMaterial_16; }
	inline Material_t340375123 ** get_address_of__previewMaterial_16() { return &____previewMaterial_16; }
	inline void set__previewMaterial_16(Material_t340375123 * value)
	{
		____previewMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&____previewMaterial_16), value);
	}

	inline static int32_t get_offset_of__previewMesh_17() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____previewMesh_17)); }
	inline Mesh_t3648964284 * get__previewMesh_17() const { return ____previewMesh_17; }
	inline Mesh_t3648964284 ** get_address_of__previewMesh_17() { return &____previewMesh_17; }
	inline void set__previewMesh_17(Mesh_t3648964284 * value)
	{
		____previewMesh_17 = value;
		Il2CppCodeGenWriteBarrier((&____previewMesh_17), value);
	}

	inline static int32_t get_offset_of__wikitudeCamera_18() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____wikitudeCamera_18)); }
	inline WikitudeCamera_t2188881370 * get__wikitudeCamera_18() const { return ____wikitudeCamera_18; }
	inline WikitudeCamera_t2188881370 ** get_address_of__wikitudeCamera_18() { return &____wikitudeCamera_18; }
	inline void set__wikitudeCamera_18(WikitudeCamera_t2188881370 * value)
	{
		____wikitudeCamera_18 = value;
		Il2CppCodeGenWriteBarrier((&____wikitudeCamera_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETRACKABLE_T2462741734_H
#ifndef INSTANTTRACKER_T684352120_H
#define INSTANTTRACKER_T684352120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTracker
struct  InstantTracker_t684352120  : public TrackerBehaviour_t921360922
{
public:
	// Wikitude.InstantTracker/OnStateChangedEvent Wikitude.InstantTracker::OnStateChanged
	OnStateChangedEvent_t1359267737 * ___OnStateChanged_20;
	// System.Single Wikitude.InstantTracker::_deviceHeightAboveGround
	float ____deviceHeightAboveGround_21;
	// Wikitude.InstantTracker/OnScreenConversionComputedEvent Wikitude.InstantTracker::OnScreenConversionComputed
	OnScreenConversionComputedEvent_t608500589 * ___OnScreenConversionComputed_22;
	// Wikitude.InstantTrackingPlaneOrientation Wikitude.InstantTracker::_trackingPlaneOrientation
	int32_t ____trackingPlaneOrientation_23;
	// System.Single Wikitude.InstantTracker::_trackingPlaneOrientationAngle
	float ____trackingPlaneOrientationAngle_24;
	// Wikitude.InstantTrackingState Wikitude.InstantTracker::CurrentState
	int32_t ___CurrentState_25;

public:
	inline static int32_t get_offset_of_OnStateChanged_20() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___OnStateChanged_20)); }
	inline OnStateChangedEvent_t1359267737 * get_OnStateChanged_20() const { return ___OnStateChanged_20; }
	inline OnStateChangedEvent_t1359267737 ** get_address_of_OnStateChanged_20() { return &___OnStateChanged_20; }
	inline void set_OnStateChanged_20(OnStateChangedEvent_t1359267737 * value)
	{
		___OnStateChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_20), value);
	}

	inline static int32_t get_offset_of__deviceHeightAboveGround_21() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____deviceHeightAboveGround_21)); }
	inline float get__deviceHeightAboveGround_21() const { return ____deviceHeightAboveGround_21; }
	inline float* get_address_of__deviceHeightAboveGround_21() { return &____deviceHeightAboveGround_21; }
	inline void set__deviceHeightAboveGround_21(float value)
	{
		____deviceHeightAboveGround_21 = value;
	}

	inline static int32_t get_offset_of_OnScreenConversionComputed_22() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___OnScreenConversionComputed_22)); }
	inline OnScreenConversionComputedEvent_t608500589 * get_OnScreenConversionComputed_22() const { return ___OnScreenConversionComputed_22; }
	inline OnScreenConversionComputedEvent_t608500589 ** get_address_of_OnScreenConversionComputed_22() { return &___OnScreenConversionComputed_22; }
	inline void set_OnScreenConversionComputed_22(OnScreenConversionComputedEvent_t608500589 * value)
	{
		___OnScreenConversionComputed_22 = value;
		Il2CppCodeGenWriteBarrier((&___OnScreenConversionComputed_22), value);
	}

	inline static int32_t get_offset_of__trackingPlaneOrientation_23() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____trackingPlaneOrientation_23)); }
	inline int32_t get__trackingPlaneOrientation_23() const { return ____trackingPlaneOrientation_23; }
	inline int32_t* get_address_of__trackingPlaneOrientation_23() { return &____trackingPlaneOrientation_23; }
	inline void set__trackingPlaneOrientation_23(int32_t value)
	{
		____trackingPlaneOrientation_23 = value;
	}

	inline static int32_t get_offset_of__trackingPlaneOrientationAngle_24() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____trackingPlaneOrientationAngle_24)); }
	inline float get__trackingPlaneOrientationAngle_24() const { return ____trackingPlaneOrientationAngle_24; }
	inline float* get_address_of__trackingPlaneOrientationAngle_24() { return &____trackingPlaneOrientationAngle_24; }
	inline void set__trackingPlaneOrientationAngle_24(float value)
	{
		____trackingPlaneOrientationAngle_24 = value;
	}

	inline static int32_t get_offset_of_CurrentState_25() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___CurrentState_25)); }
	inline int32_t get_CurrentState_25() const { return ___CurrentState_25; }
	inline int32_t* get_address_of_CurrentState_25() { return &___CurrentState_25; }
	inline void set_CurrentState_25(int32_t value)
	{
		___CurrentState_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTTRACKER_T684352120_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TMP_TEXT_T2599618874_H
#define TMP_TEXT_T2599618874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t2599618874  : public MaskableGraphic_t3839221559
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_28;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_29;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_30;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t364381626 * ___m_currentFontAsset_31;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_32;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_33;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t340375123 * ___m_currentMaterial_34;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t648826345* ___m_materialReferences_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1839659084 * ___m_materialReferenceIndexLookup_36;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___m_materialReferenceStack_37;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_38;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t561872642* ___m_fontSharedMaterials_39;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t340375123 * ___m_fontMaterial_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t561872642* ___m_fontMaterials_41;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_42;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t2600501292  ___m_fontColor32_43;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2555686324  ___m_fontColor_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t2600501292  ___m_underlineColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t2600501292  ___m_strikethroughColor_47;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t2600501292  ___m_highlightColor_48;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_49;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t345148380  ___m_fontColorGradient_50;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_fontColorGradientPreset_51;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_52;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_53;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t2600501292  ___m_spriteColor_55;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t2600501292  ___m_faceColor_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t2600501292  ___m_outlineColor_58;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_59;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_60;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_61;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_62;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t960921318  ___m_sizeStack_63;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_64;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_65;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___m_fontWeightStack_66;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_67;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_68;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_69;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_70;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_71;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_72;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_73;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___m_fontStyleStack_74;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_75;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_76;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_77;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___m_lineJustificationStack_78;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1718750761* ___m_textContainerLocalCorners_79;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_80;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_81;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_82;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_83;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_84;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_85;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_86;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_88;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_89;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_90;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_91;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_92;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_93;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_94;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_95;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_96;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_97;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_98;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t2599618874 * ___m_linkedTextComponent_99;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_100;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_101;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_102;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_103;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_104;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_105;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_106;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_107;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_108;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_109;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_110;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_111;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_112;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_113;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_114;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_115;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_116;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_117;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_118;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_120;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_121;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_122;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_123;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t3319028937  ___m_margin_124;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_125;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_126;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_127;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_128;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_129;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t3598145122 * ___m_textInfo_130;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_131;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_132;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3600365921 * ___m_transform_133;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_134;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_135;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_136;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t3648964284 * ___m_mesh_137;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_138;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2836635477 * ___m_spriteAnimator_139;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_140;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_141;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_142;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_143;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_144;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_145;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_146;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_147;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_148;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_149;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_150;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_151;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_152;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_153;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_154;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_155;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_156;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_157;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_158;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_159;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_160;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_161;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_162;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_163;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_164;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_165;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3528271667* ___m_htmlTag_166;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t284240280* ___m_xmlAttribute_167;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t1444911251* ___m_attributeParameterValues_168;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_169;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_170;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t960921318  ___m_indentStack_171;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_172;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_173;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t1817901843  ___m_FXMatrix_174;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_175;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t385246372* ___m_char_buffer_176;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___m_internalCharacterInfo_177;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3528271667* ___m_input_CharArray_178;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_179;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_180;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t341939652  ___m_SavedWordWrapState_181;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t341939652  ___m_SavedLineState_182;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_183;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_184;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_185;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_186;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_188;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_189;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_190;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_191;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_192;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_193;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_194;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_195;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_196;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_197;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3837212874  ___m_meshExtents_198;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t2600501292  ___m_htmlColor_199;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t2164155836  ___m_colorStack_200;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_underlineColorStack_201;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_strikethroughColorStack_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_highlightColorStack_203;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_colorGradientPreset_204;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___m_colorGradientStack_205;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_206;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_207;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2514600297  ___m_styleStack_208;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2514600297  ___m_actionStack_209;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_210;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_211;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t960921318  ___m_baselineOffsetStack_212;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_213;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_214;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t129727469 * ___m_cached_TextElement_215;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Underline_GlyphInfo_216;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Ellipsis_GlyphInfo_217;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_218;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_currentSpriteAsset_219;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_220;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_221;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_222;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_223;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t1444911251* ___k_Power_224;

public:
	inline static int32_t get_offset_of_m_text_28() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_text_28)); }
	inline String_t* get_m_text_28() const { return ___m_text_28; }
	inline String_t** get_address_of_m_text_28() { return &___m_text_28; }
	inline void set_m_text_28(String_t* value)
	{
		___m_text_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_28), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_29() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRightToLeft_29)); }
	inline bool get_m_isRightToLeft_29() const { return ___m_isRightToLeft_29; }
	inline bool* get_address_of_m_isRightToLeft_29() { return &___m_isRightToLeft_29; }
	inline void set_m_isRightToLeft_29(bool value)
	{
		___m_isRightToLeft_29 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_30() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontAsset_30)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_30() const { return ___m_fontAsset_30; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_30() { return &___m_fontAsset_30; }
	inline void set_m_fontAsset_30(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_30), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_31() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontAsset_31)); }
	inline TMP_FontAsset_t364381626 * get_m_currentFontAsset_31() const { return ___m_currentFontAsset_31; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_currentFontAsset_31() { return &___m_currentFontAsset_31; }
	inline void set_m_currentFontAsset_31(TMP_FontAsset_t364381626 * value)
	{
		___m_currentFontAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_31), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_32() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isSDFShader_32)); }
	inline bool get_m_isSDFShader_32() const { return ___m_isSDFShader_32; }
	inline bool* get_address_of_m_isSDFShader_32() { return &___m_isSDFShader_32; }
	inline void set_m_isSDFShader_32(bool value)
	{
		___m_isSDFShader_32 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_33() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sharedMaterial_33)); }
	inline Material_t340375123 * get_m_sharedMaterial_33() const { return ___m_sharedMaterial_33; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_33() { return &___m_sharedMaterial_33; }
	inline void set_m_sharedMaterial_33(Material_t340375123 * value)
	{
		___m_sharedMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_34() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterial_34)); }
	inline Material_t340375123 * get_m_currentMaterial_34() const { return ___m_currentMaterial_34; }
	inline Material_t340375123 ** get_address_of_m_currentMaterial_34() { return &___m_currentMaterial_34; }
	inline void set_m_currentMaterial_34(Material_t340375123 * value)
	{
		___m_currentMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_34), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_35() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferences_35)); }
	inline MaterialReferenceU5BU5D_t648826345* get_m_materialReferences_35() const { return ___m_materialReferences_35; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_m_materialReferences_35() { return &___m_materialReferences_35; }
	inline void set_m_materialReferences_35(MaterialReferenceU5BU5D_t648826345* value)
	{
		___m_materialReferences_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_35), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_36() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceIndexLookup_36)); }
	inline Dictionary_2_t1839659084 * get_m_materialReferenceIndexLookup_36() const { return ___m_materialReferenceIndexLookup_36; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_materialReferenceIndexLookup_36() { return &___m_materialReferenceIndexLookup_36; }
	inline void set_m_materialReferenceIndexLookup_36(Dictionary_2_t1839659084 * value)
	{
		___m_materialReferenceIndexLookup_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_37() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceStack_37)); }
	inline TMP_XmlTagStack_1_t1515999176  get_m_materialReferenceStack_37() const { return ___m_materialReferenceStack_37; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_m_materialReferenceStack_37() { return &___m_materialReferenceStack_37; }
	inline void set_m_materialReferenceStack_37(TMP_XmlTagStack_1_t1515999176  value)
	{
		___m_materialReferenceStack_37 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_38() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterialIndex_38)); }
	inline int32_t get_m_currentMaterialIndex_38() const { return ___m_currentMaterialIndex_38; }
	inline int32_t* get_address_of_m_currentMaterialIndex_38() { return &___m_currentMaterialIndex_38; }
	inline void set_m_currentMaterialIndex_38(int32_t value)
	{
		___m_currentMaterialIndex_38 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_39() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSharedMaterials_39)); }
	inline MaterialU5BU5D_t561872642* get_m_fontSharedMaterials_39() const { return ___m_fontSharedMaterials_39; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontSharedMaterials_39() { return &___m_fontSharedMaterials_39; }
	inline void set_m_fontSharedMaterials_39(MaterialU5BU5D_t561872642* value)
	{
		___m_fontSharedMaterials_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_39), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_40() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterial_40)); }
	inline Material_t340375123 * get_m_fontMaterial_40() const { return ___m_fontMaterial_40; }
	inline Material_t340375123 ** get_address_of_m_fontMaterial_40() { return &___m_fontMaterial_40; }
	inline void set_m_fontMaterial_40(Material_t340375123 * value)
	{
		___m_fontMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_40), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterials_41)); }
	inline MaterialU5BU5D_t561872642* get_m_fontMaterials_41() const { return ___m_fontMaterials_41; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontMaterials_41() { return &___m_fontMaterials_41; }
	inline void set_m_fontMaterials_41(MaterialU5BU5D_t561872642* value)
	{
		___m_fontMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_42() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isMaterialDirty_42)); }
	inline bool get_m_isMaterialDirty_42() const { return ___m_isMaterialDirty_42; }
	inline bool* get_address_of_m_isMaterialDirty_42() { return &___m_isMaterialDirty_42; }
	inline void set_m_isMaterialDirty_42(bool value)
	{
		___m_isMaterialDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_43() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor32_43)); }
	inline Color32_t2600501292  get_m_fontColor32_43() const { return ___m_fontColor32_43; }
	inline Color32_t2600501292 * get_address_of_m_fontColor32_43() { return &___m_fontColor32_43; }
	inline void set_m_fontColor32_43(Color32_t2600501292  value)
	{
		___m_fontColor32_43 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_44() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor_44)); }
	inline Color_t2555686324  get_m_fontColor_44() const { return ___m_fontColor_44; }
	inline Color_t2555686324 * get_address_of_m_fontColor_44() { return &___m_fontColor_44; }
	inline void set_m_fontColor_44(Color_t2555686324  value)
	{
		___m_fontColor_44 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColor_46)); }
	inline Color32_t2600501292  get_m_underlineColor_46() const { return ___m_underlineColor_46; }
	inline Color32_t2600501292 * get_address_of_m_underlineColor_46() { return &___m_underlineColor_46; }
	inline void set_m_underlineColor_46(Color32_t2600501292  value)
	{
		___m_underlineColor_46 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_47() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColor_47)); }
	inline Color32_t2600501292  get_m_strikethroughColor_47() const { return ___m_strikethroughColor_47; }
	inline Color32_t2600501292 * get_address_of_m_strikethroughColor_47() { return &___m_strikethroughColor_47; }
	inline void set_m_strikethroughColor_47(Color32_t2600501292  value)
	{
		___m_strikethroughColor_47 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColor_48)); }
	inline Color32_t2600501292  get_m_highlightColor_48() const { return ___m_highlightColor_48; }
	inline Color32_t2600501292 * get_address_of_m_highlightColor_48() { return &___m_highlightColor_48; }
	inline void set_m_highlightColor_48(Color32_t2600501292  value)
	{
		___m_highlightColor_48 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_49() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableVertexGradient_49)); }
	inline bool get_m_enableVertexGradient_49() const { return ___m_enableVertexGradient_49; }
	inline bool* get_address_of_m_enableVertexGradient_49() { return &___m_enableVertexGradient_49; }
	inline void set_m_enableVertexGradient_49(bool value)
	{
		___m_enableVertexGradient_49 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_50() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradient_50)); }
	inline VertexGradient_t345148380  get_m_fontColorGradient_50() const { return ___m_fontColorGradient_50; }
	inline VertexGradient_t345148380 * get_address_of_m_fontColorGradient_50() { return &___m_fontColorGradient_50; }
	inline void set_m_fontColorGradient_50(VertexGradient_t345148380  value)
	{
		___m_fontColorGradient_50 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_51() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradientPreset_51)); }
	inline TMP_ColorGradient_t3678055768 * get_m_fontColorGradientPreset_51() const { return ___m_fontColorGradientPreset_51; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_fontColorGradientPreset_51() { return &___m_fontColorGradientPreset_51; }
	inline void set_m_fontColorGradientPreset_51(TMP_ColorGradient_t3678055768 * value)
	{
		___m_fontColorGradientPreset_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_51), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_52() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAsset_52)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_52() const { return ___m_spriteAsset_52; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_52() { return &___m_spriteAsset_52; }
	inline void set_m_spriteAsset_52(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_52), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_53() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintAllSprites_53)); }
	inline bool get_m_tintAllSprites_53() const { return ___m_tintAllSprites_53; }
	inline bool* get_address_of_m_tintAllSprites_53() { return &___m_tintAllSprites_53; }
	inline void set_m_tintAllSprites_53(bool value)
	{
		___m_tintAllSprites_53 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_54() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintSprite_54)); }
	inline bool get_m_tintSprite_54() const { return ___m_tintSprite_54; }
	inline bool* get_address_of_m_tintSprite_54() { return &___m_tintSprite_54; }
	inline void set_m_tintSprite_54(bool value)
	{
		___m_tintSprite_54 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_55() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteColor_55)); }
	inline Color32_t2600501292  get_m_spriteColor_55() const { return ___m_spriteColor_55; }
	inline Color32_t2600501292 * get_address_of_m_spriteColor_55() { return &___m_spriteColor_55; }
	inline void set_m_spriteColor_55(Color32_t2600501292  value)
	{
		___m_spriteColor_55 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_56() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overrideHtmlColors_56)); }
	inline bool get_m_overrideHtmlColors_56() const { return ___m_overrideHtmlColors_56; }
	inline bool* get_address_of_m_overrideHtmlColors_56() { return &___m_overrideHtmlColors_56; }
	inline void set_m_overrideHtmlColors_56(bool value)
	{
		___m_overrideHtmlColors_56 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_faceColor_57)); }
	inline Color32_t2600501292  get_m_faceColor_57() const { return ___m_faceColor_57; }
	inline Color32_t2600501292 * get_address_of_m_faceColor_57() { return &___m_faceColor_57; }
	inline void set_m_faceColor_57(Color32_t2600501292  value)
	{
		___m_faceColor_57 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_58() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineColor_58)); }
	inline Color32_t2600501292  get_m_outlineColor_58() const { return ___m_outlineColor_58; }
	inline Color32_t2600501292 * get_address_of_m_outlineColor_58() { return &___m_outlineColor_58; }
	inline void set_m_outlineColor_58(Color32_t2600501292  value)
	{
		___m_outlineColor_58 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_59() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineWidth_59)); }
	inline float get_m_outlineWidth_59() const { return ___m_outlineWidth_59; }
	inline float* get_address_of_m_outlineWidth_59() { return &___m_outlineWidth_59; }
	inline void set_m_outlineWidth_59(float value)
	{
		___m_outlineWidth_59 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_60() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSize_60)); }
	inline float get_m_fontSize_60() const { return ___m_fontSize_60; }
	inline float* get_address_of_m_fontSize_60() { return &___m_fontSize_60; }
	inline void set_m_fontSize_60(float value)
	{
		___m_fontSize_60 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_61() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontSize_61)); }
	inline float get_m_currentFontSize_61() const { return ___m_currentFontSize_61; }
	inline float* get_address_of_m_currentFontSize_61() { return &___m_currentFontSize_61; }
	inline void set_m_currentFontSize_61(float value)
	{
		___m_currentFontSize_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_62() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeBase_62)); }
	inline float get_m_fontSizeBase_62() const { return ___m_fontSizeBase_62; }
	inline float* get_address_of_m_fontSizeBase_62() { return &___m_fontSizeBase_62; }
	inline void set_m_fontSizeBase_62(float value)
	{
		___m_fontSizeBase_62 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_63() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sizeStack_63)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_sizeStack_63() const { return ___m_sizeStack_63; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_sizeStack_63() { return &___m_sizeStack_63; }
	inline void set_m_sizeStack_63(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_sizeStack_63 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_64() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeight_64)); }
	inline int32_t get_m_fontWeight_64() const { return ___m_fontWeight_64; }
	inline int32_t* get_address_of_m_fontWeight_64() { return &___m_fontWeight_64; }
	inline void set_m_fontWeight_64(int32_t value)
	{
		___m_fontWeight_64 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_65() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightInternal_65)); }
	inline int32_t get_m_fontWeightInternal_65() const { return ___m_fontWeightInternal_65; }
	inline int32_t* get_address_of_m_fontWeightInternal_65() { return &___m_fontWeightInternal_65; }
	inline void set_m_fontWeightInternal_65(int32_t value)
	{
		___m_fontWeightInternal_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_66() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightStack_66)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_fontWeightStack_66() const { return ___m_fontWeightStack_66; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_fontWeightStack_66() { return &___m_fontWeightStack_66; }
	inline void set_m_fontWeightStack_66(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_fontWeightStack_66 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_67() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableAutoSizing_67)); }
	inline bool get_m_enableAutoSizing_67() const { return ___m_enableAutoSizing_67; }
	inline bool* get_address_of_m_enableAutoSizing_67() { return &___m_enableAutoSizing_67; }
	inline void set_m_enableAutoSizing_67(bool value)
	{
		___m_enableAutoSizing_67 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_68() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxFontSize_68)); }
	inline float get_m_maxFontSize_68() const { return ___m_maxFontSize_68; }
	inline float* get_address_of_m_maxFontSize_68() { return &___m_maxFontSize_68; }
	inline void set_m_maxFontSize_68(float value)
	{
		___m_maxFontSize_68 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_69() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minFontSize_69)); }
	inline float get_m_minFontSize_69() const { return ___m_minFontSize_69; }
	inline float* get_address_of_m_minFontSize_69() { return &___m_minFontSize_69; }
	inline void set_m_minFontSize_69(float value)
	{
		___m_minFontSize_69 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_70() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMin_70)); }
	inline float get_m_fontSizeMin_70() const { return ___m_fontSizeMin_70; }
	inline float* get_address_of_m_fontSizeMin_70() { return &___m_fontSizeMin_70; }
	inline void set_m_fontSizeMin_70(float value)
	{
		___m_fontSizeMin_70 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_71() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMax_71)); }
	inline float get_m_fontSizeMax_71() const { return ___m_fontSizeMax_71; }
	inline float* get_address_of_m_fontSizeMax_71() { return &___m_fontSizeMax_71; }
	inline void set_m_fontSizeMax_71(float value)
	{
		___m_fontSizeMax_71 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_72() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyle_72)); }
	inline int32_t get_m_fontStyle_72() const { return ___m_fontStyle_72; }
	inline int32_t* get_address_of_m_fontStyle_72() { return &___m_fontStyle_72; }
	inline void set_m_fontStyle_72(int32_t value)
	{
		___m_fontStyle_72 = value;
	}

	inline static int32_t get_offset_of_m_style_73() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_style_73)); }
	inline int32_t get_m_style_73() const { return ___m_style_73; }
	inline int32_t* get_address_of_m_style_73() { return &___m_style_73; }
	inline void set_m_style_73(int32_t value)
	{
		___m_style_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_74() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyleStack_74)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_m_fontStyleStack_74() const { return ___m_fontStyleStack_74; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_m_fontStyleStack_74() { return &___m_fontStyleStack_74; }
	inline void set_m_fontStyleStack_74(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___m_fontStyleStack_74 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_75() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingBold_75)); }
	inline bool get_m_isUsingBold_75() const { return ___m_isUsingBold_75; }
	inline bool* get_address_of_m_isUsingBold_75() { return &___m_isUsingBold_75; }
	inline void set_m_isUsingBold_75(bool value)
	{
		___m_isUsingBold_75 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_76() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textAlignment_76)); }
	inline int32_t get_m_textAlignment_76() const { return ___m_textAlignment_76; }
	inline int32_t* get_address_of_m_textAlignment_76() { return &___m_textAlignment_76; }
	inline void set_m_textAlignment_76(int32_t value)
	{
		___m_textAlignment_76 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_77() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustification_77)); }
	inline int32_t get_m_lineJustification_77() const { return ___m_lineJustification_77; }
	inline int32_t* get_address_of_m_lineJustification_77() { return &___m_lineJustification_77; }
	inline void set_m_lineJustification_77(int32_t value)
	{
		___m_lineJustification_77 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustificationStack_78)); }
	inline TMP_XmlTagStack_1_t3600445780  get_m_lineJustificationStack_78() const { return ___m_lineJustificationStack_78; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_m_lineJustificationStack_78() { return &___m_lineJustificationStack_78; }
	inline void set_m_lineJustificationStack_78(TMP_XmlTagStack_1_t3600445780  value)
	{
		___m_lineJustificationStack_78 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_79() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textContainerLocalCorners_79)); }
	inline Vector3U5BU5D_t1718750761* get_m_textContainerLocalCorners_79() const { return ___m_textContainerLocalCorners_79; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_textContainerLocalCorners_79() { return &___m_textContainerLocalCorners_79; }
	inline void set_m_textContainerLocalCorners_79(Vector3U5BU5D_t1718750761* value)
	{
		___m_textContainerLocalCorners_79 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_79), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_80() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAlignmentEnumConverted_80)); }
	inline bool get_m_isAlignmentEnumConverted_80() const { return ___m_isAlignmentEnumConverted_80; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_80() { return &___m_isAlignmentEnumConverted_80; }
	inline void set_m_isAlignmentEnumConverted_80(bool value)
	{
		___m_isAlignmentEnumConverted_80 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_81() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterSpacing_81)); }
	inline float get_m_characterSpacing_81() const { return ___m_characterSpacing_81; }
	inline float* get_address_of_m_characterSpacing_81() { return &___m_characterSpacing_81; }
	inline void set_m_characterSpacing_81(float value)
	{
		___m_characterSpacing_81 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_82() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cSpacing_82)); }
	inline float get_m_cSpacing_82() const { return ___m_cSpacing_82; }
	inline float* get_address_of_m_cSpacing_82() { return &___m_cSpacing_82; }
	inline void set_m_cSpacing_82(float value)
	{
		___m_cSpacing_82 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_monoSpacing_83)); }
	inline float get_m_monoSpacing_83() const { return ___m_monoSpacing_83; }
	inline float* get_address_of_m_monoSpacing_83() { return &___m_monoSpacing_83; }
	inline void set_m_monoSpacing_83(float value)
	{
		___m_monoSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordSpacing_84)); }
	inline float get_m_wordSpacing_84() const { return ___m_wordSpacing_84; }
	inline float* get_address_of_m_wordSpacing_84() { return &___m_wordSpacing_84; }
	inline void set_m_wordSpacing_84(float value)
	{
		___m_wordSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacing_85)); }
	inline float get_m_lineSpacing_85() const { return ___m_lineSpacing_85; }
	inline float* get_address_of_m_lineSpacing_85() { return &___m_lineSpacing_85; }
	inline void set_m_lineSpacing_85(float value)
	{
		___m_lineSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_86() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingDelta_86)); }
	inline float get_m_lineSpacingDelta_86() const { return ___m_lineSpacingDelta_86; }
	inline float* get_address_of_m_lineSpacingDelta_86() { return &___m_lineSpacingDelta_86; }
	inline void set_m_lineSpacingDelta_86(float value)
	{
		___m_lineSpacingDelta_86 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_87() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineHeight_87)); }
	inline float get_m_lineHeight_87() const { return ___m_lineHeight_87; }
	inline float* get_address_of_m_lineHeight_87() { return &___m_lineHeight_87; }
	inline void set_m_lineHeight_87(float value)
	{
		___m_lineHeight_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_88() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingMax_88)); }
	inline float get_m_lineSpacingMax_88() const { return ___m_lineSpacingMax_88; }
	inline float* get_address_of_m_lineSpacingMax_88() { return &___m_lineSpacingMax_88; }
	inline void set_m_lineSpacingMax_88(float value)
	{
		___m_lineSpacingMax_88 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_89() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_paragraphSpacing_89)); }
	inline float get_m_paragraphSpacing_89() const { return ___m_paragraphSpacing_89; }
	inline float* get_address_of_m_paragraphSpacing_89() { return &___m_paragraphSpacing_89; }
	inline void set_m_paragraphSpacing_89(float value)
	{
		___m_paragraphSpacing_89 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_90() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthMaxAdj_90)); }
	inline float get_m_charWidthMaxAdj_90() const { return ___m_charWidthMaxAdj_90; }
	inline float* get_address_of_m_charWidthMaxAdj_90() { return &___m_charWidthMaxAdj_90; }
	inline void set_m_charWidthMaxAdj_90(float value)
	{
		___m_charWidthMaxAdj_90 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_91() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthAdjDelta_91)); }
	inline float get_m_charWidthAdjDelta_91() const { return ___m_charWidthAdjDelta_91; }
	inline float* get_address_of_m_charWidthAdjDelta_91() { return &___m_charWidthAdjDelta_91; }
	inline void set_m_charWidthAdjDelta_91(float value)
	{
		___m_charWidthAdjDelta_91 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_92() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableWordWrapping_92)); }
	inline bool get_m_enableWordWrapping_92() const { return ___m_enableWordWrapping_92; }
	inline bool* get_address_of_m_enableWordWrapping_92() { return &___m_enableWordWrapping_92; }
	inline void set_m_enableWordWrapping_92(bool value)
	{
		___m_enableWordWrapping_92 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_93() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCharacterWrappingEnabled_93)); }
	inline bool get_m_isCharacterWrappingEnabled_93() const { return ___m_isCharacterWrappingEnabled_93; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_93() { return &___m_isCharacterWrappingEnabled_93; }
	inline void set_m_isCharacterWrappingEnabled_93(bool value)
	{
		___m_isCharacterWrappingEnabled_93 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_94() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNonBreakingSpace_94)); }
	inline bool get_m_isNonBreakingSpace_94() const { return ___m_isNonBreakingSpace_94; }
	inline bool* get_address_of_m_isNonBreakingSpace_94() { return &___m_isNonBreakingSpace_94; }
	inline void set_m_isNonBreakingSpace_94(bool value)
	{
		___m_isNonBreakingSpace_94 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_95() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isIgnoringAlignment_95)); }
	inline bool get_m_isIgnoringAlignment_95() const { return ___m_isIgnoringAlignment_95; }
	inline bool* get_address_of_m_isIgnoringAlignment_95() { return &___m_isIgnoringAlignment_95; }
	inline void set_m_isIgnoringAlignment_95(bool value)
	{
		___m_isIgnoringAlignment_95 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_96() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordWrappingRatios_96)); }
	inline float get_m_wordWrappingRatios_96() const { return ___m_wordWrappingRatios_96; }
	inline float* get_address_of_m_wordWrappingRatios_96() { return &___m_wordWrappingRatios_96; }
	inline void set_m_wordWrappingRatios_96(float value)
	{
		___m_wordWrappingRatios_96 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_97() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overflowMode_97)); }
	inline int32_t get_m_overflowMode_97() const { return ___m_overflowMode_97; }
	inline int32_t* get_address_of_m_overflowMode_97() { return &___m_overflowMode_97; }
	inline void set_m_overflowMode_97(int32_t value)
	{
		___m_overflowMode_97 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_98() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstOverflowCharacterIndex_98)); }
	inline int32_t get_m_firstOverflowCharacterIndex_98() const { return ___m_firstOverflowCharacterIndex_98; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_98() { return &___m_firstOverflowCharacterIndex_98; }
	inline void set_m_firstOverflowCharacterIndex_98(int32_t value)
	{
		___m_firstOverflowCharacterIndex_98 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_99() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_linkedTextComponent_99)); }
	inline TMP_Text_t2599618874 * get_m_linkedTextComponent_99() const { return ___m_linkedTextComponent_99; }
	inline TMP_Text_t2599618874 ** get_address_of_m_linkedTextComponent_99() { return &___m_linkedTextComponent_99; }
	inline void set_m_linkedTextComponent_99(TMP_Text_t2599618874 * value)
	{
		___m_linkedTextComponent_99 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_99), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_100() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLinkedTextComponent_100)); }
	inline bool get_m_isLinkedTextComponent_100() const { return ___m_isLinkedTextComponent_100; }
	inline bool* get_address_of_m_isLinkedTextComponent_100() { return &___m_isLinkedTextComponent_100; }
	inline void set_m_isLinkedTextComponent_100(bool value)
	{
		___m_isLinkedTextComponent_100 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_101() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isTextTruncated_101)); }
	inline bool get_m_isTextTruncated_101() const { return ___m_isTextTruncated_101; }
	inline bool* get_address_of_m_isTextTruncated_101() { return &___m_isTextTruncated_101; }
	inline void set_m_isTextTruncated_101(bool value)
	{
		___m_isTextTruncated_101 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_102() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableKerning_102)); }
	inline bool get_m_enableKerning_102() const { return ___m_enableKerning_102; }
	inline bool* get_address_of_m_enableKerning_102() { return &___m_enableKerning_102; }
	inline void set_m_enableKerning_102(bool value)
	{
		___m_enableKerning_102 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_103() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableExtraPadding_103)); }
	inline bool get_m_enableExtraPadding_103() const { return ___m_enableExtraPadding_103; }
	inline bool* get_address_of_m_enableExtraPadding_103() { return &___m_enableExtraPadding_103; }
	inline void set_m_enableExtraPadding_103(bool value)
	{
		___m_enableExtraPadding_103 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_104() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___checkPaddingRequired_104)); }
	inline bool get_checkPaddingRequired_104() const { return ___checkPaddingRequired_104; }
	inline bool* get_address_of_checkPaddingRequired_104() { return &___checkPaddingRequired_104; }
	inline void set_checkPaddingRequired_104(bool value)
	{
		___checkPaddingRequired_104 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_105() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRichText_105)); }
	inline bool get_m_isRichText_105() const { return ___m_isRichText_105; }
	inline bool* get_address_of_m_isRichText_105() { return &___m_isRichText_105; }
	inline void set_m_isRichText_105(bool value)
	{
		___m_isRichText_105 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_106() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_parseCtrlCharacters_106)); }
	inline bool get_m_parseCtrlCharacters_106() const { return ___m_parseCtrlCharacters_106; }
	inline bool* get_address_of_m_parseCtrlCharacters_106() { return &___m_parseCtrlCharacters_106; }
	inline void set_m_parseCtrlCharacters_106(bool value)
	{
		___m_parseCtrlCharacters_106 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_107() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOverlay_107)); }
	inline bool get_m_isOverlay_107() const { return ___m_isOverlay_107; }
	inline bool* get_address_of_m_isOverlay_107() { return &___m_isOverlay_107; }
	inline void set_m_isOverlay_107(bool value)
	{
		___m_isOverlay_107 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_108() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOrthographic_108)); }
	inline bool get_m_isOrthographic_108() const { return ___m_isOrthographic_108; }
	inline bool* get_address_of_m_isOrthographic_108() { return &___m_isOrthographic_108; }
	inline void set_m_isOrthographic_108(bool value)
	{
		___m_isOrthographic_108 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_109() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCullingEnabled_109)); }
	inline bool get_m_isCullingEnabled_109() const { return ___m_isCullingEnabled_109; }
	inline bool* get_address_of_m_isCullingEnabled_109() { return &___m_isCullingEnabled_109; }
	inline void set_m_isCullingEnabled_109(bool value)
	{
		___m_isCullingEnabled_109 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_110() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreRectMaskCulling_110)); }
	inline bool get_m_ignoreRectMaskCulling_110() const { return ___m_ignoreRectMaskCulling_110; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_110() { return &___m_ignoreRectMaskCulling_110; }
	inline void set_m_ignoreRectMaskCulling_110(bool value)
	{
		___m_ignoreRectMaskCulling_110 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_111() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreCulling_111)); }
	inline bool get_m_ignoreCulling_111() const { return ___m_ignoreCulling_111; }
	inline bool* get_address_of_m_ignoreCulling_111() { return &___m_ignoreCulling_111; }
	inline void set_m_ignoreCulling_111(bool value)
	{
		___m_ignoreCulling_111 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_112() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_horizontalMapping_112)); }
	inline int32_t get_m_horizontalMapping_112() const { return ___m_horizontalMapping_112; }
	inline int32_t* get_address_of_m_horizontalMapping_112() { return &___m_horizontalMapping_112; }
	inline void set_m_horizontalMapping_112(int32_t value)
	{
		___m_horizontalMapping_112 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_113() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticalMapping_113)); }
	inline int32_t get_m_verticalMapping_113() const { return ___m_verticalMapping_113; }
	inline int32_t* get_address_of_m_verticalMapping_113() { return &___m_verticalMapping_113; }
	inline void set_m_verticalMapping_113(int32_t value)
	{
		___m_verticalMapping_113 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_114() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_uvLineOffset_114)); }
	inline float get_m_uvLineOffset_114() const { return ___m_uvLineOffset_114; }
	inline float* get_address_of_m_uvLineOffset_114() { return &___m_uvLineOffset_114; }
	inline void set_m_uvLineOffset_114(float value)
	{
		___m_uvLineOffset_114 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_115() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderMode_115)); }
	inline int32_t get_m_renderMode_115() const { return ___m_renderMode_115; }
	inline int32_t* get_address_of_m_renderMode_115() { return &___m_renderMode_115; }
	inline void set_m_renderMode_115(int32_t value)
	{
		___m_renderMode_115 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_116() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_geometrySortingOrder_116)); }
	inline int32_t get_m_geometrySortingOrder_116() const { return ___m_geometrySortingOrder_116; }
	inline int32_t* get_address_of_m_geometrySortingOrder_116() { return &___m_geometrySortingOrder_116; }
	inline void set_m_geometrySortingOrder_116(int32_t value)
	{
		___m_geometrySortingOrder_116 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_117() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacter_117)); }
	inline int32_t get_m_firstVisibleCharacter_117() const { return ___m_firstVisibleCharacter_117; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_117() { return &___m_firstVisibleCharacter_117; }
	inline void set_m_firstVisibleCharacter_117(int32_t value)
	{
		___m_firstVisibleCharacter_117 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_118() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleCharacters_118)); }
	inline int32_t get_m_maxVisibleCharacters_118() const { return ___m_maxVisibleCharacters_118; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_118() { return &___m_maxVisibleCharacters_118; }
	inline void set_m_maxVisibleCharacters_118(int32_t value)
	{
		___m_maxVisibleCharacters_118 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_119() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleWords_119)); }
	inline int32_t get_m_maxVisibleWords_119() const { return ___m_maxVisibleWords_119; }
	inline int32_t* get_address_of_m_maxVisibleWords_119() { return &___m_maxVisibleWords_119; }
	inline void set_m_maxVisibleWords_119(int32_t value)
	{
		___m_maxVisibleWords_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_120() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleLines_120)); }
	inline int32_t get_m_maxVisibleLines_120() const { return ___m_maxVisibleLines_120; }
	inline int32_t* get_address_of_m_maxVisibleLines_120() { return &___m_maxVisibleLines_120; }
	inline void set_m_maxVisibleLines_120(int32_t value)
	{
		___m_maxVisibleLines_120 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_121() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_useMaxVisibleDescender_121)); }
	inline bool get_m_useMaxVisibleDescender_121() const { return ___m_useMaxVisibleDescender_121; }
	inline bool* get_address_of_m_useMaxVisibleDescender_121() { return &___m_useMaxVisibleDescender_121; }
	inline void set_m_useMaxVisibleDescender_121(bool value)
	{
		___m_useMaxVisibleDescender_121 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_122() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageToDisplay_122)); }
	inline int32_t get_m_pageToDisplay_122() const { return ___m_pageToDisplay_122; }
	inline int32_t* get_address_of_m_pageToDisplay_122() { return &___m_pageToDisplay_122; }
	inline void set_m_pageToDisplay_122(int32_t value)
	{
		___m_pageToDisplay_122 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_123() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNewPage_123)); }
	inline bool get_m_isNewPage_123() const { return ___m_isNewPage_123; }
	inline bool* get_address_of_m_isNewPage_123() { return &___m_isNewPage_123; }
	inline void set_m_isNewPage_123(bool value)
	{
		___m_isNewPage_123 = value;
	}

	inline static int32_t get_offset_of_m_margin_124() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_margin_124)); }
	inline Vector4_t3319028937  get_m_margin_124() const { return ___m_margin_124; }
	inline Vector4_t3319028937 * get_address_of_m_margin_124() { return &___m_margin_124; }
	inline void set_m_margin_124(Vector4_t3319028937  value)
	{
		___m_margin_124 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_125() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginLeft_125)); }
	inline float get_m_marginLeft_125() const { return ___m_marginLeft_125; }
	inline float* get_address_of_m_marginLeft_125() { return &___m_marginLeft_125; }
	inline void set_m_marginLeft_125(float value)
	{
		___m_marginLeft_125 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_126() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginRight_126)); }
	inline float get_m_marginRight_126() const { return ___m_marginRight_126; }
	inline float* get_address_of_m_marginRight_126() { return &___m_marginRight_126; }
	inline void set_m_marginRight_126(float value)
	{
		___m_marginRight_126 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_127() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginWidth_127)); }
	inline float get_m_marginWidth_127() const { return ___m_marginWidth_127; }
	inline float* get_address_of_m_marginWidth_127() { return &___m_marginWidth_127; }
	inline void set_m_marginWidth_127(float value)
	{
		___m_marginWidth_127 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginHeight_128)); }
	inline float get_m_marginHeight_128() const { return ___m_marginHeight_128; }
	inline float* get_address_of_m_marginHeight_128() { return &___m_marginHeight_128; }
	inline void set_m_marginHeight_128(float value)
	{
		___m_marginHeight_128 = value;
	}

	inline static int32_t get_offset_of_m_width_129() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_width_129)); }
	inline float get_m_width_129() const { return ___m_width_129; }
	inline float* get_address_of_m_width_129() { return &___m_width_129; }
	inline void set_m_width_129(float value)
	{
		___m_width_129 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_130() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textInfo_130)); }
	inline TMP_TextInfo_t3598145122 * get_m_textInfo_130() const { return ___m_textInfo_130; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_m_textInfo_130() { return &___m_textInfo_130; }
	inline void set_m_textInfo_130(TMP_TextInfo_t3598145122 * value)
	{
		___m_textInfo_130 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_130), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_131() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_havePropertiesChanged_131)); }
	inline bool get_m_havePropertiesChanged_131() const { return ___m_havePropertiesChanged_131; }
	inline bool* get_address_of_m_havePropertiesChanged_131() { return &___m_havePropertiesChanged_131; }
	inline void set_m_havePropertiesChanged_131(bool value)
	{
		___m_havePropertiesChanged_131 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_132() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingLegacyAnimationComponent_132)); }
	inline bool get_m_isUsingLegacyAnimationComponent_132() const { return ___m_isUsingLegacyAnimationComponent_132; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_132() { return &___m_isUsingLegacyAnimationComponent_132; }
	inline void set_m_isUsingLegacyAnimationComponent_132(bool value)
	{
		___m_isUsingLegacyAnimationComponent_132 = value;
	}

	inline static int32_t get_offset_of_m_transform_133() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_transform_133)); }
	inline Transform_t3600365921 * get_m_transform_133() const { return ___m_transform_133; }
	inline Transform_t3600365921 ** get_address_of_m_transform_133() { return &___m_transform_133; }
	inline void set_m_transform_133(Transform_t3600365921 * value)
	{
		___m_transform_133 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_133), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_134() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_rectTransform_134)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_134() const { return ___m_rectTransform_134; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_134() { return &___m_rectTransform_134; }
	inline void set_m_rectTransform_134(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_134), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___U3CautoSizeTextContainerU3Ek__BackingField_135)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_135() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return &___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_135(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_135 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_136() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_autoSizeTextContainer_136)); }
	inline bool get_m_autoSizeTextContainer_136() const { return ___m_autoSizeTextContainer_136; }
	inline bool* get_address_of_m_autoSizeTextContainer_136() { return &___m_autoSizeTextContainer_136; }
	inline void set_m_autoSizeTextContainer_136(bool value)
	{
		___m_autoSizeTextContainer_136 = value;
	}

	inline static int32_t get_offset_of_m_mesh_137() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_mesh_137)); }
	inline Mesh_t3648964284 * get_m_mesh_137() const { return ___m_mesh_137; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_137() { return &___m_mesh_137; }
	inline void set_m_mesh_137(Mesh_t3648964284 * value)
	{
		___m_mesh_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_137), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_138() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isVolumetricText_138)); }
	inline bool get_m_isVolumetricText_138() const { return ___m_isVolumetricText_138; }
	inline bool* get_address_of_m_isVolumetricText_138() { return &___m_isVolumetricText_138; }
	inline void set_m_isVolumetricText_138(bool value)
	{
		___m_isVolumetricText_138 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_139() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimator_139)); }
	inline TMP_SpriteAnimator_t2836635477 * get_m_spriteAnimator_139() const { return ___m_spriteAnimator_139; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_m_spriteAnimator_139() { return &___m_spriteAnimator_139; }
	inline void set_m_spriteAnimator_139(TMP_SpriteAnimator_t2836635477 * value)
	{
		___m_spriteAnimator_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_139), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_140() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleHeight_140)); }
	inline float get_m_flexibleHeight_140() const { return ___m_flexibleHeight_140; }
	inline float* get_address_of_m_flexibleHeight_140() { return &___m_flexibleHeight_140; }
	inline void set_m_flexibleHeight_140(float value)
	{
		___m_flexibleHeight_140 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_141() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleWidth_141)); }
	inline float get_m_flexibleWidth_141() const { return ___m_flexibleWidth_141; }
	inline float* get_address_of_m_flexibleWidth_141() { return &___m_flexibleWidth_141; }
	inline void set_m_flexibleWidth_141(float value)
	{
		___m_flexibleWidth_141 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_142() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minWidth_142)); }
	inline float get_m_minWidth_142() const { return ___m_minWidth_142; }
	inline float* get_address_of_m_minWidth_142() { return &___m_minWidth_142; }
	inline void set_m_minWidth_142(float value)
	{
		___m_minWidth_142 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_143() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minHeight_143)); }
	inline float get_m_minHeight_143() const { return ___m_minHeight_143; }
	inline float* get_address_of_m_minHeight_143() { return &___m_minHeight_143; }
	inline void set_m_minHeight_143(float value)
	{
		___m_minHeight_143 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxWidth_144)); }
	inline float get_m_maxWidth_144() const { return ___m_maxWidth_144; }
	inline float* get_address_of_m_maxWidth_144() { return &___m_maxWidth_144; }
	inline void set_m_maxWidth_144(float value)
	{
		___m_maxWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxHeight_145)); }
	inline float get_m_maxHeight_145() const { return ___m_maxHeight_145; }
	inline float* get_address_of_m_maxHeight_145() { return &___m_maxHeight_145; }
	inline void set_m_maxHeight_145(float value)
	{
		___m_maxHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_146() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_LayoutElement_146)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_146() const { return ___m_LayoutElement_146; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_146() { return &___m_LayoutElement_146; }
	inline void set_m_LayoutElement_146(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_146 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_146), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_147() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredWidth_147)); }
	inline float get_m_preferredWidth_147() const { return ___m_preferredWidth_147; }
	inline float* get_address_of_m_preferredWidth_147() { return &___m_preferredWidth_147; }
	inline void set_m_preferredWidth_147(float value)
	{
		___m_preferredWidth_147 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedWidth_148)); }
	inline float get_m_renderedWidth_148() const { return ___m_renderedWidth_148; }
	inline float* get_address_of_m_renderedWidth_148() { return &___m_renderedWidth_148; }
	inline void set_m_renderedWidth_148(float value)
	{
		___m_renderedWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_149() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredWidthDirty_149)); }
	inline bool get_m_isPreferredWidthDirty_149() const { return ___m_isPreferredWidthDirty_149; }
	inline bool* get_address_of_m_isPreferredWidthDirty_149() { return &___m_isPreferredWidthDirty_149; }
	inline void set_m_isPreferredWidthDirty_149(bool value)
	{
		___m_isPreferredWidthDirty_149 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_150() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredHeight_150)); }
	inline float get_m_preferredHeight_150() const { return ___m_preferredHeight_150; }
	inline float* get_address_of_m_preferredHeight_150() { return &___m_preferredHeight_150; }
	inline void set_m_preferredHeight_150(float value)
	{
		___m_preferredHeight_150 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_151() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedHeight_151)); }
	inline float get_m_renderedHeight_151() const { return ___m_renderedHeight_151; }
	inline float* get_address_of_m_renderedHeight_151() { return &___m_renderedHeight_151; }
	inline void set_m_renderedHeight_151(float value)
	{
		___m_renderedHeight_151 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_152() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredHeightDirty_152)); }
	inline bool get_m_isPreferredHeightDirty_152() const { return ___m_isPreferredHeightDirty_152; }
	inline bool* get_address_of_m_isPreferredHeightDirty_152() { return &___m_isPreferredHeightDirty_152; }
	inline void set_m_isPreferredHeightDirty_152(bool value)
	{
		___m_isPreferredHeightDirty_152 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_153() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculatingPreferredValues_153)); }
	inline bool get_m_isCalculatingPreferredValues_153() const { return ___m_isCalculatingPreferredValues_153; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_153() { return &___m_isCalculatingPreferredValues_153; }
	inline void set_m_isCalculatingPreferredValues_153(bool value)
	{
		___m_isCalculatingPreferredValues_153 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_154() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_recursiveCount_154)); }
	inline int32_t get_m_recursiveCount_154() const { return ___m_recursiveCount_154; }
	inline int32_t* get_address_of_m_recursiveCount_154() { return &___m_recursiveCount_154; }
	inline void set_m_recursiveCount_154(int32_t value)
	{
		___m_recursiveCount_154 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_155() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutPriority_155)); }
	inline int32_t get_m_layoutPriority_155() const { return ___m_layoutPriority_155; }
	inline int32_t* get_address_of_m_layoutPriority_155() { return &___m_layoutPriority_155; }
	inline void set_m_layoutPriority_155(int32_t value)
	{
		___m_layoutPriority_155 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_156() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculateSizeRequired_156)); }
	inline bool get_m_isCalculateSizeRequired_156() const { return ___m_isCalculateSizeRequired_156; }
	inline bool* get_address_of_m_isCalculateSizeRequired_156() { return &___m_isCalculateSizeRequired_156; }
	inline void set_m_isCalculateSizeRequired_156(bool value)
	{
		___m_isCalculateSizeRequired_156 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_157() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLayoutDirty_157)); }
	inline bool get_m_isLayoutDirty_157() const { return ___m_isLayoutDirty_157; }
	inline bool* get_address_of_m_isLayoutDirty_157() { return &___m_isLayoutDirty_157; }
	inline void set_m_isLayoutDirty_157(bool value)
	{
		___m_isLayoutDirty_157 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_158() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticesAlreadyDirty_158)); }
	inline bool get_m_verticesAlreadyDirty_158() const { return ___m_verticesAlreadyDirty_158; }
	inline bool* get_address_of_m_verticesAlreadyDirty_158() { return &___m_verticesAlreadyDirty_158; }
	inline void set_m_verticesAlreadyDirty_158(bool value)
	{
		___m_verticesAlreadyDirty_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutAlreadyDirty_159)); }
	inline bool get_m_layoutAlreadyDirty_159() const { return ___m_layoutAlreadyDirty_159; }
	inline bool* get_address_of_m_layoutAlreadyDirty_159() { return &___m_layoutAlreadyDirty_159; }
	inline void set_m_layoutAlreadyDirty_159(bool value)
	{
		___m_layoutAlreadyDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_160() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAwake_160)); }
	inline bool get_m_isAwake_160() const { return ___m_isAwake_160; }
	inline bool* get_address_of_m_isAwake_160() { return &___m_isAwake_160; }
	inline void set_m_isAwake_160(bool value)
	{
		___m_isAwake_160 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_161() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isInputParsingRequired_161)); }
	inline bool get_m_isInputParsingRequired_161() const { return ___m_isInputParsingRequired_161; }
	inline bool* get_address_of_m_isInputParsingRequired_161() { return &___m_isInputParsingRequired_161; }
	inline void set_m_isInputParsingRequired_161(bool value)
	{
		___m_isInputParsingRequired_161 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_162() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inputSource_162)); }
	inline int32_t get_m_inputSource_162() const { return ___m_inputSource_162; }
	inline int32_t* get_address_of_m_inputSource_162() { return &___m_inputSource_162; }
	inline void set_m_inputSource_162(int32_t value)
	{
		___m_inputSource_162 = value;
	}

	inline static int32_t get_offset_of_old_text_163() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___old_text_163)); }
	inline String_t* get_old_text_163() const { return ___old_text_163; }
	inline String_t** get_address_of_old_text_163() { return &___old_text_163; }
	inline void set_old_text_163(String_t* value)
	{
		___old_text_163 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_163), value);
	}

	inline static int32_t get_offset_of_m_fontScale_164() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScale_164)); }
	inline float get_m_fontScale_164() const { return ___m_fontScale_164; }
	inline float* get_address_of_m_fontScale_164() { return &___m_fontScale_164; }
	inline void set_m_fontScale_164(float value)
	{
		___m_fontScale_164 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_165() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScaleMultiplier_165)); }
	inline float get_m_fontScaleMultiplier_165() const { return ___m_fontScaleMultiplier_165; }
	inline float* get_address_of_m_fontScaleMultiplier_165() { return &___m_fontScaleMultiplier_165; }
	inline void set_m_fontScaleMultiplier_165(float value)
	{
		___m_fontScaleMultiplier_165 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_166() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlTag_166)); }
	inline CharU5BU5D_t3528271667* get_m_htmlTag_166() const { return ___m_htmlTag_166; }
	inline CharU5BU5D_t3528271667** get_address_of_m_htmlTag_166() { return &___m_htmlTag_166; }
	inline void set_m_htmlTag_166(CharU5BU5D_t3528271667* value)
	{
		___m_htmlTag_166 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_166), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_167() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xmlAttribute_167)); }
	inline XML_TagAttributeU5BU5D_t284240280* get_m_xmlAttribute_167() const { return ___m_xmlAttribute_167; }
	inline XML_TagAttributeU5BU5D_t284240280** get_address_of_m_xmlAttribute_167() { return &___m_xmlAttribute_167; }
	inline void set_m_xmlAttribute_167(XML_TagAttributeU5BU5D_t284240280* value)
	{
		___m_xmlAttribute_167 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_167), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_168() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_attributeParameterValues_168)); }
	inline SingleU5BU5D_t1444911251* get_m_attributeParameterValues_168() const { return ___m_attributeParameterValues_168; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_attributeParameterValues_168() { return &___m_attributeParameterValues_168; }
	inline void set_m_attributeParameterValues_168(SingleU5BU5D_t1444911251* value)
	{
		___m_attributeParameterValues_168 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_168), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_169() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_LineIndent_169)); }
	inline float get_tag_LineIndent_169() const { return ___tag_LineIndent_169; }
	inline float* get_address_of_tag_LineIndent_169() { return &___tag_LineIndent_169; }
	inline void set_tag_LineIndent_169(float value)
	{
		___tag_LineIndent_169 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_170() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_Indent_170)); }
	inline float get_tag_Indent_170() const { return ___tag_Indent_170; }
	inline float* get_address_of_tag_Indent_170() { return &___tag_Indent_170; }
	inline void set_tag_Indent_170(float value)
	{
		___tag_Indent_170 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_171() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_indentStack_171)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_indentStack_171() const { return ___m_indentStack_171; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_indentStack_171() { return &___m_indentStack_171; }
	inline void set_m_indentStack_171(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_indentStack_171 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_172() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_NoParsing_172)); }
	inline bool get_tag_NoParsing_172() const { return ___tag_NoParsing_172; }
	inline bool* get_address_of_tag_NoParsing_172() { return &___tag_NoParsing_172; }
	inline void set_tag_NoParsing_172(bool value)
	{
		___tag_NoParsing_172 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_173() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isParsingText_173)); }
	inline bool get_m_isParsingText_173() const { return ___m_isParsingText_173; }
	inline bool* get_address_of_m_isParsingText_173() { return &___m_isParsingText_173; }
	inline void set_m_isParsingText_173(bool value)
	{
		___m_isParsingText_173 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_174() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_FXMatrix_174)); }
	inline Matrix4x4_t1817901843  get_m_FXMatrix_174() const { return ___m_FXMatrix_174; }
	inline Matrix4x4_t1817901843 * get_address_of_m_FXMatrix_174() { return &___m_FXMatrix_174; }
	inline void set_m_FXMatrix_174(Matrix4x4_t1817901843  value)
	{
		___m_FXMatrix_174 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_175() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isFXMatrixSet_175)); }
	inline bool get_m_isFXMatrixSet_175() const { return ___m_isFXMatrixSet_175; }
	inline bool* get_address_of_m_isFXMatrixSet_175() { return &___m_isFXMatrixSet_175; }
	inline void set_m_isFXMatrixSet_175(bool value)
	{
		___m_isFXMatrixSet_175 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_176() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_char_buffer_176)); }
	inline Int32U5BU5D_t385246372* get_m_char_buffer_176() const { return ___m_char_buffer_176; }
	inline Int32U5BU5D_t385246372** get_address_of_m_char_buffer_176() { return &___m_char_buffer_176; }
	inline void set_m_char_buffer_176(Int32U5BU5D_t385246372* value)
	{
		___m_char_buffer_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_176), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_177() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_internalCharacterInfo_177)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_m_internalCharacterInfo_177() const { return ___m_internalCharacterInfo_177; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_m_internalCharacterInfo_177() { return &___m_internalCharacterInfo_177; }
	inline void set_m_internalCharacterInfo_177(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___m_internalCharacterInfo_177 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_177), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_178() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_input_CharArray_178)); }
	inline CharU5BU5D_t3528271667* get_m_input_CharArray_178() const { return ___m_input_CharArray_178; }
	inline CharU5BU5D_t3528271667** get_address_of_m_input_CharArray_178() { return &___m_input_CharArray_178; }
	inline void set_m_input_CharArray_178(CharU5BU5D_t3528271667* value)
	{
		___m_input_CharArray_178 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_178), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_179() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charArray_Length_179)); }
	inline int32_t get_m_charArray_Length_179() const { return ___m_charArray_Length_179; }
	inline int32_t* get_address_of_m_charArray_Length_179() { return &___m_charArray_Length_179; }
	inline void set_m_charArray_Length_179(int32_t value)
	{
		___m_charArray_Length_179 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_180() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_totalCharacterCount_180)); }
	inline int32_t get_m_totalCharacterCount_180() const { return ___m_totalCharacterCount_180; }
	inline int32_t* get_address_of_m_totalCharacterCount_180() { return &___m_totalCharacterCount_180; }
	inline void set_m_totalCharacterCount_180(int32_t value)
	{
		___m_totalCharacterCount_180 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_181() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedWordWrapState_181)); }
	inline WordWrapState_t341939652  get_m_SavedWordWrapState_181() const { return ___m_SavedWordWrapState_181; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedWordWrapState_181() { return &___m_SavedWordWrapState_181; }
	inline void set_m_SavedWordWrapState_181(WordWrapState_t341939652  value)
	{
		___m_SavedWordWrapState_181 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_182() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedLineState_182)); }
	inline WordWrapState_t341939652  get_m_SavedLineState_182() const { return ___m_SavedLineState_182; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedLineState_182() { return &___m_SavedLineState_182; }
	inline void set_m_SavedLineState_182(WordWrapState_t341939652  value)
	{
		___m_SavedLineState_182 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterCount_183)); }
	inline int32_t get_m_characterCount_183() const { return ___m_characterCount_183; }
	inline int32_t* get_address_of_m_characterCount_183() { return &___m_characterCount_183; }
	inline void set_m_characterCount_183(int32_t value)
	{
		___m_characterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_184() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstCharacterOfLine_184)); }
	inline int32_t get_m_firstCharacterOfLine_184() const { return ___m_firstCharacterOfLine_184; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_184() { return &___m_firstCharacterOfLine_184; }
	inline void set_m_firstCharacterOfLine_184(int32_t value)
	{
		___m_firstCharacterOfLine_184 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_185() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacterOfLine_185)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_185() const { return ___m_firstVisibleCharacterOfLine_185; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_185() { return &___m_firstVisibleCharacterOfLine_185; }
	inline void set_m_firstVisibleCharacterOfLine_185(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_185 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_186() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastCharacterOfLine_186)); }
	inline int32_t get_m_lastCharacterOfLine_186() const { return ___m_lastCharacterOfLine_186; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_186() { return &___m_lastCharacterOfLine_186; }
	inline void set_m_lastCharacterOfLine_186(int32_t value)
	{
		___m_lastCharacterOfLine_186 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastVisibleCharacterOfLine_187)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_187() const { return ___m_lastVisibleCharacterOfLine_187; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_187() { return &___m_lastVisibleCharacterOfLine_187; }
	inline void set_m_lastVisibleCharacterOfLine_187(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_188() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineNumber_188)); }
	inline int32_t get_m_lineNumber_188() const { return ___m_lineNumber_188; }
	inline int32_t* get_address_of_m_lineNumber_188() { return &___m_lineNumber_188; }
	inline void set_m_lineNumber_188(int32_t value)
	{
		___m_lineNumber_188 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_189() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineVisibleCharacterCount_189)); }
	inline int32_t get_m_lineVisibleCharacterCount_189() const { return ___m_lineVisibleCharacterCount_189; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_189() { return &___m_lineVisibleCharacterCount_189; }
	inline void set_m_lineVisibleCharacterCount_189(int32_t value)
	{
		___m_lineVisibleCharacterCount_189 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_190() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageNumber_190)); }
	inline int32_t get_m_pageNumber_190() const { return ___m_pageNumber_190; }
	inline int32_t* get_address_of_m_pageNumber_190() { return &___m_pageNumber_190; }
	inline void set_m_pageNumber_190(int32_t value)
	{
		___m_pageNumber_190 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_191() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxAscender_191)); }
	inline float get_m_maxAscender_191() const { return ___m_maxAscender_191; }
	inline float* get_address_of_m_maxAscender_191() { return &___m_maxAscender_191; }
	inline void set_m_maxAscender_191(float value)
	{
		___m_maxAscender_191 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_192() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxCapHeight_192)); }
	inline float get_m_maxCapHeight_192() const { return ___m_maxCapHeight_192; }
	inline float* get_address_of_m_maxCapHeight_192() { return &___m_maxCapHeight_192; }
	inline void set_m_maxCapHeight_192(float value)
	{
		___m_maxCapHeight_192 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_193() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxDescender_193)); }
	inline float get_m_maxDescender_193() const { return ___m_maxDescender_193; }
	inline float* get_address_of_m_maxDescender_193() { return &___m_maxDescender_193; }
	inline void set_m_maxDescender_193(float value)
	{
		___m_maxDescender_193 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineAscender_194)); }
	inline float get_m_maxLineAscender_194() const { return ___m_maxLineAscender_194; }
	inline float* get_address_of_m_maxLineAscender_194() { return &___m_maxLineAscender_194; }
	inline void set_m_maxLineAscender_194(float value)
	{
		___m_maxLineAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_195() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineDescender_195)); }
	inline float get_m_maxLineDescender_195() const { return ___m_maxLineDescender_195; }
	inline float* get_address_of_m_maxLineDescender_195() { return &___m_maxLineDescender_195; }
	inline void set_m_maxLineDescender_195(float value)
	{
		___m_maxLineDescender_195 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_startOfLineAscender_196)); }
	inline float get_m_startOfLineAscender_196() const { return ___m_startOfLineAscender_196; }
	inline float* get_address_of_m_startOfLineAscender_196() { return &___m_startOfLineAscender_196; }
	inline void set_m_startOfLineAscender_196(float value)
	{
		___m_startOfLineAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_197() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineOffset_197)); }
	inline float get_m_lineOffset_197() const { return ___m_lineOffset_197; }
	inline float* get_address_of_m_lineOffset_197() { return &___m_lineOffset_197; }
	inline void set_m_lineOffset_197(float value)
	{
		___m_lineOffset_197 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_198() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_meshExtents_198)); }
	inline Extents_t3837212874  get_m_meshExtents_198() const { return ___m_meshExtents_198; }
	inline Extents_t3837212874 * get_address_of_m_meshExtents_198() { return &___m_meshExtents_198; }
	inline void set_m_meshExtents_198(Extents_t3837212874  value)
	{
		___m_meshExtents_198 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_199() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlColor_199)); }
	inline Color32_t2600501292  get_m_htmlColor_199() const { return ___m_htmlColor_199; }
	inline Color32_t2600501292 * get_address_of_m_htmlColor_199() { return &___m_htmlColor_199; }
	inline void set_m_htmlColor_199(Color32_t2600501292  value)
	{
		___m_htmlColor_199 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_200() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorStack_200)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_colorStack_200() const { return ___m_colorStack_200; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_colorStack_200() { return &___m_colorStack_200; }
	inline void set_m_colorStack_200(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_colorStack_200 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_201() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColorStack_201)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_underlineColorStack_201() const { return ___m_underlineColorStack_201; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_underlineColorStack_201() { return &___m_underlineColorStack_201; }
	inline void set_m_underlineColorStack_201(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_underlineColorStack_201 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_202() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColorStack_202)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_strikethroughColorStack_202() const { return ___m_strikethroughColorStack_202; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_strikethroughColorStack_202() { return &___m_strikethroughColorStack_202; }
	inline void set_m_strikethroughColorStack_202(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_strikethroughColorStack_202 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColorStack_203)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_highlightColorStack_203() const { return ___m_highlightColorStack_203; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_highlightColorStack_203() { return &___m_highlightColorStack_203; }
	inline void set_m_highlightColorStack_203(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_highlightColorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_204() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientPreset_204)); }
	inline TMP_ColorGradient_t3678055768 * get_m_colorGradientPreset_204() const { return ___m_colorGradientPreset_204; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_colorGradientPreset_204() { return &___m_colorGradientPreset_204; }
	inline void set_m_colorGradientPreset_204(TMP_ColorGradient_t3678055768 * value)
	{
		___m_colorGradientPreset_204 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_204), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientStack_205)); }
	inline TMP_XmlTagStack_1_t3241710312  get_m_colorGradientStack_205() const { return ___m_colorGradientStack_205; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_m_colorGradientStack_205() { return &___m_colorGradientStack_205; }
	inline void set_m_colorGradientStack_205(TMP_XmlTagStack_1_t3241710312  value)
	{
		___m_colorGradientStack_205 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_206() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tabSpacing_206)); }
	inline float get_m_tabSpacing_206() const { return ___m_tabSpacing_206; }
	inline float* get_address_of_m_tabSpacing_206() { return &___m_tabSpacing_206; }
	inline void set_m_tabSpacing_206(float value)
	{
		___m_tabSpacing_206 = value;
	}

	inline static int32_t get_offset_of_m_spacing_207() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spacing_207)); }
	inline float get_m_spacing_207() const { return ___m_spacing_207; }
	inline float* get_address_of_m_spacing_207() { return &___m_spacing_207; }
	inline void set_m_spacing_207(float value)
	{
		___m_spacing_207 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_styleStack_208)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_styleStack_208() const { return ___m_styleStack_208; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_styleStack_208() { return &___m_styleStack_208; }
	inline void set_m_styleStack_208(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_styleStack_208 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_209() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_actionStack_209)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_actionStack_209() const { return ___m_actionStack_209; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_actionStack_209() { return &___m_actionStack_209; }
	inline void set_m_actionStack_209(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_actionStack_209 = value;
	}

	inline static int32_t get_offset_of_m_padding_210() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_padding_210)); }
	inline float get_m_padding_210() const { return ___m_padding_210; }
	inline float* get_address_of_m_padding_210() { return &___m_padding_210; }
	inline void set_m_padding_210(float value)
	{
		___m_padding_210 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_211() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffset_211)); }
	inline float get_m_baselineOffset_211() const { return ___m_baselineOffset_211; }
	inline float* get_address_of_m_baselineOffset_211() { return &___m_baselineOffset_211; }
	inline void set_m_baselineOffset_211(float value)
	{
		___m_baselineOffset_211 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffsetStack_212)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_baselineOffsetStack_212() const { return ___m_baselineOffsetStack_212; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_baselineOffsetStack_212() { return &___m_baselineOffsetStack_212; }
	inline void set_m_baselineOffsetStack_212(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_baselineOffsetStack_212 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_213() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xAdvance_213)); }
	inline float get_m_xAdvance_213() const { return ___m_xAdvance_213; }
	inline float* get_address_of_m_xAdvance_213() { return &___m_xAdvance_213; }
	inline void set_m_xAdvance_213(float value)
	{
		___m_xAdvance_213 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_214() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textElementType_214)); }
	inline int32_t get_m_textElementType_214() const { return ___m_textElementType_214; }
	inline int32_t* get_address_of_m_textElementType_214() { return &___m_textElementType_214; }
	inline void set_m_textElementType_214(int32_t value)
	{
		___m_textElementType_214 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_215() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_TextElement_215)); }
	inline TMP_TextElement_t129727469 * get_m_cached_TextElement_215() const { return ___m_cached_TextElement_215; }
	inline TMP_TextElement_t129727469 ** get_address_of_m_cached_TextElement_215() { return &___m_cached_TextElement_215; }
	inline void set_m_cached_TextElement_215(TMP_TextElement_t129727469 * value)
	{
		___m_cached_TextElement_215 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_215), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_216() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Underline_GlyphInfo_216)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Underline_GlyphInfo_216() const { return ___m_cached_Underline_GlyphInfo_216; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Underline_GlyphInfo_216() { return &___m_cached_Underline_GlyphInfo_216; }
	inline void set_m_cached_Underline_GlyphInfo_216(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Underline_GlyphInfo_216 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_216), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_217() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Ellipsis_GlyphInfo_217)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Ellipsis_GlyphInfo_217() const { return ___m_cached_Ellipsis_GlyphInfo_217; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Ellipsis_GlyphInfo_217() { return &___m_cached_Ellipsis_GlyphInfo_217; }
	inline void set_m_cached_Ellipsis_GlyphInfo_217(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_217), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_218() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_defaultSpriteAsset_218)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_218() const { return ___m_defaultSpriteAsset_218; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_218() { return &___m_defaultSpriteAsset_218; }
	inline void set_m_defaultSpriteAsset_218(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_218), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_219() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentSpriteAsset_219)); }
	inline TMP_SpriteAsset_t484820633 * get_m_currentSpriteAsset_219() const { return ___m_currentSpriteAsset_219; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_currentSpriteAsset_219() { return &___m_currentSpriteAsset_219; }
	inline void set_m_currentSpriteAsset_219(TMP_SpriteAsset_t484820633 * value)
	{
		___m_currentSpriteAsset_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_219), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_220() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteCount_220)); }
	inline int32_t get_m_spriteCount_220() const { return ___m_spriteCount_220; }
	inline int32_t* get_address_of_m_spriteCount_220() { return &___m_spriteCount_220; }
	inline void set_m_spriteCount_220(int32_t value)
	{
		___m_spriteCount_220 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_221() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteIndex_221)); }
	inline int32_t get_m_spriteIndex_221() const { return ___m_spriteIndex_221; }
	inline int32_t* get_address_of_m_spriteIndex_221() { return &___m_spriteIndex_221; }
	inline void set_m_spriteIndex_221(int32_t value)
	{
		___m_spriteIndex_221 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_222() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimationID_222)); }
	inline int32_t get_m_spriteAnimationID_222() const { return ___m_spriteAnimationID_222; }
	inline int32_t* get_address_of_m_spriteAnimationID_222() { return &___m_spriteAnimationID_222; }
	inline void set_m_spriteAnimationID_222(int32_t value)
	{
		___m_spriteAnimationID_222 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_223() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreActiveState_223)); }
	inline bool get_m_ignoreActiveState_223() const { return ___m_ignoreActiveState_223; }
	inline bool* get_address_of_m_ignoreActiveState_223() { return &___m_ignoreActiveState_223; }
	inline void set_m_ignoreActiveState_223(bool value)
	{
		___m_ignoreActiveState_223 = value;
	}

	inline static int32_t get_offset_of_k_Power_224() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___k_Power_224)); }
	inline SingleU5BU5D_t1444911251* get_k_Power_224() const { return ___k_Power_224; }
	inline SingleU5BU5D_t1444911251** get_address_of_k_Power_224() { return &___k_Power_224; }
	inline void set_k_Power_224(SingleU5BU5D_t1444911251* value)
	{
		___k_Power_224 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_224), value);
	}
};

struct TMP_Text_t2599618874_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t2600501292  ___s_colorWhite_45;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2156229523  ___k_LargePositiveVector2_225;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2156229523  ___k_LargeNegativeVector2_226;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_227;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_228;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_229;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_230;

public:
	inline static int32_t get_offset_of_s_colorWhite_45() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___s_colorWhite_45)); }
	inline Color32_t2600501292  get_s_colorWhite_45() const { return ___s_colorWhite_45; }
	inline Color32_t2600501292 * get_address_of_s_colorWhite_45() { return &___s_colorWhite_45; }
	inline void set_s_colorWhite_45(Color32_t2600501292  value)
	{
		___s_colorWhite_45 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_225() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveVector2_225)); }
	inline Vector2_t2156229523  get_k_LargePositiveVector2_225() const { return ___k_LargePositiveVector2_225; }
	inline Vector2_t2156229523 * get_address_of_k_LargePositiveVector2_225() { return &___k_LargePositiveVector2_225; }
	inline void set_k_LargePositiveVector2_225(Vector2_t2156229523  value)
	{
		___k_LargePositiveVector2_225 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_226() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeVector2_226)); }
	inline Vector2_t2156229523  get_k_LargeNegativeVector2_226() const { return ___k_LargeNegativeVector2_226; }
	inline Vector2_t2156229523 * get_address_of_k_LargeNegativeVector2_226() { return &___k_LargeNegativeVector2_226; }
	inline void set_k_LargeNegativeVector2_226(Vector2_t2156229523  value)
	{
		___k_LargeNegativeVector2_226 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_227() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveFloat_227)); }
	inline float get_k_LargePositiveFloat_227() const { return ___k_LargePositiveFloat_227; }
	inline float* get_address_of_k_LargePositiveFloat_227() { return &___k_LargePositiveFloat_227; }
	inline void set_k_LargePositiveFloat_227(float value)
	{
		___k_LargePositiveFloat_227 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_228() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeFloat_228)); }
	inline float get_k_LargeNegativeFloat_228() const { return ___k_LargeNegativeFloat_228; }
	inline float* get_address_of_k_LargeNegativeFloat_228() { return &___k_LargeNegativeFloat_228; }
	inline void set_k_LargeNegativeFloat_228(float value)
	{
		___k_LargeNegativeFloat_228 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_229() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveInt_229)); }
	inline int32_t get_k_LargePositiveInt_229() const { return ___k_LargePositiveInt_229; }
	inline int32_t* get_address_of_k_LargePositiveInt_229() { return &___k_LargePositiveInt_229; }
	inline void set_k_LargePositiveInt_229(int32_t value)
	{
		___k_LargePositiveInt_229 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_230() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeInt_230)); }
	inline int32_t get_k_LargeNegativeInt_230() const { return ___k_LargeNegativeInt_230; }
	inline int32_t* get_address_of_k_LargeNegativeInt_230() { return &___k_LargeNegativeInt_230; }
	inline void set_k_LargeNegativeInt_230(int32_t value)
	{
		___k_LargeNegativeInt_230 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T2599618874_H
#ifndef TMP_SUBMESHUI_T1578871311_H
#define TMP_SUBMESHUI_T1578871311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMeshUI
struct  TMP_SubMeshUI_t1578871311  : public MaskableGraphic_t3839221559
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMeshUI::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_28;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMeshUI::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_29;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_material
	Material_t340375123 * ___m_material_30;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_31;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackMaterial
	Material_t340375123 * ___m_fallbackMaterial_32;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackSourceMaterial
	Material_t340375123 * ___m_fallbackSourceMaterial_33;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_34;
	// System.Single TMPro.TMP_SubMeshUI::m_padding
	float ___m_padding_35;
	// UnityEngine.CanvasRenderer TMPro.TMP_SubMeshUI::m_canvasRenderer
	CanvasRenderer_t2598313366 * ___m_canvasRenderer_36;
	// UnityEngine.Mesh TMPro.TMP_SubMeshUI::m_mesh
	Mesh_t3648964284 * ___m_mesh_37;
	// TMPro.TextMeshProUGUI TMPro.TMP_SubMeshUI::m_TextComponent
	TextMeshProUGUI_t529313277 * ___m_TextComponent_38;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_39;
	// System.Boolean TMPro.TMP_SubMeshUI::m_materialDirty
	bool ___m_materialDirty_40;
	// System.Int32 TMPro.TMP_SubMeshUI::m_materialReferenceIndex
	int32_t ___m_materialReferenceIndex_41;

public:
	inline static int32_t get_offset_of_m_fontAsset_28() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fontAsset_28)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_28() const { return ___m_fontAsset_28; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_28() { return &___m_fontAsset_28; }
	inline void set_m_fontAsset_28(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_28), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_29() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_spriteAsset_29)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_29() const { return ___m_spriteAsset_29; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_29() { return &___m_spriteAsset_29; }
	inline void set_m_spriteAsset_29(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_29), value);
	}

	inline static int32_t get_offset_of_m_material_30() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_material_30)); }
	inline Material_t340375123 * get_m_material_30() const { return ___m_material_30; }
	inline Material_t340375123 ** get_address_of_m_material_30() { return &___m_material_30; }
	inline void set_m_material_30(Material_t340375123 * value)
	{
		___m_material_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_30), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_31() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_sharedMaterial_31)); }
	inline Material_t340375123 * get_m_sharedMaterial_31() const { return ___m_sharedMaterial_31; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_31() { return &___m_sharedMaterial_31; }
	inline void set_m_sharedMaterial_31(Material_t340375123 * value)
	{
		___m_sharedMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_31), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_32() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fallbackMaterial_32)); }
	inline Material_t340375123 * get_m_fallbackMaterial_32() const { return ___m_fallbackMaterial_32; }
	inline Material_t340375123 ** get_address_of_m_fallbackMaterial_32() { return &___m_fallbackMaterial_32; }
	inline void set_m_fallbackMaterial_32(Material_t340375123 * value)
	{
		___m_fallbackMaterial_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_32), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_33() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fallbackSourceMaterial_33)); }
	inline Material_t340375123 * get_m_fallbackSourceMaterial_33() const { return ___m_fallbackSourceMaterial_33; }
	inline Material_t340375123 ** get_address_of_m_fallbackSourceMaterial_33() { return &___m_fallbackSourceMaterial_33; }
	inline void set_m_fallbackSourceMaterial_33(Material_t340375123 * value)
	{
		___m_fallbackSourceMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_34() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_isDefaultMaterial_34)); }
	inline bool get_m_isDefaultMaterial_34() const { return ___m_isDefaultMaterial_34; }
	inline bool* get_address_of_m_isDefaultMaterial_34() { return &___m_isDefaultMaterial_34; }
	inline void set_m_isDefaultMaterial_34(bool value)
	{
		___m_isDefaultMaterial_34 = value;
	}

	inline static int32_t get_offset_of_m_padding_35() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_padding_35)); }
	inline float get_m_padding_35() const { return ___m_padding_35; }
	inline float* get_address_of_m_padding_35() { return &___m_padding_35; }
	inline void set_m_padding_35(float value)
	{
		___m_padding_35 = value;
	}

	inline static int32_t get_offset_of_m_canvasRenderer_36() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_canvasRenderer_36)); }
	inline CanvasRenderer_t2598313366 * get_m_canvasRenderer_36() const { return ___m_canvasRenderer_36; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_canvasRenderer_36() { return &___m_canvasRenderer_36; }
	inline void set_m_canvasRenderer_36(CanvasRenderer_t2598313366 * value)
	{
		___m_canvasRenderer_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_36), value);
	}

	inline static int32_t get_offset_of_m_mesh_37() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_mesh_37)); }
	inline Mesh_t3648964284 * get_m_mesh_37() const { return ___m_mesh_37; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_37() { return &___m_mesh_37; }
	inline void set_m_mesh_37(Mesh_t3648964284 * value)
	{
		___m_mesh_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_37), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_38() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_TextComponent_38)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextComponent_38() const { return ___m_TextComponent_38; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextComponent_38() { return &___m_TextComponent_38; }
	inline void set_m_TextComponent_38(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextComponent_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_38), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_39() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_isRegisteredForEvents_39)); }
	inline bool get_m_isRegisteredForEvents_39() const { return ___m_isRegisteredForEvents_39; }
	inline bool* get_address_of_m_isRegisteredForEvents_39() { return &___m_isRegisteredForEvents_39; }
	inline void set_m_isRegisteredForEvents_39(bool value)
	{
		___m_isRegisteredForEvents_39 = value;
	}

	inline static int32_t get_offset_of_m_materialDirty_40() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_materialDirty_40)); }
	inline bool get_m_materialDirty_40() const { return ___m_materialDirty_40; }
	inline bool* get_address_of_m_materialDirty_40() { return &___m_materialDirty_40; }
	inline void set_m_materialDirty_40(bool value)
	{
		___m_materialDirty_40 = value;
	}

	inline static int32_t get_offset_of_m_materialReferenceIndex_41() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_materialReferenceIndex_41)); }
	inline int32_t get_m_materialReferenceIndex_41() const { return ___m_materialReferenceIndex_41; }
	inline int32_t* get_address_of_m_materialReferenceIndex_41() { return &___m_materialReferenceIndex_41; }
	inline void set_m_materialReferenceIndex_41(int32_t value)
	{
		___m_materialReferenceIndex_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESHUI_T1578871311_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (SpriteAssetImportFormats_t116390639)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2000[3] = 
{
	SpriteAssetImportFormats_t116390639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (TexturePacker_t3148178657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (SpriteFrame_t3912389194)+ sizeof (RuntimeObject), sizeof(SpriteFrame_t3912389194 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2002[4] = 
{
	SpriteFrame_t3912389194::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (SpriteSize_t3355290999)+ sizeof (RuntimeObject), sizeof(SpriteSize_t3355290999 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[2] = 
{
	SpriteSize_t3355290999::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t3355290999::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (SpriteData_t3048397587)+ sizeof (RuntimeObject), sizeof(SpriteData_t3048397587_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[7] = 
{
	SpriteData_t3048397587::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (SpriteDataObject_t308163541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[1] = 
{
	SpriteDataObject_t308163541::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (TMP_Style_t3479380764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[6] = 
{
	TMP_Style_t3479380764::get_offset_of_m_Name_0(),
	TMP_Style_t3479380764::get_offset_of_m_HashCode_1(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningDefinition_2(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingDefinition_3(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningTagArray_4(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingTagArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (TMP_StyleSheet_t917564226), -1, sizeof(TMP_StyleSheet_t917564226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2007[3] = 
{
	TMP_StyleSheet_t917564226_StaticFields::get_offset_of_s_Instance_2(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleList_3(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (TMP_SubMesh_t2613037997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[14] = 
{
	TMP_SubMesh_t2613037997::get_offset_of_m_fontAsset_2(),
	TMP_SubMesh_t2613037997::get_offset_of_m_spriteAsset_3(),
	TMP_SubMesh_t2613037997::get_offset_of_m_material_4(),
	TMP_SubMesh_t2613037997::get_offset_of_m_sharedMaterial_5(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackMaterial_6(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackSourceMaterial_7(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isDefaultMaterial_8(),
	TMP_SubMesh_t2613037997::get_offset_of_m_padding_9(),
	TMP_SubMesh_t2613037997::get_offset_of_m_renderer_10(),
	TMP_SubMesh_t2613037997::get_offset_of_m_meshFilter_11(),
	TMP_SubMesh_t2613037997::get_offset_of_m_mesh_12(),
	TMP_SubMesh_t2613037997::get_offset_of_m_boxCollider_13(),
	TMP_SubMesh_t2613037997::get_offset_of_m_TextComponent_14(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isRegisteredForEvents_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (TMP_SubMeshUI_t1578871311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[14] = 
{
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fontAsset_28(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_spriteAsset_29(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_material_30(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_sharedMaterial_31(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackMaterial_32(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackSourceMaterial_33(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isDefaultMaterial_34(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_padding_35(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_canvasRenderer_36(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_mesh_37(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_TextComponent_38(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isRegisteredForEvents_39(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialDirty_40(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialReferenceIndex_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (TextAlignmentOptions_t4036791236)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2011[37] = 
{
	TextAlignmentOptions_t4036791236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (_HorizontalAlignmentOptions_t2379014378)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[7] = 
{
	_HorizontalAlignmentOptions_t2379014378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (_VerticalAlignmentOptions_t2825528260)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2013[7] = 
{
	_VerticalAlignmentOptions_t2825528260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (TextRenderFlags_t2418684345)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[3] = 
{
	TextRenderFlags_t2418684345::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (TMP_TextElementType_t1276645592)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2015[3] = 
{
	TMP_TextElementType_t1276645592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (MaskingTypes_t3687969768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2016[4] = 
{
	MaskingTypes_t3687969768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (TextOverflowModes_t1430035314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2017[8] = 
{
	TextOverflowModes_t1430035314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (MaskingOffsetMode_t2266644590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[3] = 
{
	MaskingOffsetMode_t2266644590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (TextureMappingOptions_t270963663)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2019[5] = 
{
	TextureMappingOptions_t270963663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (FontStyles_t3828945032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2020[12] = 
{
	FontStyles_t3828945032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (FontWeights_t3122883458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2021[10] = 
{
	FontWeights_t3122883458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (TagUnits_t1169424683)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2022[4] = 
{
	TagUnits_t1169424683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (TagType_t123236451)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2023[5] = 
{
	TagType_t123236451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (TMP_Text_t2599618874), -1, sizeof(TMP_Text_t2599618874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2024[203] = 
{
	TMP_Text_t2599618874::get_offset_of_m_text_28(),
	TMP_Text_t2599618874::get_offset_of_m_isRightToLeft_29(),
	TMP_Text_t2599618874::get_offset_of_m_fontAsset_30(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontAsset_31(),
	TMP_Text_t2599618874::get_offset_of_m_isSDFShader_32(),
	TMP_Text_t2599618874::get_offset_of_m_sharedMaterial_33(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterial_34(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferences_35(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceIndexLookup_36(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceStack_37(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterialIndex_38(),
	TMP_Text_t2599618874::get_offset_of_m_fontSharedMaterials_39(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterial_40(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterials_41(),
	TMP_Text_t2599618874::get_offset_of_m_isMaterialDirty_42(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor32_43(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor_44(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_s_colorWhite_45(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColor_46(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColor_47(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColor_48(),
	TMP_Text_t2599618874::get_offset_of_m_enableVertexGradient_49(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradient_50(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradientPreset_51(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAsset_52(),
	TMP_Text_t2599618874::get_offset_of_m_tintAllSprites_53(),
	TMP_Text_t2599618874::get_offset_of_m_tintSprite_54(),
	TMP_Text_t2599618874::get_offset_of_m_spriteColor_55(),
	TMP_Text_t2599618874::get_offset_of_m_overrideHtmlColors_56(),
	TMP_Text_t2599618874::get_offset_of_m_faceColor_57(),
	TMP_Text_t2599618874::get_offset_of_m_outlineColor_58(),
	TMP_Text_t2599618874::get_offset_of_m_outlineWidth_59(),
	TMP_Text_t2599618874::get_offset_of_m_fontSize_60(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontSize_61(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeBase_62(),
	TMP_Text_t2599618874::get_offset_of_m_sizeStack_63(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeight_64(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightInternal_65(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightStack_66(),
	TMP_Text_t2599618874::get_offset_of_m_enableAutoSizing_67(),
	TMP_Text_t2599618874::get_offset_of_m_maxFontSize_68(),
	TMP_Text_t2599618874::get_offset_of_m_minFontSize_69(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMin_70(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMax_71(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyle_72(),
	TMP_Text_t2599618874::get_offset_of_m_style_73(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyleStack_74(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingBold_75(),
	TMP_Text_t2599618874::get_offset_of_m_textAlignment_76(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustification_77(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustificationStack_78(),
	TMP_Text_t2599618874::get_offset_of_m_textContainerLocalCorners_79(),
	TMP_Text_t2599618874::get_offset_of_m_isAlignmentEnumConverted_80(),
	TMP_Text_t2599618874::get_offset_of_m_characterSpacing_81(),
	TMP_Text_t2599618874::get_offset_of_m_cSpacing_82(),
	TMP_Text_t2599618874::get_offset_of_m_monoSpacing_83(),
	TMP_Text_t2599618874::get_offset_of_m_wordSpacing_84(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacing_85(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingDelta_86(),
	TMP_Text_t2599618874::get_offset_of_m_lineHeight_87(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingMax_88(),
	TMP_Text_t2599618874::get_offset_of_m_paragraphSpacing_89(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthMaxAdj_90(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthAdjDelta_91(),
	TMP_Text_t2599618874::get_offset_of_m_enableWordWrapping_92(),
	TMP_Text_t2599618874::get_offset_of_m_isCharacterWrappingEnabled_93(),
	TMP_Text_t2599618874::get_offset_of_m_isNonBreakingSpace_94(),
	TMP_Text_t2599618874::get_offset_of_m_isIgnoringAlignment_95(),
	TMP_Text_t2599618874::get_offset_of_m_wordWrappingRatios_96(),
	TMP_Text_t2599618874::get_offset_of_m_overflowMode_97(),
	TMP_Text_t2599618874::get_offset_of_m_firstOverflowCharacterIndex_98(),
	TMP_Text_t2599618874::get_offset_of_m_linkedTextComponent_99(),
	TMP_Text_t2599618874::get_offset_of_m_isLinkedTextComponent_100(),
	TMP_Text_t2599618874::get_offset_of_m_isTextTruncated_101(),
	TMP_Text_t2599618874::get_offset_of_m_enableKerning_102(),
	TMP_Text_t2599618874::get_offset_of_m_enableExtraPadding_103(),
	TMP_Text_t2599618874::get_offset_of_checkPaddingRequired_104(),
	TMP_Text_t2599618874::get_offset_of_m_isRichText_105(),
	TMP_Text_t2599618874::get_offset_of_m_parseCtrlCharacters_106(),
	TMP_Text_t2599618874::get_offset_of_m_isOverlay_107(),
	TMP_Text_t2599618874::get_offset_of_m_isOrthographic_108(),
	TMP_Text_t2599618874::get_offset_of_m_isCullingEnabled_109(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreRectMaskCulling_110(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreCulling_111(),
	TMP_Text_t2599618874::get_offset_of_m_horizontalMapping_112(),
	TMP_Text_t2599618874::get_offset_of_m_verticalMapping_113(),
	TMP_Text_t2599618874::get_offset_of_m_uvLineOffset_114(),
	TMP_Text_t2599618874::get_offset_of_m_renderMode_115(),
	TMP_Text_t2599618874::get_offset_of_m_geometrySortingOrder_116(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacter_117(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleCharacters_118(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleWords_119(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleLines_120(),
	TMP_Text_t2599618874::get_offset_of_m_useMaxVisibleDescender_121(),
	TMP_Text_t2599618874::get_offset_of_m_pageToDisplay_122(),
	TMP_Text_t2599618874::get_offset_of_m_isNewPage_123(),
	TMP_Text_t2599618874::get_offset_of_m_margin_124(),
	TMP_Text_t2599618874::get_offset_of_m_marginLeft_125(),
	TMP_Text_t2599618874::get_offset_of_m_marginRight_126(),
	TMP_Text_t2599618874::get_offset_of_m_marginWidth_127(),
	TMP_Text_t2599618874::get_offset_of_m_marginHeight_128(),
	TMP_Text_t2599618874::get_offset_of_m_width_129(),
	TMP_Text_t2599618874::get_offset_of_m_textInfo_130(),
	TMP_Text_t2599618874::get_offset_of_m_havePropertiesChanged_131(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingLegacyAnimationComponent_132(),
	TMP_Text_t2599618874::get_offset_of_m_transform_133(),
	TMP_Text_t2599618874::get_offset_of_m_rectTransform_134(),
	TMP_Text_t2599618874::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135(),
	TMP_Text_t2599618874::get_offset_of_m_autoSizeTextContainer_136(),
	TMP_Text_t2599618874::get_offset_of_m_mesh_137(),
	TMP_Text_t2599618874::get_offset_of_m_isVolumetricText_138(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimator_139(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleHeight_140(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleWidth_141(),
	TMP_Text_t2599618874::get_offset_of_m_minWidth_142(),
	TMP_Text_t2599618874::get_offset_of_m_minHeight_143(),
	TMP_Text_t2599618874::get_offset_of_m_maxWidth_144(),
	TMP_Text_t2599618874::get_offset_of_m_maxHeight_145(),
	TMP_Text_t2599618874::get_offset_of_m_LayoutElement_146(),
	TMP_Text_t2599618874::get_offset_of_m_preferredWidth_147(),
	TMP_Text_t2599618874::get_offset_of_m_renderedWidth_148(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredWidthDirty_149(),
	TMP_Text_t2599618874::get_offset_of_m_preferredHeight_150(),
	TMP_Text_t2599618874::get_offset_of_m_renderedHeight_151(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredHeightDirty_152(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculatingPreferredValues_153(),
	TMP_Text_t2599618874::get_offset_of_m_recursiveCount_154(),
	TMP_Text_t2599618874::get_offset_of_m_layoutPriority_155(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculateSizeRequired_156(),
	TMP_Text_t2599618874::get_offset_of_m_isLayoutDirty_157(),
	TMP_Text_t2599618874::get_offset_of_m_verticesAlreadyDirty_158(),
	TMP_Text_t2599618874::get_offset_of_m_layoutAlreadyDirty_159(),
	TMP_Text_t2599618874::get_offset_of_m_isAwake_160(),
	TMP_Text_t2599618874::get_offset_of_m_isInputParsingRequired_161(),
	TMP_Text_t2599618874::get_offset_of_m_inputSource_162(),
	TMP_Text_t2599618874::get_offset_of_old_text_163(),
	TMP_Text_t2599618874::get_offset_of_m_fontScale_164(),
	TMP_Text_t2599618874::get_offset_of_m_fontScaleMultiplier_165(),
	TMP_Text_t2599618874::get_offset_of_m_htmlTag_166(),
	TMP_Text_t2599618874::get_offset_of_m_xmlAttribute_167(),
	TMP_Text_t2599618874::get_offset_of_m_attributeParameterValues_168(),
	TMP_Text_t2599618874::get_offset_of_tag_LineIndent_169(),
	TMP_Text_t2599618874::get_offset_of_tag_Indent_170(),
	TMP_Text_t2599618874::get_offset_of_m_indentStack_171(),
	TMP_Text_t2599618874::get_offset_of_tag_NoParsing_172(),
	TMP_Text_t2599618874::get_offset_of_m_isParsingText_173(),
	TMP_Text_t2599618874::get_offset_of_m_FXMatrix_174(),
	TMP_Text_t2599618874::get_offset_of_m_isFXMatrixSet_175(),
	TMP_Text_t2599618874::get_offset_of_m_char_buffer_176(),
	TMP_Text_t2599618874::get_offset_of_m_internalCharacterInfo_177(),
	TMP_Text_t2599618874::get_offset_of_m_input_CharArray_178(),
	TMP_Text_t2599618874::get_offset_of_m_charArray_Length_179(),
	TMP_Text_t2599618874::get_offset_of_m_totalCharacterCount_180(),
	TMP_Text_t2599618874::get_offset_of_m_SavedWordWrapState_181(),
	TMP_Text_t2599618874::get_offset_of_m_SavedLineState_182(),
	TMP_Text_t2599618874::get_offset_of_m_characterCount_183(),
	TMP_Text_t2599618874::get_offset_of_m_firstCharacterOfLine_184(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacterOfLine_185(),
	TMP_Text_t2599618874::get_offset_of_m_lastCharacterOfLine_186(),
	TMP_Text_t2599618874::get_offset_of_m_lastVisibleCharacterOfLine_187(),
	TMP_Text_t2599618874::get_offset_of_m_lineNumber_188(),
	TMP_Text_t2599618874::get_offset_of_m_lineVisibleCharacterCount_189(),
	TMP_Text_t2599618874::get_offset_of_m_pageNumber_190(),
	TMP_Text_t2599618874::get_offset_of_m_maxAscender_191(),
	TMP_Text_t2599618874::get_offset_of_m_maxCapHeight_192(),
	TMP_Text_t2599618874::get_offset_of_m_maxDescender_193(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineAscender_194(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineDescender_195(),
	TMP_Text_t2599618874::get_offset_of_m_startOfLineAscender_196(),
	TMP_Text_t2599618874::get_offset_of_m_lineOffset_197(),
	TMP_Text_t2599618874::get_offset_of_m_meshExtents_198(),
	TMP_Text_t2599618874::get_offset_of_m_htmlColor_199(),
	TMP_Text_t2599618874::get_offset_of_m_colorStack_200(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColorStack_201(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColorStack_202(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColorStack_203(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientPreset_204(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientStack_205(),
	TMP_Text_t2599618874::get_offset_of_m_tabSpacing_206(),
	TMP_Text_t2599618874::get_offset_of_m_spacing_207(),
	TMP_Text_t2599618874::get_offset_of_m_styleStack_208(),
	TMP_Text_t2599618874::get_offset_of_m_actionStack_209(),
	TMP_Text_t2599618874::get_offset_of_m_padding_210(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffset_211(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffsetStack_212(),
	TMP_Text_t2599618874::get_offset_of_m_xAdvance_213(),
	TMP_Text_t2599618874::get_offset_of_m_textElementType_214(),
	TMP_Text_t2599618874::get_offset_of_m_cached_TextElement_215(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Underline_GlyphInfo_216(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Ellipsis_GlyphInfo_217(),
	TMP_Text_t2599618874::get_offset_of_m_defaultSpriteAsset_218(),
	TMP_Text_t2599618874::get_offset_of_m_currentSpriteAsset_219(),
	TMP_Text_t2599618874::get_offset_of_m_spriteCount_220(),
	TMP_Text_t2599618874::get_offset_of_m_spriteIndex_221(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimationID_222(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreActiveState_223(),
	TMP_Text_t2599618874::get_offset_of_k_Power_224(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveVector2_225(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeVector2_226(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveFloat_227(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeFloat_228(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveInt_229(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeInt_230(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (TextInputSources_t1522115805)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2025[5] = 
{
	TextInputSources_t1522115805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (TMP_TextElement_t129727469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[9] = 
{
	TMP_TextElement_t129727469::get_offset_of_id_0(),
	TMP_TextElement_t129727469::get_offset_of_x_1(),
	TMP_TextElement_t129727469::get_offset_of_y_2(),
	TMP_TextElement_t129727469::get_offset_of_width_3(),
	TMP_TextElement_t129727469::get_offset_of_height_4(),
	TMP_TextElement_t129727469::get_offset_of_xOffset_5(),
	TMP_TextElement_t129727469::get_offset_of_yOffset_6(),
	TMP_TextElement_t129727469::get_offset_of_xAdvance_7(),
	TMP_TextElement_t129727469::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (TMP_TextInfo_t3598145122), -1, sizeof(TMP_TextInfo_t3598145122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2027[18] = 
{
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t3598145122::get_offset_of_textComponent_2(),
	TMP_TextInfo_t3598145122::get_offset_of_characterCount_3(),
	TMP_TextInfo_t3598145122::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t3598145122::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t3598145122::get_offset_of_wordCount_6(),
	TMP_TextInfo_t3598145122::get_offset_of_linkCount_7(),
	TMP_TextInfo_t3598145122::get_offset_of_lineCount_8(),
	TMP_TextInfo_t3598145122::get_offset_of_pageCount_9(),
	TMP_TextInfo_t3598145122::get_offset_of_materialCount_10(),
	TMP_TextInfo_t3598145122::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t3598145122::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t3598145122::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t3598145122::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t3598145122::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t3598145122::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t3598145122::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (CaretPosition_t3997512201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[4] = 
{
	CaretPosition_t3997512201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (CaretInfo_t841780893)+ sizeof (RuntimeObject), sizeof(CaretInfo_t841780893 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2029[2] = 
{
	CaretInfo_t841780893::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_t841780893::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (TMP_TextUtilities_t2105690005), -1, sizeof(TMP_TextUtilities_t2105690005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2030[3] = 
{
	TMP_TextUtilities_t2105690005_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (LineSegment_t1526544958)+ sizeof (RuntimeObject), sizeof(LineSegment_t1526544958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[2] = 
{
	LineSegment_t1526544958::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t1526544958::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (TMP_UpdateManager_t4114267509), -1, sizeof(TMP_UpdateManager_t4114267509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2032[5] = 
{
	TMP_UpdateManager_t4114267509_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (TMP_UpdateRegistry_t461608481), -1, sizeof(TMP_UpdateRegistry_t461608481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2033[5] = 
{
	TMP_UpdateRegistry_t461608481_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (TMP_BasicXmlTagStack_t2962628096)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_t2962628096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[10] = 
{
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (U24ArrayTypeU3D40_t2865632059)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2865632059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (AndroidBridge_t2441337961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[12] = 
{
	AndroidBridge_t2441337961::get_offset_of_androidClass_0(),
	AndroidBridge_t2441337961::get_offset_of_androidJNIType_1(),
	AndroidBridge_t2441337961::get_offset_of_androidActivity_2(),
	AndroidBridge_t2441337961::get_offset_of__callMethod_3(),
	AndroidBridge_t2441337961::get_offset_of__callStringMethod_4(),
	AndroidBridge_t2441337961::get_offset_of__callFloatArrayMethod_5(),
	AndroidBridge_t2441337961::get_offset_of__callLongArrayMethod_6(),
	AndroidBridge_t2441337961::get_offset_of__callIntMethod_7(),
	AndroidBridge_t2441337961::get_offset_of__callFloatMethod_8(),
	AndroidBridge_t2441337961::get_offset_of__callBoolMethod_9(),
	AndroidBridge_t2441337961::get_offset_of__callLongMethod_10(),
	AndroidBridge_t2441337961::get_offset_of__nativeFramePtr_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (iOSBridge_t3849146092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[5] = 
{
	iOSBridge_t3849146092::get_offset_of__screenOrientation_0(),
	iOSBridge_t3849146092::get_offset_of__cachedTargetIdBuffer_1(),
	iOSBridge_t3849146092::get_offset_of__cachedProjectionMatrix_2(),
	iOSBridge_t3849146092::get_offset_of__cachedModelViewMatrices_3(),
	iOSBridge_t3849146092::get_offset_of__nativeFramePtr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (UnityBridge_t2504510425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[10] = 
{
	UnityBridge_t2504510425::get_offset_of_GenericTrackingMatrices_0(),
	UnityBridge_t2504510425::get_offset_of_GenericProjectionMatrix_1(),
	UnityBridge_t2504510425::get_offset_of_ObjectTrackingMatrix_2(),
	UnityBridge_t2504510425::get_offset_of_ObjectProjectionMatrix_3(),
	UnityBridge_t2504510425::get_offset_of_PhysicalTargetHeights_4(),
	UnityBridge_t2504510425::get_offset_of_TargetIDs_5(),
	UnityBridge_t2504510425::get_offset_of_TrackedTargets_6(),
	UnityBridge_t2504510425::get_offset_of_TargetCount_7(),
	UnityBridge_t2504510425::get_offset_of__isObjectTrackingRunning_8(),
	UnityBridge_t2504510425::get_offset_of__trackerManagerName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (WikitudeBridge_t4110292378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[2] = 
{
	WikitudeBridge_t4110292378::get_offset_of__trackerManager_0(),
	WikitudeBridge_t4110292378::get_offset_of__trackerManagerGameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (UnityEditorSimulator_t2033215626), -1, sizeof(UnityEditorSimulator_t2033215626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2045[2] = 
{
	UnityEditorSimulator_t2033215626_StaticFields::get_offset_of__defaultTrackingMatrix_0(),
	UnityEditorSimulator_t2033215626_StaticFields::get_offset_of__currentTargets_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (SimulatedTarget_t1564170948)+ sizeof (RuntimeObject), sizeof(SimulatedTarget_t1564170948_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2046[4] = 
{
	SimulatedTarget_t1564170948::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimulatedTarget_t1564170948::get_offset_of_UniqueId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimulatedTarget_t1564170948::get_offset_of_Offset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimulatedTarget_t1564170948::get_offset_of_PhysicalTargetHeight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (ImageTrackable_t2462741734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[9] = 
{
	ImageTrackable_t2462741734::get_offset_of__extendedTracking_10(),
	ImageTrackable_t2462741734::get_offset_of__targetsForExtendedTracking_11(),
	ImageTrackable_t2462741734::get_offset_of_OnImageRecognized_12(),
	ImageTrackable_t2462741734::get_offset_of_OnImageLost_13(),
	ImageTrackable_t2462741734::get_offset_of_U3CPreviewU3Ek__BackingField_14(),
	ImageTrackable_t2462741734::get_offset_of_U3CImageTargetHeightU3Ek__BackingField_15(),
	ImageTrackable_t2462741734::get_offset_of__previewMaterial_16(),
	ImageTrackable_t2462741734::get_offset_of__previewMesh_17(),
	ImageTrackable_t2462741734::get_offset_of__wikitudeCamera_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (OnImageRecognizedEvent_t773132985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (OnImageLostEvent_t966623312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (ExtendedTrackingQuality_t364496425)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2050[4] = 
{
	ExtendedTrackingQuality_t364496425::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (ImageRecognitionRangeExtension_t604843042)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[4] = 
{
	ImageRecognitionRangeExtension_t604843042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ImageTracker_t271395264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[8] = 
{
	ImageTracker_t271395264::get_offset_of_OnExtendedTrackingQualityChanged_20(),
	ImageTracker_t271395264::get_offset_of__targetSourceType_21(),
	ImageTracker_t271395264::get_offset_of__targetCollectionResource_22(),
	ImageTracker_t271395264::get_offset_of__cloudRecognitionService_23(),
	ImageTracker_t271395264::get_offset_of__maximumNumberOfConcurrentTrackableTargets_24(),
	ImageTracker_t271395264::get_offset_of__rangeExtension_25(),
	ImageTracker_t271395264::get_offset_of_U3CPhysicalTargetImageHeightsU3Ek__BackingField_26(),
	ImageTracker_t271395264::get_offset_of_U3CIsRegisteredU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (OnExtendedTrackingQualityChangedEvent_t2560195974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (InstantTrackable_t3187174337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[4] = 
{
	InstantTrackable_t3187174337::get_offset_of_OnInitializationStarted_10(),
	InstantTrackable_t3187174337::get_offset_of_OnInitializationStopped_11(),
	InstantTrackable_t3187174337::get_offset_of_OnSceneRecognized_12(),
	InstantTrackable_t3187174337::get_offset_of_OnSceneLost_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (OnInitializationStartedEvent_t272217273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (OnInitializationStoppedEvent_t4207911327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (OnSceneRecognizedEvent_t2416994635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (OnSceneLostEvent_t389101256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (InstantTrackingPlaneOrientation_t1439566444)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2059[4] = 
{
	InstantTrackingPlaneOrientation_t1439566444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (InstantTrackingState_t3973410783)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2060[3] = 
{
	InstantTrackingState_t3973410783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (InstantTracker_t684352120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[6] = 
{
	InstantTracker_t684352120::get_offset_of_OnStateChanged_20(),
	InstantTracker_t684352120::get_offset_of__deviceHeightAboveGround_21(),
	InstantTracker_t684352120::get_offset_of_OnScreenConversionComputed_22(),
	InstantTracker_t684352120::get_offset_of__trackingPlaneOrientation_23(),
	InstantTracker_t684352120::get_offset_of__trackingPlaneOrientationAngle_24(),
	InstantTracker_t684352120::get_offset_of_CurrentState_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (OnStateChangedEvent_t1359267737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (OnScreenConversionComputedEvent_t608500589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (ObjectTrackable_t3378627079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[4] = 
{
	ObjectTrackable_t3378627079::get_offset_of_OnObjectRecognized_10(),
	ObjectTrackable_t3378627079::get_offset_of_OnObjectLost_11(),
	ObjectTrackable_t3378627079::get_offset_of__pointCloud_12(),
	ObjectTrackable_t3378627079::get_offset_of__mapRenderer_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (OnObjectRecognizedEvent_t3443511796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (OnObjectLostEvent_t460465675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (ObjectTracker_t3006704158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[2] = 
{
	ObjectTracker_t3006704158::get_offset_of__targetCollectionResource_20(),
	ObjectTracker_t3006704158::get_offset_of_U3CIsRegisteredU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (PluginManager_t1017991214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[3] = 
{
	PluginManager_t1017991214::get_offset_of_OnCameraFrameAvailable_2(),
	PluginManager_t1017991214::get_offset_of_OnPluginFailure_3(),
	PluginManager_t1017991214::get_offset_of__bridge_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (OnCameraFrameAvailableEvent_t3843633858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (OnPluginFailureEvent_t2376488508), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Trackable_t347424808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[8] = 
{
	Trackable_t347424808::get_offset_of__targetPattern_2(),
	Trackable_t347424808::get_offset_of__targetPatternRegex_3(),
	Trackable_t347424808::get_offset_of__drawable_4(),
	Trackable_t347424808::get_offset_of__autoToggleVisibility_5(),
	Trackable_t347424808::get_offset_of__registeredToTracker_6(),
	Trackable_t347424808::get_offset_of__tracker_7(),
	Trackable_t347424808::get_offset_of__activeDrawables_8(),
	Trackable_t347424808::get_offset_of_UpdatedTransform_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (TrackerBehaviour_t921360922), -1, sizeof(TrackerBehaviour_t921360922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2072[18] = 
{
	TrackerBehaviour_t921360922::get_offset_of__manager_2(),
	TrackerBehaviour_t921360922::get_offset_of__registeredTrackables_3(),
	TrackerBehaviour_t921360922::get_offset_of__anchorTarget_4(),
	TrackerBehaviour_t921360922::get_offset_of__anchorPositionOffset_5(),
	TrackerBehaviour_t921360922::get_offset_of__anchorRotationOffset_6(),
	TrackerBehaviour_t921360922::get_offset_of__legacyTrackables_7(),
	TrackerBehaviour_t921360922_StaticFields::get_offset_of__conversion_8(),
	TrackerBehaviour_t921360922_StaticFields::get_offset_of__modelMatrix_9(),
	TrackerBehaviour_t921360922::get_offset_of__cachedTrackedTargets_10(),
	TrackerBehaviour_t921360922::get_offset_of__cachedTargetsToAdd_11(),
	TrackerBehaviour_t921360922::get_offset_of__cachedTargetsToRemove_12(),
	TrackerBehaviour_t921360922::get_offset_of_U3CInitializedU3Ek__BackingField_13(),
	TrackerBehaviour_t921360922::get_offset_of__sceneCamera_14(),
	TrackerBehaviour_t921360922::get_offset_of__initialSceneCameraPosition_15(),
	TrackerBehaviour_t921360922::get_offset_of__initialSceneCameraRotation_16(),
	TrackerBehaviour_t921360922::get_offset_of__wikitudeCamera_17(),
	TrackerBehaviour_t921360922::get_offset_of_OnTargetsLoaded_18(),
	TrackerBehaviour_t921360922::get_offset_of_OnErrorLoadingTargets_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (OnTargetsLoadedEvent_t1820866191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (OnErrorLoadingTargetsEvent_t394526247), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (TrackerManager_t2102804711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[11] = 
{
	TrackerManager_t2102804711::get_offset_of__targetCollectionResourceId_2(),
	TrackerManager_t2102804711::get_offset_of__cloudRecognitionServiceId_3(),
	TrackerManager_t2102804711::get_offset_of__registeredResources_4(),
	TrackerManager_t2102804711::get_offset_of__registeredCloudRecognitionServices_5(),
	TrackerManager_t2102804711::get_offset_of__recognizedTargets_6(),
	TrackerManager_t2102804711::get_offset_of__updatedTargets_7(),
	TrackerManager_t2102804711::get_offset_of__cachedSplitString_8(),
	TrackerManager_t2102804711::get_offset_of__bridge_9(),
	TrackerManager_t2102804711::get_offset_of__registeredTrackers_10(),
	TrackerManager_t2102804711::get_offset_of__activeTracker_11(),
	TrackerManager_t2102804711::get_offset_of__wikitudeCamera_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (BridgeError_t1304078858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[3] = 
{
	BridgeError_t1304078858::get_offset_of_U3CCodeU3Ek__BackingField_0(),
	BridgeError_t1304078858::get_offset_of_U3CMessageU3Ek__BackingField_1(),
	BridgeError_t1304078858::get_offset_of_U3CIdentifierU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (CloudRecognitionServerRegion_t2146108836)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2077[4] = 
{
	CloudRecognitionServerRegion_t2146108836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (CaptureDevicePosition_t3635123692)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2078[3] = 
{
	CaptureDevicePosition_t3635123692::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (CaptureFocusMode_t3452326368)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2079[4] = 
{
	CaptureFocusMode_t3452326368::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (CaptureAutoFocusRestriction_t4132688021)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[4] = 
{
	CaptureAutoFocusRestriction_t4132688021::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (CaptureFlashMode_t2444581339)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2081[3] = 
{
	CaptureFlashMode_t2444581339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (CaptureDeviceResolution_t2690595878)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2082[5] = 
{
	CaptureDeviceResolution_t2690595878::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (CaptureDeviceFramerate_t2114957004)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[4] = 
{
	CaptureDeviceFramerate_t2114957004::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (CaptureExposureMode_t1315396563)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	CaptureExposureMode_t1315396563::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (CameraInfo_t2490161545)+ sizeof (RuntimeObject), sizeof(CameraInfo_t2490161545 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[3] = 
{
	CameraInfo_t2490161545::get_offset_of_Width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraInfo_t2490161545::get_offset_of_Height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraInfo_t2490161545::get_offset_of_Framerate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (WikitudeCamera_t2188881370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[33] = 
{
	WikitudeCamera_t2188881370::get_offset_of__WikitudeLicenseKey_2(),
	WikitudeCamera_t2188881370::get_offset_of__cameraTexture_3(),
	WikitudeCamera_t2188881370::get_offset_of__cachedDevicePosition_4(),
	WikitudeCamera_t2188881370::get_offset_of__cachedFocusMode_5(),
	WikitudeCamera_t2188881370::get_offset_of__cachedAutoFocusRestriction_6(),
	WikitudeCamera_t2188881370::get_offset_of__cachedZoomLevel_7(),
	WikitudeCamera_t2188881370::get_offset_of__cachedFlashMode_8(),
	WikitudeCamera_t2188881370::get_offset_of__desiredDeviceResolution_9(),
	WikitudeCamera_t2188881370::get_offset_of__desiredDeviceFramerate_10(),
	WikitudeCamera_t2188881370::get_offset_of__enableCamera2_11(),
	WikitudeCamera_t2188881370::get_offset_of__enableCameraRendering_12(),
	WikitudeCamera_t2188881370::get_offset_of__staticCamera_13(),
	WikitudeCamera_t2188881370::get_offset_of__enableInputPlugin_14(),
	WikitudeCamera_t2188881370::get_offset_of__enableMirroring_15(),
	WikitudeCamera_t2188881370::get_offset_of__invertedFrame_16(),
	WikitudeCamera_t2188881370::get_offset_of__mirroredFrame_17(),
	WikitudeCamera_t2188881370::get_offset_of__inputFrameColorSpace_18(),
	WikitudeCamera_t2188881370::get_offset_of__horizontalAngle_19(),
	WikitudeCamera_t2188881370::get_offset_of__inputFrameWidth_20(),
	WikitudeCamera_t2188881370::get_offset_of__inputFrameHeight_21(),
	WikitudeCamera_t2188881370::get_offset_of__requestInputFrameRendering_22(),
	WikitudeCamera_t2188881370::get_offset_of_OnInputPluginRegistered_23(),
	WikitudeCamera_t2188881370::get_offset_of_OnInputPluginFailure_24(),
	WikitudeCamera_t2188881370::get_offset_of_OnCameraFailure_25(),
	WikitudeCamera_t2188881370::get_offset_of__inputPluginRegistered_26(),
	WikitudeCamera_t2188881370::get_offset_of__cameraOpened_27(),
	WikitudeCamera_t2188881370::get_offset_of__bridge_28(),
	WikitudeCamera_t2188881370::get_offset_of__backgroundCamera_29(),
	WikitudeCamera_t2188881370::get_offset_of__blackPixels_30(),
	WikitudeCamera_t2188881370::get_offset_of__calibrationPositionOffset_31(),
	WikitudeCamera_t2188881370::get_offset_of__calibrationRotationOffset_32(),
	WikitudeCamera_t2188881370::get_offset_of__ignoreTrackableScale_33(),
	WikitudeCamera_t2188881370::get_offset_of_ActiveOverride_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (OnCameraFailureEvent_t2345510068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (OnInputPluginFailureEvent_t818001259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (BackgroundCamera_t2368011250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[1] = 
{
	BackgroundCamera_t2368011250::get_offset_of_WikitudeCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (CloudRecognitionServiceResponse_t1770101589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[3] = 
{
	CloudRecognitionServiceResponse_t1770101589::get_offset_of_U3CRecognizedU3Ek__BackingField_0(),
	CloudRecognitionServiceResponse_t1770101589::get_offset_of_U3CInfoU3Ek__BackingField_1(),
	CloudRecognitionServiceResponse_t1770101589::get_offset_of_U3CMetadataU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (CloudRecognitionService_t3866570042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[10] = 
{
	CloudRecognitionService_t3866570042::get_offset_of__clientToken_2(),
	CloudRecognitionService_t3866570042::get_offset_of__targetCollectionId_3(),
	CloudRecognitionService_t3866570042::get_offset_of__serverRegion_4(),
	CloudRecognitionService_t3866570042::get_offset_of__customServerURL_5(),
	CloudRecognitionService_t3866570042::get_offset_of_OnInitialized_6(),
	CloudRecognitionService_t3866570042::get_offset_of_OnInitializationError_7(),
	CloudRecognitionService_t3866570042::get_offset_of_OnRecognitionResponse_8(),
	CloudRecognitionService_t3866570042::get_offset_of_OnRecognitionError_9(),
	CloudRecognitionService_t3866570042::get_offset_of_OnInterruption_10(),
	CloudRecognitionService_t3866570042::get_offset_of_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (OnInitializedEvent_t1242699385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (OnInitializationErrorEvent_t511199451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (OnRecognitionResponseEvent_t3712700845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (OnRecognitionErrorEvent_t2212464566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (OnInterruptionEvent_t1530367979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (FrameColorSpace_t1906248721)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	FrameColorSpace_t1906248721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (FrameStrides_t352336826)+ sizeof (RuntimeObject), sizeof(FrameStrides_t352336826 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[6] = 
{
	FrameStrides_t352336826::get_offset_of_LuminancePixelStride_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameStrides_t352336826::get_offset_of_LuminanceRowStride_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceRedPixelStride_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceRedRowStride_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceBluePixelStride_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceBlueRowStride_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Frame_t1062777224)+ sizeof (RuntimeObject), sizeof(Frame_t1062777224_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[7] = 
{
	Frame_t1062777224::get_offset_of_Data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t1062777224::get_offset_of_DataSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t1062777224::get_offset_of_Width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t1062777224::get_offset_of_Height_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t1062777224::get_offset_of_ColorSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t1062777224::get_offset_of_HasStrides_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t1062777224::get_offset_of_Strides_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
