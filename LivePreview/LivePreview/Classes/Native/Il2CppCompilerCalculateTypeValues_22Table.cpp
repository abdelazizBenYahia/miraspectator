﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t2679013775;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// TMPro.Examples.TeleType
struct TeleType_t2513439854;
// TMPro.TMP_Text
struct TMP_Text_t1920000777;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t2849466151;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t2207663326;
// Mira.Transition2D
struct Transition2D_t3717442181;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3449578277;
// SettingsManager
struct SettingsManager_t2519859232;
// FadeInScene
struct FadeInScene_t62196113;
// Mira.RotationalTrackingManager
struct RotationalTrackingManager_t1512110775;
// MiraExampleInteractionScript
struct MiraExampleInteractionScript_t1495489050;
// MiraWikitudeManager
struct MiraWikitudeManager_t2317471266;
// TMPro.Examples.Benchmark01
struct Benchmark01_t2768175604;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3378890949;
// EnvMapAnimator
struct EnvMapAnimator_t1635389402;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Material
struct Material_t193706927;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2517845841;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Double[]
struct DoubleU5BU5D_t1889952540;
// DistortionEquation
struct DistortionEquation_t3976810169;
// TMPro.TextMeshPro
struct TextMeshPro_t2521834357;
// System.String
struct String_t;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3517219175;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// MiraBTRemoteInput
struct MiraBTRemoteInput_t988911721;
// MiraBasePointer
struct MiraBasePointer_t885132991;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MiraReticle
struct MiraReticle_t2195475589;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// Mira.MiraViewer
struct MiraViewer_t1267122109;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.EventSystems.MiraInputModule
struct MiraInputModule_t2476427099;
// UnityEngine.Renderer
struct Renderer_t257310565;
// MarkerRotationWatcher/RotateAction
struct RotateAction_t2368064732;
// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t2063364415;
// System.Collections.Generic.Queue`1<UnityEngine.Quaternion>
struct Queue_1_t3849730753;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// MarkerRotationWatcher
struct MarkerRotationWatcher_t3969903492;
// Mira.Fishingline
struct Fishingline_t2350076148;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Camera
struct Camera_t189460977;
// RemotesController
struct RemotesController_t2607477933;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Font
struct Font_t4239498691;
// MiraReticlePointer
struct MiraReticlePointer_t592362690;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// TMPro.TMP_InputField
struct TMP_InputField_t1778301588;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t6181308;
// AxisSpin
struct AxisSpin_t3838354289;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t2530419979;
// TMPro.TextContainer
struct TextContainer_t4263764796;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t934157183;
// Wikitude.ImageTrackable
struct ImageTrackable_t3105654606;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t1524870173;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t621514313;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t607610358;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t3572864619;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t1348919171;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t1795346708;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t3687964427;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SERIALIZABLEVECTOR4_T4294681242_H
#define SERIALIZABLEVECTOR4_T4294681242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector4
struct  SerializableVector4_t4294681242  : public RuntimeObject
{
public:
	// System.Single Utils.SerializableVector4::x
	float ___x_0;
	// System.Single Utils.SerializableVector4::y
	float ___y_1;
	// System.Single Utils.SerializableVector4::z
	float ___z_2;
	// System.Single Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_T4294681242_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T35148318_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T35148318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t35148318  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t2679013775 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24this_1)); }
	inline ShaderPropAnimator_t2679013775 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t2679013775 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t2679013775 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T35148318_H
#ifndef SERIALIZABLEPOINTCLOUD_T1992421910_H
#define SERIALIZABLEPOINTCLOUD_T1992421910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializablePointCloud
struct  serializablePointCloud_t1992421910  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_t3397334013* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_t1992421910, ___pointCloudData_0)); }
	inline ByteU5BU5D_t3397334013* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_t3397334013* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_T1992421910_H
#ifndef SERIALIZABLEBTREMOTEBUTTONS_T2327889448_H
#define SERIALIZABLEBTREMOTEBUTTONS_T2327889448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemoteButtons
struct  serializableBTRemoteButtons_t2327889448  : public RuntimeObject
{
public:
	// System.Boolean Utils.serializableBTRemoteButtons::startButton
	bool ___startButton_0;
	// System.Boolean Utils.serializableBTRemoteButtons::backButton
	bool ___backButton_1;
	// System.Boolean Utils.serializableBTRemoteButtons::triggerButton
	bool ___triggerButton_2;

public:
	inline static int32_t get_offset_of_startButton_0() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t2327889448, ___startButton_0)); }
	inline bool get_startButton_0() const { return ___startButton_0; }
	inline bool* get_address_of_startButton_0() { return &___startButton_0; }
	inline void set_startButton_0(bool value)
	{
		___startButton_0 = value;
	}

	inline static int32_t get_offset_of_backButton_1() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t2327889448, ___backButton_1)); }
	inline bool get_backButton_1() const { return ___backButton_1; }
	inline bool* get_address_of_backButton_1() { return &___backButton_1; }
	inline void set_backButton_1(bool value)
	{
		___backButton_1 = value;
	}

	inline static int32_t get_offset_of_triggerButton_2() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t2327889448, ___triggerButton_2)); }
	inline bool get_triggerButton_2() const { return ___triggerButton_2; }
	inline bool* get_address_of_triggerButton_2() { return &___triggerButton_2; }
	inline void set_triggerButton_2(bool value)
	{
		___triggerButton_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTEBUTTONS_T2327889448_H
#ifndef U3CSTARTU3EC__ITERATOR0_T189980609_H
#define U3CSTARTU3EC__ITERATOR0_T189980609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t189980609  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2513439854 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24this_3)); }
	inline TeleType_t2513439854 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2513439854 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2513439854 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T189980609_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T1407882744_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T1407882744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t1407882744  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t1920000777 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t2207663326 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___textComponent_0)); }
	inline TMP_Text_t1920000777 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t1920000777 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24this_4)); }
	inline TextConsoleSimulator_t2207663326 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t2207663326 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t2207663326 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T1407882744_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T2877888826_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T2877888826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t2877888826  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t1920000777 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___textComponent_0)); }
	inline TMP_Text_t1920000777 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t1920000777 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T2877888826_H
#ifndef SERIALIZABLEFLOAT_T3811907803_H
#define SERIALIZABLEFLOAT_T3811907803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFloat
struct  serializableFloat_t3811907803  : public RuntimeObject
{
public:
	// System.Single Utils.serializableFloat::x
	float ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(serializableFloat_t3811907803, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFLOAT_T3811907803_H
#ifndef U3CTRANSITIONTOLANDSCAPEU3EC__ITERATOR1_T3812029554_H
#define U3CTRANSITIONTOLANDSCAPEU3EC__ITERATOR1_T3812029554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D/<TransitionToLandscape>c__Iterator1
struct  U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554  : public RuntimeObject
{
public:
	// Mira.Transition2D Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$this
	Transition2D_t3717442181 * ___U24this_0;
	// System.Object Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.Transition2D/<TransitionToLandscape>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554, ___U24this_0)); }
	inline Transition2D_t3717442181 * get_U24this_0() const { return ___U24this_0; }
	inline Transition2D_t3717442181 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Transition2D_t3717442181 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONTOLANDSCAPEU3EC__ITERATOR1_T3812029554_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T2339960184_H
#define OBJECTSERIALIZATIONEXTENSION_T2339960184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t2339960184  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T2339960184_H
#ifndef U3CSTARTU3EC__ITERATOR0_T17013742_H
#define U3CSTARTU3EC__ITERATOR0_T17013742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t17013742  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3449578277 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24this_1)); }
	inline Benchmark01_UGUI_t3449578277 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3449578277 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3449578277 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T17013742_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2484427336_H
#define U3CSTARTU3EC__ITERATOR0_T2484427336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2484427336  : public RuntimeObject
{
public:
	// Mira.Transition2D Mira.Transition2D/<Start>c__Iterator0::$this
	Transition2D_t3717442181 * ___U24this_0;
	// System.Object Mira.Transition2D/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.Transition2D/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.Transition2D/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2484427336, ___U24this_0)); }
	inline Transition2D_t3717442181 * get_U24this_0() const { return ___U24this_0; }
	inline Transition2D_t3717442181 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Transition2D_t3717442181 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2484427336, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2484427336, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2484427336, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2484427336_H
#ifndef U3CTRANSITIONTOPORTRAITU3EC__ITERATOR2_T444968087_H
#define U3CTRANSITIONTOPORTRAITU3EC__ITERATOR2_T444968087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D/<TransitionToPortrait>c__Iterator2
struct  U3CTransitionToPortraitU3Ec__Iterator2_t444968087  : public RuntimeObject
{
public:
	// Mira.Transition2D Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$this
	Transition2D_t3717442181 * ___U24this_0;
	// System.Object Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.Transition2D/<TransitionToPortrait>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t444968087, ___U24this_0)); }
	inline Transition2D_t3717442181 * get_U24this_0() const { return ___U24this_0; }
	inline Transition2D_t3717442181 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Transition2D_t3717442181 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t444968087, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t444968087, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTransitionToPortraitU3Ec__Iterator2_t444968087, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONTOPORTRAITU3EC__ITERATOR2_T444968087_H
#ifndef U3CREMOTEDISCONNECTEDNOTIFICATIONU3EC__ITERATOR1_T2434143261_H
#define U3CREMOTEDISCONNECTEDNOTIFICATIONU3EC__ITERATOR1_T2434143261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager/<RemoteDisconnectedNotification>c__Iterator1
struct  U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261  : public RuntimeObject
{
public:
	// SettingsManager SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$this
	SettingsManager_t2519859232 * ___U24this_0;
	// System.Object SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 SettingsManager/<RemoteDisconnectedNotification>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261, ___U24this_0)); }
	inline SettingsManager_t2519859232 * get_U24this_0() const { return ___U24this_0; }
	inline SettingsManager_t2519859232 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SettingsManager_t2519859232 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEDISCONNECTEDNOTIFICATIONU3EC__ITERATOR1_T2434143261_H
#ifndef U3CREMOTECONNECTEDNOTIFICATIONU3EC__ITERATOR0_T4057175900_H
#define U3CREMOTECONNECTEDNOTIFICATIONU3EC__ITERATOR0_T4057175900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager/<RemoteConnectedNotification>c__Iterator0
struct  U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900  : public RuntimeObject
{
public:
	// SettingsManager SettingsManager/<RemoteConnectedNotification>c__Iterator0::$this
	SettingsManager_t2519859232 * ___U24this_0;
	// System.Object SettingsManager/<RemoteConnectedNotification>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SettingsManager/<RemoteConnectedNotification>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SettingsManager/<RemoteConnectedNotification>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900, ___U24this_0)); }
	inline SettingsManager_t2519859232 * get_U24this_0() const { return ___U24this_0; }
	inline SettingsManager_t2519859232 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SettingsManager_t2519859232 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTECONNECTEDNOTIFICATIONU3EC__ITERATOR0_T4057175900_H
#ifndef U3CSTARTLOGOUTU3EC__ITERATOR0_T1067525859_H
#define U3CSTARTLOGOUTU3EC__ITERATOR0_T1067525859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeInScene/<StartLogOut>c__Iterator0
struct  U3CStartLogOutU3Ec__Iterator0_t1067525859  : public RuntimeObject
{
public:
	// System.Int32 FadeInScene/<StartLogOut>c__Iterator0::sceneToLoad
	int32_t ___sceneToLoad_0;
	// FadeInScene FadeInScene/<StartLogOut>c__Iterator0::$this
	FadeInScene_t62196113 * ___U24this_1;
	// System.Object FadeInScene/<StartLogOut>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean FadeInScene/<StartLogOut>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 FadeInScene/<StartLogOut>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_sceneToLoad_0() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t1067525859, ___sceneToLoad_0)); }
	inline int32_t get_sceneToLoad_0() const { return ___sceneToLoad_0; }
	inline int32_t* get_address_of_sceneToLoad_0() { return &___sceneToLoad_0; }
	inline void set_sceneToLoad_0(int32_t value)
	{
		___sceneToLoad_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t1067525859, ___U24this_1)); }
	inline FadeInScene_t62196113 * get_U24this_1() const { return ___U24this_1; }
	inline FadeInScene_t62196113 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FadeInScene_t62196113 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t1067525859, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t1067525859, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartLogOutU3Ec__Iterator0_t1067525859, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTLOGOUTU3EC__ITERATOR0_T1067525859_H
#ifndef U3CBUFFERPOSITIONU3EC__ITERATOR1_T605192684_H
#define U3CBUFFERPOSITIONU3EC__ITERATOR1_T605192684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1
struct  U3CBufferPositionU3Ec__Iterator1_t605192684  : public RuntimeObject
{
public:
	// Mira.RotationalTrackingManager Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$this
	RotationalTrackingManager_t1512110775 * ___U24this_0;
	// System.Object Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.RotationalTrackingManager/<BufferPosition>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t605192684, ___U24this_0)); }
	inline RotationalTrackingManager_t1512110775 * get_U24this_0() const { return ___U24this_0; }
	inline RotationalTrackingManager_t1512110775 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RotationalTrackingManager_t1512110775 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t605192684, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t605192684, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CBufferPositionU3Ec__Iterator1_t605192684, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUFFERPOSITIONU3EC__ITERATOR1_T605192684_H
#ifndef U3CROTATIONALSETUPU3EC__ITERATOR0_T2905453790_H
#define U3CROTATIONALSETUPU3EC__ITERATOR0_T2905453790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0
struct  U3CRotationalSetupU3Ec__Iterator0_t2905453790  : public RuntimeObject
{
public:
	// Mira.RotationalTrackingManager Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$this
	RotationalTrackingManager_t1512110775 * ___U24this_0;
	// System.Object Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Mira.RotationalTrackingManager/<RotationalSetup>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t2905453790, ___U24this_0)); }
	inline RotationalTrackingManager_t1512110775 * get_U24this_0() const { return ___U24this_0; }
	inline RotationalTrackingManager_t1512110775 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RotationalTrackingManager_t1512110775 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t2905453790, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t2905453790, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRotationalSetupU3Ec__Iterator0_t2905453790, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CROTATIONALSETUPU3EC__ITERATOR0_T2905453790_H
#ifndef U3CTEMPERTANTRUMU3EC__ITERATOR0_T2561833528_H
#define U3CTEMPERTANTRUMU3EC__ITERATOR0_T2561833528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraExampleInteractionScript/<temperTantrum>c__Iterator0
struct  U3CtemperTantrumU3Ec__Iterator0_t2561833528  : public RuntimeObject
{
public:
	// System.Int32 MiraExampleInteractionScript/<temperTantrum>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Single MiraExampleInteractionScript/<temperTantrum>c__Iterator0::<distance>__2
	float ___U3CdistanceU3E__2_1;
	// System.Int32 MiraExampleInteractionScript/<temperTantrum>c__Iterator0::<i>__3
	int32_t ___U3CiU3E__3_2;
	// MiraExampleInteractionScript MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$this
	MiraExampleInteractionScript_t1495489050 * ___U24this_3;
	// System.Object MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 MiraExampleInteractionScript/<temperTantrum>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E__2_1() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U3CdistanceU3E__2_1)); }
	inline float get_U3CdistanceU3E__2_1() const { return ___U3CdistanceU3E__2_1; }
	inline float* get_address_of_U3CdistanceU3E__2_1() { return &___U3CdistanceU3E__2_1; }
	inline void set_U3CdistanceU3E__2_1(float value)
	{
		___U3CdistanceU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__3_2() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U3CiU3E__3_2)); }
	inline int32_t get_U3CiU3E__3_2() const { return ___U3CiU3E__3_2; }
	inline int32_t* get_address_of_U3CiU3E__3_2() { return &___U3CiU3E__3_2; }
	inline void set_U3CiU3E__3_2(int32_t value)
	{
		___U3CiU3E__3_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U24this_3)); }
	inline MiraExampleInteractionScript_t1495489050 * get_U24this_3() const { return ___U24this_3; }
	inline MiraExampleInteractionScript_t1495489050 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MiraExampleInteractionScript_t1495489050 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CtemperTantrumU3Ec__Iterator0_t2561833528, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTEMPERTANTRUMU3EC__ITERATOR0_T2561833528_H
#ifndef U3CACTIVATETRACKINGU3EC__ITERATOR0_T1868664202_H
#define U3CACTIVATETRACKINGU3EC__ITERATOR0_T1868664202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraWikitudeManager/<ActivateTracking>c__Iterator0
struct  U3CActivateTrackingU3Ec__Iterator0_t1868664202  : public RuntimeObject
{
public:
	// MiraWikitudeManager MiraWikitudeManager/<ActivateTracking>c__Iterator0::$this
	MiraWikitudeManager_t2317471266 * ___U24this_0;
	// System.Object MiraWikitudeManager/<ActivateTracking>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MiraWikitudeManager/<ActivateTracking>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MiraWikitudeManager/<ActivateTracking>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1868664202, ___U24this_0)); }
	inline MiraWikitudeManager_t2317471266 * get_U24this_0() const { return ___U24this_0; }
	inline MiraWikitudeManager_t2317471266 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MiraWikitudeManager_t2317471266 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1868664202, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1868664202, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CActivateTrackingU3Ec__Iterator0_t1868664202, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATETRACKINGU3EC__ITERATOR0_T1868664202_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1060020671_H
#define U3CSTARTU3EC__ITERATOR0_T1060020671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1060020671  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t2768175604 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24this_1)); }
	inline Benchmark01_t2768175604 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t2768175604 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t2768175604 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1060020671_H
#ifndef MIRABASEPOINTER_T885132991_H
#define MIRABASEPOINTER_T885132991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBasePointer
struct  MiraBasePointer_t885132991  : public RuntimeObject
{
public:
	// System.Single MiraBasePointer::<maxDistance>k__BackingField
	float ___U3CmaxDistanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmaxDistanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MiraBasePointer_t885132991, ___U3CmaxDistanceU3Ek__BackingField_0)); }
	inline float get_U3CmaxDistanceU3Ek__BackingField_0() const { return ___U3CmaxDistanceU3Ek__BackingField_0; }
	inline float* get_address_of_U3CmaxDistanceU3Ek__BackingField_0() { return &___U3CmaxDistanceU3Ek__BackingField_0; }
	inline void set_U3CmaxDistanceU3Ek__BackingField_0(float value)
	{
		___U3CmaxDistanceU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASEPOINTER_T885132991_H
#ifndef PLATFORMBASE_T3918687204_H
#define PLATFORMBASE_T3918687204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PlatformBase
struct  PlatformBase_t3918687204  : public RuntimeObject
{
public:

public:
};

struct PlatformBase_t3918687204_StaticFields
{
public:
	// Wikitude.PlatformBase Wikitude.PlatformBase::_instance
	PlatformBase_t3918687204 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PlatformBase_t3918687204_StaticFields, ____instance_0)); }
	inline PlatformBase_t3918687204 * get__instance_0() const { return ____instance_0; }
	inline PlatformBase_t3918687204 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PlatformBase_t3918687204 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMBASE_T3918687204_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef MIRACONNECTIONMESSAGEIDS_T1100361794_H
#define MIRACONNECTIONMESSAGEIDS_T1100361794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraConnectionMessageIds
struct  MiraConnectionMessageIds_t1100361794  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRACONNECTIONMESSAGEIDS_T1100361794_H
#ifndef MIRASUBMESSAGEIDS_T3396168626_H
#define MIRASUBMESSAGEIDS_T3396168626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraSubMessageIds
struct  MiraSubMessageIds_t3396168626  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRASUBMESSAGEIDS_T3396168626_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2510243513 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2510243513 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2510243513 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef PLATFORM_T358478341_H
#define PLATFORM_T358478341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Platform
struct  Platform_t358478341  : public PlatformBase_t3918687204
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T358478341_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef SERIALIZABLEVECTOR3_T4294681249_H
#define SERIALIZABLEVECTOR3_T4294681249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector3
struct  SerializableVector3_t4294681249 
{
public:
	// System.Single Utils.SerializableVector3::x
	float ___x_0;
	// System.Single Utils.SerializableVector3::y
	float ___y_1;
	// System.Single Utils.SerializableVector3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector3_t4294681249, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector3_t4294681249, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector3_t4294681249, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR3_T4294681249_H
#ifndef SERIALIZABLEVECTOR2_T4294681248_H
#define SERIALIZABLEVECTOR2_T4294681248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector2
struct  SerializableVector2_t4294681248 
{
public:
	// System.Single Utils.SerializableVector2::x
	float ___x_0;
	// System.Single Utils.SerializableVector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector2_t4294681248, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector2_t4294681248, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR2_T4294681248_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SERIALIZABLEQUATERNION_T3902400085_H
#define SERIALIZABLEQUATERNION_T3902400085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableQuaternion
struct  SerializableQuaternion_t3902400085 
{
public:
	// System.Single Utils.SerializableQuaternion::x
	float ___x_0;
	// System.Single Utils.SerializableQuaternion::y
	float ___y_1;
	// System.Single Utils.SerializableQuaternion::z
	float ___z_2;
	// System.Single Utils.SerializableQuaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEQUATERNION_T3902400085_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef MIRARETICLEPOINTER_T592362690_H
#define MIRARETICLEPOINTER_T592362690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraReticlePointer
struct  MiraReticlePointer_t592362690  : public MiraBasePointer_t885132991
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRARETICLEPOINTER_T592362690_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef RAYCASTSTYLE_T2248787495_H
#define RAYCASTSTYLE_T2248787495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster/RaycastStyle
struct  RaycastStyle_t2248787495 
{
public:
	// System.Int32 MiraBaseRaycaster/RaycastStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RaycastStyle_t2248787495, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTSTYLE_T2248787495_H
#ifndef BLOCKINGOBJECTS_T2548930813_H
#define BLOCKINGOBJECTS_T2548930813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GraphicRaycaster/BlockingObjects
struct  BlockingObjects_t2548930813 
{
public:
	// System.Int32 UnityEngine.UI.GraphicRaycaster/BlockingObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockingObjects_t2548930813, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_T2548930813_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T1799541603_H
#define U3CWARPTEXTU3EC__ITERATOR0_T1799541603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t1799541603  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3306541151 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1172311765* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3378890949 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3306541151 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3306541151 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1172311765* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1172311765** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1172311765* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24this_9)); }
	inline SkewTextExample_t3378890949 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3378890949 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3378890949 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T1799541603_H
#ifndef MOTIONTYPE_T2454277493_H
#define MOTIONTYPE_T2454277493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t2454277493 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t2454277493, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T2454277493_H
#ifndef U3CSTARTU3EC__ITERATOR0_T110035792_H
#define U3CSTARTU3EC__ITERATOR0_T110035792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t110035792  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t2933234003  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1635389402 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24this_1)); }
	inline EnvMapAnimator_t1635389402 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1635389402 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1635389402 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T110035792_H
#ifndef CAMERAMODES_T2188281734_H
#define CAMERAMODES_T2188281734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t2188281734 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t2188281734, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T2188281734_H
#ifndef RIGIDBODYCONSTRAINTS_T251614631_H
#define RIGIDBODYCONSTRAINTS_T251614631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_t251614631 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_t251614631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS_T251614631_H
#ifndef U3CRECENTERANIMU3EC__ITERATOR3_T1224236469_H
#define U3CRECENTERANIMU3EC__ITERATOR3_T1224236469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<RecenterAnim>c__Iterator3
struct  U3CRecenterAnimU3Ec__Iterator3_t1224236469  : public RuntimeObject
{
public:
	// System.Single MiraController/<RecenterAnim>c__Iterator3::<animTime>__0
	float ___U3CanimTimeU3E__0_0;
	// UnityEngine.Transform MiraController/<RecenterAnim>c__Iterator3::<reticle>__0
	Transform_t3275118058 * ___U3CreticleU3E__0_1;
	// UnityEngine.Material MiraController/<RecenterAnim>c__Iterator3::<reticleVis>__0
	Material_t193706927 * ___U3CreticleVisU3E__0_2;
	// UnityEngine.Color MiraController/<RecenterAnim>c__Iterator3::<originalColor>__0
	Color_t2020392075  ___U3CoriginalColorU3E__0_3;
	// UnityEngine.Color MiraController/<RecenterAnim>c__Iterator3::<fadeColor>__0
	Color_t2020392075  ___U3CfadeColorU3E__0_4;
	// System.Single MiraController/<RecenterAnim>c__Iterator3::<timer>__0
	float ___U3CtimerU3E__0_5;
	// System.Object MiraController/<RecenterAnim>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MiraController/<RecenterAnim>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 MiraController/<RecenterAnim>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CanimTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U3CanimTimeU3E__0_0)); }
	inline float get_U3CanimTimeU3E__0_0() const { return ___U3CanimTimeU3E__0_0; }
	inline float* get_address_of_U3CanimTimeU3E__0_0() { return &___U3CanimTimeU3E__0_0; }
	inline void set_U3CanimTimeU3E__0_0(float value)
	{
		___U3CanimTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CreticleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U3CreticleU3E__0_1)); }
	inline Transform_t3275118058 * get_U3CreticleU3E__0_1() const { return ___U3CreticleU3E__0_1; }
	inline Transform_t3275118058 ** get_address_of_U3CreticleU3E__0_1() { return &___U3CreticleU3E__0_1; }
	inline void set_U3CreticleU3E__0_1(Transform_t3275118058 * value)
	{
		___U3CreticleU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreticleU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CreticleVisU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U3CreticleVisU3E__0_2)); }
	inline Material_t193706927 * get_U3CreticleVisU3E__0_2() const { return ___U3CreticleVisU3E__0_2; }
	inline Material_t193706927 ** get_address_of_U3CreticleVisU3E__0_2() { return &___U3CreticleVisU3E__0_2; }
	inline void set_U3CreticleVisU3E__0_2(Material_t193706927 * value)
	{
		___U3CreticleVisU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreticleVisU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CoriginalColorU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U3CoriginalColorU3E__0_3)); }
	inline Color_t2020392075  get_U3CoriginalColorU3E__0_3() const { return ___U3CoriginalColorU3E__0_3; }
	inline Color_t2020392075 * get_address_of_U3CoriginalColorU3E__0_3() { return &___U3CoriginalColorU3E__0_3; }
	inline void set_U3CoriginalColorU3E__0_3(Color_t2020392075  value)
	{
		___U3CoriginalColorU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CfadeColorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U3CfadeColorU3E__0_4)); }
	inline Color_t2020392075  get_U3CfadeColorU3E__0_4() const { return ___U3CfadeColorU3E__0_4; }
	inline Color_t2020392075 * get_address_of_U3CfadeColorU3E__0_4() { return &___U3CfadeColorU3E__0_4; }
	inline void set_U3CfadeColorU3E__0_4(Color_t2020392075  value)
	{
		___U3CfadeColorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U3CtimerU3E__0_5)); }
	inline float get_U3CtimerU3E__0_5() const { return ___U3CtimerU3E__0_5; }
	inline float* get_address_of_U3CtimerU3E__0_5() { return &___U3CtimerU3E__0_5; }
	inline void set_U3CtimerU3E__0_5(float value)
	{
		___U3CtimerU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRecenterAnimU3Ec__Iterator3_t1224236469, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECENTERANIMU3EC__ITERATOR3_T1224236469_H
#ifndef SERIALIZABLEBTREMOTETOUCHPAD_T2020069673_H
#define SERIALIZABLEBTREMOTETOUCHPAD_T2020069673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemoteTouchPad
struct  serializableBTRemoteTouchPad_t2020069673  : public RuntimeObject
{
public:
	// System.Boolean Utils.serializableBTRemoteTouchPad::touchActive
	bool ___touchActive_0;
	// System.Boolean Utils.serializableBTRemoteTouchPad::touchButton
	bool ___touchButton_1;
	// Utils.SerializableVector2 Utils.serializableBTRemoteTouchPad::touchPos
	SerializableVector2_t4294681248  ___touchPos_2;
	// System.Boolean Utils.serializableBTRemoteTouchPad::upButton
	bool ___upButton_3;
	// System.Boolean Utils.serializableBTRemoteTouchPad::downButton
	bool ___downButton_4;
	// System.Boolean Utils.serializableBTRemoteTouchPad::leftButton
	bool ___leftButton_5;
	// System.Boolean Utils.serializableBTRemoteTouchPad::rightButton
	bool ___rightButton_6;

public:
	inline static int32_t get_offset_of_touchActive_0() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___touchActive_0)); }
	inline bool get_touchActive_0() const { return ___touchActive_0; }
	inline bool* get_address_of_touchActive_0() { return &___touchActive_0; }
	inline void set_touchActive_0(bool value)
	{
		___touchActive_0 = value;
	}

	inline static int32_t get_offset_of_touchButton_1() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___touchButton_1)); }
	inline bool get_touchButton_1() const { return ___touchButton_1; }
	inline bool* get_address_of_touchButton_1() { return &___touchButton_1; }
	inline void set_touchButton_1(bool value)
	{
		___touchButton_1 = value;
	}

	inline static int32_t get_offset_of_touchPos_2() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___touchPos_2)); }
	inline SerializableVector2_t4294681248  get_touchPos_2() const { return ___touchPos_2; }
	inline SerializableVector2_t4294681248 * get_address_of_touchPos_2() { return &___touchPos_2; }
	inline void set_touchPos_2(SerializableVector2_t4294681248  value)
	{
		___touchPos_2 = value;
	}

	inline static int32_t get_offset_of_upButton_3() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___upButton_3)); }
	inline bool get_upButton_3() const { return ___upButton_3; }
	inline bool* get_address_of_upButton_3() { return &___upButton_3; }
	inline void set_upButton_3(bool value)
	{
		___upButton_3 = value;
	}

	inline static int32_t get_offset_of_downButton_4() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___downButton_4)); }
	inline bool get_downButton_4() const { return ___downButton_4; }
	inline bool* get_address_of_downButton_4() { return &___downButton_4; }
	inline void set_downButton_4(bool value)
	{
		___downButton_4 = value;
	}

	inline static int32_t get_offset_of_leftButton_5() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___leftButton_5)); }
	inline bool get_leftButton_5() const { return ___leftButton_5; }
	inline bool* get_address_of_leftButton_5() { return &___leftButton_5; }
	inline void set_leftButton_5(bool value)
	{
		___leftButton_5 = value;
	}

	inline static int32_t get_offset_of_rightButton_6() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___rightButton_6)); }
	inline bool get_rightButton_6() const { return ___rightButton_6; }
	inline bool* get_address_of_rightButton_6() { return &___rightButton_6; }
	inline void set_rightButton_6(bool value)
	{
		___rightButton_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTETOUCHPAD_T2020069673_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_T2894567809_H
#define SERIALIZABLEFROMEDITORMESSAGE_T2894567809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_t2894567809  : public RuntimeObject
{
public:
	// System.Guid Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// System.Byte[] Utils.serializableFromEditorMessage::bytes
	ByteU5BU5D_t3397334013* ___bytes_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t2894567809, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t2894567809, ___bytes_1)); }
	inline ByteU5BU5D_t3397334013* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t3397334013* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_T2894567809_H
#ifndef SERIALIZABLEBTREMOTE_T622411689_H
#define SERIALIZABLEBTREMOTE_T622411689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemote
struct  serializableBTRemote_t622411689  : public RuntimeObject
{
public:
	// Utils.SerializableVector3 Utils.serializableBTRemote::orientation
	SerializableVector3_t4294681249  ___orientation_0;
	// Utils.SerializableVector3 Utils.serializableBTRemote::rotationRate
	SerializableVector3_t4294681249  ___rotationRate_1;
	// Utils.SerializableVector3 Utils.serializableBTRemote::acceleration
	SerializableVector3_t4294681249  ___acceleration_2;

public:
	inline static int32_t get_offset_of_orientation_0() { return static_cast<int32_t>(offsetof(serializableBTRemote_t622411689, ___orientation_0)); }
	inline SerializableVector3_t4294681249  get_orientation_0() const { return ___orientation_0; }
	inline SerializableVector3_t4294681249 * get_address_of_orientation_0() { return &___orientation_0; }
	inline void set_orientation_0(SerializableVector3_t4294681249  value)
	{
		___orientation_0 = value;
	}

	inline static int32_t get_offset_of_rotationRate_1() { return static_cast<int32_t>(offsetof(serializableBTRemote_t622411689, ___rotationRate_1)); }
	inline SerializableVector3_t4294681249  get_rotationRate_1() const { return ___rotationRate_1; }
	inline SerializableVector3_t4294681249 * get_address_of_rotationRate_1() { return &___rotationRate_1; }
	inline void set_rotationRate_1(SerializableVector3_t4294681249  value)
	{
		___rotationRate_1 = value;
	}

	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(serializableBTRemote_t622411689, ___acceleration_2)); }
	inline SerializableVector3_t4294681249  get_acceleration_2() const { return ___acceleration_2; }
	inline SerializableVector3_t4294681249 * get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(SerializableVector3_t4294681249  value)
	{
		___acceleration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTE_T622411689_H
#ifndef SERIALIZABLETRANSFORM_T2202979937_H
#define SERIALIZABLETRANSFORM_T2202979937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableTransform
struct  serializableTransform_t2202979937  : public RuntimeObject
{
public:
	// Utils.SerializableVector3 Utils.serializableTransform::position
	SerializableVector3_t4294681249  ___position_0;
	// Utils.SerializableQuaternion Utils.serializableTransform::rotation
	SerializableQuaternion_t3902400085  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(serializableTransform_t2202979937, ___position_0)); }
	inline SerializableVector3_t4294681249  get_position_0() const { return ___position_0; }
	inline SerializableVector3_t4294681249 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(SerializableVector3_t4294681249  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(serializableTransform_t2202979937, ___rotation_1)); }
	inline SerializableQuaternion_t3902400085  get_rotation_1() const { return ___rotation_1; }
	inline SerializableQuaternion_t3902400085 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(SerializableQuaternion_t3902400085  value)
	{
		___rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLETRANSFORM_T2202979937_H
#ifndef SERIALIZABLEGYROSCOPE_T1293559986_H
#define SERIALIZABLEGYROSCOPE_T1293559986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableGyroscope
struct  serializableGyroscope_t1293559986  : public RuntimeObject
{
public:
	// Utils.SerializableQuaternion Utils.serializableGyroscope::attitude
	SerializableQuaternion_t3902400085  ___attitude_0;
	// Utils.SerializableVector3 Utils.serializableGyroscope::userAcceleration
	SerializableVector3_t4294681249  ___userAcceleration_1;

public:
	inline static int32_t get_offset_of_attitude_0() { return static_cast<int32_t>(offsetof(serializableGyroscope_t1293559986, ___attitude_0)); }
	inline SerializableQuaternion_t3902400085  get_attitude_0() const { return ___attitude_0; }
	inline SerializableQuaternion_t3902400085 * get_address_of_attitude_0() { return &___attitude_0; }
	inline void set_attitude_0(SerializableQuaternion_t3902400085  value)
	{
		___attitude_0 = value;
	}

	inline static int32_t get_offset_of_userAcceleration_1() { return static_cast<int32_t>(offsetof(serializableGyroscope_t1293559986, ___userAcceleration_1)); }
	inline SerializableVector3_t4294681249  get_userAcceleration_1() const { return ___userAcceleration_1; }
	inline SerializableVector3_t4294681249 * get_address_of_userAcceleration_1() { return &___userAcceleration_1; }
	inline void set_userAcceleration_1(SerializableVector3_t4294681249  value)
	{
		___userAcceleration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEGYROSCOPE_T1293559986_H
#ifndef INPUTMODE_T4136831962_H
#define INPUTMODE_T4136831962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MiraInputModule/InputMode
struct  InputMode_t4136831962 
{
public:
	// System.Int32 UnityEngine.EventSystems.MiraInputModule/InputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputMode_t4136831962, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMODE_T4136831962_H
#ifndef CAMERANAMES_T2563008952_H
#define CAMERANAMES_T2563008952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraViewer/CameraNames
struct  CameraNames_t2563008952 
{
public:
	// System.Int32 Mira.MiraViewer/CameraNames::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraNames_t2563008952, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERANAMES_T2563008952_H
#ifndef EYE_T3339251924_H
#define EYE_T3339251924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraPostRender/Eye
struct  Eye_t3339251924 
{
public:
	// System.Int32 Mira.MiraPostRender/Eye::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Eye_t3339251924, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYE_T3339251924_H
#ifndef U3CSWITCHTOCAMERAGYROU3EC__ITERATOR2_T419419835_H
#define U3CSWITCHTOCAMERAGYROU3EC__ITERATOR2_T419419835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2
struct  U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::<offsetPosition>__0
	Vector3_t2243707580  ___U3CoffsetPositionU3E__0_0;
	// Mira.RotationalTrackingManager Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$this
	RotationalTrackingManager_t1512110775 * ___U24this_1;
	// System.Object Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 Mira.RotationalTrackingManager/<SwitchToCameraGyro>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoffsetPositionU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835, ___U3CoffsetPositionU3E__0_0)); }
	inline Vector3_t2243707580  get_U3CoffsetPositionU3E__0_0() const { return ___U3CoffsetPositionU3E__0_0; }
	inline Vector3_t2243707580 * get_address_of_U3CoffsetPositionU3E__0_0() { return &___U3CoffsetPositionU3E__0_0; }
	inline void set_U3CoffsetPositionU3E__0_0(Vector3_t2243707580  value)
	{
		___U3CoffsetPositionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835, ___U24this_1)); }
	inline RotationalTrackingManager_t1512110775 * get_U24this_1() const { return ___U24this_1; }
	inline RotationalTrackingManager_t1512110775 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RotationalTrackingManager_t1512110775 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSWITCHTOCAMERAGYROU3EC__ITERATOR2_T419419835_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ROTATEACTION_T2368064732_H
#define ROTATEACTION_T2368064732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerRotationWatcher/RotateAction
struct  RotateAction_t2368064732  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEACTION_T2368064732_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef MIRALIVEPREVIEWVIDEO_T2362804972_H
#define MIRALIVEPREVIEWVIDEO_T2362804972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewVideo
struct  MiraLivePreviewVideo_t2362804972  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWVIDEO_T2362804972_H
#ifndef MIRALIVEPREVIEWWIKICONFIG_T2812595783_H
#define MIRALIVEPREVIEWWIKICONFIG_T2812595783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraLivePreviewWikiConfig
struct  MiraLivePreviewWikiConfig_t2812595783  : public MonoBehaviour_t1158329972
{
public:
	// Wikitude.WikitudeCamera MiraLivePreviewWikiConfig::ArCam
	WikitudeCamera_t2517845841 * ___ArCam_2;

public:
	inline static int32_t get_offset_of_ArCam_2() { return static_cast<int32_t>(offsetof(MiraLivePreviewWikiConfig_t2812595783, ___ArCam_2)); }
	inline WikitudeCamera_t2517845841 * get_ArCam_2() const { return ___ArCam_2; }
	inline WikitudeCamera_t2517845841 ** get_address_of_ArCam_2() { return &___ArCam_2; }
	inline void set_ArCam_2(WikitudeCamera_t2517845841 * value)
	{
		___ArCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___ArCam_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWWIKICONFIG_T2812595783_H
#ifndef MIRAPOSTRENDER_T51508757_H
#define MIRAPOSTRENDER_T51508757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraPostRender
struct  MiraPostRender_t51508757  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.MiraPostRender::stereoCamFov
	float ___stereoCamFov_2;
	// System.Boolean Mira.MiraPostRender::noDistortion
	bool ___noDistortion_3;
	// System.Boolean Mira.MiraPostRender::postReduceDistortion
	bool ___postReduceDistortion_4;
	// System.Single Mira.MiraPostRender::xDistAmount
	float ___xDistAmount_5;
	// System.Single Mira.MiraPostRender::yDistAmount
	float ___yDistAmount_6;
	// System.Boolean Mira.MiraPostRender::tanCoordinates
	bool ___tanCoordinates_7;
	// Mira.MiraPostRender/Eye Mira.MiraPostRender::eye
	int32_t ___eye_8;
	// System.Int32 Mira.MiraPostRender::xSize
	int32_t ___xSize_9;
	// System.Int32 Mira.MiraPostRender::ySize
	int32_t ___ySize_10;
	// System.Single Mira.MiraPostRender::zOffset
	float ___zOffset_11;
	// System.Single Mira.MiraPostRender::xScalar
	float ___xScalar_12;
	// System.Single Mira.MiraPostRender::yScalar
	float ___yScalar_13;
	// System.Single Mira.MiraPostRender::tanConstant
	float ___tanConstant_14;
	// System.Single Mira.MiraPostRender::desiredParallaxDist
	float ___desiredParallaxDist_15;
	// UnityEngine.Mesh Mira.MiraPostRender::mesh
	Mesh_t1356156583 * ___mesh_16;
	// UnityEngine.Material Mira.MiraPostRender::renderTextureMaterial
	Material_t193706927 * ___renderTextureMaterial_17;
	// System.Single Mira.MiraPostRender::IPD
	float ___IPD_18;
	// System.Single Mira.MiraPostRender::ParallaxShift
	float ___ParallaxShift_19;
	// System.Double[] Mira.MiraPostRender::coefficientsXLeft
	DoubleU5BU5D_t1889952540* ___coefficientsXLeft_20;
	// System.Double[] Mira.MiraPostRender::coefficientsYLeft
	DoubleU5BU5D_t1889952540* ___coefficientsYLeft_21;
	// System.Double[] Mira.MiraPostRender::coefficientsXRight
	DoubleU5BU5D_t1889952540* ___coefficientsXRight_22;
	// System.Double[] Mira.MiraPostRender::coefficientsYRight
	DoubleU5BU5D_t1889952540* ___coefficientsYRight_23;
	// DistortionEquation Mira.MiraPostRender::distortion
	DistortionEquation_t3976810169 * ___distortion_24;

public:
	inline static int32_t get_offset_of_stereoCamFov_2() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___stereoCamFov_2)); }
	inline float get_stereoCamFov_2() const { return ___stereoCamFov_2; }
	inline float* get_address_of_stereoCamFov_2() { return &___stereoCamFov_2; }
	inline void set_stereoCamFov_2(float value)
	{
		___stereoCamFov_2 = value;
	}

	inline static int32_t get_offset_of_noDistortion_3() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___noDistortion_3)); }
	inline bool get_noDistortion_3() const { return ___noDistortion_3; }
	inline bool* get_address_of_noDistortion_3() { return &___noDistortion_3; }
	inline void set_noDistortion_3(bool value)
	{
		___noDistortion_3 = value;
	}

	inline static int32_t get_offset_of_postReduceDistortion_4() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___postReduceDistortion_4)); }
	inline bool get_postReduceDistortion_4() const { return ___postReduceDistortion_4; }
	inline bool* get_address_of_postReduceDistortion_4() { return &___postReduceDistortion_4; }
	inline void set_postReduceDistortion_4(bool value)
	{
		___postReduceDistortion_4 = value;
	}

	inline static int32_t get_offset_of_xDistAmount_5() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___xDistAmount_5)); }
	inline float get_xDistAmount_5() const { return ___xDistAmount_5; }
	inline float* get_address_of_xDistAmount_5() { return &___xDistAmount_5; }
	inline void set_xDistAmount_5(float value)
	{
		___xDistAmount_5 = value;
	}

	inline static int32_t get_offset_of_yDistAmount_6() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___yDistAmount_6)); }
	inline float get_yDistAmount_6() const { return ___yDistAmount_6; }
	inline float* get_address_of_yDistAmount_6() { return &___yDistAmount_6; }
	inline void set_yDistAmount_6(float value)
	{
		___yDistAmount_6 = value;
	}

	inline static int32_t get_offset_of_tanCoordinates_7() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___tanCoordinates_7)); }
	inline bool get_tanCoordinates_7() const { return ___tanCoordinates_7; }
	inline bool* get_address_of_tanCoordinates_7() { return &___tanCoordinates_7; }
	inline void set_tanCoordinates_7(bool value)
	{
		___tanCoordinates_7 = value;
	}

	inline static int32_t get_offset_of_eye_8() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___eye_8)); }
	inline int32_t get_eye_8() const { return ___eye_8; }
	inline int32_t* get_address_of_eye_8() { return &___eye_8; }
	inline void set_eye_8(int32_t value)
	{
		___eye_8 = value;
	}

	inline static int32_t get_offset_of_xSize_9() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___xSize_9)); }
	inline int32_t get_xSize_9() const { return ___xSize_9; }
	inline int32_t* get_address_of_xSize_9() { return &___xSize_9; }
	inline void set_xSize_9(int32_t value)
	{
		___xSize_9 = value;
	}

	inline static int32_t get_offset_of_ySize_10() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___ySize_10)); }
	inline int32_t get_ySize_10() const { return ___ySize_10; }
	inline int32_t* get_address_of_ySize_10() { return &___ySize_10; }
	inline void set_ySize_10(int32_t value)
	{
		___ySize_10 = value;
	}

	inline static int32_t get_offset_of_zOffset_11() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___zOffset_11)); }
	inline float get_zOffset_11() const { return ___zOffset_11; }
	inline float* get_address_of_zOffset_11() { return &___zOffset_11; }
	inline void set_zOffset_11(float value)
	{
		___zOffset_11 = value;
	}

	inline static int32_t get_offset_of_xScalar_12() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___xScalar_12)); }
	inline float get_xScalar_12() const { return ___xScalar_12; }
	inline float* get_address_of_xScalar_12() { return &___xScalar_12; }
	inline void set_xScalar_12(float value)
	{
		___xScalar_12 = value;
	}

	inline static int32_t get_offset_of_yScalar_13() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___yScalar_13)); }
	inline float get_yScalar_13() const { return ___yScalar_13; }
	inline float* get_address_of_yScalar_13() { return &___yScalar_13; }
	inline void set_yScalar_13(float value)
	{
		___yScalar_13 = value;
	}

	inline static int32_t get_offset_of_tanConstant_14() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___tanConstant_14)); }
	inline float get_tanConstant_14() const { return ___tanConstant_14; }
	inline float* get_address_of_tanConstant_14() { return &___tanConstant_14; }
	inline void set_tanConstant_14(float value)
	{
		___tanConstant_14 = value;
	}

	inline static int32_t get_offset_of_desiredParallaxDist_15() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___desiredParallaxDist_15)); }
	inline float get_desiredParallaxDist_15() const { return ___desiredParallaxDist_15; }
	inline float* get_address_of_desiredParallaxDist_15() { return &___desiredParallaxDist_15; }
	inline void set_desiredParallaxDist_15(float value)
	{
		___desiredParallaxDist_15 = value;
	}

	inline static int32_t get_offset_of_mesh_16() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___mesh_16)); }
	inline Mesh_t1356156583 * get_mesh_16() const { return ___mesh_16; }
	inline Mesh_t1356156583 ** get_address_of_mesh_16() { return &___mesh_16; }
	inline void set_mesh_16(Mesh_t1356156583 * value)
	{
		___mesh_16 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_16), value);
	}

	inline static int32_t get_offset_of_renderTextureMaterial_17() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___renderTextureMaterial_17)); }
	inline Material_t193706927 * get_renderTextureMaterial_17() const { return ___renderTextureMaterial_17; }
	inline Material_t193706927 ** get_address_of_renderTextureMaterial_17() { return &___renderTextureMaterial_17; }
	inline void set_renderTextureMaterial_17(Material_t193706927 * value)
	{
		___renderTextureMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureMaterial_17), value);
	}

	inline static int32_t get_offset_of_IPD_18() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___IPD_18)); }
	inline float get_IPD_18() const { return ___IPD_18; }
	inline float* get_address_of_IPD_18() { return &___IPD_18; }
	inline void set_IPD_18(float value)
	{
		___IPD_18 = value;
	}

	inline static int32_t get_offset_of_ParallaxShift_19() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___ParallaxShift_19)); }
	inline float get_ParallaxShift_19() const { return ___ParallaxShift_19; }
	inline float* get_address_of_ParallaxShift_19() { return &___ParallaxShift_19; }
	inline void set_ParallaxShift_19(float value)
	{
		___ParallaxShift_19 = value;
	}

	inline static int32_t get_offset_of_coefficientsXLeft_20() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___coefficientsXLeft_20)); }
	inline DoubleU5BU5D_t1889952540* get_coefficientsXLeft_20() const { return ___coefficientsXLeft_20; }
	inline DoubleU5BU5D_t1889952540** get_address_of_coefficientsXLeft_20() { return &___coefficientsXLeft_20; }
	inline void set_coefficientsXLeft_20(DoubleU5BU5D_t1889952540* value)
	{
		___coefficientsXLeft_20 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsXLeft_20), value);
	}

	inline static int32_t get_offset_of_coefficientsYLeft_21() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___coefficientsYLeft_21)); }
	inline DoubleU5BU5D_t1889952540* get_coefficientsYLeft_21() const { return ___coefficientsYLeft_21; }
	inline DoubleU5BU5D_t1889952540** get_address_of_coefficientsYLeft_21() { return &___coefficientsYLeft_21; }
	inline void set_coefficientsYLeft_21(DoubleU5BU5D_t1889952540* value)
	{
		___coefficientsYLeft_21 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsYLeft_21), value);
	}

	inline static int32_t get_offset_of_coefficientsXRight_22() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___coefficientsXRight_22)); }
	inline DoubleU5BU5D_t1889952540* get_coefficientsXRight_22() const { return ___coefficientsXRight_22; }
	inline DoubleU5BU5D_t1889952540** get_address_of_coefficientsXRight_22() { return &___coefficientsXRight_22; }
	inline void set_coefficientsXRight_22(DoubleU5BU5D_t1889952540* value)
	{
		___coefficientsXRight_22 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsXRight_22), value);
	}

	inline static int32_t get_offset_of_coefficientsYRight_23() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___coefficientsYRight_23)); }
	inline DoubleU5BU5D_t1889952540* get_coefficientsYRight_23() const { return ___coefficientsYRight_23; }
	inline DoubleU5BU5D_t1889952540** get_address_of_coefficientsYRight_23() { return &___coefficientsYRight_23; }
	inline void set_coefficientsYRight_23(DoubleU5BU5D_t1889952540* value)
	{
		___coefficientsYRight_23 = value;
		Il2CppCodeGenWriteBarrier((&___coefficientsYRight_23), value);
	}

	inline static int32_t get_offset_of_distortion_24() { return static_cast<int32_t>(offsetof(MiraPostRender_t51508757, ___distortion_24)); }
	inline DistortionEquation_t3976810169 * get_distortion_24() const { return ___distortion_24; }
	inline DistortionEquation_t3976810169 ** get_address_of_distortion_24() { return &___distortion_24; }
	inline void set_distortion_24(DistortionEquation_t3976810169 * value)
	{
		___distortion_24 = value;
		Il2CppCodeGenWriteBarrier((&___distortion_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPOSTRENDER_T51508757_H
#ifndef SIMPLESCRIPT_T767280347_H
#define SIMPLESCRIPT_T767280347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t767280347  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_2;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_4;

public:
	inline static int32_t get_offset_of_m_textMeshPro_2() { return static_cast<int32_t>(offsetof(SimpleScript_t767280347, ___m_textMeshPro_2)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_2() const { return ___m_textMeshPro_2; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_2() { return &___m_textMeshPro_2; }
	inline void set_m_textMeshPro_2(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_frame_4() { return static_cast<int32_t>(offsetof(SimpleScript_t767280347, ___m_frame_4)); }
	inline float get_m_frame_4() const { return ___m_frame_4; }
	inline float* get_address_of_m_frame_4() { return &___m_frame_4; }
	inline void set_m_frame_4(float value)
	{
		___m_frame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T767280347_H
#ifndef SKEWTEXTEXAMPLE_T3378890949_H
#define SKEWTEXTEXAMPLE_T3378890949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3378890949  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3306541151 * ___VertexCurve_3;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_4;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___m_TextComponent_2)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___VertexCurve_3)); }
	inline AnimationCurve_t3306541151 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3306541151 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3306541151 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___ShearAmount_5)); }
	inline float get_ShearAmount_5() const { return ___ShearAmount_5; }
	inline float* get_address_of_ShearAmount_5() { return &___ShearAmount_5; }
	inline void set_ShearAmount_5(float value)
	{
		___ShearAmount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3378890949_H
#ifndef TEXTCONSOLESIMULATOR_T2207663326_H
#define TEXTCONSOLESIMULATOR_T2207663326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t2207663326  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_2;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_3;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t2207663326, ___m_TextComponent_2)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_3() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t2207663326, ___hasTextChanged_3)); }
	inline bool get_hasTextChanged_3() const { return ___hasTextChanged_3; }
	inline bool* get_address_of_hasTextChanged_3() { return &___hasTextChanged_3; }
	inline void set_hasTextChanged_3(bool value)
	{
		___hasTextChanged_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T2207663326_H
#ifndef TELETYPE_T2513439854_H
#define TELETYPE_T2513439854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2513439854  : public MonoBehaviour_t1158329972
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_2;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_3;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t1920000777 * ___m_textMeshPro_4;

public:
	inline static int32_t get_offset_of_label01_2() { return static_cast<int32_t>(offsetof(TeleType_t2513439854, ___label01_2)); }
	inline String_t* get_label01_2() const { return ___label01_2; }
	inline String_t** get_address_of_label01_2() { return &___label01_2; }
	inline void set_label01_2(String_t* value)
	{
		___label01_2 = value;
		Il2CppCodeGenWriteBarrier((&___label01_2), value);
	}

	inline static int32_t get_offset_of_label02_3() { return static_cast<int32_t>(offsetof(TeleType_t2513439854, ___label02_3)); }
	inline String_t* get_label02_3() const { return ___label02_3; }
	inline String_t** get_address_of_label02_3() { return &___label02_3; }
	inline void set_label02_3(String_t* value)
	{
		___label02_3 = value;
		Il2CppCodeGenWriteBarrier((&___label02_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TeleType_t2513439854, ___m_textMeshPro_4)); }
	inline TMP_Text_t1920000777 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TMP_Text_t1920000777 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TMP_Text_t1920000777 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2513439854_H
#ifndef MIRALIVEPREVIEWPLAYER_T3516305084_H
#define MIRALIVEPREVIEWPLAYER_T3516305084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewPlayer
struct  MiraLivePreviewPlayer_t3516305084  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.MiraLivePreviewPlayer::playerConnection
	PlayerConnection_t3517219175 * ___playerConnection_2;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::bSessionActive
	bool ___bSessionActive_3;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::editorID
	int32_t ___editorID_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.MiraLivePreviewPlayer::liveViewScreenTex
	Texture2D_t3542995729 * ___liveViewScreenTex_5;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::bTexturesInitialized
	bool ___bTexturesInitialized_6;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::isTracking
	bool ___isTracking_7;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::btFrameCounter
	int32_t ___btFrameCounter_9;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::btSendRate
	int32_t ___btSendRate_10;

public:
	inline static int32_t get_offset_of_playerConnection_2() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___playerConnection_2)); }
	inline PlayerConnection_t3517219175 * get_playerConnection_2() const { return ___playerConnection_2; }
	inline PlayerConnection_t3517219175 ** get_address_of_playerConnection_2() { return &___playerConnection_2; }
	inline void set_playerConnection_2(PlayerConnection_t3517219175 * value)
	{
		___playerConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_2), value);
	}

	inline static int32_t get_offset_of_bSessionActive_3() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___bSessionActive_3)); }
	inline bool get_bSessionActive_3() const { return ___bSessionActive_3; }
	inline bool* get_address_of_bSessionActive_3() { return &___bSessionActive_3; }
	inline void set_bSessionActive_3(bool value)
	{
		___bSessionActive_3 = value;
	}

	inline static int32_t get_offset_of_editorID_4() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___editorID_4)); }
	inline int32_t get_editorID_4() const { return ___editorID_4; }
	inline int32_t* get_address_of_editorID_4() { return &___editorID_4; }
	inline void set_editorID_4(int32_t value)
	{
		___editorID_4 = value;
	}

	inline static int32_t get_offset_of_liveViewScreenTex_5() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___liveViewScreenTex_5)); }
	inline Texture2D_t3542995729 * get_liveViewScreenTex_5() const { return ___liveViewScreenTex_5; }
	inline Texture2D_t3542995729 ** get_address_of_liveViewScreenTex_5() { return &___liveViewScreenTex_5; }
	inline void set_liveViewScreenTex_5(Texture2D_t3542995729 * value)
	{
		___liveViewScreenTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___liveViewScreenTex_5), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_6() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___bTexturesInitialized_6)); }
	inline bool get_bTexturesInitialized_6() const { return ___bTexturesInitialized_6; }
	inline bool* get_address_of_bTexturesInitialized_6() { return &___bTexturesInitialized_6; }
	inline void set_bTexturesInitialized_6(bool value)
	{
		___bTexturesInitialized_6 = value;
	}

	inline static int32_t get_offset_of_isTracking_7() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___isTracking_7)); }
	inline bool get_isTracking_7() const { return ___isTracking_7; }
	inline bool* get_address_of_isTracking_7() { return &___isTracking_7; }
	inline void set_isTracking_7(bool value)
	{
		___isTracking_7 = value;
	}

	inline static int32_t get_offset_of_btFrameCounter_9() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___btFrameCounter_9)); }
	inline int32_t get_btFrameCounter_9() const { return ___btFrameCounter_9; }
	inline int32_t* get_address_of_btFrameCounter_9() { return &___btFrameCounter_9; }
	inline void set_btFrameCounter_9(int32_t value)
	{
		___btFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_btSendRate_10() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___btSendRate_10)); }
	inline int32_t get_btSendRate_10() const { return ___btSendRate_10; }
	inline int32_t* get_address_of_btSendRate_10() { return &___btSendRate_10; }
	inline void set_btSendRate_10(int32_t value)
	{
		___btSendRate_10 = value;
	}
};

struct MiraLivePreviewPlayer_t3516305084_StaticFields
{
public:
	// MiraBTRemoteInput UnityEngine.XR.iOS.MiraLivePreviewPlayer::m_userInput
	MiraBTRemoteInput_t988911721 * ___m_userInput_8;

public:
	inline static int32_t get_offset_of_m_userInput_8() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084_StaticFields, ___m_userInput_8)); }
	inline MiraBTRemoteInput_t988911721 * get_m_userInput_8() const { return ___m_userInput_8; }
	inline MiraBTRemoteInput_t988911721 ** get_address_of_m_userInput_8() { return &___m_userInput_8; }
	inline void set_m_userInput_8(MiraBTRemoteInput_t988911721 * value)
	{
		___m_userInput_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_userInput_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWPLAYER_T3516305084_H
#ifndef INVERSEGYROCONTROLLER_T2551975429_H
#define INVERSEGYROCONTROLLER_T2551975429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.InverseGyroController
struct  InverseGyroController_t2551975429  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Mira.InverseGyroController::stereoCamRig
	Transform_t3275118058 * ___stereoCamRig_2;

public:
	inline static int32_t get_offset_of_stereoCamRig_2() { return static_cast<int32_t>(offsetof(InverseGyroController_t2551975429, ___stereoCamRig_2)); }
	inline Transform_t3275118058 * get_stereoCamRig_2() const { return ___stereoCamRig_2; }
	inline Transform_t3275118058 ** get_address_of_stereoCamRig_2() { return &___stereoCamRig_2; }
	inline void set_stereoCamRig_2(Transform_t3275118058 * value)
	{
		___stereoCamRig_2 = value;
		Il2CppCodeGenWriteBarrier((&___stereoCamRig_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVERSEGYROCONTROLLER_T2551975429_H
#ifndef GYROCONTROLLER_T3857203917_H
#define GYROCONTROLLER_T3857203917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroController
struct  GyroController_t3857203917  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Quaternion GyroController::gyroRotation
	Quaternion_t4030073918  ___gyroRotation_3;
	// System.Boolean GyroController::gyroEnabled
	bool ___gyroEnabled_4;
	// System.Single GyroController::lowPassFilterFactor
	float ___lowPassFilterFactor_5;
	// UnityEngine.Quaternion GyroController::frontCamera
	Quaternion_t4030073918  ___frontCamera_6;
	// UnityEngine.Quaternion GyroController::cameraBase
	Quaternion_t4030073918  ___cameraBase_7;
	// UnityEngine.Quaternion GyroController::calibration
	Quaternion_t4030073918  ___calibration_8;
	// UnityEngine.Quaternion GyroController::baseOrientation
	Quaternion_t4030073918  ___baseOrientation_9;
	// UnityEngine.Quaternion GyroController::referenceRotation
	Quaternion_t4030073918  ___referenceRotation_10;
	// System.Boolean GyroController::inverseGyroSetup
	bool ___inverseGyroSetup_11;
	// System.Boolean GyroController::debugGUI
	bool ___debugGUI_12;
	// System.Boolean GyroController::useFrontCamera
	bool ___useFrontCamera_13;
	// System.Boolean GyroController::miraRemoteMode
	bool ___miraRemoteMode_14;
	// UnityEngine.Quaternion GyroController::rotationOffset
	Quaternion_t4030073918  ___rotationOffset_15;

public:
	inline static int32_t get_offset_of_gyroRotation_3() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___gyroRotation_3)); }
	inline Quaternion_t4030073918  get_gyroRotation_3() const { return ___gyroRotation_3; }
	inline Quaternion_t4030073918 * get_address_of_gyroRotation_3() { return &___gyroRotation_3; }
	inline void set_gyroRotation_3(Quaternion_t4030073918  value)
	{
		___gyroRotation_3 = value;
	}

	inline static int32_t get_offset_of_gyroEnabled_4() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___gyroEnabled_4)); }
	inline bool get_gyroEnabled_4() const { return ___gyroEnabled_4; }
	inline bool* get_address_of_gyroEnabled_4() { return &___gyroEnabled_4; }
	inline void set_gyroEnabled_4(bool value)
	{
		___gyroEnabled_4 = value;
	}

	inline static int32_t get_offset_of_lowPassFilterFactor_5() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___lowPassFilterFactor_5)); }
	inline float get_lowPassFilterFactor_5() const { return ___lowPassFilterFactor_5; }
	inline float* get_address_of_lowPassFilterFactor_5() { return &___lowPassFilterFactor_5; }
	inline void set_lowPassFilterFactor_5(float value)
	{
		___lowPassFilterFactor_5 = value;
	}

	inline static int32_t get_offset_of_frontCamera_6() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___frontCamera_6)); }
	inline Quaternion_t4030073918  get_frontCamera_6() const { return ___frontCamera_6; }
	inline Quaternion_t4030073918 * get_address_of_frontCamera_6() { return &___frontCamera_6; }
	inline void set_frontCamera_6(Quaternion_t4030073918  value)
	{
		___frontCamera_6 = value;
	}

	inline static int32_t get_offset_of_cameraBase_7() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___cameraBase_7)); }
	inline Quaternion_t4030073918  get_cameraBase_7() const { return ___cameraBase_7; }
	inline Quaternion_t4030073918 * get_address_of_cameraBase_7() { return &___cameraBase_7; }
	inline void set_cameraBase_7(Quaternion_t4030073918  value)
	{
		___cameraBase_7 = value;
	}

	inline static int32_t get_offset_of_calibration_8() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___calibration_8)); }
	inline Quaternion_t4030073918  get_calibration_8() const { return ___calibration_8; }
	inline Quaternion_t4030073918 * get_address_of_calibration_8() { return &___calibration_8; }
	inline void set_calibration_8(Quaternion_t4030073918  value)
	{
		___calibration_8 = value;
	}

	inline static int32_t get_offset_of_baseOrientation_9() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___baseOrientation_9)); }
	inline Quaternion_t4030073918  get_baseOrientation_9() const { return ___baseOrientation_9; }
	inline Quaternion_t4030073918 * get_address_of_baseOrientation_9() { return &___baseOrientation_9; }
	inline void set_baseOrientation_9(Quaternion_t4030073918  value)
	{
		___baseOrientation_9 = value;
	}

	inline static int32_t get_offset_of_referenceRotation_10() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___referenceRotation_10)); }
	inline Quaternion_t4030073918  get_referenceRotation_10() const { return ___referenceRotation_10; }
	inline Quaternion_t4030073918 * get_address_of_referenceRotation_10() { return &___referenceRotation_10; }
	inline void set_referenceRotation_10(Quaternion_t4030073918  value)
	{
		___referenceRotation_10 = value;
	}

	inline static int32_t get_offset_of_inverseGyroSetup_11() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___inverseGyroSetup_11)); }
	inline bool get_inverseGyroSetup_11() const { return ___inverseGyroSetup_11; }
	inline bool* get_address_of_inverseGyroSetup_11() { return &___inverseGyroSetup_11; }
	inline void set_inverseGyroSetup_11(bool value)
	{
		___inverseGyroSetup_11 = value;
	}

	inline static int32_t get_offset_of_debugGUI_12() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___debugGUI_12)); }
	inline bool get_debugGUI_12() const { return ___debugGUI_12; }
	inline bool* get_address_of_debugGUI_12() { return &___debugGUI_12; }
	inline void set_debugGUI_12(bool value)
	{
		___debugGUI_12 = value;
	}

	inline static int32_t get_offset_of_useFrontCamera_13() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___useFrontCamera_13)); }
	inline bool get_useFrontCamera_13() const { return ___useFrontCamera_13; }
	inline bool* get_address_of_useFrontCamera_13() { return &___useFrontCamera_13; }
	inline void set_useFrontCamera_13(bool value)
	{
		___useFrontCamera_13 = value;
	}

	inline static int32_t get_offset_of_miraRemoteMode_14() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___miraRemoteMode_14)); }
	inline bool get_miraRemoteMode_14() const { return ___miraRemoteMode_14; }
	inline bool* get_address_of_miraRemoteMode_14() { return &___miraRemoteMode_14; }
	inline void set_miraRemoteMode_14(bool value)
	{
		___miraRemoteMode_14 = value;
	}

	inline static int32_t get_offset_of_rotationOffset_15() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___rotationOffset_15)); }
	inline Quaternion_t4030073918  get_rotationOffset_15() const { return ___rotationOffset_15; }
	inline Quaternion_t4030073918 * get_address_of_rotationOffset_15() { return &___rotationOffset_15; }
	inline void set_rotationOffset_15(Quaternion_t4030073918  value)
	{
		___rotationOffset_15 = value;
	}
};

struct GyroController_t3857203917_StaticFields
{
public:
	// GyroController GyroController::instance
	GyroController_t3857203917 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GyroController_t3857203917_StaticFields, ___instance_2)); }
	inline GyroController_t3857203917 * get_instance_2() const { return ___instance_2; }
	inline GyroController_t3857203917 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GyroController_t3857203917 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROCONTROLLER_T3857203917_H
#ifndef FOLLOWHEADAUTOMATICSCALE_T1766947074_H
#define FOLLOWHEADAUTOMATICSCALE_T1766947074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.FollowHeadAutomaticScale
struct  FollowHeadAutomaticScale_t1766947074  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Mira.FollowHeadAutomaticScale::headFollowPositionM
	Vector3_t2243707580  ___headFollowPositionM_2;
	// System.Single Mira.FollowHeadAutomaticScale::lerpSpeed
	float ___lerpSpeed_3;
	// UnityEngine.Transform Mira.FollowHeadAutomaticScale::head
	Transform_t3275118058 * ___head_4;

public:
	inline static int32_t get_offset_of_headFollowPositionM_2() { return static_cast<int32_t>(offsetof(FollowHeadAutomaticScale_t1766947074, ___headFollowPositionM_2)); }
	inline Vector3_t2243707580  get_headFollowPositionM_2() const { return ___headFollowPositionM_2; }
	inline Vector3_t2243707580 * get_address_of_headFollowPositionM_2() { return &___headFollowPositionM_2; }
	inline void set_headFollowPositionM_2(Vector3_t2243707580  value)
	{
		___headFollowPositionM_2 = value;
	}

	inline static int32_t get_offset_of_lerpSpeed_3() { return static_cast<int32_t>(offsetof(FollowHeadAutomaticScale_t1766947074, ___lerpSpeed_3)); }
	inline float get_lerpSpeed_3() const { return ___lerpSpeed_3; }
	inline float* get_address_of_lerpSpeed_3() { return &___lerpSpeed_3; }
	inline void set_lerpSpeed_3(float value)
	{
		___lerpSpeed_3 = value;
	}

	inline static int32_t get_offset_of_head_4() { return static_cast<int32_t>(offsetof(FollowHeadAutomaticScale_t1766947074, ___head_4)); }
	inline Transform_t3275118058 * get_head_4() const { return ___head_4; }
	inline Transform_t3275118058 ** get_address_of_head_4() { return &___head_4; }
	inline void set_head_4(Transform_t3275118058 * value)
	{
		___head_4 = value;
		Il2CppCodeGenWriteBarrier((&___head_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWHEADAUTOMATICSCALE_T1766947074_H
#ifndef FOLLOWHEADAUTOMATIC_T1062824722_H
#define FOLLOWHEADAUTOMATIC_T1062824722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.FollowHeadAutomatic
struct  FollowHeadAutomatic_t1062824722  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Mira.FollowHeadAutomatic::headFollowPositionCM
	Vector3_t2243707580  ___headFollowPositionCM_2;
	// System.Single Mira.FollowHeadAutomatic::lerpSpeed
	float ___lerpSpeed_3;
	// UnityEngine.Transform Mira.FollowHeadAutomatic::head
	Transform_t3275118058 * ___head_4;

public:
	inline static int32_t get_offset_of_headFollowPositionCM_2() { return static_cast<int32_t>(offsetof(FollowHeadAutomatic_t1062824722, ___headFollowPositionCM_2)); }
	inline Vector3_t2243707580  get_headFollowPositionCM_2() const { return ___headFollowPositionCM_2; }
	inline Vector3_t2243707580 * get_address_of_headFollowPositionCM_2() { return &___headFollowPositionCM_2; }
	inline void set_headFollowPositionCM_2(Vector3_t2243707580  value)
	{
		___headFollowPositionCM_2 = value;
	}

	inline static int32_t get_offset_of_lerpSpeed_3() { return static_cast<int32_t>(offsetof(FollowHeadAutomatic_t1062824722, ___lerpSpeed_3)); }
	inline float get_lerpSpeed_3() const { return ___lerpSpeed_3; }
	inline float* get_address_of_lerpSpeed_3() { return &___lerpSpeed_3; }
	inline void set_lerpSpeed_3(float value)
	{
		___lerpSpeed_3 = value;
	}

	inline static int32_t get_offset_of_head_4() { return static_cast<int32_t>(offsetof(FollowHeadAutomatic_t1062824722, ___head_4)); }
	inline Transform_t3275118058 * get_head_4() const { return ___head_4; }
	inline Transform_t3275118058 ** get_address_of_head_4() { return &___head_4; }
	inline void set_head_4(Transform_t3275118058 * value)
	{
		___head_4 = value;
		Il2CppCodeGenWriteBarrier((&___head_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWHEADAUTOMATIC_T1062824722_H
#ifndef MIRAPOINTERMANAGER_T3638939077_H
#define MIRAPOINTERMANAGER_T3638939077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPointerManager
struct  MiraPointerManager_t3638939077  : public MonoBehaviour_t1158329972
{
public:
	// MiraBasePointer MiraPointerManager::_pointer
	MiraBasePointer_t885132991 * ____pointer_3;
	// UnityEngine.GameObject MiraPointerManager::_pointerGameObject
	GameObject_t1756533147 * ____pointerGameObject_4;

public:
	inline static int32_t get_offset_of__pointer_3() { return static_cast<int32_t>(offsetof(MiraPointerManager_t3638939077, ____pointer_3)); }
	inline MiraBasePointer_t885132991 * get__pointer_3() const { return ____pointer_3; }
	inline MiraBasePointer_t885132991 ** get_address_of__pointer_3() { return &____pointer_3; }
	inline void set__pointer_3(MiraBasePointer_t885132991 * value)
	{
		____pointer_3 = value;
		Il2CppCodeGenWriteBarrier((&____pointer_3), value);
	}

	inline static int32_t get_offset_of__pointerGameObject_4() { return static_cast<int32_t>(offsetof(MiraPointerManager_t3638939077, ____pointerGameObject_4)); }
	inline GameObject_t1756533147 * get__pointerGameObject_4() const { return ____pointerGameObject_4; }
	inline GameObject_t1756533147 ** get_address_of__pointerGameObject_4() { return &____pointerGameObject_4; }
	inline void set__pointerGameObject_4(GameObject_t1756533147 * value)
	{
		____pointerGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____pointerGameObject_4), value);
	}
};

struct MiraPointerManager_t3638939077_StaticFields
{
public:
	// MiraPointerManager MiraPointerManager::_instance
	MiraPointerManager_t3638939077 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MiraPointerManager_t3638939077_StaticFields, ____instance_2)); }
	inline MiraPointerManager_t3638939077 * get__instance_2() const { return ____instance_2; }
	inline MiraPointerManager_t3638939077 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(MiraPointerManager_t3638939077 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPOINTERMANAGER_T3638939077_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef TRANSFORMOVERRIDE_T3077475158_H
#define TRANSFORMOVERRIDE_T3077475158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformOverride
struct  TransformOverride_t3077475158  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMOVERRIDE_T3077475158_H
#ifndef MIRALIVEPREVIEWEDITOR_T3425584316_H
#define MIRALIVEPREVIEWEDITOR_T3425584316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewEditor
struct  MiraLivePreviewEditor_t3425584316  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWEDITOR_T3425584316_H
#ifndef MIRAARVIDEO_T1477805071_H
#define MIRAARVIDEO_T1477805071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraARVideo
struct  MiraARVideo_t1477805071  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D MiraARVideo::m_clearTexture
	Texture2D_t3542995729 * ___m_clearTexture_2;

public:
	inline static int32_t get_offset_of_m_clearTexture_2() { return static_cast<int32_t>(offsetof(MiraARVideo_t1477805071, ___m_clearTexture_2)); }
	inline Texture2D_t3542995729 * get_m_clearTexture_2() const { return ___m_clearTexture_2; }
	inline Texture2D_t3542995729 ** get_address_of_m_clearTexture_2() { return &___m_clearTexture_2; }
	inline void set_m_clearTexture_2(Texture2D_t3542995729 * value)
	{
		___m_clearTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_clearTexture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAARVIDEO_T1477805071_H
#ifndef MIRALASERPOINTERLENGTH_T1165938491_H
#define MIRALASERPOINTERLENGTH_T1165938491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraLaserPointerLength
struct  MiraLaserPointerLength_t1165938491  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MiraLaserPointerLength::widthMultiplier
	float ___widthMultiplier_2;
	// MiraReticle MiraLaserPointerLength::reticle
	MiraReticle_t2195475589 * ___reticle_3;
	// UnityEngine.LineRenderer MiraLaserPointerLength::rend
	LineRenderer_t849157671 * ___rend_4;

public:
	inline static int32_t get_offset_of_widthMultiplier_2() { return static_cast<int32_t>(offsetof(MiraLaserPointerLength_t1165938491, ___widthMultiplier_2)); }
	inline float get_widthMultiplier_2() const { return ___widthMultiplier_2; }
	inline float* get_address_of_widthMultiplier_2() { return &___widthMultiplier_2; }
	inline void set_widthMultiplier_2(float value)
	{
		___widthMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_reticle_3() { return static_cast<int32_t>(offsetof(MiraLaserPointerLength_t1165938491, ___reticle_3)); }
	inline MiraReticle_t2195475589 * get_reticle_3() const { return ___reticle_3; }
	inline MiraReticle_t2195475589 ** get_address_of_reticle_3() { return &___reticle_3; }
	inline void set_reticle_3(MiraReticle_t2195475589 * value)
	{
		___reticle_3 = value;
		Il2CppCodeGenWriteBarrier((&___reticle_3), value);
	}

	inline static int32_t get_offset_of_rend_4() { return static_cast<int32_t>(offsetof(MiraLaserPointerLength_t1165938491, ___rend_4)); }
	inline LineRenderer_t849157671 * get_rend_4() const { return ___rend_4; }
	inline LineRenderer_t849157671 ** get_address_of_rend_4() { return &___rend_4; }
	inline void set_rend_4(LineRenderer_t849157671 * value)
	{
		___rend_4 = value;
		Il2CppCodeGenWriteBarrier((&___rend_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALASERPOINTERLENGTH_T1165938491_H
#ifndef MIRAIOSBRIDGE_T2338730851_H
#define MIRAIOSBRIDGE_T2338730851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraiOSBridge
struct  MiraiOSBridge_t2338730851  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAIOSBRIDGE_T2338730851_H
#ifndef MIRAEDITORPREVIEW_T3716774330_H
#define MIRAEDITORPREVIEW_T3716774330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraEditorPreview
struct  MiraEditorPreview_t3716774330  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAEDITORPREVIEW_T3716774330_H
#ifndef MIRAARCONTROLLER_T555016598_H
#define MIRAARCONTROLLER_T555016598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraArController
struct  MiraArController_t555016598  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.MiraArController::IPD
	float ___IPD_2;
	// UnityEngine.GameObject Mira.MiraArController::leftCam
	GameObject_t1756533147 * ___leftCam_3;
	// UnityEngine.GameObject Mira.MiraArController::rightCam
	GameObject_t1756533147 * ___rightCam_4;
	// UnityEngine.GameObject Mira.MiraArController::cameraRig
	GameObject_t1756533147 * ___cameraRig_5;
	// System.Boolean Mira.MiraArController::isRotationalOnly
	bool ___isRotationalOnly_6;
	// System.Boolean Mira.MiraArController::isSpectator
	bool ___isSpectator_7;
	// System.Single Mira.MiraArController::nearClipPlane
	float ___nearClipPlane_8;
	// System.Single Mira.MiraArController::farClipPlane
	float ___farClipPlane_9;
	// System.Boolean Mira.MiraArController::defaultScale
	bool ___defaultScale_10;
	// System.Single Mira.MiraArController::setScaleMultiplier
	float ___setScaleMultiplier_11;
	// System.Single Mira.MiraArController::stereoCamFov
	float ___stereoCamFov_13;
	// Mira.MiraViewer Mira.MiraArController::mv
	MiraViewer_t1267122109 * ___mv_14;
	// UnityEngine.Texture Mira.MiraArController::btnTexture
	Texture_t2243626319 * ___btnTexture_16;
	// System.Single Mira.MiraArController::buttonHeight
	float ___buttonHeight_17;
	// System.Single Mira.MiraArController::buttonWidth
	float ___buttonWidth_18;
	// UnityEngine.GUISkin Mira.MiraArController::MiraGuiSkin
	GUISkin_t1436893342 * ___MiraGuiSkin_19;
	// UnityEngine.EventSystems.MiraInputModule Mira.MiraArController::miraInputModule
	MiraInputModule_t2476427099 * ___miraInputModule_20;
	// System.Boolean Mira.MiraArController::GUIEnabled
	bool ___GUIEnabled_21;
	// UnityEngine.GameObject Mira.MiraArController::settingsMenu
	GameObject_t1756533147 * ___settingsMenu_22;

public:
	inline static int32_t get_offset_of_IPD_2() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___IPD_2)); }
	inline float get_IPD_2() const { return ___IPD_2; }
	inline float* get_address_of_IPD_2() { return &___IPD_2; }
	inline void set_IPD_2(float value)
	{
		___IPD_2 = value;
	}

	inline static int32_t get_offset_of_leftCam_3() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___leftCam_3)); }
	inline GameObject_t1756533147 * get_leftCam_3() const { return ___leftCam_3; }
	inline GameObject_t1756533147 ** get_address_of_leftCam_3() { return &___leftCam_3; }
	inline void set_leftCam_3(GameObject_t1756533147 * value)
	{
		___leftCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___leftCam_3), value);
	}

	inline static int32_t get_offset_of_rightCam_4() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___rightCam_4)); }
	inline GameObject_t1756533147 * get_rightCam_4() const { return ___rightCam_4; }
	inline GameObject_t1756533147 ** get_address_of_rightCam_4() { return &___rightCam_4; }
	inline void set_rightCam_4(GameObject_t1756533147 * value)
	{
		___rightCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___rightCam_4), value);
	}

	inline static int32_t get_offset_of_cameraRig_5() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___cameraRig_5)); }
	inline GameObject_t1756533147 * get_cameraRig_5() const { return ___cameraRig_5; }
	inline GameObject_t1756533147 ** get_address_of_cameraRig_5() { return &___cameraRig_5; }
	inline void set_cameraRig_5(GameObject_t1756533147 * value)
	{
		___cameraRig_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_5), value);
	}

	inline static int32_t get_offset_of_isRotationalOnly_6() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___isRotationalOnly_6)); }
	inline bool get_isRotationalOnly_6() const { return ___isRotationalOnly_6; }
	inline bool* get_address_of_isRotationalOnly_6() { return &___isRotationalOnly_6; }
	inline void set_isRotationalOnly_6(bool value)
	{
		___isRotationalOnly_6 = value;
	}

	inline static int32_t get_offset_of_isSpectator_7() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___isSpectator_7)); }
	inline bool get_isSpectator_7() const { return ___isSpectator_7; }
	inline bool* get_address_of_isSpectator_7() { return &___isSpectator_7; }
	inline void set_isSpectator_7(bool value)
	{
		___isSpectator_7 = value;
	}

	inline static int32_t get_offset_of_nearClipPlane_8() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___nearClipPlane_8)); }
	inline float get_nearClipPlane_8() const { return ___nearClipPlane_8; }
	inline float* get_address_of_nearClipPlane_8() { return &___nearClipPlane_8; }
	inline void set_nearClipPlane_8(float value)
	{
		___nearClipPlane_8 = value;
	}

	inline static int32_t get_offset_of_farClipPlane_9() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___farClipPlane_9)); }
	inline float get_farClipPlane_9() const { return ___farClipPlane_9; }
	inline float* get_address_of_farClipPlane_9() { return &___farClipPlane_9; }
	inline void set_farClipPlane_9(float value)
	{
		___farClipPlane_9 = value;
	}

	inline static int32_t get_offset_of_defaultScale_10() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___defaultScale_10)); }
	inline bool get_defaultScale_10() const { return ___defaultScale_10; }
	inline bool* get_address_of_defaultScale_10() { return &___defaultScale_10; }
	inline void set_defaultScale_10(bool value)
	{
		___defaultScale_10 = value;
	}

	inline static int32_t get_offset_of_setScaleMultiplier_11() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___setScaleMultiplier_11)); }
	inline float get_setScaleMultiplier_11() const { return ___setScaleMultiplier_11; }
	inline float* get_address_of_setScaleMultiplier_11() { return &___setScaleMultiplier_11; }
	inline void set_setScaleMultiplier_11(float value)
	{
		___setScaleMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_stereoCamFov_13() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___stereoCamFov_13)); }
	inline float get_stereoCamFov_13() const { return ___stereoCamFov_13; }
	inline float* get_address_of_stereoCamFov_13() { return &___stereoCamFov_13; }
	inline void set_stereoCamFov_13(float value)
	{
		___stereoCamFov_13 = value;
	}

	inline static int32_t get_offset_of_mv_14() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___mv_14)); }
	inline MiraViewer_t1267122109 * get_mv_14() const { return ___mv_14; }
	inline MiraViewer_t1267122109 ** get_address_of_mv_14() { return &___mv_14; }
	inline void set_mv_14(MiraViewer_t1267122109 * value)
	{
		___mv_14 = value;
		Il2CppCodeGenWriteBarrier((&___mv_14), value);
	}

	inline static int32_t get_offset_of_btnTexture_16() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___btnTexture_16)); }
	inline Texture_t2243626319 * get_btnTexture_16() const { return ___btnTexture_16; }
	inline Texture_t2243626319 ** get_address_of_btnTexture_16() { return &___btnTexture_16; }
	inline void set_btnTexture_16(Texture_t2243626319 * value)
	{
		___btnTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___btnTexture_16), value);
	}

	inline static int32_t get_offset_of_buttonHeight_17() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___buttonHeight_17)); }
	inline float get_buttonHeight_17() const { return ___buttonHeight_17; }
	inline float* get_address_of_buttonHeight_17() { return &___buttonHeight_17; }
	inline void set_buttonHeight_17(float value)
	{
		___buttonHeight_17 = value;
	}

	inline static int32_t get_offset_of_buttonWidth_18() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___buttonWidth_18)); }
	inline float get_buttonWidth_18() const { return ___buttonWidth_18; }
	inline float* get_address_of_buttonWidth_18() { return &___buttonWidth_18; }
	inline void set_buttonWidth_18(float value)
	{
		___buttonWidth_18 = value;
	}

	inline static int32_t get_offset_of_MiraGuiSkin_19() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___MiraGuiSkin_19)); }
	inline GUISkin_t1436893342 * get_MiraGuiSkin_19() const { return ___MiraGuiSkin_19; }
	inline GUISkin_t1436893342 ** get_address_of_MiraGuiSkin_19() { return &___MiraGuiSkin_19; }
	inline void set_MiraGuiSkin_19(GUISkin_t1436893342 * value)
	{
		___MiraGuiSkin_19 = value;
		Il2CppCodeGenWriteBarrier((&___MiraGuiSkin_19), value);
	}

	inline static int32_t get_offset_of_miraInputModule_20() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___miraInputModule_20)); }
	inline MiraInputModule_t2476427099 * get_miraInputModule_20() const { return ___miraInputModule_20; }
	inline MiraInputModule_t2476427099 ** get_address_of_miraInputModule_20() { return &___miraInputModule_20; }
	inline void set_miraInputModule_20(MiraInputModule_t2476427099 * value)
	{
		___miraInputModule_20 = value;
		Il2CppCodeGenWriteBarrier((&___miraInputModule_20), value);
	}

	inline static int32_t get_offset_of_GUIEnabled_21() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___GUIEnabled_21)); }
	inline bool get_GUIEnabled_21() const { return ___GUIEnabled_21; }
	inline bool* get_address_of_GUIEnabled_21() { return &___GUIEnabled_21; }
	inline void set_GUIEnabled_21(bool value)
	{
		___GUIEnabled_21 = value;
	}

	inline static int32_t get_offset_of_settingsMenu_22() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___settingsMenu_22)); }
	inline GameObject_t1756533147 * get_settingsMenu_22() const { return ___settingsMenu_22; }
	inline GameObject_t1756533147 ** get_address_of_settingsMenu_22() { return &___settingsMenu_22; }
	inline void set_settingsMenu_22(GameObject_t1756533147 * value)
	{
		___settingsMenu_22 = value;
		Il2CppCodeGenWriteBarrier((&___settingsMenu_22), value);
	}
};

struct MiraArController_t555016598_StaticFields
{
public:
	// System.Single Mira.MiraArController::scaleMultiplier
	float ___scaleMultiplier_12;
	// Mira.MiraArController Mira.MiraArController::instance
	MiraArController_t555016598 * ___instance_15;

public:
	inline static int32_t get_offset_of_scaleMultiplier_12() { return static_cast<int32_t>(offsetof(MiraArController_t555016598_StaticFields, ___scaleMultiplier_12)); }
	inline float get_scaleMultiplier_12() const { return ___scaleMultiplier_12; }
	inline float* get_address_of_scaleMultiplier_12() { return &___scaleMultiplier_12; }
	inline void set_scaleMultiplier_12(float value)
	{
		___scaleMultiplier_12 = value;
	}

	inline static int32_t get_offset_of_instance_15() { return static_cast<int32_t>(offsetof(MiraArController_t555016598_StaticFields, ___instance_15)); }
	inline MiraArController_t555016598 * get_instance_15() const { return ___instance_15; }
	inline MiraArController_t555016598 ** get_address_of_instance_15() { return &___instance_15; }
	inline void set_instance_15(MiraArController_t555016598 * value)
	{
		___instance_15 = value;
		Il2CppCodeGenWriteBarrier((&___instance_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAARCONTROLLER_T555016598_H
#ifndef LASERLERP_T3011343440_H
#define LASERLERP_T3011343440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.LaserLerp
struct  LaserLerp_t3011343440  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.LaserLerp::laserLerpSpeed
	float ___laserLerpSpeed_2;

public:
	inline static int32_t get_offset_of_laserLerpSpeed_2() { return static_cast<int32_t>(offsetof(LaserLerp_t3011343440, ___laserLerpSpeed_2)); }
	inline float get_laserLerpSpeed_2() const { return ___laserLerpSpeed_2; }
	inline float* get_address_of_laserLerpSpeed_2() { return &___laserLerpSpeed_2; }
	inline void set_laserLerpSpeed_2(float value)
	{
		___laserLerpSpeed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LASERLERP_T3011343440_H
#ifndef SHADERPROPANIMATOR_T2679013775_H
#define SHADERPROPANIMATOR_T2679013775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t2679013775  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t257310565 * ___m_Renderer_2;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t193706927 * ___m_Material_3;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3306541151 * ___GlowCurve_4;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_5;

public:
	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___m_Renderer_2)); }
	inline Renderer_t257310565 * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline Renderer_t257310565 ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(Renderer_t257310565 * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___m_Material_3)); }
	inline Material_t193706927 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t193706927 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t193706927 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}

	inline static int32_t get_offset_of_GlowCurve_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___GlowCurve_4)); }
	inline AnimationCurve_t3306541151 * get_GlowCurve_4() const { return ___GlowCurve_4; }
	inline AnimationCurve_t3306541151 ** get_address_of_GlowCurve_4() { return &___GlowCurve_4; }
	inline void set_GlowCurve_4(AnimationCurve_t3306541151 * value)
	{
		___GlowCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_4), value);
	}

	inline static int32_t get_offset_of_m_frame_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___m_frame_5)); }
	inline float get_m_frame_5() const { return ___m_frame_5; }
	inline float* get_address_of_m_frame_5() { return &___m_frame_5; }
	inline void set_m_frame_5(float value)
	{
		___m_frame_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T2679013775_H
#ifndef MARKERROTATIONWATCHER_T3969903492_H
#define MARKERROTATIONWATCHER_T3969903492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerRotationWatcher
struct  MarkerRotationWatcher_t3969903492  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MarkerRotationWatcher::mainCamera
	Transform_t3275118058 * ___mainCamera_2;
	// System.Single MarkerRotationWatcher::deltaDotProduct
	float ___deltaDotProduct_3;
	// System.Single MarkerRotationWatcher::necessaryRotation
	float ___necessaryRotation_4;
	// System.Single MarkerRotationWatcher::dot
	float ___dot_5;
	// UnityEngine.Vector3 MarkerRotationWatcher::startVector
	Vector3_t2243707580  ___startVector_7;
	// System.Boolean MarkerRotationWatcher::firstAngle
	bool ___firstAngle_8;
	// System.Single MarkerRotationWatcher::counter
	float ___counter_9;
	// System.Boolean MarkerRotationWatcher::hasRotated
	bool ___hasRotated_10;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___mainCamera_2)); }
	inline Transform_t3275118058 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Transform_t3275118058 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Transform_t3275118058 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}

	inline static int32_t get_offset_of_deltaDotProduct_3() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___deltaDotProduct_3)); }
	inline float get_deltaDotProduct_3() const { return ___deltaDotProduct_3; }
	inline float* get_address_of_deltaDotProduct_3() { return &___deltaDotProduct_3; }
	inline void set_deltaDotProduct_3(float value)
	{
		___deltaDotProduct_3 = value;
	}

	inline static int32_t get_offset_of_necessaryRotation_4() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___necessaryRotation_4)); }
	inline float get_necessaryRotation_4() const { return ___necessaryRotation_4; }
	inline float* get_address_of_necessaryRotation_4() { return &___necessaryRotation_4; }
	inline void set_necessaryRotation_4(float value)
	{
		___necessaryRotation_4 = value;
	}

	inline static int32_t get_offset_of_dot_5() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___dot_5)); }
	inline float get_dot_5() const { return ___dot_5; }
	inline float* get_address_of_dot_5() { return &___dot_5; }
	inline void set_dot_5(float value)
	{
		___dot_5 = value;
	}

	inline static int32_t get_offset_of_startVector_7() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___startVector_7)); }
	inline Vector3_t2243707580  get_startVector_7() const { return ___startVector_7; }
	inline Vector3_t2243707580 * get_address_of_startVector_7() { return &___startVector_7; }
	inline void set_startVector_7(Vector3_t2243707580  value)
	{
		___startVector_7 = value;
	}

	inline static int32_t get_offset_of_firstAngle_8() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___firstAngle_8)); }
	inline bool get_firstAngle_8() const { return ___firstAngle_8; }
	inline bool* get_address_of_firstAngle_8() { return &___firstAngle_8; }
	inline void set_firstAngle_8(bool value)
	{
		___firstAngle_8 = value;
	}

	inline static int32_t get_offset_of_counter_9() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___counter_9)); }
	inline float get_counter_9() const { return ___counter_9; }
	inline float* get_address_of_counter_9() { return &___counter_9; }
	inline void set_counter_9(float value)
	{
		___counter_9 = value;
	}

	inline static int32_t get_offset_of_hasRotated_10() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___hasRotated_10)); }
	inline bool get_hasRotated_10() const { return ___hasRotated_10; }
	inline bool* get_address_of_hasRotated_10() { return &___hasRotated_10; }
	inline void set_hasRotated_10(bool value)
	{
		___hasRotated_10 = value;
	}
};

struct MarkerRotationWatcher_t3969903492_StaticFields
{
public:
	// MarkerRotationWatcher/RotateAction MarkerRotationWatcher::OnRotated
	RotateAction_t2368064732 * ___OnRotated_6;

public:
	inline static int32_t get_offset_of_OnRotated_6() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492_StaticFields, ___OnRotated_6)); }
	inline RotateAction_t2368064732 * get_OnRotated_6() const { return ___OnRotated_6; }
	inline RotateAction_t2368064732 ** get_address_of_OnRotated_6() { return &___OnRotated_6; }
	inline void set_OnRotated_6(RotateAction_t2368064732 * value)
	{
		___OnRotated_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnRotated_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERROTATIONWATCHER_T3969903492_H
#ifndef ROTATIONALTRACKINGMANAGER_T1512110775_H
#define ROTATIONALTRACKINGMANAGER_T1512110775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.RotationalTrackingManager
struct  RotationalTrackingManager_t1512110775  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Mira.RotationalTrackingManager::mainCamera
	Transform_t3275118058 * ___mainCamera_2;
	// UnityEngine.Transform Mira.RotationalTrackingManager::cameraRig
	Transform_t3275118058 * ___cameraRig_3;
	// System.Boolean Mira.RotationalTrackingManager::isRotational
	bool ___isRotational_4;
	// System.Collections.Generic.Queue`1<UnityEngine.Vector3> Mira.RotationalTrackingManager::positionBuffer
	Queue_1_t2063364415 * ___positionBuffer_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Quaternion> Mira.RotationalTrackingManager::rotationBuffer
	Queue_1_t3849730753 * ___rotationBuffer_6;
	// System.Single Mira.RotationalTrackingManager::delay
	float ___delay_7;
	// System.Int32 Mira.RotationalTrackingManager::bufferSize
	int32_t ___bufferSize_8;
	// System.Int32 Mira.RotationalTrackingManager::bufferDiscardLast
	int32_t ___bufferDiscardLast_9;
	// UnityEngine.Vector3 Mira.RotationalTrackingManager::camRigStartPosition
	Vector3_t2243707580  ___camRigStartPosition_10;
	// System.Boolean Mira.RotationalTrackingManager::isSpectator
	bool ___isSpectator_11;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___mainCamera_2)); }
	inline Transform_t3275118058 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Transform_t3275118058 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Transform_t3275118058 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}

	inline static int32_t get_offset_of_cameraRig_3() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___cameraRig_3)); }
	inline Transform_t3275118058 * get_cameraRig_3() const { return ___cameraRig_3; }
	inline Transform_t3275118058 ** get_address_of_cameraRig_3() { return &___cameraRig_3; }
	inline void set_cameraRig_3(Transform_t3275118058 * value)
	{
		___cameraRig_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_3), value);
	}

	inline static int32_t get_offset_of_isRotational_4() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___isRotational_4)); }
	inline bool get_isRotational_4() const { return ___isRotational_4; }
	inline bool* get_address_of_isRotational_4() { return &___isRotational_4; }
	inline void set_isRotational_4(bool value)
	{
		___isRotational_4 = value;
	}

	inline static int32_t get_offset_of_positionBuffer_5() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___positionBuffer_5)); }
	inline Queue_1_t2063364415 * get_positionBuffer_5() const { return ___positionBuffer_5; }
	inline Queue_1_t2063364415 ** get_address_of_positionBuffer_5() { return &___positionBuffer_5; }
	inline void set_positionBuffer_5(Queue_1_t2063364415 * value)
	{
		___positionBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___positionBuffer_5), value);
	}

	inline static int32_t get_offset_of_rotationBuffer_6() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___rotationBuffer_6)); }
	inline Queue_1_t3849730753 * get_rotationBuffer_6() const { return ___rotationBuffer_6; }
	inline Queue_1_t3849730753 ** get_address_of_rotationBuffer_6() { return &___rotationBuffer_6; }
	inline void set_rotationBuffer_6(Queue_1_t3849730753 * value)
	{
		___rotationBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotationBuffer_6), value);
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_bufferSize_8() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___bufferSize_8)); }
	inline int32_t get_bufferSize_8() const { return ___bufferSize_8; }
	inline int32_t* get_address_of_bufferSize_8() { return &___bufferSize_8; }
	inline void set_bufferSize_8(int32_t value)
	{
		___bufferSize_8 = value;
	}

	inline static int32_t get_offset_of_bufferDiscardLast_9() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___bufferDiscardLast_9)); }
	inline int32_t get_bufferDiscardLast_9() const { return ___bufferDiscardLast_9; }
	inline int32_t* get_address_of_bufferDiscardLast_9() { return &___bufferDiscardLast_9; }
	inline void set_bufferDiscardLast_9(int32_t value)
	{
		___bufferDiscardLast_9 = value;
	}

	inline static int32_t get_offset_of_camRigStartPosition_10() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___camRigStartPosition_10)); }
	inline Vector3_t2243707580  get_camRigStartPosition_10() const { return ___camRigStartPosition_10; }
	inline Vector3_t2243707580 * get_address_of_camRigStartPosition_10() { return &___camRigStartPosition_10; }
	inline void set_camRigStartPosition_10(Vector3_t2243707580  value)
	{
		___camRigStartPosition_10 = value;
	}

	inline static int32_t get_offset_of_isSpectator_11() { return static_cast<int32_t>(offsetof(RotationalTrackingManager_t1512110775, ___isSpectator_11)); }
	inline bool get_isSpectator_11() const { return ___isSpectator_11; }
	inline bool* get_address_of_isSpectator_11() { return &___isSpectator_11; }
	inline void set_isSpectator_11(bool value)
	{
		___isSpectator_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALTRACKINGMANAGER_T1512110775_H
#ifndef MIRAGRABEXAMPLE_T576458027_H
#define MIRAGRABEXAMPLE_T576458027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraGrabExample
struct  MiraGrabExample_t576458027  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MiraGrabExample::textBeneathPlanet
	Text_t356221433 * ___textBeneathPlanet_2;
	// System.Boolean MiraGrabExample::isGrabbing
	bool ___isGrabbing_3;
	// System.Single MiraGrabExample::lastTouchPosition
	float ___lastTouchPosition_4;

public:
	inline static int32_t get_offset_of_textBeneathPlanet_2() { return static_cast<int32_t>(offsetof(MiraGrabExample_t576458027, ___textBeneathPlanet_2)); }
	inline Text_t356221433 * get_textBeneathPlanet_2() const { return ___textBeneathPlanet_2; }
	inline Text_t356221433 ** get_address_of_textBeneathPlanet_2() { return &___textBeneathPlanet_2; }
	inline void set_textBeneathPlanet_2(Text_t356221433 * value)
	{
		___textBeneathPlanet_2 = value;
		Il2CppCodeGenWriteBarrier((&___textBeneathPlanet_2), value);
	}

	inline static int32_t get_offset_of_isGrabbing_3() { return static_cast<int32_t>(offsetof(MiraGrabExample_t576458027, ___isGrabbing_3)); }
	inline bool get_isGrabbing_3() const { return ___isGrabbing_3; }
	inline bool* get_address_of_isGrabbing_3() { return &___isGrabbing_3; }
	inline void set_isGrabbing_3(bool value)
	{
		___isGrabbing_3 = value;
	}

	inline static int32_t get_offset_of_lastTouchPosition_4() { return static_cast<int32_t>(offsetof(MiraGrabExample_t576458027, ___lastTouchPosition_4)); }
	inline float get_lastTouchPosition_4() const { return ___lastTouchPosition_4; }
	inline float* get_address_of_lastTouchPosition_4() { return &___lastTouchPosition_4; }
	inline void set_lastTouchPosition_4(float value)
	{
		___lastTouchPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAGRABEXAMPLE_T576458027_H
#ifndef MIRAPHYSICSGRABEXAMPLE_T1656480436_H
#define MIRAPHYSICSGRABEXAMPLE_T1656480436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsGrabExample
struct  MiraPhysicsGrabExample_t1656480436  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean MiraPhysicsGrabExample::isGrabbing
	bool ___isGrabbing_2;
	// UnityEngine.RigidbodyConstraints MiraPhysicsGrabExample::originalConstraints
	int32_t ___originalConstraints_3;
	// UnityEngine.Rigidbody MiraPhysicsGrabExample::rigidBody
	Rigidbody_t4233889191 * ___rigidBody_4;
	// System.Single MiraPhysicsGrabExample::lastTouchPosition
	float ___lastTouchPosition_5;

public:
	inline static int32_t get_offset_of_isGrabbing_2() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t1656480436, ___isGrabbing_2)); }
	inline bool get_isGrabbing_2() const { return ___isGrabbing_2; }
	inline bool* get_address_of_isGrabbing_2() { return &___isGrabbing_2; }
	inline void set_isGrabbing_2(bool value)
	{
		___isGrabbing_2 = value;
	}

	inline static int32_t get_offset_of_originalConstraints_3() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t1656480436, ___originalConstraints_3)); }
	inline int32_t get_originalConstraints_3() const { return ___originalConstraints_3; }
	inline int32_t* get_address_of_originalConstraints_3() { return &___originalConstraints_3; }
	inline void set_originalConstraints_3(int32_t value)
	{
		___originalConstraints_3 = value;
	}

	inline static int32_t get_offset_of_rigidBody_4() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t1656480436, ___rigidBody_4)); }
	inline Rigidbody_t4233889191 * get_rigidBody_4() const { return ___rigidBody_4; }
	inline Rigidbody_t4233889191 ** get_address_of_rigidBody_4() { return &___rigidBody_4; }
	inline void set_rigidBody_4(Rigidbody_t4233889191 * value)
	{
		___rigidBody_4 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_4), value);
	}

	inline static int32_t get_offset_of_lastTouchPosition_5() { return static_cast<int32_t>(offsetof(MiraPhysicsGrabExample_t1656480436, ___lastTouchPosition_5)); }
	inline float get_lastTouchPosition_5() const { return ___lastTouchPosition_5; }
	inline float* get_address_of_lastTouchPosition_5() { return &___lastTouchPosition_5; }
	inline void set_lastTouchPosition_5(float value)
	{
		___lastTouchPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSGRABEXAMPLE_T1656480436_H
#ifndef MIRAPHYSICSPLAYPENBOUNDARY_T3180346929_H
#define MIRAPHYSICSPLAYPENBOUNDARY_T3180346929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsPlaypenBoundary
struct  MiraPhysicsPlaypenBoundary_t3180346929  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSPLAYPENBOUNDARY_T3180346929_H
#ifndef TOGGLEMODES_T4125091540_H
#define TOGGLEMODES_T4125091540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleModes
struct  ToggleModes_t4125091540  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEMODES_T4125091540_H
#ifndef TRACKERLERPDRIVER_T1630291735_H
#define TRACKERLERPDRIVER_T1630291735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackerLerpDriver
struct  TrackerLerpDriver_t1630291735  : public MonoBehaviour_t1158329972
{
public:
	// MarkerRotationWatcher TrackerLerpDriver::rotationWatcher
	MarkerRotationWatcher_t3969903492 * ___rotationWatcher_2;
	// UnityEngine.Vector3 TrackerLerpDriver::startPosition
	Vector3_t2243707580  ___startPosition_3;
	// UnityEngine.Vector3 TrackerLerpDriver::endPosition
	Vector3_t2243707580  ___endPosition_4;

public:
	inline static int32_t get_offset_of_rotationWatcher_2() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t1630291735, ___rotationWatcher_2)); }
	inline MarkerRotationWatcher_t3969903492 * get_rotationWatcher_2() const { return ___rotationWatcher_2; }
	inline MarkerRotationWatcher_t3969903492 ** get_address_of_rotationWatcher_2() { return &___rotationWatcher_2; }
	inline void set_rotationWatcher_2(MarkerRotationWatcher_t3969903492 * value)
	{
		___rotationWatcher_2 = value;
		Il2CppCodeGenWriteBarrier((&___rotationWatcher_2), value);
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t1630291735, ___startPosition_3)); }
	inline Vector3_t2243707580  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t2243707580 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t2243707580  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_endPosition_4() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t1630291735, ___endPosition_4)); }
	inline Vector3_t2243707580  get_endPosition_4() const { return ___endPosition_4; }
	inline Vector3_t2243707580 * get_address_of_endPosition_4() { return &___endPosition_4; }
	inline void set_endPosition_4(Vector3_t2243707580  value)
	{
		___endPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERLERPDRIVER_T1630291735_H
#ifndef FISHINGLINECAST_T1149735085_H
#define FISHINGLINECAST_T1149735085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.FishinglineCast
struct  FishinglineCast_t1149735085  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.FishinglineCast::minCast
	float ___minCast_2;
	// System.Single Mira.FishinglineCast::maxCast
	float ___maxCast_3;
	// System.Single Mira.FishinglineCast::castSpeedMultiplier
	float ___castSpeedMultiplier_4;
	// System.Boolean Mira.FishinglineCast::castStart
	bool ___castStart_5;
	// System.Single Mira.FishinglineCast::lastCast
	float ___lastCast_6;
	// Mira.Fishingline Mira.FishinglineCast::fishingline
	Fishingline_t2350076148 * ___fishingline_7;
	// System.Single Mira.FishinglineCast::currentScaleMult
	float ___currentScaleMult_8;

public:
	inline static int32_t get_offset_of_minCast_2() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___minCast_2)); }
	inline float get_minCast_2() const { return ___minCast_2; }
	inline float* get_address_of_minCast_2() { return &___minCast_2; }
	inline void set_minCast_2(float value)
	{
		___minCast_2 = value;
	}

	inline static int32_t get_offset_of_maxCast_3() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___maxCast_3)); }
	inline float get_maxCast_3() const { return ___maxCast_3; }
	inline float* get_address_of_maxCast_3() { return &___maxCast_3; }
	inline void set_maxCast_3(float value)
	{
		___maxCast_3 = value;
	}

	inline static int32_t get_offset_of_castSpeedMultiplier_4() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___castSpeedMultiplier_4)); }
	inline float get_castSpeedMultiplier_4() const { return ___castSpeedMultiplier_4; }
	inline float* get_address_of_castSpeedMultiplier_4() { return &___castSpeedMultiplier_4; }
	inline void set_castSpeedMultiplier_4(float value)
	{
		___castSpeedMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_castStart_5() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___castStart_5)); }
	inline bool get_castStart_5() const { return ___castStart_5; }
	inline bool* get_address_of_castStart_5() { return &___castStart_5; }
	inline void set_castStart_5(bool value)
	{
		___castStart_5 = value;
	}

	inline static int32_t get_offset_of_lastCast_6() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___lastCast_6)); }
	inline float get_lastCast_6() const { return ___lastCast_6; }
	inline float* get_address_of_lastCast_6() { return &___lastCast_6; }
	inline void set_lastCast_6(float value)
	{
		___lastCast_6 = value;
	}

	inline static int32_t get_offset_of_fishingline_7() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___fishingline_7)); }
	inline Fishingline_t2350076148 * get_fishingline_7() const { return ___fishingline_7; }
	inline Fishingline_t2350076148 ** get_address_of_fishingline_7() { return &___fishingline_7; }
	inline void set_fishingline_7(Fishingline_t2350076148 * value)
	{
		___fishingline_7 = value;
		Il2CppCodeGenWriteBarrier((&___fishingline_7), value);
	}

	inline static int32_t get_offset_of_currentScaleMult_8() { return static_cast<int32_t>(offsetof(FishinglineCast_t1149735085, ___currentScaleMult_8)); }
	inline float get_currentScaleMult_8() const { return ___currentScaleMult_8; }
	inline float* get_address_of_currentScaleMult_8() { return &___currentScaleMult_8; }
	inline void set_currentScaleMult_8(float value)
	{
		___currentScaleMult_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHINGLINECAST_T1149735085_H
#ifndef TRANSITION2D_T3717442181_H
#define TRANSITION2D_T3717442181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Transition2D
struct  Transition2D_t3717442181  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Mira.Transition2D::displayOnce
	bool ___displayOnce_2;
	// UnityEngine.GameObject Mira.Transition2D::MiraCameraRig
	GameObject_t1756533147 * ___MiraCameraRig_3;
	// UnityEngine.Canvas Mira.Transition2D::instructionScreen
	Canvas_t209405766 * ___instructionScreen_4;
	// UnityEngine.Camera Mira.Transition2D::twoDCamera
	Camera_t189460977 * ___twoDCamera_5;
	// UnityEngine.Camera Mira.Transition2D::distortionL
	Camera_t189460977 * ___distortionL_6;
	// UnityEngine.Camera Mira.Transition2D::distortionR
	Camera_t189460977 * ___distortionR_7;

public:
	inline static int32_t get_offset_of_displayOnce_2() { return static_cast<int32_t>(offsetof(Transition2D_t3717442181, ___displayOnce_2)); }
	inline bool get_displayOnce_2() const { return ___displayOnce_2; }
	inline bool* get_address_of_displayOnce_2() { return &___displayOnce_2; }
	inline void set_displayOnce_2(bool value)
	{
		___displayOnce_2 = value;
	}

	inline static int32_t get_offset_of_MiraCameraRig_3() { return static_cast<int32_t>(offsetof(Transition2D_t3717442181, ___MiraCameraRig_3)); }
	inline GameObject_t1756533147 * get_MiraCameraRig_3() const { return ___MiraCameraRig_3; }
	inline GameObject_t1756533147 ** get_address_of_MiraCameraRig_3() { return &___MiraCameraRig_3; }
	inline void set_MiraCameraRig_3(GameObject_t1756533147 * value)
	{
		___MiraCameraRig_3 = value;
		Il2CppCodeGenWriteBarrier((&___MiraCameraRig_3), value);
	}

	inline static int32_t get_offset_of_instructionScreen_4() { return static_cast<int32_t>(offsetof(Transition2D_t3717442181, ___instructionScreen_4)); }
	inline Canvas_t209405766 * get_instructionScreen_4() const { return ___instructionScreen_4; }
	inline Canvas_t209405766 ** get_address_of_instructionScreen_4() { return &___instructionScreen_4; }
	inline void set_instructionScreen_4(Canvas_t209405766 * value)
	{
		___instructionScreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___instructionScreen_4), value);
	}

	inline static int32_t get_offset_of_twoDCamera_5() { return static_cast<int32_t>(offsetof(Transition2D_t3717442181, ___twoDCamera_5)); }
	inline Camera_t189460977 * get_twoDCamera_5() const { return ___twoDCamera_5; }
	inline Camera_t189460977 ** get_address_of_twoDCamera_5() { return &___twoDCamera_5; }
	inline void set_twoDCamera_5(Camera_t189460977 * value)
	{
		___twoDCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___twoDCamera_5), value);
	}

	inline static int32_t get_offset_of_distortionL_6() { return static_cast<int32_t>(offsetof(Transition2D_t3717442181, ___distortionL_6)); }
	inline Camera_t189460977 * get_distortionL_6() const { return ___distortionL_6; }
	inline Camera_t189460977 ** get_address_of_distortionL_6() { return &___distortionL_6; }
	inline void set_distortionL_6(Camera_t189460977 * value)
	{
		___distortionL_6 = value;
		Il2CppCodeGenWriteBarrier((&___distortionL_6), value);
	}

	inline static int32_t get_offset_of_distortionR_7() { return static_cast<int32_t>(offsetof(Transition2D_t3717442181, ___distortionR_7)); }
	inline Camera_t189460977 * get_distortionR_7() const { return ___distortionR_7; }
	inline Camera_t189460977 ** get_address_of_distortionR_7() { return &___distortionR_7; }
	inline void set_distortionR_7(Camera_t189460977 * value)
	{
		___distortionR_7 = value;
		Il2CppCodeGenWriteBarrier((&___distortionR_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION2D_T3717442181_H
#ifndef AXISSPIN_T3838354289_H
#define AXISSPIN_T3838354289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AxisSpin
struct  AxisSpin_t3838354289  : public MonoBehaviour_t1158329972
{
public:
	// System.Single AxisSpin::spinRate
	float ___spinRate_2;
	// UnityEngine.Vector3 AxisSpin::spinDirection
	Vector3_t2243707580  ___spinDirection_3;

public:
	inline static int32_t get_offset_of_spinRate_2() { return static_cast<int32_t>(offsetof(AxisSpin_t3838354289, ___spinRate_2)); }
	inline float get_spinRate_2() const { return ___spinRate_2; }
	inline float* get_address_of_spinRate_2() { return &___spinRate_2; }
	inline void set_spinRate_2(float value)
	{
		___spinRate_2 = value;
	}

	inline static int32_t get_offset_of_spinDirection_3() { return static_cast<int32_t>(offsetof(AxisSpin_t3838354289, ___spinDirection_3)); }
	inline Vector3_t2243707580  get_spinDirection_3() const { return ___spinDirection_3; }
	inline Vector3_t2243707580 * get_address_of_spinDirection_3() { return &___spinDirection_3; }
	inline void set_spinDirection_3(Vector3_t2243707580  value)
	{
		___spinDirection_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSPIN_T3838354289_H
#ifndef DEMOSCENEMANAGER_T779426248_H
#define DEMOSCENEMANAGER_T779426248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoSceneManager
struct  DemoSceneManager_t779426248  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct DemoSceneManager_t779426248_StaticFields
{
public:
	// System.Boolean DemoSceneManager::isSpectator
	bool ___isSpectator_2;
	// DemoSceneManager DemoSceneManager::instance
	DemoSceneManager_t779426248 * ___instance_3;

public:
	inline static int32_t get_offset_of_isSpectator_2() { return static_cast<int32_t>(offsetof(DemoSceneManager_t779426248_StaticFields, ___isSpectator_2)); }
	inline bool get_isSpectator_2() const { return ___isSpectator_2; }
	inline bool* get_address_of_isSpectator_2() { return &___isSpectator_2; }
	inline void set_isSpectator_2(bool value)
	{
		___isSpectator_2 = value;
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(DemoSceneManager_t779426248_StaticFields, ___instance_3)); }
	inline DemoSceneManager_t779426248 * get_instance_3() const { return ___instance_3; }
	inline DemoSceneManager_t779426248 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(DemoSceneManager_t779426248 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCENEMANAGER_T779426248_H
#ifndef DYNAMICALLYCHANGEIPD_T626189094_H
#define DYNAMICALLYCHANGEIPD_T626189094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicallyChangeIPD
struct  DynamicallyChangeIPD_t626189094  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICALLYCHANGEIPD_T626189094_H
#ifndef FACECAMERA_T2774504826_H
#define FACECAMERA_T2774504826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCamera
struct  FaceCamera_t2774504826  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform FaceCamera::stereoCamRig
	Transform_t3275118058 * ___stereoCamRig_2;

public:
	inline static int32_t get_offset_of_stereoCamRig_2() { return static_cast<int32_t>(offsetof(FaceCamera_t2774504826, ___stereoCamRig_2)); }
	inline Transform_t3275118058 * get_stereoCamRig_2() const { return ___stereoCamRig_2; }
	inline Transform_t3275118058 ** get_address_of_stereoCamRig_2() { return &___stereoCamRig_2; }
	inline void set_stereoCamRig_2(Transform_t3275118058 * value)
	{
		___stereoCamRig_2 = value;
		Il2CppCodeGenWriteBarrier((&___stereoCamRig_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECAMERA_T2774504826_H
#ifndef FADEINSCENE_T62196113_H
#define FADEINSCENE_T62196113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeInScene
struct  FadeInScene_t62196113  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean FadeInScene::shouldFadeOut
	bool ___shouldFadeOut_2;
	// UnityEngine.Texture2D FadeInScene::fadeTexture
	Texture2D_t3542995729 * ___fadeTexture_3;
	// System.Single FadeInScene::fadeSpeed
	float ___fadeSpeed_4;
	// System.Int32 FadeInScene::drawDepth
	int32_t ___drawDepth_5;
	// System.Single FadeInScene::alpha
	float ___alpha_6;
	// System.Int32 FadeInScene::fadeDir
	int32_t ___fadeDir_7;
	// System.Boolean FadeInScene::shouldBlackOut
	bool ___shouldBlackOut_8;

public:
	inline static int32_t get_offset_of_shouldFadeOut_2() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___shouldFadeOut_2)); }
	inline bool get_shouldFadeOut_2() const { return ___shouldFadeOut_2; }
	inline bool* get_address_of_shouldFadeOut_2() { return &___shouldFadeOut_2; }
	inline void set_shouldFadeOut_2(bool value)
	{
		___shouldFadeOut_2 = value;
	}

	inline static int32_t get_offset_of_fadeTexture_3() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___fadeTexture_3)); }
	inline Texture2D_t3542995729 * get_fadeTexture_3() const { return ___fadeTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_fadeTexture_3() { return &___fadeTexture_3; }
	inline void set_fadeTexture_3(Texture2D_t3542995729 * value)
	{
		___fadeTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___fadeTexture_3), value);
	}

	inline static int32_t get_offset_of_fadeSpeed_4() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___fadeSpeed_4)); }
	inline float get_fadeSpeed_4() const { return ___fadeSpeed_4; }
	inline float* get_address_of_fadeSpeed_4() { return &___fadeSpeed_4; }
	inline void set_fadeSpeed_4(float value)
	{
		___fadeSpeed_4 = value;
	}

	inline static int32_t get_offset_of_drawDepth_5() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___drawDepth_5)); }
	inline int32_t get_drawDepth_5() const { return ___drawDepth_5; }
	inline int32_t* get_address_of_drawDepth_5() { return &___drawDepth_5; }
	inline void set_drawDepth_5(int32_t value)
	{
		___drawDepth_5 = value;
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}

	inline static int32_t get_offset_of_fadeDir_7() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___fadeDir_7)); }
	inline int32_t get_fadeDir_7() const { return ___fadeDir_7; }
	inline int32_t* get_address_of_fadeDir_7() { return &___fadeDir_7; }
	inline void set_fadeDir_7(int32_t value)
	{
		___fadeDir_7 = value;
	}

	inline static int32_t get_offset_of_shouldBlackOut_8() { return static_cast<int32_t>(offsetof(FadeInScene_t62196113, ___shouldBlackOut_8)); }
	inline bool get_shouldBlackOut_8() const { return ___shouldBlackOut_8; }
	inline bool* get_address_of_shouldBlackOut_8() { return &___shouldBlackOut_8; }
	inline void set_shouldBlackOut_8(bool value)
	{
		___shouldBlackOut_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEINSCENE_T62196113_H
#ifndef SETTINGSMANAGER_T2519859232_H
#define SETTINGSMANAGER_T2519859232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager
struct  SettingsManager_t2519859232  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SettingsManager::MainSettingsMenu
	GameObject_t1756533147 * ___MainSettingsMenu_2;
	// UnityEngine.GameObject SettingsManager::RemoteMenu
	GameObject_t1756533147 * ___RemoteMenu_3;
	// UnityEngine.GameObject SettingsManager::connectedNotification
	GameObject_t1756533147 * ___connectedNotification_4;
	// UnityEngine.GameObject SettingsManager::disconnectedNotification
	GameObject_t1756533147 * ___disconnectedNotification_5;
	// RemotesController SettingsManager::remotesController
	RemotesController_t2607477933 * ___remotesController_7;
	// UnityEngine.GameObject SettingsManager::settingsButton
	GameObject_t1756533147 * ___settingsButton_8;

public:
	inline static int32_t get_offset_of_MainSettingsMenu_2() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___MainSettingsMenu_2)); }
	inline GameObject_t1756533147 * get_MainSettingsMenu_2() const { return ___MainSettingsMenu_2; }
	inline GameObject_t1756533147 ** get_address_of_MainSettingsMenu_2() { return &___MainSettingsMenu_2; }
	inline void set_MainSettingsMenu_2(GameObject_t1756533147 * value)
	{
		___MainSettingsMenu_2 = value;
		Il2CppCodeGenWriteBarrier((&___MainSettingsMenu_2), value);
	}

	inline static int32_t get_offset_of_RemoteMenu_3() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___RemoteMenu_3)); }
	inline GameObject_t1756533147 * get_RemoteMenu_3() const { return ___RemoteMenu_3; }
	inline GameObject_t1756533147 ** get_address_of_RemoteMenu_3() { return &___RemoteMenu_3; }
	inline void set_RemoteMenu_3(GameObject_t1756533147 * value)
	{
		___RemoteMenu_3 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteMenu_3), value);
	}

	inline static int32_t get_offset_of_connectedNotification_4() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___connectedNotification_4)); }
	inline GameObject_t1756533147 * get_connectedNotification_4() const { return ___connectedNotification_4; }
	inline GameObject_t1756533147 ** get_address_of_connectedNotification_4() { return &___connectedNotification_4; }
	inline void set_connectedNotification_4(GameObject_t1756533147 * value)
	{
		___connectedNotification_4 = value;
		Il2CppCodeGenWriteBarrier((&___connectedNotification_4), value);
	}

	inline static int32_t get_offset_of_disconnectedNotification_5() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___disconnectedNotification_5)); }
	inline GameObject_t1756533147 * get_disconnectedNotification_5() const { return ___disconnectedNotification_5; }
	inline GameObject_t1756533147 ** get_address_of_disconnectedNotification_5() { return &___disconnectedNotification_5; }
	inline void set_disconnectedNotification_5(GameObject_t1756533147 * value)
	{
		___disconnectedNotification_5 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectedNotification_5), value);
	}

	inline static int32_t get_offset_of_remotesController_7() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___remotesController_7)); }
	inline RemotesController_t2607477933 * get_remotesController_7() const { return ___remotesController_7; }
	inline RemotesController_t2607477933 ** get_address_of_remotesController_7() { return &___remotesController_7; }
	inline void set_remotesController_7(RemotesController_t2607477933 * value)
	{
		___remotesController_7 = value;
		Il2CppCodeGenWriteBarrier((&___remotesController_7), value);
	}

	inline static int32_t get_offset_of_settingsButton_8() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___settingsButton_8)); }
	inline GameObject_t1756533147 * get_settingsButton_8() const { return ___settingsButton_8; }
	inline GameObject_t1756533147 ** get_address_of_settingsButton_8() { return &___settingsButton_8; }
	inline void set_settingsButton_8(GameObject_t1756533147 * value)
	{
		___settingsButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___settingsButton_8), value);
	}
};

struct SettingsManager_t2519859232_StaticFields
{
public:
	// SettingsManager SettingsManager::Instance
	SettingsManager_t2519859232 * ___Instance_6;

public:
	inline static int32_t get_offset_of_Instance_6() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232_StaticFields, ___Instance_6)); }
	inline SettingsManager_t2519859232 * get_Instance_6() const { return ___Instance_6; }
	inline SettingsManager_t2519859232 ** get_address_of_Instance_6() { return &___Instance_6; }
	inline void set_Instance_6(SettingsManager_t2519859232 * value)
	{
		___Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMANAGER_T2519859232_H
#ifndef FISHINGLINE_T2350076148_H
#define FISHINGLINE_T2350076148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.Fishingline
struct  Fishingline_t2350076148  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform[] Mira.Fishingline::currentSegments
	TransformU5BU5D_t3764228911* ___currentSegments_2;
	// UnityEngine.Transform[] Mira.Fishingline::referencePoints
	TransformU5BU5D_t3764228911* ___referencePoints_3;
	// UnityEngine.Vector3[] Mira.Fishingline::defaultPositions
	Vector3U5BU5D_t1172311765* ___defaultPositions_4;
	// System.Single Mira.Fishingline::defaultFishlineLength
	float ___defaultFishlineLength_5;
	// System.Int32 Mira.Fishingline::numSegments
	int32_t ___numSegments_6;
	// UnityEngine.Transform Mira.Fishingline::destination
	Transform_t3275118058 * ___destination_7;
	// System.Single Mira.Fishingline::hitDistance
	float ___hitDistance_8;
	// MiraReticle Mira.Fishingline::reticle
	MiraReticle_t2195475589 * ___reticle_9;
	// System.Single Mira.Fishingline::lerpSpeedMult
	float ___lerpSpeedMult_10;
	// UnityEngine.Transform Mira.Fishingline::fishinglineParent
	Transform_t3275118058 * ___fishinglineParent_11;
	// UnityEngine.LineRenderer Mira.Fishingline::lineRend
	LineRenderer_t849157671 * ___lineRend_12;
	// System.Boolean Mira.Fishingline::colliders
	bool ___colliders_13;
	// UnityEngine.GameObject Mira.Fishingline::parentedToEnd
	GameObject_t1756533147 * ___parentedToEnd_14;
	// UnityEngine.Transform Mira.Fishingline::attachPoint
	Transform_t3275118058 * ___attachPoint_15;

public:
	inline static int32_t get_offset_of_currentSegments_2() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___currentSegments_2)); }
	inline TransformU5BU5D_t3764228911* get_currentSegments_2() const { return ___currentSegments_2; }
	inline TransformU5BU5D_t3764228911** get_address_of_currentSegments_2() { return &___currentSegments_2; }
	inline void set_currentSegments_2(TransformU5BU5D_t3764228911* value)
	{
		___currentSegments_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentSegments_2), value);
	}

	inline static int32_t get_offset_of_referencePoints_3() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___referencePoints_3)); }
	inline TransformU5BU5D_t3764228911* get_referencePoints_3() const { return ___referencePoints_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_referencePoints_3() { return &___referencePoints_3; }
	inline void set_referencePoints_3(TransformU5BU5D_t3764228911* value)
	{
		___referencePoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___referencePoints_3), value);
	}

	inline static int32_t get_offset_of_defaultPositions_4() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___defaultPositions_4)); }
	inline Vector3U5BU5D_t1172311765* get_defaultPositions_4() const { return ___defaultPositions_4; }
	inline Vector3U5BU5D_t1172311765** get_address_of_defaultPositions_4() { return &___defaultPositions_4; }
	inline void set_defaultPositions_4(Vector3U5BU5D_t1172311765* value)
	{
		___defaultPositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultPositions_4), value);
	}

	inline static int32_t get_offset_of_defaultFishlineLength_5() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___defaultFishlineLength_5)); }
	inline float get_defaultFishlineLength_5() const { return ___defaultFishlineLength_5; }
	inline float* get_address_of_defaultFishlineLength_5() { return &___defaultFishlineLength_5; }
	inline void set_defaultFishlineLength_5(float value)
	{
		___defaultFishlineLength_5 = value;
	}

	inline static int32_t get_offset_of_numSegments_6() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___numSegments_6)); }
	inline int32_t get_numSegments_6() const { return ___numSegments_6; }
	inline int32_t* get_address_of_numSegments_6() { return &___numSegments_6; }
	inline void set_numSegments_6(int32_t value)
	{
		___numSegments_6 = value;
	}

	inline static int32_t get_offset_of_destination_7() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___destination_7)); }
	inline Transform_t3275118058 * get_destination_7() const { return ___destination_7; }
	inline Transform_t3275118058 ** get_address_of_destination_7() { return &___destination_7; }
	inline void set_destination_7(Transform_t3275118058 * value)
	{
		___destination_7 = value;
		Il2CppCodeGenWriteBarrier((&___destination_7), value);
	}

	inline static int32_t get_offset_of_hitDistance_8() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___hitDistance_8)); }
	inline float get_hitDistance_8() const { return ___hitDistance_8; }
	inline float* get_address_of_hitDistance_8() { return &___hitDistance_8; }
	inline void set_hitDistance_8(float value)
	{
		___hitDistance_8 = value;
	}

	inline static int32_t get_offset_of_reticle_9() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___reticle_9)); }
	inline MiraReticle_t2195475589 * get_reticle_9() const { return ___reticle_9; }
	inline MiraReticle_t2195475589 ** get_address_of_reticle_9() { return &___reticle_9; }
	inline void set_reticle_9(MiraReticle_t2195475589 * value)
	{
		___reticle_9 = value;
		Il2CppCodeGenWriteBarrier((&___reticle_9), value);
	}

	inline static int32_t get_offset_of_lerpSpeedMult_10() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___lerpSpeedMult_10)); }
	inline float get_lerpSpeedMult_10() const { return ___lerpSpeedMult_10; }
	inline float* get_address_of_lerpSpeedMult_10() { return &___lerpSpeedMult_10; }
	inline void set_lerpSpeedMult_10(float value)
	{
		___lerpSpeedMult_10 = value;
	}

	inline static int32_t get_offset_of_fishinglineParent_11() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___fishinglineParent_11)); }
	inline Transform_t3275118058 * get_fishinglineParent_11() const { return ___fishinglineParent_11; }
	inline Transform_t3275118058 ** get_address_of_fishinglineParent_11() { return &___fishinglineParent_11; }
	inline void set_fishinglineParent_11(Transform_t3275118058 * value)
	{
		___fishinglineParent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fishinglineParent_11), value);
	}

	inline static int32_t get_offset_of_lineRend_12() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___lineRend_12)); }
	inline LineRenderer_t849157671 * get_lineRend_12() const { return ___lineRend_12; }
	inline LineRenderer_t849157671 ** get_address_of_lineRend_12() { return &___lineRend_12; }
	inline void set_lineRend_12(LineRenderer_t849157671 * value)
	{
		___lineRend_12 = value;
		Il2CppCodeGenWriteBarrier((&___lineRend_12), value);
	}

	inline static int32_t get_offset_of_colliders_13() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___colliders_13)); }
	inline bool get_colliders_13() const { return ___colliders_13; }
	inline bool* get_address_of_colliders_13() { return &___colliders_13; }
	inline void set_colliders_13(bool value)
	{
		___colliders_13 = value;
	}

	inline static int32_t get_offset_of_parentedToEnd_14() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___parentedToEnd_14)); }
	inline GameObject_t1756533147 * get_parentedToEnd_14() const { return ___parentedToEnd_14; }
	inline GameObject_t1756533147 ** get_address_of_parentedToEnd_14() { return &___parentedToEnd_14; }
	inline void set_parentedToEnd_14(GameObject_t1756533147 * value)
	{
		___parentedToEnd_14 = value;
		Il2CppCodeGenWriteBarrier((&___parentedToEnd_14), value);
	}

	inline static int32_t get_offset_of_attachPoint_15() { return static_cast<int32_t>(offsetof(Fishingline_t2350076148, ___attachPoint_15)); }
	inline Transform_t3275118058 * get_attachPoint_15() const { return ___attachPoint_15; }
	inline Transform_t3275118058 ** get_address_of_attachPoint_15() { return &___attachPoint_15; }
	inline void set_attachPoint_15(Transform_t3275118058 * value)
	{
		___attachPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___attachPoint_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHINGLINE_T2350076148_H
#ifndef EXPOSURECONTROLLER_T4178516857_H
#define EXPOSURECONTROLLER_T4178516857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExposureController
struct  ExposureController_t4178516857  : public MonoBehaviour_t1158329972
{
public:
	// Wikitude.WikitudeCamera ExposureController::WikiCamera
	WikitudeCamera_t2517845841 * ___WikiCamera_2;

public:
	inline static int32_t get_offset_of_WikiCamera_2() { return static_cast<int32_t>(offsetof(ExposureController_t4178516857, ___WikiCamera_2)); }
	inline WikitudeCamera_t2517845841 * get_WikiCamera_2() const { return ___WikiCamera_2; }
	inline WikitudeCamera_t2517845841 ** get_address_of_WikiCamera_2() { return &___WikiCamera_2; }
	inline void set_WikiCamera_2(WikitudeCamera_t2517845841 * value)
	{
		___WikiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___WikiCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSURECONTROLLER_T4178516857_H
#ifndef BENCHMARK03_T1605376190_H
#define BENCHMARK03_T1605376190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1605376190  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t4239498691 * ___TheFont_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark03_t1605376190, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark03_t1605376190, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1605376190, ___TheFont_4)); }
	inline Font_t4239498691 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t4239498691 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t4239498691 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1605376190_H
#ifndef BENCHMARK04_T3171460131_H
#define BENCHMARK04_T3171460131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t3171460131  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_3;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_4;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_5;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3275118058 * ___m_Transform_6;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_3() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___MinPointSize_3)); }
	inline int32_t get_MinPointSize_3() const { return ___MinPointSize_3; }
	inline int32_t* get_address_of_MinPointSize_3() { return &___MinPointSize_3; }
	inline void set_MinPointSize_3(int32_t value)
	{
		___MinPointSize_3 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_4() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___MaxPointSize_4)); }
	inline int32_t get_MaxPointSize_4() const { return ___MaxPointSize_4; }
	inline int32_t* get_address_of_MaxPointSize_4() { return &___MaxPointSize_4; }
	inline void set_MaxPointSize_4(int32_t value)
	{
		___MaxPointSize_4 = value;
	}

	inline static int32_t get_offset_of_Steps_5() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___Steps_5)); }
	inline int32_t get_Steps_5() const { return ___Steps_5; }
	inline int32_t* get_address_of_Steps_5() { return &___Steps_5; }
	inline void set_Steps_5(int32_t value)
	{
		___Steps_5 = value;
	}

	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___m_Transform_6)); }
	inline Transform_t3275118058 * get_m_Transform_6() const { return ___m_Transform_6; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Transform_t3275118058 * value)
	{
		___m_Transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T3171460131_H
#ifndef CAMERACONTROLLER_T766129913_H
#define CAMERACONTROLLER_T766129913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t766129913  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3275118058 * ___cameraTransform_2;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3275118058 * ___dummyTarget_3;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3275118058 * ___CameraTarget_4;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_5;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_6;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_7;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_8;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_9;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_11;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_12;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_13;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_14;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_15;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_16;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_17;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_18;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t2243707580  ___currentVelocity_19;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t2243707580  ___desiredPosition_20;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_21;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_22;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t2243707580  ___moveVector_23;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_24;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___cameraTransform_2)); }
	inline Transform_t3275118058 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3275118058 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3275118058 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}

	inline static int32_t get_offset_of_dummyTarget_3() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___dummyTarget_3)); }
	inline Transform_t3275118058 * get_dummyTarget_3() const { return ___dummyTarget_3; }
	inline Transform_t3275118058 ** get_address_of_dummyTarget_3() { return &___dummyTarget_3; }
	inline void set_dummyTarget_3(Transform_t3275118058 * value)
	{
		___dummyTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_3), value);
	}

	inline static int32_t get_offset_of_CameraTarget_4() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___CameraTarget_4)); }
	inline Transform_t3275118058 * get_CameraTarget_4() const { return ___CameraTarget_4; }
	inline Transform_t3275118058 ** get_address_of_CameraTarget_4() { return &___CameraTarget_4; }
	inline void set_CameraTarget_4(Transform_t3275118058 * value)
	{
		___CameraTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_4), value);
	}

	inline static int32_t get_offset_of_FollowDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___FollowDistance_5)); }
	inline float get_FollowDistance_5() const { return ___FollowDistance_5; }
	inline float* get_address_of_FollowDistance_5() { return &___FollowDistance_5; }
	inline void set_FollowDistance_5(float value)
	{
		___FollowDistance_5 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_6() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MaxFollowDistance_6)); }
	inline float get_MaxFollowDistance_6() const { return ___MaxFollowDistance_6; }
	inline float* get_address_of_MaxFollowDistance_6() { return &___MaxFollowDistance_6; }
	inline void set_MaxFollowDistance_6(float value)
	{
		___MaxFollowDistance_6 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MinFollowDistance_7)); }
	inline float get_MinFollowDistance_7() const { return ___MinFollowDistance_7; }
	inline float* get_address_of_MinFollowDistance_7() { return &___MinFollowDistance_7; }
	inline void set_MinFollowDistance_7(float value)
	{
		___MinFollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_8() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___ElevationAngle_8)); }
	inline float get_ElevationAngle_8() const { return ___ElevationAngle_8; }
	inline float* get_address_of_ElevationAngle_8() { return &___ElevationAngle_8; }
	inline void set_ElevationAngle_8(float value)
	{
		___ElevationAngle_8 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_9() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MaxElevationAngle_9)); }
	inline float get_MaxElevationAngle_9() const { return ___MaxElevationAngle_9; }
	inline float* get_address_of_MaxElevationAngle_9() { return &___MaxElevationAngle_9; }
	inline void set_MaxElevationAngle_9(float value)
	{
		___MaxElevationAngle_9 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MinElevationAngle_10)); }
	inline float get_MinElevationAngle_10() const { return ___MinElevationAngle_10; }
	inline float* get_address_of_MinElevationAngle_10() { return &___MinElevationAngle_10; }
	inline void set_MinElevationAngle_10(float value)
	{
		___MinElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___OrbitalAngle_11)); }
	inline float get_OrbitalAngle_11() const { return ___OrbitalAngle_11; }
	inline float* get_address_of_OrbitalAngle_11() { return &___OrbitalAngle_11; }
	inline void set_OrbitalAngle_11(float value)
	{
		___OrbitalAngle_11 = value;
	}

	inline static int32_t get_offset_of_CameraMode_12() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___CameraMode_12)); }
	inline int32_t get_CameraMode_12() const { return ___CameraMode_12; }
	inline int32_t* get_address_of_CameraMode_12() { return &___CameraMode_12; }
	inline void set_CameraMode_12(int32_t value)
	{
		___CameraMode_12 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_13() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MovementSmoothing_13)); }
	inline bool get_MovementSmoothing_13() const { return ___MovementSmoothing_13; }
	inline bool* get_address_of_MovementSmoothing_13() { return &___MovementSmoothing_13; }
	inline void set_MovementSmoothing_13(bool value)
	{
		___MovementSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_14() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___RotationSmoothing_14)); }
	inline bool get_RotationSmoothing_14() const { return ___RotationSmoothing_14; }
	inline bool* get_address_of_RotationSmoothing_14() { return &___RotationSmoothing_14; }
	inline void set_RotationSmoothing_14(bool value)
	{
		___RotationSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___previousSmoothing_15)); }
	inline bool get_previousSmoothing_15() const { return ___previousSmoothing_15; }
	inline bool* get_address_of_previousSmoothing_15() { return &___previousSmoothing_15; }
	inline void set_previousSmoothing_15(bool value)
	{
		___previousSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_16() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MovementSmoothingValue_16)); }
	inline float get_MovementSmoothingValue_16() const { return ___MovementSmoothingValue_16; }
	inline float* get_address_of_MovementSmoothingValue_16() { return &___MovementSmoothingValue_16; }
	inline void set_MovementSmoothingValue_16(float value)
	{
		___MovementSmoothingValue_16 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_17() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___RotationSmoothingValue_17)); }
	inline float get_RotationSmoothingValue_17() const { return ___RotationSmoothingValue_17; }
	inline float* get_address_of_RotationSmoothingValue_17() { return &___RotationSmoothingValue_17; }
	inline void set_RotationSmoothingValue_17(float value)
	{
		___RotationSmoothingValue_17 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_18() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MoveSensitivity_18)); }
	inline float get_MoveSensitivity_18() const { return ___MoveSensitivity_18; }
	inline float* get_address_of_MoveSensitivity_18() { return &___MoveSensitivity_18; }
	inline void set_MoveSensitivity_18(float value)
	{
		___MoveSensitivity_18 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_19() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___currentVelocity_19)); }
	inline Vector3_t2243707580  get_currentVelocity_19() const { return ___currentVelocity_19; }
	inline Vector3_t2243707580 * get_address_of_currentVelocity_19() { return &___currentVelocity_19; }
	inline void set_currentVelocity_19(Vector3_t2243707580  value)
	{
		___currentVelocity_19 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_20() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___desiredPosition_20)); }
	inline Vector3_t2243707580  get_desiredPosition_20() const { return ___desiredPosition_20; }
	inline Vector3_t2243707580 * get_address_of_desiredPosition_20() { return &___desiredPosition_20; }
	inline void set_desiredPosition_20(Vector3_t2243707580  value)
	{
		___desiredPosition_20 = value;
	}

	inline static int32_t get_offset_of_mouseX_21() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___mouseX_21)); }
	inline float get_mouseX_21() const { return ___mouseX_21; }
	inline float* get_address_of_mouseX_21() { return &___mouseX_21; }
	inline void set_mouseX_21(float value)
	{
		___mouseX_21 = value;
	}

	inline static int32_t get_offset_of_mouseY_22() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___mouseY_22)); }
	inline float get_mouseY_22() const { return ___mouseY_22; }
	inline float* get_address_of_mouseY_22() { return &___mouseY_22; }
	inline void set_mouseY_22(float value)
	{
		___mouseY_22 = value;
	}

	inline static int32_t get_offset_of_moveVector_23() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___moveVector_23)); }
	inline Vector3_t2243707580  get_moveVector_23() const { return ___moveVector_23; }
	inline Vector3_t2243707580 * get_address_of_moveVector_23() { return &___moveVector_23; }
	inline void set_moveVector_23(Vector3_t2243707580  value)
	{
		___moveVector_23 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_24() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___mouseWheel_24)); }
	inline float get_mouseWheel_24() const { return ___mouseWheel_24; }
	inline float* get_address_of_mouseWheel_24() { return &___mouseWheel_24; }
	inline void set_mouseWheel_24(float value)
	{
		___mouseWheel_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T766129913_H
#ifndef MIRARETICLE_T2195475589_H
#define MIRARETICLE_T2195475589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraReticle
struct  MiraReticle_t2195475589  : public MonoBehaviour_t1158329972
{
public:
	// MiraReticlePointer MiraReticle::reticlepointer
	MiraReticlePointer_t592362690 * ___reticlepointer_2;
	// UnityEngine.Transform MiraReticle::cameraRig
	Transform_t3275118058 * ___cameraRig_4;
	// System.Boolean MiraReticle::onlyVisibleOnHover
	bool ___onlyVisibleOnHover_5;
	// System.Single MiraReticle::maxDistance
	float ___maxDistance_6;
	// System.Single MiraReticle::minDistance
	float ___minDistance_7;
	// System.Single MiraReticle::minScale
	float ___minScale_8;
	// System.Single MiraReticle::maxScale
	float ___maxScale_9;
	// System.Single MiraReticle::lastDistance
	float ___lastDistance_10;
	// System.Single MiraReticle::externalMultiplier
	float ___externalMultiplier_11;
	// UnityEngine.Color MiraReticle::reticleHoverColor
	Color_t2020392075  ___reticleHoverColor_12;
	// UnityEngine.Color MiraReticle::reticleIdleColor
	Color_t2020392075  ___reticleIdleColor_13;
	// UnityEngine.SpriteRenderer MiraReticle::reticleRenderer
	SpriteRenderer_t1209076198 * ___reticleRenderer_14;
	// UnityEngine.Vector3 MiraReticle::reticleOriginalScale
	Vector3_t2243707580  ___reticleOriginalScale_15;

public:
	inline static int32_t get_offset_of_reticlepointer_2() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticlepointer_2)); }
	inline MiraReticlePointer_t592362690 * get_reticlepointer_2() const { return ___reticlepointer_2; }
	inline MiraReticlePointer_t592362690 ** get_address_of_reticlepointer_2() { return &___reticlepointer_2; }
	inline void set_reticlepointer_2(MiraReticlePointer_t592362690 * value)
	{
		___reticlepointer_2 = value;
		Il2CppCodeGenWriteBarrier((&___reticlepointer_2), value);
	}

	inline static int32_t get_offset_of_cameraRig_4() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___cameraRig_4)); }
	inline Transform_t3275118058 * get_cameraRig_4() const { return ___cameraRig_4; }
	inline Transform_t3275118058 ** get_address_of_cameraRig_4() { return &___cameraRig_4; }
	inline void set_cameraRig_4(Transform_t3275118058 * value)
	{
		___cameraRig_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_4), value);
	}

	inline static int32_t get_offset_of_onlyVisibleOnHover_5() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___onlyVisibleOnHover_5)); }
	inline bool get_onlyVisibleOnHover_5() const { return ___onlyVisibleOnHover_5; }
	inline bool* get_address_of_onlyVisibleOnHover_5() { return &___onlyVisibleOnHover_5; }
	inline void set_onlyVisibleOnHover_5(bool value)
	{
		___onlyVisibleOnHover_5 = value;
	}

	inline static int32_t get_offset_of_maxDistance_6() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___maxDistance_6)); }
	inline float get_maxDistance_6() const { return ___maxDistance_6; }
	inline float* get_address_of_maxDistance_6() { return &___maxDistance_6; }
	inline void set_maxDistance_6(float value)
	{
		___maxDistance_6 = value;
	}

	inline static int32_t get_offset_of_minDistance_7() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___minDistance_7)); }
	inline float get_minDistance_7() const { return ___minDistance_7; }
	inline float* get_address_of_minDistance_7() { return &___minDistance_7; }
	inline void set_minDistance_7(float value)
	{
		___minDistance_7 = value;
	}

	inline static int32_t get_offset_of_minScale_8() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___minScale_8)); }
	inline float get_minScale_8() const { return ___minScale_8; }
	inline float* get_address_of_minScale_8() { return &___minScale_8; }
	inline void set_minScale_8(float value)
	{
		___minScale_8 = value;
	}

	inline static int32_t get_offset_of_maxScale_9() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___maxScale_9)); }
	inline float get_maxScale_9() const { return ___maxScale_9; }
	inline float* get_address_of_maxScale_9() { return &___maxScale_9; }
	inline void set_maxScale_9(float value)
	{
		___maxScale_9 = value;
	}

	inline static int32_t get_offset_of_lastDistance_10() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___lastDistance_10)); }
	inline float get_lastDistance_10() const { return ___lastDistance_10; }
	inline float* get_address_of_lastDistance_10() { return &___lastDistance_10; }
	inline void set_lastDistance_10(float value)
	{
		___lastDistance_10 = value;
	}

	inline static int32_t get_offset_of_externalMultiplier_11() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___externalMultiplier_11)); }
	inline float get_externalMultiplier_11() const { return ___externalMultiplier_11; }
	inline float* get_address_of_externalMultiplier_11() { return &___externalMultiplier_11; }
	inline void set_externalMultiplier_11(float value)
	{
		___externalMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_reticleHoverColor_12() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleHoverColor_12)); }
	inline Color_t2020392075  get_reticleHoverColor_12() const { return ___reticleHoverColor_12; }
	inline Color_t2020392075 * get_address_of_reticleHoverColor_12() { return &___reticleHoverColor_12; }
	inline void set_reticleHoverColor_12(Color_t2020392075  value)
	{
		___reticleHoverColor_12 = value;
	}

	inline static int32_t get_offset_of_reticleIdleColor_13() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleIdleColor_13)); }
	inline Color_t2020392075  get_reticleIdleColor_13() const { return ___reticleIdleColor_13; }
	inline Color_t2020392075 * get_address_of_reticleIdleColor_13() { return &___reticleIdleColor_13; }
	inline void set_reticleIdleColor_13(Color_t2020392075  value)
	{
		___reticleIdleColor_13 = value;
	}

	inline static int32_t get_offset_of_reticleRenderer_14() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleRenderer_14)); }
	inline SpriteRenderer_t1209076198 * get_reticleRenderer_14() const { return ___reticleRenderer_14; }
	inline SpriteRenderer_t1209076198 ** get_address_of_reticleRenderer_14() { return &___reticleRenderer_14; }
	inline void set_reticleRenderer_14(SpriteRenderer_t1209076198 * value)
	{
		___reticleRenderer_14 = value;
		Il2CppCodeGenWriteBarrier((&___reticleRenderer_14), value);
	}

	inline static int32_t get_offset_of_reticleOriginalScale_15() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleOriginalScale_15)); }
	inline Vector3_t2243707580  get_reticleOriginalScale_15() const { return ___reticleOriginalScale_15; }
	inline Vector3_t2243707580 * get_address_of_reticleOriginalScale_15() { return &___reticleOriginalScale_15; }
	inline void set_reticleOriginalScale_15(Vector3_t2243707580  value)
	{
		___reticleOriginalScale_15 = value;
	}
};

struct MiraReticle_t2195475589_StaticFields
{
public:
	// MiraReticle MiraReticle::_instance
	MiraReticle_t2195475589 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589_StaticFields, ____instance_3)); }
	inline MiraReticle_t2195475589 * get__instance_3() const { return ____instance_3; }
	inline MiraReticle_t2195475589 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(MiraReticle_t2195475589 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRARETICLE_T2195475589_H
#ifndef CHATCONTROLLER_T2669781690_H
#define CHATCONTROLLER_T2669781690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t2669781690  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1778301588 * ___TMP_ChatInput_2;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t1920000777 * ___TMP_ChatOutput_3;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t3248359358 * ___ChatScrollbar_4;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_2() { return static_cast<int32_t>(offsetof(ChatController_t2669781690, ___TMP_ChatInput_2)); }
	inline TMP_InputField_t1778301588 * get_TMP_ChatInput_2() const { return ___TMP_ChatInput_2; }
	inline TMP_InputField_t1778301588 ** get_address_of_TMP_ChatInput_2() { return &___TMP_ChatInput_2; }
	inline void set_TMP_ChatInput_2(TMP_InputField_t1778301588 * value)
	{
		___TMP_ChatInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_2), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_3() { return static_cast<int32_t>(offsetof(ChatController_t2669781690, ___TMP_ChatOutput_3)); }
	inline TMP_Text_t1920000777 * get_TMP_ChatOutput_3() const { return ___TMP_ChatOutput_3; }
	inline TMP_Text_t1920000777 ** get_address_of_TMP_ChatOutput_3() { return &___TMP_ChatOutput_3; }
	inline void set_TMP_ChatOutput_3(TMP_Text_t1920000777 * value)
	{
		___TMP_ChatOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_3), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_4() { return static_cast<int32_t>(offsetof(ChatController_t2669781690, ___ChatScrollbar_4)); }
	inline Scrollbar_t3248359358 * get_ChatScrollbar_4() const { return ___ChatScrollbar_4; }
	inline Scrollbar_t3248359358 ** get_address_of_ChatScrollbar_4() { return &___ChatScrollbar_4; }
	inline void set_ChatScrollbar_4(Scrollbar_t3248359358 * value)
	{
		___ChatScrollbar_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T2669781690_H
#ifndef ENVMAPANIMATOR_T1635389402_H
#define ENVMAPANIMATOR_T1635389402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1635389402  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t2243707580  ___RotationSpeeds_2;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t1920000777 * ___m_textMeshPro_3;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t193706927 * ___m_material_4;

public:
	inline static int32_t get_offset_of_RotationSpeeds_2() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1635389402, ___RotationSpeeds_2)); }
	inline Vector3_t2243707580  get_RotationSpeeds_2() const { return ___RotationSpeeds_2; }
	inline Vector3_t2243707580 * get_address_of_RotationSpeeds_2() { return &___RotationSpeeds_2; }
	inline void set_RotationSpeeds_2(Vector3_t2243707580  value)
	{
		___RotationSpeeds_2 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_3() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1635389402, ___m_textMeshPro_3)); }
	inline TMP_Text_t1920000777 * get_m_textMeshPro_3() const { return ___m_textMeshPro_3; }
	inline TMP_Text_t1920000777 ** get_address_of_m_textMeshPro_3() { return &___m_textMeshPro_3; }
	inline void set_m_textMeshPro_3(TMP_Text_t1920000777 * value)
	{
		___m_textMeshPro_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1635389402, ___m_material_4)); }
	inline Material_t193706927 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t193706927 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t193706927 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1635389402_H
#ifndef MIRAPRERENDER_T2810281508_H
#define MIRAPRERENDER_T2810281508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPreRender
struct  MiraPreRender_t2810281508  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera MiraPreRender::<cam>k__BackingField
	Camera_t189460977 * ___U3CcamU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MiraPreRender_t2810281508, ___U3CcamU3Ek__BackingField_2)); }
	inline Camera_t189460977 * get_U3CcamU3Ek__BackingField_2() const { return ___U3CcamU3Ek__BackingField_2; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3Ek__BackingField_2() { return &___U3CcamU3Ek__BackingField_2; }
	inline void set_U3CcamU3Ek__BackingField_2(Camera_t189460977 * value)
	{
		___U3CcamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcamU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPRERENDER_T2810281508_H
#ifndef OBJECTSPIN_T3410945885_H
#define OBJECTSPIN_T3410945885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t3410945885  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_2;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_3;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3275118058 * ___m_transform_4;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_5;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t2243707580  ___m_prevPOS_6;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t2243707580  ___m_initial_Rotation_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t2243707580  ___m_initial_Position_8;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t874517518  ___m_lightColor_9;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_10;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_11;

public:
	inline static int32_t get_offset_of_SpinSpeed_2() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___SpinSpeed_2)); }
	inline float get_SpinSpeed_2() const { return ___SpinSpeed_2; }
	inline float* get_address_of_SpinSpeed_2() { return &___SpinSpeed_2; }
	inline void set_SpinSpeed_2(float value)
	{
		___SpinSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RotationRange_3() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___RotationRange_3)); }
	inline int32_t get_RotationRange_3() const { return ___RotationRange_3; }
	inline int32_t* get_address_of_RotationRange_3() { return &___RotationRange_3; }
	inline void set_RotationRange_3(int32_t value)
	{
		___RotationRange_3 = value;
	}

	inline static int32_t get_offset_of_m_transform_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_transform_4)); }
	inline Transform_t3275118058 * get_m_transform_4() const { return ___m_transform_4; }
	inline Transform_t3275118058 ** get_address_of_m_transform_4() { return &___m_transform_4; }
	inline void set_m_transform_4(Transform_t3275118058 * value)
	{
		___m_transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_4), value);
	}

	inline static int32_t get_offset_of_m_time_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_time_5)); }
	inline float get_m_time_5() const { return ___m_time_5; }
	inline float* get_address_of_m_time_5() { return &___m_time_5; }
	inline void set_m_time_5(float value)
	{
		___m_time_5 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_prevPOS_6)); }
	inline Vector3_t2243707580  get_m_prevPOS_6() const { return ___m_prevPOS_6; }
	inline Vector3_t2243707580 * get_address_of_m_prevPOS_6() { return &___m_prevPOS_6; }
	inline void set_m_prevPOS_6(Vector3_t2243707580  value)
	{
		___m_prevPOS_6 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_initial_Rotation_7)); }
	inline Vector3_t2243707580  get_m_initial_Rotation_7() const { return ___m_initial_Rotation_7; }
	inline Vector3_t2243707580 * get_address_of_m_initial_Rotation_7() { return &___m_initial_Rotation_7; }
	inline void set_m_initial_Rotation_7(Vector3_t2243707580  value)
	{
		___m_initial_Rotation_7 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_initial_Position_8)); }
	inline Vector3_t2243707580  get_m_initial_Position_8() const { return ___m_initial_Position_8; }
	inline Vector3_t2243707580 * get_address_of_m_initial_Position_8() { return &___m_initial_Position_8; }
	inline void set_m_initial_Position_8(Vector3_t2243707580  value)
	{
		___m_initial_Position_8 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_lightColor_9)); }
	inline Color32_t874517518  get_m_lightColor_9() const { return ___m_lightColor_9; }
	inline Color32_t874517518 * get_address_of_m_lightColor_9() { return &___m_lightColor_9; }
	inline void set_m_lightColor_9(Color32_t874517518  value)
	{
		___m_lightColor_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___frames_10)); }
	inline int32_t get_frames_10() const { return ___frames_10; }
	inline int32_t* get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(int32_t value)
	{
		___frames_10 = value;
	}

	inline static int32_t get_offset_of_Motion_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___Motion_11)); }
	inline int32_t get_Motion_11() const { return ___Motion_11; }
	inline int32_t* get_address_of_Motion_11() { return &___Motion_11; }
	inline void set_Motion_11(int32_t value)
	{
		___Motion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T3410945885_H
#ifndef BENCHMARK02_T39292249_H
#define BENCHMARK02_T39292249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t39292249  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t6181308 * ___floatingText_Script_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark02_t39292249, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark02_t39292249, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_4() { return static_cast<int32_t>(offsetof(Benchmark02_t39292249, ___floatingText_Script_4)); }
	inline TextMeshProFloatingText_t6181308 * get_floatingText_Script_4() const { return ___floatingText_Script_4; }
	inline TextMeshProFloatingText_t6181308 ** get_address_of_floatingText_Script_4() { return &___floatingText_Script_4; }
	inline void set_floatingText_Script_4(TextMeshProFloatingText_t6181308 * value)
	{
		___floatingText_Script_4 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T39292249_H
#ifndef IMAGETRACKINGCONTROLLER_T3751215764_H
#define IMAGETRACKINGCONTROLLER_T3751215764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageTrackingController
struct  ImageTrackingController_t3751215764  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETRACKINGCONTROLLER_T3751215764_H
#ifndef MIRAEXAMPLEINTERACTIONSCRIPT_T1495489050_H
#define MIRAEXAMPLEINTERACTIONSCRIPT_T1495489050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraExampleInteractionScript
struct  MiraExampleInteractionScript_t1495489050  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MiraExampleInteractionScript::textBeneathPlanet
	Text_t356221433 * ___textBeneathPlanet_2;
	// System.Single MiraExampleInteractionScript::timeSinceLastInteraction
	float ___timeSinceLastInteraction_3;
	// UnityEngine.Vector3 MiraExampleInteractionScript::startingLocation
	Vector3_t2243707580  ___startingLocation_4;
	// System.Boolean MiraExampleInteractionScript::isPlanetFeelingSatisfied
	bool ___isPlanetFeelingSatisfied_5;
	// System.Boolean MiraExampleInteractionScript::isUserPointingAtPlanet
	bool ___isUserPointingAtPlanet_6;
	// AxisSpin MiraExampleInteractionScript::spin
	AxisSpin_t3838354289 * ___spin_7;

public:
	inline static int32_t get_offset_of_textBeneathPlanet_2() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t1495489050, ___textBeneathPlanet_2)); }
	inline Text_t356221433 * get_textBeneathPlanet_2() const { return ___textBeneathPlanet_2; }
	inline Text_t356221433 ** get_address_of_textBeneathPlanet_2() { return &___textBeneathPlanet_2; }
	inline void set_textBeneathPlanet_2(Text_t356221433 * value)
	{
		___textBeneathPlanet_2 = value;
		Il2CppCodeGenWriteBarrier((&___textBeneathPlanet_2), value);
	}

	inline static int32_t get_offset_of_timeSinceLastInteraction_3() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t1495489050, ___timeSinceLastInteraction_3)); }
	inline float get_timeSinceLastInteraction_3() const { return ___timeSinceLastInteraction_3; }
	inline float* get_address_of_timeSinceLastInteraction_3() { return &___timeSinceLastInteraction_3; }
	inline void set_timeSinceLastInteraction_3(float value)
	{
		___timeSinceLastInteraction_3 = value;
	}

	inline static int32_t get_offset_of_startingLocation_4() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t1495489050, ___startingLocation_4)); }
	inline Vector3_t2243707580  get_startingLocation_4() const { return ___startingLocation_4; }
	inline Vector3_t2243707580 * get_address_of_startingLocation_4() { return &___startingLocation_4; }
	inline void set_startingLocation_4(Vector3_t2243707580  value)
	{
		___startingLocation_4 = value;
	}

	inline static int32_t get_offset_of_isPlanetFeelingSatisfied_5() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t1495489050, ___isPlanetFeelingSatisfied_5)); }
	inline bool get_isPlanetFeelingSatisfied_5() const { return ___isPlanetFeelingSatisfied_5; }
	inline bool* get_address_of_isPlanetFeelingSatisfied_5() { return &___isPlanetFeelingSatisfied_5; }
	inline void set_isPlanetFeelingSatisfied_5(bool value)
	{
		___isPlanetFeelingSatisfied_5 = value;
	}

	inline static int32_t get_offset_of_isUserPointingAtPlanet_6() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t1495489050, ___isUserPointingAtPlanet_6)); }
	inline bool get_isUserPointingAtPlanet_6() const { return ___isUserPointingAtPlanet_6; }
	inline bool* get_address_of_isUserPointingAtPlanet_6() { return &___isUserPointingAtPlanet_6; }
	inline void set_isUserPointingAtPlanet_6(bool value)
	{
		___isUserPointingAtPlanet_6 = value;
	}

	inline static int32_t get_offset_of_spin_7() { return static_cast<int32_t>(offsetof(MiraExampleInteractionScript_t1495489050, ___spin_7)); }
	inline AxisSpin_t3838354289 * get_spin_7() const { return ___spin_7; }
	inline AxisSpin_t3838354289 ** get_address_of_spin_7() { return &___spin_7; }
	inline void set_spin_7(AxisSpin_t3838354289 * value)
	{
		___spin_7 = value;
		Il2CppCodeGenWriteBarrier((&___spin_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAEXAMPLEINTERACTIONSCRIPT_T1495489050_H
#ifndef BENCHMARK01_T2768175604_H
#define BENCHMARK01_T2768175604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t2768175604  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t2530419979 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t4239498691 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t4263764796 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1641806576 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t193706927 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t193706927 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___TMProFont_3)); }
	inline TMP_FontAsset_t2530419979 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t2530419979 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___TextMeshFont_4)); }
	inline Font_t4239498691 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t4239498691 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t4239498691 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textContainer_6)); }
	inline TextContainer_t4263764796 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t4263764796 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t4263764796 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textMesh_7)); }
	inline TextMesh_t1641806576 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1641806576 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1641806576 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_material01_10)); }
	inline Material_t193706927 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t193706927 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t193706927 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_material02_11)); }
	inline Material_t193706927 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t193706927 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t193706927 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T2768175604_H
#ifndef MIRAVIEWER_T1267122109_H
#define MIRAVIEWER_T1267122109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraViewer
struct  MiraViewer_t1267122109  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.MiraViewer::stereoCamFov
	float ___stereoCamFov_2;
	// UnityEngine.GameObject Mira.MiraViewer::cameraRig
	GameObject_t1756533147 * ___cameraRig_4;
	// UnityEngine.GameObject Mira.MiraViewer::Left_Eye
	GameObject_t1756533147 * ___Left_Eye_5;
	// UnityEngine.GameObject Mira.MiraViewer::Right_Eye
	GameObject_t1756533147 * ___Right_Eye_6;

public:
	inline static int32_t get_offset_of_stereoCamFov_2() { return static_cast<int32_t>(offsetof(MiraViewer_t1267122109, ___stereoCamFov_2)); }
	inline float get_stereoCamFov_2() const { return ___stereoCamFov_2; }
	inline float* get_address_of_stereoCamFov_2() { return &___stereoCamFov_2; }
	inline void set_stereoCamFov_2(float value)
	{
		___stereoCamFov_2 = value;
	}

	inline static int32_t get_offset_of_cameraRig_4() { return static_cast<int32_t>(offsetof(MiraViewer_t1267122109, ___cameraRig_4)); }
	inline GameObject_t1756533147 * get_cameraRig_4() const { return ___cameraRig_4; }
	inline GameObject_t1756533147 ** get_address_of_cameraRig_4() { return &___cameraRig_4; }
	inline void set_cameraRig_4(GameObject_t1756533147 * value)
	{
		___cameraRig_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_4), value);
	}

	inline static int32_t get_offset_of_Left_Eye_5() { return static_cast<int32_t>(offsetof(MiraViewer_t1267122109, ___Left_Eye_5)); }
	inline GameObject_t1756533147 * get_Left_Eye_5() const { return ___Left_Eye_5; }
	inline GameObject_t1756533147 ** get_address_of_Left_Eye_5() { return &___Left_Eye_5; }
	inline void set_Left_Eye_5(GameObject_t1756533147 * value)
	{
		___Left_Eye_5 = value;
		Il2CppCodeGenWriteBarrier((&___Left_Eye_5), value);
	}

	inline static int32_t get_offset_of_Right_Eye_6() { return static_cast<int32_t>(offsetof(MiraViewer_t1267122109, ___Right_Eye_6)); }
	inline GameObject_t1756533147 * get_Right_Eye_6() const { return ___Right_Eye_6; }
	inline GameObject_t1756533147 ** get_address_of_Right_Eye_6() { return &___Right_Eye_6; }
	inline void set_Right_Eye_6(GameObject_t1756533147 * value)
	{
		___Right_Eye_6 = value;
		Il2CppCodeGenWriteBarrier((&___Right_Eye_6), value);
	}
};

struct MiraViewer_t1267122109_StaticFields
{
public:
	// UnityEngine.GameObject Mira.MiraViewer::viewer
	GameObject_t1756533147 * ___viewer_3;
	// Mira.MiraViewer Mira.MiraViewer::Instance
	MiraViewer_t1267122109 * ___Instance_7;

public:
	inline static int32_t get_offset_of_viewer_3() { return static_cast<int32_t>(offsetof(MiraViewer_t1267122109_StaticFields, ___viewer_3)); }
	inline GameObject_t1756533147 * get_viewer_3() const { return ___viewer_3; }
	inline GameObject_t1756533147 ** get_address_of_viewer_3() { return &___viewer_3; }
	inline void set_viewer_3(GameObject_t1756533147 * value)
	{
		___viewer_3 = value;
		Il2CppCodeGenWriteBarrier((&___viewer_3), value);
	}

	inline static int32_t get_offset_of_Instance_7() { return static_cast<int32_t>(offsetof(MiraViewer_t1267122109_StaticFields, ___Instance_7)); }
	inline MiraViewer_t1267122109 * get_Instance_7() const { return ___Instance_7; }
	inline MiraViewer_t1267122109 ** get_address_of_Instance_7() { return &___Instance_7; }
	inline void set_Instance_7(MiraViewer_t1267122109 * value)
	{
		___Instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAVIEWER_T1267122109_H
#ifndef BENCHMARK01_UGUI_T3449578277_H
#define BENCHMARK01_UGUI_T3449578277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3449578277  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t209405766 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t2530419979 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t4239498691 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t934157183 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t356221433 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t193706927 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t193706927 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___canvas_3)); }
	inline Canvas_t209405766 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t209405766 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t209405766 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_3), value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___TMProFont_4)); }
	inline TMP_FontAsset_t2530419979 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t2530419979 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_4), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___TextMeshFont_5)); }
	inline Font_t4239498691 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t4239498691 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t4239498691 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t934157183 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t934157183 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_textMesh_7)); }
	inline Text_t356221433 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t356221433 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t356221433 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_material01_10)); }
	inline Material_t193706927 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t193706927 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t193706927 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_material02_11)); }
	inline Material_t193706927 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t193706927 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t193706927 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3449578277_H
#ifndef MIRAWIKITUDEMANAGER_T2317471266_H
#define MIRAWIKITUDEMANAGER_T2317471266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraWikitudeManager
struct  MiraWikitudeManager_t2317471266  : public TransformOverride_t3077475158
{
public:
	// Wikitude.WikitudeCamera MiraWikitudeManager::ArCam
	WikitudeCamera_t2517845841 * ___ArCam_2;
	// UnityEngine.GameObject MiraWikitudeManager::headOrientation3DOF
	GameObject_t1756533147 * ___headOrientation3DOF_3;
	// Wikitude.ImageTrackable MiraWikitudeManager::imageTracker
	ImageTrackable_t3105654606 * ___imageTracker_4;
	// System.Single MiraWikitudeManager::scaleMultiplier
	float ___scaleMultiplier_6;
	// UnityEngine.Quaternion MiraWikitudeManager::rotationalOffset
	Quaternion_t4030073918  ___rotationalOffset_7;
	// UnityEngine.Vector3 MiraWikitudeManager::positionalOffset
	Vector3_t2243707580  ___positionalOffset_8;
	// Mira.RotationalTrackingManager MiraWikitudeManager::rotationalTracking
	RotationalTrackingManager_t1512110775 * ___rotationalTracking_9;
	// System.Boolean MiraWikitudeManager::flag
	bool ___flag_10;

public:
	inline static int32_t get_offset_of_ArCam_2() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___ArCam_2)); }
	inline WikitudeCamera_t2517845841 * get_ArCam_2() const { return ___ArCam_2; }
	inline WikitudeCamera_t2517845841 ** get_address_of_ArCam_2() { return &___ArCam_2; }
	inline void set_ArCam_2(WikitudeCamera_t2517845841 * value)
	{
		___ArCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___ArCam_2), value);
	}

	inline static int32_t get_offset_of_headOrientation3DOF_3() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___headOrientation3DOF_3)); }
	inline GameObject_t1756533147 * get_headOrientation3DOF_3() const { return ___headOrientation3DOF_3; }
	inline GameObject_t1756533147 ** get_address_of_headOrientation3DOF_3() { return &___headOrientation3DOF_3; }
	inline void set_headOrientation3DOF_3(GameObject_t1756533147 * value)
	{
		___headOrientation3DOF_3 = value;
		Il2CppCodeGenWriteBarrier((&___headOrientation3DOF_3), value);
	}

	inline static int32_t get_offset_of_imageTracker_4() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___imageTracker_4)); }
	inline ImageTrackable_t3105654606 * get_imageTracker_4() const { return ___imageTracker_4; }
	inline ImageTrackable_t3105654606 ** get_address_of_imageTracker_4() { return &___imageTracker_4; }
	inline void set_imageTracker_4(ImageTrackable_t3105654606 * value)
	{
		___imageTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageTracker_4), value);
	}

	inline static int32_t get_offset_of_scaleMultiplier_6() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___scaleMultiplier_6)); }
	inline float get_scaleMultiplier_6() const { return ___scaleMultiplier_6; }
	inline float* get_address_of_scaleMultiplier_6() { return &___scaleMultiplier_6; }
	inline void set_scaleMultiplier_6(float value)
	{
		___scaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_rotationalOffset_7() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___rotationalOffset_7)); }
	inline Quaternion_t4030073918  get_rotationalOffset_7() const { return ___rotationalOffset_7; }
	inline Quaternion_t4030073918 * get_address_of_rotationalOffset_7() { return &___rotationalOffset_7; }
	inline void set_rotationalOffset_7(Quaternion_t4030073918  value)
	{
		___rotationalOffset_7 = value;
	}

	inline static int32_t get_offset_of_positionalOffset_8() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___positionalOffset_8)); }
	inline Vector3_t2243707580  get_positionalOffset_8() const { return ___positionalOffset_8; }
	inline Vector3_t2243707580 * get_address_of_positionalOffset_8() { return &___positionalOffset_8; }
	inline void set_positionalOffset_8(Vector3_t2243707580  value)
	{
		___positionalOffset_8 = value;
	}

	inline static int32_t get_offset_of_rotationalTracking_9() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___rotationalTracking_9)); }
	inline RotationalTrackingManager_t1512110775 * get_rotationalTracking_9() const { return ___rotationalTracking_9; }
	inline RotationalTrackingManager_t1512110775 ** get_address_of_rotationalTracking_9() { return &___rotationalTracking_9; }
	inline void set_rotationalTracking_9(RotationalTrackingManager_t1512110775 * value)
	{
		___rotationalTracking_9 = value;
		Il2CppCodeGenWriteBarrier((&___rotationalTracking_9), value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266, ___flag_10)); }
	inline bool get_flag_10() const { return ___flag_10; }
	inline bool* get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(bool value)
	{
		___flag_10 = value;
	}
};

struct MiraWikitudeManager_t2317471266_StaticFields
{
public:
	// MiraWikitudeManager MiraWikitudeManager::instance
	MiraWikitudeManager_t2317471266 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(MiraWikitudeManager_t2317471266_StaticFields, ___instance_5)); }
	inline MiraWikitudeManager_t2317471266 * get_instance_5() const { return ___instance_5; }
	inline MiraWikitudeManager_t2317471266 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(MiraWikitudeManager_t2317471266 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAWIKITUDEMANAGER_T2317471266_H
#ifndef BASEINPUTMODULE_T1295781545_H
#define BASEINPUTMODULE_T1295781545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t1295781545  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t3685274804 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t1524870173 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t2681005625 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t621514313 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t621514313 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_RaycastResultCache_2)); }
	inline List_1_t3685274804 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t3685274804 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t3685274804 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_AxisEventData_3)); }
	inline AxisEventData_t1524870173 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t1524870173 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t1524870173 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_EventSystem_4)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_BaseEventData_5)); }
	inline BaseEventData_t2681005625 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t2681005625 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t2681005625 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_InputOverride_6)); }
	inline BaseInput_t621514313 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t621514313 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t621514313 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_DefaultInput_7)); }
	inline BaseInput_t621514313 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t621514313 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t621514313 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T1295781545_H
#ifndef BASERAYCASTER_T2336171397_H
#define BASERAYCASTER_T2336171397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t2336171397  : public UIBehaviour_t3960014691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T2336171397_H
#ifndef SCALEOVERRIDE_T3404595986_H
#define SCALEOVERRIDE_T3404595986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleOverride
struct  ScaleOverride_t3404595986  : public TransformOverride_t3077475158
{
public:
	// Wikitude.WikitudeCamera ScaleOverride::wikiCamera
	WikitudeCamera_t2517845841 * ___wikiCamera_2;

public:
	inline static int32_t get_offset_of_wikiCamera_2() { return static_cast<int32_t>(offsetof(ScaleOverride_t3404595986, ___wikiCamera_2)); }
	inline WikitudeCamera_t2517845841 * get_wikiCamera_2() const { return ___wikiCamera_2; }
	inline WikitudeCamera_t2517845841 ** get_address_of_wikiCamera_2() { return &___wikiCamera_2; }
	inline void set_wikiCamera_2(WikitudeCamera_t2517845841 * value)
	{
		___wikiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___wikiCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEOVERRIDE_T3404595986_H
#ifndef POINTERINPUTMODULE_T1441575871_H
#define POINTERINPUTMODULE_T1441575871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t1441575871  : public BaseInputModule_t1295781545
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t607610358 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t3572864619 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t1441575871, ___m_PointerData_12)); }
	inline Dictionary_2_t607610358 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t607610358 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t607610358 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t1441575871, ___m_MouseState_13)); }
	inline MouseState_t3572864619 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t3572864619 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t3572864619 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T1441575871_H
#ifndef MIRABASERAYCASTER_T1612955938_H
#define MIRABASERAYCASTER_T1612955938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster
struct  MiraBaseRaycaster_t1612955938  : public BaseRaycaster_t2336171397
{
public:
	// MiraBaseRaycaster/RaycastStyle MiraBaseRaycaster::raycastStyle
	int32_t ___raycastStyle_2;
	// UnityEngine.Ray MiraBaseRaycaster::lastray
	Ray_t2469606224  ___lastray_3;

public:
	inline static int32_t get_offset_of_raycastStyle_2() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t1612955938, ___raycastStyle_2)); }
	inline int32_t get_raycastStyle_2() const { return ___raycastStyle_2; }
	inline int32_t* get_address_of_raycastStyle_2() { return &___raycastStyle_2; }
	inline void set_raycastStyle_2(int32_t value)
	{
		___raycastStyle_2 = value;
	}

	inline static int32_t get_offset_of_lastray_3() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t1612955938, ___lastray_3)); }
	inline Ray_t2469606224  get_lastray_3() const { return ___lastray_3; }
	inline Ray_t2469606224 * get_address_of_lastray_3() { return &___lastray_3; }
	inline void set_lastray_3(Ray_t2469606224  value)
	{
		___lastray_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASERAYCASTER_T1612955938_H
#ifndef MIRAPHYSICSRAYCAST_T1727560409_H
#define MIRAPHYSICSRAYCAST_T1727560409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsRaycast
struct  MiraPhysicsRaycast_t1727560409  : public MiraBaseRaycaster_t1612955938
{
public:
	// UnityEngine.Camera MiraPhysicsRaycast::_EventCamera
	Camera_t189460977 * ____EventCamera_5;
	// UnityEngine.LayerMask MiraPhysicsRaycast::_EventMask
	LayerMask_t3188175821  ____EventMask_6;

public:
	inline static int32_t get_offset_of__EventCamera_5() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1727560409, ____EventCamera_5)); }
	inline Camera_t189460977 * get__EventCamera_5() const { return ____EventCamera_5; }
	inline Camera_t189460977 ** get_address_of__EventCamera_5() { return &____EventCamera_5; }
	inline void set__EventCamera_5(Camera_t189460977 * value)
	{
		____EventCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&____EventCamera_5), value);
	}

	inline static int32_t get_offset_of__EventMask_6() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1727560409, ____EventMask_6)); }
	inline LayerMask_t3188175821  get__EventMask_6() const { return ____EventMask_6; }
	inline LayerMask_t3188175821 * get_address_of__EventMask_6() { return &____EventMask_6; }
	inline void set__EventMask_6(LayerMask_t3188175821  value)
	{
		____EventMask_6 = value;
	}
};

struct MiraPhysicsRaycast_t1727560409_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.RaycastHit> MiraPhysicsRaycast::<>f__am$cache0
	Comparison_1_t1348919171 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1727560409_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Comparison_1_t1348919171 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Comparison_1_t1348919171 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Comparison_1_t1348919171 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSRAYCAST_T1727560409_H
#ifndef MIRAGRAPHICRAYCAST_T590279048_H
#define MIRAGRAPHICRAYCAST_T590279048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraGraphicRaycast
struct  MiraGraphicRaycast_t590279048  : public MiraBaseRaycaster_t1612955938
{
public:
	// UnityEngine.LayerMask MiraGraphicRaycast::blockingMask
	LayerMask_t3188175821  ___blockingMask_5;
	// System.Boolean MiraGraphicRaycast::ignoreReversedGraphics
	bool ___ignoreReversedGraphics_6;
	// UnityEngine.UI.GraphicRaycaster/BlockingObjects MiraGraphicRaycast::blockingObjects
	int32_t ___blockingObjects_7;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::raycastResults
	List_1_t1795346708 * ___raycastResults_8;
	// UnityEngine.Canvas MiraGraphicRaycast::_canvas
	Canvas_t209405766 * ____canvas_9;

public:
	inline static int32_t get_offset_of_blockingMask_5() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___blockingMask_5)); }
	inline LayerMask_t3188175821  get_blockingMask_5() const { return ___blockingMask_5; }
	inline LayerMask_t3188175821 * get_address_of_blockingMask_5() { return &___blockingMask_5; }
	inline void set_blockingMask_5(LayerMask_t3188175821  value)
	{
		___blockingMask_5 = value;
	}

	inline static int32_t get_offset_of_ignoreReversedGraphics_6() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___ignoreReversedGraphics_6)); }
	inline bool get_ignoreReversedGraphics_6() const { return ___ignoreReversedGraphics_6; }
	inline bool* get_address_of_ignoreReversedGraphics_6() { return &___ignoreReversedGraphics_6; }
	inline void set_ignoreReversedGraphics_6(bool value)
	{
		___ignoreReversedGraphics_6 = value;
	}

	inline static int32_t get_offset_of_blockingObjects_7() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___blockingObjects_7)); }
	inline int32_t get_blockingObjects_7() const { return ___blockingObjects_7; }
	inline int32_t* get_address_of_blockingObjects_7() { return &___blockingObjects_7; }
	inline void set_blockingObjects_7(int32_t value)
	{
		___blockingObjects_7 = value;
	}

	inline static int32_t get_offset_of_raycastResults_8() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___raycastResults_8)); }
	inline List_1_t1795346708 * get_raycastResults_8() const { return ___raycastResults_8; }
	inline List_1_t1795346708 ** get_address_of_raycastResults_8() { return &___raycastResults_8; }
	inline void set_raycastResults_8(List_1_t1795346708 * value)
	{
		___raycastResults_8 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_8), value);
	}

	inline static int32_t get_offset_of__canvas_9() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ____canvas_9)); }
	inline Canvas_t209405766 * get__canvas_9() const { return ____canvas_9; }
	inline Canvas_t209405766 ** get_address_of__canvas_9() { return &____canvas_9; }
	inline void set__canvas_9(Canvas_t209405766 * value)
	{
		____canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_9), value);
	}
};

struct MiraGraphicRaycast_t590279048_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::sortedGraphics
	List_1_t1795346708 * ___sortedGraphics_10;
	// System.Comparison`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::<>f__am$cache0
	Comparison_1_t3687964427 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_sortedGraphics_10() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048_StaticFields, ___sortedGraphics_10)); }
	inline List_1_t1795346708 * get_sortedGraphics_10() const { return ___sortedGraphics_10; }
	inline List_1_t1795346708 ** get_address_of_sortedGraphics_10() { return &___sortedGraphics_10; }
	inline void set_sortedGraphics_10(List_1_t1795346708 * value)
	{
		___sortedGraphics_10 = value;
		Il2CppCodeGenWriteBarrier((&___sortedGraphics_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Comparison_1_t3687964427 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Comparison_1_t3687964427 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Comparison_1_t3687964427 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAGRAPHICRAYCAST_T590279048_H
#ifndef MIRAINPUTMODULE_T2476427099_H
#define MIRAINPUTMODULE_T2476427099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MiraInputModule
struct  MiraInputModule_t2476427099  : public PointerInputModule_t1441575871
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.MiraInputModule::pointerData
	PointerEventData_t1599784723 * ___pointerData_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::lastPose
	Vector2_t2243707579  ___lastPose_15;
	// System.Boolean UnityEngine.EventSystems.MiraInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_16;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_PrevActionTime
	float ___m_PrevActionTime_17;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_LastMoveVector
	Vector2_t2243707579  ___m_LastMoveVector_18;
	// System.Int32 UnityEngine.EventSystems.MiraInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_LastMousePosition
	Vector2_t2243707579  ___m_LastMousePosition_20;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_MousePosition
	Vector2_t2243707579  ___m_MousePosition_21;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_22;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_23;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_24;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_CancelButton
	String_t* ___m_CancelButton_25;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_26;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_RepeatDelay
	float ___m_RepeatDelay_27;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.MiraInputModule::m_MouseState
	MouseState_t3572864619 * ___m_MouseState_28;

public:
	inline static int32_t get_offset_of_pointerData_14() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___pointerData_14)); }
	inline PointerEventData_t1599784723 * get_pointerData_14() const { return ___pointerData_14; }
	inline PointerEventData_t1599784723 ** get_address_of_pointerData_14() { return &___pointerData_14; }
	inline void set_pointerData_14(PointerEventData_t1599784723 * value)
	{
		___pointerData_14 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_14), value);
	}

	inline static int32_t get_offset_of_lastPose_15() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___lastPose_15)); }
	inline Vector2_t2243707579  get_lastPose_15() const { return ___lastPose_15; }
	inline Vector2_t2243707579 * get_address_of_lastPose_15() { return &___lastPose_15; }
	inline void set_lastPose_15(Vector2_t2243707579  value)
	{
		___lastPose_15 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_16() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_ForceModuleActive_16)); }
	inline bool get_m_ForceModuleActive_16() const { return ___m_ForceModuleActive_16; }
	inline bool* get_address_of_m_ForceModuleActive_16() { return &___m_ForceModuleActive_16; }
	inline void set_m_ForceModuleActive_16(bool value)
	{
		___m_ForceModuleActive_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevActionTime_17() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_PrevActionTime_17)); }
	inline float get_m_PrevActionTime_17() const { return ___m_PrevActionTime_17; }
	inline float* get_address_of_m_PrevActionTime_17() { return &___m_PrevActionTime_17; }
	inline void set_m_PrevActionTime_17(float value)
	{
		___m_PrevActionTime_17 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_18() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_LastMoveVector_18)); }
	inline Vector2_t2243707579  get_m_LastMoveVector_18() const { return ___m_LastMoveVector_18; }
	inline Vector2_t2243707579 * get_address_of_m_LastMoveVector_18() { return &___m_LastMoveVector_18; }
	inline void set_m_LastMoveVector_18(Vector2_t2243707579  value)
	{
		___m_LastMoveVector_18 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_19() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_ConsecutiveMoveCount_19)); }
	inline int32_t get_m_ConsecutiveMoveCount_19() const { return ___m_ConsecutiveMoveCount_19; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_19() { return &___m_ConsecutiveMoveCount_19; }
	inline void set_m_ConsecutiveMoveCount_19(int32_t value)
	{
		___m_ConsecutiveMoveCount_19 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_20() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_LastMousePosition_20)); }
	inline Vector2_t2243707579  get_m_LastMousePosition_20() const { return ___m_LastMousePosition_20; }
	inline Vector2_t2243707579 * get_address_of_m_LastMousePosition_20() { return &___m_LastMousePosition_20; }
	inline void set_m_LastMousePosition_20(Vector2_t2243707579  value)
	{
		___m_LastMousePosition_20 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_21() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_MousePosition_21)); }
	inline Vector2_t2243707579  get_m_MousePosition_21() const { return ___m_MousePosition_21; }
	inline Vector2_t2243707579 * get_address_of_m_MousePosition_21() { return &___m_MousePosition_21; }
	inline void set_m_MousePosition_21(Vector2_t2243707579  value)
	{
		___m_MousePosition_21 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_22() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_HorizontalAxis_22)); }
	inline String_t* get_m_HorizontalAxis_22() const { return ___m_HorizontalAxis_22; }
	inline String_t** get_address_of_m_HorizontalAxis_22() { return &___m_HorizontalAxis_22; }
	inline void set_m_HorizontalAxis_22(String_t* value)
	{
		___m_HorizontalAxis_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_22), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_23() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_VerticalAxis_23)); }
	inline String_t* get_m_VerticalAxis_23() const { return ___m_VerticalAxis_23; }
	inline String_t** get_address_of_m_VerticalAxis_23() { return &___m_VerticalAxis_23; }
	inline void set_m_VerticalAxis_23(String_t* value)
	{
		___m_VerticalAxis_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_23), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_24() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_SubmitButton_24)); }
	inline String_t* get_m_SubmitButton_24() const { return ___m_SubmitButton_24; }
	inline String_t** get_address_of_m_SubmitButton_24() { return &___m_SubmitButton_24; }
	inline void set_m_SubmitButton_24(String_t* value)
	{
		___m_SubmitButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_24), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_25() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_CancelButton_25)); }
	inline String_t* get_m_CancelButton_25() const { return ___m_CancelButton_25; }
	inline String_t** get_address_of_m_CancelButton_25() { return &___m_CancelButton_25; }
	inline void set_m_CancelButton_25(String_t* value)
	{
		___m_CancelButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_25), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_26() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_InputActionsPerSecond_26)); }
	inline float get_m_InputActionsPerSecond_26() const { return ___m_InputActionsPerSecond_26; }
	inline float* get_address_of_m_InputActionsPerSecond_26() { return &___m_InputActionsPerSecond_26; }
	inline void set_m_InputActionsPerSecond_26(float value)
	{
		___m_InputActionsPerSecond_26 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_27() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_RepeatDelay_27)); }
	inline float get_m_RepeatDelay_27() const { return ___m_RepeatDelay_27; }
	inline float* get_address_of_m_RepeatDelay_27() { return &___m_RepeatDelay_27; }
	inline void set_m_RepeatDelay_27(float value)
	{
		___m_RepeatDelay_27 = value;
	}

	inline static int32_t get_offset_of_m_MouseState_28() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_MouseState_28)); }
	inline MouseState_t3572864619 * get_m_MouseState_28() const { return ___m_MouseState_28; }
	inline MouseState_t3572864619 ** get_address_of_m_MouseState_28() { return &___m_MouseState_28; }
	inline void set_m_MouseState_28(MouseState_t3572864619 * value)
	{
		___m_MouseState_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAINPUTMODULE_T2476427099_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (U3CRecenterAnimU3Ec__Iterator3_t1224236469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[9] = 
{
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U3CanimTimeU3E__0_0(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U3CreticleU3E__0_1(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U3CreticleVisU3E__0_2(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U3CoriginalColorU3E__0_3(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U3CfadeColorU3E__0_4(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U3CtimerU3E__0_5(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U24current_6(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U24disposing_7(),
	U3CRecenterAnimU3Ec__Iterator3_t1224236469::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (MiraGraphicRaycast_t590279048), -1, sizeof(MiraGraphicRaycast_t590279048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2201[8] = 
{
	0,
	MiraGraphicRaycast_t590279048::get_offset_of_blockingMask_5(),
	MiraGraphicRaycast_t590279048::get_offset_of_ignoreReversedGraphics_6(),
	MiraGraphicRaycast_t590279048::get_offset_of_blockingObjects_7(),
	MiraGraphicRaycast_t590279048::get_offset_of_raycastResults_8(),
	MiraGraphicRaycast_t590279048::get_offset_of__canvas_9(),
	MiraGraphicRaycast_t590279048_StaticFields::get_offset_of_sortedGraphics_10(),
	MiraGraphicRaycast_t590279048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (MiraInputModule_t2476427099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[15] = 
{
	MiraInputModule_t2476427099::get_offset_of_pointerData_14(),
	MiraInputModule_t2476427099::get_offset_of_lastPose_15(),
	MiraInputModule_t2476427099::get_offset_of_m_ForceModuleActive_16(),
	MiraInputModule_t2476427099::get_offset_of_m_PrevActionTime_17(),
	MiraInputModule_t2476427099::get_offset_of_m_LastMoveVector_18(),
	MiraInputModule_t2476427099::get_offset_of_m_ConsecutiveMoveCount_19(),
	MiraInputModule_t2476427099::get_offset_of_m_LastMousePosition_20(),
	MiraInputModule_t2476427099::get_offset_of_m_MousePosition_21(),
	MiraInputModule_t2476427099::get_offset_of_m_HorizontalAxis_22(),
	MiraInputModule_t2476427099::get_offset_of_m_VerticalAxis_23(),
	MiraInputModule_t2476427099::get_offset_of_m_SubmitButton_24(),
	MiraInputModule_t2476427099::get_offset_of_m_CancelButton_25(),
	MiraInputModule_t2476427099::get_offset_of_m_InputActionsPerSecond_26(),
	MiraInputModule_t2476427099::get_offset_of_m_RepeatDelay_27(),
	MiraInputModule_t2476427099::get_offset_of_m_MouseState_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (InputMode_t4136831962)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	InputMode_t4136831962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (MiraPhysicsRaycast_t1727560409), -1, sizeof(MiraPhysicsRaycast_t1727560409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2204[4] = 
{
	0,
	MiraPhysicsRaycast_t1727560409::get_offset_of__EventCamera_5(),
	MiraPhysicsRaycast_t1727560409::get_offset_of__EventMask_6(),
	MiraPhysicsRaycast_t1727560409_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (MiraPointerManager_t3638939077), -1, sizeof(MiraPointerManager_t3638939077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2205[3] = 
{
	MiraPointerManager_t3638939077_StaticFields::get_offset_of__instance_2(),
	MiraPointerManager_t3638939077::get_offset_of__pointer_3(),
	MiraPointerManager_t3638939077::get_offset_of__pointerGameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (MiraReticlePointer_t592362690), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (FollowHeadAutomatic_t1062824722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[3] = 
{
	FollowHeadAutomatic_t1062824722::get_offset_of_headFollowPositionCM_2(),
	FollowHeadAutomatic_t1062824722::get_offset_of_lerpSpeed_3(),
	FollowHeadAutomatic_t1062824722::get_offset_of_head_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (FollowHeadAutomaticScale_t1766947074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[3] = 
{
	FollowHeadAutomaticScale_t1766947074::get_offset_of_headFollowPositionM_2(),
	FollowHeadAutomaticScale_t1766947074::get_offset_of_lerpSpeed_3(),
	FollowHeadAutomaticScale_t1766947074::get_offset_of_head_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (GyroController_t3857203917), -1, sizeof(GyroController_t3857203917_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2209[14] = 
{
	GyroController_t3857203917_StaticFields::get_offset_of_instance_2(),
	GyroController_t3857203917::get_offset_of_gyroRotation_3(),
	GyroController_t3857203917::get_offset_of_gyroEnabled_4(),
	GyroController_t3857203917::get_offset_of_lowPassFilterFactor_5(),
	GyroController_t3857203917::get_offset_of_frontCamera_6(),
	GyroController_t3857203917::get_offset_of_cameraBase_7(),
	GyroController_t3857203917::get_offset_of_calibration_8(),
	GyroController_t3857203917::get_offset_of_baseOrientation_9(),
	GyroController_t3857203917::get_offset_of_referenceRotation_10(),
	GyroController_t3857203917::get_offset_of_inverseGyroSetup_11(),
	GyroController_t3857203917::get_offset_of_debugGUI_12(),
	GyroController_t3857203917::get_offset_of_useFrontCamera_13(),
	GyroController_t3857203917::get_offset_of_miraRemoteMode_14(),
	GyroController_t3857203917::get_offset_of_rotationOffset_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (InverseGyroController_t2551975429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[1] = 
{
	InverseGyroController_t2551975429::get_offset_of_stereoCamRig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (LaserLerp_t3011343440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[1] = 
{
	LaserLerp_t3011343440::get_offset_of_laserLerpSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (MiraArController_t555016598), -1, sizeof(MiraArController_t555016598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2212[21] = 
{
	MiraArController_t555016598::get_offset_of_IPD_2(),
	MiraArController_t555016598::get_offset_of_leftCam_3(),
	MiraArController_t555016598::get_offset_of_rightCam_4(),
	MiraArController_t555016598::get_offset_of_cameraRig_5(),
	MiraArController_t555016598::get_offset_of_isRotationalOnly_6(),
	MiraArController_t555016598::get_offset_of_isSpectator_7(),
	MiraArController_t555016598::get_offset_of_nearClipPlane_8(),
	MiraArController_t555016598::get_offset_of_farClipPlane_9(),
	MiraArController_t555016598::get_offset_of_defaultScale_10(),
	MiraArController_t555016598::get_offset_of_setScaleMultiplier_11(),
	MiraArController_t555016598_StaticFields::get_offset_of_scaleMultiplier_12(),
	MiraArController_t555016598::get_offset_of_stereoCamFov_13(),
	MiraArController_t555016598::get_offset_of_mv_14(),
	MiraArController_t555016598_StaticFields::get_offset_of_instance_15(),
	MiraArController_t555016598::get_offset_of_btnTexture_16(),
	MiraArController_t555016598::get_offset_of_buttonHeight_17(),
	MiraArController_t555016598::get_offset_of_buttonWidth_18(),
	MiraArController_t555016598::get_offset_of_MiraGuiSkin_19(),
	MiraArController_t555016598::get_offset_of_miraInputModule_20(),
	MiraArController_t555016598::get_offset_of_GUIEnabled_21(),
	MiraArController_t555016598::get_offset_of_settingsMenu_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (MiraEditorPreview_t3716774330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (MiraiOSBridge_t2338730851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (MiraLaserPointerLength_t1165938491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[3] = 
{
	MiraLaserPointerLength_t1165938491::get_offset_of_widthMultiplier_2(),
	MiraLaserPointerLength_t1165938491::get_offset_of_reticle_3(),
	MiraLaserPointerLength_t1165938491::get_offset_of_rend_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (MiraARVideo_t1477805071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[1] = 
{
	MiraARVideo_t1477805071::get_offset_of_m_clearTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (MiraConnectionMessageIds_t1100361794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (MiraSubMessageIds_t3396168626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (MiraLivePreviewEditor_t3425584316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (MiraLivePreviewPlayer_t3516305084), -1, sizeof(MiraLivePreviewPlayer_t3516305084_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2220[9] = 
{
	MiraLivePreviewPlayer_t3516305084::get_offset_of_playerConnection_2(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_bSessionActive_3(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_editorID_4(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_liveViewScreenTex_5(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_bTexturesInitialized_6(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_isTracking_7(),
	MiraLivePreviewPlayer_t3516305084_StaticFields::get_offset_of_m_userInput_8(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_btFrameCounter_9(),
	MiraLivePreviewPlayer_t3516305084::get_offset_of_btSendRate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (MiraLivePreviewVideo_t2362804972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (MiraLivePreviewWikiConfig_t2812595783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[1] = 
{
	MiraLivePreviewWikiConfig_t2812595783::get_offset_of_ArCam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (ObjectSerializationExtension_t2339960184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (SerializableVector3_t4294681249)+ sizeof (RuntimeObject), sizeof(SerializableVector3_t4294681249 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[3] = 
{
	SerializableVector3_t4294681249::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector3_t4294681249::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector3_t4294681249::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (SerializableVector2_t4294681248)+ sizeof (RuntimeObject), sizeof(SerializableVector2_t4294681248 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[2] = 
{
	SerializableVector2_t4294681248::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector2_t4294681248::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (SerializableVector4_t4294681242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[4] = 
{
	SerializableVector4_t4294681242::get_offset_of_x_0(),
	SerializableVector4_t4294681242::get_offset_of_y_1(),
	SerializableVector4_t4294681242::get_offset_of_z_2(),
	SerializableVector4_t4294681242::get_offset_of_w_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (SerializableQuaternion_t3902400085)+ sizeof (RuntimeObject), sizeof(SerializableQuaternion_t3902400085 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	SerializableQuaternion_t3902400085::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableQuaternion_t3902400085::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableQuaternion_t3902400085::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableQuaternion_t3902400085::get_offset_of_w_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (serializableFloat_t3811907803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[1] = 
{
	serializableFloat_t3811907803::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (serializableGyroscope_t1293559986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[2] = 
{
	serializableGyroscope_t1293559986::get_offset_of_attitude_0(),
	serializableGyroscope_t1293559986::get_offset_of_userAcceleration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (serializableTransform_t2202979937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[2] = 
{
	serializableTransform_t2202979937::get_offset_of_position_0(),
	serializableTransform_t2202979937::get_offset_of_rotation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (serializableBTRemote_t622411689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[3] = 
{
	serializableBTRemote_t622411689::get_offset_of_orientation_0(),
	serializableBTRemote_t622411689::get_offset_of_rotationRate_1(),
	serializableBTRemote_t622411689::get_offset_of_acceleration_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (serializableBTRemoteTouchPad_t2020069673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[7] = 
{
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_touchActive_0(),
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_touchButton_1(),
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_touchPos_2(),
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_upButton_3(),
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_downButton_4(),
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_leftButton_5(),
	serializableBTRemoteTouchPad_t2020069673::get_offset_of_rightButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (serializableBTRemoteButtons_t2327889448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[3] = 
{
	serializableBTRemoteButtons_t2327889448::get_offset_of_startButton_0(),
	serializableBTRemoteButtons_t2327889448::get_offset_of_backButton_1(),
	serializableBTRemoteButtons_t2327889448::get_offset_of_triggerButton_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (serializablePointCloud_t1992421910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	serializablePointCloud_t1992421910::get_offset_of_pointCloudData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (serializableFromEditorMessage_t2894567809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	serializableFromEditorMessage_t2894567809::get_offset_of_subMessageId_0(),
	serializableFromEditorMessage_t2894567809::get_offset_of_bytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (MiraPostRender_t51508757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[23] = 
{
	MiraPostRender_t51508757::get_offset_of_stereoCamFov_2(),
	MiraPostRender_t51508757::get_offset_of_noDistortion_3(),
	MiraPostRender_t51508757::get_offset_of_postReduceDistortion_4(),
	MiraPostRender_t51508757::get_offset_of_xDistAmount_5(),
	MiraPostRender_t51508757::get_offset_of_yDistAmount_6(),
	MiraPostRender_t51508757::get_offset_of_tanCoordinates_7(),
	MiraPostRender_t51508757::get_offset_of_eye_8(),
	MiraPostRender_t51508757::get_offset_of_xSize_9(),
	MiraPostRender_t51508757::get_offset_of_ySize_10(),
	MiraPostRender_t51508757::get_offset_of_zOffset_11(),
	MiraPostRender_t51508757::get_offset_of_xScalar_12(),
	MiraPostRender_t51508757::get_offset_of_yScalar_13(),
	MiraPostRender_t51508757::get_offset_of_tanConstant_14(),
	MiraPostRender_t51508757::get_offset_of_desiredParallaxDist_15(),
	MiraPostRender_t51508757::get_offset_of_mesh_16(),
	MiraPostRender_t51508757::get_offset_of_renderTextureMaterial_17(),
	MiraPostRender_t51508757::get_offset_of_IPD_18(),
	MiraPostRender_t51508757::get_offset_of_ParallaxShift_19(),
	MiraPostRender_t51508757::get_offset_of_coefficientsXLeft_20(),
	MiraPostRender_t51508757::get_offset_of_coefficientsYLeft_21(),
	MiraPostRender_t51508757::get_offset_of_coefficientsXRight_22(),
	MiraPostRender_t51508757::get_offset_of_coefficientsYRight_23(),
	MiraPostRender_t51508757::get_offset_of_distortion_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (Eye_t3339251924)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2237[3] = 
{
	Eye_t3339251924::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (MiraPreRender_t2810281508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	MiraPreRender_t2810281508::get_offset_of_U3CcamU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (MiraReticle_t2195475589), -1, sizeof(MiraReticle_t2195475589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2239[14] = 
{
	MiraReticle_t2195475589::get_offset_of_reticlepointer_2(),
	MiraReticle_t2195475589_StaticFields::get_offset_of__instance_3(),
	MiraReticle_t2195475589::get_offset_of_cameraRig_4(),
	MiraReticle_t2195475589::get_offset_of_onlyVisibleOnHover_5(),
	MiraReticle_t2195475589::get_offset_of_maxDistance_6(),
	MiraReticle_t2195475589::get_offset_of_minDistance_7(),
	MiraReticle_t2195475589::get_offset_of_minScale_8(),
	MiraReticle_t2195475589::get_offset_of_maxScale_9(),
	MiraReticle_t2195475589::get_offset_of_lastDistance_10(),
	MiraReticle_t2195475589::get_offset_of_externalMultiplier_11(),
	MiraReticle_t2195475589::get_offset_of_reticleHoverColor_12(),
	MiraReticle_t2195475589::get_offset_of_reticleIdleColor_13(),
	MiraReticle_t2195475589::get_offset_of_reticleRenderer_14(),
	MiraReticle_t2195475589::get_offset_of_reticleOriginalScale_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (MiraViewer_t1267122109), -1, sizeof(MiraViewer_t1267122109_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2240[6] = 
{
	MiraViewer_t1267122109::get_offset_of_stereoCamFov_2(),
	MiraViewer_t1267122109_StaticFields::get_offset_of_viewer_3(),
	MiraViewer_t1267122109::get_offset_of_cameraRig_4(),
	MiraViewer_t1267122109::get_offset_of_Left_Eye_5(),
	MiraViewer_t1267122109::get_offset_of_Right_Eye_6(),
	MiraViewer_t1267122109_StaticFields::get_offset_of_Instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (CameraNames_t2563008952)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2241[3] = 
{
	CameraNames_t2563008952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (MiraWikitudeManager_t2317471266), -1, sizeof(MiraWikitudeManager_t2317471266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2242[9] = 
{
	MiraWikitudeManager_t2317471266::get_offset_of_ArCam_2(),
	MiraWikitudeManager_t2317471266::get_offset_of_headOrientation3DOF_3(),
	MiraWikitudeManager_t2317471266::get_offset_of_imageTracker_4(),
	MiraWikitudeManager_t2317471266_StaticFields::get_offset_of_instance_5(),
	MiraWikitudeManager_t2317471266::get_offset_of_scaleMultiplier_6(),
	MiraWikitudeManager_t2317471266::get_offset_of_rotationalOffset_7(),
	MiraWikitudeManager_t2317471266::get_offset_of_positionalOffset_8(),
	MiraWikitudeManager_t2317471266::get_offset_of_rotationalTracking_9(),
	MiraWikitudeManager_t2317471266::get_offset_of_flag_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (U3CActivateTrackingU3Ec__Iterator0_t1868664202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[4] = 
{
	U3CActivateTrackingU3Ec__Iterator0_t1868664202::get_offset_of_U24this_0(),
	U3CActivateTrackingU3Ec__Iterator0_t1868664202::get_offset_of_U24current_1(),
	U3CActivateTrackingU3Ec__Iterator0_t1868664202::get_offset_of_U24disposing_2(),
	U3CActivateTrackingU3Ec__Iterator0_t1868664202::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (RotationalTrackingManager_t1512110775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[10] = 
{
	RotationalTrackingManager_t1512110775::get_offset_of_mainCamera_2(),
	RotationalTrackingManager_t1512110775::get_offset_of_cameraRig_3(),
	RotationalTrackingManager_t1512110775::get_offset_of_isRotational_4(),
	RotationalTrackingManager_t1512110775::get_offset_of_positionBuffer_5(),
	RotationalTrackingManager_t1512110775::get_offset_of_rotationBuffer_6(),
	RotationalTrackingManager_t1512110775::get_offset_of_delay_7(),
	RotationalTrackingManager_t1512110775::get_offset_of_bufferSize_8(),
	RotationalTrackingManager_t1512110775::get_offset_of_bufferDiscardLast_9(),
	RotationalTrackingManager_t1512110775::get_offset_of_camRigStartPosition_10(),
	RotationalTrackingManager_t1512110775::get_offset_of_isSpectator_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CRotationalSetupU3Ec__Iterator0_t2905453790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[4] = 
{
	U3CRotationalSetupU3Ec__Iterator0_t2905453790::get_offset_of_U24this_0(),
	U3CRotationalSetupU3Ec__Iterator0_t2905453790::get_offset_of_U24current_1(),
	U3CRotationalSetupU3Ec__Iterator0_t2905453790::get_offset_of_U24disposing_2(),
	U3CRotationalSetupU3Ec__Iterator0_t2905453790::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (U3CBufferPositionU3Ec__Iterator1_t605192684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[4] = 
{
	U3CBufferPositionU3Ec__Iterator1_t605192684::get_offset_of_U24this_0(),
	U3CBufferPositionU3Ec__Iterator1_t605192684::get_offset_of_U24current_1(),
	U3CBufferPositionU3Ec__Iterator1_t605192684::get_offset_of_U24disposing_2(),
	U3CBufferPositionU3Ec__Iterator1_t605192684::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[5] = 
{
	U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835::get_offset_of_U3CoffsetPositionU3E__0_0(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835::get_offset_of_U24this_1(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835::get_offset_of_U24current_2(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835::get_offset_of_U24disposing_3(),
	U3CSwitchToCameraGyroU3Ec__Iterator2_t419419835::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (SettingsManager_t2519859232), -1, sizeof(SettingsManager_t2519859232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2248[7] = 
{
	SettingsManager_t2519859232::get_offset_of_MainSettingsMenu_2(),
	SettingsManager_t2519859232::get_offset_of_RemoteMenu_3(),
	SettingsManager_t2519859232::get_offset_of_connectedNotification_4(),
	SettingsManager_t2519859232::get_offset_of_disconnectedNotification_5(),
	SettingsManager_t2519859232_StaticFields::get_offset_of_Instance_6(),
	SettingsManager_t2519859232::get_offset_of_remotesController_7(),
	SettingsManager_t2519859232::get_offset_of_settingsButton_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[4] = 
{
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900::get_offset_of_U24this_0(),
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900::get_offset_of_U24current_1(),
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900::get_offset_of_U24disposing_2(),
	U3CRemoteConnectedNotificationU3Ec__Iterator0_t4057175900::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[4] = 
{
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261::get_offset_of_U24this_0(),
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261::get_offset_of_U24current_1(),
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261::get_offset_of_U24disposing_2(),
	U3CRemoteDisconnectedNotificationU3Ec__Iterator1_t2434143261::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (Transition2D_t3717442181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[6] = 
{
	Transition2D_t3717442181::get_offset_of_displayOnce_2(),
	Transition2D_t3717442181::get_offset_of_MiraCameraRig_3(),
	Transition2D_t3717442181::get_offset_of_instructionScreen_4(),
	Transition2D_t3717442181::get_offset_of_twoDCamera_5(),
	Transition2D_t3717442181::get_offset_of_distortionL_6(),
	Transition2D_t3717442181::get_offset_of_distortionR_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (U3CStartU3Ec__Iterator0_t2484427336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[4] = 
{
	U3CStartU3Ec__Iterator0_t2484427336::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2484427336::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2484427336::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2484427336::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[4] = 
{
	U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554::get_offset_of_U24this_0(),
	U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554::get_offset_of_U24current_1(),
	U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554::get_offset_of_U24disposing_2(),
	U3CTransitionToLandscapeU3Ec__Iterator1_t3812029554::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (U3CTransitionToPortraitU3Ec__Iterator2_t444968087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[4] = 
{
	U3CTransitionToPortraitU3Ec__Iterator2_t444968087::get_offset_of_U24this_0(),
	U3CTransitionToPortraitU3Ec__Iterator2_t444968087::get_offset_of_U24current_1(),
	U3CTransitionToPortraitU3Ec__Iterator2_t444968087::get_offset_of_U24disposing_2(),
	U3CTransitionToPortraitU3Ec__Iterator2_t444968087::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (AxisSpin_t3838354289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[2] = 
{
	AxisSpin_t3838354289::get_offset_of_spinRate_2(),
	AxisSpin_t3838354289::get_offset_of_spinDirection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (DemoSceneManager_t779426248), -1, sizeof(DemoSceneManager_t779426248_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2256[2] = 
{
	DemoSceneManager_t779426248_StaticFields::get_offset_of_isSpectator_2(),
	DemoSceneManager_t779426248_StaticFields::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (DynamicallyChangeIPD_t626189094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (FaceCamera_t2774504826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[1] = 
{
	FaceCamera_t2774504826::get_offset_of_stereoCamRig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (FadeInScene_t62196113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[7] = 
{
	FadeInScene_t62196113::get_offset_of_shouldFadeOut_2(),
	FadeInScene_t62196113::get_offset_of_fadeTexture_3(),
	FadeInScene_t62196113::get_offset_of_fadeSpeed_4(),
	FadeInScene_t62196113::get_offset_of_drawDepth_5(),
	FadeInScene_t62196113::get_offset_of_alpha_6(),
	FadeInScene_t62196113::get_offset_of_fadeDir_7(),
	FadeInScene_t62196113::get_offset_of_shouldBlackOut_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CStartLogOutU3Ec__Iterator0_t1067525859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[5] = 
{
	U3CStartLogOutU3Ec__Iterator0_t1067525859::get_offset_of_sceneToLoad_0(),
	U3CStartLogOutU3Ec__Iterator0_t1067525859::get_offset_of_U24this_1(),
	U3CStartLogOutU3Ec__Iterator0_t1067525859::get_offset_of_U24current_2(),
	U3CStartLogOutU3Ec__Iterator0_t1067525859::get_offset_of_U24disposing_3(),
	U3CStartLogOutU3Ec__Iterator0_t1067525859::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (Fishingline_t2350076148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[14] = 
{
	Fishingline_t2350076148::get_offset_of_currentSegments_2(),
	Fishingline_t2350076148::get_offset_of_referencePoints_3(),
	Fishingline_t2350076148::get_offset_of_defaultPositions_4(),
	Fishingline_t2350076148::get_offset_of_defaultFishlineLength_5(),
	Fishingline_t2350076148::get_offset_of_numSegments_6(),
	Fishingline_t2350076148::get_offset_of_destination_7(),
	Fishingline_t2350076148::get_offset_of_hitDistance_8(),
	Fishingline_t2350076148::get_offset_of_reticle_9(),
	Fishingline_t2350076148::get_offset_of_lerpSpeedMult_10(),
	Fishingline_t2350076148::get_offset_of_fishinglineParent_11(),
	Fishingline_t2350076148::get_offset_of_lineRend_12(),
	Fishingline_t2350076148::get_offset_of_colliders_13(),
	Fishingline_t2350076148::get_offset_of_parentedToEnd_14(),
	Fishingline_t2350076148::get_offset_of_attachPoint_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (FishinglineCast_t1149735085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[7] = 
{
	FishinglineCast_t1149735085::get_offset_of_minCast_2(),
	FishinglineCast_t1149735085::get_offset_of_maxCast_3(),
	FishinglineCast_t1149735085::get_offset_of_castSpeedMultiplier_4(),
	FishinglineCast_t1149735085::get_offset_of_castStart_5(),
	FishinglineCast_t1149735085::get_offset_of_lastCast_6(),
	FishinglineCast_t1149735085::get_offset_of_fishingline_7(),
	FishinglineCast_t1149735085::get_offset_of_currentScaleMult_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (MarkerRotationWatcher_t3969903492), -1, sizeof(MarkerRotationWatcher_t3969903492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[9] = 
{
	MarkerRotationWatcher_t3969903492::get_offset_of_mainCamera_2(),
	MarkerRotationWatcher_t3969903492::get_offset_of_deltaDotProduct_3(),
	MarkerRotationWatcher_t3969903492::get_offset_of_necessaryRotation_4(),
	MarkerRotationWatcher_t3969903492::get_offset_of_dot_5(),
	MarkerRotationWatcher_t3969903492_StaticFields::get_offset_of_OnRotated_6(),
	MarkerRotationWatcher_t3969903492::get_offset_of_startVector_7(),
	MarkerRotationWatcher_t3969903492::get_offset_of_firstAngle_8(),
	MarkerRotationWatcher_t3969903492::get_offset_of_counter_9(),
	MarkerRotationWatcher_t3969903492::get_offset_of_hasRotated_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (RotateAction_t2368064732), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (MiraExampleInteractionScript_t1495489050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[6] = 
{
	MiraExampleInteractionScript_t1495489050::get_offset_of_textBeneathPlanet_2(),
	MiraExampleInteractionScript_t1495489050::get_offset_of_timeSinceLastInteraction_3(),
	MiraExampleInteractionScript_t1495489050::get_offset_of_startingLocation_4(),
	MiraExampleInteractionScript_t1495489050::get_offset_of_isPlanetFeelingSatisfied_5(),
	MiraExampleInteractionScript_t1495489050::get_offset_of_isUserPointingAtPlanet_6(),
	MiraExampleInteractionScript_t1495489050::get_offset_of_spin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (U3CtemperTantrumU3Ec__Iterator0_t2561833528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[7] = 
{
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U3CiU3E__1_0(),
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U3CdistanceU3E__2_1(),
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U3CiU3E__3_2(),
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U24this_3(),
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U24current_4(),
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U24disposing_5(),
	U3CtemperTantrumU3Ec__Iterator0_t2561833528::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (MiraGrabExample_t576458027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	MiraGrabExample_t576458027::get_offset_of_textBeneathPlanet_2(),
	MiraGrabExample_t576458027::get_offset_of_isGrabbing_3(),
	MiraGrabExample_t576458027::get_offset_of_lastTouchPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (MiraPhysicsGrabExample_t1656480436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[4] = 
{
	MiraPhysicsGrabExample_t1656480436::get_offset_of_isGrabbing_2(),
	MiraPhysicsGrabExample_t1656480436::get_offset_of_originalConstraints_3(),
	MiraPhysicsGrabExample_t1656480436::get_offset_of_rigidBody_4(),
	MiraPhysicsGrabExample_t1656480436::get_offset_of_lastTouchPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (MiraPhysicsPlaypenBoundary_t3180346929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (ToggleModes_t4125091540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (TrackerLerpDriver_t1630291735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[3] = 
{
	TrackerLerpDriver_t1630291735::get_offset_of_rotationWatcher_2(),
	TrackerLerpDriver_t1630291735::get_offset_of_startPosition_3(),
	TrackerLerpDriver_t1630291735::get_offset_of_endPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (Platform_t358478341), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (ExposureController_t4178516857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[1] = 
{
	ExposureController_t4178516857::get_offset_of_WikiCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ImageTrackingController_t3751215764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (ScaleOverride_t3404595986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[1] = 
{
	ScaleOverride_t3404595986::get_offset_of_wikiCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (Benchmark01_t2768175604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[10] = 
{
	Benchmark01_t2768175604::get_offset_of_BenchmarkType_2(),
	Benchmark01_t2768175604::get_offset_of_TMProFont_3(),
	Benchmark01_t2768175604::get_offset_of_TextMeshFont_4(),
	Benchmark01_t2768175604::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t2768175604::get_offset_of_m_textContainer_6(),
	Benchmark01_t2768175604::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t2768175604::get_offset_of_m_material01_10(),
	Benchmark01_t2768175604::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CStartU3Ec__Iterator0_t1060020671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[5] = 
{
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (Benchmark01_UGUI_t3449578277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[10] = 
{
	Benchmark01_UGUI_t3449578277::get_offset_of_BenchmarkType_2(),
	Benchmark01_UGUI_t3449578277::get_offset_of_canvas_3(),
	Benchmark01_UGUI_t3449578277::get_offset_of_TMProFont_4(),
	Benchmark01_UGUI_t3449578277::get_offset_of_TextMeshFont_5(),
	Benchmark01_UGUI_t3449578277::get_offset_of_m_textMeshPro_6(),
	Benchmark01_UGUI_t3449578277::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_UGUI_t3449578277::get_offset_of_m_material01_10(),
	Benchmark01_UGUI_t3449578277::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CStartU3Ec__Iterator0_t17013742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[5] = 
{
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (Benchmark02_t39292249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[3] = 
{
	Benchmark02_t39292249::get_offset_of_SpawnType_2(),
	Benchmark02_t39292249::get_offset_of_NumberOfNPC_3(),
	Benchmark02_t39292249::get_offset_of_floatingText_Script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (Benchmark03_t1605376190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[3] = 
{
	Benchmark03_t1605376190::get_offset_of_SpawnType_2(),
	Benchmark03_t1605376190::get_offset_of_NumberOfNPC_3(),
	Benchmark03_t1605376190::get_offset_of_TheFont_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (Benchmark04_t3171460131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[5] = 
{
	Benchmark04_t3171460131::get_offset_of_SpawnType_2(),
	Benchmark04_t3171460131::get_offset_of_MinPointSize_3(),
	Benchmark04_t3171460131::get_offset_of_MaxPointSize_4(),
	Benchmark04_t3171460131::get_offset_of_Steps_5(),
	Benchmark04_t3171460131::get_offset_of_m_Transform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (CameraController_t766129913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[25] = 
{
	CameraController_t766129913::get_offset_of_cameraTransform_2(),
	CameraController_t766129913::get_offset_of_dummyTarget_3(),
	CameraController_t766129913::get_offset_of_CameraTarget_4(),
	CameraController_t766129913::get_offset_of_FollowDistance_5(),
	CameraController_t766129913::get_offset_of_MaxFollowDistance_6(),
	CameraController_t766129913::get_offset_of_MinFollowDistance_7(),
	CameraController_t766129913::get_offset_of_ElevationAngle_8(),
	CameraController_t766129913::get_offset_of_MaxElevationAngle_9(),
	CameraController_t766129913::get_offset_of_MinElevationAngle_10(),
	CameraController_t766129913::get_offset_of_OrbitalAngle_11(),
	CameraController_t766129913::get_offset_of_CameraMode_12(),
	CameraController_t766129913::get_offset_of_MovementSmoothing_13(),
	CameraController_t766129913::get_offset_of_RotationSmoothing_14(),
	CameraController_t766129913::get_offset_of_previousSmoothing_15(),
	CameraController_t766129913::get_offset_of_MovementSmoothingValue_16(),
	CameraController_t766129913::get_offset_of_RotationSmoothingValue_17(),
	CameraController_t766129913::get_offset_of_MoveSensitivity_18(),
	CameraController_t766129913::get_offset_of_currentVelocity_19(),
	CameraController_t766129913::get_offset_of_desiredPosition_20(),
	CameraController_t766129913::get_offset_of_mouseX_21(),
	CameraController_t766129913::get_offset_of_mouseY_22(),
	CameraController_t766129913::get_offset_of_moveVector_23(),
	CameraController_t766129913::get_offset_of_mouseWheel_24(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (CameraModes_t2188281734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2284[4] = 
{
	CameraModes_t2188281734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (ChatController_t2669781690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[3] = 
{
	ChatController_t2669781690::get_offset_of_TMP_ChatInput_2(),
	ChatController_t2669781690::get_offset_of_TMP_ChatOutput_3(),
	ChatController_t2669781690::get_offset_of_ChatScrollbar_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (EnvMapAnimator_t1635389402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[3] = 
{
	EnvMapAnimator_t1635389402::get_offset_of_RotationSpeeds_2(),
	EnvMapAnimator_t1635389402::get_offset_of_m_textMeshPro_3(),
	EnvMapAnimator_t1635389402::get_offset_of_m_material_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (U3CStartU3Ec__Iterator0_t110035792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[5] = 
{
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (ObjectSpin_t3410945885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[10] = 
{
	ObjectSpin_t3410945885::get_offset_of_SpinSpeed_2(),
	ObjectSpin_t3410945885::get_offset_of_RotationRange_3(),
	ObjectSpin_t3410945885::get_offset_of_m_transform_4(),
	ObjectSpin_t3410945885::get_offset_of_m_time_5(),
	ObjectSpin_t3410945885::get_offset_of_m_prevPOS_6(),
	ObjectSpin_t3410945885::get_offset_of_m_initial_Rotation_7(),
	ObjectSpin_t3410945885::get_offset_of_m_initial_Position_8(),
	ObjectSpin_t3410945885::get_offset_of_m_lightColor_9(),
	ObjectSpin_t3410945885::get_offset_of_frames_10(),
	ObjectSpin_t3410945885::get_offset_of_Motion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (MotionType_t2454277493)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[4] = 
{
	MotionType_t2454277493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (ShaderPropAnimator_t2679013775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[4] = 
{
	ShaderPropAnimator_t2679013775::get_offset_of_m_Renderer_2(),
	ShaderPropAnimator_t2679013775::get_offset_of_m_Material_3(),
	ShaderPropAnimator_t2679013775::get_offset_of_GlowCurve_4(),
	ShaderPropAnimator_t2679013775::get_offset_of_m_frame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t35148318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (SimpleScript_t767280347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[3] = 
{
	SimpleScript_t767280347::get_offset_of_m_textMeshPro_2(),
	0,
	SimpleScript_t767280347::get_offset_of_m_frame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (SkewTextExample_t3378890949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[4] = 
{
	SkewTextExample_t3378890949::get_offset_of_m_TextComponent_2(),
	SkewTextExample_t3378890949::get_offset_of_VertexCurve_3(),
	SkewTextExample_t3378890949::get_offset_of_CurveScale_4(),
	SkewTextExample_t3378890949::get_offset_of_ShearAmount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U3CWarpTextU3Ec__Iterator0_t1799541603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (TeleType_t2513439854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[3] = 
{
	TeleType_t2513439854::get_offset_of_label01_2(),
	TeleType_t2513439854::get_offset_of_label02_3(),
	TeleType_t2513439854::get_offset_of_m_textMeshPro_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (U3CStartU3Ec__Iterator0_t189980609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[7] = 
{
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (TextConsoleSimulator_t2207663326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[2] = 
{
	TextConsoleSimulator_t2207663326::get_offset_of_m_TextComponent_2(),
	TextConsoleSimulator_t2207663326::get_offset_of_hasTextChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t1407882744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t2877888826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U24PC_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
