﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_EmptyCallback_m158545079(char* ___identifier0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_BoolCallback_m2992614063(char* ___identifier0, int32_t ___value1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_IntCallback_m729026750(char* ___identifier0, int32_t ___value1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_FloatCallback_m3540308995(char* ___identifier0, float ___value1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_ErrorCallback_m1040443852(char* ___identifier0, int32_t ___success1, int32_t ___errorCode2, char* ___message3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_DiscoveredRemoteCallback_m2760543242(char* ___identifier0, char* ___name1, char* ___remoteIdentifier2, int32_t ___rssi3, int32_t ___isPreferred4);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_ConnectedRemoteCallback_m1059875201(char* ___identifier0, char* ___name1, char* ___remoteIdentifier2, char* ___productName3, char* ___serialNumber4, char* ___hardwareIdentifier5, char* ___firmwareVersion6, float ___batteryPercentage7, int32_t ___rssi8, int32_t ___isConnected9, int32_t ___isPreferred10);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeBridge_RemoteMotionCallback_m2331612649(char* ___identifier0, float ___x1, float ___y2, float ___z3);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[8] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_EmptyCallback_m158545079),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_BoolCallback_m2992614063),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_IntCallback_m729026750),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_FloatCallback_m3540308995),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_ErrorCallback_m1040443852),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_DiscoveredRemoteCallback_m2760543242),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_ConnectedRemoteCallback_m1059875201),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeBridge_RemoteMotionCallback_m2331612649),
};
