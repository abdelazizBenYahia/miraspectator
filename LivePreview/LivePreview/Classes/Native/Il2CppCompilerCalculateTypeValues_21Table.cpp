﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Action`1<Remote>
struct Action_1_t462642944;
// RemoteManager
struct RemoteManager_t2208998541;
// RemoteTouchInput/RemoteTouchInputEventHandler
struct RemoteTouchInputEventHandler_t1375955781;
// RemoteOrientationInput/RemoteOrientationInputEventHandler
struct RemoteOrientationInputEventHandler_t4128243105;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// ConnectToPreviousRemote
struct ConnectToPreviousRemote_t1323906560;
// System.Action`1<MiraRemoteException>
struct Action_1_t3189573364;
// MiraController
struct MiraController_t1058876281;
// System.Collections.Generic.Dictionary`2<System.String,System.Action>
struct Dictionary_2_t846283718;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Boolean>>
struct Dictionary_2_t1247186066;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Int32>>
struct Dictionary_2_t3788456092;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Single>>
struct Dictionary_2_t3793088576;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<MiraRemoteException>>
struct Dictionary_2_t809385330;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<Remote>>
struct Dictionary_2_t2377422206;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.Single,System.Single,System.Single>>
struct Dictionary_2_t4078021086;
// NativeBridge/MRIntCallback
struct MRIntCallback_t2215762430;
// NativeBridge/MRBoolCallback
struct MRBoolCallback_t2221477487;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// NativeBridge/MRDiscoveredRemoteCallback
struct MRDiscoveredRemoteCallback_t3604605211;
// NativeBridge/MRErrorCallback
struct MRErrorCallback_t1972775801;
// NativeBridge/MRConnectedRemoteCallback
struct MRConnectedRemoteCallback_t3085918786;
// NativeBridge/MRFloatCallback
struct MRFloatCallback_t3699197057;
// NativeBridge/MREmptyCallback
struct MREmptyCallback_t3969082922;
// NativeBridge/MRRemoteMotionCallback
struct MRRemoteMotionCallback_t3634481787;
// RemoteBase
struct RemoteBase_t3616301967;
// VirtualRemote
struct VirtualRemote_t4041918011;
// Remote
struct Remote_t660843562;
// RemoteButtonInput
struct RemoteButtonInput_t144791130;
// RemoteTouchPadInput
struct RemoteTouchPadInput_t1081319266;
// RemoteMotionInput
struct RemoteMotionInput_t3548926620;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// RemoteButtonInput/RemoteButtonInputEventHandler
struct RemoteButtonInputEventHandler_t1063480517;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// RemoteAxisInput/RemoteAxisInputEventHandler
struct RemoteAxisInputEventHandler_t3796077797;
// FlatLighting.MaterialBlender
struct MaterialBlender_t4011564903;
// MiraRemoteException
struct MiraRemoteException_t3387773982;
// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler
struct RemoteMotionSensorInputEventHandler_t2876817765;
// RemoteTouchPadInput/RemoteTouchPadInputEventHandler
struct RemoteTouchPadInputEventHandler_t2832767717;
// RemoteAxisInput
struct RemoteAxisInput_t2770128439;
// RemoteTouchInput
struct RemoteTouchInput_t3826108381;
// RemoteMotionInput/RemoteMotionInputEventHandler
struct RemoteMotionInputEventHandler_t1458362501;
// RemoteOrientationInput
struct RemoteOrientationInput_t3303200544;
// RemoteMotionSensorInput
struct RemoteMotionSensorInput_t841531810;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Remote/RemoteRefreshedEventHandler
struct RemoteRefreshedEventHandler_t1036293937;
// System.EventArgs
struct EventArgs_t3289624707;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// GameManager
struct GameManager_t2252321495;
// AudioManager
struct AudioManager_t4222704959;
// FlatLighting.LightSource`1/LightBag<FlatLighting.ShadowProjector>
struct LightBag_t2584411587;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.String[]
struct StringU5BU5D_t1642385972;
// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent
struct OnEnterFieldOfVisionEvent_t3362888889;
// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent
struct OnExitFieldOfVisionEvent_t2513344377;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Renderer
struct Renderer_t257310565;
// SceneLigtingSetup
struct SceneLigtingSetup_t3978848013;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// PlanetScript
struct PlanetScript_t2420377963;
// MiraBTRemoteInput
struct MiraBTRemoteInput_t988911721;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Collections.Generic.List`1<System.Func`1<System.Boolean>>
struct List_1_t854121236;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2045506961;
// UnityEngine.UI.Text
struct Text_t356221433;
// RemoteManager/RemoteConnectedEventHandler
struct RemoteConnectedEventHandler_t3456249665;
// RemoteManager/RemoteDisconnectedEventHandler
struct RemoteDisconnectedEventHandler_t730656759;
// System.Collections.Generic.List`1<Remote>
struct List_1_t29964694;
// FlatLighting.LightSource`1/LightBag<FlatLighting.SpotLight>
struct LightBag_t98915391;
// FlatLighting.LightSource`1/LightBag<FlatLighting.PointLight>
struct LightBag_t1178250251;
// FlatLighting.LightSource`1/LightBag<FlatLighting.DirectionalLight>
struct LightBag_t2305681159;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// TMPro.TextMeshPro
struct TextMeshPro_t2521834357;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t2328801282;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1301679762;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;




#ifndef U3CMODULEU3E_T3783534237_H
#define U3CMODULEU3E_T3783534237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534237 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534237_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSTARTREMOTEDISCOVERYU3EC__ANONSTOREY0_T1165427101_H
#define U3CSTARTREMOTEDISCOVERYU3EC__ANONSTOREY0_T1165427101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/<StartRemoteDiscovery>c__AnonStorey0
struct  U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1165427101  : public RuntimeObject
{
public:
	// System.Action`1<Remote> RemoteManager/<StartRemoteDiscovery>c__AnonStorey0::action
	Action_1_t462642944 * ___action_0;
	// RemoteManager RemoteManager/<StartRemoteDiscovery>c__AnonStorey0::$this
	RemoteManager_t2208998541 * ___U24this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1165427101, ___action_0)); }
	inline Action_1_t462642944 * get_action_0() const { return ___action_0; }
	inline Action_1_t462642944 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t462642944 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1165427101, ___U24this_1)); }
	inline RemoteManager_t2208998541 * get_U24this_1() const { return ___U24this_1; }
	inline RemoteManager_t2208998541 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RemoteManager_t2208998541 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTREMOTEDISCOVERYU3EC__ANONSTOREY0_T1165427101_H
#ifndef U3CCHECKFORCHANGEU3EC__ITERATOR0_T136193089_H
#define U3CCHECKFORCHANGEU3EC__ITERATOR0_T136193089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceOrientationChange/<CheckForChange>c__Iterator0
struct  U3CCheckForChangeU3Ec__Iterator0_t136193089  : public RuntimeObject
{
public:
	// System.Object DeviceOrientationChange/<CheckForChange>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean DeviceOrientationChange/<CheckForChange>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 DeviceOrientationChange/<CheckForChange>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ec__Iterator0_t136193089, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ec__Iterator0_t136193089, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ec__Iterator0_t136193089, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORCHANGEU3EC__ITERATOR0_T136193089_H
#ifndef REMOTETOUCHINPUT_T3826108381_H
#define REMOTETOUCHINPUT_T3826108381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchInput
struct  RemoteTouchInput_t3826108381  : public RuntimeObject
{
public:
	// RemoteTouchInput/RemoteTouchInputEventHandler RemoteTouchInput::OnActiveChanged
	RemoteTouchInputEventHandler_t1375955781 * ___OnActiveChanged_0;
	// System.Boolean RemoteTouchInput::_isActive
	bool ____isActive_1;

public:
	inline static int32_t get_offset_of_OnActiveChanged_0() { return static_cast<int32_t>(offsetof(RemoteTouchInput_t3826108381, ___OnActiveChanged_0)); }
	inline RemoteTouchInputEventHandler_t1375955781 * get_OnActiveChanged_0() const { return ___OnActiveChanged_0; }
	inline RemoteTouchInputEventHandler_t1375955781 ** get_address_of_OnActiveChanged_0() { return &___OnActiveChanged_0; }
	inline void set_OnActiveChanged_0(RemoteTouchInputEventHandler_t1375955781 * value)
	{
		___OnActiveChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnActiveChanged_0), value);
	}

	inline static int32_t get_offset_of__isActive_1() { return static_cast<int32_t>(offsetof(RemoteTouchInput_t3826108381, ____isActive_1)); }
	inline bool get__isActive_1() const { return ____isActive_1; }
	inline bool* get_address_of__isActive_1() { return &____isActive_1; }
	inline void set__isActive_1(bool value)
	{
		____isActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHINPUT_T3826108381_H
#ifndef REMOTEORIENTATIONINPUT_T3303200544_H
#define REMOTEORIENTATIONINPUT_T3303200544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteOrientationInput
struct  RemoteOrientationInput_t3303200544  : public RuntimeObject
{
public:
	// RemoteOrientationInput/RemoteOrientationInputEventHandler RemoteOrientationInput::OnValueChanged
	RemoteOrientationInputEventHandler_t4128243105 * ___OnValueChanged_0;
	// System.Single RemoteOrientationInput::<pitch>k__BackingField
	float ___U3CpitchU3Ek__BackingField_1;
	// System.Single RemoteOrientationInput::<yaw>k__BackingField
	float ___U3CyawU3Ek__BackingField_2;
	// System.Single RemoteOrientationInput::<roll>k__BackingField
	float ___U3CrollU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___OnValueChanged_0)); }
	inline RemoteOrientationInputEventHandler_t4128243105 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteOrientationInputEventHandler_t4128243105 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteOrientationInputEventHandler_t4128243105 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of_U3CpitchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___U3CpitchU3Ek__BackingField_1)); }
	inline float get_U3CpitchU3Ek__BackingField_1() const { return ___U3CpitchU3Ek__BackingField_1; }
	inline float* get_address_of_U3CpitchU3Ek__BackingField_1() { return &___U3CpitchU3Ek__BackingField_1; }
	inline void set_U3CpitchU3Ek__BackingField_1(float value)
	{
		___U3CpitchU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CyawU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___U3CyawU3Ek__BackingField_2)); }
	inline float get_U3CyawU3Ek__BackingField_2() const { return ___U3CyawU3Ek__BackingField_2; }
	inline float* get_address_of_U3CyawU3Ek__BackingField_2() { return &___U3CyawU3Ek__BackingField_2; }
	inline void set_U3CyawU3Ek__BackingField_2(float value)
	{
		___U3CyawU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CrollU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___U3CrollU3Ek__BackingField_3)); }
	inline float get_U3CrollU3Ek__BackingField_3() const { return ___U3CrollU3Ek__BackingField_3; }
	inline float* get_address_of_U3CrollU3Ek__BackingField_3() { return &___U3CrollU3Ek__BackingField_3; }
	inline void set_U3CrollU3Ek__BackingField_3(float value)
	{
		___U3CrollU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEORIENTATIONINPUT_T3303200544_H
#ifndef U3CREMOTEMANAGERAUTOMATICALLYCONNECTSTOPREVIOUSCONNECTEDREMOTEU3EC__ANONSTOREY1_T3515152649_H
#define U3CREMOTEMANAGERAUTOMATICALLYCONNECTSTOPREVIOUSCONNECTEDREMOTEU3EC__ANONSTOREY1_T3515152649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerAutomaticallyConnectsToPreviousConnectedRemote>c__AnonStorey1
struct  U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t3515152649  : public RuntimeObject
{
public:
	// System.Boolean NativeBridge/<RemoteManagerAutomaticallyConnectsToPreviousConnectedRemote>c__AnonStorey1::value
	bool ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t3515152649, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERAUTOMATICALLYCONNECTSTOPREVIOUSCONNECTEDREMOTEU3EC__ANONSTOREY1_T3515152649_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef SHADER_T1450904180_H
#define SHADER_T1450904180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.Constants/Shader
struct  Shader_t1450904180  : public RuntimeObject
{
public:

public:
};

struct Shader_t1450904180_StaticFields
{
public:
	// System.String FlatLighting.Constants/Shader::AXIS_COLORS_LOCAL
	String_t* ___AXIS_COLORS_LOCAL_0;
	// System.String FlatLighting.Constants/Shader::AXIS_COLORS_GLOBAL
	String_t* ___AXIS_COLORS_GLOBAL_1;
	// System.String FlatLighting.Constants/Shader::SYMETRIC_COLORS_ON_KEYWORD
	String_t* ___SYMETRIC_COLORS_ON_KEYWORD_2;
	// System.String FlatLighting.Constants/Shader::SYMETRIC_COLORS_OFF_KEYWORD
	String_t* ___SYMETRIC_COLORS_OFF_KEYWORD_3;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_ON_X_KEYWORD
	String_t* ___AXIS_GRADIENT_ON_X_KEYWORD_4;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_ON_Y_KEYWORD
	String_t* ___AXIS_GRADIENT_ON_Y_KEYWORD_5;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_ON_Z_KEYWORD
	String_t* ___AXIS_GRADIENT_ON_Z_KEYWORD_6;
	// System.String FlatLighting.Constants/Shader::AXIS_GRADIENT_OFF_KEYWORD
	String_t* ___AXIS_GRADIENT_OFF_KEYWORD_7;
	// System.String FlatLighting.Constants/Shader::VERTEX_COLOR_KEYWORD
	String_t* ___VERTEX_COLOR_KEYWORD_8;
	// System.String FlatLighting.Constants/Shader::AMBIENT_LIGHT_KEYWORD
	String_t* ___AMBIENT_LIGHT_KEYWORD_9;
	// System.String FlatLighting.Constants/Shader::DIRECT_LIGHT_KEYWORD
	String_t* ___DIRECT_LIGHT_KEYWORD_10;
	// System.String FlatLighting.Constants/Shader::SPOT_LIGHT_KEYWORD
	String_t* ___SPOT_LIGHT_KEYWORD_11;
	// System.String FlatLighting.Constants/Shader::POINT_LIGHT_KEYWORD
	String_t* ___POINT_LIGHT_KEYWORD_12;
	// System.String FlatLighting.Constants/Shader::BLEND_LIGHT_SOURCES_KEYWORD
	String_t* ___BLEND_LIGHT_SOURCES_KEYWORD_13;
	// System.String FlatLighting.Constants/Shader::GRADIENT_LOCAL_KEYWORD
	String_t* ___GRADIENT_LOCAL_KEYWORD_14;
	// System.String FlatLighting.Constants/Shader::GRADIENT_WORLD_KEYWORD
	String_t* ___GRADIENT_WORLD_KEYWORD_15;
	// System.String FlatLighting.Constants/Shader::CUSTOM_LIGHTMAPPING_KEYWORD
	String_t* ___CUSTOM_LIGHTMAPPING_KEYWORD_16;
	// System.String FlatLighting.Constants/Shader::UNITY_LIGHTMAPPING_KEYWORD
	String_t* ___UNITY_LIGHTMAPPING_KEYWORD_17;
	// System.String FlatLighting.Constants/Shader::RECEIVE_CUSTOM_SHADOW_KEYWORD
	String_t* ___RECEIVE_CUSTOM_SHADOW_KEYWORD_18;
	// System.String FlatLighting.Constants/Shader::CAST_CUSTOM_SHADOW_ON_KEYWORD
	String_t* ___CAST_CUSTOM_SHADOW_ON_KEYWORD_19;
	// System.String FlatLighting.Constants/Shader::CAST_CUSTOM_SHADOW_OFF_KEYWORD
	String_t* ___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20;
	// System.String FlatLighting.Constants/Shader::USE_MAIN_TEXTURE_KEYWORD
	String_t* ___USE_MAIN_TEXTURE_KEYWORD_21;
	// System.String FlatLighting.Constants/Shader::LightPositiveX
	String_t* ___LightPositiveX_22;
	// System.String FlatLighting.Constants/Shader::LightPositiveY
	String_t* ___LightPositiveY_23;
	// System.String FlatLighting.Constants/Shader::LightPositiveZ
	String_t* ___LightPositiveZ_24;
	// System.String FlatLighting.Constants/Shader::LightNegativeX
	String_t* ___LightNegativeX_25;
	// System.String FlatLighting.Constants/Shader::LightNegativeY
	String_t* ___LightNegativeY_26;
	// System.String FlatLighting.Constants/Shader::LightNegativeZ
	String_t* ___LightNegativeZ_27;
	// System.String FlatLighting.Constants/Shader::LightPositive2X
	String_t* ___LightPositive2X_28;
	// System.String FlatLighting.Constants/Shader::LightPositive2Y
	String_t* ___LightPositive2Y_29;
	// System.String FlatLighting.Constants/Shader::LightPositive2Z
	String_t* ___LightPositive2Z_30;
	// System.String FlatLighting.Constants/Shader::LightNegative2X
	String_t* ___LightNegative2X_31;
	// System.String FlatLighting.Constants/Shader::LightNegative2Y
	String_t* ___LightNegative2Y_32;
	// System.String FlatLighting.Constants/Shader::LightNegative2Z
	String_t* ___LightNegative2Z_33;
	// System.String FlatLighting.Constants/Shader::GradientWidthPositiveX
	String_t* ___GradientWidthPositiveX_34;
	// System.String FlatLighting.Constants/Shader::GradientWidthPositiveY
	String_t* ___GradientWidthPositiveY_35;
	// System.String FlatLighting.Constants/Shader::GradientWidthPositiveZ
	String_t* ___GradientWidthPositiveZ_36;
	// System.String FlatLighting.Constants/Shader::GradientWidthNegativeX
	String_t* ___GradientWidthNegativeX_37;
	// System.String FlatLighting.Constants/Shader::GradientWidthNegativeY
	String_t* ___GradientWidthNegativeY_38;
	// System.String FlatLighting.Constants/Shader::GradientWidthNegativeZ
	String_t* ___GradientWidthNegativeZ_39;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetPositiveX
	String_t* ___GradientOriginOffsetPositiveX_40;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetPositiveY
	String_t* ___GradientOriginOffsetPositiveY_41;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetPositiveZ
	String_t* ___GradientOriginOffsetPositiveZ_42;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetNegativeX
	String_t* ___GradientOriginOffsetNegativeX_43;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetNegativeY
	String_t* ___GradientOriginOffsetNegativeY_44;
	// System.String FlatLighting.Constants/Shader::GradientOriginOffsetNegativeZ
	String_t* ___GradientOriginOffsetNegativeZ_45;

public:
	inline static int32_t get_offset_of_AXIS_COLORS_LOCAL_0() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AXIS_COLORS_LOCAL_0)); }
	inline String_t* get_AXIS_COLORS_LOCAL_0() const { return ___AXIS_COLORS_LOCAL_0; }
	inline String_t** get_address_of_AXIS_COLORS_LOCAL_0() { return &___AXIS_COLORS_LOCAL_0; }
	inline void set_AXIS_COLORS_LOCAL_0(String_t* value)
	{
		___AXIS_COLORS_LOCAL_0 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_COLORS_LOCAL_0), value);
	}

	inline static int32_t get_offset_of_AXIS_COLORS_GLOBAL_1() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AXIS_COLORS_GLOBAL_1)); }
	inline String_t* get_AXIS_COLORS_GLOBAL_1() const { return ___AXIS_COLORS_GLOBAL_1; }
	inline String_t** get_address_of_AXIS_COLORS_GLOBAL_1() { return &___AXIS_COLORS_GLOBAL_1; }
	inline void set_AXIS_COLORS_GLOBAL_1(String_t* value)
	{
		___AXIS_COLORS_GLOBAL_1 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_COLORS_GLOBAL_1), value);
	}

	inline static int32_t get_offset_of_SYMETRIC_COLORS_ON_KEYWORD_2() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___SYMETRIC_COLORS_ON_KEYWORD_2)); }
	inline String_t* get_SYMETRIC_COLORS_ON_KEYWORD_2() const { return ___SYMETRIC_COLORS_ON_KEYWORD_2; }
	inline String_t** get_address_of_SYMETRIC_COLORS_ON_KEYWORD_2() { return &___SYMETRIC_COLORS_ON_KEYWORD_2; }
	inline void set_SYMETRIC_COLORS_ON_KEYWORD_2(String_t* value)
	{
		___SYMETRIC_COLORS_ON_KEYWORD_2 = value;
		Il2CppCodeGenWriteBarrier((&___SYMETRIC_COLORS_ON_KEYWORD_2), value);
	}

	inline static int32_t get_offset_of_SYMETRIC_COLORS_OFF_KEYWORD_3() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___SYMETRIC_COLORS_OFF_KEYWORD_3)); }
	inline String_t* get_SYMETRIC_COLORS_OFF_KEYWORD_3() const { return ___SYMETRIC_COLORS_OFF_KEYWORD_3; }
	inline String_t** get_address_of_SYMETRIC_COLORS_OFF_KEYWORD_3() { return &___SYMETRIC_COLORS_OFF_KEYWORD_3; }
	inline void set_SYMETRIC_COLORS_OFF_KEYWORD_3(String_t* value)
	{
		___SYMETRIC_COLORS_OFF_KEYWORD_3 = value;
		Il2CppCodeGenWriteBarrier((&___SYMETRIC_COLORS_OFF_KEYWORD_3), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_ON_X_KEYWORD_4() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AXIS_GRADIENT_ON_X_KEYWORD_4)); }
	inline String_t* get_AXIS_GRADIENT_ON_X_KEYWORD_4() const { return ___AXIS_GRADIENT_ON_X_KEYWORD_4; }
	inline String_t** get_address_of_AXIS_GRADIENT_ON_X_KEYWORD_4() { return &___AXIS_GRADIENT_ON_X_KEYWORD_4; }
	inline void set_AXIS_GRADIENT_ON_X_KEYWORD_4(String_t* value)
	{
		___AXIS_GRADIENT_ON_X_KEYWORD_4 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_ON_X_KEYWORD_4), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_ON_Y_KEYWORD_5() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AXIS_GRADIENT_ON_Y_KEYWORD_5)); }
	inline String_t* get_AXIS_GRADIENT_ON_Y_KEYWORD_5() const { return ___AXIS_GRADIENT_ON_Y_KEYWORD_5; }
	inline String_t** get_address_of_AXIS_GRADIENT_ON_Y_KEYWORD_5() { return &___AXIS_GRADIENT_ON_Y_KEYWORD_5; }
	inline void set_AXIS_GRADIENT_ON_Y_KEYWORD_5(String_t* value)
	{
		___AXIS_GRADIENT_ON_Y_KEYWORD_5 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_ON_Y_KEYWORD_5), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_ON_Z_KEYWORD_6() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AXIS_GRADIENT_ON_Z_KEYWORD_6)); }
	inline String_t* get_AXIS_GRADIENT_ON_Z_KEYWORD_6() const { return ___AXIS_GRADIENT_ON_Z_KEYWORD_6; }
	inline String_t** get_address_of_AXIS_GRADIENT_ON_Z_KEYWORD_6() { return &___AXIS_GRADIENT_ON_Z_KEYWORD_6; }
	inline void set_AXIS_GRADIENT_ON_Z_KEYWORD_6(String_t* value)
	{
		___AXIS_GRADIENT_ON_Z_KEYWORD_6 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_ON_Z_KEYWORD_6), value);
	}

	inline static int32_t get_offset_of_AXIS_GRADIENT_OFF_KEYWORD_7() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AXIS_GRADIENT_OFF_KEYWORD_7)); }
	inline String_t* get_AXIS_GRADIENT_OFF_KEYWORD_7() const { return ___AXIS_GRADIENT_OFF_KEYWORD_7; }
	inline String_t** get_address_of_AXIS_GRADIENT_OFF_KEYWORD_7() { return &___AXIS_GRADIENT_OFF_KEYWORD_7; }
	inline void set_AXIS_GRADIENT_OFF_KEYWORD_7(String_t* value)
	{
		___AXIS_GRADIENT_OFF_KEYWORD_7 = value;
		Il2CppCodeGenWriteBarrier((&___AXIS_GRADIENT_OFF_KEYWORD_7), value);
	}

	inline static int32_t get_offset_of_VERTEX_COLOR_KEYWORD_8() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___VERTEX_COLOR_KEYWORD_8)); }
	inline String_t* get_VERTEX_COLOR_KEYWORD_8() const { return ___VERTEX_COLOR_KEYWORD_8; }
	inline String_t** get_address_of_VERTEX_COLOR_KEYWORD_8() { return &___VERTEX_COLOR_KEYWORD_8; }
	inline void set_VERTEX_COLOR_KEYWORD_8(String_t* value)
	{
		___VERTEX_COLOR_KEYWORD_8 = value;
		Il2CppCodeGenWriteBarrier((&___VERTEX_COLOR_KEYWORD_8), value);
	}

	inline static int32_t get_offset_of_AMBIENT_LIGHT_KEYWORD_9() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___AMBIENT_LIGHT_KEYWORD_9)); }
	inline String_t* get_AMBIENT_LIGHT_KEYWORD_9() const { return ___AMBIENT_LIGHT_KEYWORD_9; }
	inline String_t** get_address_of_AMBIENT_LIGHT_KEYWORD_9() { return &___AMBIENT_LIGHT_KEYWORD_9; }
	inline void set_AMBIENT_LIGHT_KEYWORD_9(String_t* value)
	{
		___AMBIENT_LIGHT_KEYWORD_9 = value;
		Il2CppCodeGenWriteBarrier((&___AMBIENT_LIGHT_KEYWORD_9), value);
	}

	inline static int32_t get_offset_of_DIRECT_LIGHT_KEYWORD_10() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___DIRECT_LIGHT_KEYWORD_10)); }
	inline String_t* get_DIRECT_LIGHT_KEYWORD_10() const { return ___DIRECT_LIGHT_KEYWORD_10; }
	inline String_t** get_address_of_DIRECT_LIGHT_KEYWORD_10() { return &___DIRECT_LIGHT_KEYWORD_10; }
	inline void set_DIRECT_LIGHT_KEYWORD_10(String_t* value)
	{
		___DIRECT_LIGHT_KEYWORD_10 = value;
		Il2CppCodeGenWriteBarrier((&___DIRECT_LIGHT_KEYWORD_10), value);
	}

	inline static int32_t get_offset_of_SPOT_LIGHT_KEYWORD_11() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___SPOT_LIGHT_KEYWORD_11)); }
	inline String_t* get_SPOT_LIGHT_KEYWORD_11() const { return ___SPOT_LIGHT_KEYWORD_11; }
	inline String_t** get_address_of_SPOT_LIGHT_KEYWORD_11() { return &___SPOT_LIGHT_KEYWORD_11; }
	inline void set_SPOT_LIGHT_KEYWORD_11(String_t* value)
	{
		___SPOT_LIGHT_KEYWORD_11 = value;
		Il2CppCodeGenWriteBarrier((&___SPOT_LIGHT_KEYWORD_11), value);
	}

	inline static int32_t get_offset_of_POINT_LIGHT_KEYWORD_12() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___POINT_LIGHT_KEYWORD_12)); }
	inline String_t* get_POINT_LIGHT_KEYWORD_12() const { return ___POINT_LIGHT_KEYWORD_12; }
	inline String_t** get_address_of_POINT_LIGHT_KEYWORD_12() { return &___POINT_LIGHT_KEYWORD_12; }
	inline void set_POINT_LIGHT_KEYWORD_12(String_t* value)
	{
		___POINT_LIGHT_KEYWORD_12 = value;
		Il2CppCodeGenWriteBarrier((&___POINT_LIGHT_KEYWORD_12), value);
	}

	inline static int32_t get_offset_of_BLEND_LIGHT_SOURCES_KEYWORD_13() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___BLEND_LIGHT_SOURCES_KEYWORD_13)); }
	inline String_t* get_BLEND_LIGHT_SOURCES_KEYWORD_13() const { return ___BLEND_LIGHT_SOURCES_KEYWORD_13; }
	inline String_t** get_address_of_BLEND_LIGHT_SOURCES_KEYWORD_13() { return &___BLEND_LIGHT_SOURCES_KEYWORD_13; }
	inline void set_BLEND_LIGHT_SOURCES_KEYWORD_13(String_t* value)
	{
		___BLEND_LIGHT_SOURCES_KEYWORD_13 = value;
		Il2CppCodeGenWriteBarrier((&___BLEND_LIGHT_SOURCES_KEYWORD_13), value);
	}

	inline static int32_t get_offset_of_GRADIENT_LOCAL_KEYWORD_14() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GRADIENT_LOCAL_KEYWORD_14)); }
	inline String_t* get_GRADIENT_LOCAL_KEYWORD_14() const { return ___GRADIENT_LOCAL_KEYWORD_14; }
	inline String_t** get_address_of_GRADIENT_LOCAL_KEYWORD_14() { return &___GRADIENT_LOCAL_KEYWORD_14; }
	inline void set_GRADIENT_LOCAL_KEYWORD_14(String_t* value)
	{
		___GRADIENT_LOCAL_KEYWORD_14 = value;
		Il2CppCodeGenWriteBarrier((&___GRADIENT_LOCAL_KEYWORD_14), value);
	}

	inline static int32_t get_offset_of_GRADIENT_WORLD_KEYWORD_15() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GRADIENT_WORLD_KEYWORD_15)); }
	inline String_t* get_GRADIENT_WORLD_KEYWORD_15() const { return ___GRADIENT_WORLD_KEYWORD_15; }
	inline String_t** get_address_of_GRADIENT_WORLD_KEYWORD_15() { return &___GRADIENT_WORLD_KEYWORD_15; }
	inline void set_GRADIENT_WORLD_KEYWORD_15(String_t* value)
	{
		___GRADIENT_WORLD_KEYWORD_15 = value;
		Il2CppCodeGenWriteBarrier((&___GRADIENT_WORLD_KEYWORD_15), value);
	}

	inline static int32_t get_offset_of_CUSTOM_LIGHTMAPPING_KEYWORD_16() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___CUSTOM_LIGHTMAPPING_KEYWORD_16)); }
	inline String_t* get_CUSTOM_LIGHTMAPPING_KEYWORD_16() const { return ___CUSTOM_LIGHTMAPPING_KEYWORD_16; }
	inline String_t** get_address_of_CUSTOM_LIGHTMAPPING_KEYWORD_16() { return &___CUSTOM_LIGHTMAPPING_KEYWORD_16; }
	inline void set_CUSTOM_LIGHTMAPPING_KEYWORD_16(String_t* value)
	{
		___CUSTOM_LIGHTMAPPING_KEYWORD_16 = value;
		Il2CppCodeGenWriteBarrier((&___CUSTOM_LIGHTMAPPING_KEYWORD_16), value);
	}

	inline static int32_t get_offset_of_UNITY_LIGHTMAPPING_KEYWORD_17() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___UNITY_LIGHTMAPPING_KEYWORD_17)); }
	inline String_t* get_UNITY_LIGHTMAPPING_KEYWORD_17() const { return ___UNITY_LIGHTMAPPING_KEYWORD_17; }
	inline String_t** get_address_of_UNITY_LIGHTMAPPING_KEYWORD_17() { return &___UNITY_LIGHTMAPPING_KEYWORD_17; }
	inline void set_UNITY_LIGHTMAPPING_KEYWORD_17(String_t* value)
	{
		___UNITY_LIGHTMAPPING_KEYWORD_17 = value;
		Il2CppCodeGenWriteBarrier((&___UNITY_LIGHTMAPPING_KEYWORD_17), value);
	}

	inline static int32_t get_offset_of_RECEIVE_CUSTOM_SHADOW_KEYWORD_18() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___RECEIVE_CUSTOM_SHADOW_KEYWORD_18)); }
	inline String_t* get_RECEIVE_CUSTOM_SHADOW_KEYWORD_18() const { return ___RECEIVE_CUSTOM_SHADOW_KEYWORD_18; }
	inline String_t** get_address_of_RECEIVE_CUSTOM_SHADOW_KEYWORD_18() { return &___RECEIVE_CUSTOM_SHADOW_KEYWORD_18; }
	inline void set_RECEIVE_CUSTOM_SHADOW_KEYWORD_18(String_t* value)
	{
		___RECEIVE_CUSTOM_SHADOW_KEYWORD_18 = value;
		Il2CppCodeGenWriteBarrier((&___RECEIVE_CUSTOM_SHADOW_KEYWORD_18), value);
	}

	inline static int32_t get_offset_of_CAST_CUSTOM_SHADOW_ON_KEYWORD_19() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___CAST_CUSTOM_SHADOW_ON_KEYWORD_19)); }
	inline String_t* get_CAST_CUSTOM_SHADOW_ON_KEYWORD_19() const { return ___CAST_CUSTOM_SHADOW_ON_KEYWORD_19; }
	inline String_t** get_address_of_CAST_CUSTOM_SHADOW_ON_KEYWORD_19() { return &___CAST_CUSTOM_SHADOW_ON_KEYWORD_19; }
	inline void set_CAST_CUSTOM_SHADOW_ON_KEYWORD_19(String_t* value)
	{
		___CAST_CUSTOM_SHADOW_ON_KEYWORD_19 = value;
		Il2CppCodeGenWriteBarrier((&___CAST_CUSTOM_SHADOW_ON_KEYWORD_19), value);
	}

	inline static int32_t get_offset_of_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20)); }
	inline String_t* get_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20() const { return ___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20; }
	inline String_t** get_address_of_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20() { return &___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20; }
	inline void set_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20(String_t* value)
	{
		___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20 = value;
		Il2CppCodeGenWriteBarrier((&___CAST_CUSTOM_SHADOW_OFF_KEYWORD_20), value);
	}

	inline static int32_t get_offset_of_USE_MAIN_TEXTURE_KEYWORD_21() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___USE_MAIN_TEXTURE_KEYWORD_21)); }
	inline String_t* get_USE_MAIN_TEXTURE_KEYWORD_21() const { return ___USE_MAIN_TEXTURE_KEYWORD_21; }
	inline String_t** get_address_of_USE_MAIN_TEXTURE_KEYWORD_21() { return &___USE_MAIN_TEXTURE_KEYWORD_21; }
	inline void set_USE_MAIN_TEXTURE_KEYWORD_21(String_t* value)
	{
		___USE_MAIN_TEXTURE_KEYWORD_21 = value;
		Il2CppCodeGenWriteBarrier((&___USE_MAIN_TEXTURE_KEYWORD_21), value);
	}

	inline static int32_t get_offset_of_LightPositiveX_22() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightPositiveX_22)); }
	inline String_t* get_LightPositiveX_22() const { return ___LightPositiveX_22; }
	inline String_t** get_address_of_LightPositiveX_22() { return &___LightPositiveX_22; }
	inline void set_LightPositiveX_22(String_t* value)
	{
		___LightPositiveX_22 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositiveX_22), value);
	}

	inline static int32_t get_offset_of_LightPositiveY_23() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightPositiveY_23)); }
	inline String_t* get_LightPositiveY_23() const { return ___LightPositiveY_23; }
	inline String_t** get_address_of_LightPositiveY_23() { return &___LightPositiveY_23; }
	inline void set_LightPositiveY_23(String_t* value)
	{
		___LightPositiveY_23 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositiveY_23), value);
	}

	inline static int32_t get_offset_of_LightPositiveZ_24() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightPositiveZ_24)); }
	inline String_t* get_LightPositiveZ_24() const { return ___LightPositiveZ_24; }
	inline String_t** get_address_of_LightPositiveZ_24() { return &___LightPositiveZ_24; }
	inline void set_LightPositiveZ_24(String_t* value)
	{
		___LightPositiveZ_24 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositiveZ_24), value);
	}

	inline static int32_t get_offset_of_LightNegativeX_25() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightNegativeX_25)); }
	inline String_t* get_LightNegativeX_25() const { return ___LightNegativeX_25; }
	inline String_t** get_address_of_LightNegativeX_25() { return &___LightNegativeX_25; }
	inline void set_LightNegativeX_25(String_t* value)
	{
		___LightNegativeX_25 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegativeX_25), value);
	}

	inline static int32_t get_offset_of_LightNegativeY_26() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightNegativeY_26)); }
	inline String_t* get_LightNegativeY_26() const { return ___LightNegativeY_26; }
	inline String_t** get_address_of_LightNegativeY_26() { return &___LightNegativeY_26; }
	inline void set_LightNegativeY_26(String_t* value)
	{
		___LightNegativeY_26 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegativeY_26), value);
	}

	inline static int32_t get_offset_of_LightNegativeZ_27() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightNegativeZ_27)); }
	inline String_t* get_LightNegativeZ_27() const { return ___LightNegativeZ_27; }
	inline String_t** get_address_of_LightNegativeZ_27() { return &___LightNegativeZ_27; }
	inline void set_LightNegativeZ_27(String_t* value)
	{
		___LightNegativeZ_27 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegativeZ_27), value);
	}

	inline static int32_t get_offset_of_LightPositive2X_28() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightPositive2X_28)); }
	inline String_t* get_LightPositive2X_28() const { return ___LightPositive2X_28; }
	inline String_t** get_address_of_LightPositive2X_28() { return &___LightPositive2X_28; }
	inline void set_LightPositive2X_28(String_t* value)
	{
		___LightPositive2X_28 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositive2X_28), value);
	}

	inline static int32_t get_offset_of_LightPositive2Y_29() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightPositive2Y_29)); }
	inline String_t* get_LightPositive2Y_29() const { return ___LightPositive2Y_29; }
	inline String_t** get_address_of_LightPositive2Y_29() { return &___LightPositive2Y_29; }
	inline void set_LightPositive2Y_29(String_t* value)
	{
		___LightPositive2Y_29 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositive2Y_29), value);
	}

	inline static int32_t get_offset_of_LightPositive2Z_30() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightPositive2Z_30)); }
	inline String_t* get_LightPositive2Z_30() const { return ___LightPositive2Z_30; }
	inline String_t** get_address_of_LightPositive2Z_30() { return &___LightPositive2Z_30; }
	inline void set_LightPositive2Z_30(String_t* value)
	{
		___LightPositive2Z_30 = value;
		Il2CppCodeGenWriteBarrier((&___LightPositive2Z_30), value);
	}

	inline static int32_t get_offset_of_LightNegative2X_31() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightNegative2X_31)); }
	inline String_t* get_LightNegative2X_31() const { return ___LightNegative2X_31; }
	inline String_t** get_address_of_LightNegative2X_31() { return &___LightNegative2X_31; }
	inline void set_LightNegative2X_31(String_t* value)
	{
		___LightNegative2X_31 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegative2X_31), value);
	}

	inline static int32_t get_offset_of_LightNegative2Y_32() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightNegative2Y_32)); }
	inline String_t* get_LightNegative2Y_32() const { return ___LightNegative2Y_32; }
	inline String_t** get_address_of_LightNegative2Y_32() { return &___LightNegative2Y_32; }
	inline void set_LightNegative2Y_32(String_t* value)
	{
		___LightNegative2Y_32 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegative2Y_32), value);
	}

	inline static int32_t get_offset_of_LightNegative2Z_33() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___LightNegative2Z_33)); }
	inline String_t* get_LightNegative2Z_33() const { return ___LightNegative2Z_33; }
	inline String_t** get_address_of_LightNegative2Z_33() { return &___LightNegative2Z_33; }
	inline void set_LightNegative2Z_33(String_t* value)
	{
		___LightNegative2Z_33 = value;
		Il2CppCodeGenWriteBarrier((&___LightNegative2Z_33), value);
	}

	inline static int32_t get_offset_of_GradientWidthPositiveX_34() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientWidthPositiveX_34)); }
	inline String_t* get_GradientWidthPositiveX_34() const { return ___GradientWidthPositiveX_34; }
	inline String_t** get_address_of_GradientWidthPositiveX_34() { return &___GradientWidthPositiveX_34; }
	inline void set_GradientWidthPositiveX_34(String_t* value)
	{
		___GradientWidthPositiveX_34 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthPositiveX_34), value);
	}

	inline static int32_t get_offset_of_GradientWidthPositiveY_35() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientWidthPositiveY_35)); }
	inline String_t* get_GradientWidthPositiveY_35() const { return ___GradientWidthPositiveY_35; }
	inline String_t** get_address_of_GradientWidthPositiveY_35() { return &___GradientWidthPositiveY_35; }
	inline void set_GradientWidthPositiveY_35(String_t* value)
	{
		___GradientWidthPositiveY_35 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthPositiveY_35), value);
	}

	inline static int32_t get_offset_of_GradientWidthPositiveZ_36() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientWidthPositiveZ_36)); }
	inline String_t* get_GradientWidthPositiveZ_36() const { return ___GradientWidthPositiveZ_36; }
	inline String_t** get_address_of_GradientWidthPositiveZ_36() { return &___GradientWidthPositiveZ_36; }
	inline void set_GradientWidthPositiveZ_36(String_t* value)
	{
		___GradientWidthPositiveZ_36 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthPositiveZ_36), value);
	}

	inline static int32_t get_offset_of_GradientWidthNegativeX_37() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientWidthNegativeX_37)); }
	inline String_t* get_GradientWidthNegativeX_37() const { return ___GradientWidthNegativeX_37; }
	inline String_t** get_address_of_GradientWidthNegativeX_37() { return &___GradientWidthNegativeX_37; }
	inline void set_GradientWidthNegativeX_37(String_t* value)
	{
		___GradientWidthNegativeX_37 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthNegativeX_37), value);
	}

	inline static int32_t get_offset_of_GradientWidthNegativeY_38() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientWidthNegativeY_38)); }
	inline String_t* get_GradientWidthNegativeY_38() const { return ___GradientWidthNegativeY_38; }
	inline String_t** get_address_of_GradientWidthNegativeY_38() { return &___GradientWidthNegativeY_38; }
	inline void set_GradientWidthNegativeY_38(String_t* value)
	{
		___GradientWidthNegativeY_38 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthNegativeY_38), value);
	}

	inline static int32_t get_offset_of_GradientWidthNegativeZ_39() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientWidthNegativeZ_39)); }
	inline String_t* get_GradientWidthNegativeZ_39() const { return ___GradientWidthNegativeZ_39; }
	inline String_t** get_address_of_GradientWidthNegativeZ_39() { return &___GradientWidthNegativeZ_39; }
	inline void set_GradientWidthNegativeZ_39(String_t* value)
	{
		___GradientWidthNegativeZ_39 = value;
		Il2CppCodeGenWriteBarrier((&___GradientWidthNegativeZ_39), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetPositiveX_40() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientOriginOffsetPositiveX_40)); }
	inline String_t* get_GradientOriginOffsetPositiveX_40() const { return ___GradientOriginOffsetPositiveX_40; }
	inline String_t** get_address_of_GradientOriginOffsetPositiveX_40() { return &___GradientOriginOffsetPositiveX_40; }
	inline void set_GradientOriginOffsetPositiveX_40(String_t* value)
	{
		___GradientOriginOffsetPositiveX_40 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetPositiveX_40), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetPositiveY_41() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientOriginOffsetPositiveY_41)); }
	inline String_t* get_GradientOriginOffsetPositiveY_41() const { return ___GradientOriginOffsetPositiveY_41; }
	inline String_t** get_address_of_GradientOriginOffsetPositiveY_41() { return &___GradientOriginOffsetPositiveY_41; }
	inline void set_GradientOriginOffsetPositiveY_41(String_t* value)
	{
		___GradientOriginOffsetPositiveY_41 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetPositiveY_41), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetPositiveZ_42() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientOriginOffsetPositiveZ_42)); }
	inline String_t* get_GradientOriginOffsetPositiveZ_42() const { return ___GradientOriginOffsetPositiveZ_42; }
	inline String_t** get_address_of_GradientOriginOffsetPositiveZ_42() { return &___GradientOriginOffsetPositiveZ_42; }
	inline void set_GradientOriginOffsetPositiveZ_42(String_t* value)
	{
		___GradientOriginOffsetPositiveZ_42 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetPositiveZ_42), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetNegativeX_43() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientOriginOffsetNegativeX_43)); }
	inline String_t* get_GradientOriginOffsetNegativeX_43() const { return ___GradientOriginOffsetNegativeX_43; }
	inline String_t** get_address_of_GradientOriginOffsetNegativeX_43() { return &___GradientOriginOffsetNegativeX_43; }
	inline void set_GradientOriginOffsetNegativeX_43(String_t* value)
	{
		___GradientOriginOffsetNegativeX_43 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetNegativeX_43), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetNegativeY_44() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientOriginOffsetNegativeY_44)); }
	inline String_t* get_GradientOriginOffsetNegativeY_44() const { return ___GradientOriginOffsetNegativeY_44; }
	inline String_t** get_address_of_GradientOriginOffsetNegativeY_44() { return &___GradientOriginOffsetNegativeY_44; }
	inline void set_GradientOriginOffsetNegativeY_44(String_t* value)
	{
		___GradientOriginOffsetNegativeY_44 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetNegativeY_44), value);
	}

	inline static int32_t get_offset_of_GradientOriginOffsetNegativeZ_45() { return static_cast<int32_t>(offsetof(Shader_t1450904180_StaticFields, ___GradientOriginOffsetNegativeZ_45)); }
	inline String_t* get_GradientOriginOffsetNegativeZ_45() const { return ___GradientOriginOffsetNegativeZ_45; }
	inline String_t** get_address_of_GradientOriginOffsetNegativeZ_45() { return &___GradientOriginOffsetNegativeZ_45; }
	inline void set_GradientOriginOffsetNegativeZ_45(String_t* value)
	{
		___GradientOriginOffsetNegativeZ_45 = value;
		Il2CppCodeGenWriteBarrier((&___GradientOriginOffsetNegativeZ_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T1450904180_H
#ifndef MIRABASEPOINTER_T885132991_H
#define MIRABASEPOINTER_T885132991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBasePointer
struct  MiraBasePointer_t885132991  : public RuntimeObject
{
public:
	// System.Single MiraBasePointer::<maxDistance>k__BackingField
	float ___U3CmaxDistanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmaxDistanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MiraBasePointer_t885132991, ___U3CmaxDistanceU3Ek__BackingField_0)); }
	inline float get_U3CmaxDistanceU3Ek__BackingField_0() const { return ___U3CmaxDistanceU3Ek__BackingField_0; }
	inline float* get_address_of_U3CmaxDistanceU3Ek__BackingField_0() { return &___U3CmaxDistanceU3Ek__BackingField_0; }
	inline void set_U3CmaxDistanceU3Ek__BackingField_0(float value)
	{
		___U3CmaxDistanceU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASEPOINTER_T885132991_H
#ifndef U3CCHECKREMOTESU3EC__ITERATOR1_T3747014210_H
#define U3CCHECKREMOTESU3EC__ITERATOR1_T3747014210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectToPreviousRemote/<CheckRemotes>c__Iterator1
struct  U3CCheckRemotesU3Ec__Iterator1_t3747014210  : public RuntimeObject
{
public:
	// ConnectToPreviousRemote ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$this
	ConnectToPreviousRemote_t1323906560 * ___U24this_0;
	// System.Object ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t3747014210, ___U24this_0)); }
	inline ConnectToPreviousRemote_t1323906560 * get_U24this_0() const { return ___U24this_0; }
	inline ConnectToPreviousRemote_t1323906560 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ConnectToPreviousRemote_t1323906560 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t3747014210, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t3747014210, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t3747014210, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

struct U3CCheckRemotesU3Ec__Iterator1_t3747014210_StaticFields
{
public:
	// System.Action`1<MiraRemoteException> ConnectToPreviousRemote/<CheckRemotes>c__Iterator1::<>f__am$cache0
	Action_1_t3189573364 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(U3CCheckRemotesU3Ec__Iterator1_t3747014210_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_1_t3189573364 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_1_t3189573364 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_1_t3189573364 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKREMOTESU3EC__ITERATOR1_T3747014210_H
#ifndef U3CAUTOCONNECTLASTREMOTEU3EC__ITERATOR0_T1086687105_H
#define U3CAUTOCONNECTLASTREMOTEU3EC__ITERATOR0_T1086687105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0
struct  U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105  : public RuntimeObject
{
public:
	// ConnectToPreviousRemote ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$this
	ConnectToPreviousRemote_t1323906560 * ___U24this_0;
	// System.Object ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ConnectToPreviousRemote/<AutoConnectLastRemote>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105, ___U24this_0)); }
	inline ConnectToPreviousRemote_t1323906560 * get_U24this_0() const { return ___U24this_0; }
	inline ConnectToPreviousRemote_t1323906560 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ConnectToPreviousRemote_t1323906560 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOCONNECTLASTREMOTEU3EC__ITERATOR0_T1086687105_H
#ifndef U3CSTARTCONTROLLERU3EC__ITERATOR0_T3030372693_H
#define U3CSTARTCONTROLLERU3EC__ITERATOR0_T3030372693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<StartController>c__Iterator0
struct  U3CStartControllerU3Ec__Iterator0_t3030372693  : public RuntimeObject
{
public:
	// MiraController MiraController/<StartController>c__Iterator0::$this
	MiraController_t1058876281 * ___U24this_0;
	// System.Object MiraController/<StartController>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MiraController/<StartController>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MiraController/<StartController>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3030372693, ___U24this_0)); }
	inline MiraController_t1058876281 * get_U24this_0() const { return ___U24this_0; }
	inline MiraController_t1058876281 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MiraController_t1058876281 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3030372693, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3030372693, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartControllerU3Ec__Iterator0_t3030372693, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTCONTROLLERU3EC__ITERATOR0_T3030372693_H
#ifndef U3CDELAYEDRECENTERU3EC__ITERATOR1_T3807338448_H
#define U3CDELAYEDRECENTERU3EC__ITERATOR1_T3807338448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<DelayedRecenter>c__Iterator1
struct  U3CDelayedRecenterU3Ec__Iterator1_t3807338448  : public RuntimeObject
{
public:
	// MiraController MiraController/<DelayedRecenter>c__Iterator1::$this
	MiraController_t1058876281 * ___U24this_0;
	// System.Object MiraController/<DelayedRecenter>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MiraController/<DelayedRecenter>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 MiraController/<DelayedRecenter>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3807338448, ___U24this_0)); }
	inline MiraController_t1058876281 * get_U24this_0() const { return ___U24this_0; }
	inline MiraController_t1058876281 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MiraController_t1058876281 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3807338448, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3807338448, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedRecenterU3Ec__Iterator1_t3807338448, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDRECENTERU3EC__ITERATOR1_T3807338448_H
#ifndef U3CRETURNTOHOMEU3EC__ITERATOR2_T3855721357_H
#define U3CRETURNTOHOMEU3EC__ITERATOR2_T3855721357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/<ReturnToHome>c__Iterator2
struct  U3CReturnToHomeU3Ec__Iterator2_t3855721357  : public RuntimeObject
{
public:
	// System.Single MiraController/<ReturnToHome>c__Iterator2::<timer>__0
	float ___U3CtimerU3E__0_0;
	// System.Boolean MiraController/<ReturnToHome>c__Iterator2::<animStarted>__0
	bool ___U3CanimStartedU3E__0_1;
	// System.Single MiraController/<ReturnToHome>c__Iterator2::<animProgress>__0
	float ___U3CanimProgressU3E__0_2;
	// MiraController MiraController/<ReturnToHome>c__Iterator2::$this
	MiraController_t1058876281 * ___U24this_3;
	// System.Object MiraController/<ReturnToHome>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MiraController/<ReturnToHome>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 MiraController/<ReturnToHome>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U3CtimerU3E__0_0)); }
	inline float get_U3CtimerU3E__0_0() const { return ___U3CtimerU3E__0_0; }
	inline float* get_address_of_U3CtimerU3E__0_0() { return &___U3CtimerU3E__0_0; }
	inline void set_U3CtimerU3E__0_0(float value)
	{
		___U3CtimerU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CanimStartedU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U3CanimStartedU3E__0_1)); }
	inline bool get_U3CanimStartedU3E__0_1() const { return ___U3CanimStartedU3E__0_1; }
	inline bool* get_address_of_U3CanimStartedU3E__0_1() { return &___U3CanimStartedU3E__0_1; }
	inline void set_U3CanimStartedU3E__0_1(bool value)
	{
		___U3CanimStartedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CanimProgressU3E__0_2() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U3CanimProgressU3E__0_2)); }
	inline float get_U3CanimProgressU3E__0_2() const { return ___U3CanimProgressU3E__0_2; }
	inline float* get_address_of_U3CanimProgressU3E__0_2() { return &___U3CanimProgressU3E__0_2; }
	inline void set_U3CanimProgressU3E__0_2(float value)
	{
		___U3CanimProgressU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U24this_3)); }
	inline MiraController_t1058876281 * get_U24this_3() const { return ___U24this_3; }
	inline MiraController_t1058876281 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MiraController_t1058876281 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CReturnToHomeU3Ec__Iterator2_t3855721357, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRETURNTOHOMEU3EC__ITERATOR2_T3855721357_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef NATIVEBRIDGE_T2060501782_H
#define NATIVEBRIDGE_T2060501782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge
struct  NativeBridge_t2060501782  : public RuntimeObject
{
public:

public:
};

struct NativeBridge_t2060501782_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Action> NativeBridge::emptyActions
	Dictionary_2_t846283718 * ___emptyActions_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Boolean>> NativeBridge::boolActions
	Dictionary_2_t1247186066 * ___boolActions_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Int32>> NativeBridge::intActions
	Dictionary_2_t3788456092 * ___intActions_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.Single>> NativeBridge::floatActions
	Dictionary_2_t3793088576 * ___floatActions_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<MiraRemoteException>> NativeBridge::exceptionActions
	Dictionary_2_t809385330 * ___exceptionActions_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<Remote>> NativeBridge::remoteActions
	Dictionary_2_t2377422206 * ___remoteActions_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.Single,System.Single,System.Single>> NativeBridge::remoteMotionActions
	Dictionary_2_t4078021086 * ___remoteMotionActions_6;
	// NativeBridge/MRIntCallback NativeBridge::<>f__mg$cache0
	MRIntCallback_t2215762430 * ___U3CU3Ef__mgU24cache0_7;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cache1
	MRBoolCallback_t2221477487 * ___U3CU3Ef__mgU24cache1_8;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cache2
	MRBoolCallback_t2221477487 * ___U3CU3Ef__mgU24cache2_9;
	// System.Action`1<System.Boolean> NativeBridge::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_10;
	// NativeBridge/MRDiscoveredRemoteCallback NativeBridge::<>f__mg$cache3
	MRDiscoveredRemoteCallback_t3604605211 * ___U3CU3Ef__mgU24cache3_11;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache4
	MRErrorCallback_t1972775801 * ___U3CU3Ef__mgU24cache4_12;
	// NativeBridge/MRConnectedRemoteCallback NativeBridge::<>f__mg$cache5
	MRConnectedRemoteCallback_t3085918786 * ___U3CU3Ef__mgU24cache5_13;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache6
	MRErrorCallback_t1972775801 * ___U3CU3Ef__mgU24cache6_14;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache7
	MRErrorCallback_t1972775801 * ___U3CU3Ef__mgU24cache7_15;
	// NativeBridge/MRConnectedRemoteCallback NativeBridge::<>f__mg$cache8
	MRConnectedRemoteCallback_t3085918786 * ___U3CU3Ef__mgU24cache8_16;
	// NativeBridge/MRErrorCallback NativeBridge::<>f__mg$cache9
	MRErrorCallback_t1972775801 * ___U3CU3Ef__mgU24cache9_17;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cacheA
	MRBoolCallback_t2221477487 * ___U3CU3Ef__mgU24cacheA_18;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cacheB
	MRBoolCallback_t2221477487 * ___U3CU3Ef__mgU24cacheB_19;
	// NativeBridge/MRFloatCallback NativeBridge::<>f__mg$cacheC
	MRFloatCallback_t3699197057 * ___U3CU3Ef__mgU24cacheC_20;
	// NativeBridge/MRBoolCallback NativeBridge::<>f__mg$cacheD
	MRBoolCallback_t2221477487 * ___U3CU3Ef__mgU24cacheD_21;
	// NativeBridge/MREmptyCallback NativeBridge::<>f__mg$cacheE
	MREmptyCallback_t3969082922 * ___U3CU3Ef__mgU24cacheE_22;
	// NativeBridge/MRRemoteMotionCallback NativeBridge::<>f__mg$cacheF
	MRRemoteMotionCallback_t3634481787 * ___U3CU3Ef__mgU24cacheF_23;
	// NativeBridge/MRRemoteMotionCallback NativeBridge::<>f__mg$cache10
	MRRemoteMotionCallback_t3634481787 * ___U3CU3Ef__mgU24cache10_24;
	// NativeBridge/MRConnectedRemoteCallback NativeBridge::<>f__mg$cache11
	MRConnectedRemoteCallback_t3085918786 * ___U3CU3Ef__mgU24cache11_25;
	// NativeBridge/MRDiscoveredRemoteCallback NativeBridge::<>f__mg$cache12
	MRDiscoveredRemoteCallback_t3604605211 * ___U3CU3Ef__mgU24cache12_26;
	// NativeBridge/MRDiscoveredRemoteCallback NativeBridge::<>f__mg$cache13
	MRDiscoveredRemoteCallback_t3604605211 * ___U3CU3Ef__mgU24cache13_27;

public:
	inline static int32_t get_offset_of_emptyActions_0() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___emptyActions_0)); }
	inline Dictionary_2_t846283718 * get_emptyActions_0() const { return ___emptyActions_0; }
	inline Dictionary_2_t846283718 ** get_address_of_emptyActions_0() { return &___emptyActions_0; }
	inline void set_emptyActions_0(Dictionary_2_t846283718 * value)
	{
		___emptyActions_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyActions_0), value);
	}

	inline static int32_t get_offset_of_boolActions_1() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___boolActions_1)); }
	inline Dictionary_2_t1247186066 * get_boolActions_1() const { return ___boolActions_1; }
	inline Dictionary_2_t1247186066 ** get_address_of_boolActions_1() { return &___boolActions_1; }
	inline void set_boolActions_1(Dictionary_2_t1247186066 * value)
	{
		___boolActions_1 = value;
		Il2CppCodeGenWriteBarrier((&___boolActions_1), value);
	}

	inline static int32_t get_offset_of_intActions_2() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___intActions_2)); }
	inline Dictionary_2_t3788456092 * get_intActions_2() const { return ___intActions_2; }
	inline Dictionary_2_t3788456092 ** get_address_of_intActions_2() { return &___intActions_2; }
	inline void set_intActions_2(Dictionary_2_t3788456092 * value)
	{
		___intActions_2 = value;
		Il2CppCodeGenWriteBarrier((&___intActions_2), value);
	}

	inline static int32_t get_offset_of_floatActions_3() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___floatActions_3)); }
	inline Dictionary_2_t3793088576 * get_floatActions_3() const { return ___floatActions_3; }
	inline Dictionary_2_t3793088576 ** get_address_of_floatActions_3() { return &___floatActions_3; }
	inline void set_floatActions_3(Dictionary_2_t3793088576 * value)
	{
		___floatActions_3 = value;
		Il2CppCodeGenWriteBarrier((&___floatActions_3), value);
	}

	inline static int32_t get_offset_of_exceptionActions_4() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___exceptionActions_4)); }
	inline Dictionary_2_t809385330 * get_exceptionActions_4() const { return ___exceptionActions_4; }
	inline Dictionary_2_t809385330 ** get_address_of_exceptionActions_4() { return &___exceptionActions_4; }
	inline void set_exceptionActions_4(Dictionary_2_t809385330 * value)
	{
		___exceptionActions_4 = value;
		Il2CppCodeGenWriteBarrier((&___exceptionActions_4), value);
	}

	inline static int32_t get_offset_of_remoteActions_5() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___remoteActions_5)); }
	inline Dictionary_2_t2377422206 * get_remoteActions_5() const { return ___remoteActions_5; }
	inline Dictionary_2_t2377422206 ** get_address_of_remoteActions_5() { return &___remoteActions_5; }
	inline void set_remoteActions_5(Dictionary_2_t2377422206 * value)
	{
		___remoteActions_5 = value;
		Il2CppCodeGenWriteBarrier((&___remoteActions_5), value);
	}

	inline static int32_t get_offset_of_remoteMotionActions_6() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___remoteMotionActions_6)); }
	inline Dictionary_2_t4078021086 * get_remoteMotionActions_6() const { return ___remoteMotionActions_6; }
	inline Dictionary_2_t4078021086 ** get_address_of_remoteMotionActions_6() { return &___remoteMotionActions_6; }
	inline void set_remoteMotionActions_6(Dictionary_2_t4078021086 * value)
	{
		___remoteMotionActions_6 = value;
		Il2CppCodeGenWriteBarrier((&___remoteMotionActions_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline MRIntCallback_t2215762430 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline MRIntCallback_t2215762430 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(MRIntCallback_t2215762430 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline MRBoolCallback_t2221477487 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline MRBoolCallback_t2221477487 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(MRBoolCallback_t2221477487 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline MRBoolCallback_t2221477487 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline MRBoolCallback_t2221477487 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(MRBoolCallback_t2221477487 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_11() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache3_11)); }
	inline MRDiscoveredRemoteCallback_t3604605211 * get_U3CU3Ef__mgU24cache3_11() const { return ___U3CU3Ef__mgU24cache3_11; }
	inline MRDiscoveredRemoteCallback_t3604605211 ** get_address_of_U3CU3Ef__mgU24cache3_11() { return &___U3CU3Ef__mgU24cache3_11; }
	inline void set_U3CU3Ef__mgU24cache3_11(MRDiscoveredRemoteCallback_t3604605211 * value)
	{
		___U3CU3Ef__mgU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_12() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache4_12)); }
	inline MRErrorCallback_t1972775801 * get_U3CU3Ef__mgU24cache4_12() const { return ___U3CU3Ef__mgU24cache4_12; }
	inline MRErrorCallback_t1972775801 ** get_address_of_U3CU3Ef__mgU24cache4_12() { return &___U3CU3Ef__mgU24cache4_12; }
	inline void set_U3CU3Ef__mgU24cache4_12(MRErrorCallback_t1972775801 * value)
	{
		___U3CU3Ef__mgU24cache4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_13() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache5_13)); }
	inline MRConnectedRemoteCallback_t3085918786 * get_U3CU3Ef__mgU24cache5_13() const { return ___U3CU3Ef__mgU24cache5_13; }
	inline MRConnectedRemoteCallback_t3085918786 ** get_address_of_U3CU3Ef__mgU24cache5_13() { return &___U3CU3Ef__mgU24cache5_13; }
	inline void set_U3CU3Ef__mgU24cache5_13(MRConnectedRemoteCallback_t3085918786 * value)
	{
		___U3CU3Ef__mgU24cache5_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_14() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache6_14)); }
	inline MRErrorCallback_t1972775801 * get_U3CU3Ef__mgU24cache6_14() const { return ___U3CU3Ef__mgU24cache6_14; }
	inline MRErrorCallback_t1972775801 ** get_address_of_U3CU3Ef__mgU24cache6_14() { return &___U3CU3Ef__mgU24cache6_14; }
	inline void set_U3CU3Ef__mgU24cache6_14(MRErrorCallback_t1972775801 * value)
	{
		___U3CU3Ef__mgU24cache6_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_15() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache7_15)); }
	inline MRErrorCallback_t1972775801 * get_U3CU3Ef__mgU24cache7_15() const { return ___U3CU3Ef__mgU24cache7_15; }
	inline MRErrorCallback_t1972775801 ** get_address_of_U3CU3Ef__mgU24cache7_15() { return &___U3CU3Ef__mgU24cache7_15; }
	inline void set_U3CU3Ef__mgU24cache7_15(MRErrorCallback_t1972775801 * value)
	{
		___U3CU3Ef__mgU24cache7_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_16() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache8_16)); }
	inline MRConnectedRemoteCallback_t3085918786 * get_U3CU3Ef__mgU24cache8_16() const { return ___U3CU3Ef__mgU24cache8_16; }
	inline MRConnectedRemoteCallback_t3085918786 ** get_address_of_U3CU3Ef__mgU24cache8_16() { return &___U3CU3Ef__mgU24cache8_16; }
	inline void set_U3CU3Ef__mgU24cache8_16(MRConnectedRemoteCallback_t3085918786 * value)
	{
		___U3CU3Ef__mgU24cache8_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_17() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache9_17)); }
	inline MRErrorCallback_t1972775801 * get_U3CU3Ef__mgU24cache9_17() const { return ___U3CU3Ef__mgU24cache9_17; }
	inline MRErrorCallback_t1972775801 ** get_address_of_U3CU3Ef__mgU24cache9_17() { return &___U3CU3Ef__mgU24cache9_17; }
	inline void set_U3CU3Ef__mgU24cache9_17(MRErrorCallback_t1972775801 * value)
	{
		___U3CU3Ef__mgU24cache9_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_18() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cacheA_18)); }
	inline MRBoolCallback_t2221477487 * get_U3CU3Ef__mgU24cacheA_18() const { return ___U3CU3Ef__mgU24cacheA_18; }
	inline MRBoolCallback_t2221477487 ** get_address_of_U3CU3Ef__mgU24cacheA_18() { return &___U3CU3Ef__mgU24cacheA_18; }
	inline void set_U3CU3Ef__mgU24cacheA_18(MRBoolCallback_t2221477487 * value)
	{
		___U3CU3Ef__mgU24cacheA_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_19() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cacheB_19)); }
	inline MRBoolCallback_t2221477487 * get_U3CU3Ef__mgU24cacheB_19() const { return ___U3CU3Ef__mgU24cacheB_19; }
	inline MRBoolCallback_t2221477487 ** get_address_of_U3CU3Ef__mgU24cacheB_19() { return &___U3CU3Ef__mgU24cacheB_19; }
	inline void set_U3CU3Ef__mgU24cacheB_19(MRBoolCallback_t2221477487 * value)
	{
		___U3CU3Ef__mgU24cacheB_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_20() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cacheC_20)); }
	inline MRFloatCallback_t3699197057 * get_U3CU3Ef__mgU24cacheC_20() const { return ___U3CU3Ef__mgU24cacheC_20; }
	inline MRFloatCallback_t3699197057 ** get_address_of_U3CU3Ef__mgU24cacheC_20() { return &___U3CU3Ef__mgU24cacheC_20; }
	inline void set_U3CU3Ef__mgU24cacheC_20(MRFloatCallback_t3699197057 * value)
	{
		___U3CU3Ef__mgU24cacheC_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_21() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cacheD_21)); }
	inline MRBoolCallback_t2221477487 * get_U3CU3Ef__mgU24cacheD_21() const { return ___U3CU3Ef__mgU24cacheD_21; }
	inline MRBoolCallback_t2221477487 ** get_address_of_U3CU3Ef__mgU24cacheD_21() { return &___U3CU3Ef__mgU24cacheD_21; }
	inline void set_U3CU3Ef__mgU24cacheD_21(MRBoolCallback_t2221477487 * value)
	{
		___U3CU3Ef__mgU24cacheD_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_22() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cacheE_22)); }
	inline MREmptyCallback_t3969082922 * get_U3CU3Ef__mgU24cacheE_22() const { return ___U3CU3Ef__mgU24cacheE_22; }
	inline MREmptyCallback_t3969082922 ** get_address_of_U3CU3Ef__mgU24cacheE_22() { return &___U3CU3Ef__mgU24cacheE_22; }
	inline void set_U3CU3Ef__mgU24cacheE_22(MREmptyCallback_t3969082922 * value)
	{
		___U3CU3Ef__mgU24cacheE_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_23() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cacheF_23)); }
	inline MRRemoteMotionCallback_t3634481787 * get_U3CU3Ef__mgU24cacheF_23() const { return ___U3CU3Ef__mgU24cacheF_23; }
	inline MRRemoteMotionCallback_t3634481787 ** get_address_of_U3CU3Ef__mgU24cacheF_23() { return &___U3CU3Ef__mgU24cacheF_23; }
	inline void set_U3CU3Ef__mgU24cacheF_23(MRRemoteMotionCallback_t3634481787 * value)
	{
		___U3CU3Ef__mgU24cacheF_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_24() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache10_24)); }
	inline MRRemoteMotionCallback_t3634481787 * get_U3CU3Ef__mgU24cache10_24() const { return ___U3CU3Ef__mgU24cache10_24; }
	inline MRRemoteMotionCallback_t3634481787 ** get_address_of_U3CU3Ef__mgU24cache10_24() { return &___U3CU3Ef__mgU24cache10_24; }
	inline void set_U3CU3Ef__mgU24cache10_24(MRRemoteMotionCallback_t3634481787 * value)
	{
		___U3CU3Ef__mgU24cache10_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_25() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache11_25)); }
	inline MRConnectedRemoteCallback_t3085918786 * get_U3CU3Ef__mgU24cache11_25() const { return ___U3CU3Ef__mgU24cache11_25; }
	inline MRConnectedRemoteCallback_t3085918786 ** get_address_of_U3CU3Ef__mgU24cache11_25() { return &___U3CU3Ef__mgU24cache11_25; }
	inline void set_U3CU3Ef__mgU24cache11_25(MRConnectedRemoteCallback_t3085918786 * value)
	{
		___U3CU3Ef__mgU24cache11_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_26() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache12_26)); }
	inline MRDiscoveredRemoteCallback_t3604605211 * get_U3CU3Ef__mgU24cache12_26() const { return ___U3CU3Ef__mgU24cache12_26; }
	inline MRDiscoveredRemoteCallback_t3604605211 ** get_address_of_U3CU3Ef__mgU24cache12_26() { return &___U3CU3Ef__mgU24cache12_26; }
	inline void set_U3CU3Ef__mgU24cache12_26(MRDiscoveredRemoteCallback_t3604605211 * value)
	{
		___U3CU3Ef__mgU24cache12_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache13_27() { return static_cast<int32_t>(offsetof(NativeBridge_t2060501782_StaticFields, ___U3CU3Ef__mgU24cache13_27)); }
	inline MRDiscoveredRemoteCallback_t3604605211 * get_U3CU3Ef__mgU24cache13_27() const { return ___U3CU3Ef__mgU24cache13_27; }
	inline MRDiscoveredRemoteCallback_t3604605211 ** get_address_of_U3CU3Ef__mgU24cache13_27() { return &___U3CU3Ef__mgU24cache13_27; }
	inline void set_U3CU3Ef__mgU24cache13_27(MRDiscoveredRemoteCallback_t3604605211 * value)
	{
		___U3CU3Ef__mgU24cache13_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache13_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEBRIDGE_T2060501782_H
#ifndef MIRABTREMOTEINPUT_T988911721_H
#define MIRABTREMOTEINPUT_T988911721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBTRemoteInput
struct  MiraBTRemoteInput_t988911721  : public RuntimeObject
{
public:
	// RemoteBase MiraBTRemoteInput::controller
	RemoteBase_t3616301967 * ___controller_0;
	// VirtualRemote MiraBTRemoteInput::_virtualRemote
	VirtualRemote_t4041918011 * ____virtualRemote_1;
	// Remote MiraBTRemoteInput::_connectedRemote
	Remote_t660843562 * ____connectedRemote_2;
	// Remote MiraBTRemoteInput::connectedRemote
	Remote_t660843562 * ___connectedRemote_3;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ___controller_0)); }
	inline RemoteBase_t3616301967 * get_controller_0() const { return ___controller_0; }
	inline RemoteBase_t3616301967 ** get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(RemoteBase_t3616301967 * value)
	{
		___controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___controller_0), value);
	}

	inline static int32_t get_offset_of__virtualRemote_1() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ____virtualRemote_1)); }
	inline VirtualRemote_t4041918011 * get__virtualRemote_1() const { return ____virtualRemote_1; }
	inline VirtualRemote_t4041918011 ** get_address_of__virtualRemote_1() { return &____virtualRemote_1; }
	inline void set__virtualRemote_1(VirtualRemote_t4041918011 * value)
	{
		____virtualRemote_1 = value;
		Il2CppCodeGenWriteBarrier((&____virtualRemote_1), value);
	}

	inline static int32_t get_offset_of__connectedRemote_2() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ____connectedRemote_2)); }
	inline Remote_t660843562 * get__connectedRemote_2() const { return ____connectedRemote_2; }
	inline Remote_t660843562 ** get_address_of__connectedRemote_2() { return &____connectedRemote_2; }
	inline void set__connectedRemote_2(Remote_t660843562 * value)
	{
		____connectedRemote_2 = value;
		Il2CppCodeGenWriteBarrier((&____connectedRemote_2), value);
	}

	inline static int32_t get_offset_of_connectedRemote_3() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ___connectedRemote_3)); }
	inline Remote_t660843562 * get_connectedRemote_3() const { return ___connectedRemote_3; }
	inline Remote_t660843562 ** get_address_of_connectedRemote_3() { return &___connectedRemote_3; }
	inline void set_connectedRemote_3(Remote_t660843562 * value)
	{
		___connectedRemote_3 = value;
		Il2CppCodeGenWriteBarrier((&___connectedRemote_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABTREMOTEINPUT_T988911721_H
#ifndef CONSTANTS_T1236580865_H
#define CONSTANTS_T1236580865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.Constants
struct  Constants_t1236580865  : public RuntimeObject
{
public:

public:
};

struct Constants_t1236580865_StaticFields
{
public:
	// System.String FlatLighting.Constants::FlatLightingShaderPath
	String_t* ___FlatLightingShaderPath_0;
	// System.String FlatLighting.Constants::FlatLightingTag
	String_t* ___FlatLightingTag_1;
	// System.String FlatLighting.Constants::FlatLightingBakedTag
	String_t* ___FlatLightingBakedTag_2;
	// System.String FlatLighting.Constants::GizmoIconsPath
	String_t* ___GizmoIconsPath_3;

public:
	inline static int32_t get_offset_of_FlatLightingShaderPath_0() { return static_cast<int32_t>(offsetof(Constants_t1236580865_StaticFields, ___FlatLightingShaderPath_0)); }
	inline String_t* get_FlatLightingShaderPath_0() const { return ___FlatLightingShaderPath_0; }
	inline String_t** get_address_of_FlatLightingShaderPath_0() { return &___FlatLightingShaderPath_0; }
	inline void set_FlatLightingShaderPath_0(String_t* value)
	{
		___FlatLightingShaderPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightingShaderPath_0), value);
	}

	inline static int32_t get_offset_of_FlatLightingTag_1() { return static_cast<int32_t>(offsetof(Constants_t1236580865_StaticFields, ___FlatLightingTag_1)); }
	inline String_t* get_FlatLightingTag_1() const { return ___FlatLightingTag_1; }
	inline String_t** get_address_of_FlatLightingTag_1() { return &___FlatLightingTag_1; }
	inline void set_FlatLightingTag_1(String_t* value)
	{
		___FlatLightingTag_1 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightingTag_1), value);
	}

	inline static int32_t get_offset_of_FlatLightingBakedTag_2() { return static_cast<int32_t>(offsetof(Constants_t1236580865_StaticFields, ___FlatLightingBakedTag_2)); }
	inline String_t* get_FlatLightingBakedTag_2() const { return ___FlatLightingBakedTag_2; }
	inline String_t** get_address_of_FlatLightingBakedTag_2() { return &___FlatLightingBakedTag_2; }
	inline void set_FlatLightingBakedTag_2(String_t* value)
	{
		___FlatLightingBakedTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightingBakedTag_2), value);
	}

	inline static int32_t get_offset_of_GizmoIconsPath_3() { return static_cast<int32_t>(offsetof(Constants_t1236580865_StaticFields, ___GizmoIconsPath_3)); }
	inline String_t* get_GizmoIconsPath_3() const { return ___GizmoIconsPath_3; }
	inline String_t** get_address_of_GizmoIconsPath_3() { return &___GizmoIconsPath_3; }
	inline void set_GizmoIconsPath_3(String_t* value)
	{
		___GizmoIconsPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___GizmoIconsPath_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T1236580865_H
#ifndef U3CDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY2_T3821765992_H
#define U3CDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY2_T3821765992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/<DisconnectConnectedRemote>c__AnonStorey2
struct  U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t3821765992  : public RuntimeObject
{
public:
	// System.Action`1<MiraRemoteException> RemoteManager/<DisconnectConnectedRemote>c__AnonStorey2::action
	Action_1_t3189573364 * ___action_0;
	// RemoteManager RemoteManager/<DisconnectConnectedRemote>c__AnonStorey2::$this
	RemoteManager_t2208998541 * ___U24this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t3821765992, ___action_0)); }
	inline Action_1_t3189573364 * get_action_0() const { return ___action_0; }
	inline Action_1_t3189573364 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3189573364 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t3821765992, ___U24this_1)); }
	inline RemoteManager_t2208998541 * get_U24this_1() const { return ___U24this_1; }
	inline RemoteManager_t2208998541 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RemoteManager_t2208998541 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY2_T3821765992_H
#ifndef REMOTEBASE_T3616301967_H
#define REMOTEBASE_T3616301967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteBase
struct  RemoteBase_t3616301967  : public RuntimeObject
{
public:
	// RemoteButtonInput RemoteBase::<menuButton>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CmenuButtonU3Ek__BackingField_0;
	// RemoteButtonInput RemoteBase::<homeButton>k__BackingField
	RemoteButtonInput_t144791130 * ___U3ChomeButtonU3Ek__BackingField_1;
	// RemoteButtonInput RemoteBase::<trigger>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CtriggerU3Ek__BackingField_2;
	// RemoteTouchPadInput RemoteBase::<touchPad>k__BackingField
	RemoteTouchPadInput_t1081319266 * ___U3CtouchPadU3Ek__BackingField_3;
	// RemoteMotionInput RemoteBase::<motion>k__BackingField
	RemoteMotionInput_t3548926620 * ___U3CmotionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CmenuButtonU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CmenuButtonU3Ek__BackingField_0)); }
	inline RemoteButtonInput_t144791130 * get_U3CmenuButtonU3Ek__BackingField_0() const { return ___U3CmenuButtonU3Ek__BackingField_0; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CmenuButtonU3Ek__BackingField_0() { return &___U3CmenuButtonU3Ek__BackingField_0; }
	inline void set_U3CmenuButtonU3Ek__BackingField_0(RemoteButtonInput_t144791130 * value)
	{
		___U3CmenuButtonU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmenuButtonU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ChomeButtonU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3ChomeButtonU3Ek__BackingField_1)); }
	inline RemoteButtonInput_t144791130 * get_U3ChomeButtonU3Ek__BackingField_1() const { return ___U3ChomeButtonU3Ek__BackingField_1; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3ChomeButtonU3Ek__BackingField_1() { return &___U3ChomeButtonU3Ek__BackingField_1; }
	inline void set_U3ChomeButtonU3Ek__BackingField_1(RemoteButtonInput_t144791130 * value)
	{
		___U3ChomeButtonU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChomeButtonU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtriggerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CtriggerU3Ek__BackingField_2)); }
	inline RemoteButtonInput_t144791130 * get_U3CtriggerU3Ek__BackingField_2() const { return ___U3CtriggerU3Ek__BackingField_2; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CtriggerU3Ek__BackingField_2() { return &___U3CtriggerU3Ek__BackingField_2; }
	inline void set_U3CtriggerU3Ek__BackingField_2(RemoteButtonInput_t144791130 * value)
	{
		___U3CtriggerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtriggerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtouchPadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CtouchPadU3Ek__BackingField_3)); }
	inline RemoteTouchPadInput_t1081319266 * get_U3CtouchPadU3Ek__BackingField_3() const { return ___U3CtouchPadU3Ek__BackingField_3; }
	inline RemoteTouchPadInput_t1081319266 ** get_address_of_U3CtouchPadU3Ek__BackingField_3() { return &___U3CtouchPadU3Ek__BackingField_3; }
	inline void set_U3CtouchPadU3Ek__BackingField_3(RemoteTouchPadInput_t1081319266 * value)
	{
		___U3CtouchPadU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchPadU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CmotionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CmotionU3Ek__BackingField_4)); }
	inline RemoteMotionInput_t3548926620 * get_U3CmotionU3Ek__BackingField_4() const { return ___U3CmotionU3Ek__BackingField_4; }
	inline RemoteMotionInput_t3548926620 ** get_address_of_U3CmotionU3Ek__BackingField_4() { return &___U3CmotionU3Ek__BackingField_4; }
	inline void set_U3CmotionU3Ek__BackingField_4(RemoteMotionInput_t3548926620 * value)
	{
		___U3CmotionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmotionU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBASE_T3616301967_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef REMOTEBUTTONINPUT_T144791130_H
#define REMOTEBUTTONINPUT_T144791130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteButtonInput
struct  RemoteButtonInput_t144791130  : public RuntimeObject
{
public:
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnPressChanged
	RemoteButtonInputEventHandler_t1063480517 * ___OnPressChanged_0;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnHeldState
	RemoteButtonInputEventHandler_t1063480517 * ___OnHeldState_1;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnPresseddState
	RemoteButtonInputEventHandler_t1063480517 * ___OnPresseddState_2;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnReleasedState
	RemoteButtonInputEventHandler_t1063480517 * ___OnReleasedState_3;
	// System.Int32 RemoteButtonInput::m_PrevFrameCount
	int32_t ___m_PrevFrameCount_4;
	// System.Boolean RemoteButtonInput::m_PrevState
	bool ___m_PrevState_5;
	// System.Boolean RemoteButtonInput::m_State
	bool ___m_State_6;
	// System.Boolean RemoteButtonInput::_isPressed
	bool ____isPressed_7;
	// System.Boolean RemoteButtonInput::_onPressed
	bool ____onPressed_8;
	// System.Boolean RemoteButtonInput::_onReleased
	bool ____onReleased_9;
	// System.Boolean RemoteButtonInput::_onHeld
	bool ____onHeld_10;

public:
	inline static int32_t get_offset_of_OnPressChanged_0() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnPressChanged_0)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnPressChanged_0() const { return ___OnPressChanged_0; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnPressChanged_0() { return &___OnPressChanged_0; }
	inline void set_OnPressChanged_0(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnPressChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressChanged_0), value);
	}

	inline static int32_t get_offset_of_OnHeldState_1() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnHeldState_1)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnHeldState_1() const { return ___OnHeldState_1; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnHeldState_1() { return &___OnHeldState_1; }
	inline void set_OnHeldState_1(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnHeldState_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeldState_1), value);
	}

	inline static int32_t get_offset_of_OnPresseddState_2() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnPresseddState_2)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnPresseddState_2() const { return ___OnPresseddState_2; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnPresseddState_2() { return &___OnPresseddState_2; }
	inline void set_OnPresseddState_2(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnPresseddState_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnPresseddState_2), value);
	}

	inline static int32_t get_offset_of_OnReleasedState_3() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnReleasedState_3)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnReleasedState_3() const { return ___OnReleasedState_3; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnReleasedState_3() { return &___OnReleasedState_3; }
	inline void set_OnReleasedState_3(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnReleasedState_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnReleasedState_3), value);
	}

	inline static int32_t get_offset_of_m_PrevFrameCount_4() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___m_PrevFrameCount_4)); }
	inline int32_t get_m_PrevFrameCount_4() const { return ___m_PrevFrameCount_4; }
	inline int32_t* get_address_of_m_PrevFrameCount_4() { return &___m_PrevFrameCount_4; }
	inline void set_m_PrevFrameCount_4(int32_t value)
	{
		___m_PrevFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_5() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___m_PrevState_5)); }
	inline bool get_m_PrevState_5() const { return ___m_PrevState_5; }
	inline bool* get_address_of_m_PrevState_5() { return &___m_PrevState_5; }
	inline void set_m_PrevState_5(bool value)
	{
		___m_PrevState_5 = value;
	}

	inline static int32_t get_offset_of_m_State_6() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___m_State_6)); }
	inline bool get_m_State_6() const { return ___m_State_6; }
	inline bool* get_address_of_m_State_6() { return &___m_State_6; }
	inline void set_m_State_6(bool value)
	{
		___m_State_6 = value;
	}

	inline static int32_t get_offset_of__isPressed_7() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____isPressed_7)); }
	inline bool get__isPressed_7() const { return ____isPressed_7; }
	inline bool* get_address_of__isPressed_7() { return &____isPressed_7; }
	inline void set__isPressed_7(bool value)
	{
		____isPressed_7 = value;
	}

	inline static int32_t get_offset_of__onPressed_8() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____onPressed_8)); }
	inline bool get__onPressed_8() const { return ____onPressed_8; }
	inline bool* get_address_of__onPressed_8() { return &____onPressed_8; }
	inline void set__onPressed_8(bool value)
	{
		____onPressed_8 = value;
	}

	inline static int32_t get_offset_of__onReleased_9() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____onReleased_9)); }
	inline bool get__onReleased_9() const { return ____onReleased_9; }
	inline bool* get_address_of__onReleased_9() { return &____onReleased_9; }
	inline void set__onReleased_9(bool value)
	{
		____onReleased_9 = value;
	}

	inline static int32_t get_offset_of__onHeld_10() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____onHeld_10)); }
	inline bool get__onHeld_10() const { return ____onHeld_10; }
	inline bool* get_address_of__onHeld_10() { return &____onHeld_10; }
	inline void set__onHeld_10(bool value)
	{
		____onHeld_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBUTTONINPUT_T144791130_H
#ifndef MAPPOINTCLOUD_T2215873309_H
#define MAPPOINTCLOUD_T2215873309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.MapPointCloud
struct  MapPointCloud_t2215873309  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Wikitude.MapPointCloud::FeaturePoints
	Vector3U5BU5D_t1172311765* ___FeaturePoints_0;
	// UnityEngine.Color[] Wikitude.MapPointCloud::Colors
	ColorU5BU5D_t672350442* ___Colors_1;
	// System.Single Wikitude.MapPointCloud::Scale
	float ___Scale_2;

public:
	inline static int32_t get_offset_of_FeaturePoints_0() { return static_cast<int32_t>(offsetof(MapPointCloud_t2215873309, ___FeaturePoints_0)); }
	inline Vector3U5BU5D_t1172311765* get_FeaturePoints_0() const { return ___FeaturePoints_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_FeaturePoints_0() { return &___FeaturePoints_0; }
	inline void set_FeaturePoints_0(Vector3U5BU5D_t1172311765* value)
	{
		___FeaturePoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___FeaturePoints_0), value);
	}

	inline static int32_t get_offset_of_Colors_1() { return static_cast<int32_t>(offsetof(MapPointCloud_t2215873309, ___Colors_1)); }
	inline ColorU5BU5D_t672350442* get_Colors_1() const { return ___Colors_1; }
	inline ColorU5BU5D_t672350442** get_address_of_Colors_1() { return &___Colors_1; }
	inline void set_Colors_1(ColorU5BU5D_t672350442* value)
	{
		___Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_1), value);
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(MapPointCloud_t2215873309, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPOINTCLOUD_T2215873309_H
#ifndef POINTCLOUDRENDERER_T1003576694_H
#define POINTCLOUDRENDERER_T1003576694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WikitudeEditor.PointCloudRenderer
struct  PointCloudRenderer_t1003576694  : public RuntimeObject
{
public:
	// UnityEngine.Mesh WikitudeEditor.PointCloudRenderer::_testMesh
	Mesh_t1356156583 * ____testMesh_0;
	// UnityEngine.Camera WikitudeEditor.PointCloudRenderer::_sceneCamera
	Camera_t189460977 * ____sceneCamera_1;
	// UnityEngine.Material WikitudeEditor.PointCloudRenderer::_renderMaterial
	Material_t193706927 * ____renderMaterial_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> WikitudeEditor.PointCloudRenderer::_cubePoints
	List_1_t1612828712 * ____cubePoints_3;
	// System.Collections.Generic.List`1<System.Int32> WikitudeEditor.PointCloudRenderer::_cubeIndices
	List_1_t1440998580 * ____cubeIndices_4;
	// System.Int32 WikitudeEditor.PointCloudRenderer::_meshVertexCount
	int32_t ____meshVertexCount_5;
	// System.Boolean WikitudeEditor.PointCloudRenderer::_drawWithCommandBuffer
	bool ____drawWithCommandBuffer_6;
	// UnityEngine.Vector3[] WikitudeEditor.PointCloudRenderer::_vertices
	Vector3U5BU5D_t1172311765* ____vertices_7;
	// System.Int32[] WikitudeEditor.PointCloudRenderer::_indices
	Int32U5BU5D_t3030399641* ____indices_8;
	// UnityEngine.Color[] WikitudeEditor.PointCloudRenderer::_vertexColors
	ColorU5BU5D_t672350442* ____vertexColors_9;

public:
	inline static int32_t get_offset_of__testMesh_0() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____testMesh_0)); }
	inline Mesh_t1356156583 * get__testMesh_0() const { return ____testMesh_0; }
	inline Mesh_t1356156583 ** get_address_of__testMesh_0() { return &____testMesh_0; }
	inline void set__testMesh_0(Mesh_t1356156583 * value)
	{
		____testMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&____testMesh_0), value);
	}

	inline static int32_t get_offset_of__sceneCamera_1() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____sceneCamera_1)); }
	inline Camera_t189460977 * get__sceneCamera_1() const { return ____sceneCamera_1; }
	inline Camera_t189460977 ** get_address_of__sceneCamera_1() { return &____sceneCamera_1; }
	inline void set__sceneCamera_1(Camera_t189460977 * value)
	{
		____sceneCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneCamera_1), value);
	}

	inline static int32_t get_offset_of__renderMaterial_2() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____renderMaterial_2)); }
	inline Material_t193706927 * get__renderMaterial_2() const { return ____renderMaterial_2; }
	inline Material_t193706927 ** get_address_of__renderMaterial_2() { return &____renderMaterial_2; }
	inline void set__renderMaterial_2(Material_t193706927 * value)
	{
		____renderMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&____renderMaterial_2), value);
	}

	inline static int32_t get_offset_of__cubePoints_3() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____cubePoints_3)); }
	inline List_1_t1612828712 * get__cubePoints_3() const { return ____cubePoints_3; }
	inline List_1_t1612828712 ** get_address_of__cubePoints_3() { return &____cubePoints_3; }
	inline void set__cubePoints_3(List_1_t1612828712 * value)
	{
		____cubePoints_3 = value;
		Il2CppCodeGenWriteBarrier((&____cubePoints_3), value);
	}

	inline static int32_t get_offset_of__cubeIndices_4() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____cubeIndices_4)); }
	inline List_1_t1440998580 * get__cubeIndices_4() const { return ____cubeIndices_4; }
	inline List_1_t1440998580 ** get_address_of__cubeIndices_4() { return &____cubeIndices_4; }
	inline void set__cubeIndices_4(List_1_t1440998580 * value)
	{
		____cubeIndices_4 = value;
		Il2CppCodeGenWriteBarrier((&____cubeIndices_4), value);
	}

	inline static int32_t get_offset_of__meshVertexCount_5() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____meshVertexCount_5)); }
	inline int32_t get__meshVertexCount_5() const { return ____meshVertexCount_5; }
	inline int32_t* get_address_of__meshVertexCount_5() { return &____meshVertexCount_5; }
	inline void set__meshVertexCount_5(int32_t value)
	{
		____meshVertexCount_5 = value;
	}

	inline static int32_t get_offset_of__drawWithCommandBuffer_6() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____drawWithCommandBuffer_6)); }
	inline bool get__drawWithCommandBuffer_6() const { return ____drawWithCommandBuffer_6; }
	inline bool* get_address_of__drawWithCommandBuffer_6() { return &____drawWithCommandBuffer_6; }
	inline void set__drawWithCommandBuffer_6(bool value)
	{
		____drawWithCommandBuffer_6 = value;
	}

	inline static int32_t get_offset_of__vertices_7() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____vertices_7)); }
	inline Vector3U5BU5D_t1172311765* get__vertices_7() const { return ____vertices_7; }
	inline Vector3U5BU5D_t1172311765** get_address_of__vertices_7() { return &____vertices_7; }
	inline void set__vertices_7(Vector3U5BU5D_t1172311765* value)
	{
		____vertices_7 = value;
		Il2CppCodeGenWriteBarrier((&____vertices_7), value);
	}

	inline static int32_t get_offset_of__indices_8() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____indices_8)); }
	inline Int32U5BU5D_t3030399641* get__indices_8() const { return ____indices_8; }
	inline Int32U5BU5D_t3030399641** get_address_of__indices_8() { return &____indices_8; }
	inline void set__indices_8(Int32U5BU5D_t3030399641* value)
	{
		____indices_8 = value;
		Il2CppCodeGenWriteBarrier((&____indices_8), value);
	}

	inline static int32_t get_offset_of__vertexColors_9() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t1003576694, ____vertexColors_9)); }
	inline ColorU5BU5D_t672350442* get__vertexColors_9() const { return ____vertexColors_9; }
	inline ColorU5BU5D_t672350442** get_address_of__vertexColors_9() { return &____vertexColors_9; }
	inline void set__vertexColors_9(ColorU5BU5D_t672350442* value)
	{
		____vertexColors_9 = value;
		Il2CppCodeGenWriteBarrier((&____vertexColors_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDRENDERER_T1003576694_H
#ifndef PLATFORMBASE_T3918687204_H
#define PLATFORMBASE_T3918687204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PlatformBase
struct  PlatformBase_t3918687204  : public RuntimeObject
{
public:

public:
};

struct PlatformBase_t3918687204_StaticFields
{
public:
	// Wikitude.PlatformBase Wikitude.PlatformBase::_instance
	PlatformBase_t3918687204 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PlatformBase_t3918687204_StaticFields, ____instance_0)); }
	inline PlatformBase_t3918687204 * get__instance_0() const { return ____instance_0; }
	inline PlatformBase_t3918687204 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PlatformBase_t3918687204 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMBASE_T3918687204_H
#ifndef U3CCONNECTREMOTEU3EC__ANONSTOREY1_T3364220388_H
#define U3CCONNECTREMOTEU3EC__ANONSTOREY1_T3364220388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/<ConnectRemote>c__AnonStorey1
struct  U3CConnectRemoteU3Ec__AnonStorey1_t3364220388  : public RuntimeObject
{
public:
	// Remote RemoteManager/<ConnectRemote>c__AnonStorey1::remote
	Remote_t660843562 * ___remote_0;
	// System.Action`1<MiraRemoteException> RemoteManager/<ConnectRemote>c__AnonStorey1::action
	Action_1_t3189573364 * ___action_1;
	// RemoteManager RemoteManager/<ConnectRemote>c__AnonStorey1::$this
	RemoteManager_t2208998541 * ___U24this_2;

public:
	inline static int32_t get_offset_of_remote_0() { return static_cast<int32_t>(offsetof(U3CConnectRemoteU3Ec__AnonStorey1_t3364220388, ___remote_0)); }
	inline Remote_t660843562 * get_remote_0() const { return ___remote_0; }
	inline Remote_t660843562 ** get_address_of_remote_0() { return &___remote_0; }
	inline void set_remote_0(Remote_t660843562 * value)
	{
		___remote_0 = value;
		Il2CppCodeGenWriteBarrier((&___remote_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CConnectRemoteU3Ec__AnonStorey1_t3364220388, ___action_1)); }
	inline Action_1_t3189573364 * get_action_1() const { return ___action_1; }
	inline Action_1_t3189573364 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3189573364 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CConnectRemoteU3Ec__AnonStorey1_t3364220388, ___U24this_2)); }
	inline RemoteManager_t2208998541 * get_U24this_2() const { return ___U24this_2; }
	inline RemoteManager_t2208998541 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(RemoteManager_t2208998541 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONNECTREMOTEU3EC__ANONSTOREY1_T3364220388_H
#ifndef REMOTEAXISINPUT_T2770128439_H
#define REMOTEAXISINPUT_T2770128439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteAxisInput
struct  RemoteAxisInput_t2770128439  : public RuntimeObject
{
public:
	// RemoteAxisInput/RemoteAxisInputEventHandler RemoteAxisInput::OnValueChanged
	RemoteAxisInputEventHandler_t3796077797 * ___OnValueChanged_0;
	// System.Single RemoteAxisInput::_value
	float ____value_1;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteAxisInput_t2770128439, ___OnValueChanged_0)); }
	inline RemoteAxisInputEventHandler_t3796077797 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteAxisInputEventHandler_t3796077797 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteAxisInputEventHandler_t3796077797 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(RemoteAxisInput_t2770128439, ____value_1)); }
	inline float get__value_1() const { return ____value_1; }
	inline float* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(float value)
	{
		____value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEAXISINPUT_T2770128439_H
#ifndef U3CUPDATEBLENDINGU3EC__ITERATOR0_T3290861102_H
#define U3CUPDATEBLENDINGU3EC__ITERATOR0_T3290861102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0
struct  U3CUpdateBlendingU3Ec__Iterator0_t3290861102  : public RuntimeObject
{
public:
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<currentMaterialIndex>__0
	int32_t ___U3CcurrentMaterialIndexU3E__0_0;
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<nextMaterialIndex>__0
	int32_t ___U3CnextMaterialIndexU3E__0_1;
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_2;
	// UnityEngine.Material FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<currentMaterial>__2
	Material_t193706927 * ___U3CcurrentMaterialU3E__2_3;
	// UnityEngine.Material FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<nextMaterial>__2
	Material_t193706927 * ___U3CnextMaterialU3E__2_4;
	// System.Single FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::<t>__3
	float ___U3CtU3E__3_5;
	// FlatLighting.MaterialBlender FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$this
	MaterialBlender_t4011564903 * ___U24this_6;
	// System.Object FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 FlatLighting.MaterialBlender/<UpdateBlending>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CcurrentMaterialIndexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U3CcurrentMaterialIndexU3E__0_0)); }
	inline int32_t get_U3CcurrentMaterialIndexU3E__0_0() const { return ___U3CcurrentMaterialIndexU3E__0_0; }
	inline int32_t* get_address_of_U3CcurrentMaterialIndexU3E__0_0() { return &___U3CcurrentMaterialIndexU3E__0_0; }
	inline void set_U3CcurrentMaterialIndexU3E__0_0(int32_t value)
	{
		___U3CcurrentMaterialIndexU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnextMaterialIndexU3E__0_1() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U3CnextMaterialIndexU3E__0_1)); }
	inline int32_t get_U3CnextMaterialIndexU3E__0_1() const { return ___U3CnextMaterialIndexU3E__0_1; }
	inline int32_t* get_address_of_U3CnextMaterialIndexU3E__0_1() { return &___U3CnextMaterialIndexU3E__0_1; }
	inline void set_U3CnextMaterialIndexU3E__0_1(int32_t value)
	{
		___U3CnextMaterialIndexU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_2() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U3CiU3E__1_2)); }
	inline int32_t get_U3CiU3E__1_2() const { return ___U3CiU3E__1_2; }
	inline int32_t* get_address_of_U3CiU3E__1_2() { return &___U3CiU3E__1_2; }
	inline void set_U3CiU3E__1_2(int32_t value)
	{
		___U3CiU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentMaterialU3E__2_3() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U3CcurrentMaterialU3E__2_3)); }
	inline Material_t193706927 * get_U3CcurrentMaterialU3E__2_3() const { return ___U3CcurrentMaterialU3E__2_3; }
	inline Material_t193706927 ** get_address_of_U3CcurrentMaterialU3E__2_3() { return &___U3CcurrentMaterialU3E__2_3; }
	inline void set_U3CcurrentMaterialU3E__2_3(Material_t193706927 * value)
	{
		___U3CcurrentMaterialU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentMaterialU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U3CnextMaterialU3E__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U3CnextMaterialU3E__2_4)); }
	inline Material_t193706927 * get_U3CnextMaterialU3E__2_4() const { return ___U3CnextMaterialU3E__2_4; }
	inline Material_t193706927 ** get_address_of_U3CnextMaterialU3E__2_4() { return &___U3CnextMaterialU3E__2_4; }
	inline void set_U3CnextMaterialU3E__2_4(Material_t193706927 * value)
	{
		___U3CnextMaterialU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnextMaterialU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3CtU3E__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U3CtU3E__3_5)); }
	inline float get_U3CtU3E__3_5() const { return ___U3CtU3E__3_5; }
	inline float* get_address_of_U3CtU3E__3_5() { return &___U3CtU3E__3_5; }
	inline void set_U3CtU3E__3_5(float value)
	{
		___U3CtU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U24this_6)); }
	inline MaterialBlender_t4011564903 * get_U24this_6() const { return ___U24this_6; }
	inline MaterialBlender_t4011564903 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(MaterialBlender_t4011564903 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CUpdateBlendingU3Ec__Iterator0_t3290861102, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBLENDINGU3EC__ITERATOR0_T3290861102_H
#ifndef U3CREMOTEMANAGERSTARTREMOTEDISCOVERYU3EC__ANONSTOREY2_T1765939533_H
#define U3CREMOTEMANAGERSTARTREMOTEDISCOVERYU3EC__ANONSTOREY2_T1765939533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2
struct  U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533  : public RuntimeObject
{
public:
	// System.Action`1<Remote> NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2::action
	Action_1_t462642944 * ___action_0;
	// System.String NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2::identifier
	String_t* ___identifier_1;
	// MiraRemoteException NativeBridge/<RemoteManagerStartRemoteDiscovery>c__AnonStorey2::discoveryException
	MiraRemoteException_t3387773982 * ___discoveryException_2;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533, ___action_0)); }
	inline Action_1_t462642944 * get_action_0() const { return ___action_0; }
	inline Action_1_t462642944 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t462642944 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_identifier_1() { return static_cast<int32_t>(offsetof(U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533, ___identifier_1)); }
	inline String_t* get_identifier_1() const { return ___identifier_1; }
	inline String_t** get_address_of_identifier_1() { return &___identifier_1; }
	inline void set_identifier_1(String_t* value)
	{
		___identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_1), value);
	}

	inline static int32_t get_offset_of_discoveryException_2() { return static_cast<int32_t>(offsetof(U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533, ___discoveryException_2)); }
	inline MiraRemoteException_t3387773982 * get_discoveryException_2() const { return ___discoveryException_2; }
	inline MiraRemoteException_t3387773982 ** get_address_of_discoveryException_2() { return &___discoveryException_2; }
	inline void set_discoveryException_2(MiraRemoteException_t3387773982 * value)
	{
		___discoveryException_2 = value;
		Il2CppCodeGenWriteBarrier((&___discoveryException_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERSTARTREMOTEDISCOVERYU3EC__ANONSTOREY2_T1765939533_H
#ifndef U3CREMOTEMANAGERCONNECTREMOTEU3EC__ANONSTOREY3_T1494848566_H
#define U3CREMOTEMANAGERCONNECTREMOTEU3EC__ANONSTOREY3_T1494848566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3
struct  U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566  : public RuntimeObject
{
public:
	// Remote NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3::remote
	Remote_t660843562 * ___remote_0;
	// System.Action`1<MiraRemoteException> NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3::action
	Action_1_t3189573364 * ___action_1;
	// System.String NativeBridge/<RemoteManagerConnectRemote>c__AnonStorey3::identifier
	String_t* ___identifier_2;

public:
	inline static int32_t get_offset_of_remote_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566, ___remote_0)); }
	inline Remote_t660843562 * get_remote_0() const { return ___remote_0; }
	inline Remote_t660843562 ** get_address_of_remote_0() { return &___remote_0; }
	inline void set_remote_0(Remote_t660843562 * value)
	{
		___remote_0 = value;
		Il2CppCodeGenWriteBarrier((&___remote_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566, ___action_1)); }
	inline Action_1_t3189573364 * get_action_1() const { return ___action_1; }
	inline Action_1_t3189573364 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3189573364 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566, ___identifier_2)); }
	inline String_t* get_identifier_2() const { return ___identifier_2; }
	inline String_t** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(String_t* value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERCONNECTREMOTEU3EC__ANONSTOREY3_T1494848566_H
#ifndef U3CREMOTEMANAGERDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY4_T3663694162_H
#define U3CREMOTEMANAGERDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY4_T3663694162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerDisconnectConnectedRemote>c__AnonStorey4
struct  U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t3663694162  : public RuntimeObject
{
public:
	// System.Action`1<MiraRemoteException> NativeBridge/<RemoteManagerDisconnectConnectedRemote>c__AnonStorey4::action
	Action_1_t3189573364 * ___action_0;
	// System.String NativeBridge/<RemoteManagerDisconnectConnectedRemote>c__AnonStorey4::identifier
	String_t* ___identifier_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t3663694162, ___action_0)); }
	inline Action_1_t3189573364 * get_action_0() const { return ___action_0; }
	inline Action_1_t3189573364 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3189573364 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_identifier_1() { return static_cast<int32_t>(offsetof(U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t3663694162, ___identifier_1)); }
	inline String_t* get_identifier_1() const { return ___identifier_1; }
	inline String_t** get_address_of_identifier_1() { return &___identifier_1; }
	inline void set_identifier_1(String_t* value)
	{
		___identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERDISCONNECTCONNECTEDREMOTEU3EC__ANONSTOREY4_T3663694162_H
#ifndef U3CREMOTEREFRESHU3EC__ANONSTOREY5_T751324336_H
#define U3CREMOTEREFRESHU3EC__ANONSTOREY5_T751324336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteRefresh>c__AnonStorey5
struct  U3CRemoteRefreshU3Ec__AnonStorey5_t751324336  : public RuntimeObject
{
public:
	// Remote NativeBridge/<RemoteRefresh>c__AnonStorey5::remote
	Remote_t660843562 * ___remote_0;
	// System.Action`1<MiraRemoteException> NativeBridge/<RemoteRefresh>c__AnonStorey5::action
	Action_1_t3189573364 * ___action_1;
	// System.String NativeBridge/<RemoteRefresh>c__AnonStorey5::identifier
	String_t* ___identifier_2;

public:
	inline static int32_t get_offset_of_remote_0() { return static_cast<int32_t>(offsetof(U3CRemoteRefreshU3Ec__AnonStorey5_t751324336, ___remote_0)); }
	inline Remote_t660843562 * get_remote_0() const { return ___remote_0; }
	inline Remote_t660843562 ** get_address_of_remote_0() { return &___remote_0; }
	inline void set_remote_0(Remote_t660843562 * value)
	{
		___remote_0 = value;
		Il2CppCodeGenWriteBarrier((&___remote_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CRemoteRefreshU3Ec__AnonStorey5_t751324336, ___action_1)); }
	inline Action_1_t3189573364 * get_action_1() const { return ___action_1; }
	inline Action_1_t3189573364 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3189573364 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(U3CRemoteRefreshU3Ec__AnonStorey5_t751324336, ___identifier_2)); }
	inline String_t* get_identifier_2() const { return ___identifier_2; }
	inline String_t** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(String_t* value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEREFRESHU3EC__ANONSTOREY5_T751324336_H
#ifndef REMOTEMOTIONSENSORINPUT_T841531810_H
#define REMOTEMOTIONSENSORINPUT_T841531810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionSensorInput
struct  RemoteMotionSensorInput_t841531810  : public RuntimeObject
{
public:
	// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler RemoteMotionSensorInput::OnValueChanged
	RemoteMotionSensorInputEventHandler_t2876817765 * ___OnValueChanged_0;
	// System.Single RemoteMotionSensorInput::<x>k__BackingField
	float ___U3CxU3Ek__BackingField_1;
	// System.Single RemoteMotionSensorInput::<y>k__BackingField
	float ___U3CyU3Ek__BackingField_2;
	// System.Single RemoteMotionSensorInput::<z>k__BackingField
	float ___U3CzU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___OnValueChanged_0)); }
	inline RemoteMotionSensorInputEventHandler_t2876817765 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteMotionSensorInputEventHandler_t2876817765 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteMotionSensorInputEventHandler_t2876817765 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of_U3CxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___U3CxU3Ek__BackingField_1)); }
	inline float get_U3CxU3Ek__BackingField_1() const { return ___U3CxU3Ek__BackingField_1; }
	inline float* get_address_of_U3CxU3Ek__BackingField_1() { return &___U3CxU3Ek__BackingField_1; }
	inline void set_U3CxU3Ek__BackingField_1(float value)
	{
		___U3CxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___U3CyU3Ek__BackingField_2)); }
	inline float get_U3CyU3Ek__BackingField_2() const { return ___U3CyU3Ek__BackingField_2; }
	inline float* get_address_of_U3CyU3Ek__BackingField_2() { return &___U3CyU3Ek__BackingField_2; }
	inline void set_U3CyU3Ek__BackingField_2(float value)
	{
		___U3CyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CzU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___U3CzU3Ek__BackingField_3)); }
	inline float get_U3CzU3Ek__BackingField_3() const { return ___U3CzU3Ek__BackingField_3; }
	inline float* get_address_of_U3CzU3Ek__BackingField_3() { return &___U3CzU3Ek__BackingField_3; }
	inline void set_U3CzU3Ek__BackingField_3(float value)
	{
		___U3CzU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONSENSORINPUT_T841531810_H
#ifndef REMOTETOUCHPADINPUT_T1081319266_H
#define REMOTETOUCHPADINPUT_T1081319266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchPadInput
struct  RemoteTouchPadInput_t1081319266  : public RemoteTouchInput_t3826108381
{
public:
	// RemoteTouchPadInput/RemoteTouchPadInputEventHandler RemoteTouchPadInput::OnValueChanged
	RemoteTouchPadInputEventHandler_t2832767717 * ___OnValueChanged_2;
	// RemoteButtonInput RemoteTouchPadInput::<button>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CbuttonU3Ek__BackingField_3;
	// RemoteButtonInput RemoteTouchPadInput::<touchActive>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CtouchActiveU3Ek__BackingField_4;
	// RemoteAxisInput RemoteTouchPadInput::<xAxis>k__BackingField
	RemoteAxisInput_t2770128439 * ___U3CxAxisU3Ek__BackingField_5;
	// RemoteAxisInput RemoteTouchPadInput::<yAxis>k__BackingField
	RemoteAxisInput_t2770128439 * ___U3CyAxisU3Ek__BackingField_6;
	// RemoteTouchInput RemoteTouchPadInput::<up>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CupU3Ek__BackingField_7;
	// RemoteTouchInput RemoteTouchPadInput::<down>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CdownU3Ek__BackingField_8;
	// RemoteTouchInput RemoteTouchPadInput::<left>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CleftU3Ek__BackingField_9;
	// RemoteTouchInput RemoteTouchPadInput::<right>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CrightU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_OnValueChanged_2() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___OnValueChanged_2)); }
	inline RemoteTouchPadInputEventHandler_t2832767717 * get_OnValueChanged_2() const { return ___OnValueChanged_2; }
	inline RemoteTouchPadInputEventHandler_t2832767717 ** get_address_of_OnValueChanged_2() { return &___OnValueChanged_2; }
	inline void set_OnValueChanged_2(RemoteTouchPadInputEventHandler_t2832767717 * value)
	{
		___OnValueChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CbuttonU3Ek__BackingField_3)); }
	inline RemoteButtonInput_t144791130 * get_U3CbuttonU3Ek__BackingField_3() const { return ___U3CbuttonU3Ek__BackingField_3; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CbuttonU3Ek__BackingField_3() { return &___U3CbuttonU3Ek__BackingField_3; }
	inline void set_U3CbuttonU3Ek__BackingField_3(RemoteButtonInput_t144791130 * value)
	{
		___U3CbuttonU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuttonU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtouchActiveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CtouchActiveU3Ek__BackingField_4)); }
	inline RemoteButtonInput_t144791130 * get_U3CtouchActiveU3Ek__BackingField_4() const { return ___U3CtouchActiveU3Ek__BackingField_4; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CtouchActiveU3Ek__BackingField_4() { return &___U3CtouchActiveU3Ek__BackingField_4; }
	inline void set_U3CtouchActiveU3Ek__BackingField_4(RemoteButtonInput_t144791130 * value)
	{
		___U3CtouchActiveU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchActiveU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CxAxisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CxAxisU3Ek__BackingField_5)); }
	inline RemoteAxisInput_t2770128439 * get_U3CxAxisU3Ek__BackingField_5() const { return ___U3CxAxisU3Ek__BackingField_5; }
	inline RemoteAxisInput_t2770128439 ** get_address_of_U3CxAxisU3Ek__BackingField_5() { return &___U3CxAxisU3Ek__BackingField_5; }
	inline void set_U3CxAxisU3Ek__BackingField_5(RemoteAxisInput_t2770128439 * value)
	{
		___U3CxAxisU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxAxisU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CyAxisU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CyAxisU3Ek__BackingField_6)); }
	inline RemoteAxisInput_t2770128439 * get_U3CyAxisU3Ek__BackingField_6() const { return ___U3CyAxisU3Ek__BackingField_6; }
	inline RemoteAxisInput_t2770128439 ** get_address_of_U3CyAxisU3Ek__BackingField_6() { return &___U3CyAxisU3Ek__BackingField_6; }
	inline void set_U3CyAxisU3Ek__BackingField_6(RemoteAxisInput_t2770128439 * value)
	{
		___U3CyAxisU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CyAxisU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CupU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CupU3Ek__BackingField_7)); }
	inline RemoteTouchInput_t3826108381 * get_U3CupU3Ek__BackingField_7() const { return ___U3CupU3Ek__BackingField_7; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CupU3Ek__BackingField_7() { return &___U3CupU3Ek__BackingField_7; }
	inline void set_U3CupU3Ek__BackingField_7(RemoteTouchInput_t3826108381 * value)
	{
		___U3CupU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdownU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CdownU3Ek__BackingField_8)); }
	inline RemoteTouchInput_t3826108381 * get_U3CdownU3Ek__BackingField_8() const { return ___U3CdownU3Ek__BackingField_8; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CdownU3Ek__BackingField_8() { return &___U3CdownU3Ek__BackingField_8; }
	inline void set_U3CdownU3Ek__BackingField_8(RemoteTouchInput_t3826108381 * value)
	{
		___U3CdownU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CleftU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CleftU3Ek__BackingField_9)); }
	inline RemoteTouchInput_t3826108381 * get_U3CleftU3Ek__BackingField_9() const { return ___U3CleftU3Ek__BackingField_9; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CleftU3Ek__BackingField_9() { return &___U3CleftU3Ek__BackingField_9; }
	inline void set_U3CleftU3Ek__BackingField_9(RemoteTouchInput_t3826108381 * value)
	{
		___U3CleftU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CleftU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CrightU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CrightU3Ek__BackingField_10)); }
	inline RemoteTouchInput_t3826108381 * get_U3CrightU3Ek__BackingField_10() const { return ___U3CrightU3Ek__BackingField_10; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CrightU3Ek__BackingField_10() { return &___U3CrightU3Ek__BackingField_10; }
	inline void set_U3CrightU3Ek__BackingField_10(RemoteTouchInput_t3826108381 * value)
	{
		___U3CrightU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrightU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHPADINPUT_T1081319266_H
#ifndef VIRTUALREMOTE_T4041918011_H
#define VIRTUALREMOTE_T4041918011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VirtualRemote
struct  VirtualRemote_t4041918011  : public RemoteBase_t3616301967
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALREMOTE_T4041918011_H
#ifndef REMOTEMOTIONINPUT_T3548926620_H
#define REMOTEMOTIONINPUT_T3548926620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionInput
struct  RemoteMotionInput_t3548926620  : public RemoteTouchInput_t3826108381
{
public:
	// RemoteMotionInput/RemoteMotionInputEventHandler RemoteMotionInput::OnValueChanged
	RemoteMotionInputEventHandler_t1458362501 * ___OnValueChanged_2;
	// RemoteOrientationInput RemoteMotionInput::<orientation>k__BackingField
	RemoteOrientationInput_t3303200544 * ___U3CorientationU3Ek__BackingField_3;
	// RemoteMotionSensorInput RemoteMotionInput::<acceleration>k__BackingField
	RemoteMotionSensorInput_t841531810 * ___U3CaccelerationU3Ek__BackingField_4;
	// RemoteMotionSensorInput RemoteMotionInput::<rotationRate>k__BackingField
	RemoteMotionSensorInput_t841531810 * ___U3CrotationRateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_OnValueChanged_2() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___OnValueChanged_2)); }
	inline RemoteMotionInputEventHandler_t1458362501 * get_OnValueChanged_2() const { return ___OnValueChanged_2; }
	inline RemoteMotionInputEventHandler_t1458362501 ** get_address_of_OnValueChanged_2() { return &___OnValueChanged_2; }
	inline void set_OnValueChanged_2(RemoteMotionInputEventHandler_t1458362501 * value)
	{
		___OnValueChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CorientationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___U3CorientationU3Ek__BackingField_3)); }
	inline RemoteOrientationInput_t3303200544 * get_U3CorientationU3Ek__BackingField_3() const { return ___U3CorientationU3Ek__BackingField_3; }
	inline RemoteOrientationInput_t3303200544 ** get_address_of_U3CorientationU3Ek__BackingField_3() { return &___U3CorientationU3Ek__BackingField_3; }
	inline void set_U3CorientationU3Ek__BackingField_3(RemoteOrientationInput_t3303200544 * value)
	{
		___U3CorientationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CorientationU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CaccelerationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___U3CaccelerationU3Ek__BackingField_4)); }
	inline RemoteMotionSensorInput_t841531810 * get_U3CaccelerationU3Ek__BackingField_4() const { return ___U3CaccelerationU3Ek__BackingField_4; }
	inline RemoteMotionSensorInput_t841531810 ** get_address_of_U3CaccelerationU3Ek__BackingField_4() { return &___U3CaccelerationU3Ek__BackingField_4; }
	inline void set_U3CaccelerationU3Ek__BackingField_4(RemoteMotionSensorInput_t841531810 * value)
	{
		___U3CaccelerationU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaccelerationU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrotationRateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___U3CrotationRateU3Ek__BackingField_5)); }
	inline RemoteMotionSensorInput_t841531810 * get_U3CrotationRateU3Ek__BackingField_5() const { return ___U3CrotationRateU3Ek__BackingField_5; }
	inline RemoteMotionSensorInput_t841531810 ** get_address_of_U3CrotationRateU3Ek__BackingField_5() { return &___U3CrotationRateU3Ek__BackingField_5; }
	inline void set_U3CrotationRateU3Ek__BackingField_5(RemoteMotionSensorInput_t841531810 * value)
	{
		___U3CrotationRateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrotationRateU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONINPUT_T3548926620_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2510243513 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2510243513 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2510243513 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef NULLABLE_1_T339576247_H
#define NULLABLE_1_T339576247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t339576247 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T339576247_H
#ifndef NULLABLE_1_T334943763_H
#define NULLABLE_1_T334943763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t334943763 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T334943763_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef PROPERTYATTRIBUTE_T2606999759_H
#define PROPERTYATTRIBUTE_T2606999759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t2606999759  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T2606999759_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef U24ARRAYTYPEU3D64_T762068660_H
#define U24ARRAYTYPEU3D64_T762068660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t762068660 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t762068660__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D64_T762068660_H
#ifndef UNITYEVENT_1_T2067570248_H
#define UNITYEVENT_1_T2067570248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2067570248  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2067570248, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2067570248_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef TRANSFORMPROPERTIES_T3533762257_H
#define TRANSFORMPROPERTIES_T3533762257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformProperties
struct  TransformProperties_t3533762257 
{
public:
	// UnityEngine.Vector3 Wikitude.TransformProperties::Position
	Vector3_t2243707580  ___Position_0;
	// UnityEngine.Quaternion Wikitude.TransformProperties::Rotation
	Quaternion_t4030073918  ___Rotation_1;
	// UnityEngine.Vector3 Wikitude.TransformProperties::Scale
	Vector3_t2243707580  ___Scale_2;
	// System.Single Wikitude.TransformProperties::FieldOfView
	float ___FieldOfView_3;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(TransformProperties_t3533762257, ___Position_0)); }
	inline Vector3_t2243707580  get_Position_0() const { return ___Position_0; }
	inline Vector3_t2243707580 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t2243707580  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(TransformProperties_t3533762257, ___Rotation_1)); }
	inline Quaternion_t4030073918  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t4030073918 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t4030073918  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(TransformProperties_t3533762257, ___Scale_2)); }
	inline Vector3_t2243707580  get_Scale_2() const { return ___Scale_2; }
	inline Vector3_t2243707580 * get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(Vector3_t2243707580  value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_3() { return static_cast<int32_t>(offsetof(TransformProperties_t3533762257, ___FieldOfView_3)); }
	inline float get_FieldOfView_3() const { return ___FieldOfView_3; }
	inline float* get_address_of_FieldOfView_3() { return &___FieldOfView_3; }
	inline void set_FieldOfView_3(float value)
	{
		___FieldOfView_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMPROPERTIES_T3533762257_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef CLICKCHOICES_T763649284_H
#define CLICKCHOICES_T763649284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/ClickChoices
struct  ClickChoices_t763649284 
{
public:
	// System.Int32 MiraController/ClickChoices::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClickChoices_t763649284, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKCHOICES_T763649284_H
#ifndef RAYCASTSTYLE_T2248787495_H
#define RAYCASTSTYLE_T2248787495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster/RaycastStyle
struct  RaycastStyle_t2248787495 
{
public:
	// System.Int32 MiraBaseRaycaster/RaycastStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RaycastStyle_t2248787495, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTSTYLE_T2248787495_H
#ifndef CONTROLLERTYPE_T693226312_H
#define CONTROLLERTYPE_T693226312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/ControllerType
struct  ControllerType_t693226312 
{
public:
	// System.Int32 MiraController/ControllerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControllerType_t693226312, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T693226312_H
#ifndef HANDEDNESS_T261377275_H
#define HANDEDNESS_T261377275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController/Handedness
struct  Handedness_t261377275 
{
public:
	// System.Int32 MiraController/Handedness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Handedness_t261377275, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDEDNESS_T261377275_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef UNITYVERSION_T3349894225_H
#define UNITYVERSION_T3349894225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityVersion
struct  UnityVersion_t3349894225 
{
public:
	// System.Int32 Wikitude.UnityVersion::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityVersion_t3349894225, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVERSION_T3349894225_H
#ifndef ONENTERFIELDOFVISIONEVENT_T3362888889_H
#define ONENTERFIELDOFVISIONEVENT_T3362888889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent
struct  OnEnterFieldOfVisionEvent_t3362888889  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENTERFIELDOFVISIONEVENT_T3362888889_H
#ifndef ONEXITFIELDOFVISIONEVENT_T2513344377_H
#define ONEXITFIELDOFVISIONEVENT_T2513344377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent
struct  OnExitFieldOfVisionEvent_t2513344377  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEXITFIELDOFVISIONEVENT_T2513344377_H
#ifndef MIRAREMOTEERRORCODE_T1636063552_H
#define MIRAREMOTEERRORCODE_T1636063552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraRemoteErrorCode
struct  MiraRemoteErrorCode_t1636063552 
{
public:
	// System.Int32 MiraRemoteErrorCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MiraRemoteErrorCode_t1636063552, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAREMOTEERRORCODE_T1636063552_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305144  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB
	U24ArrayTypeU3D64_t762068660  ___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-5C26586BC03E63060E1A58A973915A014ECFCA38
	U24ArrayTypeU3D64_t762068660  ___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF
	U24ArrayTypeU3D64_t762068660  ___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-CD81E62297C7ECE146ADA340591341414F9422DB
	U24ArrayTypeU3D64_t762068660  ___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3;

public:
	inline static int32_t get_offset_of_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0)); }
	inline U24ArrayTypeU3D64_t762068660  get_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0() const { return ___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0; }
	inline U24ArrayTypeU3D64_t762068660 * get_address_of_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0() { return &___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0; }
	inline void set_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0(U24ArrayTypeU3D64_t762068660  value)
	{
		___U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1)); }
	inline U24ArrayTypeU3D64_t762068660  get_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1() const { return ___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1; }
	inline U24ArrayTypeU3D64_t762068660 * get_address_of_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1() { return &___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1; }
	inline void set_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1(U24ArrayTypeU3D64_t762068660  value)
	{
		___U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2)); }
	inline U24ArrayTypeU3D64_t762068660  get_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2() const { return ___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2; }
	inline U24ArrayTypeU3D64_t762068660 * get_address_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2() { return &___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2; }
	inline void set_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2(U24ArrayTypeU3D64_t762068660  value)
	{
		___U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3)); }
	inline U24ArrayTypeU3D64_t762068660  get_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3() const { return ___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3; }
	inline U24ArrayTypeU3D64_t762068660 * get_address_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3() { return &___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3; }
	inline void set_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3(U24ArrayTypeU3D64_t762068660  value)
	{
		___U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#ifndef RANGEWITHSTEPATTRIBUTE_T713572535_H
#define RANGEWITHSTEPATTRIBUTE_T713572535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.RangeWithStepAttribute
struct  RangeWithStepAttribute_t713572535  : public PropertyAttribute_t2606999759
{
public:
	// System.Int32 FlatLighting.RangeWithStepAttribute::min
	int32_t ___min_0;
	// System.Int32 FlatLighting.RangeWithStepAttribute::max
	int32_t ___max_1;
	// System.Int32 FlatLighting.RangeWithStepAttribute::step
	int32_t ___step_2;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeWithStepAttribute_t713572535, ___min_0)); }
	inline int32_t get_min_0() const { return ___min_0; }
	inline int32_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int32_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeWithStepAttribute_t713572535, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_step_2() { return static_cast<int32_t>(offsetof(RangeWithStepAttribute_t713572535, ___step_2)); }
	inline int32_t get_step_2() const { return ___step_2; }
	inline int32_t* get_address_of_step_2() { return &___step_2; }
	inline void set_step_2(int32_t value)
	{
		___step_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEWITHSTEPATTRIBUTE_T713572535_H
#ifndef REMOTE_T660843562_H
#define REMOTE_T660843562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Remote
struct  Remote_t660843562  : public RemoteBase_t3616301967
{
public:
	// Remote/RemoteRefreshedEventHandler Remote::OnRefresh
	RemoteRefreshedEventHandler_t1036293937 * ___OnRefresh_5;
	// System.Guid Remote::identifier
	Guid_t  ___identifier_6;
	// System.String Remote::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_7;
	// System.String Remote::<productName>k__BackingField
	String_t* ___U3CproductNameU3Ek__BackingField_8;
	// System.String Remote::<serialNumber>k__BackingField
	String_t* ___U3CserialNumberU3Ek__BackingField_9;
	// System.String Remote::<hardwareIdentifier>k__BackingField
	String_t* ___U3ChardwareIdentifierU3Ek__BackingField_10;
	// System.String Remote::<firmwareVersion>k__BackingField
	String_t* ___U3CfirmwareVersionU3Ek__BackingField_11;
	// System.Nullable`1<System.Single> Remote::<batteryPercentage>k__BackingField
	Nullable_1_t339576247  ___U3CbatteryPercentageU3Ek__BackingField_12;
	// System.Nullable`1<System.Int32> Remote::<rssi>k__BackingField
	Nullable_1_t334943763  ___U3CrssiU3Ek__BackingField_13;
	// System.Boolean Remote::<isConnected>k__BackingField
	bool ___U3CisConnectedU3Ek__BackingField_14;
	// System.Boolean Remote::<isPreferred>k__BackingField
	bool ___U3CisPreferredU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_OnRefresh_5() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___OnRefresh_5)); }
	inline RemoteRefreshedEventHandler_t1036293937 * get_OnRefresh_5() const { return ___OnRefresh_5; }
	inline RemoteRefreshedEventHandler_t1036293937 ** get_address_of_OnRefresh_5() { return &___OnRefresh_5; }
	inline void set_OnRefresh_5(RemoteRefreshedEventHandler_t1036293937 * value)
	{
		___OnRefresh_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnRefresh_5), value);
	}

	inline static int32_t get_offset_of_identifier_6() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___identifier_6)); }
	inline Guid_t  get_identifier_6() const { return ___identifier_6; }
	inline Guid_t * get_address_of_identifier_6() { return &___identifier_6; }
	inline void set_identifier_6(Guid_t  value)
	{
		___identifier_6 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CnameU3Ek__BackingField_7)); }
	inline String_t* get_U3CnameU3Ek__BackingField_7() const { return ___U3CnameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_7() { return &___U3CnameU3Ek__BackingField_7; }
	inline void set_U3CnameU3Ek__BackingField_7(String_t* value)
	{
		___U3CnameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CproductNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CproductNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CproductNameU3Ek__BackingField_8() const { return ___U3CproductNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CproductNameU3Ek__BackingField_8() { return &___U3CproductNameU3Ek__BackingField_8; }
	inline void set_U3CproductNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CproductNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductNameU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CserialNumberU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CserialNumberU3Ek__BackingField_9)); }
	inline String_t* get_U3CserialNumberU3Ek__BackingField_9() const { return ___U3CserialNumberU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CserialNumberU3Ek__BackingField_9() { return &___U3CserialNumberU3Ek__BackingField_9; }
	inline void set_U3CserialNumberU3Ek__BackingField_9(String_t* value)
	{
		___U3CserialNumberU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CserialNumberU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3ChardwareIdentifierU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3ChardwareIdentifierU3Ek__BackingField_10)); }
	inline String_t* get_U3ChardwareIdentifierU3Ek__BackingField_10() const { return ___U3ChardwareIdentifierU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3ChardwareIdentifierU3Ek__BackingField_10() { return &___U3ChardwareIdentifierU3Ek__BackingField_10; }
	inline void set_U3ChardwareIdentifierU3Ek__BackingField_10(String_t* value)
	{
		___U3ChardwareIdentifierU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChardwareIdentifierU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CfirmwareVersionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CfirmwareVersionU3Ek__BackingField_11)); }
	inline String_t* get_U3CfirmwareVersionU3Ek__BackingField_11() const { return ___U3CfirmwareVersionU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CfirmwareVersionU3Ek__BackingField_11() { return &___U3CfirmwareVersionU3Ek__BackingField_11; }
	inline void set_U3CfirmwareVersionU3Ek__BackingField_11(String_t* value)
	{
		___U3CfirmwareVersionU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfirmwareVersionU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CbatteryPercentageU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CbatteryPercentageU3Ek__BackingField_12)); }
	inline Nullable_1_t339576247  get_U3CbatteryPercentageU3Ek__BackingField_12() const { return ___U3CbatteryPercentageU3Ek__BackingField_12; }
	inline Nullable_1_t339576247 * get_address_of_U3CbatteryPercentageU3Ek__BackingField_12() { return &___U3CbatteryPercentageU3Ek__BackingField_12; }
	inline void set_U3CbatteryPercentageU3Ek__BackingField_12(Nullable_1_t339576247  value)
	{
		___U3CbatteryPercentageU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CrssiU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CrssiU3Ek__BackingField_13)); }
	inline Nullable_1_t334943763  get_U3CrssiU3Ek__BackingField_13() const { return ___U3CrssiU3Ek__BackingField_13; }
	inline Nullable_1_t334943763 * get_address_of_U3CrssiU3Ek__BackingField_13() { return &___U3CrssiU3Ek__BackingField_13; }
	inline void set_U3CrssiU3Ek__BackingField_13(Nullable_1_t334943763  value)
	{
		___U3CrssiU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CisConnectedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CisConnectedU3Ek__BackingField_14)); }
	inline bool get_U3CisConnectedU3Ek__BackingField_14() const { return ___U3CisConnectedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisConnectedU3Ek__BackingField_14() { return &___U3CisConnectedU3Ek__BackingField_14; }
	inline void set_U3CisConnectedU3Ek__BackingField_14(bool value)
	{
		___U3CisConnectedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPreferredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CisPreferredU3Ek__BackingField_15)); }
	inline bool get_U3CisPreferredU3Ek__BackingField_15() const { return ___U3CisPreferredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPreferredU3Ek__BackingField_15() { return &___U3CisPreferredU3Ek__BackingField_15; }
	inline void set_U3CisPreferredU3Ek__BackingField_15(bool value)
	{
		___U3CisPreferredU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTE_T660843562_H
#ifndef BLUETOOTHSTATE_T1243475723_H
#define BLUETOOTHSTATE_T1243475723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothState
struct  BluetoothState_t1243475723 
{
public:
	// System.Int32 BluetoothState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BluetoothState_t1243475723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUETOOTHSTATE_T1243475723_H
#ifndef VECTORASSLIDERSATTRIBUTE_T803388163_H
#define VECTORASSLIDERSATTRIBUTE_T803388163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.VectorAsSlidersAttribute
struct  VectorAsSlidersAttribute_t803388163  : public PropertyAttribute_t2606999759
{
public:
	// System.String FlatLighting.VectorAsSlidersAttribute::label
	String_t* ___label_0;
	// System.Single FlatLighting.VectorAsSlidersAttribute::min
	float ___min_1;
	// System.Single FlatLighting.VectorAsSlidersAttribute::max
	float ___max_2;
	// System.Int32 FlatLighting.VectorAsSlidersAttribute::dimensions
	int32_t ___dimensions_3;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t803388163, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((&___label_0), value);
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t803388163, ___min_1)); }
	inline float get_min_1() const { return ___min_1; }
	inline float* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(float value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t803388163, ___max_2)); }
	inline float get_max_2() const { return ___max_2; }
	inline float* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(float value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_dimensions_3() { return static_cast<int32_t>(offsetof(VectorAsSlidersAttribute_t803388163, ___dimensions_3)); }
	inline int32_t get_dimensions_3() const { return ___dimensions_3; }
	inline int32_t* get_address_of_dimensions_3() { return &___dimensions_3; }
	inline void set_dimensions_3(int32_t value)
	{
		___dimensions_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORASSLIDERSATTRIBUTE_T803388163_H
#ifndef U3CREMOTEMANAGERBLUETOOTHSTATEU3EC__ANONSTOREY0_T2360024038_H
#define U3CREMOTEMANAGERBLUETOOTHSTATEU3EC__ANONSTOREY0_T2360024038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/<RemoteManagerBluetoothState>c__AnonStorey0
struct  U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t2360024038  : public RuntimeObject
{
public:
	// BluetoothState NativeBridge/<RemoteManagerBluetoothState>c__AnonStorey0::bluetoothState
	int32_t ___bluetoothState_0;

public:
	inline static int32_t get_offset_of_bluetoothState_0() { return static_cast<int32_t>(offsetof(U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t2360024038, ___bluetoothState_0)); }
	inline int32_t get_bluetoothState_0() const { return ___bluetoothState_0; }
	inline int32_t* get_address_of_bluetoothState_0() { return &___bluetoothState_0; }
	inline void set_bluetoothState_0(int32_t value)
	{
		___bluetoothState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOTEMANAGERBLUETOOTHSTATEU3EC__ANONSTOREY0_T2360024038_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef MIRAREMOTEEXCEPTION_T3387773982_H
#define MIRAREMOTEEXCEPTION_T3387773982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraRemoteException
struct  MiraRemoteException_t3387773982  : public Exception_t1927440687
{
public:
	// MiraRemoteErrorCode MiraRemoteException::<errorCode>k__BackingField
	int32_t ___U3CerrorCodeU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CerrorCodeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MiraRemoteException_t3387773982, ___U3CerrorCodeU3Ek__BackingField_11)); }
	inline int32_t get_U3CerrorCodeU3Ek__BackingField_11() const { return ___U3CerrorCodeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CerrorCodeU3Ek__BackingField_11() { return &___U3CerrorCodeU3Ek__BackingField_11; }
	inline void set_U3CerrorCodeU3Ek__BackingField_11(int32_t value)
	{
		___U3CerrorCodeU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAREMOTEEXCEPTION_T3387773982_H
#ifndef REMOTEMOTIONSENSORINPUTEVENTHANDLER_T2876817765_H
#define REMOTEMOTIONSENSORINPUTEVENTHANDLER_T2876817765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler
struct  RemoteMotionSensorInputEventHandler_t2876817765  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONSENSORINPUTEVENTHANDLER_T2876817765_H
#ifndef MRINTCALLBACK_T2215762430_H
#define MRINTCALLBACK_T2215762430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRIntCallback
struct  MRIntCallback_t2215762430  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRINTCALLBACK_T2215762430_H
#ifndef REMOTEBUTTONINPUTEVENTHANDLER_T1063480517_H
#define REMOTEBUTTONINPUTEVENTHANDLER_T1063480517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteButtonInput/RemoteButtonInputEventHandler
struct  RemoteButtonInputEventHandler_t1063480517  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBUTTONINPUTEVENTHANDLER_T1063480517_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MREMPTYCALLBACK_T3969082922_H
#define MREMPTYCALLBACK_T3969082922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MREmptyCallback
struct  MREmptyCallback_t3969082922  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MREMPTYCALLBACK_T3969082922_H
#ifndef REMOTEMOTIONINPUTEVENTHANDLER_T1458362501_H
#define REMOTEMOTIONINPUTEVENTHANDLER_T1458362501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionInput/RemoteMotionInputEventHandler
struct  RemoteMotionInputEventHandler_t1458362501  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONINPUTEVENTHANDLER_T1458362501_H
#ifndef REMOTEDISCONNECTEDEVENTHANDLER_T730656759_H
#define REMOTEDISCONNECTEDEVENTHANDLER_T730656759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/RemoteDisconnectedEventHandler
struct  RemoteDisconnectedEventHandler_t730656759  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEDISCONNECTEDEVENTHANDLER_T730656759_H
#ifndef REMOTECONNECTEDEVENTHANDLER_T3456249665_H
#define REMOTECONNECTEDEVENTHANDLER_T3456249665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/RemoteConnectedEventHandler
struct  RemoteConnectedEventHandler_t3456249665  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTECONNECTEDEVENTHANDLER_T3456249665_H
#ifndef MRFLOATCALLBACK_T3699197057_H
#define MRFLOATCALLBACK_T3699197057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRFloatCallback
struct  MRFloatCallback_t3699197057  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRFLOATCALLBACK_T3699197057_H
#ifndef REMOTEORIENTATIONINPUTEVENTHANDLER_T4128243105_H
#define REMOTEORIENTATIONINPUTEVENTHANDLER_T4128243105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteOrientationInput/RemoteOrientationInputEventHandler
struct  RemoteOrientationInputEventHandler_t4128243105  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEORIENTATIONINPUTEVENTHANDLER_T4128243105_H
#ifndef REMOTEREFRESHEDEVENTHANDLER_T1036293937_H
#define REMOTEREFRESHEDEVENTHANDLER_T1036293937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Remote/RemoteRefreshedEventHandler
struct  RemoteRefreshedEventHandler_t1036293937  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEREFRESHEDEVENTHANDLER_T1036293937_H
#ifndef MRBOOLCALLBACK_T2221477487_H
#define MRBOOLCALLBACK_T2221477487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRBoolCallback
struct  MRBoolCallback_t2221477487  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRBOOLCALLBACK_T2221477487_H
#ifndef MRREMOTEMOTIONCALLBACK_T3634481787_H
#define MRREMOTEMOTIONCALLBACK_T3634481787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRRemoteMotionCallback
struct  MRRemoteMotionCallback_t3634481787  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRREMOTEMOTIONCALLBACK_T3634481787_H
#ifndef REMOTETOUCHINPUTEVENTHANDLER_T1375955781_H
#define REMOTETOUCHINPUTEVENTHANDLER_T1375955781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchInput/RemoteTouchInputEventHandler
struct  RemoteTouchInputEventHandler_t1375955781  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHINPUTEVENTHANDLER_T1375955781_H
#ifndef MRCONNECTEDREMOTECALLBACK_T3085918786_H
#define MRCONNECTEDREMOTECALLBACK_T3085918786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRConnectedRemoteCallback
struct  MRConnectedRemoteCallback_t3085918786  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRCONNECTEDREMOTECALLBACK_T3085918786_H
#ifndef REMOTETOUCHPADINPUTEVENTHANDLER_T2832767717_H
#define REMOTETOUCHPADINPUTEVENTHANDLER_T2832767717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchPadInput/RemoteTouchPadInputEventHandler
struct  RemoteTouchPadInputEventHandler_t2832767717  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHPADINPUTEVENTHANDLER_T2832767717_H
#ifndef MRDISCOVEREDREMOTECALLBACK_T3604605211_H
#define MRDISCOVEREDREMOTECALLBACK_T3604605211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRDiscoveredRemoteCallback
struct  MRDiscoveredRemoteCallback_t3604605211  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRDISCOVEREDREMOTECALLBACK_T3604605211_H
#ifndef REMOTEAXISINPUTEVENTHANDLER_T3796077797_H
#define REMOTEAXISINPUTEVENTHANDLER_T3796077797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteAxisInput/RemoteAxisInputEventHandler
struct  RemoteAxisInputEventHandler_t3796077797  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEAXISINPUTEVENTHANDLER_T3796077797_H
#ifndef MRERRORCALLBACK_T1972775801_H
#define MRERRORCALLBACK_T1972775801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeBridge/MRErrorCallback
struct  MRErrorCallback_t1972775801  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRERRORCALLBACK_T1972775801_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SINGLETON_1_T3428205882_H
#define SINGLETON_1_T3428205882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<GameManager>
struct  Singleton_1_t3428205882  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t3428205882_StaticFields
{
public:
	// T Singleton`1::instance
	GameManager_t2252321495 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3428205882_StaticFields, ___instance_2)); }
	inline GameManager_t2252321495 * get_instance_2() const { return ___instance_2; }
	inline GameManager_t2252321495 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GameManager_t2252321495 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3428205882_H
#ifndef SINGLETON_1_T1103622050_H
#define SINGLETON_1_T1103622050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<AudioManager>
struct  Singleton_1_t1103622050  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t1103622050_StaticFields
{
public:
	// T Singleton`1::instance
	AudioManager_t4222704959 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1103622050_StaticFields, ___instance_2)); }
	inline AudioManager_t4222704959 * get_instance_2() const { return ___instance_2; }
	inline AudioManager_t4222704959 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AudioManager_t4222704959 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1103622050_H
#ifndef LIGHTSOURCE_1_T46329857_H
#define LIGHTSOURCE_1_T46329857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.ShadowProjector>
struct  LightSource_1_t46329857  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t46329857, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t46329857_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t2584411587 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t46329857_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t46329857_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t46329857_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t46329857_StaticFields, ___lights_5)); }
	inline LightBag_t2584411587 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t2584411587 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t2584411587 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T46329857_H
#ifndef MATERIALBLENDER_T4011564903_H
#define MATERIALBLENDER_T4011564903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.MaterialBlender
struct  MaterialBlender_t4011564903  : public MonoBehaviour_t1158329972
{
public:
	// System.Single FlatLighting.MaterialBlender::durationSeconds
	float ___durationSeconds_2;
	// System.Boolean FlatLighting.MaterialBlender::loop
	bool ___loop_3;
	// UnityEngine.Material[] FlatLighting.MaterialBlender::materials
	MaterialU5BU5D_t3123989686* ___materials_4;
	// UnityEngine.Material FlatLighting.MaterialBlender::internalMaterial
	Material_t193706927 * ___internalMaterial_5;

public:
	inline static int32_t get_offset_of_durationSeconds_2() { return static_cast<int32_t>(offsetof(MaterialBlender_t4011564903, ___durationSeconds_2)); }
	inline float get_durationSeconds_2() const { return ___durationSeconds_2; }
	inline float* get_address_of_durationSeconds_2() { return &___durationSeconds_2; }
	inline void set_durationSeconds_2(float value)
	{
		___durationSeconds_2 = value;
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(MaterialBlender_t4011564903, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}

	inline static int32_t get_offset_of_materials_4() { return static_cast<int32_t>(offsetof(MaterialBlender_t4011564903, ___materials_4)); }
	inline MaterialU5BU5D_t3123989686* get_materials_4() const { return ___materials_4; }
	inline MaterialU5BU5D_t3123989686** get_address_of_materials_4() { return &___materials_4; }
	inline void set_materials_4(MaterialU5BU5D_t3123989686* value)
	{
		___materials_4 = value;
		Il2CppCodeGenWriteBarrier((&___materials_4), value);
	}

	inline static int32_t get_offset_of_internalMaterial_5() { return static_cast<int32_t>(offsetof(MaterialBlender_t4011564903, ___internalMaterial_5)); }
	inline Material_t193706927 * get_internalMaterial_5() const { return ___internalMaterial_5; }
	inline Material_t193706927 ** get_address_of_internalMaterial_5() { return &___internalMaterial_5; }
	inline void set_internalMaterial_5(Material_t193706927 * value)
	{
		___internalMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALBLENDER_T4011564903_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef SPINANIMATION_T4115072874_H
#define SPINANIMATION_T4115072874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpinAnimation
struct  SpinAnimation_t4115072874  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINANIMATION_T4115072874_H
#ifndef TRACKABLEBEHAVIOUR_T3643631172_H
#define TRACKABLEBEHAVIOUR_T3643631172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour
struct  TrackableBehaviour_t3643631172  : public MonoBehaviour_t1158329972
{
public:
	// System.String Wikitude.TrackableBehaviour::_targetPattern
	String_t* ____targetPattern_2;
	// System.Text.RegularExpressions.Regex Wikitude.TrackableBehaviour::_targetPatternRegex
	Regex_t1803876613 * ____targetPatternRegex_3;
	// System.Boolean Wikitude.TrackableBehaviour::_isKnown
	bool ____isKnown_4;
	// System.String Wikitude.TrackableBehaviour::_currentTargetName
	String_t* ____currentTargetName_5;
	// System.Boolean Wikitude.TrackableBehaviour::_extendedTracking
	bool ____extendedTracking_6;
	// System.String[] Wikitude.TrackableBehaviour::_targetsForExtendedTracking
	StringU5BU5D_t1642385972* ____targetsForExtendedTracking_7;
	// System.Boolean Wikitude.TrackableBehaviour::_autoToggleVisibility
	bool ____autoToggleVisibility_8;
	// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent Wikitude.TrackableBehaviour::OnEnterFieldOfVision
	OnEnterFieldOfVisionEvent_t3362888889 * ___OnEnterFieldOfVision_9;
	// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent Wikitude.TrackableBehaviour::OnExitFieldOfVision
	OnExitFieldOfVisionEvent_t2513344377 * ___OnExitFieldOfVision_10;
	// System.Boolean Wikitude.TrackableBehaviour::_eventsFoldout
	bool ____eventsFoldout_11;
	// System.Boolean Wikitude.TrackableBehaviour::_registeredToTracker
	bool ____registeredToTracker_12;
	// UnityEngine.Texture2D Wikitude.TrackableBehaviour::_preview
	Texture2D_t3542995729 * ____preview_13;
	// UnityEngine.Material Wikitude.TrackableBehaviour::_previewMaterial
	Material_t193706927 * ____previewMaterial_14;
	// UnityEngine.Mesh Wikitude.TrackableBehaviour::_previewMesh
	Mesh_t1356156583 * ____previewMesh_15;

public:
	inline static int32_t get_offset_of__targetPattern_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____targetPattern_2)); }
	inline String_t* get__targetPattern_2() const { return ____targetPattern_2; }
	inline String_t** get_address_of__targetPattern_2() { return &____targetPattern_2; }
	inline void set__targetPattern_2(String_t* value)
	{
		____targetPattern_2 = value;
		Il2CppCodeGenWriteBarrier((&____targetPattern_2), value);
	}

	inline static int32_t get_offset_of__targetPatternRegex_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____targetPatternRegex_3)); }
	inline Regex_t1803876613 * get__targetPatternRegex_3() const { return ____targetPatternRegex_3; }
	inline Regex_t1803876613 ** get_address_of__targetPatternRegex_3() { return &____targetPatternRegex_3; }
	inline void set__targetPatternRegex_3(Regex_t1803876613 * value)
	{
		____targetPatternRegex_3 = value;
		Il2CppCodeGenWriteBarrier((&____targetPatternRegex_3), value);
	}

	inline static int32_t get_offset_of__isKnown_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____isKnown_4)); }
	inline bool get__isKnown_4() const { return ____isKnown_4; }
	inline bool* get_address_of__isKnown_4() { return &____isKnown_4; }
	inline void set__isKnown_4(bool value)
	{
		____isKnown_4 = value;
	}

	inline static int32_t get_offset_of__currentTargetName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____currentTargetName_5)); }
	inline String_t* get__currentTargetName_5() const { return ____currentTargetName_5; }
	inline String_t** get_address_of__currentTargetName_5() { return &____currentTargetName_5; }
	inline void set__currentTargetName_5(String_t* value)
	{
		____currentTargetName_5 = value;
		Il2CppCodeGenWriteBarrier((&____currentTargetName_5), value);
	}

	inline static int32_t get_offset_of__extendedTracking_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____extendedTracking_6)); }
	inline bool get__extendedTracking_6() const { return ____extendedTracking_6; }
	inline bool* get_address_of__extendedTracking_6() { return &____extendedTracking_6; }
	inline void set__extendedTracking_6(bool value)
	{
		____extendedTracking_6 = value;
	}

	inline static int32_t get_offset_of__targetsForExtendedTracking_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____targetsForExtendedTracking_7)); }
	inline StringU5BU5D_t1642385972* get__targetsForExtendedTracking_7() const { return ____targetsForExtendedTracking_7; }
	inline StringU5BU5D_t1642385972** get_address_of__targetsForExtendedTracking_7() { return &____targetsForExtendedTracking_7; }
	inline void set__targetsForExtendedTracking_7(StringU5BU5D_t1642385972* value)
	{
		____targetsForExtendedTracking_7 = value;
		Il2CppCodeGenWriteBarrier((&____targetsForExtendedTracking_7), value);
	}

	inline static int32_t get_offset_of__autoToggleVisibility_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____autoToggleVisibility_8)); }
	inline bool get__autoToggleVisibility_8() const { return ____autoToggleVisibility_8; }
	inline bool* get_address_of__autoToggleVisibility_8() { return &____autoToggleVisibility_8; }
	inline void set__autoToggleVisibility_8(bool value)
	{
		____autoToggleVisibility_8 = value;
	}

	inline static int32_t get_offset_of_OnEnterFieldOfVision_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ___OnEnterFieldOfVision_9)); }
	inline OnEnterFieldOfVisionEvent_t3362888889 * get_OnEnterFieldOfVision_9() const { return ___OnEnterFieldOfVision_9; }
	inline OnEnterFieldOfVisionEvent_t3362888889 ** get_address_of_OnEnterFieldOfVision_9() { return &___OnEnterFieldOfVision_9; }
	inline void set_OnEnterFieldOfVision_9(OnEnterFieldOfVisionEvent_t3362888889 * value)
	{
		___OnEnterFieldOfVision_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnterFieldOfVision_9), value);
	}

	inline static int32_t get_offset_of_OnExitFieldOfVision_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ___OnExitFieldOfVision_10)); }
	inline OnExitFieldOfVisionEvent_t2513344377 * get_OnExitFieldOfVision_10() const { return ___OnExitFieldOfVision_10; }
	inline OnExitFieldOfVisionEvent_t2513344377 ** get_address_of_OnExitFieldOfVision_10() { return &___OnExitFieldOfVision_10; }
	inline void set_OnExitFieldOfVision_10(OnExitFieldOfVisionEvent_t2513344377 * value)
	{
		___OnExitFieldOfVision_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnExitFieldOfVision_10), value);
	}

	inline static int32_t get_offset_of__eventsFoldout_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____eventsFoldout_11)); }
	inline bool get__eventsFoldout_11() const { return ____eventsFoldout_11; }
	inline bool* get_address_of__eventsFoldout_11() { return &____eventsFoldout_11; }
	inline void set__eventsFoldout_11(bool value)
	{
		____eventsFoldout_11 = value;
	}

	inline static int32_t get_offset_of__registeredToTracker_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____registeredToTracker_12)); }
	inline bool get__registeredToTracker_12() const { return ____registeredToTracker_12; }
	inline bool* get_address_of__registeredToTracker_12() { return &____registeredToTracker_12; }
	inline void set__registeredToTracker_12(bool value)
	{
		____registeredToTracker_12 = value;
	}

	inline static int32_t get_offset_of__preview_13() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____preview_13)); }
	inline Texture2D_t3542995729 * get__preview_13() const { return ____preview_13; }
	inline Texture2D_t3542995729 ** get_address_of__preview_13() { return &____preview_13; }
	inline void set__preview_13(Texture2D_t3542995729 * value)
	{
		____preview_13 = value;
		Il2CppCodeGenWriteBarrier((&____preview_13), value);
	}

	inline static int32_t get_offset_of__previewMaterial_14() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____previewMaterial_14)); }
	inline Material_t193706927 * get__previewMaterial_14() const { return ____previewMaterial_14; }
	inline Material_t193706927 ** get_address_of__previewMaterial_14() { return &____previewMaterial_14; }
	inline void set__previewMaterial_14(Material_t193706927 * value)
	{
		____previewMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&____previewMaterial_14), value);
	}

	inline static int32_t get_offset_of__previewMesh_15() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t3643631172, ____previewMesh_15)); }
	inline Mesh_t1356156583 * get__previewMesh_15() const { return ____previewMesh_15; }
	inline Mesh_t1356156583 ** get_address_of__previewMesh_15() { return &____previewMesh_15; }
	inline void set__previewMesh_15(Mesh_t1356156583 * value)
	{
		____previewMesh_15 = value;
		Il2CppCodeGenWriteBarrier((&____previewMesh_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T3643631172_H
#ifndef MOUSEORBIT_T3219898523_H
#define MOUSEORBIT_T3219898523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.MouseOrbit
struct  MouseOrbit_t3219898523  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform FlatLighting.MouseOrbit::target
	Transform_t3275118058 * ___target_2;
	// System.Single FlatLighting.MouseOrbit::yPosClamp
	float ___yPosClamp_3;
	// System.Single FlatLighting.MouseOrbit::targetFocusLerpSpeed
	float ___targetFocusLerpSpeed_4;
	// System.Single FlatLighting.MouseOrbit::initialDistance
	float ___initialDistance_5;
	// System.Single FlatLighting.MouseOrbit::smooth
	float ___smooth_6;
	// System.Single FlatLighting.MouseOrbit::xSpeed
	float ___xSpeed_7;
	// System.Single FlatLighting.MouseOrbit::ySpeed
	float ___ySpeed_8;
	// System.Single FlatLighting.MouseOrbit::wasdSpeed
	float ___wasdSpeed_9;
	// System.Single FlatLighting.MouseOrbit::wasdSmooth
	float ___wasdSmooth_10;
	// System.Single FlatLighting.MouseOrbit::panSpeed
	float ___panSpeed_11;
	// System.Single FlatLighting.MouseOrbit::zoomSmooth
	float ___zoomSmooth_12;
	// System.Single FlatLighting.MouseOrbit::minZoom
	float ___minZoom_13;
	// System.Single FlatLighting.MouseOrbit::maxZoom
	float ___maxZoom_14;
	// System.Single FlatLighting.MouseOrbit::mouseSensitivityScaler
	float ___mouseSensitivityScaler_15;
	// System.Single FlatLighting.MouseOrbit::scrollSensitivityScaler
	float ___scrollSensitivityScaler_16;
	// System.Single FlatLighting.MouseOrbit::distance
	float ___distance_17;
	// System.Single FlatLighting.MouseOrbit::yMaxLimit
	float ___yMaxLimit_18;
	// System.Single FlatLighting.MouseOrbit::yMinLimit
	float ___yMinLimit_19;
	// System.Single FlatLighting.MouseOrbit::smoothY
	float ___smoothY_20;
	// System.Single FlatLighting.MouseOrbit::smoothX
	float ___smoothX_21;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::movementPOS
	Vector3_t2243707580  ___movementPOS_22;
	// UnityEngine.Quaternion FlatLighting.MouseOrbit::rotation
	Quaternion_t4030073918  ___rotation_23;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::position
	Vector3_t2243707580  ___position_24;
	// System.Single FlatLighting.MouseOrbit::clickTimey
	float ___clickTimey_25;
	// UnityEngine.Transform FlatLighting.MouseOrbit::myTransform
	Transform_t3275118058 * ___myTransform_26;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::posToBe
	Vector3_t2243707580  ___posToBe_27;
	// System.Single FlatLighting.MouseOrbit::zoomSmoothDelegate
	float ___zoomSmoothDelegate_28;
	// System.Single FlatLighting.MouseOrbit::x
	float ___x_29;
	// System.Single FlatLighting.MouseOrbit::y
	float ___y_30;
	// UnityEngine.Vector3 FlatLighting.MouseOrbit::movementPosOffset
	Vector3_t2243707580  ___movementPosOffset_31;
	// System.Boolean FlatLighting.MouseOrbit::isOrthographic
	bool ___isOrthographic_32;
	// UnityEngine.Camera FlatLighting.MouseOrbit::myCamera
	Camera_t189460977 * ___myCamera_33;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___target_2)); }
	inline Transform_t3275118058 * get_target_2() const { return ___target_2; }
	inline Transform_t3275118058 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3275118058 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_yPosClamp_3() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___yPosClamp_3)); }
	inline float get_yPosClamp_3() const { return ___yPosClamp_3; }
	inline float* get_address_of_yPosClamp_3() { return &___yPosClamp_3; }
	inline void set_yPosClamp_3(float value)
	{
		___yPosClamp_3 = value;
	}

	inline static int32_t get_offset_of_targetFocusLerpSpeed_4() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___targetFocusLerpSpeed_4)); }
	inline float get_targetFocusLerpSpeed_4() const { return ___targetFocusLerpSpeed_4; }
	inline float* get_address_of_targetFocusLerpSpeed_4() { return &___targetFocusLerpSpeed_4; }
	inline void set_targetFocusLerpSpeed_4(float value)
	{
		___targetFocusLerpSpeed_4 = value;
	}

	inline static int32_t get_offset_of_initialDistance_5() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___initialDistance_5)); }
	inline float get_initialDistance_5() const { return ___initialDistance_5; }
	inline float* get_address_of_initialDistance_5() { return &___initialDistance_5; }
	inline void set_initialDistance_5(float value)
	{
		___initialDistance_5 = value;
	}

	inline static int32_t get_offset_of_smooth_6() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___smooth_6)); }
	inline float get_smooth_6() const { return ___smooth_6; }
	inline float* get_address_of_smooth_6() { return &___smooth_6; }
	inline void set_smooth_6(float value)
	{
		___smooth_6 = value;
	}

	inline static int32_t get_offset_of_xSpeed_7() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___xSpeed_7)); }
	inline float get_xSpeed_7() const { return ___xSpeed_7; }
	inline float* get_address_of_xSpeed_7() { return &___xSpeed_7; }
	inline void set_xSpeed_7(float value)
	{
		___xSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ySpeed_8() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___ySpeed_8)); }
	inline float get_ySpeed_8() const { return ___ySpeed_8; }
	inline float* get_address_of_ySpeed_8() { return &___ySpeed_8; }
	inline void set_ySpeed_8(float value)
	{
		___ySpeed_8 = value;
	}

	inline static int32_t get_offset_of_wasdSpeed_9() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___wasdSpeed_9)); }
	inline float get_wasdSpeed_9() const { return ___wasdSpeed_9; }
	inline float* get_address_of_wasdSpeed_9() { return &___wasdSpeed_9; }
	inline void set_wasdSpeed_9(float value)
	{
		___wasdSpeed_9 = value;
	}

	inline static int32_t get_offset_of_wasdSmooth_10() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___wasdSmooth_10)); }
	inline float get_wasdSmooth_10() const { return ___wasdSmooth_10; }
	inline float* get_address_of_wasdSmooth_10() { return &___wasdSmooth_10; }
	inline void set_wasdSmooth_10(float value)
	{
		___wasdSmooth_10 = value;
	}

	inline static int32_t get_offset_of_panSpeed_11() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___panSpeed_11)); }
	inline float get_panSpeed_11() const { return ___panSpeed_11; }
	inline float* get_address_of_panSpeed_11() { return &___panSpeed_11; }
	inline void set_panSpeed_11(float value)
	{
		___panSpeed_11 = value;
	}

	inline static int32_t get_offset_of_zoomSmooth_12() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___zoomSmooth_12)); }
	inline float get_zoomSmooth_12() const { return ___zoomSmooth_12; }
	inline float* get_address_of_zoomSmooth_12() { return &___zoomSmooth_12; }
	inline void set_zoomSmooth_12(float value)
	{
		___zoomSmooth_12 = value;
	}

	inline static int32_t get_offset_of_minZoom_13() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___minZoom_13)); }
	inline float get_minZoom_13() const { return ___minZoom_13; }
	inline float* get_address_of_minZoom_13() { return &___minZoom_13; }
	inline void set_minZoom_13(float value)
	{
		___minZoom_13 = value;
	}

	inline static int32_t get_offset_of_maxZoom_14() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___maxZoom_14)); }
	inline float get_maxZoom_14() const { return ___maxZoom_14; }
	inline float* get_address_of_maxZoom_14() { return &___maxZoom_14; }
	inline void set_maxZoom_14(float value)
	{
		___maxZoom_14 = value;
	}

	inline static int32_t get_offset_of_mouseSensitivityScaler_15() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___mouseSensitivityScaler_15)); }
	inline float get_mouseSensitivityScaler_15() const { return ___mouseSensitivityScaler_15; }
	inline float* get_address_of_mouseSensitivityScaler_15() { return &___mouseSensitivityScaler_15; }
	inline void set_mouseSensitivityScaler_15(float value)
	{
		___mouseSensitivityScaler_15 = value;
	}

	inline static int32_t get_offset_of_scrollSensitivityScaler_16() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___scrollSensitivityScaler_16)); }
	inline float get_scrollSensitivityScaler_16() const { return ___scrollSensitivityScaler_16; }
	inline float* get_address_of_scrollSensitivityScaler_16() { return &___scrollSensitivityScaler_16; }
	inline void set_scrollSensitivityScaler_16(float value)
	{
		___scrollSensitivityScaler_16 = value;
	}

	inline static int32_t get_offset_of_distance_17() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___distance_17)); }
	inline float get_distance_17() const { return ___distance_17; }
	inline float* get_address_of_distance_17() { return &___distance_17; }
	inline void set_distance_17(float value)
	{
		___distance_17 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_18() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___yMaxLimit_18)); }
	inline float get_yMaxLimit_18() const { return ___yMaxLimit_18; }
	inline float* get_address_of_yMaxLimit_18() { return &___yMaxLimit_18; }
	inline void set_yMaxLimit_18(float value)
	{
		___yMaxLimit_18 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_19() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___yMinLimit_19)); }
	inline float get_yMinLimit_19() const { return ___yMinLimit_19; }
	inline float* get_address_of_yMinLimit_19() { return &___yMinLimit_19; }
	inline void set_yMinLimit_19(float value)
	{
		___yMinLimit_19 = value;
	}

	inline static int32_t get_offset_of_smoothY_20() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___smoothY_20)); }
	inline float get_smoothY_20() const { return ___smoothY_20; }
	inline float* get_address_of_smoothY_20() { return &___smoothY_20; }
	inline void set_smoothY_20(float value)
	{
		___smoothY_20 = value;
	}

	inline static int32_t get_offset_of_smoothX_21() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___smoothX_21)); }
	inline float get_smoothX_21() const { return ___smoothX_21; }
	inline float* get_address_of_smoothX_21() { return &___smoothX_21; }
	inline void set_smoothX_21(float value)
	{
		___smoothX_21 = value;
	}

	inline static int32_t get_offset_of_movementPOS_22() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___movementPOS_22)); }
	inline Vector3_t2243707580  get_movementPOS_22() const { return ___movementPOS_22; }
	inline Vector3_t2243707580 * get_address_of_movementPOS_22() { return &___movementPOS_22; }
	inline void set_movementPOS_22(Vector3_t2243707580  value)
	{
		___movementPOS_22 = value;
	}

	inline static int32_t get_offset_of_rotation_23() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___rotation_23)); }
	inline Quaternion_t4030073918  get_rotation_23() const { return ___rotation_23; }
	inline Quaternion_t4030073918 * get_address_of_rotation_23() { return &___rotation_23; }
	inline void set_rotation_23(Quaternion_t4030073918  value)
	{
		___rotation_23 = value;
	}

	inline static int32_t get_offset_of_position_24() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___position_24)); }
	inline Vector3_t2243707580  get_position_24() const { return ___position_24; }
	inline Vector3_t2243707580 * get_address_of_position_24() { return &___position_24; }
	inline void set_position_24(Vector3_t2243707580  value)
	{
		___position_24 = value;
	}

	inline static int32_t get_offset_of_clickTimey_25() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___clickTimey_25)); }
	inline float get_clickTimey_25() const { return ___clickTimey_25; }
	inline float* get_address_of_clickTimey_25() { return &___clickTimey_25; }
	inline void set_clickTimey_25(float value)
	{
		___clickTimey_25 = value;
	}

	inline static int32_t get_offset_of_myTransform_26() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___myTransform_26)); }
	inline Transform_t3275118058 * get_myTransform_26() const { return ___myTransform_26; }
	inline Transform_t3275118058 ** get_address_of_myTransform_26() { return &___myTransform_26; }
	inline void set_myTransform_26(Transform_t3275118058 * value)
	{
		___myTransform_26 = value;
		Il2CppCodeGenWriteBarrier((&___myTransform_26), value);
	}

	inline static int32_t get_offset_of_posToBe_27() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___posToBe_27)); }
	inline Vector3_t2243707580  get_posToBe_27() const { return ___posToBe_27; }
	inline Vector3_t2243707580 * get_address_of_posToBe_27() { return &___posToBe_27; }
	inline void set_posToBe_27(Vector3_t2243707580  value)
	{
		___posToBe_27 = value;
	}

	inline static int32_t get_offset_of_zoomSmoothDelegate_28() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___zoomSmoothDelegate_28)); }
	inline float get_zoomSmoothDelegate_28() const { return ___zoomSmoothDelegate_28; }
	inline float* get_address_of_zoomSmoothDelegate_28() { return &___zoomSmoothDelegate_28; }
	inline void set_zoomSmoothDelegate_28(float value)
	{
		___zoomSmoothDelegate_28 = value;
	}

	inline static int32_t get_offset_of_x_29() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___x_29)); }
	inline float get_x_29() const { return ___x_29; }
	inline float* get_address_of_x_29() { return &___x_29; }
	inline void set_x_29(float value)
	{
		___x_29 = value;
	}

	inline static int32_t get_offset_of_y_30() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___y_30)); }
	inline float get_y_30() const { return ___y_30; }
	inline float* get_address_of_y_30() { return &___y_30; }
	inline void set_y_30(float value)
	{
		___y_30 = value;
	}

	inline static int32_t get_offset_of_movementPosOffset_31() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___movementPosOffset_31)); }
	inline Vector3_t2243707580  get_movementPosOffset_31() const { return ___movementPosOffset_31; }
	inline Vector3_t2243707580 * get_address_of_movementPosOffset_31() { return &___movementPosOffset_31; }
	inline void set_movementPosOffset_31(Vector3_t2243707580  value)
	{
		___movementPosOffset_31 = value;
	}

	inline static int32_t get_offset_of_isOrthographic_32() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___isOrthographic_32)); }
	inline bool get_isOrthographic_32() const { return ___isOrthographic_32; }
	inline bool* get_address_of_isOrthographic_32() { return &___isOrthographic_32; }
	inline void set_isOrthographic_32(bool value)
	{
		___isOrthographic_32 = value;
	}

	inline static int32_t get_offset_of_myCamera_33() { return static_cast<int32_t>(offsetof(MouseOrbit_t3219898523, ___myCamera_33)); }
	inline Camera_t189460977 * get_myCamera_33() const { return ___myCamera_33; }
	inline Camera_t189460977 ** get_address_of_myCamera_33() { return &___myCamera_33; }
	inline void set_myCamera_33(Camera_t189460977 * value)
	{
		___myCamera_33 = value;
		Il2CppCodeGenWriteBarrier((&___myCamera_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORBIT_T3219898523_H
#ifndef MATERIALSWAPPER_T2559573863_H
#define MATERIALSWAPPER_T2559573863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialSwapper
struct  MaterialSwapper_t2559573863  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material[] MaterialSwapper::materials
	MaterialU5BU5D_t3123989686* ___materials_2;
	// UnityEngine.GameObject MaterialSwapper::UnityLightsRoot
	GameObject_t1756533147 * ___UnityLightsRoot_3;
	// System.Int32 MaterialSwapper::UnityLightsMaterialIndex
	int32_t ___UnityLightsMaterialIndex_4;
	// UnityEngine.GameObject MaterialSwapper::FlatLightsRoot
	GameObject_t1756533147 * ___FlatLightsRoot_5;
	// System.Int32 MaterialSwapper::FlatLightsMaterialIndex
	int32_t ___FlatLightsMaterialIndex_6;
	// UnityEngine.Renderer MaterialSwapper::myRenderer
	Renderer_t257310565 * ___myRenderer_7;

public:
	inline static int32_t get_offset_of_materials_2() { return static_cast<int32_t>(offsetof(MaterialSwapper_t2559573863, ___materials_2)); }
	inline MaterialU5BU5D_t3123989686* get_materials_2() const { return ___materials_2; }
	inline MaterialU5BU5D_t3123989686** get_address_of_materials_2() { return &___materials_2; }
	inline void set_materials_2(MaterialU5BU5D_t3123989686* value)
	{
		___materials_2 = value;
		Il2CppCodeGenWriteBarrier((&___materials_2), value);
	}

	inline static int32_t get_offset_of_UnityLightsRoot_3() { return static_cast<int32_t>(offsetof(MaterialSwapper_t2559573863, ___UnityLightsRoot_3)); }
	inline GameObject_t1756533147 * get_UnityLightsRoot_3() const { return ___UnityLightsRoot_3; }
	inline GameObject_t1756533147 ** get_address_of_UnityLightsRoot_3() { return &___UnityLightsRoot_3; }
	inline void set_UnityLightsRoot_3(GameObject_t1756533147 * value)
	{
		___UnityLightsRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___UnityLightsRoot_3), value);
	}

	inline static int32_t get_offset_of_UnityLightsMaterialIndex_4() { return static_cast<int32_t>(offsetof(MaterialSwapper_t2559573863, ___UnityLightsMaterialIndex_4)); }
	inline int32_t get_UnityLightsMaterialIndex_4() const { return ___UnityLightsMaterialIndex_4; }
	inline int32_t* get_address_of_UnityLightsMaterialIndex_4() { return &___UnityLightsMaterialIndex_4; }
	inline void set_UnityLightsMaterialIndex_4(int32_t value)
	{
		___UnityLightsMaterialIndex_4 = value;
	}

	inline static int32_t get_offset_of_FlatLightsRoot_5() { return static_cast<int32_t>(offsetof(MaterialSwapper_t2559573863, ___FlatLightsRoot_5)); }
	inline GameObject_t1756533147 * get_FlatLightsRoot_5() const { return ___FlatLightsRoot_5; }
	inline GameObject_t1756533147 ** get_address_of_FlatLightsRoot_5() { return &___FlatLightsRoot_5; }
	inline void set_FlatLightsRoot_5(GameObject_t1756533147 * value)
	{
		___FlatLightsRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___FlatLightsRoot_5), value);
	}

	inline static int32_t get_offset_of_FlatLightsMaterialIndex_6() { return static_cast<int32_t>(offsetof(MaterialSwapper_t2559573863, ___FlatLightsMaterialIndex_6)); }
	inline int32_t get_FlatLightsMaterialIndex_6() const { return ___FlatLightsMaterialIndex_6; }
	inline int32_t* get_address_of_FlatLightsMaterialIndex_6() { return &___FlatLightsMaterialIndex_6; }
	inline void set_FlatLightsMaterialIndex_6(int32_t value)
	{
		___FlatLightsMaterialIndex_6 = value;
	}

	inline static int32_t get_offset_of_myRenderer_7() { return static_cast<int32_t>(offsetof(MaterialSwapper_t2559573863, ___myRenderer_7)); }
	inline Renderer_t257310565 * get_myRenderer_7() const { return ___myRenderer_7; }
	inline Renderer_t257310565 ** get_address_of_myRenderer_7() { return &___myRenderer_7; }
	inline void set_myRenderer_7(Renderer_t257310565 * value)
	{
		___myRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___myRenderer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSWAPPER_T2559573863_H
#ifndef LIGHTINGMANAGER_T1089587063_H
#define LIGHTINGMANAGER_T1089587063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightingManager
struct  LightingManager_t1089587063  : public MonoBehaviour_t1158329972
{
public:
	// SceneLigtingSetup LightingManager::day
	SceneLigtingSetup_t3978848013 * ___day_2;
	// SceneLigtingSetup LightingManager::night
	SceneLigtingSetup_t3978848013 * ___night_3;
	// UnityEngine.GameObject LightingManager::root
	GameObject_t1756533147 * ___root_4;

public:
	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(LightingManager_t1089587063, ___day_2)); }
	inline SceneLigtingSetup_t3978848013 * get_day_2() const { return ___day_2; }
	inline SceneLigtingSetup_t3978848013 ** get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(SceneLigtingSetup_t3978848013 * value)
	{
		___day_2 = value;
		Il2CppCodeGenWriteBarrier((&___day_2), value);
	}

	inline static int32_t get_offset_of_night_3() { return static_cast<int32_t>(offsetof(LightingManager_t1089587063, ___night_3)); }
	inline SceneLigtingSetup_t3978848013 * get_night_3() const { return ___night_3; }
	inline SceneLigtingSetup_t3978848013 ** get_address_of_night_3() { return &___night_3; }
	inline void set_night_3(SceneLigtingSetup_t3978848013 * value)
	{
		___night_3 = value;
		Il2CppCodeGenWriteBarrier((&___night_3), value);
	}

	inline static int32_t get_offset_of_root_4() { return static_cast<int32_t>(offsetof(LightingManager_t1089587063, ___root_4)); }
	inline GameObject_t1756533147 * get_root_4() const { return ___root_4; }
	inline GameObject_t1756533147 ** get_address_of_root_4() { return &___root_4; }
	inline void set_root_4(GameObject_t1756533147 * value)
	{
		___root_4 = value;
		Il2CppCodeGenWriteBarrier((&___root_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTINGMANAGER_T1089587063_H
#ifndef SCENELIGTINGSETUP_T3978848013_H
#define SCENELIGTINGSETUP_T3978848013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLigtingSetup
struct  SceneLigtingSetup_t3978848013  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color SceneLigtingSetup::cameraBackground
	Color_t2020392075  ___cameraBackground_2;
	// UnityEngine.Material SceneLigtingSetup::globalMaterial
	Material_t193706927 * ___globalMaterial_3;
	// UnityEngine.Material SceneLigtingSetup::vegetationMaterial
	Material_t193706927 * ___vegetationMaterial_4;
	// UnityEngine.Transform SceneLigtingSetup::vegetationRoot
	Transform_t3275118058 * ___vegetationRoot_5;
	// UnityEngine.Material SceneLigtingSetup::bridgeMaterial
	Material_t193706927 * ___bridgeMaterial_6;
	// UnityEngine.Transform SceneLigtingSetup::bridgeRoot
	Transform_t3275118058 * ___bridgeRoot_7;
	// UnityEngine.Material SceneLigtingSetup::lookoutMaterial
	Material_t193706927 * ___lookoutMaterial_8;
	// UnityEngine.Transform SceneLigtingSetup::lookoutRoot
	Transform_t3275118058 * ___lookoutRoot_9;
	// UnityEngine.Material SceneLigtingSetup::towerMaterial
	Material_t193706927 * ___towerMaterial_10;
	// UnityEngine.Transform SceneLigtingSetup::towerRoot
	Transform_t3275118058 * ___towerRoot_11;
	// UnityEngine.Material SceneLigtingSetup::deadTreeMaterial
	Material_t193706927 * ___deadTreeMaterial_12;
	// UnityEngine.Transform SceneLigtingSetup::deadTreeRoot
	Transform_t3275118058 * ___deadTreeRoot_13;
	// UnityEngine.Material SceneLigtingSetup::rocksMaterial
	Material_t193706927 * ___rocksMaterial_14;
	// UnityEngine.Transform SceneLigtingSetup::rocksTreeRoot
	Transform_t3275118058 * ___rocksTreeRoot_15;
	// UnityEngine.GameObject[] SceneLigtingSetup::objectsToEnable
	GameObjectU5BU5D_t3057952154* ___objectsToEnable_16;
	// UnityEngine.Renderer[] SceneLigtingSetup::sceneRenderers
	RendererU5BU5D_t2810717544* ___sceneRenderers_17;
	// UnityEngine.Renderer[] SceneLigtingSetup::vegetationRenderers
	RendererU5BU5D_t2810717544* ___vegetationRenderers_18;
	// UnityEngine.Renderer[] SceneLigtingSetup::bridgeRenderers
	RendererU5BU5D_t2810717544* ___bridgeRenderers_19;
	// UnityEngine.Renderer[] SceneLigtingSetup::lookoutRenderers
	RendererU5BU5D_t2810717544* ___lookoutRenderers_20;
	// UnityEngine.Renderer[] SceneLigtingSetup::towerRenderers
	RendererU5BU5D_t2810717544* ___towerRenderers_21;
	// UnityEngine.Renderer[] SceneLigtingSetup::deadTreeRenderers
	RendererU5BU5D_t2810717544* ___deadTreeRenderers_22;
	// UnityEngine.Renderer[] SceneLigtingSetup::boatRenderers
	RendererU5BU5D_t2810717544* ___boatRenderers_23;
	// UnityEngine.Renderer[] SceneLigtingSetup::rocksRenderers
	RendererU5BU5D_t2810717544* ___rocksRenderers_24;

public:
	inline static int32_t get_offset_of_cameraBackground_2() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___cameraBackground_2)); }
	inline Color_t2020392075  get_cameraBackground_2() const { return ___cameraBackground_2; }
	inline Color_t2020392075 * get_address_of_cameraBackground_2() { return &___cameraBackground_2; }
	inline void set_cameraBackground_2(Color_t2020392075  value)
	{
		___cameraBackground_2 = value;
	}

	inline static int32_t get_offset_of_globalMaterial_3() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___globalMaterial_3)); }
	inline Material_t193706927 * get_globalMaterial_3() const { return ___globalMaterial_3; }
	inline Material_t193706927 ** get_address_of_globalMaterial_3() { return &___globalMaterial_3; }
	inline void set_globalMaterial_3(Material_t193706927 * value)
	{
		___globalMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___globalMaterial_3), value);
	}

	inline static int32_t get_offset_of_vegetationMaterial_4() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___vegetationMaterial_4)); }
	inline Material_t193706927 * get_vegetationMaterial_4() const { return ___vegetationMaterial_4; }
	inline Material_t193706927 ** get_address_of_vegetationMaterial_4() { return &___vegetationMaterial_4; }
	inline void set_vegetationMaterial_4(Material_t193706927 * value)
	{
		___vegetationMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___vegetationMaterial_4), value);
	}

	inline static int32_t get_offset_of_vegetationRoot_5() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___vegetationRoot_5)); }
	inline Transform_t3275118058 * get_vegetationRoot_5() const { return ___vegetationRoot_5; }
	inline Transform_t3275118058 ** get_address_of_vegetationRoot_5() { return &___vegetationRoot_5; }
	inline void set_vegetationRoot_5(Transform_t3275118058 * value)
	{
		___vegetationRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___vegetationRoot_5), value);
	}

	inline static int32_t get_offset_of_bridgeMaterial_6() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___bridgeMaterial_6)); }
	inline Material_t193706927 * get_bridgeMaterial_6() const { return ___bridgeMaterial_6; }
	inline Material_t193706927 ** get_address_of_bridgeMaterial_6() { return &___bridgeMaterial_6; }
	inline void set_bridgeMaterial_6(Material_t193706927 * value)
	{
		___bridgeMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___bridgeMaterial_6), value);
	}

	inline static int32_t get_offset_of_bridgeRoot_7() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___bridgeRoot_7)); }
	inline Transform_t3275118058 * get_bridgeRoot_7() const { return ___bridgeRoot_7; }
	inline Transform_t3275118058 ** get_address_of_bridgeRoot_7() { return &___bridgeRoot_7; }
	inline void set_bridgeRoot_7(Transform_t3275118058 * value)
	{
		___bridgeRoot_7 = value;
		Il2CppCodeGenWriteBarrier((&___bridgeRoot_7), value);
	}

	inline static int32_t get_offset_of_lookoutMaterial_8() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___lookoutMaterial_8)); }
	inline Material_t193706927 * get_lookoutMaterial_8() const { return ___lookoutMaterial_8; }
	inline Material_t193706927 ** get_address_of_lookoutMaterial_8() { return &___lookoutMaterial_8; }
	inline void set_lookoutMaterial_8(Material_t193706927 * value)
	{
		___lookoutMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___lookoutMaterial_8), value);
	}

	inline static int32_t get_offset_of_lookoutRoot_9() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___lookoutRoot_9)); }
	inline Transform_t3275118058 * get_lookoutRoot_9() const { return ___lookoutRoot_9; }
	inline Transform_t3275118058 ** get_address_of_lookoutRoot_9() { return &___lookoutRoot_9; }
	inline void set_lookoutRoot_9(Transform_t3275118058 * value)
	{
		___lookoutRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&___lookoutRoot_9), value);
	}

	inline static int32_t get_offset_of_towerMaterial_10() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___towerMaterial_10)); }
	inline Material_t193706927 * get_towerMaterial_10() const { return ___towerMaterial_10; }
	inline Material_t193706927 ** get_address_of_towerMaterial_10() { return &___towerMaterial_10; }
	inline void set_towerMaterial_10(Material_t193706927 * value)
	{
		___towerMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___towerMaterial_10), value);
	}

	inline static int32_t get_offset_of_towerRoot_11() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___towerRoot_11)); }
	inline Transform_t3275118058 * get_towerRoot_11() const { return ___towerRoot_11; }
	inline Transform_t3275118058 ** get_address_of_towerRoot_11() { return &___towerRoot_11; }
	inline void set_towerRoot_11(Transform_t3275118058 * value)
	{
		___towerRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&___towerRoot_11), value);
	}

	inline static int32_t get_offset_of_deadTreeMaterial_12() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___deadTreeMaterial_12)); }
	inline Material_t193706927 * get_deadTreeMaterial_12() const { return ___deadTreeMaterial_12; }
	inline Material_t193706927 ** get_address_of_deadTreeMaterial_12() { return &___deadTreeMaterial_12; }
	inline void set_deadTreeMaterial_12(Material_t193706927 * value)
	{
		___deadTreeMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___deadTreeMaterial_12), value);
	}

	inline static int32_t get_offset_of_deadTreeRoot_13() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___deadTreeRoot_13)); }
	inline Transform_t3275118058 * get_deadTreeRoot_13() const { return ___deadTreeRoot_13; }
	inline Transform_t3275118058 ** get_address_of_deadTreeRoot_13() { return &___deadTreeRoot_13; }
	inline void set_deadTreeRoot_13(Transform_t3275118058 * value)
	{
		___deadTreeRoot_13 = value;
		Il2CppCodeGenWriteBarrier((&___deadTreeRoot_13), value);
	}

	inline static int32_t get_offset_of_rocksMaterial_14() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___rocksMaterial_14)); }
	inline Material_t193706927 * get_rocksMaterial_14() const { return ___rocksMaterial_14; }
	inline Material_t193706927 ** get_address_of_rocksMaterial_14() { return &___rocksMaterial_14; }
	inline void set_rocksMaterial_14(Material_t193706927 * value)
	{
		___rocksMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___rocksMaterial_14), value);
	}

	inline static int32_t get_offset_of_rocksTreeRoot_15() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___rocksTreeRoot_15)); }
	inline Transform_t3275118058 * get_rocksTreeRoot_15() const { return ___rocksTreeRoot_15; }
	inline Transform_t3275118058 ** get_address_of_rocksTreeRoot_15() { return &___rocksTreeRoot_15; }
	inline void set_rocksTreeRoot_15(Transform_t3275118058 * value)
	{
		___rocksTreeRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&___rocksTreeRoot_15), value);
	}

	inline static int32_t get_offset_of_objectsToEnable_16() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___objectsToEnable_16)); }
	inline GameObjectU5BU5D_t3057952154* get_objectsToEnable_16() const { return ___objectsToEnable_16; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_objectsToEnable_16() { return &___objectsToEnable_16; }
	inline void set_objectsToEnable_16(GameObjectU5BU5D_t3057952154* value)
	{
		___objectsToEnable_16 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToEnable_16), value);
	}

	inline static int32_t get_offset_of_sceneRenderers_17() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___sceneRenderers_17)); }
	inline RendererU5BU5D_t2810717544* get_sceneRenderers_17() const { return ___sceneRenderers_17; }
	inline RendererU5BU5D_t2810717544** get_address_of_sceneRenderers_17() { return &___sceneRenderers_17; }
	inline void set_sceneRenderers_17(RendererU5BU5D_t2810717544* value)
	{
		___sceneRenderers_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneRenderers_17), value);
	}

	inline static int32_t get_offset_of_vegetationRenderers_18() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___vegetationRenderers_18)); }
	inline RendererU5BU5D_t2810717544* get_vegetationRenderers_18() const { return ___vegetationRenderers_18; }
	inline RendererU5BU5D_t2810717544** get_address_of_vegetationRenderers_18() { return &___vegetationRenderers_18; }
	inline void set_vegetationRenderers_18(RendererU5BU5D_t2810717544* value)
	{
		___vegetationRenderers_18 = value;
		Il2CppCodeGenWriteBarrier((&___vegetationRenderers_18), value);
	}

	inline static int32_t get_offset_of_bridgeRenderers_19() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___bridgeRenderers_19)); }
	inline RendererU5BU5D_t2810717544* get_bridgeRenderers_19() const { return ___bridgeRenderers_19; }
	inline RendererU5BU5D_t2810717544** get_address_of_bridgeRenderers_19() { return &___bridgeRenderers_19; }
	inline void set_bridgeRenderers_19(RendererU5BU5D_t2810717544* value)
	{
		___bridgeRenderers_19 = value;
		Il2CppCodeGenWriteBarrier((&___bridgeRenderers_19), value);
	}

	inline static int32_t get_offset_of_lookoutRenderers_20() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___lookoutRenderers_20)); }
	inline RendererU5BU5D_t2810717544* get_lookoutRenderers_20() const { return ___lookoutRenderers_20; }
	inline RendererU5BU5D_t2810717544** get_address_of_lookoutRenderers_20() { return &___lookoutRenderers_20; }
	inline void set_lookoutRenderers_20(RendererU5BU5D_t2810717544* value)
	{
		___lookoutRenderers_20 = value;
		Il2CppCodeGenWriteBarrier((&___lookoutRenderers_20), value);
	}

	inline static int32_t get_offset_of_towerRenderers_21() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___towerRenderers_21)); }
	inline RendererU5BU5D_t2810717544* get_towerRenderers_21() const { return ___towerRenderers_21; }
	inline RendererU5BU5D_t2810717544** get_address_of_towerRenderers_21() { return &___towerRenderers_21; }
	inline void set_towerRenderers_21(RendererU5BU5D_t2810717544* value)
	{
		___towerRenderers_21 = value;
		Il2CppCodeGenWriteBarrier((&___towerRenderers_21), value);
	}

	inline static int32_t get_offset_of_deadTreeRenderers_22() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___deadTreeRenderers_22)); }
	inline RendererU5BU5D_t2810717544* get_deadTreeRenderers_22() const { return ___deadTreeRenderers_22; }
	inline RendererU5BU5D_t2810717544** get_address_of_deadTreeRenderers_22() { return &___deadTreeRenderers_22; }
	inline void set_deadTreeRenderers_22(RendererU5BU5D_t2810717544* value)
	{
		___deadTreeRenderers_22 = value;
		Il2CppCodeGenWriteBarrier((&___deadTreeRenderers_22), value);
	}

	inline static int32_t get_offset_of_boatRenderers_23() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___boatRenderers_23)); }
	inline RendererU5BU5D_t2810717544* get_boatRenderers_23() const { return ___boatRenderers_23; }
	inline RendererU5BU5D_t2810717544** get_address_of_boatRenderers_23() { return &___boatRenderers_23; }
	inline void set_boatRenderers_23(RendererU5BU5D_t2810717544* value)
	{
		___boatRenderers_23 = value;
		Il2CppCodeGenWriteBarrier((&___boatRenderers_23), value);
	}

	inline static int32_t get_offset_of_rocksRenderers_24() { return static_cast<int32_t>(offsetof(SceneLigtingSetup_t3978848013, ___rocksRenderers_24)); }
	inline RendererU5BU5D_t2810717544* get_rocksRenderers_24() const { return ___rocksRenderers_24; }
	inline RendererU5BU5D_t2810717544** get_address_of_rocksRenderers_24() { return &___rocksRenderers_24; }
	inline void set_rocksRenderers_24(RendererU5BU5D_t2810717544* value)
	{
		___rocksRenderers_24 = value;
		Il2CppCodeGenWriteBarrier((&___rocksRenderers_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELIGTINGSETUP_T3978848013_H
#ifndef CAMERACONTROL_T2838268856_H
#define CAMERACONTROL_T2838268856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraControl
struct  CameraControl_t2838268856  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform CameraControl::pointOfInterest
	Transform_t3275118058 * ___pointOfInterest_2;
	// System.Boolean CameraControl::shouldRotateAutomatic
	bool ___shouldRotateAutomatic_3;
	// System.Single CameraControl::rotationSpeed
	float ___rotationSpeed_4;
	// System.Single CameraControl::minRadius
	float ___minRadius_5;
	// System.Single CameraControl::maxRadius
	float ___maxRadius_6;
	// System.Single CameraControl::minZoom
	float ___minZoom_7;
	// System.Single CameraControl::maxZoom
	float ___maxZoom_8;
	// UnityEngine.Camera CameraControl::myCamera
	Camera_t189460977 * ___myCamera_9;
	// UnityEngine.Vector3 CameraControl::lastNormalizedPosition
	Vector3_t2243707580  ___lastNormalizedPosition_10;
	// System.Single CameraControl::radius
	float ___radius_11;

public:
	inline static int32_t get_offset_of_pointOfInterest_2() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___pointOfInterest_2)); }
	inline Transform_t3275118058 * get_pointOfInterest_2() const { return ___pointOfInterest_2; }
	inline Transform_t3275118058 ** get_address_of_pointOfInterest_2() { return &___pointOfInterest_2; }
	inline void set_pointOfInterest_2(Transform_t3275118058 * value)
	{
		___pointOfInterest_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointOfInterest_2), value);
	}

	inline static int32_t get_offset_of_shouldRotateAutomatic_3() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___shouldRotateAutomatic_3)); }
	inline bool get_shouldRotateAutomatic_3() const { return ___shouldRotateAutomatic_3; }
	inline bool* get_address_of_shouldRotateAutomatic_3() { return &___shouldRotateAutomatic_3; }
	inline void set_shouldRotateAutomatic_3(bool value)
	{
		___shouldRotateAutomatic_3 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_4() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___rotationSpeed_4)); }
	inline float get_rotationSpeed_4() const { return ___rotationSpeed_4; }
	inline float* get_address_of_rotationSpeed_4() { return &___rotationSpeed_4; }
	inline void set_rotationSpeed_4(float value)
	{
		___rotationSpeed_4 = value;
	}

	inline static int32_t get_offset_of_minRadius_5() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___minRadius_5)); }
	inline float get_minRadius_5() const { return ___minRadius_5; }
	inline float* get_address_of_minRadius_5() { return &___minRadius_5; }
	inline void set_minRadius_5(float value)
	{
		___minRadius_5 = value;
	}

	inline static int32_t get_offset_of_maxRadius_6() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___maxRadius_6)); }
	inline float get_maxRadius_6() const { return ___maxRadius_6; }
	inline float* get_address_of_maxRadius_6() { return &___maxRadius_6; }
	inline void set_maxRadius_6(float value)
	{
		___maxRadius_6 = value;
	}

	inline static int32_t get_offset_of_minZoom_7() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___minZoom_7)); }
	inline float get_minZoom_7() const { return ___minZoom_7; }
	inline float* get_address_of_minZoom_7() { return &___minZoom_7; }
	inline void set_minZoom_7(float value)
	{
		___minZoom_7 = value;
	}

	inline static int32_t get_offset_of_maxZoom_8() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___maxZoom_8)); }
	inline float get_maxZoom_8() const { return ___maxZoom_8; }
	inline float* get_address_of_maxZoom_8() { return &___maxZoom_8; }
	inline void set_maxZoom_8(float value)
	{
		___maxZoom_8 = value;
	}

	inline static int32_t get_offset_of_myCamera_9() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___myCamera_9)); }
	inline Camera_t189460977 * get_myCamera_9() const { return ___myCamera_9; }
	inline Camera_t189460977 ** get_address_of_myCamera_9() { return &___myCamera_9; }
	inline void set_myCamera_9(Camera_t189460977 * value)
	{
		___myCamera_9 = value;
		Il2CppCodeGenWriteBarrier((&___myCamera_9), value);
	}

	inline static int32_t get_offset_of_lastNormalizedPosition_10() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___lastNormalizedPosition_10)); }
	inline Vector3_t2243707580  get_lastNormalizedPosition_10() const { return ___lastNormalizedPosition_10; }
	inline Vector3_t2243707580 * get_address_of_lastNormalizedPosition_10() { return &___lastNormalizedPosition_10; }
	inline void set_lastNormalizedPosition_10(Vector3_t2243707580  value)
	{
		___lastNormalizedPosition_10 = value;
	}

	inline static int32_t get_offset_of_radius_11() { return static_cast<int32_t>(offsetof(CameraControl_t2838268856, ___radius_11)); }
	inline float get_radius_11() const { return ___radius_11; }
	inline float* get_address_of_radius_11() { return &___radius_11; }
	inline void set_radius_11(float value)
	{
		___radius_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROL_T2838268856_H
#ifndef PLAYERGRAVITYBODY_T3825941471_H
#define PLAYERGRAVITYBODY_T3825941471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerGravityBody
struct  PlayerGravityBody_t3825941471  : public MonoBehaviour_t1158329972
{
public:
	// PlanetScript PlayerGravityBody::attractorPlanet
	PlanetScript_t2420377963 * ___attractorPlanet_2;
	// UnityEngine.Transform PlayerGravityBody::playerTransform
	Transform_t3275118058 * ___playerTransform_3;

public:
	inline static int32_t get_offset_of_attractorPlanet_2() { return static_cast<int32_t>(offsetof(PlayerGravityBody_t3825941471, ___attractorPlanet_2)); }
	inline PlanetScript_t2420377963 * get_attractorPlanet_2() const { return ___attractorPlanet_2; }
	inline PlanetScript_t2420377963 ** get_address_of_attractorPlanet_2() { return &___attractorPlanet_2; }
	inline void set_attractorPlanet_2(PlanetScript_t2420377963 * value)
	{
		___attractorPlanet_2 = value;
		Il2CppCodeGenWriteBarrier((&___attractorPlanet_2), value);
	}

	inline static int32_t get_offset_of_playerTransform_3() { return static_cast<int32_t>(offsetof(PlayerGravityBody_t3825941471, ___playerTransform_3)); }
	inline Transform_t3275118058 * get_playerTransform_3() const { return ___playerTransform_3; }
	inline Transform_t3275118058 ** get_address_of_playerTransform_3() { return &___playerTransform_3; }
	inline void set_playerTransform_3(Transform_t3275118058 * value)
	{
		___playerTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerTransform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERGRAVITYBODY_T3825941471_H
#ifndef PLAYERMOVEMENTSCRIPT_T4209466341_H
#define PLAYERMOVEMENTSCRIPT_T4209466341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMovementScript
struct  PlayerMovementScript_t4209466341  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlayerMovementScript::moveSpeed
	float ___moveSpeed_2;
	// UnityEngine.Transform PlayerMovementScript::TrackedItem
	Transform_t3275118058 * ___TrackedItem_3;
	// UnityEngine.Vector3 PlayerMovementScript::moveDirection
	Vector3_t2243707580  ___moveDirection_4;
	// UnityEngine.Transform PlayerMovementScript::Camera
	Transform_t3275118058 * ___Camera_5;
	// UnityEngine.Transform PlayerMovementScript::Player
	Transform_t3275118058 * ___Player_6;
	// UnityEngine.Transform PlayerMovementScript::scorePivot
	Transform_t3275118058 * ___scorePivot_7;
	// System.Single PlayerMovementScript::smooth
	float ___smooth_8;
	// GameManager PlayerMovementScript::gm
	GameManager_t2252321495 * ___gm_9;
	// UnityEngine.Quaternion PlayerMovementScript::prev_rot
	Quaternion_t4030073918  ___prev_rot_10;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_TrackedItem_3() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___TrackedItem_3)); }
	inline Transform_t3275118058 * get_TrackedItem_3() const { return ___TrackedItem_3; }
	inline Transform_t3275118058 ** get_address_of_TrackedItem_3() { return &___TrackedItem_3; }
	inline void set_TrackedItem_3(Transform_t3275118058 * value)
	{
		___TrackedItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___TrackedItem_3), value);
	}

	inline static int32_t get_offset_of_moveDirection_4() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___moveDirection_4)); }
	inline Vector3_t2243707580  get_moveDirection_4() const { return ___moveDirection_4; }
	inline Vector3_t2243707580 * get_address_of_moveDirection_4() { return &___moveDirection_4; }
	inline void set_moveDirection_4(Vector3_t2243707580  value)
	{
		___moveDirection_4 = value;
	}

	inline static int32_t get_offset_of_Camera_5() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___Camera_5)); }
	inline Transform_t3275118058 * get_Camera_5() const { return ___Camera_5; }
	inline Transform_t3275118058 ** get_address_of_Camera_5() { return &___Camera_5; }
	inline void set_Camera_5(Transform_t3275118058 * value)
	{
		___Camera_5 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_5), value);
	}

	inline static int32_t get_offset_of_Player_6() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___Player_6)); }
	inline Transform_t3275118058 * get_Player_6() const { return ___Player_6; }
	inline Transform_t3275118058 ** get_address_of_Player_6() { return &___Player_6; }
	inline void set_Player_6(Transform_t3275118058 * value)
	{
		___Player_6 = value;
		Il2CppCodeGenWriteBarrier((&___Player_6), value);
	}

	inline static int32_t get_offset_of_scorePivot_7() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___scorePivot_7)); }
	inline Transform_t3275118058 * get_scorePivot_7() const { return ___scorePivot_7; }
	inline Transform_t3275118058 ** get_address_of_scorePivot_7() { return &___scorePivot_7; }
	inline void set_scorePivot_7(Transform_t3275118058 * value)
	{
		___scorePivot_7 = value;
		Il2CppCodeGenWriteBarrier((&___scorePivot_7), value);
	}

	inline static int32_t get_offset_of_smooth_8() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___smooth_8)); }
	inline float get_smooth_8() const { return ___smooth_8; }
	inline float* get_address_of_smooth_8() { return &___smooth_8; }
	inline void set_smooth_8(float value)
	{
		___smooth_8 = value;
	}

	inline static int32_t get_offset_of_gm_9() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___gm_9)); }
	inline GameManager_t2252321495 * get_gm_9() const { return ___gm_9; }
	inline GameManager_t2252321495 ** get_address_of_gm_9() { return &___gm_9; }
	inline void set_gm_9(GameManager_t2252321495 * value)
	{
		___gm_9 = value;
		Il2CppCodeGenWriteBarrier((&___gm_9), value);
	}

	inline static int32_t get_offset_of_prev_rot_10() { return static_cast<int32_t>(offsetof(PlayerMovementScript_t4209466341, ___prev_rot_10)); }
	inline Quaternion_t4030073918  get_prev_rot_10() const { return ___prev_rot_10; }
	inline Quaternion_t4030073918 * get_address_of_prev_rot_10() { return &___prev_rot_10; }
	inline void set_prev_rot_10(Quaternion_t4030073918  value)
	{
		___prev_rot_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOVEMENTSCRIPT_T4209466341_H
#ifndef ASYMMETRICFRUSTUM_T2869578734_H
#define ASYMMETRICFRUSTUM_T2869578734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.AsymmetricFrustum
struct  AsymmetricFrustum_t2869578734  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.AsymmetricFrustum::left
	float ___left_2;
	// System.Single Mira.AsymmetricFrustum::right
	float ___right_3;
	// System.Single Mira.AsymmetricFrustum::top
	float ___top_4;
	// System.Single Mira.AsymmetricFrustum::bottom
	float ___bottom_5;
	// System.Single Mira.AsymmetricFrustum::near
	float ___near_6;
	// System.Single Mira.AsymmetricFrustum::far
	float ___far_7;
	// System.Single Mira.AsymmetricFrustum::fov
	float ___fov_8;
	// System.Single Mira.AsymmetricFrustum::calibratedCamNear
	float ___calibratedCamNear_9;
	// System.Single Mira.AsymmetricFrustum::slideX
	float ___slideX_10;
	// System.Boolean Mira.AsymmetricFrustum::isLeftCam
	bool ___isLeftCam_11;
	// UnityEngine.Camera Mira.AsymmetricFrustum::cam
	Camera_t189460977 * ___cam_12;
	// System.Boolean Mira.AsymmetricFrustum::asymmetricFrust
	bool ___asymmetricFrust_13;

public:
	inline static int32_t get_offset_of_left_2() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___left_2)); }
	inline float get_left_2() const { return ___left_2; }
	inline float* get_address_of_left_2() { return &___left_2; }
	inline void set_left_2(float value)
	{
		___left_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___right_3)); }
	inline float get_right_3() const { return ___right_3; }
	inline float* get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(float value)
	{
		___right_3 = value;
	}

	inline static int32_t get_offset_of_top_4() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___top_4)); }
	inline float get_top_4() const { return ___top_4; }
	inline float* get_address_of_top_4() { return &___top_4; }
	inline void set_top_4(float value)
	{
		___top_4 = value;
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___bottom_5)); }
	inline float get_bottom_5() const { return ___bottom_5; }
	inline float* get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(float value)
	{
		___bottom_5 = value;
	}

	inline static int32_t get_offset_of_near_6() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___near_6)); }
	inline float get_near_6() const { return ___near_6; }
	inline float* get_address_of_near_6() { return &___near_6; }
	inline void set_near_6(float value)
	{
		___near_6 = value;
	}

	inline static int32_t get_offset_of_far_7() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___far_7)); }
	inline float get_far_7() const { return ___far_7; }
	inline float* get_address_of_far_7() { return &___far_7; }
	inline void set_far_7(float value)
	{
		___far_7 = value;
	}

	inline static int32_t get_offset_of_fov_8() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___fov_8)); }
	inline float get_fov_8() const { return ___fov_8; }
	inline float* get_address_of_fov_8() { return &___fov_8; }
	inline void set_fov_8(float value)
	{
		___fov_8 = value;
	}

	inline static int32_t get_offset_of_calibratedCamNear_9() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___calibratedCamNear_9)); }
	inline float get_calibratedCamNear_9() const { return ___calibratedCamNear_9; }
	inline float* get_address_of_calibratedCamNear_9() { return &___calibratedCamNear_9; }
	inline void set_calibratedCamNear_9(float value)
	{
		___calibratedCamNear_9 = value;
	}

	inline static int32_t get_offset_of_slideX_10() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___slideX_10)); }
	inline float get_slideX_10() const { return ___slideX_10; }
	inline float* get_address_of_slideX_10() { return &___slideX_10; }
	inline void set_slideX_10(float value)
	{
		___slideX_10 = value;
	}

	inline static int32_t get_offset_of_isLeftCam_11() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___isLeftCam_11)); }
	inline bool get_isLeftCam_11() const { return ___isLeftCam_11; }
	inline bool* get_address_of_isLeftCam_11() { return &___isLeftCam_11; }
	inline void set_isLeftCam_11(bool value)
	{
		___isLeftCam_11 = value;
	}

	inline static int32_t get_offset_of_cam_12() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___cam_12)); }
	inline Camera_t189460977 * get_cam_12() const { return ___cam_12; }
	inline Camera_t189460977 ** get_address_of_cam_12() { return &___cam_12; }
	inline void set_cam_12(Camera_t189460977 * value)
	{
		___cam_12 = value;
		Il2CppCodeGenWriteBarrier((&___cam_12), value);
	}

	inline static int32_t get_offset_of_asymmetricFrust_13() { return static_cast<int32_t>(offsetof(AsymmetricFrustum_t2869578734, ___asymmetricFrust_13)); }
	inline bool get_asymmetricFrust_13() const { return ___asymmetricFrust_13; }
	inline bool* get_address_of_asymmetricFrust_13() { return &___asymmetricFrust_13; }
	inline void set_asymmetricFrust_13(bool value)
	{
		___asymmetricFrust_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICFRUSTUM_T2869578734_H
#ifndef CONNECTTOPREVIOUSREMOTE_T1323906560_H
#define CONNECTTOPREVIOUSREMOTE_T1323906560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectToPreviousRemote
struct  ConnectToPreviousRemote_t1323906560  : public MonoBehaviour_t1158329972
{
public:
	// System.Guid ConnectToPreviousRemote::lastRemoteID
	Guid_t  ___lastRemoteID_2;
	// System.Boolean ConnectToPreviousRemote::activelySearching
	bool ___activelySearching_3;

public:
	inline static int32_t get_offset_of_lastRemoteID_2() { return static_cast<int32_t>(offsetof(ConnectToPreviousRemote_t1323906560, ___lastRemoteID_2)); }
	inline Guid_t  get_lastRemoteID_2() const { return ___lastRemoteID_2; }
	inline Guid_t * get_address_of_lastRemoteID_2() { return &___lastRemoteID_2; }
	inline void set_lastRemoteID_2(Guid_t  value)
	{
		___lastRemoteID_2 = value;
	}

	inline static int32_t get_offset_of_activelySearching_3() { return static_cast<int32_t>(offsetof(ConnectToPreviousRemote_t1323906560, ___activelySearching_3)); }
	inline bool get_activelySearching_3() const { return ___activelySearching_3; }
	inline bool* get_address_of_activelySearching_3() { return &___activelySearching_3; }
	inline void set_activelySearching_3(bool value)
	{
		___activelySearching_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOPREVIOUSREMOTE_T1323906560_H
#ifndef MIRACONTROLLER_T1058876281_H
#define MIRACONTROLLER_T1058876281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraController
struct  MiraController_t1058876281  : public MonoBehaviour_t1158329972
{
public:
	// MiraController/Handedness MiraController::handedness
	int32_t ___handedness_3;
	// MiraController/ControllerType MiraController::controllerType
	int32_t ___controllerType_4;
	// MiraController/ClickChoices MiraController::WhatButtonIsClick
	int32_t ___WhatButtonIsClick_5;
	// UnityEngine.Transform MiraController::cameraRig
	Transform_t3275118058 * ___cameraRig_7;
	// UnityEngine.Transform MiraController::rotationalTracking
	Transform_t3275118058 * ___rotationalTracking_8;
	// System.Boolean MiraController::trackPosition
	bool ___trackPosition_9;
	// System.Boolean MiraController::shouldFollowHead
	bool ___shouldFollowHead_10;
	// System.Single MiraController::sceneScaleMult
	float ___sceneScaleMult_11;
	// System.Single MiraController::armOffsetX
	float ___armOffsetX_12;
	// System.Single MiraController::armOffsetY
	float ___armOffsetY_13;
	// System.Single MiraController::armOffsetZ
	float ___armOffsetZ_14;
	// UnityEngine.Quaternion MiraController::baseController
	Quaternion_t4030073918  ___baseController_15;
	// UnityEngine.Vector3 MiraController::offsetPos
	Vector3_t2243707580  ___offsetPos_16;
	// System.Boolean MiraController::isRotationalMode
	bool ___isRotationalMode_17;
	// System.Single MiraController::angleTiltX
	float ___angleTiltX_18;
	// System.Single MiraController::angleTiltY
	float ___angleTiltY_19;
	// UnityEngine.GameObject MiraController::returnToHome
	GameObject_t1756533147 * ___returnToHome_20;
	// UnityEngine.UI.Image MiraController::radialTimer
	Image_t2042527209 * ___radialTimer_21;

public:
	inline static int32_t get_offset_of_handedness_3() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___handedness_3)); }
	inline int32_t get_handedness_3() const { return ___handedness_3; }
	inline int32_t* get_address_of_handedness_3() { return &___handedness_3; }
	inline void set_handedness_3(int32_t value)
	{
		___handedness_3 = value;
	}

	inline static int32_t get_offset_of_controllerType_4() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___controllerType_4)); }
	inline int32_t get_controllerType_4() const { return ___controllerType_4; }
	inline int32_t* get_address_of_controllerType_4() { return &___controllerType_4; }
	inline void set_controllerType_4(int32_t value)
	{
		___controllerType_4 = value;
	}

	inline static int32_t get_offset_of_WhatButtonIsClick_5() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___WhatButtonIsClick_5)); }
	inline int32_t get_WhatButtonIsClick_5() const { return ___WhatButtonIsClick_5; }
	inline int32_t* get_address_of_WhatButtonIsClick_5() { return &___WhatButtonIsClick_5; }
	inline void set_WhatButtonIsClick_5(int32_t value)
	{
		___WhatButtonIsClick_5 = value;
	}

	inline static int32_t get_offset_of_cameraRig_7() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___cameraRig_7)); }
	inline Transform_t3275118058 * get_cameraRig_7() const { return ___cameraRig_7; }
	inline Transform_t3275118058 ** get_address_of_cameraRig_7() { return &___cameraRig_7; }
	inline void set_cameraRig_7(Transform_t3275118058 * value)
	{
		___cameraRig_7 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_7), value);
	}

	inline static int32_t get_offset_of_rotationalTracking_8() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___rotationalTracking_8)); }
	inline Transform_t3275118058 * get_rotationalTracking_8() const { return ___rotationalTracking_8; }
	inline Transform_t3275118058 ** get_address_of_rotationalTracking_8() { return &___rotationalTracking_8; }
	inline void set_rotationalTracking_8(Transform_t3275118058 * value)
	{
		___rotationalTracking_8 = value;
		Il2CppCodeGenWriteBarrier((&___rotationalTracking_8), value);
	}

	inline static int32_t get_offset_of_trackPosition_9() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___trackPosition_9)); }
	inline bool get_trackPosition_9() const { return ___trackPosition_9; }
	inline bool* get_address_of_trackPosition_9() { return &___trackPosition_9; }
	inline void set_trackPosition_9(bool value)
	{
		___trackPosition_9 = value;
	}

	inline static int32_t get_offset_of_shouldFollowHead_10() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___shouldFollowHead_10)); }
	inline bool get_shouldFollowHead_10() const { return ___shouldFollowHead_10; }
	inline bool* get_address_of_shouldFollowHead_10() { return &___shouldFollowHead_10; }
	inline void set_shouldFollowHead_10(bool value)
	{
		___shouldFollowHead_10 = value;
	}

	inline static int32_t get_offset_of_sceneScaleMult_11() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___sceneScaleMult_11)); }
	inline float get_sceneScaleMult_11() const { return ___sceneScaleMult_11; }
	inline float* get_address_of_sceneScaleMult_11() { return &___sceneScaleMult_11; }
	inline void set_sceneScaleMult_11(float value)
	{
		___sceneScaleMult_11 = value;
	}

	inline static int32_t get_offset_of_armOffsetX_12() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___armOffsetX_12)); }
	inline float get_armOffsetX_12() const { return ___armOffsetX_12; }
	inline float* get_address_of_armOffsetX_12() { return &___armOffsetX_12; }
	inline void set_armOffsetX_12(float value)
	{
		___armOffsetX_12 = value;
	}

	inline static int32_t get_offset_of_armOffsetY_13() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___armOffsetY_13)); }
	inline float get_armOffsetY_13() const { return ___armOffsetY_13; }
	inline float* get_address_of_armOffsetY_13() { return &___armOffsetY_13; }
	inline void set_armOffsetY_13(float value)
	{
		___armOffsetY_13 = value;
	}

	inline static int32_t get_offset_of_armOffsetZ_14() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___armOffsetZ_14)); }
	inline float get_armOffsetZ_14() const { return ___armOffsetZ_14; }
	inline float* get_address_of_armOffsetZ_14() { return &___armOffsetZ_14; }
	inline void set_armOffsetZ_14(float value)
	{
		___armOffsetZ_14 = value;
	}

	inline static int32_t get_offset_of_baseController_15() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___baseController_15)); }
	inline Quaternion_t4030073918  get_baseController_15() const { return ___baseController_15; }
	inline Quaternion_t4030073918 * get_address_of_baseController_15() { return &___baseController_15; }
	inline void set_baseController_15(Quaternion_t4030073918  value)
	{
		___baseController_15 = value;
	}

	inline static int32_t get_offset_of_offsetPos_16() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___offsetPos_16)); }
	inline Vector3_t2243707580  get_offsetPos_16() const { return ___offsetPos_16; }
	inline Vector3_t2243707580 * get_address_of_offsetPos_16() { return &___offsetPos_16; }
	inline void set_offsetPos_16(Vector3_t2243707580  value)
	{
		___offsetPos_16 = value;
	}

	inline static int32_t get_offset_of_isRotationalMode_17() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___isRotationalMode_17)); }
	inline bool get_isRotationalMode_17() const { return ___isRotationalMode_17; }
	inline bool* get_address_of_isRotationalMode_17() { return &___isRotationalMode_17; }
	inline void set_isRotationalMode_17(bool value)
	{
		___isRotationalMode_17 = value;
	}

	inline static int32_t get_offset_of_angleTiltX_18() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___angleTiltX_18)); }
	inline float get_angleTiltX_18() const { return ___angleTiltX_18; }
	inline float* get_address_of_angleTiltX_18() { return &___angleTiltX_18; }
	inline void set_angleTiltX_18(float value)
	{
		___angleTiltX_18 = value;
	}

	inline static int32_t get_offset_of_angleTiltY_19() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___angleTiltY_19)); }
	inline float get_angleTiltY_19() const { return ___angleTiltY_19; }
	inline float* get_address_of_angleTiltY_19() { return &___angleTiltY_19; }
	inline void set_angleTiltY_19(float value)
	{
		___angleTiltY_19 = value;
	}

	inline static int32_t get_offset_of_returnToHome_20() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___returnToHome_20)); }
	inline GameObject_t1756533147 * get_returnToHome_20() const { return ___returnToHome_20; }
	inline GameObject_t1756533147 ** get_address_of_returnToHome_20() { return &___returnToHome_20; }
	inline void set_returnToHome_20(GameObject_t1756533147 * value)
	{
		___returnToHome_20 = value;
		Il2CppCodeGenWriteBarrier((&___returnToHome_20), value);
	}

	inline static int32_t get_offset_of_radialTimer_21() { return static_cast<int32_t>(offsetof(MiraController_t1058876281, ___radialTimer_21)); }
	inline Image_t2042527209 * get_radialTimer_21() const { return ___radialTimer_21; }
	inline Image_t2042527209 ** get_address_of_radialTimer_21() { return &___radialTimer_21; }
	inline void set_radialTimer_21(Image_t2042527209 * value)
	{
		___radialTimer_21 = value;
		Il2CppCodeGenWriteBarrier((&___radialTimer_21), value);
	}
};

struct MiraController_t1058876281_StaticFields
{
public:
	// MiraController MiraController::_instance
	MiraController_t1058876281 * ____instance_2;
	// MiraBTRemoteInput MiraController::_userInput
	MiraBTRemoteInput_t988911721 * ____userInput_6;
	// System.Collections.Generic.List`1<System.Func`1<System.Boolean>> MiraController::<evaluateClickButton>k__BackingField
	List_1_t854121236 * ___U3CevaluateClickButtonU3Ek__BackingField_22;
	// System.Collections.Generic.List`1<System.Func`1<System.Boolean>> MiraController::<evaluateClickButtonPressed>k__BackingField
	List_1_t854121236 * ___U3CevaluateClickButtonPressedU3Ek__BackingField_23;
	// System.Collections.Generic.List`1<System.Func`1<System.Boolean>> MiraController::<evaluateClickButtonReleased>k__BackingField
	List_1_t854121236 * ___U3CevaluateClickButtonReleasedU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MiraController_t1058876281_StaticFields, ____instance_2)); }
	inline MiraController_t1058876281 * get__instance_2() const { return ____instance_2; }
	inline MiraController_t1058876281 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(MiraController_t1058876281 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of__userInput_6() { return static_cast<int32_t>(offsetof(MiraController_t1058876281_StaticFields, ____userInput_6)); }
	inline MiraBTRemoteInput_t988911721 * get__userInput_6() const { return ____userInput_6; }
	inline MiraBTRemoteInput_t988911721 ** get_address_of__userInput_6() { return &____userInput_6; }
	inline void set__userInput_6(MiraBTRemoteInput_t988911721 * value)
	{
		____userInput_6 = value;
		Il2CppCodeGenWriteBarrier((&____userInput_6), value);
	}

	inline static int32_t get_offset_of_U3CevaluateClickButtonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(MiraController_t1058876281_StaticFields, ___U3CevaluateClickButtonU3Ek__BackingField_22)); }
	inline List_1_t854121236 * get_U3CevaluateClickButtonU3Ek__BackingField_22() const { return ___U3CevaluateClickButtonU3Ek__BackingField_22; }
	inline List_1_t854121236 ** get_address_of_U3CevaluateClickButtonU3Ek__BackingField_22() { return &___U3CevaluateClickButtonU3Ek__BackingField_22; }
	inline void set_U3CevaluateClickButtonU3Ek__BackingField_22(List_1_t854121236 * value)
	{
		___U3CevaluateClickButtonU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevaluateClickButtonU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CevaluateClickButtonPressedU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MiraController_t1058876281_StaticFields, ___U3CevaluateClickButtonPressedU3Ek__BackingField_23)); }
	inline List_1_t854121236 * get_U3CevaluateClickButtonPressedU3Ek__BackingField_23() const { return ___U3CevaluateClickButtonPressedU3Ek__BackingField_23; }
	inline List_1_t854121236 ** get_address_of_U3CevaluateClickButtonPressedU3Ek__BackingField_23() { return &___U3CevaluateClickButtonPressedU3Ek__BackingField_23; }
	inline void set_U3CevaluateClickButtonPressedU3Ek__BackingField_23(List_1_t854121236 * value)
	{
		___U3CevaluateClickButtonPressedU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevaluateClickButtonPressedU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CevaluateClickButtonReleasedU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MiraController_t1058876281_StaticFields, ___U3CevaluateClickButtonReleasedU3Ek__BackingField_24)); }
	inline List_1_t854121236 * get_U3CevaluateClickButtonReleasedU3Ek__BackingField_24() const { return ___U3CevaluateClickButtonReleasedU3Ek__BackingField_24; }
	inline List_1_t854121236 ** get_address_of_U3CevaluateClickButtonReleasedU3Ek__BackingField_24() { return &___U3CevaluateClickButtonReleasedU3Ek__BackingField_24; }
	inline void set_U3CevaluateClickButtonReleasedU3Ek__BackingField_24(List_1_t854121236 * value)
	{
		___U3CevaluateClickButtonReleasedU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevaluateClickButtonReleasedU3Ek__BackingField_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRACONTROLLER_T1058876281_H
#ifndef DISTORTIONCAMERA_T4252183916_H
#define DISTORTIONCAMERA_T4252183916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.DistortionCamera
struct  DistortionCamera_t4252183916  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.DistortionCamera::stereoFov
	float ___stereoFov_2;

public:
	inline static int32_t get_offset_of_stereoFov_2() { return static_cast<int32_t>(offsetof(DistortionCamera_t4252183916, ___stereoFov_2)); }
	inline float get_stereoFov_2() const { return ___stereoFov_2; }
	inline float* get_address_of_stereoFov_2() { return &___stereoFov_2; }
	inline void set_stereoFov_2(float value)
	{
		___stereoFov_2 = value;
	}
};

struct DistortionCamera_t4252183916_StaticFields
{
public:
	// Mira.DistortionCamera Mira.DistortionCamera::instance
	DistortionCamera_t4252183916 * ___instance_3;
	// UnityEngine.Camera Mira.DistortionCamera::m_dcLeft
	Camera_t189460977 * ___m_dcLeft_4;
	// UnityEngine.Camera Mira.DistortionCamera::m_dcRight
	Camera_t189460977 * ___m_dcRight_5;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(DistortionCamera_t4252183916_StaticFields, ___instance_3)); }
	inline DistortionCamera_t4252183916 * get_instance_3() const { return ___instance_3; }
	inline DistortionCamera_t4252183916 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(DistortionCamera_t4252183916 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_m_dcLeft_4() { return static_cast<int32_t>(offsetof(DistortionCamera_t4252183916_StaticFields, ___m_dcLeft_4)); }
	inline Camera_t189460977 * get_m_dcLeft_4() const { return ___m_dcLeft_4; }
	inline Camera_t189460977 ** get_address_of_m_dcLeft_4() { return &___m_dcLeft_4; }
	inline void set_m_dcLeft_4(Camera_t189460977 * value)
	{
		___m_dcLeft_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_dcLeft_4), value);
	}

	inline static int32_t get_offset_of_m_dcRight_5() { return static_cast<int32_t>(offsetof(DistortionCamera_t4252183916_StaticFields, ___m_dcRight_5)); }
	inline Camera_t189460977 * get_m_dcRight_5() const { return ___m_dcRight_5; }
	inline Camera_t189460977 ** get_address_of_m_dcRight_5() { return &___m_dcRight_5; }
	inline void set_m_dcRight_5(Camera_t189460977 * value)
	{
		___m_dcRight_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_dcRight_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONCAMERA_T4252183916_H
#ifndef DEVICEORIENTATIONCHANGE_T2098609656_H
#define DEVICEORIENTATIONCHANGE_T2098609656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceOrientationChange
struct  DeviceOrientationChange_t2098609656  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct DeviceOrientationChange_t2098609656_StaticFields
{
public:
	// System.Action`1<UnityEngine.Vector2> DeviceOrientationChange::OnResolutionChange
	Action_1_t2045506961 * ___OnResolutionChange_2;
	// System.Single DeviceOrientationChange::CheckDelay
	float ___CheckDelay_3;
	// UnityEngine.Vector2 DeviceOrientationChange::resolution
	Vector2_t2243707579  ___resolution_4;
	// System.Boolean DeviceOrientationChange::isAlive
	bool ___isAlive_5;

public:
	inline static int32_t get_offset_of_OnResolutionChange_2() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2098609656_StaticFields, ___OnResolutionChange_2)); }
	inline Action_1_t2045506961 * get_OnResolutionChange_2() const { return ___OnResolutionChange_2; }
	inline Action_1_t2045506961 ** get_address_of_OnResolutionChange_2() { return &___OnResolutionChange_2; }
	inline void set_OnResolutionChange_2(Action_1_t2045506961 * value)
	{
		___OnResolutionChange_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnResolutionChange_2), value);
	}

	inline static int32_t get_offset_of_CheckDelay_3() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2098609656_StaticFields, ___CheckDelay_3)); }
	inline float get_CheckDelay_3() const { return ___CheckDelay_3; }
	inline float* get_address_of_CheckDelay_3() { return &___CheckDelay_3; }
	inline void set_CheckDelay_3(float value)
	{
		___CheckDelay_3 = value;
	}

	inline static int32_t get_offset_of_resolution_4() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2098609656_StaticFields, ___resolution_4)); }
	inline Vector2_t2243707579  get_resolution_4() const { return ___resolution_4; }
	inline Vector2_t2243707579 * get_address_of_resolution_4() { return &___resolution_4; }
	inline void set_resolution_4(Vector2_t2243707579  value)
	{
		___resolution_4 = value;
	}

	inline static int32_t get_offset_of_isAlive_5() { return static_cast<int32_t>(offsetof(DeviceOrientationChange_t2098609656_StaticFields, ___isAlive_5)); }
	inline bool get_isAlive_5() const { return ___isAlive_5; }
	inline bool* get_address_of_isAlive_5() { return &___isAlive_5; }
	inline void set_isAlive_5(bool value)
	{
		___isAlive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEORIENTATIONCHANGE_T2098609656_H
#ifndef DEBUGMIRACONTROLLER_T1181799644_H
#define DEBUGMIRACONTROLLER_T1181799644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugMiraController
struct  DebugMiraController_t1181799644  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text DebugMiraController::debugOutputText
	Text_t356221433 * ___debugOutputText_2;
	// System.String DebugMiraController::output
	String_t* ___output_3;
	// System.Boolean DebugMiraController::didPointerEnter
	bool ___didPointerEnter_4;
	// System.Boolean DebugMiraController::didPointerExit
	bool ___didPointerExit_5;
	// System.Boolean DebugMiraController::didPointerClick
	bool ___didPointerClick_6;
	// System.Boolean DebugMiraController::didPointerDown
	bool ___didPointerDown_7;
	// System.Boolean DebugMiraController::didPointerUp
	bool ___didPointerUp_8;

public:
	inline static int32_t get_offset_of_debugOutputText_2() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___debugOutputText_2)); }
	inline Text_t356221433 * get_debugOutputText_2() const { return ___debugOutputText_2; }
	inline Text_t356221433 ** get_address_of_debugOutputText_2() { return &___debugOutputText_2; }
	inline void set_debugOutputText_2(Text_t356221433 * value)
	{
		___debugOutputText_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugOutputText_2), value);
	}

	inline static int32_t get_offset_of_output_3() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___output_3)); }
	inline String_t* get_output_3() const { return ___output_3; }
	inline String_t** get_address_of_output_3() { return &___output_3; }
	inline void set_output_3(String_t* value)
	{
		___output_3 = value;
		Il2CppCodeGenWriteBarrier((&___output_3), value);
	}

	inline static int32_t get_offset_of_didPointerEnter_4() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___didPointerEnter_4)); }
	inline bool get_didPointerEnter_4() const { return ___didPointerEnter_4; }
	inline bool* get_address_of_didPointerEnter_4() { return &___didPointerEnter_4; }
	inline void set_didPointerEnter_4(bool value)
	{
		___didPointerEnter_4 = value;
	}

	inline static int32_t get_offset_of_didPointerExit_5() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___didPointerExit_5)); }
	inline bool get_didPointerExit_5() const { return ___didPointerExit_5; }
	inline bool* get_address_of_didPointerExit_5() { return &___didPointerExit_5; }
	inline void set_didPointerExit_5(bool value)
	{
		___didPointerExit_5 = value;
	}

	inline static int32_t get_offset_of_didPointerClick_6() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___didPointerClick_6)); }
	inline bool get_didPointerClick_6() const { return ___didPointerClick_6; }
	inline bool* get_address_of_didPointerClick_6() { return &___didPointerClick_6; }
	inline void set_didPointerClick_6(bool value)
	{
		___didPointerClick_6 = value;
	}

	inline static int32_t get_offset_of_didPointerDown_7() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___didPointerDown_7)); }
	inline bool get_didPointerDown_7() const { return ___didPointerDown_7; }
	inline bool* get_address_of_didPointerDown_7() { return &___didPointerDown_7; }
	inline void set_didPointerDown_7(bool value)
	{
		___didPointerDown_7 = value;
	}

	inline static int32_t get_offset_of_didPointerUp_8() { return static_cast<int32_t>(offsetof(DebugMiraController_t1181799644, ___didPointerUp_8)); }
	inline bool get_didPointerUp_8() const { return ___didPointerUp_8; }
	inline bool* get_address_of_didPointerUp_8() { return &___didPointerUp_8; }
	inline void set_didPointerUp_8(bool value)
	{
		___didPointerUp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGMIRACONTROLLER_T1181799644_H
#ifndef CAMGYRO_T694259106_H
#define CAMGYRO_T694259106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.CamGyro
struct  CamGyro_t694259106  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Mira.CamGyro::isSpectator
	bool ___isSpectator_2;
	// UnityEngine.Quaternion Mira.CamGyro::camGyroOffset
	Quaternion_t4030073918  ___camGyroOffset_3;
	// System.Boolean Mira.CamGyro::camGyroActive
	bool ___camGyroActive_4;

public:
	inline static int32_t get_offset_of_isSpectator_2() { return static_cast<int32_t>(offsetof(CamGyro_t694259106, ___isSpectator_2)); }
	inline bool get_isSpectator_2() const { return ___isSpectator_2; }
	inline bool* get_address_of_isSpectator_2() { return &___isSpectator_2; }
	inline void set_isSpectator_2(bool value)
	{
		___isSpectator_2 = value;
	}

	inline static int32_t get_offset_of_camGyroOffset_3() { return static_cast<int32_t>(offsetof(CamGyro_t694259106, ___camGyroOffset_3)); }
	inline Quaternion_t4030073918  get_camGyroOffset_3() const { return ___camGyroOffset_3; }
	inline Quaternion_t4030073918 * get_address_of_camGyroOffset_3() { return &___camGyroOffset_3; }
	inline void set_camGyroOffset_3(Quaternion_t4030073918  value)
	{
		___camGyroOffset_3 = value;
	}

	inline static int32_t get_offset_of_camGyroActive_4() { return static_cast<int32_t>(offsetof(CamGyro_t694259106, ___camGyroActive_4)); }
	inline bool get_camGyroActive_4() const { return ___camGyroActive_4; }
	inline bool* get_address_of_camGyroActive_4() { return &___camGyroActive_4; }
	inline void set_camGyroActive_4(bool value)
	{
		___camGyroActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMGYRO_T694259106_H
#ifndef REMOTESCONTROLLER_T2607477933_H
#define REMOTESCONTROLLER_T2607477933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemotesController
struct  RemotesController_t2607477933  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] RemotesController::remoteLabels
	GameObjectU5BU5D_t3057952154* ___remoteLabels_2;
	// UnityEngine.Color RemotesController::defaultColor
	Color_t2020392075  ___defaultColor_3;
	// UnityEngine.Color RemotesController::highlightColor
	Color_t2020392075  ___highlightColor_4;

public:
	inline static int32_t get_offset_of_remoteLabels_2() { return static_cast<int32_t>(offsetof(RemotesController_t2607477933, ___remoteLabels_2)); }
	inline GameObjectU5BU5D_t3057952154* get_remoteLabels_2() const { return ___remoteLabels_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_remoteLabels_2() { return &___remoteLabels_2; }
	inline void set_remoteLabels_2(GameObjectU5BU5D_t3057952154* value)
	{
		___remoteLabels_2 = value;
		Il2CppCodeGenWriteBarrier((&___remoteLabels_2), value);
	}

	inline static int32_t get_offset_of_defaultColor_3() { return static_cast<int32_t>(offsetof(RemotesController_t2607477933, ___defaultColor_3)); }
	inline Color_t2020392075  get_defaultColor_3() const { return ___defaultColor_3; }
	inline Color_t2020392075 * get_address_of_defaultColor_3() { return &___defaultColor_3; }
	inline void set_defaultColor_3(Color_t2020392075  value)
	{
		___defaultColor_3 = value;
	}

	inline static int32_t get_offset_of_highlightColor_4() { return static_cast<int32_t>(offsetof(RemotesController_t2607477933, ___highlightColor_4)); }
	inline Color_t2020392075  get_highlightColor_4() const { return ___highlightColor_4; }
	inline Color_t2020392075 * get_address_of_highlightColor_4() { return &___highlightColor_4; }
	inline void set_highlightColor_4(Color_t2020392075  value)
	{
		___highlightColor_4 = value;
	}
};

struct RemotesController_t2607477933_StaticFields
{
public:
	// System.Action`1<MiraRemoteException> RemotesController::<>f__am$cache0
	Action_1_t3189573364 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(RemotesController_t2607477933_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t3189573364 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t3189573364 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t3189573364 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESCONTROLLER_T2607477933_H
#ifndef REMOTEMANAGER_T2208998541_H
#define REMOTEMANAGER_T2208998541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager
struct  RemoteManager_t2208998541  : public MonoBehaviour_t1158329972
{
public:
	// RemoteManager/RemoteConnectedEventHandler RemoteManager::OnRemoteConnected
	RemoteConnectedEventHandler_t3456249665 * ___OnRemoteConnected_2;
	// RemoteManager/RemoteDisconnectedEventHandler RemoteManager::OnRemoteDisconnected
	RemoteDisconnectedEventHandler_t730656759 * ___OnRemoteDisconnected_3;
	// System.Boolean RemoteManager::<isStarted>k__BackingField
	bool ___U3CisStartedU3Ek__BackingField_5;
	// System.Boolean RemoteManager::<isDiscoveringRemotes>k__BackingField
	bool ___U3CisDiscoveringRemotesU3Ek__BackingField_6;
	// Remote RemoteManager::_connectedRemote
	Remote_t660843562 * ____connectedRemote_7;
	// System.Collections.Generic.List`1<Remote> RemoteManager::_discoveredRemotes
	List_1_t29964694 * ____discoveredRemotes_8;

public:
	inline static int32_t get_offset_of_OnRemoteConnected_2() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___OnRemoteConnected_2)); }
	inline RemoteConnectedEventHandler_t3456249665 * get_OnRemoteConnected_2() const { return ___OnRemoteConnected_2; }
	inline RemoteConnectedEventHandler_t3456249665 ** get_address_of_OnRemoteConnected_2() { return &___OnRemoteConnected_2; }
	inline void set_OnRemoteConnected_2(RemoteConnectedEventHandler_t3456249665 * value)
	{
		___OnRemoteConnected_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteConnected_2), value);
	}

	inline static int32_t get_offset_of_OnRemoteDisconnected_3() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___OnRemoteDisconnected_3)); }
	inline RemoteDisconnectedEventHandler_t730656759 * get_OnRemoteDisconnected_3() const { return ___OnRemoteDisconnected_3; }
	inline RemoteDisconnectedEventHandler_t730656759 ** get_address_of_OnRemoteDisconnected_3() { return &___OnRemoteDisconnected_3; }
	inline void set_OnRemoteDisconnected_3(RemoteDisconnectedEventHandler_t730656759 * value)
	{
		___OnRemoteDisconnected_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteDisconnected_3), value);
	}

	inline static int32_t get_offset_of_U3CisStartedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___U3CisStartedU3Ek__BackingField_5)); }
	inline bool get_U3CisStartedU3Ek__BackingField_5() const { return ___U3CisStartedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CisStartedU3Ek__BackingField_5() { return &___U3CisStartedU3Ek__BackingField_5; }
	inline void set_U3CisStartedU3Ek__BackingField_5(bool value)
	{
		___U3CisStartedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CisDiscoveringRemotesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___U3CisDiscoveringRemotesU3Ek__BackingField_6)); }
	inline bool get_U3CisDiscoveringRemotesU3Ek__BackingField_6() const { return ___U3CisDiscoveringRemotesU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisDiscoveringRemotesU3Ek__BackingField_6() { return &___U3CisDiscoveringRemotesU3Ek__BackingField_6; }
	inline void set_U3CisDiscoveringRemotesU3Ek__BackingField_6(bool value)
	{
		___U3CisDiscoveringRemotesU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__connectedRemote_7() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ____connectedRemote_7)); }
	inline Remote_t660843562 * get__connectedRemote_7() const { return ____connectedRemote_7; }
	inline Remote_t660843562 ** get_address_of__connectedRemote_7() { return &____connectedRemote_7; }
	inline void set__connectedRemote_7(Remote_t660843562 * value)
	{
		____connectedRemote_7 = value;
		Il2CppCodeGenWriteBarrier((&____connectedRemote_7), value);
	}

	inline static int32_t get_offset_of__discoveredRemotes_8() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ____discoveredRemotes_8)); }
	inline List_1_t29964694 * get__discoveredRemotes_8() const { return ____discoveredRemotes_8; }
	inline List_1_t29964694 ** get_address_of__discoveredRemotes_8() { return &____discoveredRemotes_8; }
	inline void set__discoveredRemotes_8(List_1_t29964694 * value)
	{
		____discoveredRemotes_8 = value;
		Il2CppCodeGenWriteBarrier((&____discoveredRemotes_8), value);
	}
};

struct RemoteManager_t2208998541_StaticFields
{
public:
	// RemoteManager RemoteManager::Instance
	RemoteManager_t2208998541 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541_StaticFields, ___Instance_4)); }
	inline RemoteManager_t2208998541 * get_Instance_4() const { return ___Instance_4; }
	inline RemoteManager_t2208998541 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(RemoteManager_t2208998541 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMANAGER_T2208998541_H
#ifndef LIGHTSOURCE_1_T1855800957_H
#define LIGHTSOURCE_1_T1855800957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.SpotLight>
struct  LightSource_1_t1855800957  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t1855800957, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t1855800957_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t98915391 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t1855800957_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t1855800957_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t1855800957_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t1855800957_StaticFields, ___lights_5)); }
	inline LightBag_t98915391 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t98915391 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t98915391 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T1855800957_H
#ifndef LIGHTSOURCE_1_T2935135817_H
#define LIGHTSOURCE_1_T2935135817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.PointLight>
struct  LightSource_1_t2935135817  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t2935135817, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t2935135817_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t1178250251 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t2935135817_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t2935135817_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t2935135817_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t2935135817_StaticFields, ___lights_5)); }
	inline LightBag_t1178250251 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t1178250251 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t1178250251 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T2935135817_H
#ifndef LIGHTSOURCE_1_T4062566725_H
#define LIGHTSOURCE_1_T4062566725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.LightSource`1<FlatLighting.DirectionalLight>
struct  LightSource_1_t4062566725  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FlatLighting.LightSource`1::Id
	int32_t ___Id_6;

public:
	inline static int32_t get_offset_of_Id_6() { return static_cast<int32_t>(offsetof(LightSource_1_t4062566725, ___Id_6)); }
	inline int32_t get_Id_6() const { return ___Id_6; }
	inline int32_t* get_address_of_Id_6() { return &___Id_6; }
	inline void set_Id_6(int32_t value)
	{
		___Id_6 = value;
	}
};

struct LightSource_1_t4062566725_StaticFields
{
public:
	// System.Int32 FlatLighting.LightSource`1::MAX_LIGHTS
	int32_t ___MAX_LIGHTS_2;
	// System.Int32 FlatLighting.LightSource`1::lightCount
	int32_t ___lightCount_3;
	// System.Object FlatLighting.LightSource`1::my_lock
	RuntimeObject * ___my_lock_4;
	// FlatLighting.LightSource`1/LightBag<T> FlatLighting.LightSource`1::lights
	LightBag_t2305681159 * ___lights_5;

public:
	inline static int32_t get_offset_of_MAX_LIGHTS_2() { return static_cast<int32_t>(offsetof(LightSource_1_t4062566725_StaticFields, ___MAX_LIGHTS_2)); }
	inline int32_t get_MAX_LIGHTS_2() const { return ___MAX_LIGHTS_2; }
	inline int32_t* get_address_of_MAX_LIGHTS_2() { return &___MAX_LIGHTS_2; }
	inline void set_MAX_LIGHTS_2(int32_t value)
	{
		___MAX_LIGHTS_2 = value;
	}

	inline static int32_t get_offset_of_lightCount_3() { return static_cast<int32_t>(offsetof(LightSource_1_t4062566725_StaticFields, ___lightCount_3)); }
	inline int32_t get_lightCount_3() const { return ___lightCount_3; }
	inline int32_t* get_address_of_lightCount_3() { return &___lightCount_3; }
	inline void set_lightCount_3(int32_t value)
	{
		___lightCount_3 = value;
	}

	inline static int32_t get_offset_of_my_lock_4() { return static_cast<int32_t>(offsetof(LightSource_1_t4062566725_StaticFields, ___my_lock_4)); }
	inline RuntimeObject * get_my_lock_4() const { return ___my_lock_4; }
	inline RuntimeObject ** get_address_of_my_lock_4() { return &___my_lock_4; }
	inline void set_my_lock_4(RuntimeObject * value)
	{
		___my_lock_4 = value;
		Il2CppCodeGenWriteBarrier((&___my_lock_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(LightSource_1_t4062566725_StaticFields, ___lights_5)); }
	inline LightBag_t2305681159 * get_lights_5() const { return ___lights_5; }
	inline LightBag_t2305681159 ** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightBag_t2305681159 * value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOURCE_1_T4062566725_H
#ifndef PLANETSCRIPT_T2420377963_H
#define PLANETSCRIPT_T2420377963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlanetScript
struct  PlanetScript_t2420377963  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlanetScript::gravity
	float ___gravity_2;

public:
	inline static int32_t get_offset_of_gravity_2() { return static_cast<int32_t>(offsetof(PlanetScript_t2420377963, ___gravity_2)); }
	inline float get_gravity_2() const { return ___gravity_2; }
	inline float* get_address_of_gravity_2() { return &___gravity_2; }
	inline void set_gravity_2(float value)
	{
		___gravity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETSCRIPT_T2420377963_H
#ifndef LOOPMANAGER_T594583541_H
#define LOOPMANAGER_T594583541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoopManager
struct  LoopManager_t594583541  : public MonoBehaviour_t1158329972
{
public:
	// System.Single LoopManager::speed
	float ___speed_2;
	// UnityEngine.GameObject LoopManager::Sphere
	GameObject_t1756533147 * ___Sphere_3;
	// UnityEngine.GameObject LoopManager::StartPoint
	GameObject_t1756533147 * ___StartPoint_4;
	// System.Single LoopManager::SpawnTime
	float ___SpawnTime_5;
	// System.Single LoopManager::Cooldown
	float ___Cooldown_6;
	// System.Boolean LoopManager::StartSpawn
	bool ___StartSpawn_7;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(LoopManager_t594583541, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_Sphere_3() { return static_cast<int32_t>(offsetof(LoopManager_t594583541, ___Sphere_3)); }
	inline GameObject_t1756533147 * get_Sphere_3() const { return ___Sphere_3; }
	inline GameObject_t1756533147 ** get_address_of_Sphere_3() { return &___Sphere_3; }
	inline void set_Sphere_3(GameObject_t1756533147 * value)
	{
		___Sphere_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sphere_3), value);
	}

	inline static int32_t get_offset_of_StartPoint_4() { return static_cast<int32_t>(offsetof(LoopManager_t594583541, ___StartPoint_4)); }
	inline GameObject_t1756533147 * get_StartPoint_4() const { return ___StartPoint_4; }
	inline GameObject_t1756533147 ** get_address_of_StartPoint_4() { return &___StartPoint_4; }
	inline void set_StartPoint_4(GameObject_t1756533147 * value)
	{
		___StartPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___StartPoint_4), value);
	}

	inline static int32_t get_offset_of_SpawnTime_5() { return static_cast<int32_t>(offsetof(LoopManager_t594583541, ___SpawnTime_5)); }
	inline float get_SpawnTime_5() const { return ___SpawnTime_5; }
	inline float* get_address_of_SpawnTime_5() { return &___SpawnTime_5; }
	inline void set_SpawnTime_5(float value)
	{
		___SpawnTime_5 = value;
	}

	inline static int32_t get_offset_of_Cooldown_6() { return static_cast<int32_t>(offsetof(LoopManager_t594583541, ___Cooldown_6)); }
	inline float get_Cooldown_6() const { return ___Cooldown_6; }
	inline float* get_address_of_Cooldown_6() { return &___Cooldown_6; }
	inline void set_Cooldown_6(float value)
	{
		___Cooldown_6 = value;
	}

	inline static int32_t get_offset_of_StartSpawn_7() { return static_cast<int32_t>(offsetof(LoopManager_t594583541, ___StartSpawn_7)); }
	inline bool get_StartSpawn_7() const { return ___StartSpawn_7; }
	inline bool* get_address_of_StartSpawn_7() { return &___StartSpawn_7; }
	inline void set_StartSpawn_7(bool value)
	{
		___StartSpawn_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPMANAGER_T594583541_H
#ifndef ACTIVETCELLSCRIPT_T1119790483_H
#define ACTIVETCELLSCRIPT_T1119790483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveTCellScript
struct  ActiveTCellScript_t1119790483  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ActiveTCellScript::ActivCell
	GameObject_t1756533147 * ___ActivCell_2;
	// UnityEngine.GameObject ActiveTCellScript::AntiGen
	GameObject_t1756533147 * ___AntiGen_3;

public:
	inline static int32_t get_offset_of_ActivCell_2() { return static_cast<int32_t>(offsetof(ActiveTCellScript_t1119790483, ___ActivCell_2)); }
	inline GameObject_t1756533147 * get_ActivCell_2() const { return ___ActivCell_2; }
	inline GameObject_t1756533147 ** get_address_of_ActivCell_2() { return &___ActivCell_2; }
	inline void set_ActivCell_2(GameObject_t1756533147 * value)
	{
		___ActivCell_2 = value;
		Il2CppCodeGenWriteBarrier((&___ActivCell_2), value);
	}

	inline static int32_t get_offset_of_AntiGen_3() { return static_cast<int32_t>(offsetof(ActiveTCellScript_t1119790483, ___AntiGen_3)); }
	inline GameObject_t1756533147 * get_AntiGen_3() const { return ___AntiGen_3; }
	inline GameObject_t1756533147 ** get_address_of_AntiGen_3() { return &___AntiGen_3; }
	inline void set_AntiGen_3(GameObject_t1756533147 * value)
	{
		___AntiGen_3 = value;
		Il2CppCodeGenWriteBarrier((&___AntiGen_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVETCELLSCRIPT_T1119790483_H
#ifndef BASERAYCASTER_T2336171397_H
#define BASERAYCASTER_T2336171397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t2336171397  : public UIBehaviour_t3960014691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T2336171397_H
#ifndef GAMEMANAGER_T2252321495_H
#define GAMEMANAGER_T2252321495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public Singleton_1_t3428205882
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::Collectables
	List_1_t1125654279 * ___Collectables_3;
	// System.Int32 GameManager::goal
	int32_t ___goal_4;
	// TMPro.TextMeshPro GameManager::numberOfCellstokill
	TextMeshPro_t2521834357 * ___numberOfCellstokill_5;
	// UnityEngine.GameObject GameManager::player
	GameObject_t1756533147 * ___player_6;
	// System.Collections.Generic.List`1<System.String> GameManager::framesText
	List_1_t1398341365 * ___framesText_7;
	// System.Int32 GameManager::framesIterator
	int32_t ___framesIterator_8;
	// TMPro.TextMeshPro GameManager::TutorialFrames
	TextMeshPro_t2521834357 * ___TutorialFrames_9;
	// TMPro.TextMeshPro GameManager::GameFrames
	TextMeshPro_t2521834357 * ___GameFrames_10;
	// UnityEngine.GameObject GameManager::TrackedItem
	GameObject_t1756533147 * ___TrackedItem_11;
	// UnityEngine.GameObject GameManager::FakeSphere
	GameObject_t1756533147 * ___FakeSphere_12;
	// UnityEngine.Transform GameManager::Camera
	Transform_t3275118058 * ___Camera_13;
	// System.Boolean GameManager::SlidesON
	bool ___SlidesON_14;
	// System.Boolean GameManager::MovementTutorialStarted
	bool ___MovementTutorialStarted_15;
	// System.Boolean GameManager::MovementTutorialFinished
	bool ___MovementTutorialFinished_16;
	// System.Boolean GameManager::GameON
	bool ___GameON_17;
	// System.Boolean GameManager::ResetPlayer
	bool ___ResetPlayer_18;

public:
	inline static int32_t get_offset_of_Collectables_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Collectables_3)); }
	inline List_1_t1125654279 * get_Collectables_3() const { return ___Collectables_3; }
	inline List_1_t1125654279 ** get_address_of_Collectables_3() { return &___Collectables_3; }
	inline void set_Collectables_3(List_1_t1125654279 * value)
	{
		___Collectables_3 = value;
		Il2CppCodeGenWriteBarrier((&___Collectables_3), value);
	}

	inline static int32_t get_offset_of_goal_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___goal_4)); }
	inline int32_t get_goal_4() const { return ___goal_4; }
	inline int32_t* get_address_of_goal_4() { return &___goal_4; }
	inline void set_goal_4(int32_t value)
	{
		___goal_4 = value;
	}

	inline static int32_t get_offset_of_numberOfCellstokill_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___numberOfCellstokill_5)); }
	inline TextMeshPro_t2521834357 * get_numberOfCellstokill_5() const { return ___numberOfCellstokill_5; }
	inline TextMeshPro_t2521834357 ** get_address_of_numberOfCellstokill_5() { return &___numberOfCellstokill_5; }
	inline void set_numberOfCellstokill_5(TextMeshPro_t2521834357 * value)
	{
		___numberOfCellstokill_5 = value;
		Il2CppCodeGenWriteBarrier((&___numberOfCellstokill_5), value);
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___player_6)); }
	inline GameObject_t1756533147 * get_player_6() const { return ___player_6; }
	inline GameObject_t1756533147 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GameObject_t1756533147 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_framesText_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___framesText_7)); }
	inline List_1_t1398341365 * get_framesText_7() const { return ___framesText_7; }
	inline List_1_t1398341365 ** get_address_of_framesText_7() { return &___framesText_7; }
	inline void set_framesText_7(List_1_t1398341365 * value)
	{
		___framesText_7 = value;
		Il2CppCodeGenWriteBarrier((&___framesText_7), value);
	}

	inline static int32_t get_offset_of_framesIterator_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___framesIterator_8)); }
	inline int32_t get_framesIterator_8() const { return ___framesIterator_8; }
	inline int32_t* get_address_of_framesIterator_8() { return &___framesIterator_8; }
	inline void set_framesIterator_8(int32_t value)
	{
		___framesIterator_8 = value;
	}

	inline static int32_t get_offset_of_TutorialFrames_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___TutorialFrames_9)); }
	inline TextMeshPro_t2521834357 * get_TutorialFrames_9() const { return ___TutorialFrames_9; }
	inline TextMeshPro_t2521834357 ** get_address_of_TutorialFrames_9() { return &___TutorialFrames_9; }
	inline void set_TutorialFrames_9(TextMeshPro_t2521834357 * value)
	{
		___TutorialFrames_9 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialFrames_9), value);
	}

	inline static int32_t get_offset_of_GameFrames_10() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___GameFrames_10)); }
	inline TextMeshPro_t2521834357 * get_GameFrames_10() const { return ___GameFrames_10; }
	inline TextMeshPro_t2521834357 ** get_address_of_GameFrames_10() { return &___GameFrames_10; }
	inline void set_GameFrames_10(TextMeshPro_t2521834357 * value)
	{
		___GameFrames_10 = value;
		Il2CppCodeGenWriteBarrier((&___GameFrames_10), value);
	}

	inline static int32_t get_offset_of_TrackedItem_11() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___TrackedItem_11)); }
	inline GameObject_t1756533147 * get_TrackedItem_11() const { return ___TrackedItem_11; }
	inline GameObject_t1756533147 ** get_address_of_TrackedItem_11() { return &___TrackedItem_11; }
	inline void set_TrackedItem_11(GameObject_t1756533147 * value)
	{
		___TrackedItem_11 = value;
		Il2CppCodeGenWriteBarrier((&___TrackedItem_11), value);
	}

	inline static int32_t get_offset_of_FakeSphere_12() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___FakeSphere_12)); }
	inline GameObject_t1756533147 * get_FakeSphere_12() const { return ___FakeSphere_12; }
	inline GameObject_t1756533147 ** get_address_of_FakeSphere_12() { return &___FakeSphere_12; }
	inline void set_FakeSphere_12(GameObject_t1756533147 * value)
	{
		___FakeSphere_12 = value;
		Il2CppCodeGenWriteBarrier((&___FakeSphere_12), value);
	}

	inline static int32_t get_offset_of_Camera_13() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Camera_13)); }
	inline Transform_t3275118058 * get_Camera_13() const { return ___Camera_13; }
	inline Transform_t3275118058 ** get_address_of_Camera_13() { return &___Camera_13; }
	inline void set_Camera_13(Transform_t3275118058 * value)
	{
		___Camera_13 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_13), value);
	}

	inline static int32_t get_offset_of_SlidesON_14() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___SlidesON_14)); }
	inline bool get_SlidesON_14() const { return ___SlidesON_14; }
	inline bool* get_address_of_SlidesON_14() { return &___SlidesON_14; }
	inline void set_SlidesON_14(bool value)
	{
		___SlidesON_14 = value;
	}

	inline static int32_t get_offset_of_MovementTutorialStarted_15() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MovementTutorialStarted_15)); }
	inline bool get_MovementTutorialStarted_15() const { return ___MovementTutorialStarted_15; }
	inline bool* get_address_of_MovementTutorialStarted_15() { return &___MovementTutorialStarted_15; }
	inline void set_MovementTutorialStarted_15(bool value)
	{
		___MovementTutorialStarted_15 = value;
	}

	inline static int32_t get_offset_of_MovementTutorialFinished_16() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MovementTutorialFinished_16)); }
	inline bool get_MovementTutorialFinished_16() const { return ___MovementTutorialFinished_16; }
	inline bool* get_address_of_MovementTutorialFinished_16() { return &___MovementTutorialFinished_16; }
	inline void set_MovementTutorialFinished_16(bool value)
	{
		___MovementTutorialFinished_16 = value;
	}

	inline static int32_t get_offset_of_GameON_17() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___GameON_17)); }
	inline bool get_GameON_17() const { return ___GameON_17; }
	inline bool* get_address_of_GameON_17() { return &___GameON_17; }
	inline void set_GameON_17(bool value)
	{
		___GameON_17 = value;
	}

	inline static int32_t get_offset_of_ResetPlayer_18() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___ResetPlayer_18)); }
	inline bool get_ResetPlayer_18() const { return ___ResetPlayer_18; }
	inline bool* get_address_of_ResetPlayer_18() { return &___ResetPlayer_18; }
	inline void set_ResetPlayer_18(bool value)
	{
		___ResetPlayer_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T2252321495_H
#ifndef SPOTLIGHT_T983333132_H
#define SPOTLIGHT_T983333132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.SpotLight
struct  SpotLight_t983333132  : public LightSource_1_t1855800957
{
public:
	// System.Single FlatLighting.SpotLight::BaseRadius
	float ___BaseRadius_16;
	// System.Single FlatLighting.SpotLight::Height
	float ___Height_17;
	// UnityEngine.Color FlatLighting.SpotLight::LightColor
	Color_t2020392075  ___LightColor_18;
	// UnityEngine.Vector4 FlatLighting.SpotLight::LightDistances
	Vector4_t2243707581  ___LightDistances_19;
	// UnityEngine.Vector4 FlatLighting.SpotLight::LightIntensities
	Vector4_t2243707581  ___LightIntensities_20;
	// System.Boolean FlatLighting.SpotLight::Smooth
	bool ___Smooth_21;
	// System.Boolean FlatLighting.SpotLight::isRealTime
	bool ___isRealTime_22;
	// System.Boolean FlatLighting.SpotLight::isFirstPass
	bool ___isFirstPass_23;

public:
	inline static int32_t get_offset_of_BaseRadius_16() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___BaseRadius_16)); }
	inline float get_BaseRadius_16() const { return ___BaseRadius_16; }
	inline float* get_address_of_BaseRadius_16() { return &___BaseRadius_16; }
	inline void set_BaseRadius_16(float value)
	{
		___BaseRadius_16 = value;
	}

	inline static int32_t get_offset_of_Height_17() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___Height_17)); }
	inline float get_Height_17() const { return ___Height_17; }
	inline float* get_address_of_Height_17() { return &___Height_17; }
	inline void set_Height_17(float value)
	{
		___Height_17 = value;
	}

	inline static int32_t get_offset_of_LightColor_18() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___LightColor_18)); }
	inline Color_t2020392075  get_LightColor_18() const { return ___LightColor_18; }
	inline Color_t2020392075 * get_address_of_LightColor_18() { return &___LightColor_18; }
	inline void set_LightColor_18(Color_t2020392075  value)
	{
		___LightColor_18 = value;
	}

	inline static int32_t get_offset_of_LightDistances_19() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___LightDistances_19)); }
	inline Vector4_t2243707581  get_LightDistances_19() const { return ___LightDistances_19; }
	inline Vector4_t2243707581 * get_address_of_LightDistances_19() { return &___LightDistances_19; }
	inline void set_LightDistances_19(Vector4_t2243707581  value)
	{
		___LightDistances_19 = value;
	}

	inline static int32_t get_offset_of_LightIntensities_20() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___LightIntensities_20)); }
	inline Vector4_t2243707581  get_LightIntensities_20() const { return ___LightIntensities_20; }
	inline Vector4_t2243707581 * get_address_of_LightIntensities_20() { return &___LightIntensities_20; }
	inline void set_LightIntensities_20(Vector4_t2243707581  value)
	{
		___LightIntensities_20 = value;
	}

	inline static int32_t get_offset_of_Smooth_21() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___Smooth_21)); }
	inline bool get_Smooth_21() const { return ___Smooth_21; }
	inline bool* get_address_of_Smooth_21() { return &___Smooth_21; }
	inline void set_Smooth_21(bool value)
	{
		___Smooth_21 = value;
	}

	inline static int32_t get_offset_of_isRealTime_22() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___isRealTime_22)); }
	inline bool get_isRealTime_22() const { return ___isRealTime_22; }
	inline bool* get_address_of_isRealTime_22() { return &___isRealTime_22; }
	inline void set_isRealTime_22(bool value)
	{
		___isRealTime_22 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_23() { return static_cast<int32_t>(offsetof(SpotLight_t983333132, ___isFirstPass_23)); }
	inline bool get_isFirstPass_23() const { return ___isFirstPass_23; }
	inline bool* get_address_of_isFirstPass_23() { return &___isFirstPass_23; }
	inline void set_isFirstPass_23(bool value)
	{
		___isFirstPass_23 = value;
	}
};

struct SpotLight_t983333132_StaticFields
{
public:
	// System.String FlatLighting.SpotLight::spotLightCountProperty
	String_t* ___spotLightCountProperty_7;
	// System.String FlatLighting.SpotLight::spotLightWorldToModelProperty
	String_t* ___spotLightWorldToModelProperty_8;
	// System.String FlatLighting.SpotLight::spotLightForwardProperty
	String_t* ___spotLightForwardProperty_9;
	// System.String FlatLighting.SpotLight::spotLightColorProperty
	String_t* ___spotLightColorProperty_10;
	// System.String FlatLighting.SpotLight::spotLightBaseRadiusProperty
	String_t* ___spotLightBaseRadiusProperty_11;
	// System.String FlatLighting.SpotLight::spotLightHeightProperty
	String_t* ___spotLightHeightProperty_12;
	// System.String FlatLighting.SpotLight::spotLightDistancesProperty
	String_t* ___spotLightDistancesProperty_13;
	// System.String FlatLighting.SpotLight::spotLightIntensitiesProperty
	String_t* ___spotLightIntensitiesProperty_14;
	// System.String FlatLighting.SpotLight::spotLightSmoothnessProperty
	String_t* ___spotLightSmoothnessProperty_15;
	// UnityEngine.Matrix4x4[] FlatLighting.SpotLight::worldToModel
	Matrix4x4U5BU5D_t2328801282* ___worldToModel_24;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::forward
	Vector4U5BU5D_t1658499504* ___forward_25;
	// System.Single[] FlatLighting.SpotLight::baseRadius
	SingleU5BU5D_t577127397* ___baseRadius_26;
	// System.Single[] FlatLighting.SpotLight::height
	SingleU5BU5D_t577127397* ___height_27;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::distances
	Vector4U5BU5D_t1658499504* ___distances_28;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::intensities
	Vector4U5BU5D_t1658499504* ___intensities_29;
	// System.Single[] FlatLighting.SpotLight::smoothness
	SingleU5BU5D_t577127397* ___smoothness_30;
	// UnityEngine.Vector4[] FlatLighting.SpotLight::color
	Vector4U5BU5D_t1658499504* ___color_31;

public:
	inline static int32_t get_offset_of_spotLightCountProperty_7() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightCountProperty_7)); }
	inline String_t* get_spotLightCountProperty_7() const { return ___spotLightCountProperty_7; }
	inline String_t** get_address_of_spotLightCountProperty_7() { return &___spotLightCountProperty_7; }
	inline void set_spotLightCountProperty_7(String_t* value)
	{
		___spotLightCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightCountProperty_7), value);
	}

	inline static int32_t get_offset_of_spotLightWorldToModelProperty_8() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightWorldToModelProperty_8)); }
	inline String_t* get_spotLightWorldToModelProperty_8() const { return ___spotLightWorldToModelProperty_8; }
	inline String_t** get_address_of_spotLightWorldToModelProperty_8() { return &___spotLightWorldToModelProperty_8; }
	inline void set_spotLightWorldToModelProperty_8(String_t* value)
	{
		___spotLightWorldToModelProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightWorldToModelProperty_8), value);
	}

	inline static int32_t get_offset_of_spotLightForwardProperty_9() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightForwardProperty_9)); }
	inline String_t* get_spotLightForwardProperty_9() const { return ___spotLightForwardProperty_9; }
	inline String_t** get_address_of_spotLightForwardProperty_9() { return &___spotLightForwardProperty_9; }
	inline void set_spotLightForwardProperty_9(String_t* value)
	{
		___spotLightForwardProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightForwardProperty_9), value);
	}

	inline static int32_t get_offset_of_spotLightColorProperty_10() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightColorProperty_10)); }
	inline String_t* get_spotLightColorProperty_10() const { return ___spotLightColorProperty_10; }
	inline String_t** get_address_of_spotLightColorProperty_10() { return &___spotLightColorProperty_10; }
	inline void set_spotLightColorProperty_10(String_t* value)
	{
		___spotLightColorProperty_10 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightColorProperty_10), value);
	}

	inline static int32_t get_offset_of_spotLightBaseRadiusProperty_11() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightBaseRadiusProperty_11)); }
	inline String_t* get_spotLightBaseRadiusProperty_11() const { return ___spotLightBaseRadiusProperty_11; }
	inline String_t** get_address_of_spotLightBaseRadiusProperty_11() { return &___spotLightBaseRadiusProperty_11; }
	inline void set_spotLightBaseRadiusProperty_11(String_t* value)
	{
		___spotLightBaseRadiusProperty_11 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightBaseRadiusProperty_11), value);
	}

	inline static int32_t get_offset_of_spotLightHeightProperty_12() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightHeightProperty_12)); }
	inline String_t* get_spotLightHeightProperty_12() const { return ___spotLightHeightProperty_12; }
	inline String_t** get_address_of_spotLightHeightProperty_12() { return &___spotLightHeightProperty_12; }
	inline void set_spotLightHeightProperty_12(String_t* value)
	{
		___spotLightHeightProperty_12 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightHeightProperty_12), value);
	}

	inline static int32_t get_offset_of_spotLightDistancesProperty_13() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightDistancesProperty_13)); }
	inline String_t* get_spotLightDistancesProperty_13() const { return ___spotLightDistancesProperty_13; }
	inline String_t** get_address_of_spotLightDistancesProperty_13() { return &___spotLightDistancesProperty_13; }
	inline void set_spotLightDistancesProperty_13(String_t* value)
	{
		___spotLightDistancesProperty_13 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightDistancesProperty_13), value);
	}

	inline static int32_t get_offset_of_spotLightIntensitiesProperty_14() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightIntensitiesProperty_14)); }
	inline String_t* get_spotLightIntensitiesProperty_14() const { return ___spotLightIntensitiesProperty_14; }
	inline String_t** get_address_of_spotLightIntensitiesProperty_14() { return &___spotLightIntensitiesProperty_14; }
	inline void set_spotLightIntensitiesProperty_14(String_t* value)
	{
		___spotLightIntensitiesProperty_14 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightIntensitiesProperty_14), value);
	}

	inline static int32_t get_offset_of_spotLightSmoothnessProperty_15() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___spotLightSmoothnessProperty_15)); }
	inline String_t* get_spotLightSmoothnessProperty_15() const { return ___spotLightSmoothnessProperty_15; }
	inline String_t** get_address_of_spotLightSmoothnessProperty_15() { return &___spotLightSmoothnessProperty_15; }
	inline void set_spotLightSmoothnessProperty_15(String_t* value)
	{
		___spotLightSmoothnessProperty_15 = value;
		Il2CppCodeGenWriteBarrier((&___spotLightSmoothnessProperty_15), value);
	}

	inline static int32_t get_offset_of_worldToModel_24() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___worldToModel_24)); }
	inline Matrix4x4U5BU5D_t2328801282* get_worldToModel_24() const { return ___worldToModel_24; }
	inline Matrix4x4U5BU5D_t2328801282** get_address_of_worldToModel_24() { return &___worldToModel_24; }
	inline void set_worldToModel_24(Matrix4x4U5BU5D_t2328801282* value)
	{
		___worldToModel_24 = value;
		Il2CppCodeGenWriteBarrier((&___worldToModel_24), value);
	}

	inline static int32_t get_offset_of_forward_25() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___forward_25)); }
	inline Vector4U5BU5D_t1658499504* get_forward_25() const { return ___forward_25; }
	inline Vector4U5BU5D_t1658499504** get_address_of_forward_25() { return &___forward_25; }
	inline void set_forward_25(Vector4U5BU5D_t1658499504* value)
	{
		___forward_25 = value;
		Il2CppCodeGenWriteBarrier((&___forward_25), value);
	}

	inline static int32_t get_offset_of_baseRadius_26() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___baseRadius_26)); }
	inline SingleU5BU5D_t577127397* get_baseRadius_26() const { return ___baseRadius_26; }
	inline SingleU5BU5D_t577127397** get_address_of_baseRadius_26() { return &___baseRadius_26; }
	inline void set_baseRadius_26(SingleU5BU5D_t577127397* value)
	{
		___baseRadius_26 = value;
		Il2CppCodeGenWriteBarrier((&___baseRadius_26), value);
	}

	inline static int32_t get_offset_of_height_27() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___height_27)); }
	inline SingleU5BU5D_t577127397* get_height_27() const { return ___height_27; }
	inline SingleU5BU5D_t577127397** get_address_of_height_27() { return &___height_27; }
	inline void set_height_27(SingleU5BU5D_t577127397* value)
	{
		___height_27 = value;
		Il2CppCodeGenWriteBarrier((&___height_27), value);
	}

	inline static int32_t get_offset_of_distances_28() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___distances_28)); }
	inline Vector4U5BU5D_t1658499504* get_distances_28() const { return ___distances_28; }
	inline Vector4U5BU5D_t1658499504** get_address_of_distances_28() { return &___distances_28; }
	inline void set_distances_28(Vector4U5BU5D_t1658499504* value)
	{
		___distances_28 = value;
		Il2CppCodeGenWriteBarrier((&___distances_28), value);
	}

	inline static int32_t get_offset_of_intensities_29() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___intensities_29)); }
	inline Vector4U5BU5D_t1658499504* get_intensities_29() const { return ___intensities_29; }
	inline Vector4U5BU5D_t1658499504** get_address_of_intensities_29() { return &___intensities_29; }
	inline void set_intensities_29(Vector4U5BU5D_t1658499504* value)
	{
		___intensities_29 = value;
		Il2CppCodeGenWriteBarrier((&___intensities_29), value);
	}

	inline static int32_t get_offset_of_smoothness_30() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___smoothness_30)); }
	inline SingleU5BU5D_t577127397* get_smoothness_30() const { return ___smoothness_30; }
	inline SingleU5BU5D_t577127397** get_address_of_smoothness_30() { return &___smoothness_30; }
	inline void set_smoothness_30(SingleU5BU5D_t577127397* value)
	{
		___smoothness_30 = value;
		Il2CppCodeGenWriteBarrier((&___smoothness_30), value);
	}

	inline static int32_t get_offset_of_color_31() { return static_cast<int32_t>(offsetof(SpotLight_t983333132_StaticFields, ___color_31)); }
	inline Vector4U5BU5D_t1658499504* get_color_31() const { return ___color_31; }
	inline Vector4U5BU5D_t1658499504** get_address_of_color_31() { return &___color_31; }
	inline void set_color_31(Vector4U5BU5D_t1658499504* value)
	{
		___color_31 = value;
		Il2CppCodeGenWriteBarrier((&___color_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPOTLIGHT_T983333132_H
#ifndef AUDIOMANAGER_T4222704959_H
#define AUDIOMANAGER_T4222704959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t4222704959  : public Singleton_1_t1103622050
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> AudioManager::soundEffects
	List_1_t1301679762 * ___soundEffects_3;

public:
	inline static int32_t get_offset_of_soundEffects_3() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___soundEffects_3)); }
	inline List_1_t1301679762 * get_soundEffects_3() const { return ___soundEffects_3; }
	inline List_1_t1301679762 ** get_address_of_soundEffects_3() { return &___soundEffects_3; }
	inline void set_soundEffects_3(List_1_t1301679762 * value)
	{
		___soundEffects_3 = value;
		Il2CppCodeGenWriteBarrier((&___soundEffects_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T4222704959_H
#ifndef SHADOWPROJECTOR_T3468829328_H
#define SHADOWPROJECTOR_T3468829328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.ShadowProjector
struct  ShadowProjector_t3468829328  : public LightSource_1_t46329857
{
public:
	// UnityEngine.Color FlatLighting.ShadowProjector::ShadowColor
	Color_t2020392075  ___ShadowColor_15;
	// UnityEngine.Shader FlatLighting.ShadowProjector::ShadowMapShader
	Shader_t2430389951 * ___ShadowMapShader_16;
	// System.Single FlatLighting.ShadowProjector::Bias
	float ___Bias_17;
	// System.Single FlatLighting.ShadowProjector::ShadowBlur
	float ___ShadowBlur_18;
	// System.Int32 FlatLighting.ShadowProjector::MemoryToUse
	int32_t ___MemoryToUse_19;
	// System.Boolean FlatLighting.ShadowProjector::isRealTime
	bool ___isRealTime_20;
	// System.Boolean FlatLighting.ShadowProjector::DebugShadowMappingTexture
	bool ___DebugShadowMappingTexture_21;
	// System.Boolean FlatLighting.ShadowProjector::isFirstPass
	bool ___isFirstPass_22;
	// UnityEngine.GameObject FlatLighting.ShadowProjector::debugObject
	GameObject_t1756533147 * ___debugObject_23;
	// UnityEngine.Material FlatLighting.ShadowProjector::debugMaterial
	Material_t193706927 * ___debugMaterial_24;
	// UnityEngine.Camera FlatLighting.ShadowProjector::ShadowCamera
	Camera_t189460977 * ___ShadowCamera_25;
	// System.Boolean FlatLighting.ShadowProjector::ShadowMapRequested
	bool ___ShadowMapRequested_26;
	// UnityEngine.Matrix4x4 FlatLighting.ShadowProjector::ShadowMapOffset
	Matrix4x4_t2933234003  ___ShadowMapOffset_27;
	// UnityEngine.RenderTexture FlatLighting.ShadowProjector::ShadowMapTexture
	RenderTexture_t2666733923 * ___ShadowMapTexture_28;

public:
	inline static int32_t get_offset_of_ShadowColor_15() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowColor_15)); }
	inline Color_t2020392075  get_ShadowColor_15() const { return ___ShadowColor_15; }
	inline Color_t2020392075 * get_address_of_ShadowColor_15() { return &___ShadowColor_15; }
	inline void set_ShadowColor_15(Color_t2020392075  value)
	{
		___ShadowColor_15 = value;
	}

	inline static int32_t get_offset_of_ShadowMapShader_16() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowMapShader_16)); }
	inline Shader_t2430389951 * get_ShadowMapShader_16() const { return ___ShadowMapShader_16; }
	inline Shader_t2430389951 ** get_address_of_ShadowMapShader_16() { return &___ShadowMapShader_16; }
	inline void set_ShadowMapShader_16(Shader_t2430389951 * value)
	{
		___ShadowMapShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___ShadowMapShader_16), value);
	}

	inline static int32_t get_offset_of_Bias_17() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___Bias_17)); }
	inline float get_Bias_17() const { return ___Bias_17; }
	inline float* get_address_of_Bias_17() { return &___Bias_17; }
	inline void set_Bias_17(float value)
	{
		___Bias_17 = value;
	}

	inline static int32_t get_offset_of_ShadowBlur_18() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowBlur_18)); }
	inline float get_ShadowBlur_18() const { return ___ShadowBlur_18; }
	inline float* get_address_of_ShadowBlur_18() { return &___ShadowBlur_18; }
	inline void set_ShadowBlur_18(float value)
	{
		___ShadowBlur_18 = value;
	}

	inline static int32_t get_offset_of_MemoryToUse_19() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___MemoryToUse_19)); }
	inline int32_t get_MemoryToUse_19() const { return ___MemoryToUse_19; }
	inline int32_t* get_address_of_MemoryToUse_19() { return &___MemoryToUse_19; }
	inline void set_MemoryToUse_19(int32_t value)
	{
		___MemoryToUse_19 = value;
	}

	inline static int32_t get_offset_of_isRealTime_20() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___isRealTime_20)); }
	inline bool get_isRealTime_20() const { return ___isRealTime_20; }
	inline bool* get_address_of_isRealTime_20() { return &___isRealTime_20; }
	inline void set_isRealTime_20(bool value)
	{
		___isRealTime_20 = value;
	}

	inline static int32_t get_offset_of_DebugShadowMappingTexture_21() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___DebugShadowMappingTexture_21)); }
	inline bool get_DebugShadowMappingTexture_21() const { return ___DebugShadowMappingTexture_21; }
	inline bool* get_address_of_DebugShadowMappingTexture_21() { return &___DebugShadowMappingTexture_21; }
	inline void set_DebugShadowMappingTexture_21(bool value)
	{
		___DebugShadowMappingTexture_21 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_22() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___isFirstPass_22)); }
	inline bool get_isFirstPass_22() const { return ___isFirstPass_22; }
	inline bool* get_address_of_isFirstPass_22() { return &___isFirstPass_22; }
	inline void set_isFirstPass_22(bool value)
	{
		___isFirstPass_22 = value;
	}

	inline static int32_t get_offset_of_debugObject_23() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___debugObject_23)); }
	inline GameObject_t1756533147 * get_debugObject_23() const { return ___debugObject_23; }
	inline GameObject_t1756533147 ** get_address_of_debugObject_23() { return &___debugObject_23; }
	inline void set_debugObject_23(GameObject_t1756533147 * value)
	{
		___debugObject_23 = value;
		Il2CppCodeGenWriteBarrier((&___debugObject_23), value);
	}

	inline static int32_t get_offset_of_debugMaterial_24() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___debugMaterial_24)); }
	inline Material_t193706927 * get_debugMaterial_24() const { return ___debugMaterial_24; }
	inline Material_t193706927 ** get_address_of_debugMaterial_24() { return &___debugMaterial_24; }
	inline void set_debugMaterial_24(Material_t193706927 * value)
	{
		___debugMaterial_24 = value;
		Il2CppCodeGenWriteBarrier((&___debugMaterial_24), value);
	}

	inline static int32_t get_offset_of_ShadowCamera_25() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowCamera_25)); }
	inline Camera_t189460977 * get_ShadowCamera_25() const { return ___ShadowCamera_25; }
	inline Camera_t189460977 ** get_address_of_ShadowCamera_25() { return &___ShadowCamera_25; }
	inline void set_ShadowCamera_25(Camera_t189460977 * value)
	{
		___ShadowCamera_25 = value;
		Il2CppCodeGenWriteBarrier((&___ShadowCamera_25), value);
	}

	inline static int32_t get_offset_of_ShadowMapRequested_26() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowMapRequested_26)); }
	inline bool get_ShadowMapRequested_26() const { return ___ShadowMapRequested_26; }
	inline bool* get_address_of_ShadowMapRequested_26() { return &___ShadowMapRequested_26; }
	inline void set_ShadowMapRequested_26(bool value)
	{
		___ShadowMapRequested_26 = value;
	}

	inline static int32_t get_offset_of_ShadowMapOffset_27() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowMapOffset_27)); }
	inline Matrix4x4_t2933234003  get_ShadowMapOffset_27() const { return ___ShadowMapOffset_27; }
	inline Matrix4x4_t2933234003 * get_address_of_ShadowMapOffset_27() { return &___ShadowMapOffset_27; }
	inline void set_ShadowMapOffset_27(Matrix4x4_t2933234003  value)
	{
		___ShadowMapOffset_27 = value;
	}

	inline static int32_t get_offset_of_ShadowMapTexture_28() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328, ___ShadowMapTexture_28)); }
	inline RenderTexture_t2666733923 * get_ShadowMapTexture_28() const { return ___ShadowMapTexture_28; }
	inline RenderTexture_t2666733923 ** get_address_of_ShadowMapTexture_28() { return &___ShadowMapTexture_28; }
	inline void set_ShadowMapTexture_28(RenderTexture_t2666733923 * value)
	{
		___ShadowMapTexture_28 = value;
		Il2CppCodeGenWriteBarrier((&___ShadowMapTexture_28), value);
	}
};

struct ShadowProjector_t3468829328_StaticFields
{
public:
	// System.String FlatLighting.ShadowProjector::shadowProjectorCountProperty
	String_t* ___shadowProjectorCountProperty_7;
	// System.String FlatLighting.ShadowProjector::shadowMapMatrixProperty
	String_t* ___shadowMapMatrixProperty_8;
	// System.String FlatLighting.ShadowProjector::shadowModelToViewProperty
	String_t* ___shadowModelToViewProperty_9;
	// System.String FlatLighting.ShadowProjector::shadowColorProperty
	String_t* ___shadowColorProperty_10;
	// System.String FlatLighting.ShadowProjector::shadowBlurProperty
	String_t* ___shadowBlurProperty_11;
	// System.String FlatLighting.ShadowProjector::shadowCameraSettingsProperty
	String_t* ___shadowCameraSettingsProperty_12;
	// System.String FlatLighting.ShadowProjector::shadowTextureProperty
	String_t* ___shadowTextureProperty_13;
	// System.String FlatLighting.ShadowProjector::DEBUG_SHADOW_TEXTURE_OBJECT_NAME
	String_t* ___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14;
	// UnityEngine.Matrix4x4[] FlatLighting.ShadowProjector::mapMatrix
	Matrix4x4U5BU5D_t2328801282* ___mapMatrix_29;
	// UnityEngine.Matrix4x4[] FlatLighting.ShadowProjector::modelToView
	Matrix4x4U5BU5D_t2328801282* ___modelToView_30;
	// UnityEngine.Vector4[] FlatLighting.ShadowProjector::cameraSettings
	Vector4U5BU5D_t1658499504* ___cameraSettings_31;
	// System.Single[] FlatLighting.ShadowProjector::blur
	SingleU5BU5D_t577127397* ___blur_32;
	// UnityEngine.Vector4[] FlatLighting.ShadowProjector::shadowColor
	Vector4U5BU5D_t1658499504* ___shadowColor_33;

public:
	inline static int32_t get_offset_of_shadowProjectorCountProperty_7() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowProjectorCountProperty_7)); }
	inline String_t* get_shadowProjectorCountProperty_7() const { return ___shadowProjectorCountProperty_7; }
	inline String_t** get_address_of_shadowProjectorCountProperty_7() { return &___shadowProjectorCountProperty_7; }
	inline void set_shadowProjectorCountProperty_7(String_t* value)
	{
		___shadowProjectorCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___shadowProjectorCountProperty_7), value);
	}

	inline static int32_t get_offset_of_shadowMapMatrixProperty_8() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowMapMatrixProperty_8)); }
	inline String_t* get_shadowMapMatrixProperty_8() const { return ___shadowMapMatrixProperty_8; }
	inline String_t** get_address_of_shadowMapMatrixProperty_8() { return &___shadowMapMatrixProperty_8; }
	inline void set_shadowMapMatrixProperty_8(String_t* value)
	{
		___shadowMapMatrixProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___shadowMapMatrixProperty_8), value);
	}

	inline static int32_t get_offset_of_shadowModelToViewProperty_9() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowModelToViewProperty_9)); }
	inline String_t* get_shadowModelToViewProperty_9() const { return ___shadowModelToViewProperty_9; }
	inline String_t** get_address_of_shadowModelToViewProperty_9() { return &___shadowModelToViewProperty_9; }
	inline void set_shadowModelToViewProperty_9(String_t* value)
	{
		___shadowModelToViewProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___shadowModelToViewProperty_9), value);
	}

	inline static int32_t get_offset_of_shadowColorProperty_10() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowColorProperty_10)); }
	inline String_t* get_shadowColorProperty_10() const { return ___shadowColorProperty_10; }
	inline String_t** get_address_of_shadowColorProperty_10() { return &___shadowColorProperty_10; }
	inline void set_shadowColorProperty_10(String_t* value)
	{
		___shadowColorProperty_10 = value;
		Il2CppCodeGenWriteBarrier((&___shadowColorProperty_10), value);
	}

	inline static int32_t get_offset_of_shadowBlurProperty_11() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowBlurProperty_11)); }
	inline String_t* get_shadowBlurProperty_11() const { return ___shadowBlurProperty_11; }
	inline String_t** get_address_of_shadowBlurProperty_11() { return &___shadowBlurProperty_11; }
	inline void set_shadowBlurProperty_11(String_t* value)
	{
		___shadowBlurProperty_11 = value;
		Il2CppCodeGenWriteBarrier((&___shadowBlurProperty_11), value);
	}

	inline static int32_t get_offset_of_shadowCameraSettingsProperty_12() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowCameraSettingsProperty_12)); }
	inline String_t* get_shadowCameraSettingsProperty_12() const { return ___shadowCameraSettingsProperty_12; }
	inline String_t** get_address_of_shadowCameraSettingsProperty_12() { return &___shadowCameraSettingsProperty_12; }
	inline void set_shadowCameraSettingsProperty_12(String_t* value)
	{
		___shadowCameraSettingsProperty_12 = value;
		Il2CppCodeGenWriteBarrier((&___shadowCameraSettingsProperty_12), value);
	}

	inline static int32_t get_offset_of_shadowTextureProperty_13() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowTextureProperty_13)); }
	inline String_t* get_shadowTextureProperty_13() const { return ___shadowTextureProperty_13; }
	inline String_t** get_address_of_shadowTextureProperty_13() { return &___shadowTextureProperty_13; }
	inline void set_shadowTextureProperty_13(String_t* value)
	{
		___shadowTextureProperty_13 = value;
		Il2CppCodeGenWriteBarrier((&___shadowTextureProperty_13), value);
	}

	inline static int32_t get_offset_of_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14)); }
	inline String_t* get_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14() const { return ___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14; }
	inline String_t** get_address_of_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14() { return &___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14; }
	inline void set_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14(String_t* value)
	{
		___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14 = value;
		Il2CppCodeGenWriteBarrier((&___DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14), value);
	}

	inline static int32_t get_offset_of_mapMatrix_29() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___mapMatrix_29)); }
	inline Matrix4x4U5BU5D_t2328801282* get_mapMatrix_29() const { return ___mapMatrix_29; }
	inline Matrix4x4U5BU5D_t2328801282** get_address_of_mapMatrix_29() { return &___mapMatrix_29; }
	inline void set_mapMatrix_29(Matrix4x4U5BU5D_t2328801282* value)
	{
		___mapMatrix_29 = value;
		Il2CppCodeGenWriteBarrier((&___mapMatrix_29), value);
	}

	inline static int32_t get_offset_of_modelToView_30() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___modelToView_30)); }
	inline Matrix4x4U5BU5D_t2328801282* get_modelToView_30() const { return ___modelToView_30; }
	inline Matrix4x4U5BU5D_t2328801282** get_address_of_modelToView_30() { return &___modelToView_30; }
	inline void set_modelToView_30(Matrix4x4U5BU5D_t2328801282* value)
	{
		___modelToView_30 = value;
		Il2CppCodeGenWriteBarrier((&___modelToView_30), value);
	}

	inline static int32_t get_offset_of_cameraSettings_31() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___cameraSettings_31)); }
	inline Vector4U5BU5D_t1658499504* get_cameraSettings_31() const { return ___cameraSettings_31; }
	inline Vector4U5BU5D_t1658499504** get_address_of_cameraSettings_31() { return &___cameraSettings_31; }
	inline void set_cameraSettings_31(Vector4U5BU5D_t1658499504* value)
	{
		___cameraSettings_31 = value;
		Il2CppCodeGenWriteBarrier((&___cameraSettings_31), value);
	}

	inline static int32_t get_offset_of_blur_32() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___blur_32)); }
	inline SingleU5BU5D_t577127397* get_blur_32() const { return ___blur_32; }
	inline SingleU5BU5D_t577127397** get_address_of_blur_32() { return &___blur_32; }
	inline void set_blur_32(SingleU5BU5D_t577127397* value)
	{
		___blur_32 = value;
		Il2CppCodeGenWriteBarrier((&___blur_32), value);
	}

	inline static int32_t get_offset_of_shadowColor_33() { return static_cast<int32_t>(offsetof(ShadowProjector_t3468829328_StaticFields, ___shadowColor_33)); }
	inline Vector4U5BU5D_t1658499504* get_shadowColor_33() const { return ___shadowColor_33; }
	inline Vector4U5BU5D_t1658499504** get_address_of_shadowColor_33() { return &___shadowColor_33; }
	inline void set_shadowColor_33(Vector4U5BU5D_t1658499504* value)
	{
		___shadowColor_33 = value;
		Il2CppCodeGenWriteBarrier((&___shadowColor_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWPROJECTOR_T3468829328_H
#ifndef DIRECTIONALLIGHT_T3190098900_H
#define DIRECTIONALLIGHT_T3190098900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.DirectionalLight
struct  DirectionalLight_t3190098900  : public LightSource_1_t4062566725
{
public:
	// System.Boolean FlatLighting.DirectionalLight::isRealTime
	bool ___isRealTime_10;
	// UnityEngine.Color FlatLighting.DirectionalLight::LightColor
	Color_t2020392075  ___LightColor_11;
	// System.Boolean FlatLighting.DirectionalLight::isFirstPass
	bool ___isFirstPass_12;

public:
	inline static int32_t get_offset_of_isRealTime_10() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900, ___isRealTime_10)); }
	inline bool get_isRealTime_10() const { return ___isRealTime_10; }
	inline bool* get_address_of_isRealTime_10() { return &___isRealTime_10; }
	inline void set_isRealTime_10(bool value)
	{
		___isRealTime_10 = value;
	}

	inline static int32_t get_offset_of_LightColor_11() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900, ___LightColor_11)); }
	inline Color_t2020392075  get_LightColor_11() const { return ___LightColor_11; }
	inline Color_t2020392075 * get_address_of_LightColor_11() { return &___LightColor_11; }
	inline void set_LightColor_11(Color_t2020392075  value)
	{
		___LightColor_11 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_12() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900, ___isFirstPass_12)); }
	inline bool get_isFirstPass_12() const { return ___isFirstPass_12; }
	inline bool* get_address_of_isFirstPass_12() { return &___isFirstPass_12; }
	inline void set_isFirstPass_12(bool value)
	{
		___isFirstPass_12 = value;
	}
};

struct DirectionalLight_t3190098900_StaticFields
{
public:
	// System.String FlatLighting.DirectionalLight::directionalLightCountProperty
	String_t* ___directionalLightCountProperty_7;
	// System.String FlatLighting.DirectionalLight::directionalLightColorProperty
	String_t* ___directionalLightColorProperty_8;
	// System.String FlatLighting.DirectionalLight::directionalLightForwardProperty
	String_t* ___directionalLightForwardProperty_9;
	// UnityEngine.Vector4[] FlatLighting.DirectionalLight::forward
	Vector4U5BU5D_t1658499504* ___forward_13;
	// UnityEngine.Vector4[] FlatLighting.DirectionalLight::color
	Vector4U5BU5D_t1658499504* ___color_14;

public:
	inline static int32_t get_offset_of_directionalLightCountProperty_7() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900_StaticFields, ___directionalLightCountProperty_7)); }
	inline String_t* get_directionalLightCountProperty_7() const { return ___directionalLightCountProperty_7; }
	inline String_t** get_address_of_directionalLightCountProperty_7() { return &___directionalLightCountProperty_7; }
	inline void set_directionalLightCountProperty_7(String_t* value)
	{
		___directionalLightCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___directionalLightCountProperty_7), value);
	}

	inline static int32_t get_offset_of_directionalLightColorProperty_8() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900_StaticFields, ___directionalLightColorProperty_8)); }
	inline String_t* get_directionalLightColorProperty_8() const { return ___directionalLightColorProperty_8; }
	inline String_t** get_address_of_directionalLightColorProperty_8() { return &___directionalLightColorProperty_8; }
	inline void set_directionalLightColorProperty_8(String_t* value)
	{
		___directionalLightColorProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___directionalLightColorProperty_8), value);
	}

	inline static int32_t get_offset_of_directionalLightForwardProperty_9() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900_StaticFields, ___directionalLightForwardProperty_9)); }
	inline String_t* get_directionalLightForwardProperty_9() const { return ___directionalLightForwardProperty_9; }
	inline String_t** get_address_of_directionalLightForwardProperty_9() { return &___directionalLightForwardProperty_9; }
	inline void set_directionalLightForwardProperty_9(String_t* value)
	{
		___directionalLightForwardProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___directionalLightForwardProperty_9), value);
	}

	inline static int32_t get_offset_of_forward_13() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900_StaticFields, ___forward_13)); }
	inline Vector4U5BU5D_t1658499504* get_forward_13() const { return ___forward_13; }
	inline Vector4U5BU5D_t1658499504** get_address_of_forward_13() { return &___forward_13; }
	inline void set_forward_13(Vector4U5BU5D_t1658499504* value)
	{
		___forward_13 = value;
		Il2CppCodeGenWriteBarrier((&___forward_13), value);
	}

	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(DirectionalLight_t3190098900_StaticFields, ___color_14)); }
	inline Vector4U5BU5D_t1658499504* get_color_14() const { return ___color_14; }
	inline Vector4U5BU5D_t1658499504** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(Vector4U5BU5D_t1658499504* value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONALLIGHT_T3190098900_H
#ifndef POINTLIGHT_T2062667992_H
#define POINTLIGHT_T2062667992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlatLighting.PointLight
struct  PointLight_t2062667992  : public LightSource_1_t2935135817
{
public:
	// System.Single FlatLighting.PointLight::Range
	float ___Range_13;
	// UnityEngine.Color FlatLighting.PointLight::LightColor
	Color_t2020392075  ___LightColor_14;
	// UnityEngine.Vector4 FlatLighting.PointLight::LightDistances
	Vector4_t2243707581  ___LightDistances_15;
	// UnityEngine.Vector4 FlatLighting.PointLight::LightIntensities
	Vector4_t2243707581  ___LightIntensities_16;
	// System.Boolean FlatLighting.PointLight::Smooth
	bool ___Smooth_17;
	// System.Boolean FlatLighting.PointLight::isRealTime
	bool ___isRealTime_18;
	// System.Boolean FlatLighting.PointLight::isFirstPass
	bool ___isFirstPass_19;

public:
	inline static int32_t get_offset_of_Range_13() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___Range_13)); }
	inline float get_Range_13() const { return ___Range_13; }
	inline float* get_address_of_Range_13() { return &___Range_13; }
	inline void set_Range_13(float value)
	{
		___Range_13 = value;
	}

	inline static int32_t get_offset_of_LightColor_14() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___LightColor_14)); }
	inline Color_t2020392075  get_LightColor_14() const { return ___LightColor_14; }
	inline Color_t2020392075 * get_address_of_LightColor_14() { return &___LightColor_14; }
	inline void set_LightColor_14(Color_t2020392075  value)
	{
		___LightColor_14 = value;
	}

	inline static int32_t get_offset_of_LightDistances_15() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___LightDistances_15)); }
	inline Vector4_t2243707581  get_LightDistances_15() const { return ___LightDistances_15; }
	inline Vector4_t2243707581 * get_address_of_LightDistances_15() { return &___LightDistances_15; }
	inline void set_LightDistances_15(Vector4_t2243707581  value)
	{
		___LightDistances_15 = value;
	}

	inline static int32_t get_offset_of_LightIntensities_16() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___LightIntensities_16)); }
	inline Vector4_t2243707581  get_LightIntensities_16() const { return ___LightIntensities_16; }
	inline Vector4_t2243707581 * get_address_of_LightIntensities_16() { return &___LightIntensities_16; }
	inline void set_LightIntensities_16(Vector4_t2243707581  value)
	{
		___LightIntensities_16 = value;
	}

	inline static int32_t get_offset_of_Smooth_17() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___Smooth_17)); }
	inline bool get_Smooth_17() const { return ___Smooth_17; }
	inline bool* get_address_of_Smooth_17() { return &___Smooth_17; }
	inline void set_Smooth_17(bool value)
	{
		___Smooth_17 = value;
	}

	inline static int32_t get_offset_of_isRealTime_18() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___isRealTime_18)); }
	inline bool get_isRealTime_18() const { return ___isRealTime_18; }
	inline bool* get_address_of_isRealTime_18() { return &___isRealTime_18; }
	inline void set_isRealTime_18(bool value)
	{
		___isRealTime_18 = value;
	}

	inline static int32_t get_offset_of_isFirstPass_19() { return static_cast<int32_t>(offsetof(PointLight_t2062667992, ___isFirstPass_19)); }
	inline bool get_isFirstPass_19() const { return ___isFirstPass_19; }
	inline bool* get_address_of_isFirstPass_19() { return &___isFirstPass_19; }
	inline void set_isFirstPass_19(bool value)
	{
		___isFirstPass_19 = value;
	}
};

struct PointLight_t2062667992_StaticFields
{
public:
	// System.String FlatLighting.PointLight::pointLightCountProperty
	String_t* ___pointLightCountProperty_7;
	// System.String FlatLighting.PointLight::pointLight0WorldToModelProperty
	String_t* ___pointLight0WorldToModelProperty_8;
	// System.String FlatLighting.PointLight::pointLightColorProperty
	String_t* ___pointLightColorProperty_9;
	// System.String FlatLighting.PointLight::pointLightDistancesProperty
	String_t* ___pointLightDistancesProperty_10;
	// System.String FlatLighting.PointLight::pointLightIntensitiesProperty
	String_t* ___pointLightIntensitiesProperty_11;
	// System.String FlatLighting.PointLight::pointLightSmoothnessProperty
	String_t* ___pointLightSmoothnessProperty_12;
	// UnityEngine.Matrix4x4[] FlatLighting.PointLight::worldToModel
	Matrix4x4U5BU5D_t2328801282* ___worldToModel_20;
	// UnityEngine.Vector4[] FlatLighting.PointLight::distances
	Vector4U5BU5D_t1658499504* ___distances_21;
	// UnityEngine.Vector4[] FlatLighting.PointLight::intensities
	Vector4U5BU5D_t1658499504* ___intensities_22;
	// System.Single[] FlatLighting.PointLight::smoothness
	SingleU5BU5D_t577127397* ___smoothness_23;
	// UnityEngine.Vector4[] FlatLighting.PointLight::color
	Vector4U5BU5D_t1658499504* ___color_24;

public:
	inline static int32_t get_offset_of_pointLightCountProperty_7() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___pointLightCountProperty_7)); }
	inline String_t* get_pointLightCountProperty_7() const { return ___pointLightCountProperty_7; }
	inline String_t** get_address_of_pointLightCountProperty_7() { return &___pointLightCountProperty_7; }
	inline void set_pointLightCountProperty_7(String_t* value)
	{
		___pointLightCountProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightCountProperty_7), value);
	}

	inline static int32_t get_offset_of_pointLight0WorldToModelProperty_8() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___pointLight0WorldToModelProperty_8)); }
	inline String_t* get_pointLight0WorldToModelProperty_8() const { return ___pointLight0WorldToModelProperty_8; }
	inline String_t** get_address_of_pointLight0WorldToModelProperty_8() { return &___pointLight0WorldToModelProperty_8; }
	inline void set_pointLight0WorldToModelProperty_8(String_t* value)
	{
		___pointLight0WorldToModelProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___pointLight0WorldToModelProperty_8), value);
	}

	inline static int32_t get_offset_of_pointLightColorProperty_9() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___pointLightColorProperty_9)); }
	inline String_t* get_pointLightColorProperty_9() const { return ___pointLightColorProperty_9; }
	inline String_t** get_address_of_pointLightColorProperty_9() { return &___pointLightColorProperty_9; }
	inline void set_pointLightColorProperty_9(String_t* value)
	{
		___pointLightColorProperty_9 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightColorProperty_9), value);
	}

	inline static int32_t get_offset_of_pointLightDistancesProperty_10() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___pointLightDistancesProperty_10)); }
	inline String_t* get_pointLightDistancesProperty_10() const { return ___pointLightDistancesProperty_10; }
	inline String_t** get_address_of_pointLightDistancesProperty_10() { return &___pointLightDistancesProperty_10; }
	inline void set_pointLightDistancesProperty_10(String_t* value)
	{
		___pointLightDistancesProperty_10 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightDistancesProperty_10), value);
	}

	inline static int32_t get_offset_of_pointLightIntensitiesProperty_11() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___pointLightIntensitiesProperty_11)); }
	inline String_t* get_pointLightIntensitiesProperty_11() const { return ___pointLightIntensitiesProperty_11; }
	inline String_t** get_address_of_pointLightIntensitiesProperty_11() { return &___pointLightIntensitiesProperty_11; }
	inline void set_pointLightIntensitiesProperty_11(String_t* value)
	{
		___pointLightIntensitiesProperty_11 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightIntensitiesProperty_11), value);
	}

	inline static int32_t get_offset_of_pointLightSmoothnessProperty_12() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___pointLightSmoothnessProperty_12)); }
	inline String_t* get_pointLightSmoothnessProperty_12() const { return ___pointLightSmoothnessProperty_12; }
	inline String_t** get_address_of_pointLightSmoothnessProperty_12() { return &___pointLightSmoothnessProperty_12; }
	inline void set_pointLightSmoothnessProperty_12(String_t* value)
	{
		___pointLightSmoothnessProperty_12 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightSmoothnessProperty_12), value);
	}

	inline static int32_t get_offset_of_worldToModel_20() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___worldToModel_20)); }
	inline Matrix4x4U5BU5D_t2328801282* get_worldToModel_20() const { return ___worldToModel_20; }
	inline Matrix4x4U5BU5D_t2328801282** get_address_of_worldToModel_20() { return &___worldToModel_20; }
	inline void set_worldToModel_20(Matrix4x4U5BU5D_t2328801282* value)
	{
		___worldToModel_20 = value;
		Il2CppCodeGenWriteBarrier((&___worldToModel_20), value);
	}

	inline static int32_t get_offset_of_distances_21() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___distances_21)); }
	inline Vector4U5BU5D_t1658499504* get_distances_21() const { return ___distances_21; }
	inline Vector4U5BU5D_t1658499504** get_address_of_distances_21() { return &___distances_21; }
	inline void set_distances_21(Vector4U5BU5D_t1658499504* value)
	{
		___distances_21 = value;
		Il2CppCodeGenWriteBarrier((&___distances_21), value);
	}

	inline static int32_t get_offset_of_intensities_22() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___intensities_22)); }
	inline Vector4U5BU5D_t1658499504* get_intensities_22() const { return ___intensities_22; }
	inline Vector4U5BU5D_t1658499504** get_address_of_intensities_22() { return &___intensities_22; }
	inline void set_intensities_22(Vector4U5BU5D_t1658499504* value)
	{
		___intensities_22 = value;
		Il2CppCodeGenWriteBarrier((&___intensities_22), value);
	}

	inline static int32_t get_offset_of_smoothness_23() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___smoothness_23)); }
	inline SingleU5BU5D_t577127397* get_smoothness_23() const { return ___smoothness_23; }
	inline SingleU5BU5D_t577127397** get_address_of_smoothness_23() { return &___smoothness_23; }
	inline void set_smoothness_23(SingleU5BU5D_t577127397* value)
	{
		___smoothness_23 = value;
		Il2CppCodeGenWriteBarrier((&___smoothness_23), value);
	}

	inline static int32_t get_offset_of_color_24() { return static_cast<int32_t>(offsetof(PointLight_t2062667992_StaticFields, ___color_24)); }
	inline Vector4U5BU5D_t1658499504* get_color_24() const { return ___color_24; }
	inline Vector4U5BU5D_t1658499504** get_address_of_color_24() { return &___color_24; }
	inline void set_color_24(Vector4U5BU5D_t1658499504* value)
	{
		___color_24 = value;
		Il2CppCodeGenWriteBarrier((&___color_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTLIGHT_T2062667992_H
#ifndef MIRABASERAYCASTER_T1612955938_H
#define MIRABASERAYCASTER_T1612955938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster
struct  MiraBaseRaycaster_t1612955938  : public BaseRaycaster_t2336171397
{
public:
	// MiraBaseRaycaster/RaycastStyle MiraBaseRaycaster::raycastStyle
	int32_t ___raycastStyle_2;
	// UnityEngine.Ray MiraBaseRaycaster::lastray
	Ray_t2469606224  ___lastray_3;

public:
	inline static int32_t get_offset_of_raycastStyle_2() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t1612955938, ___raycastStyle_2)); }
	inline int32_t get_raycastStyle_2() const { return ___raycastStyle_2; }
	inline int32_t* get_address_of_raycastStyle_2() { return &___raycastStyle_2; }
	inline void set_raycastStyle_2(int32_t value)
	{
		___raycastStyle_2 = value;
	}

	inline static int32_t get_offset_of_lastray_3() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t1612955938, ___lastray_3)); }
	inline Ray_t2469606224  get_lastray_3() const { return ___lastray_3; }
	inline Ray_t2469606224 * get_address_of_lastray_3() { return &___lastray_3; }
	inline void set_lastray_3(Ray_t2469606224  value)
	{
		___lastray_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASERAYCASTER_T1612955938_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (TransformProperties_t3533762257)+ sizeof (RuntimeObject), sizeof(TransformProperties_t3533762257 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[4] = 
{
	TransformProperties_t3533762257::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformProperties_t3533762257::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformProperties_t3533762257::get_offset_of_Scale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformProperties_t3533762257::get_offset_of_FieldOfView_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (UnityVersion_t3349894225)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[7] = 
{
	UnityVersion_t3349894225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (PlatformBase_t3918687204), -1, sizeof(PlatformBase_t3918687204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2102[1] = 
{
	PlatformBase_t3918687204_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (PointCloudRenderer_t1003576694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[10] = 
{
	PointCloudRenderer_t1003576694::get_offset_of__testMesh_0(),
	PointCloudRenderer_t1003576694::get_offset_of__sceneCamera_1(),
	PointCloudRenderer_t1003576694::get_offset_of__renderMaterial_2(),
	PointCloudRenderer_t1003576694::get_offset_of__cubePoints_3(),
	PointCloudRenderer_t1003576694::get_offset_of__cubeIndices_4(),
	PointCloudRenderer_t1003576694::get_offset_of__meshVertexCount_5(),
	PointCloudRenderer_t1003576694::get_offset_of__drawWithCommandBuffer_6(),
	PointCloudRenderer_t1003576694::get_offset_of__vertices_7(),
	PointCloudRenderer_t1003576694::get_offset_of__indices_8(),
	PointCloudRenderer_t1003576694::get_offset_of__vertexColors_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (MapPointCloud_t2215873309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[3] = 
{
	MapPointCloud_t2215873309::get_offset_of_FeaturePoints_0(),
	MapPointCloud_t2215873309::get_offset_of_Colors_1(),
	MapPointCloud_t2215873309::get_offset_of_Scale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TrackableBehaviour_t3643631172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[14] = 
{
	TrackableBehaviour_t3643631172::get_offset_of__targetPattern_2(),
	TrackableBehaviour_t3643631172::get_offset_of__targetPatternRegex_3(),
	TrackableBehaviour_t3643631172::get_offset_of__isKnown_4(),
	TrackableBehaviour_t3643631172::get_offset_of__currentTargetName_5(),
	TrackableBehaviour_t3643631172::get_offset_of__extendedTracking_6(),
	TrackableBehaviour_t3643631172::get_offset_of__targetsForExtendedTracking_7(),
	TrackableBehaviour_t3643631172::get_offset_of__autoToggleVisibility_8(),
	TrackableBehaviour_t3643631172::get_offset_of_OnEnterFieldOfVision_9(),
	TrackableBehaviour_t3643631172::get_offset_of_OnExitFieldOfVision_10(),
	TrackableBehaviour_t3643631172::get_offset_of__eventsFoldout_11(),
	TrackableBehaviour_t3643631172::get_offset_of__registeredToTracker_12(),
	TrackableBehaviour_t3643631172::get_offset_of__preview_13(),
	TrackableBehaviour_t3643631172::get_offset_of__previewMaterial_14(),
	TrackableBehaviour_t3643631172::get_offset_of__previewMesh_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (OnEnterFieldOfVisionEvent_t3362888889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (OnExitFieldOfVisionEvent_t2513344377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2108[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D81E6729BCDCEC6DFB58051B1D04EB2D2835D10FB_0(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D5C26586BC03E63060E1A58A973915A014ECFCA38_1(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_2(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (U24ArrayTypeU3D64_t762068660)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D64_t762068660 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (U3CModuleU3E_t3783534237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (SpinAnimation_t4115072874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (CameraControl_t2838268856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[10] = 
{
	CameraControl_t2838268856::get_offset_of_pointOfInterest_2(),
	CameraControl_t2838268856::get_offset_of_shouldRotateAutomatic_3(),
	CameraControl_t2838268856::get_offset_of_rotationSpeed_4(),
	CameraControl_t2838268856::get_offset_of_minRadius_5(),
	CameraControl_t2838268856::get_offset_of_maxRadius_6(),
	CameraControl_t2838268856::get_offset_of_minZoom_7(),
	CameraControl_t2838268856::get_offset_of_maxZoom_8(),
	CameraControl_t2838268856::get_offset_of_myCamera_9(),
	CameraControl_t2838268856::get_offset_of_lastNormalizedPosition_10(),
	CameraControl_t2838268856::get_offset_of_radius_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (MouseOrbit_t3219898523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[32] = 
{
	MouseOrbit_t3219898523::get_offset_of_target_2(),
	MouseOrbit_t3219898523::get_offset_of_yPosClamp_3(),
	MouseOrbit_t3219898523::get_offset_of_targetFocusLerpSpeed_4(),
	MouseOrbit_t3219898523::get_offset_of_initialDistance_5(),
	MouseOrbit_t3219898523::get_offset_of_smooth_6(),
	MouseOrbit_t3219898523::get_offset_of_xSpeed_7(),
	MouseOrbit_t3219898523::get_offset_of_ySpeed_8(),
	MouseOrbit_t3219898523::get_offset_of_wasdSpeed_9(),
	MouseOrbit_t3219898523::get_offset_of_wasdSmooth_10(),
	MouseOrbit_t3219898523::get_offset_of_panSpeed_11(),
	MouseOrbit_t3219898523::get_offset_of_zoomSmooth_12(),
	MouseOrbit_t3219898523::get_offset_of_minZoom_13(),
	MouseOrbit_t3219898523::get_offset_of_maxZoom_14(),
	MouseOrbit_t3219898523::get_offset_of_mouseSensitivityScaler_15(),
	MouseOrbit_t3219898523::get_offset_of_scrollSensitivityScaler_16(),
	MouseOrbit_t3219898523::get_offset_of_distance_17(),
	MouseOrbit_t3219898523::get_offset_of_yMaxLimit_18(),
	MouseOrbit_t3219898523::get_offset_of_yMinLimit_19(),
	MouseOrbit_t3219898523::get_offset_of_smoothY_20(),
	MouseOrbit_t3219898523::get_offset_of_smoothX_21(),
	MouseOrbit_t3219898523::get_offset_of_movementPOS_22(),
	MouseOrbit_t3219898523::get_offset_of_rotation_23(),
	MouseOrbit_t3219898523::get_offset_of_position_24(),
	MouseOrbit_t3219898523::get_offset_of_clickTimey_25(),
	MouseOrbit_t3219898523::get_offset_of_myTransform_26(),
	MouseOrbit_t3219898523::get_offset_of_posToBe_27(),
	MouseOrbit_t3219898523::get_offset_of_zoomSmoothDelegate_28(),
	MouseOrbit_t3219898523::get_offset_of_x_29(),
	MouseOrbit_t3219898523::get_offset_of_y_30(),
	MouseOrbit_t3219898523::get_offset_of_movementPosOffset_31(),
	MouseOrbit_t3219898523::get_offset_of_isOrthographic_32(),
	MouseOrbit_t3219898523::get_offset_of_myCamera_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (MaterialSwapper_t2559573863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[6] = 
{
	MaterialSwapper_t2559573863::get_offset_of_materials_2(),
	MaterialSwapper_t2559573863::get_offset_of_UnityLightsRoot_3(),
	MaterialSwapper_t2559573863::get_offset_of_UnityLightsMaterialIndex_4(),
	MaterialSwapper_t2559573863::get_offset_of_FlatLightsRoot_5(),
	MaterialSwapper_t2559573863::get_offset_of_FlatLightsMaterialIndex_6(),
	MaterialSwapper_t2559573863::get_offset_of_myRenderer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (LightingManager_t1089587063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	LightingManager_t1089587063::get_offset_of_day_2(),
	LightingManager_t1089587063::get_offset_of_night_3(),
	LightingManager_t1089587063::get_offset_of_root_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (SceneLigtingSetup_t3978848013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[23] = 
{
	SceneLigtingSetup_t3978848013::get_offset_of_cameraBackground_2(),
	SceneLigtingSetup_t3978848013::get_offset_of_globalMaterial_3(),
	SceneLigtingSetup_t3978848013::get_offset_of_vegetationMaterial_4(),
	SceneLigtingSetup_t3978848013::get_offset_of_vegetationRoot_5(),
	SceneLigtingSetup_t3978848013::get_offset_of_bridgeMaterial_6(),
	SceneLigtingSetup_t3978848013::get_offset_of_bridgeRoot_7(),
	SceneLigtingSetup_t3978848013::get_offset_of_lookoutMaterial_8(),
	SceneLigtingSetup_t3978848013::get_offset_of_lookoutRoot_9(),
	SceneLigtingSetup_t3978848013::get_offset_of_towerMaterial_10(),
	SceneLigtingSetup_t3978848013::get_offset_of_towerRoot_11(),
	SceneLigtingSetup_t3978848013::get_offset_of_deadTreeMaterial_12(),
	SceneLigtingSetup_t3978848013::get_offset_of_deadTreeRoot_13(),
	SceneLigtingSetup_t3978848013::get_offset_of_rocksMaterial_14(),
	SceneLigtingSetup_t3978848013::get_offset_of_rocksTreeRoot_15(),
	SceneLigtingSetup_t3978848013::get_offset_of_objectsToEnable_16(),
	SceneLigtingSetup_t3978848013::get_offset_of_sceneRenderers_17(),
	SceneLigtingSetup_t3978848013::get_offset_of_vegetationRenderers_18(),
	SceneLigtingSetup_t3978848013::get_offset_of_bridgeRenderers_19(),
	SceneLigtingSetup_t3978848013::get_offset_of_lookoutRenderers_20(),
	SceneLigtingSetup_t3978848013::get_offset_of_towerRenderers_21(),
	SceneLigtingSetup_t3978848013::get_offset_of_deadTreeRenderers_22(),
	SceneLigtingSetup_t3978848013::get_offset_of_boatRenderers_23(),
	SceneLigtingSetup_t3978848013::get_offset_of_rocksRenderers_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (MaterialBlender_t4011564903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[4] = 
{
	MaterialBlender_t4011564903::get_offset_of_durationSeconds_2(),
	MaterialBlender_t4011564903::get_offset_of_loop_3(),
	MaterialBlender_t4011564903::get_offset_of_materials_4(),
	MaterialBlender_t4011564903::get_offset_of_internalMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (U3CUpdateBlendingU3Ec__Iterator0_t3290861102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[10] = 
{
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U3CcurrentMaterialIndexU3E__0_0(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U3CnextMaterialIndexU3E__0_1(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U3CiU3E__1_2(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U3CcurrentMaterialU3E__2_3(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U3CnextMaterialU3E__2_4(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U3CtU3E__3_5(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U24this_6(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U24current_7(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U24disposing_8(),
	U3CUpdateBlendingU3Ec__Iterator0_t3290861102::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (Constants_t1236580865), -1, sizeof(Constants_t1236580865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2119[4] = 
{
	Constants_t1236580865_StaticFields::get_offset_of_FlatLightingShaderPath_0(),
	Constants_t1236580865_StaticFields::get_offset_of_FlatLightingTag_1(),
	Constants_t1236580865_StaticFields::get_offset_of_FlatLightingBakedTag_2(),
	Constants_t1236580865_StaticFields::get_offset_of_GizmoIconsPath_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (Shader_t1450904180), -1, sizeof(Shader_t1450904180_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[46] = 
{
	Shader_t1450904180_StaticFields::get_offset_of_AXIS_COLORS_LOCAL_0(),
	Shader_t1450904180_StaticFields::get_offset_of_AXIS_COLORS_GLOBAL_1(),
	Shader_t1450904180_StaticFields::get_offset_of_SYMETRIC_COLORS_ON_KEYWORD_2(),
	Shader_t1450904180_StaticFields::get_offset_of_SYMETRIC_COLORS_OFF_KEYWORD_3(),
	Shader_t1450904180_StaticFields::get_offset_of_AXIS_GRADIENT_ON_X_KEYWORD_4(),
	Shader_t1450904180_StaticFields::get_offset_of_AXIS_GRADIENT_ON_Y_KEYWORD_5(),
	Shader_t1450904180_StaticFields::get_offset_of_AXIS_GRADIENT_ON_Z_KEYWORD_6(),
	Shader_t1450904180_StaticFields::get_offset_of_AXIS_GRADIENT_OFF_KEYWORD_7(),
	Shader_t1450904180_StaticFields::get_offset_of_VERTEX_COLOR_KEYWORD_8(),
	Shader_t1450904180_StaticFields::get_offset_of_AMBIENT_LIGHT_KEYWORD_9(),
	Shader_t1450904180_StaticFields::get_offset_of_DIRECT_LIGHT_KEYWORD_10(),
	Shader_t1450904180_StaticFields::get_offset_of_SPOT_LIGHT_KEYWORD_11(),
	Shader_t1450904180_StaticFields::get_offset_of_POINT_LIGHT_KEYWORD_12(),
	Shader_t1450904180_StaticFields::get_offset_of_BLEND_LIGHT_SOURCES_KEYWORD_13(),
	Shader_t1450904180_StaticFields::get_offset_of_GRADIENT_LOCAL_KEYWORD_14(),
	Shader_t1450904180_StaticFields::get_offset_of_GRADIENT_WORLD_KEYWORD_15(),
	Shader_t1450904180_StaticFields::get_offset_of_CUSTOM_LIGHTMAPPING_KEYWORD_16(),
	Shader_t1450904180_StaticFields::get_offset_of_UNITY_LIGHTMAPPING_KEYWORD_17(),
	Shader_t1450904180_StaticFields::get_offset_of_RECEIVE_CUSTOM_SHADOW_KEYWORD_18(),
	Shader_t1450904180_StaticFields::get_offset_of_CAST_CUSTOM_SHADOW_ON_KEYWORD_19(),
	Shader_t1450904180_StaticFields::get_offset_of_CAST_CUSTOM_SHADOW_OFF_KEYWORD_20(),
	Shader_t1450904180_StaticFields::get_offset_of_USE_MAIN_TEXTURE_KEYWORD_21(),
	Shader_t1450904180_StaticFields::get_offset_of_LightPositiveX_22(),
	Shader_t1450904180_StaticFields::get_offset_of_LightPositiveY_23(),
	Shader_t1450904180_StaticFields::get_offset_of_LightPositiveZ_24(),
	Shader_t1450904180_StaticFields::get_offset_of_LightNegativeX_25(),
	Shader_t1450904180_StaticFields::get_offset_of_LightNegativeY_26(),
	Shader_t1450904180_StaticFields::get_offset_of_LightNegativeZ_27(),
	Shader_t1450904180_StaticFields::get_offset_of_LightPositive2X_28(),
	Shader_t1450904180_StaticFields::get_offset_of_LightPositive2Y_29(),
	Shader_t1450904180_StaticFields::get_offset_of_LightPositive2Z_30(),
	Shader_t1450904180_StaticFields::get_offset_of_LightNegative2X_31(),
	Shader_t1450904180_StaticFields::get_offset_of_LightNegative2Y_32(),
	Shader_t1450904180_StaticFields::get_offset_of_LightNegative2Z_33(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientWidthPositiveX_34(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientWidthPositiveY_35(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientWidthPositiveZ_36(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientWidthNegativeX_37(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientWidthNegativeY_38(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientWidthNegativeZ_39(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientOriginOffsetPositiveX_40(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientOriginOffsetPositiveY_41(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientOriginOffsetPositiveZ_42(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientOriginOffsetNegativeX_43(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientOriginOffsetNegativeY_44(),
	Shader_t1450904180_StaticFields::get_offset_of_GradientOriginOffsetNegativeZ_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (DirectionalLight_t3190098900), -1, sizeof(DirectionalLight_t3190098900_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2121[8] = 
{
	DirectionalLight_t3190098900_StaticFields::get_offset_of_directionalLightCountProperty_7(),
	DirectionalLight_t3190098900_StaticFields::get_offset_of_directionalLightColorProperty_8(),
	DirectionalLight_t3190098900_StaticFields::get_offset_of_directionalLightForwardProperty_9(),
	DirectionalLight_t3190098900::get_offset_of_isRealTime_10(),
	DirectionalLight_t3190098900::get_offset_of_LightColor_11(),
	DirectionalLight_t3190098900::get_offset_of_isFirstPass_12(),
	DirectionalLight_t3190098900_StaticFields::get_offset_of_forward_13(),
	DirectionalLight_t3190098900_StaticFields::get_offset_of_color_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (PointLight_t2062667992), -1, sizeof(PointLight_t2062667992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2124[18] = 
{
	PointLight_t2062667992_StaticFields::get_offset_of_pointLightCountProperty_7(),
	PointLight_t2062667992_StaticFields::get_offset_of_pointLight0WorldToModelProperty_8(),
	PointLight_t2062667992_StaticFields::get_offset_of_pointLightColorProperty_9(),
	PointLight_t2062667992_StaticFields::get_offset_of_pointLightDistancesProperty_10(),
	PointLight_t2062667992_StaticFields::get_offset_of_pointLightIntensitiesProperty_11(),
	PointLight_t2062667992_StaticFields::get_offset_of_pointLightSmoothnessProperty_12(),
	PointLight_t2062667992::get_offset_of_Range_13(),
	PointLight_t2062667992::get_offset_of_LightColor_14(),
	PointLight_t2062667992::get_offset_of_LightDistances_15(),
	PointLight_t2062667992::get_offset_of_LightIntensities_16(),
	PointLight_t2062667992::get_offset_of_Smooth_17(),
	PointLight_t2062667992::get_offset_of_isRealTime_18(),
	PointLight_t2062667992::get_offset_of_isFirstPass_19(),
	PointLight_t2062667992_StaticFields::get_offset_of_worldToModel_20(),
	PointLight_t2062667992_StaticFields::get_offset_of_distances_21(),
	PointLight_t2062667992_StaticFields::get_offset_of_intensities_22(),
	PointLight_t2062667992_StaticFields::get_offset_of_smoothness_23(),
	PointLight_t2062667992_StaticFields::get_offset_of_color_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (SpotLight_t983333132), -1, sizeof(SpotLight_t983333132_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2125[25] = 
{
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightCountProperty_7(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightWorldToModelProperty_8(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightForwardProperty_9(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightColorProperty_10(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightBaseRadiusProperty_11(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightHeightProperty_12(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightDistancesProperty_13(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightIntensitiesProperty_14(),
	SpotLight_t983333132_StaticFields::get_offset_of_spotLightSmoothnessProperty_15(),
	SpotLight_t983333132::get_offset_of_BaseRadius_16(),
	SpotLight_t983333132::get_offset_of_Height_17(),
	SpotLight_t983333132::get_offset_of_LightColor_18(),
	SpotLight_t983333132::get_offset_of_LightDistances_19(),
	SpotLight_t983333132::get_offset_of_LightIntensities_20(),
	SpotLight_t983333132::get_offset_of_Smooth_21(),
	SpotLight_t983333132::get_offset_of_isRealTime_22(),
	SpotLight_t983333132::get_offset_of_isFirstPass_23(),
	SpotLight_t983333132_StaticFields::get_offset_of_worldToModel_24(),
	SpotLight_t983333132_StaticFields::get_offset_of_forward_25(),
	SpotLight_t983333132_StaticFields::get_offset_of_baseRadius_26(),
	SpotLight_t983333132_StaticFields::get_offset_of_height_27(),
	SpotLight_t983333132_StaticFields::get_offset_of_distances_28(),
	SpotLight_t983333132_StaticFields::get_offset_of_intensities_29(),
	SpotLight_t983333132_StaticFields::get_offset_of_smoothness_30(),
	SpotLight_t983333132_StaticFields::get_offset_of_color_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (VectorAsSlidersAttribute_t803388163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[4] = 
{
	VectorAsSlidersAttribute_t803388163::get_offset_of_label_0(),
	VectorAsSlidersAttribute_t803388163::get_offset_of_min_1(),
	VectorAsSlidersAttribute_t803388163::get_offset_of_max_2(),
	VectorAsSlidersAttribute_t803388163::get_offset_of_dimensions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (RangeWithStepAttribute_t713572535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[3] = 
{
	RangeWithStepAttribute_t713572535::get_offset_of_min_0(),
	RangeWithStepAttribute_t713572535::get_offset_of_max_1(),
	RangeWithStepAttribute_t713572535::get_offset_of_step_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (ShadowProjector_t3468829328), -1, sizeof(ShadowProjector_t3468829328_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2128[27] = 
{
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowProjectorCountProperty_7(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowMapMatrixProperty_8(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowModelToViewProperty_9(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowColorProperty_10(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowBlurProperty_11(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowCameraSettingsProperty_12(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowTextureProperty_13(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_DEBUG_SHADOW_TEXTURE_OBJECT_NAME_14(),
	ShadowProjector_t3468829328::get_offset_of_ShadowColor_15(),
	ShadowProjector_t3468829328::get_offset_of_ShadowMapShader_16(),
	ShadowProjector_t3468829328::get_offset_of_Bias_17(),
	ShadowProjector_t3468829328::get_offset_of_ShadowBlur_18(),
	ShadowProjector_t3468829328::get_offset_of_MemoryToUse_19(),
	ShadowProjector_t3468829328::get_offset_of_isRealTime_20(),
	ShadowProjector_t3468829328::get_offset_of_DebugShadowMappingTexture_21(),
	ShadowProjector_t3468829328::get_offset_of_isFirstPass_22(),
	ShadowProjector_t3468829328::get_offset_of_debugObject_23(),
	ShadowProjector_t3468829328::get_offset_of_debugMaterial_24(),
	ShadowProjector_t3468829328::get_offset_of_ShadowCamera_25(),
	ShadowProjector_t3468829328::get_offset_of_ShadowMapRequested_26(),
	ShadowProjector_t3468829328::get_offset_of_ShadowMapOffset_27(),
	ShadowProjector_t3468829328::get_offset_of_ShadowMapTexture_28(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_mapMatrix_29(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_modelToView_30(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_cameraSettings_31(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_blur_32(),
	ShadowProjector_t3468829328_StaticFields::get_offset_of_shadowColor_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (ActiveTCellScript_t1119790483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[2] = 
{
	ActiveTCellScript_t1119790483::get_offset_of_ActivCell_2(),
	ActiveTCellScript_t1119790483::get_offset_of_AntiGen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (AudioManager_t4222704959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[1] = 
{
	AudioManager_t4222704959::get_offset_of_soundEffects_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (GameManager_t2252321495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[16] = 
{
	GameManager_t2252321495::get_offset_of_Collectables_3(),
	GameManager_t2252321495::get_offset_of_goal_4(),
	GameManager_t2252321495::get_offset_of_numberOfCellstokill_5(),
	GameManager_t2252321495::get_offset_of_player_6(),
	GameManager_t2252321495::get_offset_of_framesText_7(),
	GameManager_t2252321495::get_offset_of_framesIterator_8(),
	GameManager_t2252321495::get_offset_of_TutorialFrames_9(),
	GameManager_t2252321495::get_offset_of_GameFrames_10(),
	GameManager_t2252321495::get_offset_of_TrackedItem_11(),
	GameManager_t2252321495::get_offset_of_FakeSphere_12(),
	GameManager_t2252321495::get_offset_of_Camera_13(),
	GameManager_t2252321495::get_offset_of_SlidesON_14(),
	GameManager_t2252321495::get_offset_of_MovementTutorialStarted_15(),
	GameManager_t2252321495::get_offset_of_MovementTutorialFinished_16(),
	GameManager_t2252321495::get_offset_of_GameON_17(),
	GameManager_t2252321495::get_offset_of_ResetPlayer_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (LoopManager_t594583541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[6] = 
{
	LoopManager_t594583541::get_offset_of_speed_2(),
	LoopManager_t594583541::get_offset_of_Sphere_3(),
	LoopManager_t594583541::get_offset_of_StartPoint_4(),
	LoopManager_t594583541::get_offset_of_SpawnTime_5(),
	LoopManager_t594583541::get_offset_of_Cooldown_6(),
	LoopManager_t594583541::get_offset_of_StartSpawn_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (PlanetScript_t2420377963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	PlanetScript_t2420377963::get_offset_of_gravity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (PlayerGravityBody_t3825941471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[2] = 
{
	PlayerGravityBody_t3825941471::get_offset_of_attractorPlanet_2(),
	PlayerGravityBody_t3825941471::get_offset_of_playerTransform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (PlayerMovementScript_t4209466341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[9] = 
{
	PlayerMovementScript_t4209466341::get_offset_of_moveSpeed_2(),
	PlayerMovementScript_t4209466341::get_offset_of_TrackedItem_3(),
	PlayerMovementScript_t4209466341::get_offset_of_moveDirection_4(),
	PlayerMovementScript_t4209466341::get_offset_of_Camera_5(),
	PlayerMovementScript_t4209466341::get_offset_of_Player_6(),
	PlayerMovementScript_t4209466341::get_offset_of_scorePivot_7(),
	PlayerMovementScript_t4209466341::get_offset_of_smooth_8(),
	PlayerMovementScript_t4209466341::get_offset_of_gm_9(),
	PlayerMovementScript_t4209466341::get_offset_of_prev_rot_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (AsymmetricFrustum_t2869578734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[12] = 
{
	AsymmetricFrustum_t2869578734::get_offset_of_left_2(),
	AsymmetricFrustum_t2869578734::get_offset_of_right_3(),
	AsymmetricFrustum_t2869578734::get_offset_of_top_4(),
	AsymmetricFrustum_t2869578734::get_offset_of_bottom_5(),
	AsymmetricFrustum_t2869578734::get_offset_of_near_6(),
	AsymmetricFrustum_t2869578734::get_offset_of_far_7(),
	AsymmetricFrustum_t2869578734::get_offset_of_fov_8(),
	AsymmetricFrustum_t2869578734::get_offset_of_calibratedCamNear_9(),
	AsymmetricFrustum_t2869578734::get_offset_of_slideX_10(),
	AsymmetricFrustum_t2869578734::get_offset_of_isLeftCam_11(),
	AsymmetricFrustum_t2869578734::get_offset_of_cam_12(),
	AsymmetricFrustum_t2869578734::get_offset_of_asymmetricFrust_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (ConnectToPreviousRemote_t1323906560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	ConnectToPreviousRemote_t1323906560::get_offset_of_lastRemoteID_2(),
	ConnectToPreviousRemote_t1323906560::get_offset_of_activelySearching_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[4] = 
{
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105::get_offset_of_U24this_0(),
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105::get_offset_of_U24current_1(),
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105::get_offset_of_U24disposing_2(),
	U3CAutoConnectLastRemoteU3Ec__Iterator0_t1086687105::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (U3CCheckRemotesU3Ec__Iterator1_t3747014210), -1, sizeof(U3CCheckRemotesU3Ec__Iterator1_t3747014210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2140[5] = 
{
	U3CCheckRemotesU3Ec__Iterator1_t3747014210::get_offset_of_U24this_0(),
	U3CCheckRemotesU3Ec__Iterator1_t3747014210::get_offset_of_U24current_1(),
	U3CCheckRemotesU3Ec__Iterator1_t3747014210::get_offset_of_U24disposing_2(),
	U3CCheckRemotesU3Ec__Iterator1_t3747014210::get_offset_of_U24PC_3(),
	U3CCheckRemotesU3Ec__Iterator1_t3747014210_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (MiraRemoteErrorCode_t1636063552)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[7] = 
{
	MiraRemoteErrorCode_t1636063552::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (MiraRemoteException_t3387773982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[1] = 
{
	MiraRemoteException_t3387773982::get_offset_of_U3CerrorCodeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (NativeBridge_t2060501782), -1, sizeof(NativeBridge_t2060501782_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2143[28] = 
{
	NativeBridge_t2060501782_StaticFields::get_offset_of_emptyActions_0(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_boolActions_1(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_intActions_2(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_floatActions_3(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_exceptionActions_4(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_remoteActions_5(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_remoteMotionActions_6(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_11(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_12(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_13(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_14(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_15(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_16(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_17(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_18(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_19(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_20(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_21(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_22(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_23(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_24(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_25(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_26(),
	NativeBridge_t2060501782_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (MREmptyCallback_t3969082922), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (MRBoolCallback_t2221477487), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (MRIntCallback_t2215762430), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (MRFloatCallback_t3699197057), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (MRErrorCallback_t1972775801), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (MRDiscoveredRemoteCallback_t3604605211), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (MRConnectedRemoteCallback_t3085918786), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (MRRemoteMotionCallback_t3634481787), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t2360024038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[1] = 
{
	U3CRemoteManagerBluetoothStateU3Ec__AnonStorey0_t2360024038::get_offset_of_bluetoothState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t3515152649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[1] = 
{
	U3CRemoteManagerAutomaticallyConnectsToPreviousConnectedRemoteU3Ec__AnonStorey1_t3515152649::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[3] = 
{
	U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533::get_offset_of_action_0(),
	U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533::get_offset_of_identifier_1(),
	U3CRemoteManagerStartRemoteDiscoveryU3Ec__AnonStorey2_t1765939533::get_offset_of_discoveryException_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[3] = 
{
	U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566::get_offset_of_remote_0(),
	U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566::get_offset_of_action_1(),
	U3CRemoteManagerConnectRemoteU3Ec__AnonStorey3_t1494848566::get_offset_of_identifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t3663694162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[2] = 
{
	U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t3663694162::get_offset_of_action_0(),
	U3CRemoteManagerDisconnectConnectedRemoteU3Ec__AnonStorey4_t3663694162::get_offset_of_identifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (U3CRemoteRefreshU3Ec__AnonStorey5_t751324336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[3] = 
{
	U3CRemoteRefreshU3Ec__AnonStorey5_t751324336::get_offset_of_remote_0(),
	U3CRemoteRefreshU3Ec__AnonStorey5_t751324336::get_offset_of_action_1(),
	U3CRemoteRefreshU3Ec__AnonStorey5_t751324336::get_offset_of_identifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (Remote_t660843562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[11] = 
{
	Remote_t660843562::get_offset_of_OnRefresh_5(),
	Remote_t660843562::get_offset_of_identifier_6(),
	Remote_t660843562::get_offset_of_U3CnameU3Ek__BackingField_7(),
	Remote_t660843562::get_offset_of_U3CproductNameU3Ek__BackingField_8(),
	Remote_t660843562::get_offset_of_U3CserialNumberU3Ek__BackingField_9(),
	Remote_t660843562::get_offset_of_U3ChardwareIdentifierU3Ek__BackingField_10(),
	Remote_t660843562::get_offset_of_U3CfirmwareVersionU3Ek__BackingField_11(),
	Remote_t660843562::get_offset_of_U3CbatteryPercentageU3Ek__BackingField_12(),
	Remote_t660843562::get_offset_of_U3CrssiU3Ek__BackingField_13(),
	Remote_t660843562::get_offset_of_U3CisConnectedU3Ek__BackingField_14(),
	Remote_t660843562::get_offset_of_U3CisPreferredU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (RemoteRefreshedEventHandler_t1036293937), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (RemoteAxisInput_t2770128439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[2] = 
{
	RemoteAxisInput_t2770128439::get_offset_of_OnValueChanged_0(),
	RemoteAxisInput_t2770128439::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (RemoteAxisInputEventHandler_t3796077797), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (RemoteBase_t3616301967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[5] = 
{
	RemoteBase_t3616301967::get_offset_of_U3CmenuButtonU3Ek__BackingField_0(),
	RemoteBase_t3616301967::get_offset_of_U3ChomeButtonU3Ek__BackingField_1(),
	RemoteBase_t3616301967::get_offset_of_U3CtriggerU3Ek__BackingField_2(),
	RemoteBase_t3616301967::get_offset_of_U3CtouchPadU3Ek__BackingField_3(),
	RemoteBase_t3616301967::get_offset_of_U3CmotionU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (RemoteButtonInput_t144791130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[11] = 
{
	RemoteButtonInput_t144791130::get_offset_of_OnPressChanged_0(),
	RemoteButtonInput_t144791130::get_offset_of_OnHeldState_1(),
	RemoteButtonInput_t144791130::get_offset_of_OnPresseddState_2(),
	RemoteButtonInput_t144791130::get_offset_of_OnReleasedState_3(),
	RemoteButtonInput_t144791130::get_offset_of_m_PrevFrameCount_4(),
	RemoteButtonInput_t144791130::get_offset_of_m_PrevState_5(),
	RemoteButtonInput_t144791130::get_offset_of_m_State_6(),
	RemoteButtonInput_t144791130::get_offset_of__isPressed_7(),
	RemoteButtonInput_t144791130::get_offset_of__onPressed_8(),
	RemoteButtonInput_t144791130::get_offset_of__onReleased_9(),
	RemoteButtonInput_t144791130::get_offset_of__onHeld_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (RemoteButtonInputEventHandler_t1063480517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (BluetoothState_t1243475723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2165[7] = 
{
	BluetoothState_t1243475723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (RemoteManager_t2208998541), -1, sizeof(RemoteManager_t2208998541_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2166[7] = 
{
	RemoteManager_t2208998541::get_offset_of_OnRemoteConnected_2(),
	RemoteManager_t2208998541::get_offset_of_OnRemoteDisconnected_3(),
	RemoteManager_t2208998541_StaticFields::get_offset_of_Instance_4(),
	RemoteManager_t2208998541::get_offset_of_U3CisStartedU3Ek__BackingField_5(),
	RemoteManager_t2208998541::get_offset_of_U3CisDiscoveringRemotesU3Ek__BackingField_6(),
	RemoteManager_t2208998541::get_offset_of__connectedRemote_7(),
	RemoteManager_t2208998541::get_offset_of__discoveredRemotes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (RemoteConnectedEventHandler_t3456249665), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (RemoteDisconnectedEventHandler_t730656759), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1165427101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[2] = 
{
	U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1165427101::get_offset_of_action_0(),
	U3CStartRemoteDiscoveryU3Ec__AnonStorey0_t1165427101::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (U3CConnectRemoteU3Ec__AnonStorey1_t3364220388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[3] = 
{
	U3CConnectRemoteU3Ec__AnonStorey1_t3364220388::get_offset_of_remote_0(),
	U3CConnectRemoteU3Ec__AnonStorey1_t3364220388::get_offset_of_action_1(),
	U3CConnectRemoteU3Ec__AnonStorey1_t3364220388::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t3821765992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[2] = 
{
	U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t3821765992::get_offset_of_action_0(),
	U3CDisconnectConnectedRemoteU3Ec__AnonStorey2_t3821765992::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (RemoteMotionInput_t3548926620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[4] = 
{
	RemoteMotionInput_t3548926620::get_offset_of_OnValueChanged_2(),
	RemoteMotionInput_t3548926620::get_offset_of_U3CorientationU3Ek__BackingField_3(),
	RemoteMotionInput_t3548926620::get_offset_of_U3CaccelerationU3Ek__BackingField_4(),
	RemoteMotionInput_t3548926620::get_offset_of_U3CrotationRateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (RemoteMotionInputEventHandler_t1458362501), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (RemoteMotionSensorInput_t841531810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[4] = 
{
	RemoteMotionSensorInput_t841531810::get_offset_of_OnValueChanged_0(),
	RemoteMotionSensorInput_t841531810::get_offset_of_U3CxU3Ek__BackingField_1(),
	RemoteMotionSensorInput_t841531810::get_offset_of_U3CyU3Ek__BackingField_2(),
	RemoteMotionSensorInput_t841531810::get_offset_of_U3CzU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (RemoteMotionSensorInputEventHandler_t2876817765), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (RemoteOrientationInput_t3303200544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	RemoteOrientationInput_t3303200544::get_offset_of_OnValueChanged_0(),
	RemoteOrientationInput_t3303200544::get_offset_of_U3CpitchU3Ek__BackingField_1(),
	RemoteOrientationInput_t3303200544::get_offset_of_U3CyawU3Ek__BackingField_2(),
	RemoteOrientationInput_t3303200544::get_offset_of_U3CrollU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (RemoteOrientationInputEventHandler_t4128243105), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (RemotesController_t2607477933), -1, sizeof(RemotesController_t2607477933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2178[4] = 
{
	RemotesController_t2607477933::get_offset_of_remoteLabels_2(),
	RemotesController_t2607477933::get_offset_of_defaultColor_3(),
	RemotesController_t2607477933::get_offset_of_highlightColor_4(),
	RemotesController_t2607477933_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (RemoteTouchInput_t3826108381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	RemoteTouchInput_t3826108381::get_offset_of_OnActiveChanged_0(),
	RemoteTouchInput_t3826108381::get_offset_of__isActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (RemoteTouchInputEventHandler_t1375955781), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (RemoteTouchPadInput_t1081319266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[9] = 
{
	RemoteTouchPadInput_t1081319266::get_offset_of_OnValueChanged_2(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CbuttonU3Ek__BackingField_3(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CtouchActiveU3Ek__BackingField_4(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CxAxisU3Ek__BackingField_5(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CyAxisU3Ek__BackingField_6(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CupU3Ek__BackingField_7(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CdownU3Ek__BackingField_8(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CleftU3Ek__BackingField_9(),
	RemoteTouchPadInput_t1081319266::get_offset_of_U3CrightU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (RemoteTouchPadInputEventHandler_t2832767717), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (VirtualRemote_t4041918011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (CamGyro_t694259106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[3] = 
{
	CamGyro_t694259106::get_offset_of_isSpectator_2(),
	CamGyro_t694259106::get_offset_of_camGyroOffset_3(),
	CamGyro_t694259106::get_offset_of_camGyroActive_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (DebugMiraController_t1181799644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[7] = 
{
	DebugMiraController_t1181799644::get_offset_of_debugOutputText_2(),
	DebugMiraController_t1181799644::get_offset_of_output_3(),
	DebugMiraController_t1181799644::get_offset_of_didPointerEnter_4(),
	DebugMiraController_t1181799644::get_offset_of_didPointerExit_5(),
	DebugMiraController_t1181799644::get_offset_of_didPointerClick_6(),
	DebugMiraController_t1181799644::get_offset_of_didPointerDown_7(),
	DebugMiraController_t1181799644::get_offset_of_didPointerUp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (DeviceOrientationChange_t2098609656), -1, sizeof(DeviceOrientationChange_t2098609656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2186[4] = 
{
	DeviceOrientationChange_t2098609656_StaticFields::get_offset_of_OnResolutionChange_2(),
	DeviceOrientationChange_t2098609656_StaticFields::get_offset_of_CheckDelay_3(),
	DeviceOrientationChange_t2098609656_StaticFields::get_offset_of_resolution_4(),
	DeviceOrientationChange_t2098609656_StaticFields::get_offset_of_isAlive_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (U3CCheckForChangeU3Ec__Iterator0_t136193089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[3] = 
{
	U3CCheckForChangeU3Ec__Iterator0_t136193089::get_offset_of_U24current_0(),
	U3CCheckForChangeU3Ec__Iterator0_t136193089::get_offset_of_U24disposing_1(),
	U3CCheckForChangeU3Ec__Iterator0_t136193089::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (DistortionCamera_t4252183916), -1, sizeof(DistortionCamera_t4252183916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[4] = 
{
	DistortionCamera_t4252183916::get_offset_of_stereoFov_2(),
	DistortionCamera_t4252183916_StaticFields::get_offset_of_instance_3(),
	DistortionCamera_t4252183916_StaticFields::get_offset_of_m_dcLeft_4(),
	DistortionCamera_t4252183916_StaticFields::get_offset_of_m_dcRight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (MiraBasePointer_t885132991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	MiraBasePointer_t885132991::get_offset_of_U3CmaxDistanceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (MiraBaseRaycaster_t1612955938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[2] = 
{
	MiraBaseRaycaster_t1612955938::get_offset_of_raycastStyle_2(),
	MiraBaseRaycaster_t1612955938::get_offset_of_lastray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (RaycastStyle_t2248787495)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2191[3] = 
{
	RaycastStyle_t2248787495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (MiraBTRemoteInput_t988911721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[4] = 
{
	MiraBTRemoteInput_t988911721::get_offset_of_controller_0(),
	MiraBTRemoteInput_t988911721::get_offset_of__virtualRemote_1(),
	MiraBTRemoteInput_t988911721::get_offset_of__connectedRemote_2(),
	MiraBTRemoteInput_t988911721::get_offset_of_connectedRemote_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (MiraController_t1058876281), -1, sizeof(MiraController_t1058876281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2193[23] = 
{
	MiraController_t1058876281_StaticFields::get_offset_of__instance_2(),
	MiraController_t1058876281::get_offset_of_handedness_3(),
	MiraController_t1058876281::get_offset_of_controllerType_4(),
	MiraController_t1058876281::get_offset_of_WhatButtonIsClick_5(),
	MiraController_t1058876281_StaticFields::get_offset_of__userInput_6(),
	MiraController_t1058876281::get_offset_of_cameraRig_7(),
	MiraController_t1058876281::get_offset_of_rotationalTracking_8(),
	MiraController_t1058876281::get_offset_of_trackPosition_9(),
	MiraController_t1058876281::get_offset_of_shouldFollowHead_10(),
	MiraController_t1058876281::get_offset_of_sceneScaleMult_11(),
	MiraController_t1058876281::get_offset_of_armOffsetX_12(),
	MiraController_t1058876281::get_offset_of_armOffsetY_13(),
	MiraController_t1058876281::get_offset_of_armOffsetZ_14(),
	MiraController_t1058876281::get_offset_of_baseController_15(),
	MiraController_t1058876281::get_offset_of_offsetPos_16(),
	MiraController_t1058876281::get_offset_of_isRotationalMode_17(),
	MiraController_t1058876281::get_offset_of_angleTiltX_18(),
	MiraController_t1058876281::get_offset_of_angleTiltY_19(),
	MiraController_t1058876281::get_offset_of_returnToHome_20(),
	MiraController_t1058876281::get_offset_of_radialTimer_21(),
	MiraController_t1058876281_StaticFields::get_offset_of_U3CevaluateClickButtonU3Ek__BackingField_22(),
	MiraController_t1058876281_StaticFields::get_offset_of_U3CevaluateClickButtonPressedU3Ek__BackingField_23(),
	MiraController_t1058876281_StaticFields::get_offset_of_U3CevaluateClickButtonReleasedU3Ek__BackingField_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (ControllerType_t693226312)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2194[3] = 
{
	ControllerType_t693226312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (Handedness_t261377275)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2195[3] = 
{
	Handedness_t261377275::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ClickChoices_t763649284)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2196[4] = 
{
	ClickChoices_t763649284::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (U3CStartControllerU3Ec__Iterator0_t3030372693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[4] = 
{
	U3CStartControllerU3Ec__Iterator0_t3030372693::get_offset_of_U24this_0(),
	U3CStartControllerU3Ec__Iterator0_t3030372693::get_offset_of_U24current_1(),
	U3CStartControllerU3Ec__Iterator0_t3030372693::get_offset_of_U24disposing_2(),
	U3CStartControllerU3Ec__Iterator0_t3030372693::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (U3CDelayedRecenterU3Ec__Iterator1_t3807338448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[4] = 
{
	U3CDelayedRecenterU3Ec__Iterator1_t3807338448::get_offset_of_U24this_0(),
	U3CDelayedRecenterU3Ec__Iterator1_t3807338448::get_offset_of_U24current_1(),
	U3CDelayedRecenterU3Ec__Iterator1_t3807338448::get_offset_of_U24disposing_2(),
	U3CDelayedRecenterU3Ec__Iterator1_t3807338448::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (U3CReturnToHomeU3Ec__Iterator2_t3855721357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[7] = 
{
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U3CtimerU3E__0_0(),
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U3CanimStartedU3E__0_1(),
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U3CanimProgressU3E__0_2(),
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U24this_3(),
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U24current_4(),
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U24disposing_5(),
	U3CReturnToHomeU3Ec__Iterator2_t3855721357::get_offset_of_U24PC_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
