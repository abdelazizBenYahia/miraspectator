﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t2871480376;
// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct UnityEvent_3_t2425689862;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Int32,System.Int32>
struct UnityEvent_3_t1947670748;
// ToggleModes
struct ToggleModes_t4125091540;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// Mira.MiraArController
struct MiraArController_t555016598;
// System.String
struct String_t;
// TrackerLerpDriver
struct TrackerLerpDriver_t1630291735;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.EventSystems.MiraInputModule
struct MiraInputModule_t2476427099;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t3572864619;
// UnityEngine.EventSystems.PointerInputModule
struct PointerInputModule_t1441575871;
// MiraBasePointer
struct MiraBasePointer_t885132991;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t1295781545;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t3317921847;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1186599945;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t477470301;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t1109076156;
// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct ButtonState_t2688375492;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t3709210170;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t2331828160;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t2276060003;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t2426197568;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t1847959737;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t344915111;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t2888287612;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t887251860;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t4141241546;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t3253137806;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.XR.iOS.MiraLivePreviewEditor
struct MiraLivePreviewEditor_t3425584316;
// UnityEngine.XR.iOS.MiraLivePreviewPlayer
struct MiraLivePreviewPlayer_t3516305084;
// RemoteManager/RemoteConnectedEventHandler
struct RemoteConnectedEventHandler_t3456249665;
// RemoteManager
struct RemoteManager_t2208998541;
// RemoteManager/RemoteDisconnectedEventHandler
struct RemoteDisconnectedEventHandler_t730656759;
// MiraBTRemoteInput
struct MiraBTRemoteInput_t988911721;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3517219175;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3438463199;
// UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityAction_1_t1667869373;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct MessageEventArgs_t301283622;
// Utils.serializableFromEditorMessage
struct serializableFromEditorMessage_t2894567809;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// MiraLivePreviewWikiConfig
struct MiraLivePreviewWikiConfig_t2812595783;
// Remote
struct Remote_t660843562;
// System.EventArgs
struct EventArgs_t3289624707;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// MiraARVideo
struct MiraARVideo_t1477805071;
// Wikitude.ImageTrackable[]
struct ImageTrackableU5BU5D_t322116539;
// UnityEngine.Events.UnityAction`1<Wikitude.ImageTarget>
struct UnityAction_1_t3381592573;
// UnityEngine.Events.UnityEvent`1<Wikitude.ImageTarget>
struct UnityEvent_1_t2053356837;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2727799310;
// Wikitude.ImageTrackable
struct ImageTrackable_t3105654606;
// UnityEngine.Gyroscope
struct Gyroscope_t1705362817;
// Utils.serializableGyroscope
struct serializableGyroscope_t1293559986;
// Utils.serializableTransform
struct serializableTransform_t2202979937;
// Wikitude.ImageTarget
struct ImageTarget_t2015006822;
// Utils.serializableFloat
struct serializableFloat_t3811907803;
// Utils.serializableBTRemote
struct serializableBTRemote_t622411689;
// Utils.serializableBTRemoteButtons
struct serializableBTRemoteButtons_t2327889448;
// Utils.serializableBTRemoteTouchPad
struct serializableBTRemoteTouchPad_t2020069673;
// UnityEngine.XR.iOS.MiraLivePreviewVideo
struct MiraLivePreviewVideo_t2362804972;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t1866979105;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.IO.Stream
struct Stream_t3255436806;
// RemoteBase
struct RemoteBase_t3616301967;
// RemoteMotionInput
struct RemoteMotionInput_t3548926620;
// RemoteOrientationInput
struct RemoteOrientationInput_t3303200544;
// RemoteMotionSensorInput
struct RemoteMotionSensorInput_t841531810;
// RemoteButtonInput
struct RemoteButtonInput_t144791130;
// RemoteTouchPadInput
struct RemoteTouchPadInput_t1081319266;
// RemoteAxisInput
struct RemoteAxisInput_t2770128439;
// RemoteTouchInput
struct RemoteTouchInput_t3826108381;
// Utils.serializablePointCloud
struct serializablePointCloud_t1992421910;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// Utils.SerializableVector4
struct SerializableVector4_t4294681242;
// VirtualRemote
struct VirtualRemote_t4041918011;
// RemoteOrientationInput/RemoteOrientationInputEventHandler
struct RemoteOrientationInputEventHandler_t4128243105;
// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler
struct RemoteMotionSensorInputEventHandler_t2876817765;
// RemoteButtonInput/RemoteButtonInputEventHandler
struct RemoteButtonInputEventHandler_t1063480517;
// RemoteTouchInput/RemoteTouchInputEventHandler
struct RemoteTouchInputEventHandler_t1375955781;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// RemoteAxisInput/RemoteAxisInputEventHandler
struct RemoteAxisInputEventHandler_t3796077797;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t603556505;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_t2057496624;
// System.Char[]
struct CharU5BU5D_t1328083999;
// RemoteMotionInput/RemoteMotionInputEventHandler
struct RemoteMotionInputEventHandler_t1458362501;
// System.Void
struct Void_t1841601450;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// RemoteTouchPadInput/RemoteTouchPadInputEventHandler
struct RemoteTouchPadInputEventHandler_t2832767717;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Remote/RemoteRefreshedEventHandler
struct RemoteRefreshedEventHandler_t1036293937;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t2336171397;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.EventSystems.IMoveHandler
struct IMoveHandler_t2611925506;
// UnityEngine.EventSystems.ICancelHandler
struct ICancelHandler_t1980319651;
// UnityEngine.EventSystems.ISubmitHandler
struct ISubmitHandler_t525803901;
// UnityEngine.IPlayerEditorConnectionNative
struct IPlayerEditorConnectionNative_t1175081258;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t2252784345;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t3985864818;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1912587528;
// UnityEngine.EventSystems.IPointerClickHandler
struct IPointerClickHandler_t96169666;
// UnityEngine.EventSystems.IDropHandler
struct IDropHandler_t2390101210;
// UnityEngine.EventSystems.IPointerUpHandler
struct IPointerUpHandler_t1847764461;
// UnityEngine.EventSystems.IInitializePotentialDragHandler
struct IInitializePotentialDragHandler_t3350809087;
// UnityEngine.EventSystems.IPointerDownHandler
struct IPointerDownHandler_t3929046918;
// UnityEngine.EventSystems.IUpdateSelectedHandler
struct IUpdateSelectedHandler_t3778909353;
// UnityEngine.EventSystems.IPointerExitHandler
struct IPointerExitHandler_t461019860;
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t1349123600;
// UnityEngine.EventSystems.IScrollHandler
struct IScrollHandler_t3834677510;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// Wikitude.TrackerBehaviour
struct TrackerBehaviour_t3845512381;
// System.Collections.Generic.Dictionary`2<Wikitude.RecognizedTarget,UnityEngine.GameObject>
struct Dictionary_2_t3102997329;
// MarkerRotationWatcher
struct MarkerRotationWatcher_t3969903492;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2517845841;
// Mira.MiraViewer
struct MiraViewer_t1267122109;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// MarkerRotationWatcher/RotateAction
struct RotateAction_t2368064732;
// System.Collections.Generic.List`1<Remote>
struct List_1_t29964694;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t664902677;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2835956395;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1282925227;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t1524870173;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t621514313;
// System.String[]
struct StringU5BU5D_t1642385972;
// Wikitude.ImageTrackable/OnImageRecognizedEvent
struct OnImageRecognizedEvent_t2106945187;
// Wikitude.ImageTrackable/OnImageLostEvent
struct OnImageLostEvent_t2609021075;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t607610358;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t1348919171;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t1795346708;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t3687964427;

extern const RuntimeMethod* UnityEvent_3__ctor_m4061335797_RuntimeMethod_var;
extern const uint32_t WordSelectionEvent__ctor_m899574239_MetadataUsageId;
extern RuntimeClass* MiraArController_t555016598_il2cpp_TypeInfo_var;
extern RuntimeClass* DemoSceneManager_t779426248_il2cpp_TypeInfo_var;
extern const uint32_t ToggleModes_OnEnable_m3823369637_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2694533563;
extern const uint32_t ToggleModes_Update_m1198530850_MetadataUsageId;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t TrackerLerpDriver_Update_m2532790711_MetadataUsageId;
extern RuntimeClass* MouseState_t3572864619_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern Il2CppCodeGenString* _stringLiteral2014250346;
extern Il2CppCodeGenString* _stringLiteral2358390244;
extern const uint32_t MiraInputModule__ctor_m4250116751_MetadataUsageId;
extern RuntimeClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t MiraInputModule_UpdateModule_m1024007352_MetadataUsageId;
extern const uint32_t MiraInputModule_IsModuleSupported_m89617561_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t MiraInputModule_ShouldActivateModule_m4034676109_MetadataUsageId;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MiraInputModule_ActivateModule_m371367340_MetadataUsageId;
extern RuntimeClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisISubmitHandler_t525803901_m896016395_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisICancelHandler_t1980319651_m897890085_RuntimeMethod_var;
extern const uint32_t MiraInputModule_SendSubmitEventToSelectedObject_m2722784986_MetadataUsageId;
extern const uint32_t MiraInputModule_GetRawMoveVector_m1549472706_MetadataUsageId;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIMoveHandler_t2611925506_m110288646_RuntimeMethod_var;
extern const uint32_t MiraInputModule_SendMoveEventToSelectedObject_m1103889925_MetadataUsageId;
extern const RuntimeMethod* ExecuteEvents_GetEventHandler_TisIScrollHandler_t3834677510_m1788515243_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692_RuntimeMethod_var;
extern const uint32_t MiraInputModule_ProcessMouseEvent_m342372504_MetadataUsageId;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIUpdateSelectedHandler_t3778909353_m3262129831_RuntimeMethod_var;
extern const uint32_t MiraInputModule_SendUpdateEventToSelectedObject_m3857972945_MetadataUsageId;
extern const RuntimeMethod* ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_GetEventHandler_TisIDragHandler_t2583993319_m3720979028_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t3350809087_m2193269739_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIPointerUpHandler_t1847764461_m885031125_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIPointerClickHandler_t96169666_m907812816_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIEndDragHandler_t1349123600_m4238380530_RuntimeMethod_var;
extern const uint32_t MiraInputModule_ProcessMousePress_m1130092012_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m939767277_RuntimeMethod_var;
extern const RuntimeMethod* ExecuteEvents_Execute_TisIPointerExitHandler_t461019860_m4037442468_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2764296230_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m4030601119_RuntimeMethod_var;
extern const uint32_t MiraInputModule_HandlePointerExitAndEnter_m3470520529_MetadataUsageId;
extern RuntimeClass* MiraGraphicRaycast_t590279048_il2cpp_TypeInfo_var;
extern RuntimeClass* MiraPhysicsRaycast_t1727560409_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Clear_m392100656_RuntimeMethod_var;
extern const uint32_t MiraInputModule_GetRayPointerData_m766374278_MetadataUsageId;
extern const uint32_t MiraInputModule_ProcessMouseEvent_m4282372644_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1193156373;
extern const uint32_t MiraConnectionMessageIds_get_fromEditorMiraSessionMsgId_m2831389603_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2837019704;
extern const uint32_t MiraConnectionMessageIds_get_gyroMsgId_m3152547904_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3826344735;
extern const uint32_t MiraConnectionMessageIds_get_wikiCamMsgId_m3438711336_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3052444739;
extern const uint32_t MiraConnectionMessageIds_get_trackingFoundMsgId_m3240602256_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2514286891;
extern const uint32_t MiraConnectionMessageIds_get_trackingLostMsgId_m196996400_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral6139838;
extern const uint32_t MiraConnectionMessageIds_get_BTRemoteMsgId_m738691607_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1928898930;
extern const uint32_t MiraConnectionMessageIds_get_BTRemoteButtonsMsgId_m1963856294_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3753267782;
extern const uint32_t MiraConnectionMessageIds_get_BTRemoteTouchPadMsgId_m1308740695_MetadataUsageId;
extern RuntimeClass* RemoteManager_t2208998541_il2cpp_TypeInfo_var;
extern RuntimeClass* RemoteConnectedEventHandler_t3456249665_il2cpp_TypeInfo_var;
extern RuntimeClass* RemoteDisconnectedEventHandler_t730656759_il2cpp_TypeInfo_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_RemoteConnected_m518057818_RuntimeMethod_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_RemoteDisconnected_m2597138294_RuntimeMethod_var;
extern const uint32_t MiraLivePreviewPlayer_OnEnable_m3827577147_MetadataUsageId;
extern const uint32_t MiraLivePreviewPlayer_OnDisable_m1938345270_MetadataUsageId;
extern RuntimeClass* MiraBTRemoteInput_t988911721_il2cpp_TypeInfo_var;
extern RuntimeClass* MiraLivePreviewPlayer_t3516305084_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityAction_1_t3438463199_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityAction_1_t1667869373_il2cpp_TypeInfo_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_EditorConnected_m1048406888_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m490892142_RuntimeMethod_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_EditorDisconnected_m3438779146_RuntimeMethod_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_HandleEditorMessage_m4127892255_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m2955260543_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2649185284;
extern const uint32_t MiraLivePreviewPlayer_Start_m1179035_MetadataUsageId;
extern RuntimeClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3547176873;
extern const uint32_t MiraLivePreviewPlayer_OnGUI_m4271455313_MetadataUsageId;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ObjectSerializationExtension_Deserialize_TisserializableFromEditorMessage_t2894567809_m1238441737_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisMiraLivePreviewWikiConfig_t2812595783_m2483761890_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1887264353;
extern const uint32_t MiraLivePreviewPlayer_HandleEditorMessage_m4127892255_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var;
extern const uint32_t MiraLivePreviewPlayer_InitializeLivePreview_m225778839_MetadataUsageId;
extern const uint32_t MiraLivePreviewPlayer_RemoteConnected_m518057818_MetadataUsageId;
extern RuntimeClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisMiraARVideo_t1477805071_m2351498360_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2663777903;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MiraLivePreviewPlayer_InitializeTextures_m3292283093_MetadataUsageId;
extern RuntimeClass* UnityAction_1_t3381592573_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectsOfType_TisImageTrackable_t3105654606_m1655889936_RuntimeMethod_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_OnTrackingFound_m3486069731_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m1430463559_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m394150843_RuntimeMethod_var;
extern const RuntimeMethod* MiraLivePreviewPlayer_OnTrackingLost_m2787869957_RuntimeMethod_var;
extern const uint32_t MiraLivePreviewPlayer_InitializeTrackers_m2398981726_MetadataUsageId;
extern const uint32_t MiraLivePreviewPlayer_Update_m1830306472_MetadataUsageId;
extern const uint32_t MiraLivePreviewPlayer_OnTrackingLost_m2787869957_MetadataUsageId;
extern const uint32_t MiraLivePreviewPlayer_UpdateBTRemote_m820487388_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3269606001;
extern const uint32_t MiraLivePreviewPlayer_EditorConnected_m1048406888_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2474827406;
extern const uint32_t MiraLivePreviewPlayer_EditorDisconnected_m3438779146_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1086818976;
extern const uint32_t MiraLivePreviewPlayer_DisconnectFromEditor_m616998130_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3513490258;
extern const uint32_t MiraSubMessageIds_get_editorInitMiraRemote_m1234016531_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3925036185;
extern const uint32_t MiraSubMessageIds_get_editorDisconnect_m2481842044_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1825940431;
extern const uint32_t MiraSubMessageIds_get_screenCaptureJPEGMsgID_m3453747459_MetadataUsageId;
extern RuntimeClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern RuntimeClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t ObjectSerializationExtension_SerializeToByteArray_m2132233038_MetadataUsageId;
extern RuntimeClass* serializableBTRemote_t622411689_il2cpp_TypeInfo_var;
extern const uint32_t serializableBTRemote_op_Implicit_m3134586455_MetadataUsageId;
extern RuntimeClass* serializableBTRemoteButtons_t2327889448_il2cpp_TypeInfo_var;
extern const uint32_t serializableBTRemoteButtons_op_Implicit_m892522079_MetadataUsageId;
extern RuntimeClass* serializableBTRemoteTouchPad_t2020069673_il2cpp_TypeInfo_var;
extern const uint32_t serializableBTRemoteTouchPad_op_Implicit_m1392338071_MetadataUsageId;
extern RuntimeClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1367450909;
extern const uint32_t serializableFloat_ToString_m4071320364_MetadataUsageId;
extern RuntimeClass* serializableFloat_t3811907803_il2cpp_TypeInfo_var;
extern const uint32_t serializableFloat_op_Implicit_m3403528258_MetadataUsageId;
extern RuntimeClass* serializableGyroscope_t1293559986_il2cpp_TypeInfo_var;
extern const uint32_t serializableGyroscope_op_Implicit_m1484759557_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern RuntimeClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern RuntimeClass* serializablePointCloud_t1992421910_il2cpp_TypeInfo_var;
extern const uint32_t serializablePointCloud_op_Implicit_m2562631572_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const uint32_t serializablePointCloud_op_Implicit_m1111497770_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1742103562;
extern const uint32_t SerializableQuaternion_ToString_m1532411480_MetadataUsageId;
extern RuntimeClass* serializableTransform_t2202979937_il2cpp_TypeInfo_var;
extern const uint32_t serializableTransform_op_Implicit_m504541074_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1192795215;
extern const uint32_t SerializableVector2_ToString_m1702069295_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral30107939;
extern const uint32_t SerializableVector3_ToString_m2915338350_MetadataUsageId;
extern const uint32_t SerializableVector4_ToString_m137239981_MetadataUsageId;
extern RuntimeClass* SerializableVector4_t4294681242_il2cpp_TypeInfo_var;
extern const uint32_t SerializableVector4_op_Implicit_m2818398069_MetadataUsageId;
extern RuntimeClass* RemoteButtonInput_t144791130_il2cpp_TypeInfo_var;
extern RuntimeClass* RemoteTouchPadInput_t1081319266_il2cpp_TypeInfo_var;
extern RuntimeClass* RemoteMotionInput_t3548926620_il2cpp_TypeInfo_var;
extern const uint32_t VirtualRemote__ctor_m2680452056_MetadataUsageId;

struct ByteU5BU5D_t3397334013;
struct ObjectU5BU5D_t3614634134;
struct ImageTrackableU5BU5D_t322116539;
struct Vector3U5BU5D_t1172311765;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef REMOTEBASE_T3616301967_H
#define REMOTEBASE_T3616301967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteBase
struct  RemoteBase_t3616301967  : public RuntimeObject
{
public:
	// RemoteButtonInput RemoteBase::<menuButton>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CmenuButtonU3Ek__BackingField_0;
	// RemoteButtonInput RemoteBase::<homeButton>k__BackingField
	RemoteButtonInput_t144791130 * ___U3ChomeButtonU3Ek__BackingField_1;
	// RemoteButtonInput RemoteBase::<trigger>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CtriggerU3Ek__BackingField_2;
	// RemoteTouchPadInput RemoteBase::<touchPad>k__BackingField
	RemoteTouchPadInput_t1081319266 * ___U3CtouchPadU3Ek__BackingField_3;
	// RemoteMotionInput RemoteBase::<motion>k__BackingField
	RemoteMotionInput_t3548926620 * ___U3CmotionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CmenuButtonU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CmenuButtonU3Ek__BackingField_0)); }
	inline RemoteButtonInput_t144791130 * get_U3CmenuButtonU3Ek__BackingField_0() const { return ___U3CmenuButtonU3Ek__BackingField_0; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CmenuButtonU3Ek__BackingField_0() { return &___U3CmenuButtonU3Ek__BackingField_0; }
	inline void set_U3CmenuButtonU3Ek__BackingField_0(RemoteButtonInput_t144791130 * value)
	{
		___U3CmenuButtonU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmenuButtonU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ChomeButtonU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3ChomeButtonU3Ek__BackingField_1)); }
	inline RemoteButtonInput_t144791130 * get_U3ChomeButtonU3Ek__BackingField_1() const { return ___U3ChomeButtonU3Ek__BackingField_1; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3ChomeButtonU3Ek__BackingField_1() { return &___U3ChomeButtonU3Ek__BackingField_1; }
	inline void set_U3ChomeButtonU3Ek__BackingField_1(RemoteButtonInput_t144791130 * value)
	{
		___U3ChomeButtonU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChomeButtonU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtriggerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CtriggerU3Ek__BackingField_2)); }
	inline RemoteButtonInput_t144791130 * get_U3CtriggerU3Ek__BackingField_2() const { return ___U3CtriggerU3Ek__BackingField_2; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CtriggerU3Ek__BackingField_2() { return &___U3CtriggerU3Ek__BackingField_2; }
	inline void set_U3CtriggerU3Ek__BackingField_2(RemoteButtonInput_t144791130 * value)
	{
		___U3CtriggerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtriggerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtouchPadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CtouchPadU3Ek__BackingField_3)); }
	inline RemoteTouchPadInput_t1081319266 * get_U3CtouchPadU3Ek__BackingField_3() const { return ___U3CtouchPadU3Ek__BackingField_3; }
	inline RemoteTouchPadInput_t1081319266 ** get_address_of_U3CtouchPadU3Ek__BackingField_3() { return &___U3CtouchPadU3Ek__BackingField_3; }
	inline void set_U3CtouchPadU3Ek__BackingField_3(RemoteTouchPadInput_t1081319266 * value)
	{
		___U3CtouchPadU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchPadU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CmotionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteBase_t3616301967, ___U3CmotionU3Ek__BackingField_4)); }
	inline RemoteMotionInput_t3548926620 * get_U3CmotionU3Ek__BackingField_4() const { return ___U3CmotionU3Ek__BackingField_4; }
	inline RemoteMotionInput_t3548926620 ** get_address_of_U3CmotionU3Ek__BackingField_4() { return &___U3CmotionU3Ek__BackingField_4; }
	inline void set_U3CmotionU3Ek__BackingField_4(RemoteMotionInput_t3548926620 * value)
	{
		___U3CmotionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmotionU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBASE_T3616301967_H
#ifndef REMOTEORIENTATIONINPUT_T3303200544_H
#define REMOTEORIENTATIONINPUT_T3303200544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteOrientationInput
struct  RemoteOrientationInput_t3303200544  : public RuntimeObject
{
public:
	// RemoteOrientationInput/RemoteOrientationInputEventHandler RemoteOrientationInput::OnValueChanged
	RemoteOrientationInputEventHandler_t4128243105 * ___OnValueChanged_0;
	// System.Single RemoteOrientationInput::<pitch>k__BackingField
	float ___U3CpitchU3Ek__BackingField_1;
	// System.Single RemoteOrientationInput::<yaw>k__BackingField
	float ___U3CyawU3Ek__BackingField_2;
	// System.Single RemoteOrientationInput::<roll>k__BackingField
	float ___U3CrollU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___OnValueChanged_0)); }
	inline RemoteOrientationInputEventHandler_t4128243105 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteOrientationInputEventHandler_t4128243105 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteOrientationInputEventHandler_t4128243105 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of_U3CpitchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___U3CpitchU3Ek__BackingField_1)); }
	inline float get_U3CpitchU3Ek__BackingField_1() const { return ___U3CpitchU3Ek__BackingField_1; }
	inline float* get_address_of_U3CpitchU3Ek__BackingField_1() { return &___U3CpitchU3Ek__BackingField_1; }
	inline void set_U3CpitchU3Ek__BackingField_1(float value)
	{
		___U3CpitchU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CyawU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___U3CyawU3Ek__BackingField_2)); }
	inline float get_U3CyawU3Ek__BackingField_2() const { return ___U3CyawU3Ek__BackingField_2; }
	inline float* get_address_of_U3CyawU3Ek__BackingField_2() { return &___U3CyawU3Ek__BackingField_2; }
	inline void set_U3CyawU3Ek__BackingField_2(float value)
	{
		___U3CyawU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CrollU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteOrientationInput_t3303200544, ___U3CrollU3Ek__BackingField_3)); }
	inline float get_U3CrollU3Ek__BackingField_3() const { return ___U3CrollU3Ek__BackingField_3; }
	inline float* get_address_of_U3CrollU3Ek__BackingField_3() { return &___U3CrollU3Ek__BackingField_3; }
	inline void set_U3CrollU3Ek__BackingField_3(float value)
	{
		___U3CrollU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEORIENTATIONINPUT_T3303200544_H
#ifndef REMOTEMOTIONSENSORINPUT_T841531810_H
#define REMOTEMOTIONSENSORINPUT_T841531810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionSensorInput
struct  RemoteMotionSensorInput_t841531810  : public RuntimeObject
{
public:
	// RemoteMotionSensorInput/RemoteMotionSensorInputEventHandler RemoteMotionSensorInput::OnValueChanged
	RemoteMotionSensorInputEventHandler_t2876817765 * ___OnValueChanged_0;
	// System.Single RemoteMotionSensorInput::<x>k__BackingField
	float ___U3CxU3Ek__BackingField_1;
	// System.Single RemoteMotionSensorInput::<y>k__BackingField
	float ___U3CyU3Ek__BackingField_2;
	// System.Single RemoteMotionSensorInput::<z>k__BackingField
	float ___U3CzU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___OnValueChanged_0)); }
	inline RemoteMotionSensorInputEventHandler_t2876817765 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteMotionSensorInputEventHandler_t2876817765 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteMotionSensorInputEventHandler_t2876817765 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of_U3CxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___U3CxU3Ek__BackingField_1)); }
	inline float get_U3CxU3Ek__BackingField_1() const { return ___U3CxU3Ek__BackingField_1; }
	inline float* get_address_of_U3CxU3Ek__BackingField_1() { return &___U3CxU3Ek__BackingField_1; }
	inline void set_U3CxU3Ek__BackingField_1(float value)
	{
		___U3CxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___U3CyU3Ek__BackingField_2)); }
	inline float get_U3CyU3Ek__BackingField_2() const { return ___U3CyU3Ek__BackingField_2; }
	inline float* get_address_of_U3CyU3Ek__BackingField_2() { return &___U3CyU3Ek__BackingField_2; }
	inline void set_U3CyU3Ek__BackingField_2(float value)
	{
		___U3CyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CzU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteMotionSensorInput_t841531810, ___U3CzU3Ek__BackingField_3)); }
	inline float get_U3CzU3Ek__BackingField_3() const { return ___U3CzU3Ek__BackingField_3; }
	inline float* get_address_of_U3CzU3Ek__BackingField_3() { return &___U3CzU3Ek__BackingField_3; }
	inline void set_U3CzU3Ek__BackingField_3(float value)
	{
		___U3CzU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONSENSORINPUT_T841531810_H
#ifndef REMOTEBUTTONINPUT_T144791130_H
#define REMOTEBUTTONINPUT_T144791130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteButtonInput
struct  RemoteButtonInput_t144791130  : public RuntimeObject
{
public:
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnPressChanged
	RemoteButtonInputEventHandler_t1063480517 * ___OnPressChanged_0;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnHeldState
	RemoteButtonInputEventHandler_t1063480517 * ___OnHeldState_1;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnPresseddState
	RemoteButtonInputEventHandler_t1063480517 * ___OnPresseddState_2;
	// RemoteButtonInput/RemoteButtonInputEventHandler RemoteButtonInput::OnReleasedState
	RemoteButtonInputEventHandler_t1063480517 * ___OnReleasedState_3;
	// System.Int32 RemoteButtonInput::m_PrevFrameCount
	int32_t ___m_PrevFrameCount_4;
	// System.Boolean RemoteButtonInput::m_PrevState
	bool ___m_PrevState_5;
	// System.Boolean RemoteButtonInput::m_State
	bool ___m_State_6;
	// System.Boolean RemoteButtonInput::_isPressed
	bool ____isPressed_7;
	// System.Boolean RemoteButtonInput::_onPressed
	bool ____onPressed_8;
	// System.Boolean RemoteButtonInput::_onReleased
	bool ____onReleased_9;
	// System.Boolean RemoteButtonInput::_onHeld
	bool ____onHeld_10;

public:
	inline static int32_t get_offset_of_OnPressChanged_0() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnPressChanged_0)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnPressChanged_0() const { return ___OnPressChanged_0; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnPressChanged_0() { return &___OnPressChanged_0; }
	inline void set_OnPressChanged_0(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnPressChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressChanged_0), value);
	}

	inline static int32_t get_offset_of_OnHeldState_1() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnHeldState_1)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnHeldState_1() const { return ___OnHeldState_1; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnHeldState_1() { return &___OnHeldState_1; }
	inline void set_OnHeldState_1(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnHeldState_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeldState_1), value);
	}

	inline static int32_t get_offset_of_OnPresseddState_2() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnPresseddState_2)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnPresseddState_2() const { return ___OnPresseddState_2; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnPresseddState_2() { return &___OnPresseddState_2; }
	inline void set_OnPresseddState_2(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnPresseddState_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnPresseddState_2), value);
	}

	inline static int32_t get_offset_of_OnReleasedState_3() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___OnReleasedState_3)); }
	inline RemoteButtonInputEventHandler_t1063480517 * get_OnReleasedState_3() const { return ___OnReleasedState_3; }
	inline RemoteButtonInputEventHandler_t1063480517 ** get_address_of_OnReleasedState_3() { return &___OnReleasedState_3; }
	inline void set_OnReleasedState_3(RemoteButtonInputEventHandler_t1063480517 * value)
	{
		___OnReleasedState_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnReleasedState_3), value);
	}

	inline static int32_t get_offset_of_m_PrevFrameCount_4() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___m_PrevFrameCount_4)); }
	inline int32_t get_m_PrevFrameCount_4() const { return ___m_PrevFrameCount_4; }
	inline int32_t* get_address_of_m_PrevFrameCount_4() { return &___m_PrevFrameCount_4; }
	inline void set_m_PrevFrameCount_4(int32_t value)
	{
		___m_PrevFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_5() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___m_PrevState_5)); }
	inline bool get_m_PrevState_5() const { return ___m_PrevState_5; }
	inline bool* get_address_of_m_PrevState_5() { return &___m_PrevState_5; }
	inline void set_m_PrevState_5(bool value)
	{
		___m_PrevState_5 = value;
	}

	inline static int32_t get_offset_of_m_State_6() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ___m_State_6)); }
	inline bool get_m_State_6() const { return ___m_State_6; }
	inline bool* get_address_of_m_State_6() { return &___m_State_6; }
	inline void set_m_State_6(bool value)
	{
		___m_State_6 = value;
	}

	inline static int32_t get_offset_of__isPressed_7() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____isPressed_7)); }
	inline bool get__isPressed_7() const { return ____isPressed_7; }
	inline bool* get_address_of__isPressed_7() { return &____isPressed_7; }
	inline void set__isPressed_7(bool value)
	{
		____isPressed_7 = value;
	}

	inline static int32_t get_offset_of__onPressed_8() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____onPressed_8)); }
	inline bool get__onPressed_8() const { return ____onPressed_8; }
	inline bool* get_address_of__onPressed_8() { return &____onPressed_8; }
	inline void set__onPressed_8(bool value)
	{
		____onPressed_8 = value;
	}

	inline static int32_t get_offset_of__onReleased_9() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____onReleased_9)); }
	inline bool get__onReleased_9() const { return ____onReleased_9; }
	inline bool* get_address_of__onReleased_9() { return &____onReleased_9; }
	inline void set__onReleased_9(bool value)
	{
		____onReleased_9 = value;
	}

	inline static int32_t get_offset_of__onHeld_10() { return static_cast<int32_t>(offsetof(RemoteButtonInput_t144791130, ____onHeld_10)); }
	inline bool get__onHeld_10() const { return ____onHeld_10; }
	inline bool* get_address_of__onHeld_10() { return &____onHeld_10; }
	inline void set__onHeld_10(bool value)
	{
		____onHeld_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEBUTTONINPUT_T144791130_H
#ifndef REMOTETOUCHINPUT_T3826108381_H
#define REMOTETOUCHINPUT_T3826108381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchInput
struct  RemoteTouchInput_t3826108381  : public RuntimeObject
{
public:
	// RemoteTouchInput/RemoteTouchInputEventHandler RemoteTouchInput::OnActiveChanged
	RemoteTouchInputEventHandler_t1375955781 * ___OnActiveChanged_0;
	// System.Boolean RemoteTouchInput::_isActive
	bool ____isActive_1;

public:
	inline static int32_t get_offset_of_OnActiveChanged_0() { return static_cast<int32_t>(offsetof(RemoteTouchInput_t3826108381, ___OnActiveChanged_0)); }
	inline RemoteTouchInputEventHandler_t1375955781 * get_OnActiveChanged_0() const { return ___OnActiveChanged_0; }
	inline RemoteTouchInputEventHandler_t1375955781 ** get_address_of_OnActiveChanged_0() { return &___OnActiveChanged_0; }
	inline void set_OnActiveChanged_0(RemoteTouchInputEventHandler_t1375955781 * value)
	{
		___OnActiveChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnActiveChanged_0), value);
	}

	inline static int32_t get_offset_of__isActive_1() { return static_cast<int32_t>(offsetof(RemoteTouchInput_t3826108381, ____isActive_1)); }
	inline bool get__isActive_1() const { return ____isActive_1; }
	inline bool* get_address_of__isActive_1() { return &____isActive_1; }
	inline void set__isActive_1(bool value)
	{
		____isActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHINPUT_T3826108381_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef SERIALIZABLEVECTOR4_T4294681242_H
#define SERIALIZABLEVECTOR4_T4294681242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector4
struct  SerializableVector4_t4294681242  : public RuntimeObject
{
public:
	// System.Single Utils.SerializableVector4::x
	float ___x_0;
	// System.Single Utils.SerializableVector4::y
	float ___y_1;
	// System.Single Utils.SerializableVector4::z
	float ___z_2;
	// System.Single Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_t4294681242, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_T4294681242_H
#ifndef ABSTRACTEVENTDATA_T1333959294_H
#define ABSTRACTEVENTDATA_T1333959294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t1333959294  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t1333959294, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T1333959294_H
#ifndef SERIALIZABLEPOINTCLOUD_T1992421910_H
#define SERIALIZABLEPOINTCLOUD_T1992421910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializablePointCloud
struct  serializablePointCloud_t1992421910  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_t3397334013* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_t1992421910, ___pointCloudData_0)); }
	inline ByteU5BU5D_t3397334013* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_t3397334013* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_T1992421910_H
#ifndef REMOTEAXISINPUT_T2770128439_H
#define REMOTEAXISINPUT_T2770128439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteAxisInput
struct  RemoteAxisInput_t2770128439  : public RuntimeObject
{
public:
	// RemoteAxisInput/RemoteAxisInputEventHandler RemoteAxisInput::OnValueChanged
	RemoteAxisInputEventHandler_t3796077797 * ___OnValueChanged_0;
	// System.Single RemoteAxisInput::_value
	float ____value_1;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(RemoteAxisInput_t2770128439, ___OnValueChanged_0)); }
	inline RemoteAxisInputEventHandler_t3796077797 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline RemoteAxisInputEventHandler_t3796077797 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(RemoteAxisInputEventHandler_t3796077797 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(RemoteAxisInput_t2770128439, ____value_1)); }
	inline float get__value_1() const { return ____value_1; }
	inline float* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(float value)
	{
		____value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEAXISINPUT_T2770128439_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef MESSAGEEVENTARGS_T301283622_H
#define MESSAGEEVENTARGS_T301283622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct  MessageEventArgs_t301283622  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Networking.PlayerConnection.MessageEventArgs::playerId
	int32_t ___playerId_0;
	// System.Byte[] UnityEngine.Networking.PlayerConnection.MessageEventArgs::data
	ByteU5BU5D_t3397334013* ___data_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(MessageEventArgs_t301283622, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t301283622, ___data_1)); }
	inline ByteU5BU5D_t3397334013* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3397334013* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T301283622_H
#ifndef MIRABTREMOTEINPUT_T988911721_H
#define MIRABTREMOTEINPUT_T988911721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBTRemoteInput
struct  MiraBTRemoteInput_t988911721  : public RuntimeObject
{
public:
	// RemoteBase MiraBTRemoteInput::controller
	RemoteBase_t3616301967 * ___controller_0;
	// VirtualRemote MiraBTRemoteInput::_virtualRemote
	VirtualRemote_t4041918011 * ____virtualRemote_1;
	// Remote MiraBTRemoteInput::_connectedRemote
	Remote_t660843562 * ____connectedRemote_2;
	// Remote MiraBTRemoteInput::connectedRemote
	Remote_t660843562 * ___connectedRemote_3;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ___controller_0)); }
	inline RemoteBase_t3616301967 * get_controller_0() const { return ___controller_0; }
	inline RemoteBase_t3616301967 ** get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(RemoteBase_t3616301967 * value)
	{
		___controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___controller_0), value);
	}

	inline static int32_t get_offset_of__virtualRemote_1() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ____virtualRemote_1)); }
	inline VirtualRemote_t4041918011 * get__virtualRemote_1() const { return ____virtualRemote_1; }
	inline VirtualRemote_t4041918011 ** get_address_of__virtualRemote_1() { return &____virtualRemote_1; }
	inline void set__virtualRemote_1(VirtualRemote_t4041918011 * value)
	{
		____virtualRemote_1 = value;
		Il2CppCodeGenWriteBarrier((&____virtualRemote_1), value);
	}

	inline static int32_t get_offset_of__connectedRemote_2() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ____connectedRemote_2)); }
	inline Remote_t660843562 * get__connectedRemote_2() const { return ____connectedRemote_2; }
	inline Remote_t660843562 ** get_address_of__connectedRemote_2() { return &____connectedRemote_2; }
	inline void set__connectedRemote_2(Remote_t660843562 * value)
	{
		____connectedRemote_2 = value;
		Il2CppCodeGenWriteBarrier((&____connectedRemote_2), value);
	}

	inline static int32_t get_offset_of_connectedRemote_3() { return static_cast<int32_t>(offsetof(MiraBTRemoteInput_t988911721, ___connectedRemote_3)); }
	inline Remote_t660843562 * get_connectedRemote_3() const { return ___connectedRemote_3; }
	inline Remote_t660843562 ** get_address_of_connectedRemote_3() { return &___connectedRemote_3; }
	inline void set_connectedRemote_3(Remote_t660843562 * value)
	{
		___connectedRemote_3 = value;
		Il2CppCodeGenWriteBarrier((&___connectedRemote_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABTREMOTEINPUT_T988911721_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef GYROSCOPE_T1705362817_H
#define GYROSCOPE_T1705362817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gyroscope
struct  Gyroscope_t1705362817  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Gyroscope::m_GyroIndex
	int32_t ___m_GyroIndex_0;

public:
	inline static int32_t get_offset_of_m_GyroIndex_0() { return static_cast<int32_t>(offsetof(Gyroscope_t1705362817, ___m_GyroIndex_0)); }
	inline int32_t get_m_GyroIndex_0() const { return ___m_GyroIndex_0; }
	inline int32_t* get_address_of_m_GyroIndex_0() { return &___m_GyroIndex_0; }
	inline void set_m_GyroIndex_0(int32_t value)
	{
		___m_GyroIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPE_T1705362817_H
#ifndef MIRACONNECTIONMESSAGEIDS_T1100361794_H
#define MIRACONNECTIONMESSAGEIDS_T1100361794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraConnectionMessageIds
struct  MiraConnectionMessageIds_t1100361794  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRACONNECTIONMESSAGEIDS_T1100361794_H
#ifndef SERIALIZABLEFLOAT_T3811907803_H
#define SERIALIZABLEFLOAT_T3811907803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFloat
struct  serializableFloat_t3811907803  : public RuntimeObject
{
public:
	// System.Single Utils.serializableFloat::x
	float ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(serializableFloat_t3811907803, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFLOAT_T3811907803_H
#ifndef SERIALIZABLEBTREMOTEBUTTONS_T2327889448_H
#define SERIALIZABLEBTREMOTEBUTTONS_T2327889448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemoteButtons
struct  serializableBTRemoteButtons_t2327889448  : public RuntimeObject
{
public:
	// System.Boolean Utils.serializableBTRemoteButtons::startButton
	bool ___startButton_0;
	// System.Boolean Utils.serializableBTRemoteButtons::backButton
	bool ___backButton_1;
	// System.Boolean Utils.serializableBTRemoteButtons::triggerButton
	bool ___triggerButton_2;

public:
	inline static int32_t get_offset_of_startButton_0() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t2327889448, ___startButton_0)); }
	inline bool get_startButton_0() const { return ___startButton_0; }
	inline bool* get_address_of_startButton_0() { return &___startButton_0; }
	inline void set_startButton_0(bool value)
	{
		___startButton_0 = value;
	}

	inline static int32_t get_offset_of_backButton_1() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t2327889448, ___backButton_1)); }
	inline bool get_backButton_1() const { return ___backButton_1; }
	inline bool* get_address_of_backButton_1() { return &___backButton_1; }
	inline void set_backButton_1(bool value)
	{
		___backButton_1 = value;
	}

	inline static int32_t get_offset_of_triggerButton_2() { return static_cast<int32_t>(offsetof(serializableBTRemoteButtons_t2327889448, ___triggerButton_2)); }
	inline bool get_triggerButton_2() const { return ___triggerButton_2; }
	inline bool* get_address_of_triggerButton_2() { return &___triggerButton_2; }
	inline void set_triggerButton_2(bool value)
	{
		___triggerButton_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTEBUTTONS_T2327889448_H
#ifndef LIST_1_T3685274804_H
#define LIST_1_T3685274804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct  List_1_t3685274804  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RaycastResultU5BU5D_t603556505* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3685274804, ____items_1)); }
	inline RaycastResultU5BU5D_t603556505* get__items_1() const { return ____items_1; }
	inline RaycastResultU5BU5D_t603556505** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RaycastResultU5BU5D_t603556505* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3685274804, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3685274804, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3685274804_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	RaycastResultU5BU5D_t603556505* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3685274804_StaticFields, ___EmptyArray_4)); }
	inline RaycastResultU5BU5D_t603556505* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline RaycastResultU5BU5D_t603556505** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(RaycastResultU5BU5D_t603556505* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3685274804_H
#ifndef MIRASUBMESSAGEIDS_T3396168626_H
#define MIRASUBMESSAGEIDS_T3396168626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraSubMessageIds
struct  MiraSubMessageIds_t3396168626  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRASUBMESSAGEIDS_T3396168626_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T2339960184_H
#define OBJECTSERIALIZATIONEXTENSION_T2339960184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t2339960184  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T2339960184_H
#ifndef LIST_1_T1125654279_H
#define LIST_1_T1125654279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t1125654279  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3057952154* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____items_1)); }
	inline GameObjectU5BU5D_t3057952154* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3057952154* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1125654279_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3057952154* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1125654279_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3057952154* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3057952154* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1125654279_H
#ifndef RECOGNIZEDTARGET_T3661431985_H
#define RECOGNIZEDTARGET_T3661431985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.RecognizedTarget
struct  RecognizedTarget_t3661431985  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Wikitude.RecognizedTarget::Drawable
	GameObject_t1756533147 * ___Drawable_0;
	// System.String Wikitude.RecognizedTarget::Name
	String_t* ___Name_1;
	// System.Int64 Wikitude.RecognizedTarget::ID
	int64_t ___ID_2;
	// System.Single[] Wikitude.RecognizedTarget::ModelViewMatrix
	SingleU5BU5D_t577127397* ___ModelViewMatrix_3;
	// System.Boolean Wikitude.RecognizedTarget::IsKnown
	bool ___IsKnown_4;
	// System.Single Wikitude.RecognizedTarget::_physicalTargetHeight
	float ____physicalTargetHeight_5;

public:
	inline static int32_t get_offset_of_Drawable_0() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3661431985, ___Drawable_0)); }
	inline GameObject_t1756533147 * get_Drawable_0() const { return ___Drawable_0; }
	inline GameObject_t1756533147 ** get_address_of_Drawable_0() { return &___Drawable_0; }
	inline void set_Drawable_0(GameObject_t1756533147 * value)
	{
		___Drawable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Drawable_0), value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3661431985, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_ID_2() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3661431985, ___ID_2)); }
	inline int64_t get_ID_2() const { return ___ID_2; }
	inline int64_t* get_address_of_ID_2() { return &___ID_2; }
	inline void set_ID_2(int64_t value)
	{
		___ID_2 = value;
	}

	inline static int32_t get_offset_of_ModelViewMatrix_3() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3661431985, ___ModelViewMatrix_3)); }
	inline SingleU5BU5D_t577127397* get_ModelViewMatrix_3() const { return ___ModelViewMatrix_3; }
	inline SingleU5BU5D_t577127397** get_address_of_ModelViewMatrix_3() { return &___ModelViewMatrix_3; }
	inline void set_ModelViewMatrix_3(SingleU5BU5D_t577127397* value)
	{
		___ModelViewMatrix_3 = value;
		Il2CppCodeGenWriteBarrier((&___ModelViewMatrix_3), value);
	}

	inline static int32_t get_offset_of_IsKnown_4() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3661431985, ___IsKnown_4)); }
	inline bool get_IsKnown_4() const { return ___IsKnown_4; }
	inline bool* get_address_of_IsKnown_4() { return &___IsKnown_4; }
	inline void set_IsKnown_4(bool value)
	{
		___IsKnown_4 = value;
	}

	inline static int32_t get_offset_of__physicalTargetHeight_5() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3661431985, ____physicalTargetHeight_5)); }
	inline float get__physicalTargetHeight_5() const { return ____physicalTargetHeight_5; }
	inline float* get_address_of__physicalTargetHeight_5() { return &____physicalTargetHeight_5; }
	inline void set__physicalTargetHeight_5(float value)
	{
		____physicalTargetHeight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOGNIZEDTARGET_T3661431985_H
#ifndef MOUSESTATE_T3572864619_H
#define MOUSESTATE_T3572864619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseState
struct  MouseState_t3572864619  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState> UnityEngine.EventSystems.PointerInputModule/MouseState::m_TrackedButtons
	List_1_t2057496624 * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_t3572864619, ___m_TrackedButtons_0)); }
	inline List_1_t2057496624 * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_t2057496624 ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_t2057496624 * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedButtons_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSESTATE_T3572864619_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef MIRABASEPOINTER_T885132991_H
#define MIRABASEPOINTER_T885132991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBasePointer
struct  MiraBasePointer_t885132991  : public RuntimeObject
{
public:
	// System.Single MiraBasePointer::<maxDistance>k__BackingField
	float ___U3CmaxDistanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmaxDistanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MiraBasePointer_t885132991, ___U3CmaxDistanceU3Ek__BackingField_0)); }
	inline float get_U3CmaxDistanceU3Ek__BackingField_0() const { return ___U3CmaxDistanceU3Ek__BackingField_0; }
	inline float* get_address_of_U3CmaxDistanceU3Ek__BackingField_0() { return &___U3CmaxDistanceU3Ek__BackingField_0; }
	inline void set_U3CmaxDistanceU3Ek__BackingField_0(float value)
	{
		___U3CmaxDistanceU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASEPOINTER_T885132991_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef SCENE_T1684909666_H
#define SCENE_T1684909666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t1684909666 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t1684909666, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T1684909666_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef MEMORYSTREAM_T743994179_H
#define MEMORYSTREAM_T743994179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t743994179  : public Stream_t3255436806
{
public:
	// System.Boolean System.IO.MemoryStream::canWrite
	bool ___canWrite_1;
	// System.Boolean System.IO.MemoryStream::allowGetBuffer
	bool ___allowGetBuffer_2;
	// System.Int32 System.IO.MemoryStream::capacity
	int32_t ___capacity_3;
	// System.Int32 System.IO.MemoryStream::length
	int32_t ___length_4;
	// System.Byte[] System.IO.MemoryStream::internalBuffer
	ByteU5BU5D_t3397334013* ___internalBuffer_5;
	// System.Int32 System.IO.MemoryStream::initialIndex
	int32_t ___initialIndex_6;
	// System.Boolean System.IO.MemoryStream::expandable
	bool ___expandable_7;
	// System.Boolean System.IO.MemoryStream::streamClosed
	bool ___streamClosed_8;
	// System.Int32 System.IO.MemoryStream::position
	int32_t ___position_9;
	// System.Int32 System.IO.MemoryStream::dirty_bytes
	int32_t ___dirty_bytes_10;

public:
	inline static int32_t get_offset_of_canWrite_1() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___canWrite_1)); }
	inline bool get_canWrite_1() const { return ___canWrite_1; }
	inline bool* get_address_of_canWrite_1() { return &___canWrite_1; }
	inline void set_canWrite_1(bool value)
	{
		___canWrite_1 = value;
	}

	inline static int32_t get_offset_of_allowGetBuffer_2() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___allowGetBuffer_2)); }
	inline bool get_allowGetBuffer_2() const { return ___allowGetBuffer_2; }
	inline bool* get_address_of_allowGetBuffer_2() { return &___allowGetBuffer_2; }
	inline void set_allowGetBuffer_2(bool value)
	{
		___allowGetBuffer_2 = value;
	}

	inline static int32_t get_offset_of_capacity_3() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___capacity_3)); }
	inline int32_t get_capacity_3() const { return ___capacity_3; }
	inline int32_t* get_address_of_capacity_3() { return &___capacity_3; }
	inline void set_capacity_3(int32_t value)
	{
		___capacity_3 = value;
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_internalBuffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___internalBuffer_5)); }
	inline ByteU5BU5D_t3397334013* get_internalBuffer_5() const { return ___internalBuffer_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_internalBuffer_5() { return &___internalBuffer_5; }
	inline void set_internalBuffer_5(ByteU5BU5D_t3397334013* value)
	{
		___internalBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalBuffer_5), value);
	}

	inline static int32_t get_offset_of_initialIndex_6() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___initialIndex_6)); }
	inline int32_t get_initialIndex_6() const { return ___initialIndex_6; }
	inline int32_t* get_address_of_initialIndex_6() { return &___initialIndex_6; }
	inline void set_initialIndex_6(int32_t value)
	{
		___initialIndex_6 = value;
	}

	inline static int32_t get_offset_of_expandable_7() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___expandable_7)); }
	inline bool get_expandable_7() const { return ___expandable_7; }
	inline bool* get_address_of_expandable_7() { return &___expandable_7; }
	inline void set_expandable_7(bool value)
	{
		___expandable_7 = value;
	}

	inline static int32_t get_offset_of_streamClosed_8() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___streamClosed_8)); }
	inline bool get_streamClosed_8() const { return ___streamClosed_8; }
	inline bool* get_address_of_streamClosed_8() { return &___streamClosed_8; }
	inline void set_streamClosed_8(bool value)
	{
		___streamClosed_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___position_9)); }
	inline int32_t get_position_9() const { return ___position_9; }
	inline int32_t* get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(int32_t value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_dirty_bytes_10() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ___dirty_bytes_10)); }
	inline int32_t get_dirty_bytes_10() const { return ___dirty_bytes_10; }
	inline int32_t* get_address_of_dirty_bytes_10() { return &___dirty_bytes_10; }
	inline void set_dirty_bytes_10(int32_t value)
	{
		___dirty_bytes_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T743994179_H
#ifndef SERIALIZABLEVECTOR3_T4294681249_H
#define SERIALIZABLEVECTOR3_T4294681249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector3
struct  SerializableVector3_t4294681249 
{
public:
	// System.Single Utils.SerializableVector3::x
	float ___x_0;
	// System.Single Utils.SerializableVector3::y
	float ___y_1;
	// System.Single Utils.SerializableVector3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector3_t4294681249, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector3_t4294681249, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector3_t4294681249, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR3_T4294681249_H
#ifndef REMOTEMOTIONINPUT_T3548926620_H
#define REMOTEMOTIONINPUT_T3548926620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteMotionInput
struct  RemoteMotionInput_t3548926620  : public RemoteTouchInput_t3826108381
{
public:
	// RemoteMotionInput/RemoteMotionInputEventHandler RemoteMotionInput::OnValueChanged
	RemoteMotionInputEventHandler_t1458362501 * ___OnValueChanged_2;
	// RemoteOrientationInput RemoteMotionInput::<orientation>k__BackingField
	RemoteOrientationInput_t3303200544 * ___U3CorientationU3Ek__BackingField_3;
	// RemoteMotionSensorInput RemoteMotionInput::<acceleration>k__BackingField
	RemoteMotionSensorInput_t841531810 * ___U3CaccelerationU3Ek__BackingField_4;
	// RemoteMotionSensorInput RemoteMotionInput::<rotationRate>k__BackingField
	RemoteMotionSensorInput_t841531810 * ___U3CrotationRateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_OnValueChanged_2() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___OnValueChanged_2)); }
	inline RemoteMotionInputEventHandler_t1458362501 * get_OnValueChanged_2() const { return ___OnValueChanged_2; }
	inline RemoteMotionInputEventHandler_t1458362501 ** get_address_of_OnValueChanged_2() { return &___OnValueChanged_2; }
	inline void set_OnValueChanged_2(RemoteMotionInputEventHandler_t1458362501 * value)
	{
		___OnValueChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CorientationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___U3CorientationU3Ek__BackingField_3)); }
	inline RemoteOrientationInput_t3303200544 * get_U3CorientationU3Ek__BackingField_3() const { return ___U3CorientationU3Ek__BackingField_3; }
	inline RemoteOrientationInput_t3303200544 ** get_address_of_U3CorientationU3Ek__BackingField_3() { return &___U3CorientationU3Ek__BackingField_3; }
	inline void set_U3CorientationU3Ek__BackingField_3(RemoteOrientationInput_t3303200544 * value)
	{
		___U3CorientationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CorientationU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CaccelerationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___U3CaccelerationU3Ek__BackingField_4)); }
	inline RemoteMotionSensorInput_t841531810 * get_U3CaccelerationU3Ek__BackingField_4() const { return ___U3CaccelerationU3Ek__BackingField_4; }
	inline RemoteMotionSensorInput_t841531810 ** get_address_of_U3CaccelerationU3Ek__BackingField_4() { return &___U3CaccelerationU3Ek__BackingField_4; }
	inline void set_U3CaccelerationU3Ek__BackingField_4(RemoteMotionSensorInput_t841531810 * value)
	{
		___U3CaccelerationU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaccelerationU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrotationRateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteMotionInput_t3548926620, ___U3CrotationRateU3Ek__BackingField_5)); }
	inline RemoteMotionSensorInput_t841531810 * get_U3CrotationRateU3Ek__BackingField_5() const { return ___U3CrotationRateU3Ek__BackingField_5; }
	inline RemoteMotionSensorInput_t841531810 ** get_address_of_U3CrotationRateU3Ek__BackingField_5() { return &___U3CrotationRateU3Ek__BackingField_5; }
	inline void set_U3CrotationRateU3Ek__BackingField_5(RemoteMotionSensorInput_t841531810 * value)
	{
		___U3CrotationRateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrotationRateU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMOTIONINPUT_T3548926620_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef BYTE_T3683104436_H
#define BYTE_T3683104436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t3683104436 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t3683104436, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T3683104436_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef UNITYEVENT_3_T2425689862_H
#define UNITYEVENT_3_T2425689862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t2425689862  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2425689862, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2425689862_H
#ifndef UNITYEVENT_1_T2053356837_H
#define UNITYEVENT_1_T2053356837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Wikitude.ImageTarget>
struct  UnityEvent_1_t2053356837  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2053356837, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2053356837_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2510243513 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2510243513 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2510243513 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef VIRTUALREMOTE_T4041918011_H
#define VIRTUALREMOTE_T4041918011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VirtualRemote
struct  VirtualRemote_t4041918011  : public RemoteBase_t3616301967
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALREMOTE_T4041918011_H
#ifndef BASEEVENTDATA_T2681005625_H
#define BASEEVENTDATA_T2681005625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t2681005625  : public AbstractEventData_t1333959294
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t2681005625, ___m_EventSystem_1)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T2681005625_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef SERIALIZABLEQUATERNION_T3902400085_H
#define SERIALIZABLEQUATERNION_T3902400085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableQuaternion
struct  SerializableQuaternion_t3902400085 
{
public:
	// System.Single Utils.SerializableQuaternion::x
	float ___x_0;
	// System.Single Utils.SerializableQuaternion::y
	float ___y_1;
	// System.Single Utils.SerializableQuaternion::z
	float ___z_2;
	// System.Single Utils.SerializableQuaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableQuaternion_t3902400085, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEQUATERNION_T3902400085_H
#ifndef REMOTETOUCHPADINPUT_T1081319266_H
#define REMOTETOUCHPADINPUT_T1081319266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteTouchPadInput
struct  RemoteTouchPadInput_t1081319266  : public RemoteTouchInput_t3826108381
{
public:
	// RemoteTouchPadInput/RemoteTouchPadInputEventHandler RemoteTouchPadInput::OnValueChanged
	RemoteTouchPadInputEventHandler_t2832767717 * ___OnValueChanged_2;
	// RemoteButtonInput RemoteTouchPadInput::<button>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CbuttonU3Ek__BackingField_3;
	// RemoteButtonInput RemoteTouchPadInput::<touchActive>k__BackingField
	RemoteButtonInput_t144791130 * ___U3CtouchActiveU3Ek__BackingField_4;
	// RemoteAxisInput RemoteTouchPadInput::<xAxis>k__BackingField
	RemoteAxisInput_t2770128439 * ___U3CxAxisU3Ek__BackingField_5;
	// RemoteAxisInput RemoteTouchPadInput::<yAxis>k__BackingField
	RemoteAxisInput_t2770128439 * ___U3CyAxisU3Ek__BackingField_6;
	// RemoteTouchInput RemoteTouchPadInput::<up>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CupU3Ek__BackingField_7;
	// RemoteTouchInput RemoteTouchPadInput::<down>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CdownU3Ek__BackingField_8;
	// RemoteTouchInput RemoteTouchPadInput::<left>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CleftU3Ek__BackingField_9;
	// RemoteTouchInput RemoteTouchPadInput::<right>k__BackingField
	RemoteTouchInput_t3826108381 * ___U3CrightU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_OnValueChanged_2() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___OnValueChanged_2)); }
	inline RemoteTouchPadInputEventHandler_t2832767717 * get_OnValueChanged_2() const { return ___OnValueChanged_2; }
	inline RemoteTouchPadInputEventHandler_t2832767717 ** get_address_of_OnValueChanged_2() { return &___OnValueChanged_2; }
	inline void set_OnValueChanged_2(RemoteTouchPadInputEventHandler_t2832767717 * value)
	{
		___OnValueChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CbuttonU3Ek__BackingField_3)); }
	inline RemoteButtonInput_t144791130 * get_U3CbuttonU3Ek__BackingField_3() const { return ___U3CbuttonU3Ek__BackingField_3; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CbuttonU3Ek__BackingField_3() { return &___U3CbuttonU3Ek__BackingField_3; }
	inline void set_U3CbuttonU3Ek__BackingField_3(RemoteButtonInput_t144791130 * value)
	{
		___U3CbuttonU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuttonU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtouchActiveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CtouchActiveU3Ek__BackingField_4)); }
	inline RemoteButtonInput_t144791130 * get_U3CtouchActiveU3Ek__BackingField_4() const { return ___U3CtouchActiveU3Ek__BackingField_4; }
	inline RemoteButtonInput_t144791130 ** get_address_of_U3CtouchActiveU3Ek__BackingField_4() { return &___U3CtouchActiveU3Ek__BackingField_4; }
	inline void set_U3CtouchActiveU3Ek__BackingField_4(RemoteButtonInput_t144791130 * value)
	{
		___U3CtouchActiveU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchActiveU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CxAxisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CxAxisU3Ek__BackingField_5)); }
	inline RemoteAxisInput_t2770128439 * get_U3CxAxisU3Ek__BackingField_5() const { return ___U3CxAxisU3Ek__BackingField_5; }
	inline RemoteAxisInput_t2770128439 ** get_address_of_U3CxAxisU3Ek__BackingField_5() { return &___U3CxAxisU3Ek__BackingField_5; }
	inline void set_U3CxAxisU3Ek__BackingField_5(RemoteAxisInput_t2770128439 * value)
	{
		___U3CxAxisU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxAxisU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CyAxisU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CyAxisU3Ek__BackingField_6)); }
	inline RemoteAxisInput_t2770128439 * get_U3CyAxisU3Ek__BackingField_6() const { return ___U3CyAxisU3Ek__BackingField_6; }
	inline RemoteAxisInput_t2770128439 ** get_address_of_U3CyAxisU3Ek__BackingField_6() { return &___U3CyAxisU3Ek__BackingField_6; }
	inline void set_U3CyAxisU3Ek__BackingField_6(RemoteAxisInput_t2770128439 * value)
	{
		___U3CyAxisU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CyAxisU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CupU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CupU3Ek__BackingField_7)); }
	inline RemoteTouchInput_t3826108381 * get_U3CupU3Ek__BackingField_7() const { return ___U3CupU3Ek__BackingField_7; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CupU3Ek__BackingField_7() { return &___U3CupU3Ek__BackingField_7; }
	inline void set_U3CupU3Ek__BackingField_7(RemoteTouchInput_t3826108381 * value)
	{
		___U3CupU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdownU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CdownU3Ek__BackingField_8)); }
	inline RemoteTouchInput_t3826108381 * get_U3CdownU3Ek__BackingField_8() const { return ___U3CdownU3Ek__BackingField_8; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CdownU3Ek__BackingField_8() { return &___U3CdownU3Ek__BackingField_8; }
	inline void set_U3CdownU3Ek__BackingField_8(RemoteTouchInput_t3826108381 * value)
	{
		___U3CdownU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CleftU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CleftU3Ek__BackingField_9)); }
	inline RemoteTouchInput_t3826108381 * get_U3CleftU3Ek__BackingField_9() const { return ___U3CleftU3Ek__BackingField_9; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CleftU3Ek__BackingField_9() { return &___U3CleftU3Ek__BackingField_9; }
	inline void set_U3CleftU3Ek__BackingField_9(RemoteTouchInput_t3826108381 * value)
	{
		___U3CleftU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CleftU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CrightU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RemoteTouchPadInput_t1081319266, ___U3CrightU3Ek__BackingField_10)); }
	inline RemoteTouchInput_t3826108381 * get_U3CrightU3Ek__BackingField_10() const { return ___U3CrightU3Ek__BackingField_10; }
	inline RemoteTouchInput_t3826108381 ** get_address_of_U3CrightU3Ek__BackingField_10() { return &___U3CrightU3Ek__BackingField_10; }
	inline void set_U3CrightU3Ek__BackingField_10(RemoteTouchInput_t3826108381 * value)
	{
		___U3CrightU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrightU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTETOUCHPADINPUT_T1081319266_H
#ifndef SERIALIZABLEVECTOR2_T4294681248_H
#define SERIALIZABLEVECTOR2_T4294681248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector2
struct  SerializableVector2_t4294681248 
{
public:
	// System.Single Utils.SerializableVector2::x
	float ___x_0;
	// System.Single Utils.SerializableVector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector2_t4294681248, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector2_t4294681248, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR2_T4294681248_H
#ifndef NULLABLE_1_T339576247_H
#define NULLABLE_1_T339576247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t339576247 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T339576247_H
#ifndef NULLABLE_1_T334943763_H
#define NULLABLE_1_T334943763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t334943763 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T334943763_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef ONIMAGERECOGNIZEDEVENT_T2106945187_H
#define ONIMAGERECOGNIZEDEVENT_T2106945187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable/OnImageRecognizedEvent
struct  OnImageRecognizedEvent_t2106945187  : public UnityEvent_1_t2053356837
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONIMAGERECOGNIZEDEVENT_T2106945187_H
#ifndef FORMATTERASSEMBLYSTYLE_T999493661_H
#define FORMATTERASSEMBLYSTYLE_T999493661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_t999493661 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_t999493661, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_T999493661_H
#ifndef IMAGETARGET_T2015006822_H
#define IMAGETARGET_T2015006822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTarget
struct  ImageTarget_t2015006822  : public RecognizedTarget_t3661431985
{
public:
	// UnityEngine.Vector2 Wikitude.ImageTarget::_scale
	Vector2_t2243707579  ____scale_6;

public:
	inline static int32_t get_offset_of__scale_6() { return static_cast<int32_t>(offsetof(ImageTarget_t2015006822, ____scale_6)); }
	inline Vector2_t2243707579  get__scale_6() const { return ____scale_6; }
	inline Vector2_t2243707579 * get_address_of__scale_6() { return &____scale_6; }
	inline void set__scale_6(Vector2_t2243707579  value)
	{
		____scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGET_T2015006822_H
#ifndef MOVEDIRECTION_T1406276862_H
#define MOVEDIRECTION_T1406276862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t1406276862 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1406276862, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1406276862_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef TEXTUREFORMAT_T1386130234_H
#define TEXTUREFORMAT_T1386130234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t1386130234 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t1386130234, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T1386130234_H
#ifndef ONIMAGELOSTEVENT_T2609021075_H
#define ONIMAGELOSTEVENT_T2609021075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable/OnImageLostEvent
struct  OnImageLostEvent_t2609021075  : public UnityEvent_1_t2053356837
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONIMAGELOSTEVENT_T2609021075_H
#ifndef BLOCKINGOBJECTS_T2548930813_H
#define BLOCKINGOBJECTS_T2548930813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GraphicRaycaster/BlockingObjects
struct  BlockingObjects_t2548930813 
{
public:
	// System.Int32 UnityEngine.UI.GraphicRaycaster/BlockingObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockingObjects_t2548930813, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_T2548930813_H
#ifndef SERIALIZABLEBTREMOTETOUCHPAD_T2020069673_H
#define SERIALIZABLEBTREMOTETOUCHPAD_T2020069673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemoteTouchPad
struct  serializableBTRemoteTouchPad_t2020069673  : public RuntimeObject
{
public:
	// System.Boolean Utils.serializableBTRemoteTouchPad::touchActive
	bool ___touchActive_0;
	// System.Boolean Utils.serializableBTRemoteTouchPad::touchButton
	bool ___touchButton_1;
	// Utils.SerializableVector2 Utils.serializableBTRemoteTouchPad::touchPos
	SerializableVector2_t4294681248  ___touchPos_2;
	// System.Boolean Utils.serializableBTRemoteTouchPad::upButton
	bool ___upButton_3;
	// System.Boolean Utils.serializableBTRemoteTouchPad::downButton
	bool ___downButton_4;
	// System.Boolean Utils.serializableBTRemoteTouchPad::leftButton
	bool ___leftButton_5;
	// System.Boolean Utils.serializableBTRemoteTouchPad::rightButton
	bool ___rightButton_6;

public:
	inline static int32_t get_offset_of_touchActive_0() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___touchActive_0)); }
	inline bool get_touchActive_0() const { return ___touchActive_0; }
	inline bool* get_address_of_touchActive_0() { return &___touchActive_0; }
	inline void set_touchActive_0(bool value)
	{
		___touchActive_0 = value;
	}

	inline static int32_t get_offset_of_touchButton_1() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___touchButton_1)); }
	inline bool get_touchButton_1() const { return ___touchButton_1; }
	inline bool* get_address_of_touchButton_1() { return &___touchButton_1; }
	inline void set_touchButton_1(bool value)
	{
		___touchButton_1 = value;
	}

	inline static int32_t get_offset_of_touchPos_2() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___touchPos_2)); }
	inline SerializableVector2_t4294681248  get_touchPos_2() const { return ___touchPos_2; }
	inline SerializableVector2_t4294681248 * get_address_of_touchPos_2() { return &___touchPos_2; }
	inline void set_touchPos_2(SerializableVector2_t4294681248  value)
	{
		___touchPos_2 = value;
	}

	inline static int32_t get_offset_of_upButton_3() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___upButton_3)); }
	inline bool get_upButton_3() const { return ___upButton_3; }
	inline bool* get_address_of_upButton_3() { return &___upButton_3; }
	inline void set_upButton_3(bool value)
	{
		___upButton_3 = value;
	}

	inline static int32_t get_offset_of_downButton_4() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___downButton_4)); }
	inline bool get_downButton_4() const { return ___downButton_4; }
	inline bool* get_address_of_downButton_4() { return &___downButton_4; }
	inline void set_downButton_4(bool value)
	{
		___downButton_4 = value;
	}

	inline static int32_t get_offset_of_leftButton_5() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___leftButton_5)); }
	inline bool get_leftButton_5() const { return ___leftButton_5; }
	inline bool* get_address_of_leftButton_5() { return &___leftButton_5; }
	inline void set_leftButton_5(bool value)
	{
		___leftButton_5 = value;
	}

	inline static int32_t get_offset_of_rightButton_6() { return static_cast<int32_t>(offsetof(serializableBTRemoteTouchPad_t2020069673, ___rightButton_6)); }
	inline bool get_rightButton_6() const { return ___rightButton_6; }
	inline bool* get_address_of_rightButton_6() { return &___rightButton_6; }
	inline void set_rightButton_6(bool value)
	{
		___rightButton_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTETOUCHPAD_T2020069673_H
#ifndef FORMATTERTYPESTYLE_T943306207_H
#define FORMATTERTYPESTYLE_T943306207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterTypeStyle
struct  FormatterTypeStyle_t943306207 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterTypeStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FormatterTypeStyle_t943306207, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERTYPESTYLE_T943306207_H
#ifndef SERIALIZABLEBTREMOTE_T622411689_H
#define SERIALIZABLEBTREMOTE_T622411689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableBTRemote
struct  serializableBTRemote_t622411689  : public RuntimeObject
{
public:
	// Utils.SerializableVector3 Utils.serializableBTRemote::orientation
	SerializableVector3_t4294681249  ___orientation_0;
	// Utils.SerializableVector3 Utils.serializableBTRemote::rotationRate
	SerializableVector3_t4294681249  ___rotationRate_1;
	// Utils.SerializableVector3 Utils.serializableBTRemote::acceleration
	SerializableVector3_t4294681249  ___acceleration_2;

public:
	inline static int32_t get_offset_of_orientation_0() { return static_cast<int32_t>(offsetof(serializableBTRemote_t622411689, ___orientation_0)); }
	inline SerializableVector3_t4294681249  get_orientation_0() const { return ___orientation_0; }
	inline SerializableVector3_t4294681249 * get_address_of_orientation_0() { return &___orientation_0; }
	inline void set_orientation_0(SerializableVector3_t4294681249  value)
	{
		___orientation_0 = value;
	}

	inline static int32_t get_offset_of_rotationRate_1() { return static_cast<int32_t>(offsetof(serializableBTRemote_t622411689, ___rotationRate_1)); }
	inline SerializableVector3_t4294681249  get_rotationRate_1() const { return ___rotationRate_1; }
	inline SerializableVector3_t4294681249 * get_address_of_rotationRate_1() { return &___rotationRate_1; }
	inline void set_rotationRate_1(SerializableVector3_t4294681249  value)
	{
		___rotationRate_1 = value;
	}

	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(serializableBTRemote_t622411689, ___acceleration_2)); }
	inline SerializableVector3_t4294681249  get_acceleration_2() const { return ___acceleration_2; }
	inline SerializableVector3_t4294681249 * get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(SerializableVector3_t4294681249  value)
	{
		___acceleration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEBTREMOTE_T622411689_H
#ifndef TYPEFILTERLEVEL_T1182459634_H
#define TYPEFILTERLEVEL_T1182459634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.TypeFilterLevel
struct  TypeFilterLevel_t1182459634 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.TypeFilterLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeFilterLevel_t1182459634, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERLEVEL_T1182459634_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef SERIALIZABLETRANSFORM_T2202979937_H
#define SERIALIZABLETRANSFORM_T2202979937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableTransform
struct  serializableTransform_t2202979937  : public RuntimeObject
{
public:
	// Utils.SerializableVector3 Utils.serializableTransform::position
	SerializableVector3_t4294681249  ___position_0;
	// Utils.SerializableQuaternion Utils.serializableTransform::rotation
	SerializableQuaternion_t3902400085  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(serializableTransform_t2202979937, ___position_0)); }
	inline SerializableVector3_t4294681249  get_position_0() const { return ___position_0; }
	inline SerializableVector3_t4294681249 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(SerializableVector3_t4294681249  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(serializableTransform_t2202979937, ___rotation_1)); }
	inline SerializableQuaternion_t3902400085  get_rotation_1() const { return ___rotation_1; }
	inline SerializableQuaternion_t3902400085 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(SerializableQuaternion_t3902400085  value)
	{
		___rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLETRANSFORM_T2202979937_H
#ifndef SERIALIZABLEGYROSCOPE_T1293559986_H
#define SERIALIZABLEGYROSCOPE_T1293559986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableGyroscope
struct  serializableGyroscope_t1293559986  : public RuntimeObject
{
public:
	// Utils.SerializableQuaternion Utils.serializableGyroscope::attitude
	SerializableQuaternion_t3902400085  ___attitude_0;
	// Utils.SerializableVector3 Utils.serializableGyroscope::userAcceleration
	SerializableVector3_t4294681249  ___userAcceleration_1;

public:
	inline static int32_t get_offset_of_attitude_0() { return static_cast<int32_t>(offsetof(serializableGyroscope_t1293559986, ___attitude_0)); }
	inline SerializableQuaternion_t3902400085  get_attitude_0() const { return ___attitude_0; }
	inline SerializableQuaternion_t3902400085 * get_address_of_attitude_0() { return &___attitude_0; }
	inline void set_attitude_0(SerializableQuaternion_t3902400085  value)
	{
		___attitude_0 = value;
	}

	inline static int32_t get_offset_of_userAcceleration_1() { return static_cast<int32_t>(offsetof(serializableGyroscope_t1293559986, ___userAcceleration_1)); }
	inline SerializableVector3_t4294681249  get_userAcceleration_1() const { return ___userAcceleration_1; }
	inline SerializableVector3_t4294681249 * get_address_of_userAcceleration_1() { return &___userAcceleration_1; }
	inline void set_userAcceleration_1(SerializableVector3_t4294681249  value)
	{
		___userAcceleration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEGYROSCOPE_T1293559986_H
#ifndef RAYCASTSTYLE_T2248787495_H
#define RAYCASTSTYLE_T2248787495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster/RaycastStyle
struct  RaycastStyle_t2248787495 
{
public:
	// System.Int32 MiraBaseRaycaster/RaycastStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RaycastStyle_t2248787495, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTSTYLE_T2248787495_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_T2894567809_H
#define SERIALIZABLEFROMEDITORMESSAGE_T2894567809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_t2894567809  : public RuntimeObject
{
public:
	// System.Guid Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// System.Byte[] Utils.serializableFromEditorMessage::bytes
	ByteU5BU5D_t3397334013* ___bytes_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t2894567809, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t2894567809, ___bytes_1)); }
	inline ByteU5BU5D_t3397334013* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t3397334013* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_T2894567809_H
#ifndef INPUTBUTTON_T2981963041_H
#define INPUTBUTTON_T2981963041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t2981963041 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t2981963041, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T2981963041_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef REMOTE_T660843562_H
#define REMOTE_T660843562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Remote
struct  Remote_t660843562  : public RemoteBase_t3616301967
{
public:
	// Remote/RemoteRefreshedEventHandler Remote::OnRefresh
	RemoteRefreshedEventHandler_t1036293937 * ___OnRefresh_5;
	// System.Guid Remote::identifier
	Guid_t  ___identifier_6;
	// System.String Remote::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_7;
	// System.String Remote::<productName>k__BackingField
	String_t* ___U3CproductNameU3Ek__BackingField_8;
	// System.String Remote::<serialNumber>k__BackingField
	String_t* ___U3CserialNumberU3Ek__BackingField_9;
	// System.String Remote::<hardwareIdentifier>k__BackingField
	String_t* ___U3ChardwareIdentifierU3Ek__BackingField_10;
	// System.String Remote::<firmwareVersion>k__BackingField
	String_t* ___U3CfirmwareVersionU3Ek__BackingField_11;
	// System.Nullable`1<System.Single> Remote::<batteryPercentage>k__BackingField
	Nullable_1_t339576247  ___U3CbatteryPercentageU3Ek__BackingField_12;
	// System.Nullable`1<System.Int32> Remote::<rssi>k__BackingField
	Nullable_1_t334943763  ___U3CrssiU3Ek__BackingField_13;
	// System.Boolean Remote::<isConnected>k__BackingField
	bool ___U3CisConnectedU3Ek__BackingField_14;
	// System.Boolean Remote::<isPreferred>k__BackingField
	bool ___U3CisPreferredU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_OnRefresh_5() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___OnRefresh_5)); }
	inline RemoteRefreshedEventHandler_t1036293937 * get_OnRefresh_5() const { return ___OnRefresh_5; }
	inline RemoteRefreshedEventHandler_t1036293937 ** get_address_of_OnRefresh_5() { return &___OnRefresh_5; }
	inline void set_OnRefresh_5(RemoteRefreshedEventHandler_t1036293937 * value)
	{
		___OnRefresh_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnRefresh_5), value);
	}

	inline static int32_t get_offset_of_identifier_6() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___identifier_6)); }
	inline Guid_t  get_identifier_6() const { return ___identifier_6; }
	inline Guid_t * get_address_of_identifier_6() { return &___identifier_6; }
	inline void set_identifier_6(Guid_t  value)
	{
		___identifier_6 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CnameU3Ek__BackingField_7)); }
	inline String_t* get_U3CnameU3Ek__BackingField_7() const { return ___U3CnameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_7() { return &___U3CnameU3Ek__BackingField_7; }
	inline void set_U3CnameU3Ek__BackingField_7(String_t* value)
	{
		___U3CnameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CproductNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CproductNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CproductNameU3Ek__BackingField_8() const { return ___U3CproductNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CproductNameU3Ek__BackingField_8() { return &___U3CproductNameU3Ek__BackingField_8; }
	inline void set_U3CproductNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CproductNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductNameU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CserialNumberU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CserialNumberU3Ek__BackingField_9)); }
	inline String_t* get_U3CserialNumberU3Ek__BackingField_9() const { return ___U3CserialNumberU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CserialNumberU3Ek__BackingField_9() { return &___U3CserialNumberU3Ek__BackingField_9; }
	inline void set_U3CserialNumberU3Ek__BackingField_9(String_t* value)
	{
		___U3CserialNumberU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CserialNumberU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3ChardwareIdentifierU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3ChardwareIdentifierU3Ek__BackingField_10)); }
	inline String_t* get_U3ChardwareIdentifierU3Ek__BackingField_10() const { return ___U3ChardwareIdentifierU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3ChardwareIdentifierU3Ek__BackingField_10() { return &___U3ChardwareIdentifierU3Ek__BackingField_10; }
	inline void set_U3ChardwareIdentifierU3Ek__BackingField_10(String_t* value)
	{
		___U3ChardwareIdentifierU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChardwareIdentifierU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CfirmwareVersionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CfirmwareVersionU3Ek__BackingField_11)); }
	inline String_t* get_U3CfirmwareVersionU3Ek__BackingField_11() const { return ___U3CfirmwareVersionU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CfirmwareVersionU3Ek__BackingField_11() { return &___U3CfirmwareVersionU3Ek__BackingField_11; }
	inline void set_U3CfirmwareVersionU3Ek__BackingField_11(String_t* value)
	{
		___U3CfirmwareVersionU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfirmwareVersionU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CbatteryPercentageU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CbatteryPercentageU3Ek__BackingField_12)); }
	inline Nullable_1_t339576247  get_U3CbatteryPercentageU3Ek__BackingField_12() const { return ___U3CbatteryPercentageU3Ek__BackingField_12; }
	inline Nullable_1_t339576247 * get_address_of_U3CbatteryPercentageU3Ek__BackingField_12() { return &___U3CbatteryPercentageU3Ek__BackingField_12; }
	inline void set_U3CbatteryPercentageU3Ek__BackingField_12(Nullable_1_t339576247  value)
	{
		___U3CbatteryPercentageU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CrssiU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CrssiU3Ek__BackingField_13)); }
	inline Nullable_1_t334943763  get_U3CrssiU3Ek__BackingField_13() const { return ___U3CrssiU3Ek__BackingField_13; }
	inline Nullable_1_t334943763 * get_address_of_U3CrssiU3Ek__BackingField_13() { return &___U3CrssiU3Ek__BackingField_13; }
	inline void set_U3CrssiU3Ek__BackingField_13(Nullable_1_t334943763  value)
	{
		___U3CrssiU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CisConnectedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CisConnectedU3Ek__BackingField_14)); }
	inline bool get_U3CisConnectedU3Ek__BackingField_14() const { return ___U3CisConnectedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisConnectedU3Ek__BackingField_14() { return &___U3CisConnectedU3Ek__BackingField_14; }
	inline void set_U3CisConnectedU3Ek__BackingField_14(bool value)
	{
		___U3CisConnectedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPreferredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Remote_t660843562, ___U3CisPreferredU3Ek__BackingField_15)); }
	inline bool get_U3CisPreferredU3Ek__BackingField_15() const { return ___U3CisPreferredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPreferredU3Ek__BackingField_15() { return &___U3CisPreferredU3Ek__BackingField_15; }
	inline void set_U3CisPreferredU3Ek__BackingField_15(bool value)
	{
		___U3CisPreferredU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTE_T660843562_H
#ifndef INPUTMODE_T4136831962_H
#define INPUTMODE_T4136831962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MiraInputModule/InputMode
struct  InputMode_t4136831962 
{
public:
	// System.Int32 UnityEngine.EventSystems.MiraInputModule/InputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputMode_t4136831962, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMODE_T4136831962_H
#ifndef RAYCASTRESULT_T21186376_H
#define RAYCASTRESULT_T21186376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t21186376 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t2336171397 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2243707580  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2243707580  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2243707579  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___m_GameObject_0)); }
	inline GameObject_t1756533147 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1756533147 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___module_1)); }
	inline BaseRaycaster_t2336171397 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t2336171397 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t2336171397 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldPosition_7)); }
	inline Vector3_t2243707580  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2243707580 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2243707580  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldNormal_8)); }
	inline Vector3_t2243707580  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2243707580 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2243707580  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___screenPosition_9)); }
	inline Vector2_t2243707579  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2243707579 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2243707579  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_pinvoke
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_com
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T21186376_H
#ifndef WORDSELECTIONEVENT_T2871480376_H
#define WORDSELECTIONEVENT_T2871480376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_t2871480376  : public UnityEvent_3_t2425689862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_T2871480376_H
#ifndef FRAMEPRESSSTATE_T1414739712_H
#define FRAMEPRESSSTATE_T1414739712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/FramePressState
struct  FramePressState_t1414739712 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/FramePressState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FramePressState_t1414739712, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEPRESSSTATE_T1414739712_H
#ifndef STREAMINGCONTEXTSTATES_T4264247603_H
#define STREAMINGCONTEXTSTATES_T4264247603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t4264247603 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t4264247603, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T4264247603_H
#ifndef MOUSEBUTTONEVENTDATA_T3709210170_H
#define MOUSEBUTTONEVENTDATA_T3709210170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct  MouseButtonEventData_t3709210170  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonData
	PointerEventData_t1599784723 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3709210170, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3709210170, ___buttonData_1)); }
	inline PointerEventData_t1599784723 * get_buttonData_1() const { return ___buttonData_1; }
	inline PointerEventData_t1599784723 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PointerEventData_t1599784723 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttonData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONEVENTDATA_T3709210170_H
#ifndef POINTEREVENTDATA_T1599784723_H
#define POINTEREVENTDATA_T1599784723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t1599784723  : public BaseEventData_t2681005625
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1756533147 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1756533147 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1756533147 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1756533147 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1756533147 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t21186376  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t21186376  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t1125654279 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2243707579  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2243707579  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2243707579  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t2243707580  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t2243707580  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2243707579  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1756533147 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1756533147 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1756533147 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___m_PointerPress_3)); }
	inline GameObject_t1756533147 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1756533147 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1756533147 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1756533147 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1756533147 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1756533147 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1756533147 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1756533147 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1756533147 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1756533147 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1756533147 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t21186376  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t21186376 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t21186376  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t21186376  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t21186376 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t21186376  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___hovered_9)); }
	inline List_1_t1125654279 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t1125654279 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t1125654279 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2243707579  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2243707579 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2243707579  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2243707579  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2243707579 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2243707579  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2243707579  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2243707579 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2243707579  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t2243707580  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t2243707580 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t2243707580  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t2243707580  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t2243707580 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t2243707580  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2243707579  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2243707579 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2243707579  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T1599784723_H
#ifndef BUTTONSTATE_T2688375492_H
#define BUTTONSTATE_T2688375492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct  ButtonState_t2688375492  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerInputModule/ButtonState::m_Button
	int32_t ___m_Button_0;
	// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData UnityEngine.EventSystems.PointerInputModule/ButtonState::m_EventData
	MouseButtonEventData_t3709210170 * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_t2688375492, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_t2688375492, ___m_EventData_1)); }
	inline MouseButtonEventData_t3709210170 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_t3709210170 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_t3709210170 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATE_T2688375492_H
#ifndef AXISEVENTDATA_T1524870173_H
#define AXISEVENTDATA_T1524870173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t1524870173  : public BaseEventData_t2681005625
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_t2243707579  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t1524870173, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_t2243707579  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_t2243707579 * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_t2243707579  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t1524870173, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENTDATA_T1524870173_H
#ifndef STREAMINGCONTEXT_T1417235061_H
#define STREAMINGCONTEXT_T1417235061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t1417235061 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t1417235061, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t1417235061, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1417235061_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1417235061_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T1417235061_H
#ifndef TEXTURE_T2243626319_H
#define TEXTURE_T2243626319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2243626319  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2243626319_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef REMOTEDISCONNECTEDEVENTHANDLER_T730656759_H
#define REMOTEDISCONNECTEDEVENTHANDLER_T730656759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/RemoteDisconnectedEventHandler
struct  RemoteDisconnectedEventHandler_t730656759  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEDISCONNECTEDEVENTHANDLER_T730656759_H
#ifndef EVENTFUNCTION_1_T1109076156_H
#define EVENTFUNCTION_1_T1109076156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct  EventFunction_1_t1109076156  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T1109076156_H
#ifndef REMOTECONNECTEDEVENTHANDLER_T3456249665_H
#define REMOTECONNECTEDEVENTHANDLER_T3456249665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager/RemoteConnectedEventHandler
struct  RemoteConnectedEventHandler_t3456249665  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTECONNECTEDEVENTHANDLER_T3456249665_H
#ifndef EVENTFUNCTION_1_T477470301_H
#define EVENTFUNCTION_1_T477470301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct  EventFunction_1_t477470301  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T477470301_H
#ifndef EVENTFUNCTION_1_T3317921847_H
#define EVENTFUNCTION_1_T3317921847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct  EventFunction_1_t3317921847  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T3317921847_H
#ifndef UNITYACTION_1_T3438463199_H
#define UNITYACTION_1_T3438463199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Int32>
struct  UnityAction_1_t3438463199  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3438463199_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef UNITYACTION_1_T3381592573_H
#define UNITYACTION_1_T3381592573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<Wikitude.ImageTarget>
struct  UnityAction_1_t3381592573  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3381592573_H
#ifndef PLAYERCONNECTION_T3517219175_H
#define PLAYERCONNECTION_T3517219175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct  PlayerConnection_t3517219175  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents UnityEngine.Networking.PlayerConnection.PlayerConnection::m_PlayerEditorConnectionEvents
	PlayerEditorConnectionEvents_t2252784345 * ___m_PlayerEditorConnectionEvents_3;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Networking.PlayerConnection.PlayerConnection::m_connectedPlayers
	List_1_t1440998580 * ___m_connectedPlayers_4;
	// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::m_IsInitilized
	bool ___m_IsInitilized_5;

public:
	inline static int32_t get_offset_of_m_PlayerEditorConnectionEvents_3() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175, ___m_PlayerEditorConnectionEvents_3)); }
	inline PlayerEditorConnectionEvents_t2252784345 * get_m_PlayerEditorConnectionEvents_3() const { return ___m_PlayerEditorConnectionEvents_3; }
	inline PlayerEditorConnectionEvents_t2252784345 ** get_address_of_m_PlayerEditorConnectionEvents_3() { return &___m_PlayerEditorConnectionEvents_3; }
	inline void set_m_PlayerEditorConnectionEvents_3(PlayerEditorConnectionEvents_t2252784345 * value)
	{
		___m_PlayerEditorConnectionEvents_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerEditorConnectionEvents_3), value);
	}

	inline static int32_t get_offset_of_m_connectedPlayers_4() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175, ___m_connectedPlayers_4)); }
	inline List_1_t1440998580 * get_m_connectedPlayers_4() const { return ___m_connectedPlayers_4; }
	inline List_1_t1440998580 ** get_address_of_m_connectedPlayers_4() { return &___m_connectedPlayers_4; }
	inline void set_m_connectedPlayers_4(List_1_t1440998580 * value)
	{
		___m_connectedPlayers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectedPlayers_4), value);
	}

	inline static int32_t get_offset_of_m_IsInitilized_5() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175, ___m_IsInitilized_5)); }
	inline bool get_m_IsInitilized_5() const { return ___m_IsInitilized_5; }
	inline bool* get_address_of_m_IsInitilized_5() { return &___m_IsInitilized_5; }
	inline void set_m_IsInitilized_5(bool value)
	{
		___m_IsInitilized_5 = value;
	}
};

struct PlayerConnection_t3517219175_StaticFields
{
public:
	// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::connectionNative
	RuntimeObject* ___connectionNative_2;
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::s_Instance
	PlayerConnection_t3517219175 * ___s_Instance_6;

public:
	inline static int32_t get_offset_of_connectionNative_2() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175_StaticFields, ___connectionNative_2)); }
	inline RuntimeObject* get_connectionNative_2() const { return ___connectionNative_2; }
	inline RuntimeObject** get_address_of_connectionNative_2() { return &___connectionNative_2; }
	inline void set_connectionNative_2(RuntimeObject* value)
	{
		___connectionNative_2 = value;
		Il2CppCodeGenWriteBarrier((&___connectionNative_2), value);
	}

	inline static int32_t get_offset_of_s_Instance_6() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175_StaticFields, ___s_Instance_6)); }
	inline PlayerConnection_t3517219175 * get_s_Instance_6() const { return ___s_Instance_6; }
	inline PlayerConnection_t3517219175 ** get_address_of_s_Instance_6() { return &___s_Instance_6; }
	inline void set_s_Instance_6(PlayerConnection_t3517219175 * value)
	{
		___s_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTION_T3517219175_H
#ifndef TEXTURE2D_T3542995729_H
#define TEXTURE2D_T3542995729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3542995729  : public Texture_t2243626319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3542995729_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef UNITYACTION_1_T1667869373_H
#define UNITYACTION_1_T1667869373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityAction_1_t1667869373  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1667869373_H
#ifndef BINARYFORMATTER_T1866979105_H
#define BINARYFORMATTER_T1866979105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct  BinaryFormatter_t1866979105  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::assembly_format
	int32_t ___assembly_format_0;
	// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::binder
	SerializationBinder_t3985864818 * ___binder_1;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::context
	StreamingContext_t1417235061  ___context_2;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::surrogate_selector
	RuntimeObject* ___surrogate_selector_3;
	// System.Runtime.Serialization.Formatters.FormatterTypeStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::type_format
	int32_t ___type_format_4;
	// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::filter_level
	int32_t ___filter_level_5;

public:
	inline static int32_t get_offset_of_assembly_format_0() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105, ___assembly_format_0)); }
	inline int32_t get_assembly_format_0() const { return ___assembly_format_0; }
	inline int32_t* get_address_of_assembly_format_0() { return &___assembly_format_0; }
	inline void set_assembly_format_0(int32_t value)
	{
		___assembly_format_0 = value;
	}

	inline static int32_t get_offset_of_binder_1() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105, ___binder_1)); }
	inline SerializationBinder_t3985864818 * get_binder_1() const { return ___binder_1; }
	inline SerializationBinder_t3985864818 ** get_address_of_binder_1() { return &___binder_1; }
	inline void set_binder_1(SerializationBinder_t3985864818 * value)
	{
		___binder_1 = value;
		Il2CppCodeGenWriteBarrier((&___binder_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105, ___context_2)); }
	inline StreamingContext_t1417235061  get_context_2() const { return ___context_2; }
	inline StreamingContext_t1417235061 * get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(StreamingContext_t1417235061  value)
	{
		___context_2 = value;
	}

	inline static int32_t get_offset_of_surrogate_selector_3() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105, ___surrogate_selector_3)); }
	inline RuntimeObject* get_surrogate_selector_3() const { return ___surrogate_selector_3; }
	inline RuntimeObject** get_address_of_surrogate_selector_3() { return &___surrogate_selector_3; }
	inline void set_surrogate_selector_3(RuntimeObject* value)
	{
		___surrogate_selector_3 = value;
		Il2CppCodeGenWriteBarrier((&___surrogate_selector_3), value);
	}

	inline static int32_t get_offset_of_type_format_4() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105, ___type_format_4)); }
	inline int32_t get_type_format_4() const { return ___type_format_4; }
	inline int32_t* get_address_of_type_format_4() { return &___type_format_4; }
	inline void set_type_format_4(int32_t value)
	{
		___type_format_4 = value;
	}

	inline static int32_t get_offset_of_filter_level_5() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105, ___filter_level_5)); }
	inline int32_t get_filter_level_5() const { return ___filter_level_5; }
	inline int32_t* get_address_of_filter_level_5() { return &___filter_level_5; }
	inline void set_filter_level_5(int32_t value)
	{
		___filter_level_5 = value;
	}
};

struct BinaryFormatter_t1866979105_StaticFields
{
public:
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::<DefaultSurrogateSelector>k__BackingField
	RuntimeObject* ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CDefaultSurrogateSelectorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BinaryFormatter_t1866979105_StaticFields, ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CDefaultSurrogateSelectorU3Ek__BackingField_6() const { return ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CDefaultSurrogateSelectorU3Ek__BackingField_6() { return &___U3CDefaultSurrogateSelectorU3Ek__BackingField_6; }
	inline void set_U3CDefaultSurrogateSelectorU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CDefaultSurrogateSelectorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultSurrogateSelectorU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYFORMATTER_T1866979105_H
#ifndef EVENTFUNCTION_1_T2888287612_H
#define EVENTFUNCTION_1_T2888287612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct  EventFunction_1_t2888287612  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T2888287612_H
#ifndef EVENTFUNCTION_1_T887251860_H
#define EVENTFUNCTION_1_T887251860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct  EventFunction_1_t887251860  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T887251860_H
#ifndef EVENTFUNCTION_1_T344915111_H
#define EVENTFUNCTION_1_T344915111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct  EventFunction_1_t344915111  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T344915111_H
#ifndef EVENTFUNCTION_1_T1847959737_H
#define EVENTFUNCTION_1_T1847959737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct  EventFunction_1_t1847959737  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T1847959737_H
#ifndef EVENTFUNCTION_1_T2426197568_H
#define EVENTFUNCTION_1_T2426197568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct  EventFunction_1_t2426197568  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T2426197568_H
#ifndef EVENTFUNCTION_1_T2276060003_H
#define EVENTFUNCTION_1_T2276060003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct  EventFunction_1_t2276060003  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T2276060003_H
#ifndef EVENTFUNCTION_1_T3253137806_H
#define EVENTFUNCTION_1_T3253137806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct  EventFunction_1_t3253137806  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T3253137806_H
#ifndef EVENTFUNCTION_1_T4141241546_H
#define EVENTFUNCTION_1_T4141241546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct  EventFunction_1_t4141241546  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T4141241546_H
#ifndef EVENTFUNCTION_1_T2331828160_H
#define EVENTFUNCTION_1_T2331828160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct  EventFunction_1_t2331828160  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T2331828160_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef MIRAARVIDEO_T1477805071_H
#define MIRAARVIDEO_T1477805071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraARVideo
struct  MiraARVideo_t1477805071  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D MiraARVideo::m_clearTexture
	Texture2D_t3542995729 * ___m_clearTexture_2;

public:
	inline static int32_t get_offset_of_m_clearTexture_2() { return static_cast<int32_t>(offsetof(MiraARVideo_t1477805071, ___m_clearTexture_2)); }
	inline Texture2D_t3542995729 * get_m_clearTexture_2() const { return ___m_clearTexture_2; }
	inline Texture2D_t3542995729 ** get_address_of_m_clearTexture_2() { return &___m_clearTexture_2; }
	inline void set_m_clearTexture_2(Texture2D_t3542995729 * value)
	{
		___m_clearTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_clearTexture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAARVIDEO_T1477805071_H
#ifndef TRACKABLE_T1596815325_H
#define TRACKABLE_T1596815325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.Trackable
struct  Trackable_t1596815325  : public MonoBehaviour_t1158329972
{
public:
	// System.String Wikitude.Trackable::_targetPattern
	String_t* ____targetPattern_2;
	// System.Text.RegularExpressions.Regex Wikitude.Trackable::_targetPatternRegex
	Regex_t1803876613 * ____targetPatternRegex_3;
	// UnityEngine.GameObject Wikitude.Trackable::_drawable
	GameObject_t1756533147 * ____drawable_4;
	// System.Boolean Wikitude.Trackable::_autoToggleVisibility
	bool ____autoToggleVisibility_5;
	// System.Boolean Wikitude.Trackable::_registeredToTracker
	bool ____registeredToTracker_6;
	// Wikitude.TrackerBehaviour Wikitude.Trackable::_tracker
	TrackerBehaviour_t3845512381 * ____tracker_7;
	// System.Collections.Generic.Dictionary`2<Wikitude.RecognizedTarget,UnityEngine.GameObject> Wikitude.Trackable::_activeDrawables
	Dictionary_2_t3102997329 * ____activeDrawables_8;
	// System.Boolean Wikitude.Trackable::UpdatedTransform
	bool ___UpdatedTransform_9;

public:
	inline static int32_t get_offset_of__targetPattern_2() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____targetPattern_2)); }
	inline String_t* get__targetPattern_2() const { return ____targetPattern_2; }
	inline String_t** get_address_of__targetPattern_2() { return &____targetPattern_2; }
	inline void set__targetPattern_2(String_t* value)
	{
		____targetPattern_2 = value;
		Il2CppCodeGenWriteBarrier((&____targetPattern_2), value);
	}

	inline static int32_t get_offset_of__targetPatternRegex_3() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____targetPatternRegex_3)); }
	inline Regex_t1803876613 * get__targetPatternRegex_3() const { return ____targetPatternRegex_3; }
	inline Regex_t1803876613 ** get_address_of__targetPatternRegex_3() { return &____targetPatternRegex_3; }
	inline void set__targetPatternRegex_3(Regex_t1803876613 * value)
	{
		____targetPatternRegex_3 = value;
		Il2CppCodeGenWriteBarrier((&____targetPatternRegex_3), value);
	}

	inline static int32_t get_offset_of__drawable_4() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____drawable_4)); }
	inline GameObject_t1756533147 * get__drawable_4() const { return ____drawable_4; }
	inline GameObject_t1756533147 ** get_address_of__drawable_4() { return &____drawable_4; }
	inline void set__drawable_4(GameObject_t1756533147 * value)
	{
		____drawable_4 = value;
		Il2CppCodeGenWriteBarrier((&____drawable_4), value);
	}

	inline static int32_t get_offset_of__autoToggleVisibility_5() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____autoToggleVisibility_5)); }
	inline bool get__autoToggleVisibility_5() const { return ____autoToggleVisibility_5; }
	inline bool* get_address_of__autoToggleVisibility_5() { return &____autoToggleVisibility_5; }
	inline void set__autoToggleVisibility_5(bool value)
	{
		____autoToggleVisibility_5 = value;
	}

	inline static int32_t get_offset_of__registeredToTracker_6() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____registeredToTracker_6)); }
	inline bool get__registeredToTracker_6() const { return ____registeredToTracker_6; }
	inline bool* get_address_of__registeredToTracker_6() { return &____registeredToTracker_6; }
	inline void set__registeredToTracker_6(bool value)
	{
		____registeredToTracker_6 = value;
	}

	inline static int32_t get_offset_of__tracker_7() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____tracker_7)); }
	inline TrackerBehaviour_t3845512381 * get__tracker_7() const { return ____tracker_7; }
	inline TrackerBehaviour_t3845512381 ** get_address_of__tracker_7() { return &____tracker_7; }
	inline void set__tracker_7(TrackerBehaviour_t3845512381 * value)
	{
		____tracker_7 = value;
		Il2CppCodeGenWriteBarrier((&____tracker_7), value);
	}

	inline static int32_t get_offset_of__activeDrawables_8() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ____activeDrawables_8)); }
	inline Dictionary_2_t3102997329 * get__activeDrawables_8() const { return ____activeDrawables_8; }
	inline Dictionary_2_t3102997329 ** get_address_of__activeDrawables_8() { return &____activeDrawables_8; }
	inline void set__activeDrawables_8(Dictionary_2_t3102997329 * value)
	{
		____activeDrawables_8 = value;
		Il2CppCodeGenWriteBarrier((&____activeDrawables_8), value);
	}

	inline static int32_t get_offset_of_UpdatedTransform_9() { return static_cast<int32_t>(offsetof(Trackable_t1596815325, ___UpdatedTransform_9)); }
	inline bool get_UpdatedTransform_9() const { return ___UpdatedTransform_9; }
	inline bool* get_address_of_UpdatedTransform_9() { return &___UpdatedTransform_9; }
	inline void set_UpdatedTransform_9(bool value)
	{
		___UpdatedTransform_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLE_T1596815325_H
#ifndef TRACKERLERPDRIVER_T1630291735_H
#define TRACKERLERPDRIVER_T1630291735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackerLerpDriver
struct  TrackerLerpDriver_t1630291735  : public MonoBehaviour_t1158329972
{
public:
	// MarkerRotationWatcher TrackerLerpDriver::rotationWatcher
	MarkerRotationWatcher_t3969903492 * ___rotationWatcher_2;
	// UnityEngine.Vector3 TrackerLerpDriver::startPosition
	Vector3_t2243707580  ___startPosition_3;
	// UnityEngine.Vector3 TrackerLerpDriver::endPosition
	Vector3_t2243707580  ___endPosition_4;

public:
	inline static int32_t get_offset_of_rotationWatcher_2() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t1630291735, ___rotationWatcher_2)); }
	inline MarkerRotationWatcher_t3969903492 * get_rotationWatcher_2() const { return ___rotationWatcher_2; }
	inline MarkerRotationWatcher_t3969903492 ** get_address_of_rotationWatcher_2() { return &___rotationWatcher_2; }
	inline void set_rotationWatcher_2(MarkerRotationWatcher_t3969903492 * value)
	{
		___rotationWatcher_2 = value;
		Il2CppCodeGenWriteBarrier((&___rotationWatcher_2), value);
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t1630291735, ___startPosition_3)); }
	inline Vector3_t2243707580  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t2243707580 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t2243707580  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_endPosition_4() { return static_cast<int32_t>(offsetof(TrackerLerpDriver_t1630291735, ___endPosition_4)); }
	inline Vector3_t2243707580  get_endPosition_4() const { return ___endPosition_4; }
	inline Vector3_t2243707580 * get_address_of_endPosition_4() { return &___endPosition_4; }
	inline void set_endPosition_4(Vector3_t2243707580  value)
	{
		___endPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERLERPDRIVER_T1630291735_H
#ifndef MIRALIVEPREVIEWWIKICONFIG_T2812595783_H
#define MIRALIVEPREVIEWWIKICONFIG_T2812595783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraLivePreviewWikiConfig
struct  MiraLivePreviewWikiConfig_t2812595783  : public MonoBehaviour_t1158329972
{
public:
	// Wikitude.WikitudeCamera MiraLivePreviewWikiConfig::ArCam
	WikitudeCamera_t2517845841 * ___ArCam_2;

public:
	inline static int32_t get_offset_of_ArCam_2() { return static_cast<int32_t>(offsetof(MiraLivePreviewWikiConfig_t2812595783, ___ArCam_2)); }
	inline WikitudeCamera_t2517845841 * get_ArCam_2() const { return ___ArCam_2; }
	inline WikitudeCamera_t2517845841 ** get_address_of_ArCam_2() { return &___ArCam_2; }
	inline void set_ArCam_2(WikitudeCamera_t2517845841 * value)
	{
		___ArCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___ArCam_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWWIKICONFIG_T2812595783_H
#ifndef DEMOSCENEMANAGER_T779426248_H
#define DEMOSCENEMANAGER_T779426248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoSceneManager
struct  DemoSceneManager_t779426248  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct DemoSceneManager_t779426248_StaticFields
{
public:
	// System.Boolean DemoSceneManager::isSpectator
	bool ___isSpectator_2;
	// DemoSceneManager DemoSceneManager::instance
	DemoSceneManager_t779426248 * ___instance_3;

public:
	inline static int32_t get_offset_of_isSpectator_2() { return static_cast<int32_t>(offsetof(DemoSceneManager_t779426248_StaticFields, ___isSpectator_2)); }
	inline bool get_isSpectator_2() const { return ___isSpectator_2; }
	inline bool* get_address_of_isSpectator_2() { return &___isSpectator_2; }
	inline void set_isSpectator_2(bool value)
	{
		___isSpectator_2 = value;
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(DemoSceneManager_t779426248_StaticFields, ___instance_3)); }
	inline DemoSceneManager_t779426248 * get_instance_3() const { return ___instance_3; }
	inline DemoSceneManager_t779426248 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(DemoSceneManager_t779426248 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCENEMANAGER_T779426248_H
#ifndef MIRAARCONTROLLER_T555016598_H
#define MIRAARCONTROLLER_T555016598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mira.MiraArController
struct  MiraArController_t555016598  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mira.MiraArController::IPD
	float ___IPD_2;
	// UnityEngine.GameObject Mira.MiraArController::leftCam
	GameObject_t1756533147 * ___leftCam_3;
	// UnityEngine.GameObject Mira.MiraArController::rightCam
	GameObject_t1756533147 * ___rightCam_4;
	// UnityEngine.GameObject Mira.MiraArController::cameraRig
	GameObject_t1756533147 * ___cameraRig_5;
	// System.Boolean Mira.MiraArController::isRotationalOnly
	bool ___isRotationalOnly_6;
	// System.Boolean Mira.MiraArController::isSpectator
	bool ___isSpectator_7;
	// System.Single Mira.MiraArController::nearClipPlane
	float ___nearClipPlane_8;
	// System.Single Mira.MiraArController::farClipPlane
	float ___farClipPlane_9;
	// System.Boolean Mira.MiraArController::defaultScale
	bool ___defaultScale_10;
	// System.Single Mira.MiraArController::setScaleMultiplier
	float ___setScaleMultiplier_11;
	// System.Single Mira.MiraArController::stereoCamFov
	float ___stereoCamFov_13;
	// Mira.MiraViewer Mira.MiraArController::mv
	MiraViewer_t1267122109 * ___mv_14;
	// UnityEngine.Texture Mira.MiraArController::btnTexture
	Texture_t2243626319 * ___btnTexture_16;
	// System.Single Mira.MiraArController::buttonHeight
	float ___buttonHeight_17;
	// System.Single Mira.MiraArController::buttonWidth
	float ___buttonWidth_18;
	// UnityEngine.GUISkin Mira.MiraArController::MiraGuiSkin
	GUISkin_t1436893342 * ___MiraGuiSkin_19;
	// UnityEngine.EventSystems.MiraInputModule Mira.MiraArController::miraInputModule
	MiraInputModule_t2476427099 * ___miraInputModule_20;
	// System.Boolean Mira.MiraArController::GUIEnabled
	bool ___GUIEnabled_21;
	// UnityEngine.GameObject Mira.MiraArController::settingsMenu
	GameObject_t1756533147 * ___settingsMenu_22;

public:
	inline static int32_t get_offset_of_IPD_2() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___IPD_2)); }
	inline float get_IPD_2() const { return ___IPD_2; }
	inline float* get_address_of_IPD_2() { return &___IPD_2; }
	inline void set_IPD_2(float value)
	{
		___IPD_2 = value;
	}

	inline static int32_t get_offset_of_leftCam_3() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___leftCam_3)); }
	inline GameObject_t1756533147 * get_leftCam_3() const { return ___leftCam_3; }
	inline GameObject_t1756533147 ** get_address_of_leftCam_3() { return &___leftCam_3; }
	inline void set_leftCam_3(GameObject_t1756533147 * value)
	{
		___leftCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___leftCam_3), value);
	}

	inline static int32_t get_offset_of_rightCam_4() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___rightCam_4)); }
	inline GameObject_t1756533147 * get_rightCam_4() const { return ___rightCam_4; }
	inline GameObject_t1756533147 ** get_address_of_rightCam_4() { return &___rightCam_4; }
	inline void set_rightCam_4(GameObject_t1756533147 * value)
	{
		___rightCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___rightCam_4), value);
	}

	inline static int32_t get_offset_of_cameraRig_5() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___cameraRig_5)); }
	inline GameObject_t1756533147 * get_cameraRig_5() const { return ___cameraRig_5; }
	inline GameObject_t1756533147 ** get_address_of_cameraRig_5() { return &___cameraRig_5; }
	inline void set_cameraRig_5(GameObject_t1756533147 * value)
	{
		___cameraRig_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_5), value);
	}

	inline static int32_t get_offset_of_isRotationalOnly_6() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___isRotationalOnly_6)); }
	inline bool get_isRotationalOnly_6() const { return ___isRotationalOnly_6; }
	inline bool* get_address_of_isRotationalOnly_6() { return &___isRotationalOnly_6; }
	inline void set_isRotationalOnly_6(bool value)
	{
		___isRotationalOnly_6 = value;
	}

	inline static int32_t get_offset_of_isSpectator_7() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___isSpectator_7)); }
	inline bool get_isSpectator_7() const { return ___isSpectator_7; }
	inline bool* get_address_of_isSpectator_7() { return &___isSpectator_7; }
	inline void set_isSpectator_7(bool value)
	{
		___isSpectator_7 = value;
	}

	inline static int32_t get_offset_of_nearClipPlane_8() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___nearClipPlane_8)); }
	inline float get_nearClipPlane_8() const { return ___nearClipPlane_8; }
	inline float* get_address_of_nearClipPlane_8() { return &___nearClipPlane_8; }
	inline void set_nearClipPlane_8(float value)
	{
		___nearClipPlane_8 = value;
	}

	inline static int32_t get_offset_of_farClipPlane_9() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___farClipPlane_9)); }
	inline float get_farClipPlane_9() const { return ___farClipPlane_9; }
	inline float* get_address_of_farClipPlane_9() { return &___farClipPlane_9; }
	inline void set_farClipPlane_9(float value)
	{
		___farClipPlane_9 = value;
	}

	inline static int32_t get_offset_of_defaultScale_10() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___defaultScale_10)); }
	inline bool get_defaultScale_10() const { return ___defaultScale_10; }
	inline bool* get_address_of_defaultScale_10() { return &___defaultScale_10; }
	inline void set_defaultScale_10(bool value)
	{
		___defaultScale_10 = value;
	}

	inline static int32_t get_offset_of_setScaleMultiplier_11() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___setScaleMultiplier_11)); }
	inline float get_setScaleMultiplier_11() const { return ___setScaleMultiplier_11; }
	inline float* get_address_of_setScaleMultiplier_11() { return &___setScaleMultiplier_11; }
	inline void set_setScaleMultiplier_11(float value)
	{
		___setScaleMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_stereoCamFov_13() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___stereoCamFov_13)); }
	inline float get_stereoCamFov_13() const { return ___stereoCamFov_13; }
	inline float* get_address_of_stereoCamFov_13() { return &___stereoCamFov_13; }
	inline void set_stereoCamFov_13(float value)
	{
		___stereoCamFov_13 = value;
	}

	inline static int32_t get_offset_of_mv_14() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___mv_14)); }
	inline MiraViewer_t1267122109 * get_mv_14() const { return ___mv_14; }
	inline MiraViewer_t1267122109 ** get_address_of_mv_14() { return &___mv_14; }
	inline void set_mv_14(MiraViewer_t1267122109 * value)
	{
		___mv_14 = value;
		Il2CppCodeGenWriteBarrier((&___mv_14), value);
	}

	inline static int32_t get_offset_of_btnTexture_16() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___btnTexture_16)); }
	inline Texture_t2243626319 * get_btnTexture_16() const { return ___btnTexture_16; }
	inline Texture_t2243626319 ** get_address_of_btnTexture_16() { return &___btnTexture_16; }
	inline void set_btnTexture_16(Texture_t2243626319 * value)
	{
		___btnTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___btnTexture_16), value);
	}

	inline static int32_t get_offset_of_buttonHeight_17() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___buttonHeight_17)); }
	inline float get_buttonHeight_17() const { return ___buttonHeight_17; }
	inline float* get_address_of_buttonHeight_17() { return &___buttonHeight_17; }
	inline void set_buttonHeight_17(float value)
	{
		___buttonHeight_17 = value;
	}

	inline static int32_t get_offset_of_buttonWidth_18() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___buttonWidth_18)); }
	inline float get_buttonWidth_18() const { return ___buttonWidth_18; }
	inline float* get_address_of_buttonWidth_18() { return &___buttonWidth_18; }
	inline void set_buttonWidth_18(float value)
	{
		___buttonWidth_18 = value;
	}

	inline static int32_t get_offset_of_MiraGuiSkin_19() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___MiraGuiSkin_19)); }
	inline GUISkin_t1436893342 * get_MiraGuiSkin_19() const { return ___MiraGuiSkin_19; }
	inline GUISkin_t1436893342 ** get_address_of_MiraGuiSkin_19() { return &___MiraGuiSkin_19; }
	inline void set_MiraGuiSkin_19(GUISkin_t1436893342 * value)
	{
		___MiraGuiSkin_19 = value;
		Il2CppCodeGenWriteBarrier((&___MiraGuiSkin_19), value);
	}

	inline static int32_t get_offset_of_miraInputModule_20() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___miraInputModule_20)); }
	inline MiraInputModule_t2476427099 * get_miraInputModule_20() const { return ___miraInputModule_20; }
	inline MiraInputModule_t2476427099 ** get_address_of_miraInputModule_20() { return &___miraInputModule_20; }
	inline void set_miraInputModule_20(MiraInputModule_t2476427099 * value)
	{
		___miraInputModule_20 = value;
		Il2CppCodeGenWriteBarrier((&___miraInputModule_20), value);
	}

	inline static int32_t get_offset_of_GUIEnabled_21() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___GUIEnabled_21)); }
	inline bool get_GUIEnabled_21() const { return ___GUIEnabled_21; }
	inline bool* get_address_of_GUIEnabled_21() { return &___GUIEnabled_21; }
	inline void set_GUIEnabled_21(bool value)
	{
		___GUIEnabled_21 = value;
	}

	inline static int32_t get_offset_of_settingsMenu_22() { return static_cast<int32_t>(offsetof(MiraArController_t555016598, ___settingsMenu_22)); }
	inline GameObject_t1756533147 * get_settingsMenu_22() const { return ___settingsMenu_22; }
	inline GameObject_t1756533147 ** get_address_of_settingsMenu_22() { return &___settingsMenu_22; }
	inline void set_settingsMenu_22(GameObject_t1756533147 * value)
	{
		___settingsMenu_22 = value;
		Il2CppCodeGenWriteBarrier((&___settingsMenu_22), value);
	}
};

struct MiraArController_t555016598_StaticFields
{
public:
	// System.Single Mira.MiraArController::scaleMultiplier
	float ___scaleMultiplier_12;
	// Mira.MiraArController Mira.MiraArController::instance
	MiraArController_t555016598 * ___instance_15;

public:
	inline static int32_t get_offset_of_scaleMultiplier_12() { return static_cast<int32_t>(offsetof(MiraArController_t555016598_StaticFields, ___scaleMultiplier_12)); }
	inline float get_scaleMultiplier_12() const { return ___scaleMultiplier_12; }
	inline float* get_address_of_scaleMultiplier_12() { return &___scaleMultiplier_12; }
	inline void set_scaleMultiplier_12(float value)
	{
		___scaleMultiplier_12 = value;
	}

	inline static int32_t get_offset_of_instance_15() { return static_cast<int32_t>(offsetof(MiraArController_t555016598_StaticFields, ___instance_15)); }
	inline MiraArController_t555016598 * get_instance_15() const { return ___instance_15; }
	inline MiraArController_t555016598 ** get_address_of_instance_15() { return &___instance_15; }
	inline void set_instance_15(MiraArController_t555016598 * value)
	{
		___instance_15 = value;
		Il2CppCodeGenWriteBarrier((&___instance_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAARCONTROLLER_T555016598_H
#ifndef TOGGLEMODES_T4125091540_H
#define TOGGLEMODES_T4125091540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleModes
struct  ToggleModes_t4125091540  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEMODES_T4125091540_H
#ifndef MARKERROTATIONWATCHER_T3969903492_H
#define MARKERROTATIONWATCHER_T3969903492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerRotationWatcher
struct  MarkerRotationWatcher_t3969903492  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MarkerRotationWatcher::mainCamera
	Transform_t3275118058 * ___mainCamera_2;
	// System.Single MarkerRotationWatcher::deltaDotProduct
	float ___deltaDotProduct_3;
	// System.Single MarkerRotationWatcher::necessaryRotation
	float ___necessaryRotation_4;
	// System.Single MarkerRotationWatcher::dot
	float ___dot_5;
	// UnityEngine.Vector3 MarkerRotationWatcher::startVector
	Vector3_t2243707580  ___startVector_7;
	// System.Boolean MarkerRotationWatcher::firstAngle
	bool ___firstAngle_8;
	// System.Single MarkerRotationWatcher::counter
	float ___counter_9;
	// System.Boolean MarkerRotationWatcher::hasRotated
	bool ___hasRotated_10;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___mainCamera_2)); }
	inline Transform_t3275118058 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Transform_t3275118058 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Transform_t3275118058 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}

	inline static int32_t get_offset_of_deltaDotProduct_3() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___deltaDotProduct_3)); }
	inline float get_deltaDotProduct_3() const { return ___deltaDotProduct_3; }
	inline float* get_address_of_deltaDotProduct_3() { return &___deltaDotProduct_3; }
	inline void set_deltaDotProduct_3(float value)
	{
		___deltaDotProduct_3 = value;
	}

	inline static int32_t get_offset_of_necessaryRotation_4() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___necessaryRotation_4)); }
	inline float get_necessaryRotation_4() const { return ___necessaryRotation_4; }
	inline float* get_address_of_necessaryRotation_4() { return &___necessaryRotation_4; }
	inline void set_necessaryRotation_4(float value)
	{
		___necessaryRotation_4 = value;
	}

	inline static int32_t get_offset_of_dot_5() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___dot_5)); }
	inline float get_dot_5() const { return ___dot_5; }
	inline float* get_address_of_dot_5() { return &___dot_5; }
	inline void set_dot_5(float value)
	{
		___dot_5 = value;
	}

	inline static int32_t get_offset_of_startVector_7() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___startVector_7)); }
	inline Vector3_t2243707580  get_startVector_7() const { return ___startVector_7; }
	inline Vector3_t2243707580 * get_address_of_startVector_7() { return &___startVector_7; }
	inline void set_startVector_7(Vector3_t2243707580  value)
	{
		___startVector_7 = value;
	}

	inline static int32_t get_offset_of_firstAngle_8() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___firstAngle_8)); }
	inline bool get_firstAngle_8() const { return ___firstAngle_8; }
	inline bool* get_address_of_firstAngle_8() { return &___firstAngle_8; }
	inline void set_firstAngle_8(bool value)
	{
		___firstAngle_8 = value;
	}

	inline static int32_t get_offset_of_counter_9() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___counter_9)); }
	inline float get_counter_9() const { return ___counter_9; }
	inline float* get_address_of_counter_9() { return &___counter_9; }
	inline void set_counter_9(float value)
	{
		___counter_9 = value;
	}

	inline static int32_t get_offset_of_hasRotated_10() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492, ___hasRotated_10)); }
	inline bool get_hasRotated_10() const { return ___hasRotated_10; }
	inline bool* get_address_of_hasRotated_10() { return &___hasRotated_10; }
	inline void set_hasRotated_10(bool value)
	{
		___hasRotated_10 = value;
	}
};

struct MarkerRotationWatcher_t3969903492_StaticFields
{
public:
	// MarkerRotationWatcher/RotateAction MarkerRotationWatcher::OnRotated
	RotateAction_t2368064732 * ___OnRotated_6;

public:
	inline static int32_t get_offset_of_OnRotated_6() { return static_cast<int32_t>(offsetof(MarkerRotationWatcher_t3969903492_StaticFields, ___OnRotated_6)); }
	inline RotateAction_t2368064732 * get_OnRotated_6() const { return ___OnRotated_6; }
	inline RotateAction_t2368064732 ** get_address_of_OnRotated_6() { return &___OnRotated_6; }
	inline void set_OnRotated_6(RotateAction_t2368064732 * value)
	{
		___OnRotated_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnRotated_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERROTATIONWATCHER_T3969903492_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef MIRALIVEPREVIEWVIDEO_T2362804972_H
#define MIRALIVEPREVIEWVIDEO_T2362804972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewVideo
struct  MiraLivePreviewVideo_t2362804972  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWVIDEO_T2362804972_H
#ifndef MIRALIVEPREVIEWPLAYER_T3516305084_H
#define MIRALIVEPREVIEWPLAYER_T3516305084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewPlayer
struct  MiraLivePreviewPlayer_t3516305084  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.MiraLivePreviewPlayer::playerConnection
	PlayerConnection_t3517219175 * ___playerConnection_2;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::bSessionActive
	bool ___bSessionActive_3;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::editorID
	int32_t ___editorID_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.MiraLivePreviewPlayer::liveViewScreenTex
	Texture2D_t3542995729 * ___liveViewScreenTex_5;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::bTexturesInitialized
	bool ___bTexturesInitialized_6;
	// System.Boolean UnityEngine.XR.iOS.MiraLivePreviewPlayer::isTracking
	bool ___isTracking_7;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::btFrameCounter
	int32_t ___btFrameCounter_9;
	// System.Int32 UnityEngine.XR.iOS.MiraLivePreviewPlayer::btSendRate
	int32_t ___btSendRate_10;

public:
	inline static int32_t get_offset_of_playerConnection_2() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___playerConnection_2)); }
	inline PlayerConnection_t3517219175 * get_playerConnection_2() const { return ___playerConnection_2; }
	inline PlayerConnection_t3517219175 ** get_address_of_playerConnection_2() { return &___playerConnection_2; }
	inline void set_playerConnection_2(PlayerConnection_t3517219175 * value)
	{
		___playerConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_2), value);
	}

	inline static int32_t get_offset_of_bSessionActive_3() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___bSessionActive_3)); }
	inline bool get_bSessionActive_3() const { return ___bSessionActive_3; }
	inline bool* get_address_of_bSessionActive_3() { return &___bSessionActive_3; }
	inline void set_bSessionActive_3(bool value)
	{
		___bSessionActive_3 = value;
	}

	inline static int32_t get_offset_of_editorID_4() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___editorID_4)); }
	inline int32_t get_editorID_4() const { return ___editorID_4; }
	inline int32_t* get_address_of_editorID_4() { return &___editorID_4; }
	inline void set_editorID_4(int32_t value)
	{
		___editorID_4 = value;
	}

	inline static int32_t get_offset_of_liveViewScreenTex_5() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___liveViewScreenTex_5)); }
	inline Texture2D_t3542995729 * get_liveViewScreenTex_5() const { return ___liveViewScreenTex_5; }
	inline Texture2D_t3542995729 ** get_address_of_liveViewScreenTex_5() { return &___liveViewScreenTex_5; }
	inline void set_liveViewScreenTex_5(Texture2D_t3542995729 * value)
	{
		___liveViewScreenTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___liveViewScreenTex_5), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_6() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___bTexturesInitialized_6)); }
	inline bool get_bTexturesInitialized_6() const { return ___bTexturesInitialized_6; }
	inline bool* get_address_of_bTexturesInitialized_6() { return &___bTexturesInitialized_6; }
	inline void set_bTexturesInitialized_6(bool value)
	{
		___bTexturesInitialized_6 = value;
	}

	inline static int32_t get_offset_of_isTracking_7() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___isTracking_7)); }
	inline bool get_isTracking_7() const { return ___isTracking_7; }
	inline bool* get_address_of_isTracking_7() { return &___isTracking_7; }
	inline void set_isTracking_7(bool value)
	{
		___isTracking_7 = value;
	}

	inline static int32_t get_offset_of_btFrameCounter_9() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___btFrameCounter_9)); }
	inline int32_t get_btFrameCounter_9() const { return ___btFrameCounter_9; }
	inline int32_t* get_address_of_btFrameCounter_9() { return &___btFrameCounter_9; }
	inline void set_btFrameCounter_9(int32_t value)
	{
		___btFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_btSendRate_10() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084, ___btSendRate_10)); }
	inline int32_t get_btSendRate_10() const { return ___btSendRate_10; }
	inline int32_t* get_address_of_btSendRate_10() { return &___btSendRate_10; }
	inline void set_btSendRate_10(int32_t value)
	{
		___btSendRate_10 = value;
	}
};

struct MiraLivePreviewPlayer_t3516305084_StaticFields
{
public:
	// MiraBTRemoteInput UnityEngine.XR.iOS.MiraLivePreviewPlayer::m_userInput
	MiraBTRemoteInput_t988911721 * ___m_userInput_8;

public:
	inline static int32_t get_offset_of_m_userInput_8() { return static_cast<int32_t>(offsetof(MiraLivePreviewPlayer_t3516305084_StaticFields, ___m_userInput_8)); }
	inline MiraBTRemoteInput_t988911721 * get_m_userInput_8() const { return ___m_userInput_8; }
	inline MiraBTRemoteInput_t988911721 ** get_address_of_m_userInput_8() { return &___m_userInput_8; }
	inline void set_m_userInput_8(MiraBTRemoteInput_t988911721 * value)
	{
		___m_userInput_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_userInput_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWPLAYER_T3516305084_H
#ifndef MIRALIVEPREVIEWEDITOR_T3425584316_H
#define MIRALIVEPREVIEWEDITOR_T3425584316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MiraLivePreviewEditor
struct  MiraLivePreviewEditor_t3425584316  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRALIVEPREVIEWEDITOR_T3425584316_H
#ifndef REMOTEMANAGER_T2208998541_H
#define REMOTEMANAGER_T2208998541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteManager
struct  RemoteManager_t2208998541  : public MonoBehaviour_t1158329972
{
public:
	// RemoteManager/RemoteConnectedEventHandler RemoteManager::OnRemoteConnected
	RemoteConnectedEventHandler_t3456249665 * ___OnRemoteConnected_2;
	// RemoteManager/RemoteDisconnectedEventHandler RemoteManager::OnRemoteDisconnected
	RemoteDisconnectedEventHandler_t730656759 * ___OnRemoteDisconnected_3;
	// System.Boolean RemoteManager::<isStarted>k__BackingField
	bool ___U3CisStartedU3Ek__BackingField_5;
	// System.Boolean RemoteManager::<isDiscoveringRemotes>k__BackingField
	bool ___U3CisDiscoveringRemotesU3Ek__BackingField_6;
	// Remote RemoteManager::_connectedRemote
	Remote_t660843562 * ____connectedRemote_7;
	// System.Collections.Generic.List`1<Remote> RemoteManager::_discoveredRemotes
	List_1_t29964694 * ____discoveredRemotes_8;

public:
	inline static int32_t get_offset_of_OnRemoteConnected_2() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___OnRemoteConnected_2)); }
	inline RemoteConnectedEventHandler_t3456249665 * get_OnRemoteConnected_2() const { return ___OnRemoteConnected_2; }
	inline RemoteConnectedEventHandler_t3456249665 ** get_address_of_OnRemoteConnected_2() { return &___OnRemoteConnected_2; }
	inline void set_OnRemoteConnected_2(RemoteConnectedEventHandler_t3456249665 * value)
	{
		___OnRemoteConnected_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteConnected_2), value);
	}

	inline static int32_t get_offset_of_OnRemoteDisconnected_3() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___OnRemoteDisconnected_3)); }
	inline RemoteDisconnectedEventHandler_t730656759 * get_OnRemoteDisconnected_3() const { return ___OnRemoteDisconnected_3; }
	inline RemoteDisconnectedEventHandler_t730656759 ** get_address_of_OnRemoteDisconnected_3() { return &___OnRemoteDisconnected_3; }
	inline void set_OnRemoteDisconnected_3(RemoteDisconnectedEventHandler_t730656759 * value)
	{
		___OnRemoteDisconnected_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteDisconnected_3), value);
	}

	inline static int32_t get_offset_of_U3CisStartedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___U3CisStartedU3Ek__BackingField_5)); }
	inline bool get_U3CisStartedU3Ek__BackingField_5() const { return ___U3CisStartedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CisStartedU3Ek__BackingField_5() { return &___U3CisStartedU3Ek__BackingField_5; }
	inline void set_U3CisStartedU3Ek__BackingField_5(bool value)
	{
		___U3CisStartedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CisDiscoveringRemotesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ___U3CisDiscoveringRemotesU3Ek__BackingField_6)); }
	inline bool get_U3CisDiscoveringRemotesU3Ek__BackingField_6() const { return ___U3CisDiscoveringRemotesU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisDiscoveringRemotesU3Ek__BackingField_6() { return &___U3CisDiscoveringRemotesU3Ek__BackingField_6; }
	inline void set_U3CisDiscoveringRemotesU3Ek__BackingField_6(bool value)
	{
		___U3CisDiscoveringRemotesU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__connectedRemote_7() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ____connectedRemote_7)); }
	inline Remote_t660843562 * get__connectedRemote_7() const { return ____connectedRemote_7; }
	inline Remote_t660843562 ** get_address_of__connectedRemote_7() { return &____connectedRemote_7; }
	inline void set__connectedRemote_7(Remote_t660843562 * value)
	{
		____connectedRemote_7 = value;
		Il2CppCodeGenWriteBarrier((&____connectedRemote_7), value);
	}

	inline static int32_t get_offset_of__discoveredRemotes_8() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541, ____discoveredRemotes_8)); }
	inline List_1_t29964694 * get__discoveredRemotes_8() const { return ____discoveredRemotes_8; }
	inline List_1_t29964694 ** get_address_of__discoveredRemotes_8() { return &____discoveredRemotes_8; }
	inline void set__discoveredRemotes_8(List_1_t29964694 * value)
	{
		____discoveredRemotes_8 = value;
		Il2CppCodeGenWriteBarrier((&____discoveredRemotes_8), value);
	}
};

struct RemoteManager_t2208998541_StaticFields
{
public:
	// RemoteManager RemoteManager::Instance
	RemoteManager_t2208998541 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(RemoteManager_t2208998541_StaticFields, ___Instance_4)); }
	inline RemoteManager_t2208998541 * get_Instance_4() const { return ___Instance_4; }
	inline RemoteManager_t2208998541 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(RemoteManager_t2208998541 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEMANAGER_T2208998541_H
#ifndef EVENTSYSTEM_T3466835263_H
#define EVENTSYSTEM_T3466835263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t3466835263  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t664902677 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t1295781545 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1756533147 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1756533147 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t2681005625 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_SystemInputModules_2)); }
	inline List_1_t664902677 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t664902677 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t664902677 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t1295781545 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t1295781545 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t1295781545 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_FirstSelected_5)); }
	inline GameObject_t1756533147 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1756533147 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1756533147 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_CurrentSelected_8)); }
	inline GameObject_t1756533147 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1756533147 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1756533147 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_DummyData_11)); }
	inline BaseEventData_t2681005625 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t2681005625 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t2681005625 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t3466835263_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2835956395 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t1282925227 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t1282925227 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2835956395 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2835956395 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2835956395 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t1282925227 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t1282925227 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t1282925227 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t1282925227 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t1282925227 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t1282925227 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T3466835263_H
#ifndef BASERAYCASTER_T2336171397_H
#define BASERAYCASTER_T2336171397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t2336171397  : public UIBehaviour_t3960014691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T2336171397_H
#ifndef BASEINPUTMODULE_T1295781545_H
#define BASEINPUTMODULE_T1295781545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t1295781545  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t3685274804 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t1524870173 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t2681005625 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t621514313 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t621514313 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_RaycastResultCache_2)); }
	inline List_1_t3685274804 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t3685274804 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t3685274804 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_AxisEventData_3)); }
	inline AxisEventData_t1524870173 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t1524870173 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t1524870173 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_EventSystem_4)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_BaseEventData_5)); }
	inline BaseEventData_t2681005625 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t2681005625 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t2681005625 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_InputOverride_6)); }
	inline BaseInput_t621514313 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t621514313 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t621514313 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_DefaultInput_7)); }
	inline BaseInput_t621514313 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t621514313 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t621514313 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T1295781545_H
#ifndef IMAGETRACKABLE_T3105654606_H
#define IMAGETRACKABLE_T3105654606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable
struct  ImageTrackable_t3105654606  : public Trackable_t1596815325
{
public:
	// System.Boolean Wikitude.ImageTrackable::_extendedTracking
	bool ____extendedTracking_10;
	// System.String[] Wikitude.ImageTrackable::_targetsForExtendedTracking
	StringU5BU5D_t1642385972* ____targetsForExtendedTracking_11;
	// Wikitude.ImageTrackable/OnImageRecognizedEvent Wikitude.ImageTrackable::OnImageRecognized
	OnImageRecognizedEvent_t2106945187 * ___OnImageRecognized_12;
	// Wikitude.ImageTrackable/OnImageLostEvent Wikitude.ImageTrackable::OnImageLost
	OnImageLostEvent_t2609021075 * ___OnImageLost_13;
	// UnityEngine.Texture2D Wikitude.ImageTrackable::<Preview>k__BackingField
	Texture2D_t3542995729 * ___U3CPreviewU3Ek__BackingField_14;
	// System.Single Wikitude.ImageTrackable::<ImageTargetHeight>k__BackingField
	float ___U3CImageTargetHeightU3Ek__BackingField_15;
	// UnityEngine.Material Wikitude.ImageTrackable::_previewMaterial
	Material_t193706927 * ____previewMaterial_16;
	// UnityEngine.Mesh Wikitude.ImageTrackable::_previewMesh
	Mesh_t1356156583 * ____previewMesh_17;
	// Wikitude.WikitudeCamera Wikitude.ImageTrackable::_wikitudeCamera
	WikitudeCamera_t2517845841 * ____wikitudeCamera_18;

public:
	inline static int32_t get_offset_of__extendedTracking_10() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ____extendedTracking_10)); }
	inline bool get__extendedTracking_10() const { return ____extendedTracking_10; }
	inline bool* get_address_of__extendedTracking_10() { return &____extendedTracking_10; }
	inline void set__extendedTracking_10(bool value)
	{
		____extendedTracking_10 = value;
	}

	inline static int32_t get_offset_of__targetsForExtendedTracking_11() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ____targetsForExtendedTracking_11)); }
	inline StringU5BU5D_t1642385972* get__targetsForExtendedTracking_11() const { return ____targetsForExtendedTracking_11; }
	inline StringU5BU5D_t1642385972** get_address_of__targetsForExtendedTracking_11() { return &____targetsForExtendedTracking_11; }
	inline void set__targetsForExtendedTracking_11(StringU5BU5D_t1642385972* value)
	{
		____targetsForExtendedTracking_11 = value;
		Il2CppCodeGenWriteBarrier((&____targetsForExtendedTracking_11), value);
	}

	inline static int32_t get_offset_of_OnImageRecognized_12() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ___OnImageRecognized_12)); }
	inline OnImageRecognizedEvent_t2106945187 * get_OnImageRecognized_12() const { return ___OnImageRecognized_12; }
	inline OnImageRecognizedEvent_t2106945187 ** get_address_of_OnImageRecognized_12() { return &___OnImageRecognized_12; }
	inline void set_OnImageRecognized_12(OnImageRecognizedEvent_t2106945187 * value)
	{
		___OnImageRecognized_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageRecognized_12), value);
	}

	inline static int32_t get_offset_of_OnImageLost_13() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ___OnImageLost_13)); }
	inline OnImageLostEvent_t2609021075 * get_OnImageLost_13() const { return ___OnImageLost_13; }
	inline OnImageLostEvent_t2609021075 ** get_address_of_OnImageLost_13() { return &___OnImageLost_13; }
	inline void set_OnImageLost_13(OnImageLostEvent_t2609021075 * value)
	{
		___OnImageLost_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageLost_13), value);
	}

	inline static int32_t get_offset_of_U3CPreviewU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ___U3CPreviewU3Ek__BackingField_14)); }
	inline Texture2D_t3542995729 * get_U3CPreviewU3Ek__BackingField_14() const { return ___U3CPreviewU3Ek__BackingField_14; }
	inline Texture2D_t3542995729 ** get_address_of_U3CPreviewU3Ek__BackingField_14() { return &___U3CPreviewU3Ek__BackingField_14; }
	inline void set_U3CPreviewU3Ek__BackingField_14(Texture2D_t3542995729 * value)
	{
		___U3CPreviewU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviewU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CImageTargetHeightU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ___U3CImageTargetHeightU3Ek__BackingField_15)); }
	inline float get_U3CImageTargetHeightU3Ek__BackingField_15() const { return ___U3CImageTargetHeightU3Ek__BackingField_15; }
	inline float* get_address_of_U3CImageTargetHeightU3Ek__BackingField_15() { return &___U3CImageTargetHeightU3Ek__BackingField_15; }
	inline void set_U3CImageTargetHeightU3Ek__BackingField_15(float value)
	{
		___U3CImageTargetHeightU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of__previewMaterial_16() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ____previewMaterial_16)); }
	inline Material_t193706927 * get__previewMaterial_16() const { return ____previewMaterial_16; }
	inline Material_t193706927 ** get_address_of__previewMaterial_16() { return &____previewMaterial_16; }
	inline void set__previewMaterial_16(Material_t193706927 * value)
	{
		____previewMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&____previewMaterial_16), value);
	}

	inline static int32_t get_offset_of__previewMesh_17() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ____previewMesh_17)); }
	inline Mesh_t1356156583 * get__previewMesh_17() const { return ____previewMesh_17; }
	inline Mesh_t1356156583 ** get_address_of__previewMesh_17() { return &____previewMesh_17; }
	inline void set__previewMesh_17(Mesh_t1356156583 * value)
	{
		____previewMesh_17 = value;
		Il2CppCodeGenWriteBarrier((&____previewMesh_17), value);
	}

	inline static int32_t get_offset_of__wikitudeCamera_18() { return static_cast<int32_t>(offsetof(ImageTrackable_t3105654606, ____wikitudeCamera_18)); }
	inline WikitudeCamera_t2517845841 * get__wikitudeCamera_18() const { return ____wikitudeCamera_18; }
	inline WikitudeCamera_t2517845841 ** get_address_of__wikitudeCamera_18() { return &____wikitudeCamera_18; }
	inline void set__wikitudeCamera_18(WikitudeCamera_t2517845841 * value)
	{
		____wikitudeCamera_18 = value;
		Il2CppCodeGenWriteBarrier((&____wikitudeCamera_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETRACKABLE_T3105654606_H
#ifndef POINTERINPUTMODULE_T1441575871_H
#define POINTERINPUTMODULE_T1441575871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t1441575871  : public BaseInputModule_t1295781545
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t607610358 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t3572864619 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t1441575871, ___m_PointerData_12)); }
	inline Dictionary_2_t607610358 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t607610358 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t607610358 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t1441575871, ___m_MouseState_13)); }
	inline MouseState_t3572864619 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t3572864619 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t3572864619 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T1441575871_H
#ifndef MIRABASERAYCASTER_T1612955938_H
#define MIRABASERAYCASTER_T1612955938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraBaseRaycaster
struct  MiraBaseRaycaster_t1612955938  : public BaseRaycaster_t2336171397
{
public:
	// MiraBaseRaycaster/RaycastStyle MiraBaseRaycaster::raycastStyle
	int32_t ___raycastStyle_2;
	// UnityEngine.Ray MiraBaseRaycaster::lastray
	Ray_t2469606224  ___lastray_3;

public:
	inline static int32_t get_offset_of_raycastStyle_2() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t1612955938, ___raycastStyle_2)); }
	inline int32_t get_raycastStyle_2() const { return ___raycastStyle_2; }
	inline int32_t* get_address_of_raycastStyle_2() { return &___raycastStyle_2; }
	inline void set_raycastStyle_2(int32_t value)
	{
		___raycastStyle_2 = value;
	}

	inline static int32_t get_offset_of_lastray_3() { return static_cast<int32_t>(offsetof(MiraBaseRaycaster_t1612955938, ___lastray_3)); }
	inline Ray_t2469606224  get_lastray_3() const { return ___lastray_3; }
	inline Ray_t2469606224 * get_address_of_lastray_3() { return &___lastray_3; }
	inline void set_lastray_3(Ray_t2469606224  value)
	{
		___lastray_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRABASERAYCASTER_T1612955938_H
#ifndef MIRAPHYSICSRAYCAST_T1727560409_H
#define MIRAPHYSICSRAYCAST_T1727560409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPhysicsRaycast
struct  MiraPhysicsRaycast_t1727560409  : public MiraBaseRaycaster_t1612955938
{
public:
	// UnityEngine.Camera MiraPhysicsRaycast::_EventCamera
	Camera_t189460977 * ____EventCamera_5;
	// UnityEngine.LayerMask MiraPhysicsRaycast::_EventMask
	LayerMask_t3188175821  ____EventMask_6;

public:
	inline static int32_t get_offset_of__EventCamera_5() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1727560409, ____EventCamera_5)); }
	inline Camera_t189460977 * get__EventCamera_5() const { return ____EventCamera_5; }
	inline Camera_t189460977 ** get_address_of__EventCamera_5() { return &____EventCamera_5; }
	inline void set__EventCamera_5(Camera_t189460977 * value)
	{
		____EventCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&____EventCamera_5), value);
	}

	inline static int32_t get_offset_of__EventMask_6() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1727560409, ____EventMask_6)); }
	inline LayerMask_t3188175821  get__EventMask_6() const { return ____EventMask_6; }
	inline LayerMask_t3188175821 * get_address_of__EventMask_6() { return &____EventMask_6; }
	inline void set__EventMask_6(LayerMask_t3188175821  value)
	{
		____EventMask_6 = value;
	}
};

struct MiraPhysicsRaycast_t1727560409_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.RaycastHit> MiraPhysicsRaycast::<>f__am$cache0
	Comparison_1_t1348919171 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(MiraPhysicsRaycast_t1727560409_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Comparison_1_t1348919171 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Comparison_1_t1348919171 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Comparison_1_t1348919171 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAPHYSICSRAYCAST_T1727560409_H
#ifndef MIRAINPUTMODULE_T2476427099_H
#define MIRAINPUTMODULE_T2476427099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MiraInputModule
struct  MiraInputModule_t2476427099  : public PointerInputModule_t1441575871
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.MiraInputModule::pointerData
	PointerEventData_t1599784723 * ___pointerData_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::lastPose
	Vector2_t2243707579  ___lastPose_15;
	// System.Boolean UnityEngine.EventSystems.MiraInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_16;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_PrevActionTime
	float ___m_PrevActionTime_17;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_LastMoveVector
	Vector2_t2243707579  ___m_LastMoveVector_18;
	// System.Int32 UnityEngine.EventSystems.MiraInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_LastMousePosition
	Vector2_t2243707579  ___m_LastMousePosition_20;
	// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::m_MousePosition
	Vector2_t2243707579  ___m_MousePosition_21;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_22;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_23;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_24;
	// System.String UnityEngine.EventSystems.MiraInputModule::m_CancelButton
	String_t* ___m_CancelButton_25;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_26;
	// System.Single UnityEngine.EventSystems.MiraInputModule::m_RepeatDelay
	float ___m_RepeatDelay_27;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.MiraInputModule::m_MouseState
	MouseState_t3572864619 * ___m_MouseState_28;

public:
	inline static int32_t get_offset_of_pointerData_14() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___pointerData_14)); }
	inline PointerEventData_t1599784723 * get_pointerData_14() const { return ___pointerData_14; }
	inline PointerEventData_t1599784723 ** get_address_of_pointerData_14() { return &___pointerData_14; }
	inline void set_pointerData_14(PointerEventData_t1599784723 * value)
	{
		___pointerData_14 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_14), value);
	}

	inline static int32_t get_offset_of_lastPose_15() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___lastPose_15)); }
	inline Vector2_t2243707579  get_lastPose_15() const { return ___lastPose_15; }
	inline Vector2_t2243707579 * get_address_of_lastPose_15() { return &___lastPose_15; }
	inline void set_lastPose_15(Vector2_t2243707579  value)
	{
		___lastPose_15 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_16() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_ForceModuleActive_16)); }
	inline bool get_m_ForceModuleActive_16() const { return ___m_ForceModuleActive_16; }
	inline bool* get_address_of_m_ForceModuleActive_16() { return &___m_ForceModuleActive_16; }
	inline void set_m_ForceModuleActive_16(bool value)
	{
		___m_ForceModuleActive_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevActionTime_17() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_PrevActionTime_17)); }
	inline float get_m_PrevActionTime_17() const { return ___m_PrevActionTime_17; }
	inline float* get_address_of_m_PrevActionTime_17() { return &___m_PrevActionTime_17; }
	inline void set_m_PrevActionTime_17(float value)
	{
		___m_PrevActionTime_17 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_18() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_LastMoveVector_18)); }
	inline Vector2_t2243707579  get_m_LastMoveVector_18() const { return ___m_LastMoveVector_18; }
	inline Vector2_t2243707579 * get_address_of_m_LastMoveVector_18() { return &___m_LastMoveVector_18; }
	inline void set_m_LastMoveVector_18(Vector2_t2243707579  value)
	{
		___m_LastMoveVector_18 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_19() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_ConsecutiveMoveCount_19)); }
	inline int32_t get_m_ConsecutiveMoveCount_19() const { return ___m_ConsecutiveMoveCount_19; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_19() { return &___m_ConsecutiveMoveCount_19; }
	inline void set_m_ConsecutiveMoveCount_19(int32_t value)
	{
		___m_ConsecutiveMoveCount_19 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_20() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_LastMousePosition_20)); }
	inline Vector2_t2243707579  get_m_LastMousePosition_20() const { return ___m_LastMousePosition_20; }
	inline Vector2_t2243707579 * get_address_of_m_LastMousePosition_20() { return &___m_LastMousePosition_20; }
	inline void set_m_LastMousePosition_20(Vector2_t2243707579  value)
	{
		___m_LastMousePosition_20 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_21() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_MousePosition_21)); }
	inline Vector2_t2243707579  get_m_MousePosition_21() const { return ___m_MousePosition_21; }
	inline Vector2_t2243707579 * get_address_of_m_MousePosition_21() { return &___m_MousePosition_21; }
	inline void set_m_MousePosition_21(Vector2_t2243707579  value)
	{
		___m_MousePosition_21 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_22() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_HorizontalAxis_22)); }
	inline String_t* get_m_HorizontalAxis_22() const { return ___m_HorizontalAxis_22; }
	inline String_t** get_address_of_m_HorizontalAxis_22() { return &___m_HorizontalAxis_22; }
	inline void set_m_HorizontalAxis_22(String_t* value)
	{
		___m_HorizontalAxis_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_22), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_23() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_VerticalAxis_23)); }
	inline String_t* get_m_VerticalAxis_23() const { return ___m_VerticalAxis_23; }
	inline String_t** get_address_of_m_VerticalAxis_23() { return &___m_VerticalAxis_23; }
	inline void set_m_VerticalAxis_23(String_t* value)
	{
		___m_VerticalAxis_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_23), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_24() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_SubmitButton_24)); }
	inline String_t* get_m_SubmitButton_24() const { return ___m_SubmitButton_24; }
	inline String_t** get_address_of_m_SubmitButton_24() { return &___m_SubmitButton_24; }
	inline void set_m_SubmitButton_24(String_t* value)
	{
		___m_SubmitButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_24), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_25() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_CancelButton_25)); }
	inline String_t* get_m_CancelButton_25() const { return ___m_CancelButton_25; }
	inline String_t** get_address_of_m_CancelButton_25() { return &___m_CancelButton_25; }
	inline void set_m_CancelButton_25(String_t* value)
	{
		___m_CancelButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_25), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_26() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_InputActionsPerSecond_26)); }
	inline float get_m_InputActionsPerSecond_26() const { return ___m_InputActionsPerSecond_26; }
	inline float* get_address_of_m_InputActionsPerSecond_26() { return &___m_InputActionsPerSecond_26; }
	inline void set_m_InputActionsPerSecond_26(float value)
	{
		___m_InputActionsPerSecond_26 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_27() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_RepeatDelay_27)); }
	inline float get_m_RepeatDelay_27() const { return ___m_RepeatDelay_27; }
	inline float* get_address_of_m_RepeatDelay_27() { return &___m_RepeatDelay_27; }
	inline void set_m_RepeatDelay_27(float value)
	{
		___m_RepeatDelay_27 = value;
	}

	inline static int32_t get_offset_of_m_MouseState_28() { return static_cast<int32_t>(offsetof(MiraInputModule_t2476427099, ___m_MouseState_28)); }
	inline MouseState_t3572864619 * get_m_MouseState_28() const { return ___m_MouseState_28; }
	inline MouseState_t3572864619 ** get_address_of_m_MouseState_28() { return &___m_MouseState_28; }
	inline void set_m_MouseState_28(MouseState_t3572864619 * value)
	{
		___m_MouseState_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAINPUTMODULE_T2476427099_H
#ifndef MIRAGRAPHICRAYCAST_T590279048_H
#define MIRAGRAPHICRAYCAST_T590279048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraGraphicRaycast
struct  MiraGraphicRaycast_t590279048  : public MiraBaseRaycaster_t1612955938
{
public:
	// UnityEngine.LayerMask MiraGraphicRaycast::blockingMask
	LayerMask_t3188175821  ___blockingMask_5;
	// System.Boolean MiraGraphicRaycast::ignoreReversedGraphics
	bool ___ignoreReversedGraphics_6;
	// UnityEngine.UI.GraphicRaycaster/BlockingObjects MiraGraphicRaycast::blockingObjects
	int32_t ___blockingObjects_7;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::raycastResults
	List_1_t1795346708 * ___raycastResults_8;
	// UnityEngine.Canvas MiraGraphicRaycast::_canvas
	Canvas_t209405766 * ____canvas_9;

public:
	inline static int32_t get_offset_of_blockingMask_5() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___blockingMask_5)); }
	inline LayerMask_t3188175821  get_blockingMask_5() const { return ___blockingMask_5; }
	inline LayerMask_t3188175821 * get_address_of_blockingMask_5() { return &___blockingMask_5; }
	inline void set_blockingMask_5(LayerMask_t3188175821  value)
	{
		___blockingMask_5 = value;
	}

	inline static int32_t get_offset_of_ignoreReversedGraphics_6() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___ignoreReversedGraphics_6)); }
	inline bool get_ignoreReversedGraphics_6() const { return ___ignoreReversedGraphics_6; }
	inline bool* get_address_of_ignoreReversedGraphics_6() { return &___ignoreReversedGraphics_6; }
	inline void set_ignoreReversedGraphics_6(bool value)
	{
		___ignoreReversedGraphics_6 = value;
	}

	inline static int32_t get_offset_of_blockingObjects_7() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___blockingObjects_7)); }
	inline int32_t get_blockingObjects_7() const { return ___blockingObjects_7; }
	inline int32_t* get_address_of_blockingObjects_7() { return &___blockingObjects_7; }
	inline void set_blockingObjects_7(int32_t value)
	{
		___blockingObjects_7 = value;
	}

	inline static int32_t get_offset_of_raycastResults_8() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ___raycastResults_8)); }
	inline List_1_t1795346708 * get_raycastResults_8() const { return ___raycastResults_8; }
	inline List_1_t1795346708 ** get_address_of_raycastResults_8() { return &___raycastResults_8; }
	inline void set_raycastResults_8(List_1_t1795346708 * value)
	{
		___raycastResults_8 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_8), value);
	}

	inline static int32_t get_offset_of__canvas_9() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048, ____canvas_9)); }
	inline Canvas_t209405766 * get__canvas_9() const { return ____canvas_9; }
	inline Canvas_t209405766 ** get_address_of__canvas_9() { return &____canvas_9; }
	inline void set__canvas_9(Canvas_t209405766 * value)
	{
		____canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_9), value);
	}
};

struct MiraGraphicRaycast_t590279048_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::sortedGraphics
	List_1_t1795346708 * ___sortedGraphics_10;
	// System.Comparison`1<UnityEngine.UI.Graphic> MiraGraphicRaycast::<>f__am$cache0
	Comparison_1_t3687964427 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_sortedGraphics_10() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048_StaticFields, ___sortedGraphics_10)); }
	inline List_1_t1795346708 * get_sortedGraphics_10() const { return ___sortedGraphics_10; }
	inline List_1_t1795346708 ** get_address_of_sortedGraphics_10() { return &___sortedGraphics_10; }
	inline void set_sortedGraphics_10(List_1_t1795346708 * value)
	{
		___sortedGraphics_10 = value;
		Il2CppCodeGenWriteBarrier((&___sortedGraphics_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(MiraGraphicRaycast_t590279048_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Comparison_1_t3687964427 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Comparison_1_t3687964427 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Comparison_1_t3687964427 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRAGRAPHICRAYCAST_T590279048_H
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Wikitude.ImageTrackable[]
struct ImageTrackableU5BU5D_t322116539  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ImageTrackable_t3105654606 * m_Items[1];

public:
	inline ImageTrackable_t3105654606 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ImageTrackable_t3105654606 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ImageTrackable_t3105654606 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ImageTrackable_t3105654606 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ImageTrackable_t3105654606 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ImageTrackable_t3105654606 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};


// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Int32,System.Int32>::.ctor()
extern "C"  void UnityEvent_3__ctor_m2874161539_gshared (UnityEvent_3_t1947670748 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C"  bool ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * p0, BaseEventData_t2681005625 * p1, EventFunction_1_t1186599945 * p2, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * ExecuteEvents_GetEventHandler_TisRuntimeObject_m3333041576_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C"  GameObject_t1756533147 * ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m2541874163_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * p0, BaseEventData_t2681005625 * p1, EventFunction_1_t1186599945 * p2, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C"  void List_1_Clear_m392100656_gshared (List_1_t3685274804 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m490892142_gshared (UnityAction_1_t3438463199 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2462078051_gshared (UnityAction_1_t4056035046 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// T Utils.ObjectSerializationExtension::Deserialize<System.Object>(System.Byte[])
extern "C"  RuntimeObject * ObjectSerializationExtension_Deserialize_TisRuntimeObject_m1078535403_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___byteArray0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2724124387_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisRuntimeObject_m1140156812_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_AddListener_m4113183338_gshared (UnityEvent_1_t2727799310 * __this, UnityAction_1_t4056035046 * p0, const RuntimeMethod* method);

// System.Void UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>::.ctor()
#define UnityEvent_3__ctor_m4061335797(__this, method) ((  void (*) (UnityEvent_3_t2425689862 *, const RuntimeMethod*))UnityEvent_3__ctor_m2874161539_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1825328214 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mira.MiraArController Mira.MiraArController::get_Instance()
extern "C"  MiraArController_t555016598 * MiraArController_get_Instance_m3200326482 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2313448302 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2923680153 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m1722284185 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m3371331170 (Scene_t1684909666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2357316016 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m3374354972 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_Lerp_m2605034858 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1073050816 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerInputModule/MouseState::.ctor()
extern "C"  void MouseState__ctor_m3076609805 (MouseState_t3572864619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerInputModule::.ctor()
extern "C"  void PointerInputModule__ctor_m3738792102 (PointerInputModule_t1441575871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// MiraBasePointer MiraPointerManager::get_Pointer()
extern "C"  MiraBasePointer_t885132991 * MiraPointerManager_get_Pointer_m1670624404 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m2069200279 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m385881926 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C"  bool Input_get_mousePresent_m1100327283 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.BaseInputModule::ShouldActivateModule()
extern "C"  bool BaseInputModule_ShouldActivateModule_m2899747874 (BaseInputModule_t1295781545 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m717298472 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m1913129537 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m1944881077 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1667221528 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m593717298 (Vector2_t2243707579 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.BaseInputModule::ActivateModule()
extern "C"  void BaseInputModule_ActivateModule_m832071241 (BaseInputModule_t1295781545 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::get_eventSystem()
extern "C"  EventSystem_t3466835263 * BaseInputModule_get_eventSystem_m2822730343 (BaseInputModule_t1295781545 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::get_currentSelectedGameObject()
extern "C"  GameObject_t1756533147 * EventSystem_get_currentSelectedGameObject_m701101735 (EventSystem_t3466835263 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2516226135 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::get_firstSelectedGameObject()
extern "C"  GameObject_t1756533147 * EventSystem_get_firstSelectedGameObject_m4059087516 (EventSystem_t3466835263 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventSystem::SetSelectedGameObject(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventSystem_SetSelectedGameObject_m2232036508 (EventSystem_t3466835263 * __this, GameObject_t1756533147 * p0, BaseEventData_t2681005625 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.BaseInputModule::DeactivateModule()
extern "C"  void BaseInputModule_DeactivateModule_m194840002 (BaseInputModule_t1295781545 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerInputModule::ClearSelection()
extern "C"  void PointerInputModule_ClearSelection_m3640318585 (PointerInputModule_t1441575871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::get_submitHandler()
extern "C"  EventFunction_1_t3317921847 * ExecuteEvents_get_submitHandler_m2230161172 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.ISubmitHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisISubmitHandler_t525803901_m896016395(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t3317921847 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::get_cancelHandler()
extern "C"  EventFunction_1_t477470301 * ExecuteEvents_get_cancelHandler_m1964324668 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.ICancelHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisICancelHandler_t1980319651_m897890085(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t477470301 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m1210615473 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m4021682882 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::GetRawMoveVector()
extern "C"  Vector2_t2243707579  MiraInputModule_GetRawMoveVector_m1549472706 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m3193821214 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::get_moveHandler()
extern "C"  EventFunction_1_t1109076156 * ExecuteEvents_get_moveHandler_m2059140926 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IMoveHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIMoveHandler_t2611925506_m110288646(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1109076156 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMouseEvent(System.Int32)
extern "C"  void MiraInputModule_ProcessMouseEvent_m342372504 (MiraInputModule_t2476427099 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.PointerInputModule/ButtonState UnityEngine.EventSystems.PointerInputModule/MouseState::GetButtonState(UnityEngine.EventSystems.PointerEventData/InputButton)
extern "C"  ButtonState_t2688375492 * MouseState_GetButtonState_m337580068 (MouseState_t3572864619 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData UnityEngine.EventSystems.PointerInputModule/ButtonState::get_eventData()
extern "C"  MouseButtonEventData_t3709210170 * ButtonState_get_eventData_m1293357996 (ButtonState_t2688375492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMousePress(UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData)
extern "C"  void MiraInputModule_ProcessMousePress_m1130092012 (MiraInputModule_t2476427099 * __this, MouseButtonEventData_t3709210170 * ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_scrollDelta()
extern "C"  Vector2_t2243707579  PointerEventData_get_scrollDelta_m1283145047 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::get_pointerCurrentRaycast()
extern "C"  RaycastResult_t21186376  PointerEventData_get_pointerCurrentRaycast_m1374279130 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::get_gameObject()
extern "C"  GameObject_t1756533147 * RaycastResult_get_gameObject_m2999022658 (RaycastResult_t21186376 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IScrollHandler>(UnityEngine.GameObject)
#define ExecuteEvents_GetEventHandler_TisIScrollHandler_t3834677510_m1788515243(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, const RuntimeMethod*))ExecuteEvents_GetEventHandler_TisRuntimeObject_m3333041576_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::get_scrollHandler()
extern "C"  EventFunction_1_t2331828160 * ExecuteEvents_get_scrollHandler_m2797719886 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IScrollHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2331828160 *, const RuntimeMethod*))ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::get_updateSelectedHandler()
extern "C"  EventFunction_1_t2276060003 * ExecuteEvents_get_updateSelectedHandler_m4157356548 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IUpdateSelectedHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIUpdateSelectedHandler_t3778909353_m3262129831(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2276060003 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.EventSystems.MiraInputModule::HandlePointerExitAndEnter(UnityEngine.EventSystems.PointerEventData,UnityEngine.GameObject)
extern "C"  void MiraInputModule_HandlePointerExitAndEnter_m3470520529 (MiraInputModule_t2476427099 * __this, PointerEventData_t1599784723 * ___currentPointerData0, GameObject_t1756533147 * ___newEnterTarget1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::PressedThisFrame()
extern "C"  bool MouseButtonEventData_PressedThisFrame_m2499346445 (MouseButtonEventData_t3709210170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_eligibleForClick(System.Boolean)
extern "C"  void PointerEventData_set_eligibleForClick_m2036057844 (PointerEventData_t1599784723 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_delta(UnityEngine.Vector2)
extern "C"  void PointerEventData_set_delta_m3672873329 (PointerEventData_t1599784723 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_dragging(System.Boolean)
extern "C"  void PointerEventData_set_dragging_m915629341 (PointerEventData_t1599784723 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_useDragThreshold(System.Boolean)
extern "C"  void PointerEventData_set_useDragThreshold_m2778439880 (PointerEventData_t1599784723 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
extern "C"  Vector2_t2243707579  PointerEventData_get_position_m2131765015 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_pressPosition(UnityEngine.Vector2)
extern "C"  void PointerEventData_set_pressPosition_m2094137883 (PointerEventData_t1599784723 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_pointerPressRaycast(UnityEngine.EventSystems.RaycastResult)
extern "C"  void PointerEventData_set_pointerPressRaycast_m2551142399 (PointerEventData_t1599784723 * __this, RaycastResult_t21186376  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerInputModule::DeselectIfSelectionChanged(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData)
extern "C"  void PointerInputModule_DeselectIfSelectionChanged_m3360889170 (PointerInputModule_t1441575871 * __this, GameObject_t1756533147 * p0, BaseEventData_t2681005625 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerDownHandler()
extern "C"  EventFunction_1_t2426197568 * ExecuteEvents_get_pointerDownHandler_m1172742772 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerDownHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2426197568 *, const RuntimeMethod*))ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject)
#define ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, const RuntimeMethod*))ExecuteEvents_GetEventHandler_TisRuntimeObject_m3333041576_gshared)(__this /* static, unused */, p0, method)
// MiraBasePointer UnityEngine.EventSystems.MiraInputModule::get_pointer()
extern "C"  MiraBasePointer_t885132991 * MiraInputModule_get_pointer_m783204755 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::get_lastPress()
extern "C"  GameObject_t1756533147 * PointerEventData_get_lastPress_m3835070463 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.EventSystems.PointerEventData::get_clickTime()
extern "C"  float PointerEventData_get_clickTime_m2587872034 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.EventSystems.PointerEventData::get_clickCount()
extern "C"  int32_t PointerEventData_get_clickCount_m4064532478 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_clickCount(System.Int32)
extern "C"  void PointerEventData_set_clickCount_m2095939005 (PointerEventData_t1599784723 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_clickTime(System.Single)
extern "C"  void PointerEventData_set_clickTime_m3931922487 (PointerEventData_t1599784723 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_pointerPress(UnityEngine.GameObject)
extern "C"  void PointerEventData_set_pointerPress_m1418261989 (PointerEventData_t1599784723 * __this, GameObject_t1756533147 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_rawPointerPress(UnityEngine.GameObject)
extern "C"  void PointerEventData_set_rawPointerPress_m1484888025 (PointerEventData_t1599784723 * __this, GameObject_t1756533147 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IDragHandler>(UnityEngine.GameObject)
#define ExecuteEvents_GetEventHandler_TisIDragHandler_t2583993319_m3720979028(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, const RuntimeMethod*))ExecuteEvents_GetEventHandler_TisRuntimeObject_m3333041576_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.EventSystems.PointerEventData::set_pointerDrag(UnityEngine.GameObject)
extern "C"  void PointerEventData_set_pointerDrag_m3543074708 (PointerEventData_t1599784723 * __this, GameObject_t1756533147 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::get_pointerDrag()
extern "C"  GameObject_t1756533147 * PointerEventData_get_pointerDrag_m2740415629 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3768854296 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_initializePotentialDrag()
extern "C"  EventFunction_1_t1847959737 * ExecuteEvents_get_initializePotentialDrag_m2227640438 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IInitializePotentialDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t3350809087_m2193269739(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1847959737 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::ReleasedThisFrame()
extern "C"  bool MouseButtonEventData_ReleasedThisFrame_m482758300 (MouseButtonEventData_t3709210170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::get_pointerPress()
extern "C"  GameObject_t1756533147 * PointerEventData_get_pointerPress_m880101744 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerUpHandler()
extern "C"  EventFunction_1_t344915111 * ExecuteEvents_get_pointerUpHandler_m3494368244 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerUpHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIPointerUpHandler_t1847764461_m885031125(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t344915111 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean UnityEngine.EventSystems.PointerEventData::get_eligibleForClick()
extern "C"  bool PointerEventData_get_eligibleForClick_m2497780621 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerClickHandler()
extern "C"  EventFunction_1_t2888287612 * ExecuteEvents_get_pointerClickHandler_m713983310 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIPointerClickHandler_t96169666_m907812816(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2888287612 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean UnityEngine.EventSystems.PointerEventData::get_dragging()
extern "C"  bool PointerEventData_get_dragging_m220490640 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::get_dropHandler()
extern "C"  EventFunction_1_t887251860 * ExecuteEvents_get_dropHandler_m1848078078 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IDropHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t887251860 *, const RuntimeMethod*))ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_endDragHandler()
extern "C"  EventFunction_1_t4141241546 * ExecuteEvents_get_endDragHandler_m56074740 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IEndDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIEndDragHandler_t1349123600_m4238380530(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t4141241546 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::get_pointerEnter()
extern "C"  GameObject_t1756533147 * PointerEventData_get_pointerEnter_m2114522773 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m939767277(__this, p0, method) ((  GameObject_t1756533147 * (*) (List_1_t1125654279 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerExitHandler()
extern "C"  EventFunction_1_t3253137806 * ExecuteEvents_get_pointerExitHandler_m3250605428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerExitHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIPointerExitHandler_t461019860_m4037442468(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t3253137806 *, const RuntimeMethod*))ExecuteEvents_Execute_TisRuntimeObject_m4168308247_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
#define List_1_get_Count_m2764296230(__this, method) ((  int32_t (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Clear()
#define List_1_Clear_m4030601119(__this, method) ((  void (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void UnityEngine.EventSystems.PointerEventData::set_pointerEnter(UnityEngine.GameObject)
extern "C"  void PointerEventData_set_pointerEnter_m1440587006 (PointerEventData_t1599784723 * __this, GameObject_t1756533147 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1757773010 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule::GetPointerData(System.Int32,UnityEngine.EventSystems.PointerEventData&,System.Boolean)
extern "C"  bool PointerInputModule_GetPointerData_m1695674453 (PointerInputModule_t1441575871 * __this, int32_t p0, PointerEventData_t1599784723 ** p1, bool p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_position(UnityEngine.Vector2)
extern "C"  void PointerEventData_set_position_m794507622 (PointerEventData_t1599784723 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern "C"  Vector2_t2243707579  Input_get_mouseScrollDelta_m3621133506 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_scrollDelta(UnityEngine.Vector2)
extern "C"  void PointerEventData_set_scrollDelta_m4002219844 (PointerEventData_t1599784723 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_button(UnityEngine.EventSystems.PointerEventData/InputButton)
extern "C"  void PointerEventData_set_button_m3279441906 (PointerEventData_t1599784723 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventSystem::RaycastAll(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void EventSystem_RaycastAll_m4000413739 (EventSystem_t3466835263 * __this, PointerEventData_t1599784723 * p0, List_1_t3685274804 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.BaseInputModule::FindFirstRaycast(System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  RaycastResult_t21186376  BaseInputModule_FindFirstRaycast_m797745207 (RuntimeObject * __this /* static, unused */, List_1_t3685274804 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_pointerCurrentRaycast(UnityEngine.EventSystems.RaycastResult)
extern "C"  void PointerEventData_set_pointerCurrentRaycast_m2431897513 (PointerEventData_t1599784723 * __this, RaycastResult_t21186376  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
#define List_1_Clear_m392100656(__this, method) ((  void (*) (List_1_t3685274804 *, const RuntimeMethod*))List_1_Clear_m392100656_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_WorldToScreenPoint_m598317217 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerInputModule::CopyFromTo(UnityEngine.EventSystems.PointerEventData,UnityEngine.EventSystems.PointerEventData)
extern "C"  void PointerInputModule_CopyFromTo_m2185451090 (PointerInputModule_t1441575871 * __this, PointerEventData_t1599784723 * p0, PointerEventData_t1599784723 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerInputModule/MouseState::SetButtonState(UnityEngine.EventSystems.PointerEventData/InputButton,UnityEngine.EventSystems.PointerEventData/FramePressState,UnityEngine.EventSystems.PointerEventData)
extern "C"  void MouseState_SetButtonState_m2329922363 (MouseState_t3572864619 * __this, int32_t p0, int32_t p1, PointerEventData_t1599784723 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiraController::get_ClickButton()
extern "C"  bool MiraController_get_ClickButton_m3709743941 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiraController::get_ClickButtonPressed()
extern "C"  bool MiraController_get_ClickButtonPressed_m2454511183 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiraController::get_ClickButtonReleased()
extern "C"  bool MiraController_get_ClickButtonReleased_m1632081504 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseState::AnyPressesThisFrame()
extern "C"  bool MouseState_AnyPressesThisFrame_m942422561 (MouseState_t3572864619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseState::AnyReleasesThisFrame()
extern "C"  bool MouseState_AnyReleasesThisFrame_m985115530 (MouseState_t3572864619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.MiraInputModule::SendUpdateEventToSelectedObject()
extern "C"  bool MiraInputModule_SendUpdateEventToSelectedObject_m3857972945 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.EventSystem::get_sendNavigationEvents()
extern "C"  bool EventSystem_get_sendNavigationEvents_m2901780066 (EventSystem_t3466835263 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.MiraInputModule::SendMoveEventToSelectedObject()
extern "C"  bool MiraInputModule_SendMoveEventToSelectedObject_m1103889925 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.MiraInputModule::SendSubmitEventToSelectedObject()
extern "C"  bool MiraInputModule_SendSubmitEventToSelectedObject_m2722784986 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMouseEvent(UnityEngine.EventSystems.PointerInputModule/MouseState)
extern "C"  void MiraInputModule_ProcessMouseEvent_m4282372644 (MiraInputModule_t2476427099 * __this, MouseState_t3572864619 * ___mouseData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m2599802704 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteManager/RemoteConnectedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void RemoteConnectedEventHandler__ctor_m2482985582 (RemoteConnectedEventHandler_t3456249665 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteManager::add_OnRemoteConnected(RemoteManager/RemoteConnectedEventHandler)
extern "C"  void RemoteManager_add_OnRemoteConnected_m3709435095 (RemoteManager_t2208998541 * __this, RemoteConnectedEventHandler_t3456249665 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteManager/RemoteDisconnectedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void RemoteDisconnectedEventHandler__ctor_m1535770 (RemoteDisconnectedEventHandler_t730656759 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteManager::add_OnRemoteDisconnected(RemoteManager/RemoteDisconnectedEventHandler)
extern "C"  void RemoteManager_add_OnRemoteDisconnected_m2068840331 (RemoteManager_t2208998541 * __this, RemoteDisconnectedEventHandler_t730656759 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteManager::remove_OnRemoteConnected(RemoteManager/RemoteConnectedEventHandler)
extern "C"  void RemoteManager_remove_OnRemoteConnected_m2251201314 (RemoteManager_t2208998541 * __this, RemoteConnectedEventHandler_t3456249665 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteManager::remove_OnRemoteDisconnected(RemoteManager/RemoteDisconnectedEventHandler)
extern "C"  void RemoteManager_remove_OnRemoteDisconnected_m3027831764 (RemoteManager_t2208998541 * __this, RemoteDisconnectedEventHandler_t730656759 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MiraBTRemoteInput::.ctor()
extern "C"  void MiraBTRemoteInput__ctor_m223735764 (MiraBTRemoteInput_t988911721 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::InitializeTrackers()
extern "C"  void MiraLivePreviewPlayer_InitializeTrackers_m2398981726 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3517219175 * PlayerConnection_get_instance_m1722843600 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m490892142(__this, p0, p1, method) ((  void (*) (UnityAction_1_t3438463199 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m490892142_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern "C"  void PlayerConnection_RegisterConnection_m2554115332 (PlayerConnection_t3517219175 * __this, UnityAction_1_t3438463199 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern "C"  void PlayerConnection_RegisterDisconnection_m541158256 (PlayerConnection_t3517219175 * __this, UnityAction_1_t3438463199 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_fromEditorMiraSessionMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_fromEditorMiraSessionMsgId_m2831389603 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m2955260543(__this, p0, p1, method) ((  void (*) (UnityAction_1_t1667869373 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m2462078051_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Register(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern "C"  void PlayerConnection_Register_m2231017457 (PlayerConnection_t3517219175 * __this, Guid_t  p0, UnityAction_1_t1667869373 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m3526633787 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m3831644396 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2109726426 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,System.String)
extern "C"  void GUI_Box_m1308812445 (RuntimeObject * __this /* static, unused */, Rect_t3681755626  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T Utils.ObjectSerializationExtension::Deserialize<Utils.serializableFromEditorMessage>(System.Byte[])
#define ObjectSerializationExtension_Deserialize_TisserializableFromEditorMessage_t2894567809_m1238441737(__this /* static, unused */, ___byteArray0, method) ((  serializableFromEditorMessage_t2894567809 * (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, const RuntimeMethod*))ObjectSerializationExtension_Deserialize_TisRuntimeObject_m1078535403_gshared)(__this /* static, unused */, ___byteArray0, method)
// System.Guid UnityEngine.XR.iOS.MiraSubMessageIds::get_editorInitMiraRemote()
extern "C"  Guid_t  MiraSubMessageIds_get_editorInitMiraRemote_m1234016531 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C"  bool Guid_op_Equality_m789465560 (RuntimeObject * __this /* static, unused */, Guid_t  p0, Guid_t  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Convert::ToBoolean(System.Byte)
extern "C"  bool Convert_ToBoolean_m2032547942 (RuntimeObject * __this /* static, unused */, uint8_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<MiraLivePreviewWikiConfig>()
#define Component_GetComponent_TisMiraLivePreviewWikiConfig_t2812595783_m2483761890(__this, method) ((  MiraLivePreviewWikiConfig_t2812595783 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void MiraLivePreviewWikiConfig::RotationalOnlyMode(System.Boolean)
extern "C"  void MiraLivePreviewWikiConfig_RotationalOnlyMode_m558320559 (MiraLivePreviewWikiConfig_t2812595783 * __this, bool ___isRotationalOnly0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::InitializeLivePreview()
extern "C"  void MiraLivePreviewPlayer_InitializeLivePreview_m225778839 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraSubMessageIds::get_editorDisconnect()
extern "C"  Guid_t  MiraSubMessageIds_get_editorDisconnect_m2481842044 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::EditorDisconnected(System.Int32)
extern "C"  void MiraLivePreviewPlayer_EditorDisconnected_m3438779146 (MiraLivePreviewPlayer_t3516305084 * __this, int32_t ___playerID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::ReceiveJPEGFrame(Utils.serializableFromEditorMessage)
extern "C"  void MiraLivePreviewPlayer_ReceiveJPEGFrame_m2269876361 (MiraLivePreviewPlayer_t3516305084 * __this, serializableFromEditorMessage_t2894567809 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m3037068888 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::InitializeTextures(UnityEngine.Camera)
extern "C"  void MiraLivePreviewPlayer_InitializeTextures_m3292283093 (MiraLivePreviewPlayer_t3516305084 * __this, Camera_t189460977 * ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiraBTRemoteInput::init()
extern "C"  bool MiraBTRemoteInput_init_m66924122 (MiraBTRemoteInput_t988911721 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m2894134608 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m1187016139 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m3959286051 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern "C"  void Texture2D__ctor_m154655748 (Texture2D_t3542995729 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, bool p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<MiraARVideo>()
#define Component_GetComponent_TisMiraARVideo_t1477805071_m2351498360(__this, method) ((  MiraARVideo_t1477805071 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<Wikitude.ImageTrackable>()
#define Object_FindObjectsOfType_TisImageTrackable_t3105654606_m1655889936(__this /* static, unused */, method) ((  ImageTrackableU5BU5D_t322116539* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectsOfType_TisRuntimeObject_m1140156812_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.Events.UnityAction`1<Wikitude.ImageTarget>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m1430463559(__this, p0, p1, method) ((  void (*) (UnityAction_1_t3381592573 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m2462078051_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`1<Wikitude.ImageTarget>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_AddListener_m394150843(__this, p0, method) ((  void (*) (UnityEvent_1_t2053356837 *, UnityAction_1_t3381592573 *, const RuntimeMethod*))UnityEvent_1_AddListener_m4113183338_gshared)(__this, p0, method)
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
extern "C"  bool ImageConversion_LoadImage_m3156064641 (RuntimeObject * __this /* static, unused */, Texture2D_t3542995729 * p0, ByteU5BU5D_t3397334013* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Gyroscope UnityEngine.Input::get_gyro()
extern "C"  Gyroscope_t1705362817 * Input_get_gyro_m1176661367 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::UpdateGyro(UnityEngine.Gyroscope)
extern "C"  void MiraLivePreviewPlayer_UpdateGyro_m2549312003 (MiraLivePreviewPlayer_t3516305084 * __this, Gyroscope_t1705362817 * ___gyro0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::UpdateBTRemote()
extern "C"  void MiraLivePreviewPlayer_UpdateBTRemote_m820487388 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::UpdateWikiCam()
extern "C"  void MiraLivePreviewPlayer_UpdateWikiCam_m493785173 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableGyroscope Utils.serializableGyroscope::op_Implicit(UnityEngine.Gyroscope)
extern "C"  serializableGyroscope_t1293559986 * serializableGyroscope_op_Implicit_m1484759557 (RuntimeObject * __this /* static, unused */, Gyroscope_t1705362817 * ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_gyroMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_gyroMsgId_m3152547904 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::SendToEditor(System.Guid,System.Object)
extern "C"  void MiraLivePreviewPlayer_SendToEditor_m1266648443 (MiraLivePreviewPlayer_t3516305084 * __this, Guid_t  ___msgId0, RuntimeObject * ___serializableObject1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m881971336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableTransform Utils.serializableTransform::op_Implicit(UnityEngine.Transform)
extern "C"  serializableTransform_t2202979937 * serializableTransform_op_Implicit_m504541074 (RuntimeObject * __this /* static, unused */, Transform_t3275118058 * ____object0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_wikiCamMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_wikiCamMsgId_m3438711336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Wikitude.ImageTarget::get_PhysicalTargetHeight()
extern "C"  float ImageTarget_get_PhysicalTargetHeight_m3486992189 (ImageTarget_t2015006822 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableFloat Utils.serializableFloat::op_Implicit(System.Single)
extern "C"  serializableFloat_t3811907803 * serializableFloat_op_Implicit_m3403528258 (RuntimeObject * __this /* static, unused */, float ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_trackingFoundMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_trackingFoundMsgId_m3240602256 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_trackingLostMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_trackingLostMsgId_m196996400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Remote RemoteManager::get_connectedRemote()
extern "C"  Remote_t660843562 * RemoteManager_get_connectedRemote_m2850669673 (RemoteManager_t2208998541 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableBTRemote Utils.serializableBTRemote::op_Implicit(Remote)
extern "C"  serializableBTRemote_t622411689 * serializableBTRemote_op_Implicit_m3134586455 (RuntimeObject * __this /* static, unused */, Remote_t660843562 * ____btValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_BTRemoteMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_BTRemoteMsgId_m738691607 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableBTRemoteButtons Utils.serializableBTRemoteButtons::op_Implicit(Remote)
extern "C"  serializableBTRemoteButtons_t2327889448 * serializableBTRemoteButtons_op_Implicit_m892522079 (RuntimeObject * __this /* static, unused */, Remote_t660843562 * ____bValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableBTRemoteTouchPad Utils.serializableBTRemoteTouchPad::op_Implicit(Remote)
extern "C"  serializableBTRemoteTouchPad_t2020069673 * serializableBTRemoteTouchPad_op_Implicit_m1392338071 (RuntimeObject * __this /* static, unused */, Remote_t660843562 * ____tpValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_BTRemoteButtonsMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_BTRemoteButtonsMsgId_m1963856294 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_BTRemoteTouchPadMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_BTRemoteTouchPadMsgId_m1308740695 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::DisconnectFromEditor()
extern "C"  void MiraLivePreviewPlayer_DisconnectFromEditor_m616998130 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Utils.ObjectSerializationExtension::SerializeToByteArray(System.Object)
extern "C"  ByteU5BU5D_t3397334013* ObjectSerializationExtension_SerializeToByteArray_m2132233038 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::SendToEditor(System.Guid,System.Byte[])
extern "C"  void MiraLivePreviewPlayer_SendToEditor_m3889611786 (MiraLivePreviewPlayer_t3516305084 * __this, Guid_t  ___msgId0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::get_isConnected()
extern "C"  bool PlayerConnection_get_isConnected_m2418883912 (PlayerConnection_t3517219175 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Send(System.Guid,System.Byte[])
extern "C"  void PlayerConnection_Send_m1490707979 (PlayerConnection_t3517219175 * __this, Guid_t  p0, ByteU5BU5D_t3397334013* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern "C"  void BinaryFormatter__ctor_m4171832002 (BinaryFormatter_t1866979105 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.MemoryStream::.ctor()
extern "C"  void MemoryStream__ctor_m1043059966 (MemoryStream_t743994179 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object)
extern "C"  void BinaryFormatter_Serialize_m433301673 (BinaryFormatter_t1866979105 * __this, Stream_t3255436806 * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteMotionInput RemoteBase::get_motion()
extern "C"  RemoteMotionInput_t3548926620 * RemoteBase_get_motion_m1992541006 (RemoteBase_t3616301967 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteOrientationInput RemoteMotionInput::get_orientation()
extern "C"  RemoteOrientationInput_t3303200544 * RemoteMotionInput_get_orientation_m4167617963 (RemoteMotionInput_t3548926620 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RemoteOrientationInput::getOrientationVector()
extern "C"  Vector3_t2243707580  RemoteOrientationInput_getOrientationVector_m1923861616 (RemoteOrientationInput_t3303200544 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.SerializableVector3 Utils.SerializableVector3::op_Implicit(UnityEngine.Vector3)
extern "C"  SerializableVector3_t4294681249  SerializableVector3_op_Implicit_m2796142158 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteMotionSensorInput RemoteMotionInput::get_rotationRate()
extern "C"  RemoteMotionSensorInput_t841531810 * RemoteMotionInput_get_rotationRate_m3968900193 (RemoteMotionInput_t3548926620 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RemoteMotionSensorInput::getMotionSensorVector()
extern "C"  Vector3_t2243707580  RemoteMotionSensorInput_getMotionSensorVector_m487213716 (RemoteMotionSensorInput_t841531810 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteMotionSensorInput RemoteMotionInput::get_acceleration()
extern "C"  RemoteMotionSensorInput_t841531810 * RemoteMotionInput_get_acceleration_m2249299093 (RemoteMotionInput_t3548926620 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableBTRemote::.ctor(Utils.SerializableVector3,Utils.SerializableVector3,Utils.SerializableVector3)
extern "C"  void serializableBTRemote__ctor_m3258305359 (serializableBTRemote_t622411689 * __this, SerializableVector3_t4294681249  ____orientation0, SerializableVector3_t4294681249  ____rotationRate1, SerializableVector3_t4294681249  ____acceleration2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteButtonInput RemoteBase::get_homeButton()
extern "C"  RemoteButtonInput_t144791130 * RemoteBase_get_homeButton_m2789731865 (RemoteBase_t3616301967 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean RemoteButtonInput::get_isPressed()
extern "C"  bool RemoteButtonInput_get_isPressed_m2552376858 (RemoteButtonInput_t144791130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteButtonInput RemoteBase::get_menuButton()
extern "C"  RemoteButtonInput_t144791130 * RemoteBase_get_menuButton_m604848353 (RemoteBase_t3616301967 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteButtonInput RemoteBase::get_trigger()
extern "C"  RemoteButtonInput_t144791130 * RemoteBase_get_trigger_m2301973040 (RemoteBase_t3616301967 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableBTRemoteButtons::.ctor(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void serializableBTRemoteButtons__ctor_m3640121971 (serializableBTRemoteButtons_t2327889448 * __this, bool ____startButton0, bool ____backButton1, bool ____triggerButton2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteTouchPadInput RemoteBase::get_touchPad()
extern "C"  RemoteTouchPadInput_t1081319266 * RemoteBase_get_touchPad_m3247253518 (RemoteBase_t3616301967 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteButtonInput RemoteTouchPadInput::get_button()
extern "C"  RemoteButtonInput_t144791130 * RemoteTouchPadInput_get_button_m341096981 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteAxisInput RemoteTouchPadInput::get_xAxis()
extern "C"  RemoteAxisInput_t2770128439 * RemoteTouchPadInput_get_xAxis_m960950733 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single RemoteAxisInput::get_value()
extern "C"  float RemoteAxisInput_get_value_m2540058260 (RemoteAxisInput_t2770128439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteAxisInput RemoteTouchPadInput::get_yAxis()
extern "C"  RemoteAxisInput_t2770128439 * RemoteTouchPadInput_get_yAxis_m3985318738 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.SerializableVector2::.ctor(System.Single,System.Single)
extern "C"  void SerializableVector2__ctor_m429252694 (SerializableVector2_t4294681248 * __this, float ___rX0, float ___rY1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteTouchInput RemoteTouchPadInput::get_up()
extern "C"  RemoteTouchInput_t3826108381 * RemoteTouchPadInput_get_up_m1922390201 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteTouchInput RemoteTouchPadInput::get_down()
extern "C"  RemoteTouchInput_t3826108381 * RemoteTouchPadInput_get_down_m3468287018 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteTouchInput RemoteTouchPadInput::get_left()
extern "C"  RemoteTouchInput_t3826108381 * RemoteTouchPadInput_get_left_m1876375955 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// RemoteTouchInput RemoteTouchPadInput::get_right()
extern "C"  RemoteTouchInput_t3826108381 * RemoteTouchPadInput_get_right_m4022269406 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableBTRemoteTouchPad::.ctor(System.Boolean,System.Boolean,Utils.SerializableVector2,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void serializableBTRemoteTouchPad__ctor_m15664844 (serializableBTRemoteTouchPad_t2020069673 * __this, bool ____TouchActive0, bool ____TouchpadButton1, SerializableVector2_t4294681248  ____TouchPos2, bool ____UpButton3, bool ____DownButton4, bool ____LeftButton5, bool ____RightButton6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m1813392066 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single System.Single::Parse(System.String)
extern "C"  float Single_Parse_m1861732734 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableFloat::.ctor(System.Single)
extern "C"  void serializableFloat__ctor_m3443302372 (serializableFloat_t3811907803 * __this, float ___rX0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::get_attitude()
extern "C"  Quaternion_t4030073918  Gyroscope_get_attitude_m2606076698 (Gyroscope_t1705362817 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.SerializableQuaternion Utils.SerializableQuaternion::op_Implicit(UnityEngine.Quaternion)
extern "C"  SerializableQuaternion_t3902400085  SerializableQuaternion_op_Implicit_m1941277880 (RuntimeObject * __this /* static, unused */, Quaternion_t4030073918  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_userAcceleration()
extern "C"  Vector3_t2243707580  Gyroscope_get_userAcceleration_m929621315 (Gyroscope_t1705362817 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableGyroscope::.ctor(Utils.SerializableQuaternion,Utils.SerializableVector3)
extern "C"  void serializableGyroscope__ctor_m669537414 (serializableGyroscope_t1293559986 * __this, SerializableQuaternion_t3902400085  ___a0, SerializableVector3_t4294681249  ___uA1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Single)
extern "C"  ByteU5BU5D_t3397334013* BitConverter_GetBytes_m4095372044 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Buffer_BlockCopy_m1586717258 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializablePointCloud::.ctor(System.Byte[])
extern "C"  void serializablePointCloud__ctor_m2350325965 (serializablePointCloud_t1992421910 * __this, ByteU5BU5D_t3397334013* ___inputPoints0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single System.BitConverter::ToSingle(System.Byte[],System.Int32)
extern "C"  float BitConverter_ToSingle_m159411893 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.SerializableQuaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SerializableQuaternion__ctor_m970837903 (SerializableQuaternion_t3902400085 * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m1263743648 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3614634134* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Utils.SerializableQuaternion::ToString()
extern "C"  String_t* SerializableQuaternion_ToString_m1532411480 (SerializableQuaternion_t3902400085 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m2707026792 (Quaternion_t4030073918 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m2304215762 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m2617026815 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableTransform::.ctor(Utils.SerializableVector3,Utils.SerializableQuaternion)
extern "C"  void serializableTransform__ctor_m1434546217 (serializableTransform_t2202979937 * __this, SerializableVector3_t4294681249  ___p0, SerializableQuaternion_t3902400085  ___r1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m1811873526 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Utils.SerializableVector2::ToString()
extern "C"  String_t* SerializableVector2_ToString_m1702069295 (SerializableVector2_t4294681248 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m847450945 (Vector2_t2243707579 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.SerializableVector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void SerializableVector3__ctor_m618413250 (SerializableVector3_t4294681249 * __this, float ___rX0, float ___rY1, float ___rZ2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m4262916296 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Utils.SerializableVector3::ToString()
extern "C"  String_t* SerializableVector3_ToString_m2915338350 (SerializableVector3_t4294681249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m3879980823 (Vector4_t2243707581 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.SerializableVector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SerializableVector4__ctor_m1161519484 (SerializableVector4_t4294681242 * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteBase::.ctor()
extern "C"  void RemoteBase__ctor_m965004078 (RemoteBase_t3616301967 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteButtonInput::.ctor()
extern "C"  void RemoteButtonInput__ctor_m2755742895 (RemoteButtonInput_t144791130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteBase::set_menuButton(RemoteButtonInput)
extern "C"  void RemoteBase_set_menuButton_m1026035502 (RemoteBase_t3616301967 * __this, RemoteButtonInput_t144791130 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteBase::set_homeButton(RemoteButtonInput)
extern "C"  void RemoteBase_set_homeButton_m3953498266 (RemoteBase_t3616301967 * __this, RemoteButtonInput_t144791130 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteBase::set_trigger(RemoteButtonInput)
extern "C"  void RemoteBase_set_trigger_m2574827387 (RemoteBase_t3616301967 * __this, RemoteButtonInput_t144791130 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteTouchPadInput::.ctor()
extern "C"  void RemoteTouchPadInput__ctor_m1266933877 (RemoteTouchPadInput_t1081319266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteBase::set_touchPad(RemoteTouchPadInput)
extern "C"  void RemoteBase_set_touchPad_m246351705 (RemoteBase_t3616301967 * __this, RemoteTouchPadInput_t1081319266 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteMotionInput::.ctor()
extern "C"  void RemoteMotionInput__ctor_m983228931 (RemoteMotionInput_t3548926620 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteBase::set_motion(RemoteMotionInput)
extern "C"  void RemoteBase_set_motion_m3159704669 (RemoteBase_t3616301967 * __this, RemoteMotionInput_t3548926620 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Utils.SerializableVector3::op_Implicit(Utils.SerializableVector3)
extern "C"  Vector3_t2243707580  SerializableVector3_op_Implicit_m2676771180 (RuntimeObject * __this /* static, unused */, SerializableVector3_t4294681249  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteMotionSensorInput::setMotionSensorVector(UnityEngine.Vector3)
extern "C"  void RemoteMotionSensorInput_setMotionSensorVector_m4116154331 (RemoteMotionSensorInput_t841531810 * __this, Vector3_t2243707580  ___sensorVal0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteOrientationInput::setOrientationVector(UnityEngine.Vector3)
extern "C"  void RemoteOrientationInput_setOrientationVector_m131086611 (RemoteOrientationInput_t3303200544 * __this, Vector3_t2243707580  ___pyr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteButtonInput::set_isPressed(System.Boolean)
extern "C"  void RemoteButtonInput_set_isPressed_m495854157 (RemoteButtonInput_t144791130 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteAxisInput::set_value(System.Single)
extern "C"  void RemoteAxisInput_set_value_m2783159073 (RemoteAxisInput_t2770128439 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern "C"  void WordSelectionEvent__ctor_m899574239 (WordSelectionEvent_t2871480376 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WordSelectionEvent__ctor_m899574239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_3__ctor_m4061335797(__this, /*hidden argument*/UnityEvent_3__ctor_m4061335797_RuntimeMethod_var);
		return;
	}
}
// System.Void ToggleModes::.ctor()
extern "C"  void ToggleModes__ctor_m2331084021 (ToggleModes_t4125091540 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToggleModes::OnEnable()
extern "C"  void ToggleModes_OnEnable_m3823369637 (ToggleModes_t4125091540 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToggleModes_OnEnable_m3823369637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MiraArController.Instance.isSpectator = DemoSceneManager.isSpectator;
		IL2CPP_RUNTIME_CLASS_INIT(MiraArController_t555016598_il2cpp_TypeInfo_var);
		MiraArController_t555016598 * L_0 = MiraArController_get_Instance_m3200326482(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DemoSceneManager_t779426248_il2cpp_TypeInfo_var);
		bool L_1 = ((DemoSceneManager_t779426248_StaticFields*)il2cpp_codegen_static_fields_for(DemoSceneManager_t779426248_il2cpp_TypeInfo_var))->get_isSpectator_2();
		NullCheck(L_0);
		L_0->set_isSpectator_7(L_1);
		// }
		return;
	}
}
// System.Void ToggleModes::Update()
extern "C"  void ToggleModes_Update_m1198530850 (ToggleModes_t4125091540 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToggleModes_Update_m1198530850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// if (Input.GetMouseButtonDown (0)) {
		// if (Input.GetMouseButtonDown (0)) {
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2313448302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		// Debug.Log ("Input touch");
		// Debug.Log ("Input touch");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral2694533563, /*hidden argument*/NULL);
		// DemoSceneManager.isSpectator = !DemoSceneManager.isSpectator;
		IL2CPP_RUNTIME_CLASS_INIT(DemoSceneManager_t779426248_il2cpp_TypeInfo_var);
		bool L_1 = ((DemoSceneManager_t779426248_StaticFields*)il2cpp_codegen_static_fields_for(DemoSceneManager_t779426248_il2cpp_TypeInfo_var))->get_isSpectator_2();
		((DemoSceneManager_t779426248_StaticFields*)il2cpp_codegen_static_fields_for(DemoSceneManager_t779426248_il2cpp_TypeInfo_var))->set_isSpectator_2((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		// SceneManager.LoadScene ( SceneManager.GetActiveScene().name);
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m1722284185(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		// SceneManager.LoadScene ( SceneManager.GetActiveScene().name);
		String_t* L_3 = Scene_get_name_m3371331170((&V_0), /*hidden argument*/NULL);
		// SceneManager.LoadScene ( SceneManager.GetActiveScene().name);
		SceneManager_LoadScene_m2357316016(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// }
		return;
	}
}
// System.Void TrackerLerpDriver::.ctor()
extern "C"  void TrackerLerpDriver__ctor_m2715664210 (TrackerLerpDriver_t1630291735 * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 startPosition = new Vector3(0, 0.15f, 0);
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m1555724485((&L_0), (0.0f), (0.15f), (0.0f), /*hidden argument*/NULL);
		__this->set_startPosition_3(L_0);
		// public Vector3 endPosition = new Vector3(0,0,0);
		Vector3_t2243707580  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m1555724485((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_endPosition_4(L_1);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackerLerpDriver::Start()
extern "C"  void TrackerLerpDriver_Start_m1732521182 (TrackerLerpDriver_t1630291735 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void TrackerLerpDriver::Update()
extern "C"  void TrackerLerpDriver_Update_m2532790711 (TrackerLerpDriver_t1630291735 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerLerpDriver_Update_m2532790711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// transform.localPosition = Vector3.Lerp (startPosition, endPosition, rotationWatcher.deltaDotProduct);
		// transform.localPosition = Vector3.Lerp (startPosition, endPosition, rotationWatcher.deltaDotProduct);
		Transform_t3275118058 * L_0 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_startPosition_3();
		Vector3_t2243707580  L_2 = __this->get_endPosition_4();
		MarkerRotationWatcher_t3969903492 * L_3 = __this->get_rotationWatcher_2();
		NullCheck(L_3);
		float L_4 = L_3->get_deltaDotProduct_3();
		// transform.localPosition = Vector3.Lerp (startPosition, endPosition, rotationWatcher.deltaDotProduct);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_5 = Vector3_Lerp_m2605034858(NULL /*static, unused*/, L_1, L_2, L_4, /*hidden argument*/NULL);
		// transform.localPosition = Vector3.Lerp (startPosition, endPosition, rotationWatcher.deltaDotProduct);
		NullCheck(L_0);
		Transform_set_localPosition_m1073050816(L_0, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::.ctor()
extern "C"  void MiraInputModule__ctor_m4250116751 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule__ctor_m4250116751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private bool m_ForceModuleActive = true;
		__this->set_m_ForceModuleActive_16((bool)1);
		// private int m_ConsecutiveMoveCount = 0;
		__this->set_m_ConsecutiveMoveCount_19(0);
		// private string m_HorizontalAxis = "Horizontal";
		__this->set_m_HorizontalAxis_22(_stringLiteral855845486);
		// private string m_VerticalAxis = "Vertical";
		__this->set_m_VerticalAxis_23(_stringLiteral1635882288);
		// private string m_SubmitButton = "Submit";
		__this->set_m_SubmitButton_24(_stringLiteral2014250346);
		// private string m_CancelButton = "Cancel";
		__this->set_m_CancelButton_25(_stringLiteral2358390244);
		// private float m_InputActionsPerSecond = 10;
		__this->set_m_InputActionsPerSecond_26((10.0f));
		// private float m_RepeatDelay = 0.5f;
		__this->set_m_RepeatDelay_27((0.5f));
		// private readonly MouseState m_MouseState = new MouseState();
		// private readonly MouseState m_MouseState = new MouseState();
		MouseState_t3572864619 * L_0 = (MouseState_t3572864619 *)il2cpp_codegen_object_new(MouseState_t3572864619_il2cpp_TypeInfo_var);
		MouseState__ctor_m3076609805(L_0, /*hidden argument*/NULL);
		__this->set_m_MouseState_28(L_0);
		// protected MiraInputModule()
		PointerInputModule__ctor_m3738792102(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// MiraBasePointer UnityEngine.EventSystems.MiraInputModule::get_pointer()
extern "C"  MiraBasePointer_t885132991 * MiraInputModule_get_pointer_m783204755 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	MiraBasePointer_t885132991 * V_0 = NULL;
	{
		// return MiraPointerManager.Pointer;
		MiraBasePointer_t885132991 * L_0 = MiraPointerManager_get_Pointer_m1670624404(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		// }
		MiraBasePointer_t885132991 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.EventSystems.MiraInputModule/InputMode UnityEngine.EventSystems.MiraInputModule::get_inputMode()
extern "C"  int32_t MiraInputModule_get_inputMode_m1635925778 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return InputMode.Mouse; }
		V_0 = 0;
		goto IL_0008;
	}

IL_0008:
	{
		// get { return InputMode.Mouse; }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::get_allowActivationOnMobileDevice()
extern "C"  bool MiraInputModule_get_allowActivationOnMobileDevice_m3166269964 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return m_ForceModuleActive; }
		bool L_0 = __this->get_m_ForceModuleActive_16();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_ForceModuleActive; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_allowActivationOnMobileDevice(System.Boolean)
extern "C"  void MiraInputModule_set_allowActivationOnMobileDevice_m1861771317 (MiraInputModule_t2476427099 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { m_ForceModuleActive = value; }
		bool L_0 = ___value0;
		__this->set_m_ForceModuleActive_16(L_0);
		// set { m_ForceModuleActive = value; }
		return;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::get_forceModuleActive()
extern "C"  bool MiraInputModule_get_forceModuleActive_m1343254209 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return m_ForceModuleActive; }
		bool L_0 = __this->get_m_ForceModuleActive_16();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_ForceModuleActive; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_forceModuleActive(System.Boolean)
extern "C"  void MiraInputModule_set_forceModuleActive_m4164467778 (MiraInputModule_t2476427099 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { m_ForceModuleActive = value; }
		bool L_0 = ___value0;
		__this->set_m_ForceModuleActive_16(L_0);
		// set { m_ForceModuleActive = value; }
		return;
	}
}
// System.Single UnityEngine.EventSystems.MiraInputModule::get_inputActionsPerSecond()
extern "C"  float MiraInputModule_get_inputActionsPerSecond_m3507905762 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// get { return m_InputActionsPerSecond; }
		float L_0 = __this->get_m_InputActionsPerSecond_26();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_InputActionsPerSecond; }
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_inputActionsPerSecond(System.Single)
extern "C"  void MiraInputModule_set_inputActionsPerSecond_m416511511 (MiraInputModule_t2476427099 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// set { m_InputActionsPerSecond = value; }
		float L_0 = ___value0;
		__this->set_m_InputActionsPerSecond_26(L_0);
		// set { m_InputActionsPerSecond = value; }
		return;
	}
}
// System.Single UnityEngine.EventSystems.MiraInputModule::get_repeatDelay()
extern "C"  float MiraInputModule_get_repeatDelay_m3188696222 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// get { return m_RepeatDelay; }
		float L_0 = __this->get_m_RepeatDelay_27();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_RepeatDelay; }
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_repeatDelay(System.Single)
extern "C"  void MiraInputModule_set_repeatDelay_m980383357 (MiraInputModule_t2476427099 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// set { m_RepeatDelay = value; }
		float L_0 = ___value0;
		__this->set_m_RepeatDelay_27(L_0);
		// set { m_RepeatDelay = value; }
		return;
	}
}
// System.String UnityEngine.EventSystems.MiraInputModule::get_horizontalAxis()
extern "C"  String_t* MiraInputModule_get_horizontalAxis_m1104400132 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return m_HorizontalAxis; }
		String_t* L_0 = __this->get_m_HorizontalAxis_22();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_HorizontalAxis; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_horizontalAxis(System.String)
extern "C"  void MiraInputModule_set_horizontalAxis_m2579108925 (MiraInputModule_t2476427099 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// set { m_HorizontalAxis = value; }
		String_t* L_0 = ___value0;
		__this->set_m_HorizontalAxis_22(L_0);
		// set { m_HorizontalAxis = value; }
		return;
	}
}
// System.String UnityEngine.EventSystems.MiraInputModule::get_verticalAxis()
extern "C"  String_t* MiraInputModule_get_verticalAxis_m3865938370 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return m_VerticalAxis; }
		String_t* L_0 = __this->get_m_VerticalAxis_23();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_VerticalAxis; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_verticalAxis(System.String)
extern "C"  void MiraInputModule_set_verticalAxis_m2940812745 (MiraInputModule_t2476427099 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// set { m_VerticalAxis = value; }
		String_t* L_0 = ___value0;
		__this->set_m_VerticalAxis_23(L_0);
		// set { m_VerticalAxis = value; }
		return;
	}
}
// System.String UnityEngine.EventSystems.MiraInputModule::get_submitButton()
extern "C"  String_t* MiraInputModule_get_submitButton_m1337714501 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return m_SubmitButton; }
		String_t* L_0 = __this->get_m_SubmitButton_24();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_SubmitButton; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_submitButton(System.String)
extern "C"  void MiraInputModule_set_submitButton_m3189592426 (MiraInputModule_t2476427099 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// set { m_SubmitButton = value; }
		String_t* L_0 = ___value0;
		__this->set_m_SubmitButton_24(L_0);
		// set { m_SubmitButton = value; }
		return;
	}
}
// System.String UnityEngine.EventSystems.MiraInputModule::get_cancelButton()
extern "C"  String_t* MiraInputModule_get_cancelButton_m2164605003 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return m_CancelButton; }
		String_t* L_0 = __this->get_m_CancelButton_25();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_CancelButton; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::set_cancelButton(System.String)
extern "C"  void MiraInputModule_set_cancelButton_m1806064880 (MiraInputModule_t2476427099 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// set { m_CancelButton = value; }
		String_t* L_0 = ___value0;
		__this->set_m_CancelButton_25(L_0);
		// set { m_CancelButton = value; }
		return;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::UpdateModule()
extern "C"  void MiraInputModule_UpdateModule_m1024007352 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_UpdateModule_m1024007352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_LastMousePosition = m_MousePosition;
		Vector2_t2243707579  L_0 = __this->get_m_MousePosition_21();
		__this->set_m_LastMousePosition_20(L_0);
		// m_MousePosition = Input.mousePosition;
		// m_MousePosition = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_1 = Input_get_mousePosition_m2069200279(NULL /*static, unused*/, /*hidden argument*/NULL);
		// m_MousePosition = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_m_MousePosition_21(L_2);
		// }
		return;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::IsModuleSupported()
extern "C"  bool MiraInputModule_IsModuleSupported_m89617561 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_IsModuleSupported_m89617561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		// return m_ForceModuleActive || Input.mousePresent;
		bool L_0 = __this->get_m_ForceModuleActive_16();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// return m_ForceModuleActive || Input.mousePresent;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_get_mousePresent_m1100327283(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 1;
	}

IL_0014:
	{
		V_0 = (bool)G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::ShouldActivateModule()
extern "C"  bool MiraInputModule_ShouldActivateModule_m4034676109 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_ShouldActivateModule_m4034676109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// if (!base.ShouldActivateModule())
		// if (!base.ShouldActivateModule())
		bool L_0 = BaseInputModule_ShouldActivateModule_m2899747874(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_009d;
	}

IL_0013:
	{
		// var shouldActivate = m_ForceModuleActive;
		bool L_1 = __this->get_m_ForceModuleActive_16();
		V_1 = L_1;
		// Input.GetButtonDown(m_SubmitButton);
		String_t* L_2 = __this->get_m_SubmitButton_24();
		// Input.GetButtonDown(m_SubmitButton);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// shouldActivate |= Input.GetButtonDown(m_CancelButton);
		bool L_3 = V_1;
		String_t* L_4 = __this->get_m_CancelButton_25();
		// shouldActivate |= Input.GetButtonDown(m_CancelButton);
		bool L_5 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = (bool)((int32_t)((int32_t)L_3|(int32_t)L_5));
		// shouldActivate |= !Mathf.Approximately(Input.GetAxisRaw(m_HorizontalAxis), 0.0f);
		bool L_6 = V_1;
		String_t* L_7 = __this->get_m_HorizontalAxis_22();
		// shouldActivate |= !Mathf.Approximately(Input.GetAxisRaw(m_HorizontalAxis), 0.0f);
		float L_8 = Input_GetAxisRaw_m1913129537(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		// shouldActivate |= !Mathf.Approximately(Input.GetAxisRaw(m_HorizontalAxis), 0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_9 = Mathf_Approximately_m1944881077(NULL /*static, unused*/, L_8, (0.0f), /*hidden argument*/NULL);
		V_1 = (bool)((int32_t)((int32_t)L_6|(int32_t)((((int32_t)L_9) == ((int32_t)0))? 1 : 0)));
		// shouldActivate |= !Mathf.Approximately(Input.GetAxisRaw(m_VerticalAxis), 0.0f);
		bool L_10 = V_1;
		String_t* L_11 = __this->get_m_VerticalAxis_23();
		// shouldActivate |= !Mathf.Approximately(Input.GetAxisRaw(m_VerticalAxis), 0.0f);
		float L_12 = Input_GetAxisRaw_m1913129537(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		// shouldActivate |= !Mathf.Approximately(Input.GetAxisRaw(m_VerticalAxis), 0.0f);
		bool L_13 = Mathf_Approximately_m1944881077(NULL /*static, unused*/, L_12, (0.0f), /*hidden argument*/NULL);
		V_1 = (bool)((int32_t)((int32_t)L_10|(int32_t)((((int32_t)L_13) == ((int32_t)0))? 1 : 0)));
		// shouldActivate |= (m_MousePosition - m_LastMousePosition).sqrMagnitude > 0.0f;
		bool L_14 = V_1;
		Vector2_t2243707579  L_15 = __this->get_m_MousePosition_21();
		Vector2_t2243707579  L_16 = __this->get_m_LastMousePosition_20();
		// shouldActivate |= (m_MousePosition - m_LastMousePosition).sqrMagnitude > 0.0f;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_17 = Vector2_op_Subtraction_m1667221528(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		// shouldActivate |= (m_MousePosition - m_LastMousePosition).sqrMagnitude > 0.0f;
		float L_18 = Vector2_get_sqrMagnitude_m593717298((&V_2), /*hidden argument*/NULL);
		V_1 = (bool)((int32_t)((int32_t)L_14|(int32_t)((((float)L_18) > ((float)(0.0f)))? 1 : 0)));
		// shouldActivate |= Input.GetMouseButtonDown(0);
		bool L_19 = V_1;
		// shouldActivate |= Input.GetMouseButtonDown(0);
		bool L_20 = Input_GetMouseButtonDown_m2313448302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = (bool)((int32_t)((int32_t)L_19|(int32_t)L_20));
		// return shouldActivate;
		bool L_21 = V_1;
		V_0 = L_21;
		goto IL_009d;
	}

IL_009d:
	{
		// }
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::ActivateModule()
extern "C"  void MiraInputModule_ActivateModule_m371367340 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_ActivateModule_m371367340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		// base.ActivateModule();
		// base.ActivateModule();
		BaseInputModule_ActivateModule_m832071241(__this, /*hidden argument*/NULL);
		// m_MousePosition = Input.mousePosition;
		// m_MousePosition = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Input_get_mousePosition_m2069200279(NULL /*static, unused*/, /*hidden argument*/NULL);
		// m_MousePosition = Input.mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_1 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_MousePosition_21(L_1);
		// m_LastMousePosition = Input.mousePosition;
		// m_LastMousePosition = Input.mousePosition;
		Vector3_t2243707580  L_2 = Input_get_mousePosition_m2069200279(NULL /*static, unused*/, /*hidden argument*/NULL);
		// m_LastMousePosition = Input.mousePosition;
		Vector2_t2243707579  L_3 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_m_LastMousePosition_20(L_3);
		// var toSelect = eventSystem.currentSelectedGameObject;
		// var toSelect = eventSystem.currentSelectedGameObject;
		EventSystem_t3466835263 * L_4 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// var toSelect = eventSystem.currentSelectedGameObject;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = EventSystem_get_currentSelectedGameObject_m701101735(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// if (toSelect == null)
		GameObject_t1756533147 * L_6 = V_0;
		// if (toSelect == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		// toSelect = eventSystem.firstSelectedGameObject;
		// toSelect = eventSystem.firstSelectedGameObject;
		EventSystem_t3466835263 * L_8 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// toSelect = eventSystem.firstSelectedGameObject;
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = EventSystem_get_firstSelectedGameObject_m4059087516(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_004b:
	{
		// eventSystem.SetSelectedGameObject(toSelect, GetBaseEventData());
		// eventSystem.SetSelectedGameObject(toSelect, GetBaseEventData());
		EventSystem_t3466835263 * L_10 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = V_0;
		// eventSystem.SetSelectedGameObject(toSelect, GetBaseEventData());
		BaseEventData_t2681005625 * L_12 = VirtFuncInvoker0< BaseEventData_t2681005625 * >::Invoke(19 /* UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::GetBaseEventData() */, __this);
		// eventSystem.SetSelectedGameObject(toSelect, GetBaseEventData());
		NullCheck(L_10);
		EventSystem_SetSelectedGameObject_m2232036508(L_10, L_11, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::DeactivateModule()
extern "C"  void MiraInputModule_DeactivateModule_m2615712361 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	{
		// base.DeactivateModule();
		// base.DeactivateModule();
		BaseInputModule_DeactivateModule_m194840002(__this, /*hidden argument*/NULL);
		// ClearSelection();
		// ClearSelection();
		PointerInputModule_ClearSelection_m3640318585(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::SendSubmitEventToSelectedObject()
extern "C"  bool MiraInputModule_SendSubmitEventToSelectedObject_m2722784986 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_SendSubmitEventToSelectedObject_m2722784986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	BaseEventData_t2681005625 * V_1 = NULL;
	{
		// if (eventSystem.currentSelectedGameObject == null)
		// if (eventSystem.currentSelectedGameObject == null)
		EventSystem_t3466835263 * L_0 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// if (eventSystem.currentSelectedGameObject == null)
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = EventSystem_get_currentSelectedGameObject_m701101735(L_0, /*hidden argument*/NULL);
		// if (eventSystem.currentSelectedGameObject == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_007f;
	}

IL_001e:
	{
		// var data = GetBaseEventData();
		// var data = GetBaseEventData();
		BaseEventData_t2681005625 * L_3 = VirtFuncInvoker0< BaseEventData_t2681005625 * >::Invoke(19 /* UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::GetBaseEventData() */, __this);
		V_1 = L_3;
		// if (Input.GetButtonDown(m_SubmitButton))
		String_t* L_4 = __this->get_m_SubmitButton_24();
		// if (Input.GetButtonDown(m_SubmitButton))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);
		EventSystem_t3466835263 * L_6 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = EventSystem_get_currentSelectedGameObject_m701101735(L_6, /*hidden argument*/NULL);
		BaseEventData_t2681005625 * L_8 = V_1;
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t3317921847 * L_9 = ExecuteEvents_get_submitHandler_m2230161172(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);
		ExecuteEvents_Execute_TisISubmitHandler_t525803901_m896016395(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/ExecuteEvents_Execute_TisISubmitHandler_t525803901_m896016395_RuntimeMethod_var);
	}

IL_004c:
	{
		// if (Input.GetButtonDown(m_CancelButton))
		String_t* L_10 = __this->get_m_CancelButton_25();
		// if (Input.GetButtonDown(m_CancelButton))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_11 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0073;
		}
	}
	{
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
		EventSystem_t3466835263 * L_12 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = EventSystem_get_currentSelectedGameObject_m701101735(L_12, /*hidden argument*/NULL);
		BaseEventData_t2681005625 * L_14 = V_1;
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t477470301 * L_15 = ExecuteEvents_get_cancelHandler_m1964324668(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
		ExecuteEvents_Execute_TisICancelHandler_t1980319651_m897890085(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/ExecuteEvents_Execute_TisICancelHandler_t1980319651_m897890085_RuntimeMethod_var);
	}

IL_0073:
	{
		// return data.used;
		BaseEventData_t2681005625 * L_16 = V_1;
		// return data.used;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean UnityEngine.EventSystems.AbstractEventData::get_used() */, L_16);
		V_0 = L_17;
		goto IL_007f;
	}

IL_007f:
	{
		// }
		bool L_18 = V_0;
		return L_18;
	}
}
// UnityEngine.Vector2 UnityEngine.EventSystems.MiraInputModule::GetRawMoveVector()
extern "C"  Vector2_t2243707579  MiraInputModule_GetRawMoveVector_m1549472706 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_GetRawMoveVector_m1549472706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// Vector2 move = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_0 = Vector2_get_zero_m1210615473(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		// move.x = Input.GetAxisRaw(m_HorizontalAxis);
		String_t* L_1 = __this->get_m_HorizontalAxis_22();
		// move.x = Input.GetAxisRaw(m_HorizontalAxis);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxisRaw_m1913129537(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(&V_0)->set_x_0(L_2);
		// move.y = Input.GetAxisRaw(m_VerticalAxis);
		String_t* L_3 = __this->get_m_VerticalAxis_23();
		// move.y = Input.GetAxisRaw(m_VerticalAxis);
		float L_4 = Input_GetAxisRaw_m1913129537(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		(&V_0)->set_y_1(L_4);
		// if (Input.GetButtonDown(m_HorizontalAxis))
		String_t* L_5 = __this->get_m_HorizontalAxis_22();
		// if (Input.GetButtonDown(m_HorizontalAxis))
		bool L_6 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0077;
		}
	}
	{
		// if (move.x < 0)
		float L_7 = (&V_0)->get_x_0();
		if ((!(((float)L_7) < ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		// move.x = -1f;
		(&V_0)->set_x_0((-1.0f));
	}

IL_0059:
	{
		// if (move.x > 0)
		float L_8 = (&V_0)->get_x_0();
		if ((!(((float)L_8) > ((float)(0.0f)))))
		{
			goto IL_0076;
		}
	}
	{
		// move.x = 1f;
		(&V_0)->set_x_0((1.0f));
	}

IL_0076:
	{
	}

IL_0077:
	{
		// if (Input.GetButtonDown(m_VerticalAxis))
		String_t* L_9 = __this->get_m_VerticalAxis_23();
		// if (Input.GetButtonDown(m_VerticalAxis))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00c3;
		}
	}
	{
		// if (move.y < 0)
		float L_11 = (&V_0)->get_y_1();
		if ((!(((float)L_11) < ((float)(0.0f)))))
		{
			goto IL_00a5;
		}
	}
	{
		// move.y = -1f;
		(&V_0)->set_y_1((-1.0f));
	}

IL_00a5:
	{
		// if (move.y > 0)
		float L_12 = (&V_0)->get_y_1();
		if ((!(((float)L_12) > ((float)(0.0f)))))
		{
			goto IL_00c2;
		}
	}
	{
		// move.y = 1f;
		(&V_0)->set_y_1((1.0f));
	}

IL_00c2:
	{
	}

IL_00c3:
	{
		// return move;
		Vector2_t2243707579  L_13 = V_0;
		V_1 = L_13;
		goto IL_00ca;
	}

IL_00ca:
	{
		// }
		Vector2_t2243707579  L_14 = V_1;
		return L_14;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::SendMoveEventToSelectedObject()
extern "C"  bool MiraInputModule_SendMoveEventToSelectedObject_m1103889925 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_SendMoveEventToSelectedObject_m1103889925_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	AxisEventData_t1524870173 * V_5 = NULL;
	int32_t G_B6_0 = 0;
	{
		// float time = Time.unscaledTime;
		float L_0 = Time_get_unscaledTime_m4021682882(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		// Vector2 movement = GetRawMoveVector();
		// Vector2 movement = GetRawMoveVector();
		Vector2_t2243707579  L_1 = MiraInputModule_GetRawMoveVector_m1549472706(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		// if (Mathf.Approximately(movement.x, 0f) && Mathf.Approximately(movement.y, 0f))
		float L_2 = (&V_1)->get_x_0();
		// if (Mathf.Approximately(movement.x, 0f) && Mathf.Approximately(movement.y, 0f))
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_3 = Mathf_Approximately_m1944881077(NULL /*static, unused*/, L_2, (0.0f), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0049;
		}
	}
	{
		float L_4 = (&V_1)->get_y_1();
		// if (Mathf.Approximately(movement.x, 0f) && Mathf.Approximately(movement.y, 0f))
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1944881077(NULL /*static, unused*/, L_4, (0.0f), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		// m_ConsecutiveMoveCount = 0;
		__this->set_m_ConsecutiveMoveCount_19(0);
		// return false;
		V_2 = (bool)0;
		goto IL_013c;
	}

IL_0049:
	{
		// bool allow = Input.GetButtonDown(m_HorizontalAxis) || Input.GetButtonDown(m_VerticalAxis);
		String_t* L_6 = __this->get_m_HorizontalAxis_22();
		// bool allow = Input.GetButtonDown(m_HorizontalAxis) || Input.GetButtonDown(m_VerticalAxis);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0066;
		}
	}
	{
		String_t* L_8 = __this->get_m_VerticalAxis_23();
		// bool allow = Input.GetButtonDown(m_HorizontalAxis) || Input.GetButtonDown(m_VerticalAxis);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_9));
		goto IL_0067;
	}

IL_0066:
	{
		G_B6_0 = 1;
	}

IL_0067:
	{
		V_3 = (bool)G_B6_0;
		// bool similarDir = (Vector2.Dot(movement, m_LastMoveVector) > 0);
		Vector2_t2243707579  L_10 = V_1;
		Vector2_t2243707579  L_11 = __this->get_m_LastMoveVector_18();
		// bool similarDir = (Vector2.Dot(movement, m_LastMoveVector) > 0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		float L_12 = Vector2_Dot_m3193821214(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_4 = (bool)((((float)L_12) > ((float)(0.0f)))? 1 : 0);
		// if (!allow)
		bool L_13 = V_3;
		if (L_13)
		{
			goto IL_00c5;
		}
	}
	{
		// if (similarDir && m_ConsecutiveMoveCount == 1)
		bool L_14 = V_4;
		if (!L_14)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_15 = __this->get_m_ConsecutiveMoveCount_19();
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_00ad;
		}
	}
	{
		// allow = (time > m_PrevActionTime + m_RepeatDelay);
		float L_16 = V_0;
		float L_17 = __this->get_m_PrevActionTime_17();
		float L_18 = __this->get_m_RepeatDelay_27();
		V_3 = (bool)((((float)L_16) > ((float)((float)((float)L_17+(float)L_18))))? 1 : 0);
		goto IL_00c4;
	}

IL_00ad:
	{
		// allow = (time > m_PrevActionTime + 1f / m_InputActionsPerSecond);
		float L_19 = V_0;
		float L_20 = __this->get_m_PrevActionTime_17();
		float L_21 = __this->get_m_InputActionsPerSecond_26();
		V_3 = (bool)((((float)L_19) > ((float)((float)((float)L_20+(float)((float)((float)(1.0f)/(float)L_21))))))? 1 : 0);
	}

IL_00c4:
	{
	}

IL_00c5:
	{
		// if (!allow)
		bool L_22 = V_3;
		if (L_22)
		{
			goto IL_00d2;
		}
	}
	{
		// return false;
		V_2 = (bool)0;
		goto IL_013c;
	}

IL_00d2:
	{
		// var axisEventData = GetAxisEventData(movement.x, movement.y, 0.6f);
		float L_23 = (&V_1)->get_x_0();
		float L_24 = (&V_1)->get_y_1();
		// var axisEventData = GetAxisEventData(movement.x, movement.y, 0.6f);
		AxisEventData_t1524870173 * L_25 = VirtFuncInvoker3< AxisEventData_t1524870173 *, float, float, float >::Invoke(18 /* UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::GetAxisEventData(System.Single,System.Single,System.Single) */, __this, L_23, L_24, (0.6f));
		V_5 = L_25;
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);
		EventSystem_t3466835263 * L_26 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);
		NullCheck(L_26);
		GameObject_t1756533147 * L_27 = EventSystem_get_currentSelectedGameObject_m701101735(L_26, /*hidden argument*/NULL);
		AxisEventData_t1524870173 * L_28 = V_5;
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t1109076156 * L_29 = ExecuteEvents_get_moveHandler_m2059140926(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);
		ExecuteEvents_Execute_TisIMoveHandler_t2611925506_m110288646(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/ExecuteEvents_Execute_TisIMoveHandler_t2611925506_m110288646_RuntimeMethod_var);
		// if (!similarDir)
		bool L_30 = V_4;
		if (L_30)
		{
			goto IL_0113;
		}
	}
	{
		// m_ConsecutiveMoveCount = 0;
		__this->set_m_ConsecutiveMoveCount_19(0);
	}

IL_0113:
	{
		// m_ConsecutiveMoveCount++;
		int32_t L_31 = __this->get_m_ConsecutiveMoveCount_19();
		__this->set_m_ConsecutiveMoveCount_19(((int32_t)((int32_t)L_31+(int32_t)1)));
		// m_PrevActionTime = time;
		float L_32 = V_0;
		__this->set_m_PrevActionTime_17(L_32);
		// m_LastMoveVector = movement;
		Vector2_t2243707579  L_33 = V_1;
		__this->set_m_LastMoveVector_18(L_33);
		// return axisEventData.used;
		AxisEventData_t1524870173 * L_34 = V_5;
		// return axisEventData.used;
		NullCheck(L_34);
		bool L_35 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean UnityEngine.EventSystems.AbstractEventData::get_used() */, L_34);
		V_2 = L_35;
		goto IL_013c;
	}

IL_013c:
	{
		// }
		bool L_36 = V_2;
		return L_36;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMouseEvent()
extern "C"  void MiraInputModule_ProcessMouseEvent_m2648542233 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	{
		// ProcessMouseEvent(0);
		// ProcessMouseEvent(0);
		MiraInputModule_ProcessMouseEvent_m342372504(__this, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMouseEvent(System.Int32)
extern "C"  void MiraInputModule_ProcessMouseEvent_m342372504 (MiraInputModule_t2476427099 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_ProcessMouseEvent_m342372504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MouseState_t3572864619 * V_0 = NULL;
	MouseButtonEventData_t3709210170 * V_1 = NULL;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1756533147 * V_3 = NULL;
	RaycastResult_t21186376  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		// var mouseData = GetMousePointerEventData(id);
		int32_t L_0 = ___id0;
		// var mouseData = GetMousePointerEventData(id);
		MouseState_t3572864619 * L_1 = VirtFuncInvoker1< MouseState_t3572864619 *, int32_t >::Invoke(27 /* UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::GetMousePointerEventData(System.Int32) */, __this, L_0);
		V_0 = L_1;
		// var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
		MouseState_t3572864619 * L_2 = V_0;
		// var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
		NullCheck(L_2);
		ButtonState_t2688375492 * L_3 = MouseState_GetButtonState_m337580068(L_2, 0, /*hidden argument*/NULL);
		// var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
		NullCheck(L_3);
		MouseButtonEventData_t3709210170 * L_4 = ButtonState_get_eventData_m1293357996(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// ProcessMousePress(leftButtonData);
		MouseButtonEventData_t3709210170 * L_5 = V_1;
		// ProcessMousePress(leftButtonData);
		MiraInputModule_ProcessMousePress_m1130092012(__this, L_5, /*hidden argument*/NULL);
		// ProcessMove(leftButtonData.buttonData);
		MouseButtonEventData_t3709210170 * L_6 = V_1;
		NullCheck(L_6);
		PointerEventData_t1599784723 * L_7 = L_6->get_buttonData_1();
		// ProcessMove(leftButtonData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(28 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessMove(UnityEngine.EventSystems.PointerEventData) */, __this, L_7);
		// ProcessDrag(leftButtonData.buttonData);
		MouseButtonEventData_t3709210170 * L_8 = V_1;
		NullCheck(L_8);
		PointerEventData_t1599784723 * L_9 = L_8->get_buttonData_1();
		// ProcessDrag(leftButtonData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(29 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_9);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		MouseState_t3572864619 * L_10 = V_0;
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		NullCheck(L_10);
		ButtonState_t2688375492 * L_11 = MouseState_GetButtonState_m337580068(L_10, 1, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		NullCheck(L_11);
		MouseButtonEventData_t3709210170 * L_12 = ButtonState_get_eventData_m1293357996(L_11, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		MiraInputModule_ProcessMousePress_m1130092012(__this, L_12, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		MouseState_t3572864619 * L_13 = V_0;
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		NullCheck(L_13);
		ButtonState_t2688375492 * L_14 = MouseState_GetButtonState_m337580068(L_13, 1, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		NullCheck(L_14);
		MouseButtonEventData_t3709210170 * L_15 = ButtonState_get_eventData_m1293357996(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		PointerEventData_t1599784723 * L_16 = L_15->get_buttonData_1();
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(29 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_16);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		MouseState_t3572864619 * L_17 = V_0;
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		NullCheck(L_17);
		ButtonState_t2688375492 * L_18 = MouseState_GetButtonState_m337580068(L_17, 2, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		NullCheck(L_18);
		MouseButtonEventData_t3709210170 * L_19 = ButtonState_get_eventData_m1293357996(L_18, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		MiraInputModule_ProcessMousePress_m1130092012(__this, L_19, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		MouseState_t3572864619 * L_20 = V_0;
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		NullCheck(L_20);
		ButtonState_t2688375492 * L_21 = MouseState_GetButtonState_m337580068(L_20, 2, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		NullCheck(L_21);
		MouseButtonEventData_t3709210170 * L_22 = ButtonState_get_eventData_m1293357996(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		PointerEventData_t1599784723 * L_23 = L_22->get_buttonData_1();
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(29 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_23);
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		MouseButtonEventData_t3709210170 * L_24 = V_1;
		NullCheck(L_24);
		PointerEventData_t1599784723 * L_25 = L_24->get_buttonData_1();
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		NullCheck(L_25);
		Vector2_t2243707579  L_26 = PointerEventData_get_scrollDelta_m1283145047(L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		float L_27 = Vector2_get_sqrMagnitude_m593717298((&V_2), /*hidden argument*/NULL);
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_28 = Mathf_Approximately_m1944881077(NULL /*static, unused*/, L_27, (0.0f), /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00d7;
		}
	}
	{
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		MouseButtonEventData_t3709210170 * L_29 = V_1;
		NullCheck(L_29);
		PointerEventData_t1599784723 * L_30 = L_29->get_buttonData_1();
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		NullCheck(L_30);
		RaycastResult_t21186376  L_31 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		GameObject_t1756533147 * L_32 = RaycastResult_get_gameObject_m2999022658((&V_4), /*hidden argument*/NULL);
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_33 = ExecuteEvents_GetEventHandler_TisIScrollHandler_t3834677510_m1788515243(NULL /*static, unused*/, L_32, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIScrollHandler_t3834677510_m1788515243_RuntimeMethod_var);
		V_3 = L_33;
		// ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
		GameObject_t1756533147 * L_34 = V_3;
		MouseButtonEventData_t3709210170 * L_35 = V_1;
		NullCheck(L_35);
		PointerEventData_t1599784723 * L_36 = L_35->get_buttonData_1();
		// ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
		EventFunction_1_t2331828160 * L_37 = ExecuteEvents_get_scrollHandler_m2797719886(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
		ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692(NULL /*static, unused*/, L_34, L_36, L_37, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692_RuntimeMethod_var);
	}

IL_00d7:
	{
		// }
		return;
	}
}
// System.Boolean UnityEngine.EventSystems.MiraInputModule::SendUpdateEventToSelectedObject()
extern "C"  bool MiraInputModule_SendUpdateEventToSelectedObject_m3857972945 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_SendUpdateEventToSelectedObject_m3857972945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	BaseEventData_t2681005625 * V_1 = NULL;
	{
		// if (eventSystem.currentSelectedGameObject == null)
		// if (eventSystem.currentSelectedGameObject == null)
		EventSystem_t3466835263 * L_0 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// if (eventSystem.currentSelectedGameObject == null)
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = EventSystem_get_currentSelectedGameObject_m701101735(L_0, /*hidden argument*/NULL);
		// if (eventSystem.currentSelectedGameObject == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0048;
	}

IL_001e:
	{
		// var data = GetBaseEventData();
		// var data = GetBaseEventData();
		BaseEventData_t2681005625 * L_3 = VirtFuncInvoker0< BaseEventData_t2681005625 * >::Invoke(19 /* UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::GetBaseEventData() */, __this);
		V_1 = L_3;
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
		EventSystem_t3466835263 * L_4 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = EventSystem_get_currentSelectedGameObject_m701101735(L_4, /*hidden argument*/NULL);
		BaseEventData_t2681005625 * L_6 = V_1;
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t2276060003 * L_7 = ExecuteEvents_get_updateSelectedHandler_m4157356548(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
		ExecuteEvents_Execute_TisIUpdateSelectedHandler_t3778909353_m3262129831(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/ExecuteEvents_Execute_TisIUpdateSelectedHandler_t3778909353_m3262129831_RuntimeMethod_var);
		// return data.used;
		BaseEventData_t2681005625 * L_8 = V_1;
		// return data.used;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean UnityEngine.EventSystems.AbstractEventData::get_used() */, L_8);
		V_0 = L_9;
		goto IL_0048;
	}

IL_0048:
	{
		// }
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMousePress(UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData)
extern "C"  void MiraInputModule_ProcessMousePress_m1130092012 (MiraInputModule_t2476427099 * __this, MouseButtonEventData_t3709210170 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_ProcessMousePress_m1130092012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PointerEventData_t1599784723 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	RaycastResult_t21186376  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1756533147 * V_3 = NULL;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	GameObject_t1756533147 * V_6 = NULL;
	{
		// var pointerEvent = data.buttonData;
		MouseButtonEventData_t3709210170 * L_0 = ___data0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_buttonData_1();
		V_0 = L_1;
		// var currentOverGo = pointerEvent.pointerCurrentRaycast.gameObject;
		PointerEventData_t1599784723 * L_2 = V_0;
		// var currentOverGo = pointerEvent.pointerCurrentRaycast.gameObject;
		NullCheck(L_2);
		RaycastResult_t21186376  L_3 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		// var currentOverGo = pointerEvent.pointerCurrentRaycast.gameObject;
		GameObject_t1756533147 * L_4 = RaycastResult_get_gameObject_m2999022658((&V_2), /*hidden argument*/NULL);
		V_1 = L_4;
		// HandlePointerExitAndEnter(pointerEvent, currentOverGo);
		PointerEventData_t1599784723 * L_5 = V_0;
		GameObject_t1756533147 * L_6 = V_1;
		// HandlePointerExitAndEnter(pointerEvent, currentOverGo);
		MiraInputModule_HandlePointerExitAndEnter_m3470520529(__this, L_5, L_6, /*hidden argument*/NULL);
		// if (data.PressedThisFrame())
		MouseButtonEventData_t3709210170 * L_7 = ___data0;
		// if (data.PressedThisFrame())
		NullCheck(L_7);
		bool L_8 = MouseButtonEventData_PressedThisFrame_m2499346445(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_013d;
		}
	}
	{
		// pointerEvent.eligibleForClick = true;
		PointerEventData_t1599784723 * L_9 = V_0;
		// pointerEvent.eligibleForClick = true;
		NullCheck(L_9);
		PointerEventData_set_eligibleForClick_m2036057844(L_9, (bool)1, /*hidden argument*/NULL);
		// pointerEvent.delta = Vector2.zero;
		PointerEventData_t1599784723 * L_10 = V_0;
		// pointerEvent.delta = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_11 = Vector2_get_zero_m1210615473(NULL /*static, unused*/, /*hidden argument*/NULL);
		// pointerEvent.delta = Vector2.zero;
		NullCheck(L_10);
		PointerEventData_set_delta_m3672873329(L_10, L_11, /*hidden argument*/NULL);
		// pointerEvent.dragging = false;
		PointerEventData_t1599784723 * L_12 = V_0;
		// pointerEvent.dragging = false;
		NullCheck(L_12);
		PointerEventData_set_dragging_m915629341(L_12, (bool)0, /*hidden argument*/NULL);
		// pointerEvent.useDragThreshold = true;
		PointerEventData_t1599784723 * L_13 = V_0;
		// pointerEvent.useDragThreshold = true;
		NullCheck(L_13);
		PointerEventData_set_useDragThreshold_m2778439880(L_13, (bool)1, /*hidden argument*/NULL);
		// pointerEvent.pressPosition = pointerEvent.position;
		PointerEventData_t1599784723 * L_14 = V_0;
		PointerEventData_t1599784723 * L_15 = V_0;
		// pointerEvent.pressPosition = pointerEvent.position;
		NullCheck(L_15);
		Vector2_t2243707579  L_16 = PointerEventData_get_position_m2131765015(L_15, /*hidden argument*/NULL);
		// pointerEvent.pressPosition = pointerEvent.position;
		NullCheck(L_14);
		PointerEventData_set_pressPosition_m2094137883(L_14, L_16, /*hidden argument*/NULL);
		// pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;
		PointerEventData_t1599784723 * L_17 = V_0;
		PointerEventData_t1599784723 * L_18 = V_0;
		// pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;
		NullCheck(L_18);
		RaycastResult_t21186376  L_19 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_18, /*hidden argument*/NULL);
		// pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;
		NullCheck(L_17);
		PointerEventData_set_pointerPressRaycast_m2551142399(L_17, L_19, /*hidden argument*/NULL);
		// DeselectIfSelectionChanged(currentOverGo, pointerEvent);
		GameObject_t1756533147 * L_20 = V_1;
		PointerEventData_t1599784723 * L_21 = V_0;
		// DeselectIfSelectionChanged(currentOverGo, pointerEvent);
		PointerInputModule_DeselectIfSelectionChanged_m3360889170(__this, L_20, L_21, /*hidden argument*/NULL);
		// var newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.pointerDownHandler);
		GameObject_t1756533147 * L_22 = V_1;
		PointerEventData_t1599784723 * L_23 = V_0;
		// var newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.pointerDownHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t2426197568 * L_24 = ExecuteEvents_get_pointerDownHandler_m1172742772(NULL /*static, unused*/, /*hidden argument*/NULL);
		// var newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.pointerDownHandler);
		GameObject_t1756533147 * L_25 = ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506_RuntimeMethod_var);
		V_3 = L_25;
		// if (newPressed == null)
		GameObject_t1756533147 * L_26 = V_3;
		// if (newPressed == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_26, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_008b;
		}
	}
	{
		// newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);
		GameObject_t1756533147 * L_28 = V_1;
		// newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_29 = ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327(NULL /*static, unused*/, L_28, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327_RuntimeMethod_var);
		V_3 = L_29;
	}

IL_008b:
	{
		// pointer.OnPointerClickDown();
		// pointer.OnPointerClickDown();
		MiraBasePointer_t885132991 * L_30 = MiraInputModule_get_pointer_m783204755(__this, /*hidden argument*/NULL);
		// pointer.OnPointerClickDown();
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(10 /* System.Void MiraBasePointer::OnPointerClickDown() */, L_30);
		// float time = Time.unscaledTime;
		float L_31 = Time_get_unscaledTime_m4021682882(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_31;
		// if (newPressed == pointerEvent.lastPress)
		GameObject_t1756533147 * L_32 = V_3;
		PointerEventData_t1599784723 * L_33 = V_0;
		// if (newPressed == pointerEvent.lastPress)
		NullCheck(L_33);
		GameObject_t1756533147 * L_34 = PointerEventData_get_lastPress_m3835070463(L_33, /*hidden argument*/NULL);
		// if (newPressed == pointerEvent.lastPress)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00ee;
		}
	}
	{
		// var diffTime = time - pointerEvent.clickTime;
		float L_36 = V_4;
		PointerEventData_t1599784723 * L_37 = V_0;
		// var diffTime = time - pointerEvent.clickTime;
		NullCheck(L_37);
		float L_38 = PointerEventData_get_clickTime_m2587872034(L_37, /*hidden argument*/NULL);
		V_5 = ((float)((float)L_36-(float)L_38));
		// if (diffTime < 0.3f)
		float L_39 = V_5;
		if ((!(((float)L_39) < ((float)(0.3f)))))
		{
			goto IL_00d9;
		}
	}
	{
		// ++pointerEvent.clickCount;
		PointerEventData_t1599784723 * L_40 = V_0;
		PointerEventData_t1599784723 * L_41 = L_40;
		// ++pointerEvent.clickCount;
		NullCheck(L_41);
		int32_t L_42 = PointerEventData_get_clickCount_m4064532478(L_41, /*hidden argument*/NULL);
		// ++pointerEvent.clickCount;
		NullCheck(L_41);
		PointerEventData_set_clickCount_m2095939005(L_41, ((int32_t)((int32_t)L_42+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_00d9:
	{
		// pointerEvent.clickCount = 1;
		PointerEventData_t1599784723 * L_43 = V_0;
		// pointerEvent.clickCount = 1;
		NullCheck(L_43);
		PointerEventData_set_clickCount_m2095939005(L_43, 1, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		// pointerEvent.clickTime = time;
		PointerEventData_t1599784723 * L_44 = V_0;
		float L_45 = V_4;
		// pointerEvent.clickTime = time;
		NullCheck(L_44);
		PointerEventData_set_clickTime_m3931922487(L_44, L_45, /*hidden argument*/NULL);
		goto IL_00f7;
	}

IL_00ee:
	{
		// pointerEvent.clickCount = 1;
		PointerEventData_t1599784723 * L_46 = V_0;
		// pointerEvent.clickCount = 1;
		NullCheck(L_46);
		PointerEventData_set_clickCount_m2095939005(L_46, 1, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		// pointerEvent.pointerPress = newPressed;
		PointerEventData_t1599784723 * L_47 = V_0;
		GameObject_t1756533147 * L_48 = V_3;
		// pointerEvent.pointerPress = newPressed;
		NullCheck(L_47);
		PointerEventData_set_pointerPress_m1418261989(L_47, L_48, /*hidden argument*/NULL);
		// pointerEvent.rawPointerPress = currentOverGo;
		PointerEventData_t1599784723 * L_49 = V_0;
		GameObject_t1756533147 * L_50 = V_1;
		// pointerEvent.rawPointerPress = currentOverGo;
		NullCheck(L_49);
		PointerEventData_set_rawPointerPress_m1484888025(L_49, L_50, /*hidden argument*/NULL);
		// pointerEvent.clickTime = time;
		PointerEventData_t1599784723 * L_51 = V_0;
		float L_52 = V_4;
		// pointerEvent.clickTime = time;
		NullCheck(L_51);
		PointerEventData_set_clickTime_m3931922487(L_51, L_52, /*hidden argument*/NULL);
		// pointerEvent.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(currentOverGo);
		PointerEventData_t1599784723 * L_53 = V_0;
		GameObject_t1756533147 * L_54 = V_1;
		// pointerEvent.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(currentOverGo);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_55 = ExecuteEvents_GetEventHandler_TisIDragHandler_t2583993319_m3720979028(NULL /*static, unused*/, L_54, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIDragHandler_t2583993319_m3720979028_RuntimeMethod_var);
		// pointerEvent.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(currentOverGo);
		NullCheck(L_53);
		PointerEventData_set_pointerDrag_m3543074708(L_53, L_55, /*hidden argument*/NULL);
		// if (pointerEvent.pointerDrag != null)
		PointerEventData_t1599784723 * L_56 = V_0;
		// if (pointerEvent.pointerDrag != null)
		NullCheck(L_56);
		GameObject_t1756533147 * L_57 = PointerEventData_get_pointerDrag_m2740415629(L_56, /*hidden argument*/NULL);
		// if (pointerEvent.pointerDrag != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_58 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_57, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_013c;
		}
	}
	{
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.initializePotentialDrag);
		PointerEventData_t1599784723 * L_59 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.initializePotentialDrag);
		NullCheck(L_59);
		GameObject_t1756533147 * L_60 = PointerEventData_get_pointerDrag_m2740415629(L_59, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_61 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.initializePotentialDrag);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t1847959737 * L_62 = ExecuteEvents_get_initializePotentialDrag_m2227640438(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.initializePotentialDrag);
		ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t3350809087_m2193269739(NULL /*static, unused*/, L_60, L_61, L_62, /*hidden argument*/ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t3350809087_m2193269739_RuntimeMethod_var);
	}

IL_013c:
	{
	}

IL_013d:
	{
		// if (data.ReleasedThisFrame())
		MouseButtonEventData_t3709210170 * L_63 = ___data0;
		// if (data.ReleasedThisFrame())
		NullCheck(L_63);
		bool L_64 = MouseButtonEventData_ReleasedThisFrame_m482758300(L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_0267;
		}
	}
	{
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);
		PointerEventData_t1599784723 * L_65 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);
		NullCheck(L_65);
		GameObject_t1756533147 * L_66 = PointerEventData_get_pointerPress_m880101744(L_65, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_67 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t344915111 * L_68 = ExecuteEvents_get_pointerUpHandler_m3494368244(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);
		ExecuteEvents_Execute_TisIPointerUpHandler_t1847764461_m885031125(NULL /*static, unused*/, L_66, L_67, L_68, /*hidden argument*/ExecuteEvents_Execute_TisIPointerUpHandler_t1847764461_m885031125_RuntimeMethod_var);
		// var pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);
		GameObject_t1756533147 * L_69 = V_1;
		// var pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);
		GameObject_t1756533147 * L_70 = ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327(NULL /*static, unused*/, L_69, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327_RuntimeMethod_var);
		V_6 = L_70;
		// if (pointerEvent.pointerPress == pointerUpHandler && pointerEvent.eligibleForClick)
		PointerEventData_t1599784723 * L_71 = V_0;
		// if (pointerEvent.pointerPress == pointerUpHandler && pointerEvent.eligibleForClick)
		NullCheck(L_71);
		GameObject_t1756533147 * L_72 = PointerEventData_get_pointerPress_m880101744(L_71, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_73 = V_6;
		// if (pointerEvent.pointerPress == pointerUpHandler && pointerEvent.eligibleForClick)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_74 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_72, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_01a4;
		}
	}
	{
		PointerEventData_t1599784723 * L_75 = V_0;
		// if (pointerEvent.pointerPress == pointerUpHandler && pointerEvent.eligibleForClick)
		NullCheck(L_75);
		bool L_76 = PointerEventData_get_eligibleForClick_m2497780621(L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_01a4;
		}
	}
	{
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
		PointerEventData_t1599784723 * L_77 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
		NullCheck(L_77);
		GameObject_t1756533147 * L_78 = PointerEventData_get_pointerPress_m880101744(L_77, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_79 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t2888287612 * L_80 = ExecuteEvents_get_pointerClickHandler_m713983310(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
		ExecuteEvents_Execute_TisIPointerClickHandler_t96169666_m907812816(NULL /*static, unused*/, L_78, L_79, L_80, /*hidden argument*/ExecuteEvents_Execute_TisIPointerClickHandler_t96169666_m907812816_RuntimeMethod_var);
		// pointer.OnPointerClickDown();
		// pointer.OnPointerClickDown();
		MiraBasePointer_t885132991 * L_81 = MiraInputModule_get_pointer_m783204755(__this, /*hidden argument*/NULL);
		// pointer.OnPointerClickDown();
		NullCheck(L_81);
		VirtActionInvoker0::Invoke(10 /* System.Void MiraBasePointer::OnPointerClickDown() */, L_81);
		goto IL_01cf;
	}

IL_01a4:
	{
		// else if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		PointerEventData_t1599784723 * L_82 = V_0;
		// else if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		NullCheck(L_82);
		GameObject_t1756533147 * L_83 = PointerEventData_get_pointerDrag_m2740415629(L_82, /*hidden argument*/NULL);
		// else if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_83, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_01cf;
		}
	}
	{
		PointerEventData_t1599784723 * L_85 = V_0;
		// else if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		NullCheck(L_85);
		bool L_86 = PointerEventData_get_dragging_m220490640(L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_01cf;
		}
	}
	{
		// ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.dropHandler);
		GameObject_t1756533147 * L_87 = V_1;
		PointerEventData_t1599784723 * L_88 = V_0;
		// ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.dropHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t887251860 * L_89 = ExecuteEvents_get_dropHandler_m1848078078(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.dropHandler);
		ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312(NULL /*static, unused*/, L_87, L_88, L_89, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312_RuntimeMethod_var);
	}

IL_01cf:
	{
		// if (!pointerEvent.eligibleForClick || !pointerEvent.dragging)
		PointerEventData_t1599784723 * L_90 = V_0;
		// if (!pointerEvent.eligibleForClick || !pointerEvent.dragging)
		NullCheck(L_90);
		bool L_91 = PointerEventData_get_eligibleForClick_m2497780621(L_90, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_01e5;
		}
	}
	{
		PointerEventData_t1599784723 * L_92 = V_0;
		// if (!pointerEvent.eligibleForClick || !pointerEvent.dragging)
		NullCheck(L_92);
		bool L_93 = PointerEventData_get_dragging_m220490640(L_92, /*hidden argument*/NULL);
		if (L_93)
		{
			goto IL_01f2;
		}
	}

IL_01e5:
	{
		// pointer.OnPointerClickUp();
		// pointer.OnPointerClickUp();
		MiraBasePointer_t885132991 * L_94 = MiraInputModule_get_pointer_m783204755(__this, /*hidden argument*/NULL);
		// pointer.OnPointerClickUp();
		NullCheck(L_94);
		VirtActionInvoker0::Invoke(11 /* System.Void MiraBasePointer::OnPointerClickUp() */, L_94);
	}

IL_01f2:
	{
		// pointerEvent.eligibleForClick = false;
		PointerEventData_t1599784723 * L_95 = V_0;
		// pointerEvent.eligibleForClick = false;
		NullCheck(L_95);
		PointerEventData_set_eligibleForClick_m2036057844(L_95, (bool)0, /*hidden argument*/NULL);
		// pointerEvent.pointerPress = null;
		PointerEventData_t1599784723 * L_96 = V_0;
		// pointerEvent.pointerPress = null;
		NullCheck(L_96);
		PointerEventData_set_pointerPress_m1418261989(L_96, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		// pointerEvent.rawPointerPress = null;
		PointerEventData_t1599784723 * L_97 = V_0;
		// pointerEvent.rawPointerPress = null;
		NullCheck(L_97);
		PointerEventData_set_rawPointerPress_m1484888025(L_97, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		// if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		PointerEventData_t1599784723 * L_98 = V_0;
		// if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		NullCheck(L_98);
		GameObject_t1756533147 * L_99 = PointerEventData_get_pointerDrag_m2740415629(L_98, /*hidden argument*/NULL);
		// if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_100 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_99, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_0235;
		}
	}
	{
		PointerEventData_t1599784723 * L_101 = V_0;
		// if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
		NullCheck(L_101);
		bool L_102 = PointerEventData_get_dragging_m220490640(L_101, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_0235;
		}
	}
	{
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);
		PointerEventData_t1599784723 * L_103 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);
		NullCheck(L_103);
		GameObject_t1756533147 * L_104 = PointerEventData_get_pointerDrag_m2740415629(L_103, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_105 = V_0;
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t4141241546 * L_106 = ExecuteEvents_get_endDragHandler_m56074740(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);
		ExecuteEvents_Execute_TisIEndDragHandler_t1349123600_m4238380530(NULL /*static, unused*/, L_104, L_105, L_106, /*hidden argument*/ExecuteEvents_Execute_TisIEndDragHandler_t1349123600_m4238380530_RuntimeMethod_var);
	}

IL_0235:
	{
		// pointerEvent.dragging = false;
		PointerEventData_t1599784723 * L_107 = V_0;
		// pointerEvent.dragging = false;
		NullCheck(L_107);
		PointerEventData_set_dragging_m915629341(L_107, (bool)0, /*hidden argument*/NULL);
		// pointerEvent.pointerDrag = null;
		PointerEventData_t1599784723 * L_108 = V_0;
		// pointerEvent.pointerDrag = null;
		NullCheck(L_108);
		PointerEventData_set_pointerDrag_m3543074708(L_108, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		// if (currentOverGo != pointerEvent.pointerEnter)
		GameObject_t1756533147 * L_109 = V_1;
		PointerEventData_t1599784723 * L_110 = V_0;
		// if (currentOverGo != pointerEvent.pointerEnter)
		NullCheck(L_110);
		GameObject_t1756533147 * L_111 = PointerEventData_get_pointerEnter_m2114522773(L_110, /*hidden argument*/NULL);
		// if (currentOverGo != pointerEvent.pointerEnter)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_112 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_109, L_111, /*hidden argument*/NULL);
		if (!L_112)
		{
			goto IL_0266;
		}
	}
	{
		// HandlePointerExitAndEnter(pointerEvent, null);
		PointerEventData_t1599784723 * L_113 = V_0;
		// HandlePointerExitAndEnter(pointerEvent, null);
		MiraInputModule_HandlePointerExitAndEnter_m3470520529(__this, L_113, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		// HandlePointerExitAndEnter(pointerEvent, currentOverGo);
		PointerEventData_t1599784723 * L_114 = V_0;
		GameObject_t1756533147 * L_115 = V_1;
		// HandlePointerExitAndEnter(pointerEvent, currentOverGo);
		MiraInputModule_HandlePointerExitAndEnter_m3470520529(__this, L_114, L_115, /*hidden argument*/NULL);
	}

IL_0266:
	{
	}

IL_0267:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::HandlePointerExitAndEnter(UnityEngine.EventSystems.PointerEventData,UnityEngine.GameObject)
extern "C"  void MiraInputModule_HandlePointerExitAndEnter_m3470520529 (MiraInputModule_t2476427099 * __this, PointerEventData_t1599784723 * ___currentPointerData0, GameObject_t1756533147 * ___newEnterTarget1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_HandlePointerExitAndEnter_m3470520529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	GameObject_t1756533147 * V_2 = NULL;
	int32_t G_B12_0 = 0;
	{
		// if (newEnterTarget == null || currentPointerData.pointerEnter == null)
		GameObject_t1756533147 * L_0 = ___newEnterTarget1;
		// if (newEnterTarget == null || currentPointerData.pointerEnter == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		PointerEventData_t1599784723 * L_2 = ___currentPointerData0;
		// if (newEnterTarget == null || currentPointerData.pointerEnter == null)
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = PointerEventData_get_pointerEnter_m2114522773(L_2, /*hidden argument*/NULL);
		// if (newEnterTarget == null || currentPointerData.pointerEnter == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0086;
		}
	}

IL_001e:
	{
		// for (var i = 0; i < currentPointerData.hovered.Count; ++i)
		V_0 = 0;
		goto IL_0044;
	}

IL_0026:
	{
		// ExecuteEvents.Execute(currentPointerData.hovered[i], currentPointerData, ExecuteEvents.pointerExitHandler);
		PointerEventData_t1599784723 * L_5 = ___currentPointerData0;
		NullCheck(L_5);
		List_1_t1125654279 * L_6 = L_5->get_hovered_9();
		int32_t L_7 = V_0;
		// ExecuteEvents.Execute(currentPointerData.hovered[i], currentPointerData, ExecuteEvents.pointerExitHandler);
		NullCheck(L_6);
		GameObject_t1756533147 * L_8 = List_1_get_Item_m939767277(L_6, L_7, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
		PointerEventData_t1599784723 * L_9 = ___currentPointerData0;
		// ExecuteEvents.Execute(currentPointerData.hovered[i], currentPointerData, ExecuteEvents.pointerExitHandler);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t3253137806 * L_10 = ExecuteEvents_get_pointerExitHandler_m3250605428(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.Execute(currentPointerData.hovered[i], currentPointerData, ExecuteEvents.pointerExitHandler);
		ExecuteEvents_Execute_TisIPointerExitHandler_t461019860_m4037442468(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/ExecuteEvents_Execute_TisIPointerExitHandler_t461019860_m4037442468_RuntimeMethod_var);
		// for (var i = 0; i < currentPointerData.hovered.Count; ++i)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0044:
	{
		// for (var i = 0; i < currentPointerData.hovered.Count; ++i)
		int32_t L_12 = V_0;
		PointerEventData_t1599784723 * L_13 = ___currentPointerData0;
		NullCheck(L_13);
		List_1_t1125654279 * L_14 = L_13->get_hovered_9();
		// for (var i = 0; i < currentPointerData.hovered.Count; ++i)
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m2764296230(L_14, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_0026;
		}
	}
	{
		// currentPointerData.hovered.Clear();
		PointerEventData_t1599784723 * L_16 = ___currentPointerData0;
		NullCheck(L_16);
		List_1_t1125654279 * L_17 = L_16->get_hovered_9();
		// currentPointerData.hovered.Clear();
		NullCheck(L_17);
		List_1_Clear_m4030601119(L_17, /*hidden argument*/List_1_Clear_m4030601119_RuntimeMethod_var);
		// if (newEnterTarget == null)
		GameObject_t1756533147 * L_18 = ___newEnterTarget1;
		// if (newEnterTarget == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0085;
		}
	}
	{
		// currentPointerData.pointerEnter = newEnterTarget;
		PointerEventData_t1599784723 * L_20 = ___currentPointerData0;
		GameObject_t1756533147 * L_21 = ___newEnterTarget1;
		// currentPointerData.pointerEnter = newEnterTarget;
		NullCheck(L_20);
		PointerEventData_set_pointerEnter_m1440587006(L_20, L_21, /*hidden argument*/NULL);
		// pointer.OnPointerExit(newEnterTarget);
		// pointer.OnPointerExit(newEnterTarget);
		MiraBasePointer_t885132991 * L_22 = MiraInputModule_get_pointer_m783204755(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = ___newEnterTarget1;
		// pointer.OnPointerExit(newEnterTarget);
		NullCheck(L_22);
		VirtActionInvoker1< GameObject_t1756533147 * >::Invoke(8 /* System.Void MiraBasePointer::OnPointerExit(UnityEngine.GameObject) */, L_22, L_23);
		// return;
		goto IL_00ec;
	}

IL_0085:
	{
	}

IL_0086:
	{
		// bool isTargetInteractive = currentPointerData.pointerPress != null ||
		PointerEventData_t1599784723 * L_24 = ___currentPointerData0;
		// bool isTargetInteractive = currentPointerData.pointerPress != null ||
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = PointerEventData_get_pointerPress_m880101744(L_24, /*hidden argument*/NULL);
		// bool isTargetInteractive = currentPointerData.pointerPress != null ||
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_25, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_t1756533147 * L_27 = ___newEnterTarget1;
		// ExecuteEvents.GetEventHandler<IPointerClickHandler>(newEnterTarget) != null ||
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_28 = ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327(NULL /*static, unused*/, L_27, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t96169666_m2931125327_RuntimeMethod_var);
		// ExecuteEvents.GetEventHandler<IPointerClickHandler>(newEnterTarget) != null ||
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_28, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_t1756533147 * L_30 = ___newEnterTarget1;
		// ExecuteEvents.GetEventHandler<IDragHandler>(newEnterTarget) != null;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_31 = ExecuteEvents_GetEventHandler_TisIDragHandler_t2583993319_m3720979028(NULL /*static, unused*/, L_30, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIDragHandler_t2583993319_m3720979028_RuntimeMethod_var);
		// ExecuteEvents.GetEventHandler<IDragHandler>(newEnterTarget) != null;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_31, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B12_0 = ((int32_t)(L_32));
		goto IL_00b7;
	}

IL_00b6:
	{
		G_B12_0 = 1;
	}

IL_00b7:
	{
		V_1 = (bool)G_B12_0;
		// if (currentPointerData.pointerEnter == newEnterTarget && newEnterTarget)
		PointerEventData_t1599784723 * L_33 = ___currentPointerData0;
		// if (currentPointerData.pointerEnter == newEnterTarget && newEnterTarget)
		NullCheck(L_33);
		GameObject_t1756533147 * L_34 = PointerEventData_get_pointerEnter_m2114522773(L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = ___newEnterTarget1;
		// if (currentPointerData.pointerEnter == newEnterTarget && newEnterTarget)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00e7;
		}
	}
	{
		GameObject_t1756533147 * L_37 = ___newEnterTarget1;
		// if (currentPointerData.pointerEnter == newEnterTarget && newEnterTarget)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00e7;
		}
	}
	{
		// pointer.OnPointerHover(newEnterTarget, currentPointerData.pointerCurrentRaycast, isTargetInteractive);
		// pointer.OnPointerHover(newEnterTarget, currentPointerData.pointerCurrentRaycast, isTargetInteractive);
		MiraBasePointer_t885132991 * L_39 = MiraInputModule_get_pointer_m783204755(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_40 = ___newEnterTarget1;
		PointerEventData_t1599784723 * L_41 = ___currentPointerData0;
		// pointer.OnPointerHover(newEnterTarget, currentPointerData.pointerCurrentRaycast, isTargetInteractive);
		NullCheck(L_41);
		RaycastResult_t21186376  L_42 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_41, /*hidden argument*/NULL);
		bool L_43 = V_1;
		// pointer.OnPointerHover(newEnterTarget, currentPointerData.pointerCurrentRaycast, isTargetInteractive);
		NullCheck(L_39);
		VirtActionInvoker3< GameObject_t1756533147 *, RaycastResult_t21186376 , bool >::Invoke(7 /* System.Void MiraBasePointer::OnPointerHover(UnityEngine.GameObject,UnityEngine.EventSystems.RaycastResult,System.Boolean) */, L_39, L_40, L_42, L_43);
	}

IL_00e7:
	{
		// return;
		goto IL_00ec;
	}

IL_00ec:
	{
		// }
		return;
	}
}
// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.MiraInputModule::GetRayPointerData()
extern "C"  MouseState_t3572864619 * MiraInputModule_GetRayPointerData_m766374278 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_GetRayPointerData_m766374278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PointerEventData_t1599784723 * V_0 = NULL;
	RaycastResult_t21186376  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	PointerEventData_t1599784723 * V_3 = NULL;
	PointerEventData_t1599784723 * V_4 = NULL;
	MouseState_t3572864619 * V_5 = NULL;
	{
		// GetPointerData(kMouseLeftId, out leftData, true);
		// GetPointerData(kMouseLeftId, out leftData, true);
		PointerInputModule_GetPointerData_m1695674453(__this, (-1), (&V_0), (bool)1, /*hidden argument*/NULL);
		// leftData.Reset();
		PointerEventData_t1599784723 * L_0 = V_0;
		// leftData.Reset();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UnityEngine.EventSystems.AbstractEventData::Reset() */, L_0);
		// leftData.position = Vector2.zero;
		PointerEventData_t1599784723 * L_1 = V_0;
		// leftData.position = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_2 = Vector2_get_zero_m1210615473(NULL /*static, unused*/, /*hidden argument*/NULL);
		// leftData.position = Vector2.zero;
		NullCheck(L_1);
		PointerEventData_set_position_m794507622(L_1, L_2, /*hidden argument*/NULL);
		// leftData.scrollDelta = Input.mouseScrollDelta;
		PointerEventData_t1599784723 * L_3 = V_0;
		// leftData.scrollDelta = Input.mouseScrollDelta;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_4 = Input_get_mouseScrollDelta_m3621133506(NULL /*static, unused*/, /*hidden argument*/NULL);
		// leftData.scrollDelta = Input.mouseScrollDelta;
		NullCheck(L_3);
		PointerEventData_set_scrollDelta_m4002219844(L_3, L_4, /*hidden argument*/NULL);
		// leftData.button = PointerEventData.InputButton.Left;
		PointerEventData_t1599784723 * L_5 = V_0;
		// leftData.button = PointerEventData.InputButton.Left;
		NullCheck(L_5);
		PointerEventData_set_button_m3279441906(L_5, 0, /*hidden argument*/NULL);
		// eventSystem.RaycastAll(leftData, m_RaycastResultCache);
		// eventSystem.RaycastAll(leftData, m_RaycastResultCache);
		EventSystem_t3466835263 * L_6 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_7 = V_0;
		List_1_t3685274804 * L_8 = ((BaseInputModule_t1295781545 *)__this)->get_m_RaycastResultCache_2();
		// eventSystem.RaycastAll(leftData, m_RaycastResultCache);
		NullCheck(L_6);
		EventSystem_RaycastAll_m4000413739(L_6, L_7, L_8, /*hidden argument*/NULL);
		// var raycast = FindFirstRaycast(m_RaycastResultCache);
		List_1_t3685274804 * L_9 = ((BaseInputModule_t1295781545 *)__this)->get_m_RaycastResultCache_2();
		// var raycast = FindFirstRaycast(m_RaycastResultCache);
		RaycastResult_t21186376  L_10 = BaseInputModule_FindFirstRaycast_m797745207(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		// leftData.pointerCurrentRaycast = raycast;
		PointerEventData_t1599784723 * L_11 = V_0;
		RaycastResult_t21186376  L_12 = V_1;
		// leftData.pointerCurrentRaycast = raycast;
		NullCheck(L_11);
		PointerEventData_set_pointerCurrentRaycast_m2431897513(L_11, L_12, /*hidden argument*/NULL);
		// m_RaycastResultCache.Clear();
		List_1_t3685274804 * L_13 = ((BaseInputModule_t1295781545 *)__this)->get_m_RaycastResultCache_2();
		// m_RaycastResultCache.Clear();
		NullCheck(L_13);
		List_1_Clear_m392100656(L_13, /*hidden argument*/List_1_Clear_m392100656_RuntimeMethod_var);
		// if (raycast.module != null)
		BaseRaycaster_t2336171397 * L_14 = (&V_1)->get_module_1();
		// if (raycast.module != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ce;
		}
	}
	{
		// if (raycast.module is MiraGraphicRaycast || raycast.module is MiraPhysicsRaycast)
		BaseRaycaster_t2336171397 * L_16 = (&V_1)->get_module_1();
		if (((MiraGraphicRaycast_t590279048 *)IsInstClass((RuntimeObject*)L_16, MiraGraphicRaycast_t590279048_il2cpp_TypeInfo_var)))
		{
			goto IL_0094;
		}
	}
	{
		BaseRaycaster_t2336171397 * L_17 = (&V_1)->get_module_1();
		if (!((MiraPhysicsRaycast_t1727560409 *)IsInstClass((RuntimeObject*)L_17, MiraPhysicsRaycast_t1727560409_il2cpp_TypeInfo_var)))
		{
			goto IL_00cd;
		}
	}

IL_0094:
	{
		// Vector2 pos = raycast.module.eventCamera.WorldToScreenPoint(raycast.worldPosition);
		BaseRaycaster_t2336171397 * L_18 = (&V_1)->get_module_1();
		// Vector2 pos = raycast.module.eventCamera.WorldToScreenPoint(raycast.worldPosition);
		NullCheck(L_18);
		Camera_t189460977 * L_19 = VirtFuncInvoker0< Camera_t189460977 * >::Invoke(18 /* UnityEngine.Camera UnityEngine.EventSystems.BaseRaycaster::get_eventCamera() */, L_18);
		Vector3_t2243707580  L_20 = (&V_1)->get_worldPosition_7();
		// Vector2 pos = raycast.module.eventCamera.WorldToScreenPoint(raycast.worldPosition);
		NullCheck(L_19);
		Vector3_t2243707580  L_21 = Camera_WorldToScreenPoint_m598317217(L_19, L_20, /*hidden argument*/NULL);
		// Vector2 pos = raycast.module.eventCamera.WorldToScreenPoint(raycast.worldPosition);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_22 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		// leftData.delta = pos - leftData.position;
		PointerEventData_t1599784723 * L_23 = V_0;
		Vector2_t2243707579  L_24 = V_2;
		PointerEventData_t1599784723 * L_25 = V_0;
		// leftData.delta = pos - leftData.position;
		NullCheck(L_25);
		Vector2_t2243707579  L_26 = PointerEventData_get_position_m2131765015(L_25, /*hidden argument*/NULL);
		// leftData.delta = pos - leftData.position;
		Vector2_t2243707579  L_27 = Vector2_op_Subtraction_m1667221528(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		// leftData.delta = pos - leftData.position;
		NullCheck(L_23);
		PointerEventData_set_delta_m3672873329(L_23, L_27, /*hidden argument*/NULL);
		// leftData.position = pos;
		PointerEventData_t1599784723 * L_28 = V_0;
		Vector2_t2243707579  L_29 = V_2;
		// leftData.position = pos;
		NullCheck(L_28);
		PointerEventData_set_position_m794507622(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00cd:
	{
	}

IL_00ce:
	{
		// GetPointerData(kMouseRightId, out rightData, true);
		// GetPointerData(kMouseRightId, out rightData, true);
		PointerInputModule_GetPointerData_m1695674453(__this, ((int32_t)-2), (&V_3), (bool)1, /*hidden argument*/NULL);
		// CopyFromTo(leftData, rightData);
		PointerEventData_t1599784723 * L_30 = V_0;
		PointerEventData_t1599784723 * L_31 = V_3;
		// CopyFromTo(leftData, rightData);
		PointerInputModule_CopyFromTo_m2185451090(__this, L_30, L_31, /*hidden argument*/NULL);
		// rightData.button = PointerEventData.InputButton.Right;
		PointerEventData_t1599784723 * L_32 = V_3;
		// rightData.button = PointerEventData.InputButton.Right;
		NullCheck(L_32);
		PointerEventData_set_button_m3279441906(L_32, 1, /*hidden argument*/NULL);
		// GetPointerData(kMouseMiddleId, out middleData, true);
		// GetPointerData(kMouseMiddleId, out middleData, true);
		PointerInputModule_GetPointerData_m1695674453(__this, ((int32_t)-3), (&V_4), (bool)1, /*hidden argument*/NULL);
		// CopyFromTo(leftData, middleData);
		PointerEventData_t1599784723 * L_33 = V_0;
		PointerEventData_t1599784723 * L_34 = V_4;
		// CopyFromTo(leftData, middleData);
		PointerInputModule_CopyFromTo_m2185451090(__this, L_33, L_34, /*hidden argument*/NULL);
		// middleData.button = PointerEventData.InputButton.Middle;
		PointerEventData_t1599784723 * L_35 = V_4;
		// middleData.button = PointerEventData.InputButton.Middle;
		NullCheck(L_35);
		PointerEventData_set_button_m3279441906(L_35, 2, /*hidden argument*/NULL);
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Left, ClickButtonState(), leftData);
		MouseState_t3572864619 * L_36 = __this->get_m_MouseState_28();
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Left, ClickButtonState(), leftData);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(31 /* UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.MiraInputModule::ClickButtonState() */, __this);
		PointerEventData_t1599784723 * L_38 = V_0;
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Left, ClickButtonState(), leftData);
		NullCheck(L_36);
		MouseState_SetButtonState_m2329922363(L_36, 0, L_37, L_38, /*hidden argument*/NULL);
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Right, PointerEventData.FramePressState.NotChanged, rightData);
		MouseState_t3572864619 * L_39 = __this->get_m_MouseState_28();
		PointerEventData_t1599784723 * L_40 = V_3;
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Right, PointerEventData.FramePressState.NotChanged, rightData);
		NullCheck(L_39);
		MouseState_SetButtonState_m2329922363(L_39, 1, 3, L_40, /*hidden argument*/NULL);
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Middle, PointerEventData.FramePressState.NotChanged, middleData);
		MouseState_t3572864619 * L_41 = __this->get_m_MouseState_28();
		PointerEventData_t1599784723 * L_42 = V_4;
		// m_MouseState.SetButtonState(PointerEventData.InputButton.Middle, PointerEventData.FramePressState.NotChanged, middleData);
		NullCheck(L_41);
		MouseState_SetButtonState_m2329922363(L_41, 2, 3, L_42, /*hidden argument*/NULL);
		// return m_MouseState;
		MouseState_t3572864619 * L_43 = __this->get_m_MouseState_28();
		V_5 = L_43;
		goto IL_0143;
	}

IL_0143:
	{
		// }
		MouseState_t3572864619 * L_44 = V_5;
		return L_44;
	}
}
// UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.MiraInputModule::ClickButtonState()
extern "C"  int32_t MiraInputModule_ClickButtonState_m1608183513 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		// bool pressed = MiraController.ClickButton || MiraController.ClickButtonPressed;
		bool L_0 = MiraController_get_ClickButton_m3709743941(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		// bool pressed = MiraController.ClickButton || MiraController.ClickButtonPressed;
		bool L_1 = MiraController_get_ClickButtonPressed_m2454511183(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 1;
	}

IL_0013:
	{
		V_0 = (bool)G_B3_0;
		// bool released = MiraController.ClickButtonReleased;
		bool L_2 = MiraController_get_ClickButtonReleased_m1632081504(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		// if (pressed && released)
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		// return PointerEventData.FramePressState.PressedAndReleased;
		V_2 = 2;
		goto IL_0052;
	}

IL_002e:
	{
		// else if (pressed)
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// return PointerEventData.FramePressState.Pressed;
		V_2 = 0;
		goto IL_0052;
	}

IL_003c:
	{
		// else if (released)
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		// return PointerEventData.FramePressState.Released;
		V_2 = 1;
		goto IL_0052;
	}

IL_004a:
	{
		// return PointerEventData.FramePressState.NotChanged;
		V_2 = 3;
		goto IL_0052;
	}

IL_0052:
	{
		// }
		int32_t L_7 = V_2;
		return L_7;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::ProcessMouseEvent(UnityEngine.EventSystems.PointerInputModule/MouseState)
extern "C"  void MiraInputModule_ProcessMouseEvent_m4282372644 (MiraInputModule_t2476427099 * __this, MouseState_t3572864619 * ___mouseData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraInputModule_ProcessMouseEvent_m4282372644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	MouseButtonEventData_t3709210170 * V_2 = NULL;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObject_t1756533147 * V_4 = NULL;
	RaycastResult_t21186376  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		// var pressed = mouseData.AnyPressesThisFrame();
		MouseState_t3572864619 * L_0 = ___mouseData0;
		// var pressed = mouseData.AnyPressesThisFrame();
		NullCheck(L_0);
		bool L_1 = MouseState_AnyPressesThisFrame_m942422561(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var released = mouseData.AnyReleasesThisFrame();
		MouseState_t3572864619 * L_2 = ___mouseData0;
		// var released = mouseData.AnyReleasesThisFrame();
		NullCheck(L_2);
		bool L_3 = MouseState_AnyReleasesThisFrame_m985115530(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
		MouseState_t3572864619 * L_4 = ___mouseData0;
		// var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
		NullCheck(L_4);
		ButtonState_t2688375492 * L_5 = MouseState_GetButtonState_m337580068(L_4, 0, /*hidden argument*/NULL);
		// var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
		NullCheck(L_5);
		MouseButtonEventData_t3709210170 * L_6 = ButtonState_get_eventData_m1293357996(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		// ProcessMousePress(leftButtonData);
		MouseButtonEventData_t3709210170 * L_7 = V_2;
		// ProcessMousePress(leftButtonData);
		MiraInputModule_ProcessMousePress_m1130092012(__this, L_7, /*hidden argument*/NULL);
		// ProcessMove(leftButtonData.buttonData);
		MouseButtonEventData_t3709210170 * L_8 = V_2;
		NullCheck(L_8);
		PointerEventData_t1599784723 * L_9 = L_8->get_buttonData_1();
		// ProcessMove(leftButtonData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(28 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessMove(UnityEngine.EventSystems.PointerEventData) */, __this, L_9);
		// ProcessDrag(leftButtonData.buttonData);
		MouseButtonEventData_t3709210170 * L_10 = V_2;
		NullCheck(L_10);
		PointerEventData_t1599784723 * L_11 = L_10->get_buttonData_1();
		// ProcessDrag(leftButtonData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(29 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_11);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		MouseState_t3572864619 * L_12 = ___mouseData0;
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		NullCheck(L_12);
		ButtonState_t2688375492 * L_13 = MouseState_GetButtonState_m337580068(L_12, 1, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		NullCheck(L_13);
		MouseButtonEventData_t3709210170 * L_14 = ButtonState_get_eventData_m1293357996(L_13, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
		MiraInputModule_ProcessMousePress_m1130092012(__this, L_14, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		MouseState_t3572864619 * L_15 = ___mouseData0;
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		NullCheck(L_15);
		ButtonState_t2688375492 * L_16 = MouseState_GetButtonState_m337580068(L_15, 1, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		NullCheck(L_16);
		MouseButtonEventData_t3709210170 * L_17 = ButtonState_get_eventData_m1293357996(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		PointerEventData_t1599784723 * L_18 = L_17->get_buttonData_1();
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(29 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_18);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		MouseState_t3572864619 * L_19 = ___mouseData0;
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		NullCheck(L_19);
		ButtonState_t2688375492 * L_20 = MouseState_GetButtonState_m337580068(L_19, 2, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		NullCheck(L_20);
		MouseButtonEventData_t3709210170 * L_21 = ButtonState_get_eventData_m1293357996(L_20, /*hidden argument*/NULL);
		// ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
		MiraInputModule_ProcessMousePress_m1130092012(__this, L_21, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		MouseState_t3572864619 * L_22 = ___mouseData0;
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		NullCheck(L_22);
		ButtonState_t2688375492 * L_23 = MouseState_GetButtonState_m337580068(L_22, 2, /*hidden argument*/NULL);
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		NullCheck(L_23);
		MouseButtonEventData_t3709210170 * L_24 = ButtonState_get_eventData_m1293357996(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		PointerEventData_t1599784723 * L_25 = L_24->get_buttonData_1();
		// ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);
		VirtActionInvoker1< PointerEventData_t1599784723 * >::Invoke(29 /* System.Void UnityEngine.EventSystems.PointerInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_25);
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		MouseButtonEventData_t3709210170 * L_26 = V_2;
		NullCheck(L_26);
		PointerEventData_t1599784723 * L_27 = L_26->get_buttonData_1();
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = PointerEventData_get_scrollDelta_m1283145047(L_27, /*hidden argument*/NULL);
		V_3 = L_28;
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		float L_29 = Vector2_get_sqrMagnitude_m593717298((&V_3), /*hidden argument*/NULL);
		// if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_30 = Mathf_Approximately_m1944881077(NULL /*static, unused*/, L_29, (0.0f), /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00df;
		}
	}
	{
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		MouseButtonEventData_t3709210170 * L_31 = V_2;
		NullCheck(L_31);
		PointerEventData_t1599784723 * L_32 = L_31->get_buttonData_1();
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		NullCheck(L_32);
		RaycastResult_t21186376  L_33 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_32, /*hidden argument*/NULL);
		V_5 = L_33;
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		GameObject_t1756533147 * L_34 = RaycastResult_get_gameObject_m2999022658((&V_5), /*hidden argument*/NULL);
		// var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_35 = ExecuteEvents_GetEventHandler_TisIScrollHandler_t3834677510_m1788515243(NULL /*static, unused*/, L_34, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIScrollHandler_t3834677510_m1788515243_RuntimeMethod_var);
		V_4 = L_35;
		// ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
		GameObject_t1756533147 * L_36 = V_4;
		MouseButtonEventData_t3709210170 * L_37 = V_2;
		NullCheck(L_37);
		PointerEventData_t1599784723 * L_38 = L_37->get_buttonData_1();
		// ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
		EventFunction_1_t2331828160 * L_39 = ExecuteEvents_get_scrollHandler_m2797719886(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
		ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692(NULL /*static, unused*/, L_36, L_38, L_39, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692_RuntimeMethod_var);
	}

IL_00df:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.EventSystems.MiraInputModule::Process()
extern "C"  void MiraInputModule_Process_m2441582200 (MiraInputModule_t2476427099 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// bool usedEvent = SendUpdateEventToSelectedObject();
		// bool usedEvent = SendUpdateEventToSelectedObject();
		bool L_0 = MiraInputModule_SendUpdateEventToSelectedObject_m3857972945(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		// if (eventSystem.sendNavigationEvents)
		// if (eventSystem.sendNavigationEvents)
		EventSystem_t3466835263 * L_1 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		// if (eventSystem.sendNavigationEvents)
		NullCheck(L_1);
		bool L_2 = EventSystem_get_sendNavigationEvents_m2901780066(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		// if (!usedEvent)
		bool L_3 = V_0;
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		// usedEvent |= SendMoveEventToSelectedObject();
		bool L_4 = V_0;
		// usedEvent |= SendMoveEventToSelectedObject();
		bool L_5 = MiraInputModule_SendMoveEventToSelectedObject_m1103889925(__this, /*hidden argument*/NULL);
		V_0 = (bool)((int32_t)((int32_t)L_4|(int32_t)L_5));
	}

IL_0028:
	{
		// if (!usedEvent)
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		// SendSubmitEventToSelectedObject();
		// SendSubmitEventToSelectedObject();
		MiraInputModule_SendSubmitEventToSelectedObject_m2722784986(__this, /*hidden argument*/NULL);
	}

IL_0035:
	{
	}

IL_0036:
	{
		// ProcessMouseEvent(GetRayPointerData());
		// ProcessMouseEvent(GetRayPointerData());
		MouseState_t3572864619 * L_7 = VirtFuncInvoker0< MouseState_t3572864619 * >::Invoke(30 /* UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.MiraInputModule::GetRayPointerData() */, __this);
		// ProcessMouseEvent(GetRayPointerData());
		MiraInputModule_ProcessMouseEvent_m4282372644(__this, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_fromEditorMiraSessionMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_fromEditorMiraSessionMsgId_m2831389603 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_fromEditorMiraSessionMsgId_m2831389603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid fromEditorMiraSessionMsgId { get { return new Guid("b4c939fd-6ece-49c8-bd2b-83cfb85b66e6"); } }
		// public static Guid fromEditorMiraSessionMsgId { get { return new Guid("b4c939fd-6ece-49c8-bd2b-83cfb85b66e6"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral1193156373, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid fromEditorMiraSessionMsgId { get { return new Guid("b4c939fd-6ece-49c8-bd2b-83cfb85b66e6"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_gyroMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_gyroMsgId_m3152547904 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_gyroMsgId_m3152547904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid gyroMsgId { get { return new Guid("6651e9c9-6d8e-4db4-8b7e-ca36678726c7"); } }
		// public static Guid gyroMsgId { get { return new Guid("6651e9c9-6d8e-4db4-8b7e-ca36678726c7"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral2837019704, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid gyroMsgId { get { return new Guid("6651e9c9-6d8e-4db4-8b7e-ca36678726c7"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_wikiCamMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_wikiCamMsgId_m3438711336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_wikiCamMsgId_m3438711336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid wikiCamMsgId { get { return new Guid("11643971-76fb-445e-91ab-427bffb8eb05"); } }
		// public static Guid wikiCamMsgId { get { return new Guid("11643971-76fb-445e-91ab-427bffb8eb05"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral3826344735, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid wikiCamMsgId { get { return new Guid("11643971-76fb-445e-91ab-427bffb8eb05"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_trackingFoundMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_trackingFoundMsgId_m3240602256 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_trackingFoundMsgId_m3240602256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid trackingFoundMsgId { get { return new Guid("44b23c3d-109d-414a-a0ca-c4ce68320827"); } }
		// public static Guid trackingFoundMsgId { get { return new Guid("44b23c3d-109d-414a-a0ca-c4ce68320827"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral3052444739, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid trackingFoundMsgId { get { return new Guid("44b23c3d-109d-414a-a0ca-c4ce68320827"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_trackingLostMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_trackingLostMsgId_m196996400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_trackingLostMsgId_m196996400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid trackingLostMsgId { get { return new Guid("4ef85dca-b6d8-45f3-840b-a1fdd4fe1735"); } }
		// public static Guid trackingLostMsgId { get { return new Guid("4ef85dca-b6d8-45f3-840b-a1fdd4fe1735"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral2514286891, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid trackingLostMsgId { get { return new Guid("4ef85dca-b6d8-45f3-840b-a1fdd4fe1735"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_BTRemoteMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_BTRemoteMsgId_m738691607 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_BTRemoteMsgId_m738691607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid BTRemoteMsgId { get { return new Guid("9c40b9e8-be0c-49ad-aee2-d525622e76d9"); } }
		// public static Guid BTRemoteMsgId { get { return new Guid("9c40b9e8-be0c-49ad-aee2-d525622e76d9"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral6139838, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid BTRemoteMsgId { get { return new Guid("9c40b9e8-be0c-49ad-aee2-d525622e76d9"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_BTRemoteButtonsMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_BTRemoteButtonsMsgId_m1963856294 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_BTRemoteButtonsMsgId_m1963856294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid BTRemoteButtonsMsgId { get { return new Guid("d1fc2dd8-502e-4088-b310-51eff60ebe0a"); } }
		// public static Guid BTRemoteButtonsMsgId { get { return new Guid("d1fc2dd8-502e-4088-b310-51eff60ebe0a"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral1928898930, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid BTRemoteButtonsMsgId { get { return new Guid("d1fc2dd8-502e-4088-b310-51eff60ebe0a"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraConnectionMessageIds::get_BTRemoteTouchPadMsgId()
extern "C"  Guid_t  MiraConnectionMessageIds_get_BTRemoteTouchPadMsgId_m1308740695 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraConnectionMessageIds_get_BTRemoteTouchPadMsgId_m1308740695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid BTRemoteTouchPadMsgId { get { return new Guid("ed3f5dc0-82dc-4eb6-ae8d-337b4b4c5fdc"); } }
		// public static Guid BTRemoteTouchPadMsgId { get { return new Guid("ed3f5dc0-82dc-4eb6-ae8d-337b4b4c5fdc"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral3753267782, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid BTRemoteTouchPadMsgId { get { return new Guid("ed3f5dc0-82dc-4eb6-ae8d-337b4b4c5fdc"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewEditor::.ctor()
extern "C"  void MiraLivePreviewEditor__ctor_m1020070001 (MiraLivePreviewEditor_t3425584316 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::.ctor()
extern "C"  void MiraLivePreviewPlayer__ctor_m3889256335 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	{
		// int btFrameCounter = 0;
		__this->set_btFrameCounter_9(0);
		// int btSendRate = 3;
		__this->set_btSendRate_10(3);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::OnEnable()
extern "C"  void MiraLivePreviewPlayer_OnEnable_m3827577147 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_OnEnable_m3827577147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// RemoteManager.Instance.OnRemoteConnected += RemoteConnected;
		IL2CPP_RUNTIME_CLASS_INIT(RemoteManager_t2208998541_il2cpp_TypeInfo_var);
		RemoteManager_t2208998541 * L_0 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		intptr_t L_1 = (intptr_t)MiraLivePreviewPlayer_RemoteConnected_m518057818_RuntimeMethod_var;
		RemoteConnectedEventHandler_t3456249665 * L_2 = (RemoteConnectedEventHandler_t3456249665 *)il2cpp_codegen_object_new(RemoteConnectedEventHandler_t3456249665_il2cpp_TypeInfo_var);
		RemoteConnectedEventHandler__ctor_m2482985582(L_2, __this, L_1, /*hidden argument*/NULL);
		// RemoteManager.Instance.OnRemoteConnected += RemoteConnected;
		NullCheck(L_0);
		RemoteManager_add_OnRemoteConnected_m3709435095(L_0, L_2, /*hidden argument*/NULL);
		// RemoteManager.Instance.OnRemoteDisconnected += RemoteDisconnected;
		RemoteManager_t2208998541 * L_3 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		intptr_t L_4 = (intptr_t)MiraLivePreviewPlayer_RemoteDisconnected_m2597138294_RuntimeMethod_var;
		RemoteDisconnectedEventHandler_t730656759 * L_5 = (RemoteDisconnectedEventHandler_t730656759 *)il2cpp_codegen_object_new(RemoteDisconnectedEventHandler_t730656759_il2cpp_TypeInfo_var);
		RemoteDisconnectedEventHandler__ctor_m1535770(L_5, __this, L_4, /*hidden argument*/NULL);
		// RemoteManager.Instance.OnRemoteDisconnected += RemoteDisconnected;
		NullCheck(L_3);
		RemoteManager_add_OnRemoteDisconnected_m2068840331(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::OnDisable()
extern "C"  void MiraLivePreviewPlayer_OnDisable_m1938345270 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_OnDisable_m1938345270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// RemoteManager.Instance.OnRemoteConnected -= RemoteConnected;
		IL2CPP_RUNTIME_CLASS_INIT(RemoteManager_t2208998541_il2cpp_TypeInfo_var);
		RemoteManager_t2208998541 * L_0 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		intptr_t L_1 = (intptr_t)MiraLivePreviewPlayer_RemoteConnected_m518057818_RuntimeMethod_var;
		RemoteConnectedEventHandler_t3456249665 * L_2 = (RemoteConnectedEventHandler_t3456249665 *)il2cpp_codegen_object_new(RemoteConnectedEventHandler_t3456249665_il2cpp_TypeInfo_var);
		RemoteConnectedEventHandler__ctor_m2482985582(L_2, __this, L_1, /*hidden argument*/NULL);
		// RemoteManager.Instance.OnRemoteConnected -= RemoteConnected;
		NullCheck(L_0);
		RemoteManager_remove_OnRemoteConnected_m2251201314(L_0, L_2, /*hidden argument*/NULL);
		// RemoteManager.Instance.OnRemoteDisconnected -= RemoteDisconnected;
		RemoteManager_t2208998541 * L_3 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		intptr_t L_4 = (intptr_t)MiraLivePreviewPlayer_RemoteDisconnected_m2597138294_RuntimeMethod_var;
		RemoteDisconnectedEventHandler_t730656759 * L_5 = (RemoteDisconnectedEventHandler_t730656759 *)il2cpp_codegen_object_new(RemoteDisconnectedEventHandler_t730656759_il2cpp_TypeInfo_var);
		RemoteDisconnectedEventHandler__ctor_m1535770(L_5, __this, L_4, /*hidden argument*/NULL);
		// RemoteManager.Instance.OnRemoteDisconnected -= RemoteDisconnected;
		NullCheck(L_3);
		RemoteManager_remove_OnRemoteDisconnected_m3027831764(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::Start()
extern "C"  void MiraLivePreviewPlayer_Start_m1179035 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_Start_m1179035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_userInput = new MiraBTRemoteInput();
		MiraBTRemoteInput_t988911721 * L_0 = (MiraBTRemoteInput_t988911721 *)il2cpp_codegen_object_new(MiraBTRemoteInput_t988911721_il2cpp_TypeInfo_var);
		MiraBTRemoteInput__ctor_m223735764(L_0, /*hidden argument*/NULL);
		((MiraLivePreviewPlayer_t3516305084_StaticFields*)il2cpp_codegen_static_fields_for(MiraLivePreviewPlayer_t3516305084_il2cpp_TypeInfo_var))->set_m_userInput_8(L_0);
		// bSessionActive = false;
		__this->set_bSessionActive_3((bool)0);
		// bTexturesInitialized = false;
		__this->set_bTexturesInitialized_6((bool)0);
		// InitializeTrackers();
		// InitializeTrackers();
		MiraLivePreviewPlayer_InitializeTrackers_m2398981726(__this, /*hidden argument*/NULL);
		// Debug.Log("STARTING ConnectToEditor");
		// Debug.Log("STARTING ConnectToEditor");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral2649185284, /*hidden argument*/NULL);
		// editorID = -1;
		__this->set_editorID_4((-1));
		// playerConnection = PlayerConnection.instance;
		// playerConnection = PlayerConnection.instance;
		PlayerConnection_t3517219175 * L_1 = PlayerConnection_get_instance_m1722843600(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_playerConnection_2(L_1);
		// playerConnection.RegisterConnection(EditorConnected);
		PlayerConnection_t3517219175 * L_2 = __this->get_playerConnection_2();
		intptr_t L_3 = (intptr_t)MiraLivePreviewPlayer_EditorConnected_m1048406888_RuntimeMethod_var;
		UnityAction_1_t3438463199 * L_4 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m490892142(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m490892142_RuntimeMethod_var);
		// playerConnection.RegisterConnection(EditorConnected);
		NullCheck(L_2);
		PlayerConnection_RegisterConnection_m2554115332(L_2, L_4, /*hidden argument*/NULL);
		// playerConnection.RegisterDisconnection(EditorDisconnected);
		PlayerConnection_t3517219175 * L_5 = __this->get_playerConnection_2();
		intptr_t L_6 = (intptr_t)MiraLivePreviewPlayer_EditorDisconnected_m3438779146_RuntimeMethod_var;
		UnityAction_1_t3438463199 * L_7 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m490892142(L_7, __this, L_6, /*hidden argument*/UnityAction_1__ctor_m490892142_RuntimeMethod_var);
		// playerConnection.RegisterDisconnection(EditorDisconnected);
		NullCheck(L_5);
		PlayerConnection_RegisterDisconnection_m541158256(L_5, L_7, /*hidden argument*/NULL);
		// playerConnection.Register(MiraConnectionMessageIds.fromEditorMiraSessionMsgId, HandleEditorMessage);
		PlayerConnection_t3517219175 * L_8 = __this->get_playerConnection_2();
		// playerConnection.Register(MiraConnectionMessageIds.fromEditorMiraSessionMsgId, HandleEditorMessage);
		Guid_t  L_9 = MiraConnectionMessageIds_get_fromEditorMiraSessionMsgId_m2831389603(NULL /*static, unused*/, /*hidden argument*/NULL);
		intptr_t L_10 = (intptr_t)MiraLivePreviewPlayer_HandleEditorMessage_m4127892255_RuntimeMethod_var;
		UnityAction_1_t1667869373 * L_11 = (UnityAction_1_t1667869373 *)il2cpp_codegen_object_new(UnityAction_1_t1667869373_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2955260543(L_11, __this, L_10, /*hidden argument*/UnityAction_1__ctor_m2955260543_RuntimeMethod_var);
		// playerConnection.Register(MiraConnectionMessageIds.fromEditorMiraSessionMsgId, HandleEditorMessage);
		NullCheck(L_8);
		PlayerConnection_Register_m2231017457(L_8, L_9, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::OnGUI()
extern "C"  void MiraLivePreviewPlayer_OnGUI_m4271455313 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_OnGUI_m4271455313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!bSessionActive) {
		bool L_0 = __this->get_bSessionActive_3();
		if (L_0)
		{
			goto IL_003d;
		}
	}
	{
		// GUI.Box(new Rect((Screen.width / 2) - 200, (Screen.height / 2), 400, 50), "Waiting for editor connection...");
		int32_t L_1 = Screen_get_width_m3526633787(NULL /*static, unused*/, /*hidden argument*/NULL);
		// GUI.Box(new Rect((Screen.width / 2) - 200, (Screen.height / 2), 400, 50), "Waiting for editor connection...");
		int32_t L_2 = Screen_get_height_m3831644396(NULL /*static, unused*/, /*hidden argument*/NULL);
		// GUI.Box(new Rect((Screen.width / 2) - 200, (Screen.height / 2), 400, 50), "Waiting for editor connection...");
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m2109726426((&L_3), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_1/(int32_t)2))-(int32_t)((int32_t)200)))))), (((float)((float)((int32_t)((int32_t)L_2/(int32_t)2))))), (400.0f), (50.0f), /*hidden argument*/NULL);
		// GUI.Box(new Rect((Screen.width / 2) - 200, (Screen.height / 2), 400, 50), "Waiting for editor connection...");
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Box_m1308812445(NULL /*static, unused*/, L_3, _stringLiteral3547176873, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::HandleEditorMessage(UnityEngine.Networking.PlayerConnection.MessageEventArgs)
extern "C"  void MiraLivePreviewPlayer_HandleEditorMessage_m4127892255 (MiraLivePreviewPlayer_t3516305084 * __this, MessageEventArgs_t301283622 * ___mea0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_HandleEditorMessage_m4127892255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableFromEditorMessage_t2894567809 * V_0 = NULL;
	bool V_1 = false;
	{
		// var message = mea.data.Deserialize<serializableFromEditorMessage>();
		MessageEventArgs_t301283622 * L_0 = ___mea0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_1 = L_0->get_data_1();
		// var message = mea.data.Deserialize<serializableFromEditorMessage>();
		serializableFromEditorMessage_t2894567809 * L_2 = ObjectSerializationExtension_Deserialize_TisserializableFromEditorMessage_t2894567809_m1238441737(NULL /*static, unused*/, L_1, /*hidden argument*/ObjectSerializationExtension_Deserialize_TisserializableFromEditorMessage_t2894567809_m1238441737_RuntimeMethod_var);
		V_0 = L_2;
		// if (message.subMessageId == MiraSubMessageIds.editorInitMiraRemote) {
		serializableFromEditorMessage_t2894567809 * L_3 = V_0;
		NullCheck(L_3);
		Guid_t  L_4 = L_3->get_subMessageId_0();
		// if (message.subMessageId == MiraSubMessageIds.editorInitMiraRemote) {
		Guid_t  L_5 = MiraSubMessageIds_get_editorInitMiraRemote_m1234016531(NULL /*static, unused*/, /*hidden argument*/NULL);
		// if (message.subMessageId == MiraSubMessageIds.editorInitMiraRemote) {
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_6 = Guid_op_Equality_m789465560(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		// bool isRotational = Convert.ToBoolean(message.bytes[0]);
		serializableFromEditorMessage_t2894567809 * L_7 = V_0;
		NullCheck(L_7);
		ByteU5BU5D_t3397334013* L_8 = L_7->get_bytes_1();
		NullCheck(L_8);
		int32_t L_9 = 0;
		uint8_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		// bool isRotational = Convert.ToBoolean(message.bytes[0]);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		bool L_11 = Convert_ToBoolean_m2032547942(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		// Debug.Log("Is rotational only: " + isRotational);
		bool L_12 = V_1;
		bool L_13 = L_12;
		RuntimeObject * L_14 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1887264353, L_14, /*hidden argument*/NULL);
		// Debug.Log("Is rotational only: " + isRotational);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		// GetComponent<MiraLivePreviewWikiConfig>().RotationalOnlyMode(isRotational);
		// GetComponent<MiraLivePreviewWikiConfig>().RotationalOnlyMode(isRotational);
		MiraLivePreviewWikiConfig_t2812595783 * L_16 = Component_GetComponent_TisMiraLivePreviewWikiConfig_t2812595783_m2483761890(__this, /*hidden argument*/Component_GetComponent_TisMiraLivePreviewWikiConfig_t2812595783_m2483761890_RuntimeMethod_var);
		bool L_17 = V_1;
		// GetComponent<MiraLivePreviewWikiConfig>().RotationalOnlyMode(isRotational);
		NullCheck(L_16);
		MiraLivePreviewWikiConfig_RotationalOnlyMode_m558320559(L_16, L_17, /*hidden argument*/NULL);
		// InitializeLivePreview();
		// InitializeLivePreview();
		MiraLivePreviewPlayer_InitializeLivePreview_m225778839(__this, /*hidden argument*/NULL);
		goto IL_008f;
	}

IL_005e:
	{
		// else if (message.subMessageId == MiraSubMessageIds.editorDisconnect)
		serializableFromEditorMessage_t2894567809 * L_18 = V_0;
		NullCheck(L_18);
		Guid_t  L_19 = L_18->get_subMessageId_0();
		// else if (message.subMessageId == MiraSubMessageIds.editorDisconnect)
		Guid_t  L_20 = MiraSubMessageIds_get_editorDisconnect_m2481842044(NULL /*static, unused*/, /*hidden argument*/NULL);
		// else if (message.subMessageId == MiraSubMessageIds.editorDisconnect)
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_21 = Guid_op_Equality_m789465560(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0086;
		}
	}
	{
		// EditorDisconnected(editorID);
		int32_t L_22 = __this->get_editorID_4();
		// EditorDisconnected(editorID);
		MiraLivePreviewPlayer_EditorDisconnected_m3438779146(__this, L_22, /*hidden argument*/NULL);
		goto IL_008f;
	}

IL_0086:
	{
		// ReceiveJPEGFrame(message);
		serializableFromEditorMessage_t2894567809 * L_23 = V_0;
		// ReceiveJPEGFrame(message);
		MiraLivePreviewPlayer_ReceiveJPEGFrame_m2269876361(__this, L_23, /*hidden argument*/NULL);
	}

IL_008f:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::InitializeLivePreview()
extern "C"  void MiraLivePreviewPlayer_InitializeLivePreview_m225778839 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_InitializeLivePreview_m225778839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Application.targetFrameRate = 60;
		// Application.targetFrameRate = 60;
		Application_set_targetFrameRate_m3037068888(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		// bSessionActive = true;
		__this->set_bSessionActive_3((bool)1);
		// InitializeTextures(transform.GetComponent<Camera>());
		// InitializeTextures(transform.GetComponent<Camera>());
		Transform_t3275118058 * L_0 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// InitializeTextures(transform.GetComponent<Camera>());
		NullCheck(L_0);
		Camera_t189460977 * L_1 = Component_GetComponent_TisCamera_t189460977_m3276577584(L_0, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var);
		// InitializeTextures(transform.GetComponent<Camera>());
		MiraLivePreviewPlayer_InitializeTextures_m3292283093(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::RemoteConnected(Remote,System.EventArgs)
extern "C"  void MiraLivePreviewPlayer_RemoteConnected_m518057818 (MiraLivePreviewPlayer_t3516305084 * __this, Remote_t660843562 * ___remote0, EventArgs_t3289624707 * ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_RemoteConnected_m518057818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_userInput.init();
		MiraBTRemoteInput_t988911721 * L_0 = ((MiraLivePreviewPlayer_t3516305084_StaticFields*)il2cpp_codegen_static_fields_for(MiraLivePreviewPlayer_t3516305084_il2cpp_TypeInfo_var))->get_m_userInput_8();
		// m_userInput.init();
		NullCheck(L_0);
		MiraBTRemoteInput_init_m66924122(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::RemoteDisconnected(Remote,System.EventArgs)
extern "C"  void MiraLivePreviewPlayer_RemoteDisconnected_m2597138294 (MiraLivePreviewPlayer_t3516305084 * __this, Remote_t660843562 * ___remote0, EventArgs_t3289624707 * ___args1, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::InitializeTextures(UnityEngine.Camera)
extern "C"  void MiraLivePreviewPlayer_InitializeTextures_m3292283093 (MiraLivePreviewPlayer_t3516305084 * __this, Camera_t189460977 * ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_InitializeTextures_m3292283093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int yWidth = camera.pixelWidth;
		Camera_t189460977 * L_0 = ___camera0;
		// int yWidth = camera.pixelWidth;
		NullCheck(L_0);
		int32_t L_1 = Camera_get_pixelWidth_m2894134608(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// int yHeight = camera.pixelHeight;
		Camera_t189460977 * L_2 = ___camera0;
		// int yHeight = camera.pixelHeight;
		NullCheck(L_2);
		int32_t L_3 = Camera_get_pixelHeight_m1187016139(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// if (liveViewScreenTex == null) {
		Texture2D_t3542995729 * L_4 = __this->get_liveViewScreenTex_5();
		// if (liveViewScreenTex == null) {
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0090;
		}
	}
	{
		// if (liveViewScreenTex) Destroy(liveViewScreenTex);
		Texture2D_t3542995729 * L_6 = __this->get_liveViewScreenTex_5();
		// if (liveViewScreenTex) Destroy(liveViewScreenTex);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		// if (liveViewScreenTex) Destroy(liveViewScreenTex);
		Texture2D_t3542995729 * L_8 = __this->get_liveViewScreenTex_5();
		// if (liveViewScreenTex) Destroy(liveViewScreenTex);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003c:
	{
		// liveViewScreenTex = new Texture2D(yWidth, yHeight, TextureFormat.RGB24, false, true);
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		// liveViewScreenTex = new Texture2D(yWidth, yHeight, TextureFormat.RGB24, false, true);
		Texture2D_t3542995729 * L_11 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m154655748(L_11, L_9, L_10, 3, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_liveViewScreenTex_5(L_11);
		// Debug.Log("GenTex: " + yWidth + " " + yHeight);
		ObjectU5BU5D_t3614634134* L_12 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral2663777903);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2663777903);
		ObjectU5BU5D_t3614634134* L_13 = L_12;
		int32_t L_14 = V_0;
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = L_13;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral372029310);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_18 = L_17;
		int32_t L_19 = V_1;
		int32_t L_20 = L_19;
		RuntimeObject * L_21 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3881798623(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		// Debug.Log("GenTex: " + yWidth + " " + yHeight);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		// camera.GetComponent<MiraARVideo>().m_clearTexture = liveViewScreenTex;
		Camera_t189460977 * L_23 = ___camera0;
		// camera.GetComponent<MiraARVideo>().m_clearTexture = liveViewScreenTex;
		NullCheck(L_23);
		MiraARVideo_t1477805071 * L_24 = Component_GetComponent_TisMiraARVideo_t1477805071_m2351498360(L_23, /*hidden argument*/Component_GetComponent_TisMiraARVideo_t1477805071_m2351498360_RuntimeMethod_var);
		Texture2D_t3542995729 * L_25 = __this->get_liveViewScreenTex_5();
		NullCheck(L_24);
		L_24->set_m_clearTexture_2(L_25);
	}

IL_0090:
	{
		// bTexturesInitialized = true;
		__this->set_bTexturesInitialized_6((bool)1);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::InitializeTrackers()
extern "C"  void MiraLivePreviewPlayer_InitializeTrackers_m2398981726 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_InitializeTrackers_m2398981726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImageTrackableU5BU5D_t322116539* V_0 = NULL;
	ImageTrackable_t3105654606 * V_1 = NULL;
	ImageTrackableU5BU5D_t322116539* V_2 = NULL;
	int32_t V_3 = 0;
	{
		// ImageTrackable[] trackers = GameObject.FindObjectsOfType<ImageTrackable>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ImageTrackableU5BU5D_t322116539* L_0 = Object_FindObjectsOfType_TisImageTrackable_t3105654606_m1655889936(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisImageTrackable_t3105654606_m1655889936_RuntimeMethod_var);
		V_0 = L_0;
		// if(trackers.Length > 0){
		ImageTrackableU5BU5D_t322116539* L_1 = V_0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		// foreach (ImageTrackable thisTracker in trackers)
		ImageTrackableU5BU5D_t322116539* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0053;
	}

IL_001b:
	{
		// foreach (ImageTrackable thisTracker in trackers)
		ImageTrackableU5BU5D_t322116539* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ImageTrackable_t3105654606 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		// thisTracker.OnImageRecognized.AddListener(OnTrackingFound);
		ImageTrackable_t3105654606 * L_7 = V_1;
		NullCheck(L_7);
		OnImageRecognizedEvent_t2106945187 * L_8 = L_7->get_OnImageRecognized_12();
		intptr_t L_9 = (intptr_t)MiraLivePreviewPlayer_OnTrackingFound_m3486069731_RuntimeMethod_var;
		UnityAction_1_t3381592573 * L_10 = (UnityAction_1_t3381592573 *)il2cpp_codegen_object_new(UnityAction_1_t3381592573_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m1430463559(L_10, __this, L_9, /*hidden argument*/UnityAction_1__ctor_m1430463559_RuntimeMethod_var);
		// thisTracker.OnImageRecognized.AddListener(OnTrackingFound);
		NullCheck(L_8);
		UnityEvent_1_AddListener_m394150843(L_8, L_10, /*hidden argument*/UnityEvent_1_AddListener_m394150843_RuntimeMethod_var);
		// thisTracker.OnImageLost.AddListener(OnTrackingLost);
		ImageTrackable_t3105654606 * L_11 = V_1;
		NullCheck(L_11);
		OnImageLostEvent_t2609021075 * L_12 = L_11->get_OnImageLost_13();
		intptr_t L_13 = (intptr_t)MiraLivePreviewPlayer_OnTrackingLost_m2787869957_RuntimeMethod_var;
		UnityAction_1_t3381592573 * L_14 = (UnityAction_1_t3381592573 *)il2cpp_codegen_object_new(UnityAction_1_t3381592573_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m1430463559(L_14, __this, L_13, /*hidden argument*/UnityAction_1__ctor_m1430463559_RuntimeMethod_var);
		// thisTracker.OnImageLost.AddListener(OnTrackingLost);
		NullCheck(L_12);
		UnityEvent_1_AddListener_m394150843(L_12, L_14, /*hidden argument*/UnityEvent_1_AddListener_m394150843_RuntimeMethod_var);
		// foreach (ImageTrackable thisTracker in trackers)
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_16 = V_3;
		ImageTrackableU5BU5D_t322116539* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
	}

IL_005d:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::ReceiveJPEGFrame(UnityEngine.Networking.PlayerConnection.MessageEventArgs)
extern "C"  void MiraLivePreviewPlayer_ReceiveJPEGFrame_m1895749053 (MiraLivePreviewPlayer_t3516305084 * __this, MessageEventArgs_t301283622 * ___mea0, const RuntimeMethod* method)
{
	{
		// if (!bTexturesInitialized) return;
		bool L_0 = __this->get_bTexturesInitialized_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (!bTexturesInitialized) return;
		goto IL_0023;
	}

IL_0011:
	{
		// liveViewScreenTex.LoadImage(mea.data);
		Texture2D_t3542995729 * L_1 = __this->get_liveViewScreenTex_5();
		MessageEventArgs_t301283622 * L_2 = ___mea0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = L_2->get_data_1();
		// liveViewScreenTex.LoadImage(mea.data);
		ImageConversion_LoadImage_m3156064641(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::ReceiveJPEGFrame(Utils.serializableFromEditorMessage)
extern "C"  void MiraLivePreviewPlayer_ReceiveJPEGFrame_m2269876361 (MiraLivePreviewPlayer_t3516305084 * __this, serializableFromEditorMessage_t2894567809 * ___message0, const RuntimeMethod* method)
{
	{
		// if (!bTexturesInitialized) return;
		bool L_0 = __this->get_bTexturesInitialized_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (!bTexturesInitialized) return;
		goto IL_0023;
	}

IL_0011:
	{
		// liveViewScreenTex.LoadImage(message.bytes);
		Texture2D_t3542995729 * L_1 = __this->get_liveViewScreenTex_5();
		serializableFromEditorMessage_t2894567809 * L_2 = ___message0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = L_2->get_bytes_1();
		// liveViewScreenTex.LoadImage(message.bytes);
		ImageConversion_LoadImage_m3156064641(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::Update()
extern "C"  void MiraLivePreviewPlayer_Update_m1830306472 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_Update_m1830306472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (bSessionActive)
		bool L_0 = __this->get_bSessionActive_3();
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		// UpdateGyro(Input.gyro);
		// UpdateGyro(Input.gyro);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Gyroscope_t1705362817 * L_1 = Input_get_gyro_m1176661367(NULL /*static, unused*/, /*hidden argument*/NULL);
		// UpdateGyro(Input.gyro);
		MiraLivePreviewPlayer_UpdateGyro_m2549312003(__this, L_1, /*hidden argument*/NULL);
		// UpdateBTRemote();
		// UpdateBTRemote();
		MiraLivePreviewPlayer_UpdateBTRemote_m820487388(__this, /*hidden argument*/NULL);
		// if(isTracking)
		bool L_2 = __this->get_isTracking_7();
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		// UpdateWikiCam();
		// UpdateWikiCam();
		MiraLivePreviewPlayer_UpdateWikiCam_m493785173(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::UpdateGyro(UnityEngine.Gyroscope)
extern "C"  void MiraLivePreviewPlayer_UpdateGyro_m2549312003 (MiraLivePreviewPlayer_t3516305084 * __this, Gyroscope_t1705362817 * ___gyro0, const RuntimeMethod* method)
{
	serializableGyroscope_t1293559986 * V_0 = NULL;
	{
		// serializableGyroscope sGyro = gyro;
		Gyroscope_t1705362817 * L_0 = ___gyro0;
		// serializableGyroscope sGyro = gyro;
		serializableGyroscope_t1293559986 * L_1 = serializableGyroscope_op_Implicit_m1484759557(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// SendToEditor(MiraConnectionMessageIds.gyroMsgId, sGyro);
		// SendToEditor(MiraConnectionMessageIds.gyroMsgId, sGyro);
		Guid_t  L_2 = MiraConnectionMessageIds_get_gyroMsgId_m3152547904(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableGyroscope_t1293559986 * L_3 = V_0;
		// SendToEditor(MiraConnectionMessageIds.gyroMsgId, sGyro);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::UpdateWikiCam()
extern "C"  void MiraLivePreviewPlayer_UpdateWikiCam_m493785173 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	serializableTransform_t2202979937 * V_0 = NULL;
	{
		// serializableTransform sWikiCam = Camera.main.transform;
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		// serializableTransform sWikiCam = Camera.main.transform;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(L_0, /*hidden argument*/NULL);
		// serializableTransform sWikiCam = Camera.main.transform;
		serializableTransform_t2202979937 * L_2 = serializableTransform_op_Implicit_m504541074(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// SendToEditor(MiraConnectionMessageIds.wikiCamMsgId, sWikiCam);
		// SendToEditor(MiraConnectionMessageIds.wikiCamMsgId, sWikiCam);
		Guid_t  L_3 = MiraConnectionMessageIds_get_wikiCamMsgId_m3438711336(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableTransform_t2202979937 * L_4 = V_0;
		// SendToEditor(MiraConnectionMessageIds.wikiCamMsgId, sWikiCam);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::OnTrackingFound(Wikitude.ImageTarget)
extern "C"  void MiraLivePreviewPlayer_OnTrackingFound_m3486069731 (MiraLivePreviewPlayer_t3516305084 * __this, ImageTarget_t2015006822 * ___imgTarget0, const RuntimeMethod* method)
{
	serializableFloat_t3811907803 * V_0 = NULL;
	{
		// serializableFloat targetHeight = imgTarget.PhysicalTargetHeight;
		ImageTarget_t2015006822 * L_0 = ___imgTarget0;
		// serializableFloat targetHeight = imgTarget.PhysicalTargetHeight;
		NullCheck(L_0);
		float L_1 = ImageTarget_get_PhysicalTargetHeight_m3486992189(L_0, /*hidden argument*/NULL);
		// serializableFloat targetHeight = imgTarget.PhysicalTargetHeight;
		serializableFloat_t3811907803 * L_2 = serializableFloat_op_Implicit_m3403528258(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// isTracking = true;
		__this->set_isTracking_7((bool)1);
		// SendToEditor(MiraConnectionMessageIds.trackingFoundMsgId, targetHeight);
		// SendToEditor(MiraConnectionMessageIds.trackingFoundMsgId, targetHeight);
		Guid_t  L_3 = MiraConnectionMessageIds_get_trackingFoundMsgId_m3240602256(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableFloat_t3811907803 * L_4 = V_0;
		// SendToEditor(MiraConnectionMessageIds.trackingFoundMsgId, targetHeight);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::OnTrackingLost(Wikitude.ImageTarget)
extern "C"  void MiraLivePreviewPlayer_OnTrackingLost_m2787869957 (MiraLivePreviewPlayer_t3516305084 * __this, ImageTarget_t2015006822 * ___imgTarget0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_OnTrackingLost_m2787869957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isTracking = false;
		__this->set_isTracking_7((bool)0);
		// SendToEditor(MiraConnectionMessageIds.trackingLostMsgId, true);
		// SendToEditor(MiraConnectionMessageIds.trackingLostMsgId, true);
		Guid_t  L_0 = MiraConnectionMessageIds_get_trackingLostMsgId_m196996400(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ((bool)1);
		RuntimeObject * L_2 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_1);
		// SendToEditor(MiraConnectionMessageIds.trackingLostMsgId, true);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_0, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::UpdateBTRemote()
extern "C"  void MiraLivePreviewPlayer_UpdateBTRemote_m820487388 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_UpdateBTRemote_m820487388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableBTRemote_t622411689 * V_0 = NULL;
	serializableBTRemoteButtons_t2327889448 * V_1 = NULL;
	serializableBTRemoteTouchPad_t2020069673 * V_2 = NULL;
	{
		// if(RemoteManager.Instance.connectedRemote != null)
		IL2CPP_RUNTIME_CLASS_INIT(RemoteManager_t2208998541_il2cpp_TypeInfo_var);
		RemoteManager_t2208998541 * L_0 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		// if(RemoteManager.Instance.connectedRemote != null)
		NullCheck(L_0);
		Remote_t660843562 * L_1 = RemoteManager_get_connectedRemote_m2850669673(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		// serializableBTRemote sBTRemote = RemoteManager.Instance.connectedRemote;
		IL2CPP_RUNTIME_CLASS_INIT(RemoteManager_t2208998541_il2cpp_TypeInfo_var);
		RemoteManager_t2208998541 * L_2 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		// serializableBTRemote sBTRemote = RemoteManager.Instance.connectedRemote;
		NullCheck(L_2);
		Remote_t660843562 * L_3 = RemoteManager_get_connectedRemote_m2850669673(L_2, /*hidden argument*/NULL);
		// serializableBTRemote sBTRemote = RemoteManager.Instance.connectedRemote;
		serializableBTRemote_t622411689 * L_4 = serializableBTRemote_op_Implicit_m3134586455(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// SendToEditor(MiraConnectionMessageIds.BTRemoteMsgId, sBTRemote);
		// SendToEditor(MiraConnectionMessageIds.BTRemoteMsgId, sBTRemote);
		Guid_t  L_5 = MiraConnectionMessageIds_get_BTRemoteMsgId_m738691607(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableBTRemote_t622411689 * L_6 = V_0;
		// SendToEditor(MiraConnectionMessageIds.BTRemoteMsgId, sBTRemote);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_5, L_6, /*hidden argument*/NULL);
		// if(btFrameCounter < btSendRate)
		int32_t L_7 = __this->get_btFrameCounter_9();
		int32_t L_8 = __this->get_btSendRate_10();
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0051;
		}
	}
	{
		// btFrameCounter += 1;
		int32_t L_9 = __this->get_btFrameCounter_9();
		__this->set_btFrameCounter_9(((int32_t)((int32_t)L_9+(int32_t)1)));
		goto IL_0092;
	}

IL_0051:
	{
		// serializableBTRemoteButtons sBTRemoteButtons = RemoteManager.Instance.connectedRemote;
		IL2CPP_RUNTIME_CLASS_INIT(RemoteManager_t2208998541_il2cpp_TypeInfo_var);
		RemoteManager_t2208998541 * L_10 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		// serializableBTRemoteButtons sBTRemoteButtons = RemoteManager.Instance.connectedRemote;
		NullCheck(L_10);
		Remote_t660843562 * L_11 = RemoteManager_get_connectedRemote_m2850669673(L_10, /*hidden argument*/NULL);
		// serializableBTRemoteButtons sBTRemoteButtons = RemoteManager.Instance.connectedRemote;
		serializableBTRemoteButtons_t2327889448 * L_12 = serializableBTRemoteButtons_op_Implicit_m892522079(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		// serializableBTRemoteTouchPad sBTRemoteTouchPad = RemoteManager.Instance.connectedRemote;
		RemoteManager_t2208998541 * L_13 = ((RemoteManager_t2208998541_StaticFields*)il2cpp_codegen_static_fields_for(RemoteManager_t2208998541_il2cpp_TypeInfo_var))->get_Instance_4();
		// serializableBTRemoteTouchPad sBTRemoteTouchPad = RemoteManager.Instance.connectedRemote;
		NullCheck(L_13);
		Remote_t660843562 * L_14 = RemoteManager_get_connectedRemote_m2850669673(L_13, /*hidden argument*/NULL);
		// serializableBTRemoteTouchPad sBTRemoteTouchPad = RemoteManager.Instance.connectedRemote;
		serializableBTRemoteTouchPad_t2020069673 * L_15 = serializableBTRemoteTouchPad_op_Implicit_m1392338071(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		// SendToEditor(MiraConnectionMessageIds.BTRemoteButtonsMsgId, sBTRemoteButtons);
		// SendToEditor(MiraConnectionMessageIds.BTRemoteButtonsMsgId, sBTRemoteButtons);
		Guid_t  L_16 = MiraConnectionMessageIds_get_BTRemoteButtonsMsgId_m1963856294(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableBTRemoteButtons_t2327889448 * L_17 = V_1;
		// SendToEditor(MiraConnectionMessageIds.BTRemoteButtonsMsgId, sBTRemoteButtons);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_16, L_17, /*hidden argument*/NULL);
		// SendToEditor(MiraConnectionMessageIds.BTRemoteTouchPadMsgId, sBTRemoteTouchPad);
		// SendToEditor(MiraConnectionMessageIds.BTRemoteTouchPadMsgId, sBTRemoteTouchPad);
		Guid_t  L_18 = MiraConnectionMessageIds_get_BTRemoteTouchPadMsgId_m1308740695(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_19 = V_2;
		// SendToEditor(MiraConnectionMessageIds.BTRemoteTouchPadMsgId, sBTRemoteTouchPad);
		MiraLivePreviewPlayer_SendToEditor_m1266648443(__this, L_18, L_19, /*hidden argument*/NULL);
		// btFrameCounter = 0;
		__this->set_btFrameCounter_9(0);
	}

IL_0092:
	{
	}

IL_0093:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::EditorConnected(System.Int32)
extern "C"  void MiraLivePreviewPlayer_EditorConnected_m1048406888 (MiraLivePreviewPlayer_t3516305084 * __this, int32_t ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_EditorConnected_m1048406888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Connected to Editor");
		// Debug.Log("Connected to Editor");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral3269606001, /*hidden argument*/NULL);
		// editorID = playerID;
		int32_t L_0 = ___playerID0;
		__this->set_editorID_4(L_0);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::EditorDisconnected(System.Int32)
extern "C"  void MiraLivePreviewPlayer_EditorDisconnected_m3438779146 (MiraLivePreviewPlayer_t3516305084 * __this, int32_t ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_EditorDisconnected_m3438779146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Editor has been disconnected");
		// Debug.Log("Editor has been disconnected");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral2474827406, /*hidden argument*/NULL);
		// DisconnectFromEditor();
		// DisconnectFromEditor();
		MiraLivePreviewPlayer_DisconnectFromEditor_m616998130(__this, /*hidden argument*/NULL);
		// if (bSessionActive) {
		bool L_0 = __this->get_bSessionActive_3();
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		// bSessionActive = false;
		__this->set_bSessionActive_3((bool)0);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::SendToEditor(System.Guid,System.Object)
extern "C"  void MiraLivePreviewPlayer_SendToEditor_m1266648443 (MiraLivePreviewPlayer_t3516305084 * __this, Guid_t  ___msgId0, RuntimeObject * ___serializableObject1, const RuntimeMethod* method)
{
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		// byte[] arrayToSend = serializableObject.SerializeToByteArray();
		RuntimeObject * L_0 = ___serializableObject1;
		// byte[] arrayToSend = serializableObject.SerializeToByteArray();
		ByteU5BU5D_t3397334013* L_1 = ObjectSerializationExtension_SerializeToByteArray_m2132233038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// SendToEditor(msgId, arrayToSend);
		Guid_t  L_2 = ___msgId0;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		// SendToEditor(msgId, arrayToSend);
		MiraLivePreviewPlayer_SendToEditor_m3889611786(__this, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::SendToEditor(System.Guid,System.Byte[])
extern "C"  void MiraLivePreviewPlayer_SendToEditor_m3889611786 (MiraLivePreviewPlayer_t3516305084 * __this, Guid_t  ___msgId0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method)
{
	{
		// if (playerConnection.isConnected) {
		PlayerConnection_t3517219175 * L_0 = __this->get_playerConnection_2();
		// if (playerConnection.isConnected) {
		NullCheck(L_0);
		bool L_1 = PlayerConnection_get_isConnected_m2418883912(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		// playerConnection.Send(msgId, data);
		PlayerConnection_t3517219175 * L_2 = __this->get_playerConnection_2();
		Guid_t  L_3 = ___msgId0;
		ByteU5BU5D_t3397334013* L_4 = ___data1;
		// playerConnection.Send(msgId, data);
		NullCheck(L_2);
		PlayerConnection_Send_m1490707979(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewPlayer::DisconnectFromEditor()
extern "C"  void MiraLivePreviewPlayer_DisconnectFromEditor_m616998130 (MiraLivePreviewPlayer_t3516305084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraLivePreviewPlayer_DisconnectFromEditor_m616998130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Disconnect from editor succeeded");
		// Debug.Log("Disconnect from editor succeeded");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral1086818976, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.MiraLivePreviewVideo::.ctor()
extern "C"  void MiraLivePreviewVideo__ctor_m2029448873 (MiraLivePreviewVideo_t2362804972 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraSubMessageIds::get_editorInitMiraRemote()
extern "C"  Guid_t  MiraSubMessageIds_get_editorInitMiraRemote_m1234016531 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraSubMessageIds_get_editorInitMiraRemote_m1234016531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid editorInitMiraRemote { get { return new Guid("2e5d7c45-daef-474d-bf55-1f02f0a10b69"); } }
		// public static Guid editorInitMiraRemote { get { return new Guid("2e5d7c45-daef-474d-bf55-1f02f0a10b69"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral3513490258, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid editorInitMiraRemote { get { return new Guid("2e5d7c45-daef-474d-bf55-1f02f0a10b69"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraSubMessageIds::get_editorDisconnect()
extern "C"  Guid_t  MiraSubMessageIds_get_editorDisconnect_m2481842044 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraSubMessageIds_get_editorDisconnect_m2481842044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid editorDisconnect { get { return new Guid("45138e0e-151f-4b89-a18c-c828d3439eb6"); } }
		// public static Guid editorDisconnect { get { return new Guid("45138e0e-151f-4b89-a18c-c828d3439eb6"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral3925036185, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid editorDisconnect { get { return new Guid("45138e0e-151f-4b89-a18c-c828d3439eb6"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Guid UnityEngine.XR.iOS.MiraSubMessageIds::get_screenCaptureJPEGMsgID()
extern "C"  Guid_t  MiraSubMessageIds_get_screenCaptureJPEGMsgID_m3453747459 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiraSubMessageIds_get_screenCaptureJPEGMsgID_m3453747459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public static Guid screenCaptureJPEGMsgID { get { return new Guid("3f8ae47c-3949-4949-b74f-4deeba378a95"); } }
		// public static Guid screenCaptureJPEGMsgID { get { return new Guid("3f8ae47c-3949-4949-b74f-4deeba378a95"); } }
		Guid_t  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Guid__ctor_m2599802704((&L_0), _stringLiteral1825940431, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		// public static Guid screenCaptureJPEGMsgID { get { return new Guid("3f8ae47c-3949-4949-b74f-4deeba378a95"); } }
		Guid_t  L_1 = V_0;
		return L_1;
	}
}
// System.Byte[] Utils.ObjectSerializationExtension::SerializeToByteArray(System.Object)
extern "C"  ByteU5BU5D_t3397334013* ObjectSerializationExtension_SerializeToByteArray_m2132233038 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectSerializationExtension_SerializeToByteArray_m2132233038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	BinaryFormatter_t1866979105 * V_1 = NULL;
	MemoryStream_t743994179 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (obj == null)
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// return null;
		V_0 = (ByteU5BU5D_t3397334013*)NULL;
		goto IL_003d;
	}

IL_000f:
	{
		// var bf = new BinaryFormatter();
		BinaryFormatter_t1866979105 * L_1 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		// using (var ms = new MemoryStream())
		MemoryStream_t743994179 * L_2 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		// bf.Serialize(ms, obj);
		BinaryFormatter_t1866979105 * L_3 = V_1;
		MemoryStream_t743994179 * L_4 = V_2;
		RuntimeObject * L_5 = ___obj0;
		// bf.Serialize(ms, obj);
		NullCheck(L_3);
		BinaryFormatter_Serialize_m433301673(L_3, L_4, L_5, /*hidden argument*/NULL);
		// return ms.ToArray();
		MemoryStream_t743994179 * L_6 = V_2;
		// return ms.ToArray();
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_7 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_6);
		V_0 = L_7;
		IL2CPP_LEAVE(0x3D, FINALLY_0030);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0036:
		{
			MemoryStream_t743994179 * L_9 = V_2;
			// using (var ms = new MemoryStream())
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		// }
		ByteU5BU5D_t3397334013* L_10 = V_0;
		return L_10;
	}
}
// System.Void Utils.serializableBTRemote::.ctor(Utils.SerializableVector3,Utils.SerializableVector3,Utils.SerializableVector3)
extern "C"  void serializableBTRemote__ctor_m3258305359 (serializableBTRemote_t622411689 * __this, SerializableVector3_t4294681249  ____orientation0, SerializableVector3_t4294681249  ____rotationRate1, SerializableVector3_t4294681249  ____acceleration2, const RuntimeMethod* method)
{
	{
		// public serializableBTRemote(SerializableVector3 _orientation, SerializableVector3 _rotationRate, SerializableVector3 _acceleration) {
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// orientation = _orientation;
		SerializableVector3_t4294681249  L_0 = ____orientation0;
		__this->set_orientation_0(L_0);
		// rotationRate = _rotationRate;
		SerializableVector3_t4294681249  L_1 = ____rotationRate1;
		__this->set_rotationRate_1(L_1);
		// acceleration = _acceleration;
		SerializableVector3_t4294681249  L_2 = ____acceleration2;
		__this->set_acceleration_2(L_2);
		// }
		return;
	}
}
// Utils.serializableBTRemote Utils.serializableBTRemote::op_Implicit(Remote)
extern "C"  serializableBTRemote_t622411689 * serializableBTRemote_op_Implicit_m3134586455 (RuntimeObject * __this /* static, unused */, Remote_t660843562 * ____btValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableBTRemote_op_Implicit_m3134586455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableBTRemote_t622411689 * V_0 = NULL;
	{
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		Remote_t660843562 * L_0 = ____btValue0;
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_0);
		RemoteMotionInput_t3548926620 * L_1 = RemoteBase_get_motion_m1992541006(L_0, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_1);
		RemoteOrientationInput_t3303200544 * L_2 = RemoteMotionInput_get_orientation_m4167617963(L_1, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = RemoteOrientationInput_getOrientationVector_m1923861616(L_2, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		SerializableVector3_t4294681249  L_4 = SerializableVector3_op_Implicit_m2796142158(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Remote_t660843562 * L_5 = ____btValue0;
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_5);
		RemoteMotionInput_t3548926620 * L_6 = RemoteBase_get_motion_m1992541006(L_5, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_6);
		RemoteMotionSensorInput_t841531810 * L_7 = RemoteMotionInput_get_rotationRate_m3968900193(L_6, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = RemoteMotionSensorInput_getMotionSensorVector_m487213716(L_7, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		SerializableVector3_t4294681249  L_9 = SerializableVector3_op_Implicit_m2796142158(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Remote_t660843562 * L_10 = ____btValue0;
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_10);
		RemoteMotionInput_t3548926620 * L_11 = RemoteBase_get_motion_m1992541006(L_10, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_11);
		RemoteMotionSensorInput_t841531810 * L_12 = RemoteMotionInput_get_acceleration_m2249299093(L_11, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = RemoteMotionSensorInput_getMotionSensorVector_m487213716(L_12, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		SerializableVector3_t4294681249  L_14 = SerializableVector3_op_Implicit_m2796142158(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		// return new serializableBTRemote(_btValue.motion.orientation.getOrientationVector(), _btValue.motion.rotationRate.getMotionSensorVector(), _btValue.motion.acceleration.getMotionSensorVector());
		serializableBTRemote_t622411689 * L_15 = (serializableBTRemote_t622411689 *)il2cpp_codegen_object_new(serializableBTRemote_t622411689_il2cpp_TypeInfo_var);
		serializableBTRemote__ctor_m3258305359(L_15, L_4, L_9, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_004b;
	}

IL_004b:
	{
		// }
		serializableBTRemote_t622411689 * L_16 = V_0;
		return L_16;
	}
}
// System.Void Utils.serializableBTRemoteButtons::.ctor(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void serializableBTRemoteButtons__ctor_m3640121971 (serializableBTRemoteButtons_t2327889448 * __this, bool ____startButton0, bool ____backButton1, bool ____triggerButton2, const RuntimeMethod* method)
{
	{
		// public serializableBTRemoteButtons(bool _startButton, bool _backButton, bool _triggerButton) {
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// startButton = _startButton;
		bool L_0 = ____startButton0;
		__this->set_startButton_0(L_0);
		// backButton = _backButton;
		bool L_1 = ____backButton1;
		__this->set_backButton_1(L_1);
		// triggerButton = _triggerButton;
		bool L_2 = ____triggerButton2;
		__this->set_triggerButton_2(L_2);
		// }
		return;
	}
}
// Utils.serializableBTRemoteButtons Utils.serializableBTRemoteButtons::op_Implicit(Remote)
extern "C"  serializableBTRemoteButtons_t2327889448 * serializableBTRemoteButtons_op_Implicit_m892522079 (RuntimeObject * __this /* static, unused */, Remote_t660843562 * ____bValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableBTRemoteButtons_op_Implicit_m892522079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableBTRemoteButtons_t2327889448 * V_0 = NULL;
	{
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		Remote_t660843562 * L_0 = ____bValue0;
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		NullCheck(L_0);
		RemoteButtonInput_t144791130 * L_1 = RemoteBase_get_homeButton_m2789731865(L_0, /*hidden argument*/NULL);
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		NullCheck(L_1);
		bool L_2 = RemoteButtonInput_get_isPressed_m2552376858(L_1, /*hidden argument*/NULL);
		Remote_t660843562 * L_3 = ____bValue0;
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		NullCheck(L_3);
		RemoteButtonInput_t144791130 * L_4 = RemoteBase_get_menuButton_m604848353(L_3, /*hidden argument*/NULL);
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		NullCheck(L_4);
		bool L_5 = RemoteButtonInput_get_isPressed_m2552376858(L_4, /*hidden argument*/NULL);
		Remote_t660843562 * L_6 = ____bValue0;
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		NullCheck(L_6);
		RemoteButtonInput_t144791130 * L_7 = RemoteBase_get_trigger_m2301973040(L_6, /*hidden argument*/NULL);
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		NullCheck(L_7);
		bool L_8 = RemoteButtonInput_get_isPressed_m2552376858(L_7, /*hidden argument*/NULL);
		// return new serializableBTRemoteButtons(_bValue.homeButton.isPressed, _bValue.menuButton.isPressed, _bValue.trigger.isPressed);
		serializableBTRemoteButtons_t2327889448 * L_9 = (serializableBTRemoteButtons_t2327889448 *)il2cpp_codegen_object_new(serializableBTRemoteButtons_t2327889448_il2cpp_TypeInfo_var);
		serializableBTRemoteButtons__ctor_m3640121971(L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_002d;
	}

IL_002d:
	{
		// }
		serializableBTRemoteButtons_t2327889448 * L_10 = V_0;
		return L_10;
	}
}
// System.Void Utils.serializableBTRemoteTouchPad::.ctor(System.Boolean,System.Boolean,Utils.SerializableVector2,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void serializableBTRemoteTouchPad__ctor_m15664844 (serializableBTRemoteTouchPad_t2020069673 * __this, bool ____TouchActive0, bool ____TouchpadButton1, SerializableVector2_t4294681248  ____TouchPos2, bool ____UpButton3, bool ____DownButton4, bool ____LeftButton5, bool ____RightButton6, const RuntimeMethod* method)
{
	{
		// public serializableBTRemoteTouchPad(bool _TouchActive, bool _TouchpadButton,
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// touchActive = _TouchActive;
		bool L_0 = ____TouchActive0;
		__this->set_touchActive_0(L_0);
		// touchPos = _TouchPos;
		SerializableVector2_t4294681248  L_1 = ____TouchPos2;
		__this->set_touchPos_2(L_1);
		// touchButton = _TouchpadButton;
		bool L_2 = ____TouchpadButton1;
		__this->set_touchButton_1(L_2);
		// upButton = _UpButton;
		bool L_3 = ____UpButton3;
		__this->set_upButton_3(L_3);
		// downButton = _DownButton;
		bool L_4 = ____DownButton4;
		__this->set_downButton_4(L_4);
		// leftButton = _LeftButton;
		bool L_5 = ____LeftButton5;
		__this->set_leftButton_5(L_5);
		// rightButton = _RightButton;
		bool L_6 = ____RightButton6;
		__this->set_rightButton_6(L_6);
		// }
		return;
	}
}
// Utils.serializableBTRemoteTouchPad Utils.serializableBTRemoteTouchPad::op_Implicit(Remote)
extern "C"  serializableBTRemoteTouchPad_t2020069673 * serializableBTRemoteTouchPad_op_Implicit_m1392338071 (RuntimeObject * __this /* static, unused */, Remote_t660843562 * ____tpValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableBTRemoteTouchPad_op_Implicit_m1392338071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableBTRemoteTouchPad_t2020069673 * V_0 = NULL;
	{
		// return new serializableBTRemoteTouchPad(
		Remote_t660843562 * L_0 = ____tpValue0;
		// _tpValue.touchPad.isActive,
		NullCheck(L_0);
		RemoteTouchPadInput_t1081319266 * L_1 = RemoteBase_get_touchPad_m3247253518(L_0, /*hidden argument*/NULL);
		// _tpValue.touchPad.isActive,
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean RemoteTouchInput::get_isActive() */, L_1);
		Remote_t660843562 * L_3 = ____tpValue0;
		// _tpValue.touchPad.button.isPressed,
		NullCheck(L_3);
		RemoteTouchPadInput_t1081319266 * L_4 = RemoteBase_get_touchPad_m3247253518(L_3, /*hidden argument*/NULL);
		// _tpValue.touchPad.button.isPressed,
		NullCheck(L_4);
		RemoteButtonInput_t144791130 * L_5 = RemoteTouchPadInput_get_button_m341096981(L_4, /*hidden argument*/NULL);
		// _tpValue.touchPad.button.isPressed,
		NullCheck(L_5);
		bool L_6 = RemoteButtonInput_get_isPressed_m2552376858(L_5, /*hidden argument*/NULL);
		Remote_t660843562 * L_7 = ____tpValue0;
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		NullCheck(L_7);
		RemoteTouchPadInput_t1081319266 * L_8 = RemoteBase_get_touchPad_m3247253518(L_7, /*hidden argument*/NULL);
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		NullCheck(L_8);
		RemoteAxisInput_t2770128439 * L_9 = RemoteTouchPadInput_get_xAxis_m960950733(L_8, /*hidden argument*/NULL);
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		NullCheck(L_9);
		float L_10 = RemoteAxisInput_get_value_m2540058260(L_9, /*hidden argument*/NULL);
		Remote_t660843562 * L_11 = ____tpValue0;
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		NullCheck(L_11);
		RemoteTouchPadInput_t1081319266 * L_12 = RemoteBase_get_touchPad_m3247253518(L_11, /*hidden argument*/NULL);
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		NullCheck(L_12);
		RemoteAxisInput_t2770128439 * L_13 = RemoteTouchPadInput_get_yAxis_m3985318738(L_12, /*hidden argument*/NULL);
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		NullCheck(L_13);
		float L_14 = RemoteAxisInput_get_value_m2540058260(L_13, /*hidden argument*/NULL);
		// new SerializableVector2(_tpValue.touchPad.xAxis.value, _tpValue.touchPad.yAxis.value),
		SerializableVector2_t4294681248  L_15;
		memset(&L_15, 0, sizeof(L_15));
		SerializableVector2__ctor_m429252694((&L_15), L_10, L_14, /*hidden argument*/NULL);
		Remote_t660843562 * L_16 = ____tpValue0;
		// _tpValue.touchPad.up.isActive,
		NullCheck(L_16);
		RemoteTouchPadInput_t1081319266 * L_17 = RemoteBase_get_touchPad_m3247253518(L_16, /*hidden argument*/NULL);
		// _tpValue.touchPad.up.isActive,
		NullCheck(L_17);
		RemoteTouchInput_t3826108381 * L_18 = RemoteTouchPadInput_get_up_m1922390201(L_17, /*hidden argument*/NULL);
		// _tpValue.touchPad.up.isActive,
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean RemoteTouchInput::get_isActive() */, L_18);
		Remote_t660843562 * L_20 = ____tpValue0;
		// _tpValue.touchPad.down.isActive,
		NullCheck(L_20);
		RemoteTouchPadInput_t1081319266 * L_21 = RemoteBase_get_touchPad_m3247253518(L_20, /*hidden argument*/NULL);
		// _tpValue.touchPad.down.isActive,
		NullCheck(L_21);
		RemoteTouchInput_t3826108381 * L_22 = RemoteTouchPadInput_get_down_m3468287018(L_21, /*hidden argument*/NULL);
		// _tpValue.touchPad.down.isActive,
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean RemoteTouchInput::get_isActive() */, L_22);
		Remote_t660843562 * L_24 = ____tpValue0;
		// _tpValue.touchPad.left.isActive,
		NullCheck(L_24);
		RemoteTouchPadInput_t1081319266 * L_25 = RemoteBase_get_touchPad_m3247253518(L_24, /*hidden argument*/NULL);
		// _tpValue.touchPad.left.isActive,
		NullCheck(L_25);
		RemoteTouchInput_t3826108381 * L_26 = RemoteTouchPadInput_get_left_m1876375955(L_25, /*hidden argument*/NULL);
		// _tpValue.touchPad.left.isActive,
		NullCheck(L_26);
		bool L_27 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean RemoteTouchInput::get_isActive() */, L_26);
		Remote_t660843562 * L_28 = ____tpValue0;
		// _tpValue.touchPad.right.isActive
		NullCheck(L_28);
		RemoteTouchPadInput_t1081319266 * L_29 = RemoteBase_get_touchPad_m3247253518(L_28, /*hidden argument*/NULL);
		// _tpValue.touchPad.right.isActive
		NullCheck(L_29);
		RemoteTouchInput_t3826108381 * L_30 = RemoteTouchPadInput_get_right_m4022269406(L_29, /*hidden argument*/NULL);
		// _tpValue.touchPad.right.isActive
		NullCheck(L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean RemoteTouchInput::get_isActive() */, L_30);
		// return new serializableBTRemoteTouchPad(
		serializableBTRemoteTouchPad_t2020069673 * L_32 = (serializableBTRemoteTouchPad_t2020069673 *)il2cpp_codegen_object_new(serializableBTRemoteTouchPad_t2020069673_il2cpp_TypeInfo_var);
		serializableBTRemoteTouchPad__ctor_m15664844(L_32, L_2, L_6, L_15, L_19, L_23, L_27, L_31, /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_008c;
	}

IL_008c:
	{
		// }
		serializableBTRemoteTouchPad_t2020069673 * L_33 = V_0;
		return L_33;
	}
}
// System.Void Utils.serializableFloat::.ctor(System.Single)
extern "C"  void serializableFloat__ctor_m3443302372 (serializableFloat_t3811907803 * __this, float ___rX0, const RuntimeMethod* method)
{
	{
		// public serializableFloat(float rX)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// x = rX;
		float L_0 = ___rX0;
		__this->set_x_0(L_0);
		// }
		return;
	}
}
// System.String Utils.serializableFloat::ToString()
extern "C"  String_t* serializableFloat_ToString_m4071320364 (serializableFloat_t3811907803 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableFloat_ToString_m4071320364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return String.Format("[{0}", x);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_1);
		// return String.Format("[{0}", x);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1367450909, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001c;
	}

IL_001c:
	{
		// }
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Single Utils.serializableFloat::op_Implicit(Utils.serializableFloat)
extern "C"  float serializableFloat_op_Implicit_m3618964296 (RuntimeObject * __this /* static, unused */, serializableFloat_t3811907803 * ___rValue0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// return float.Parse(rValue.x.ToString());
		serializableFloat_t3811907803 * L_0 = ___rValue0;
		NullCheck(L_0);
		float* L_1 = L_0->get_address_of_x_0();
		// return float.Parse(rValue.x.ToString());
		String_t* L_2 = Single_ToString_m1813392066(L_1, /*hidden argument*/NULL);
		// return float.Parse(rValue.x.ToString());
		float L_3 = Single_Parse_m1861732734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001d;
	}

IL_001d:
	{
		// }
		float L_4 = V_0;
		return L_4;
	}
}
// Utils.serializableFloat Utils.serializableFloat::op_Implicit(System.Single)
extern "C"  serializableFloat_t3811907803 * serializableFloat_op_Implicit_m3403528258 (RuntimeObject * __this /* static, unused */, float ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableFloat_op_Implicit_m3403528258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableFloat_t3811907803 * V_0 = NULL;
	{
		// return new serializableFloat(rValue);
		float L_0 = ___rValue0;
		// return new serializableFloat(rValue);
		serializableFloat_t3811907803 * L_1 = (serializableFloat_t3811907803 *)il2cpp_codegen_object_new(serializableFloat_t3811907803_il2cpp_TypeInfo_var);
		serializableFloat__ctor_m3443302372(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		// }
		serializableFloat_t3811907803 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Utils.serializableFromEditorMessage::.ctor()
extern "C"  void serializableFromEditorMessage__ctor_m2943508447 (serializableFromEditorMessage_t2894567809 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils.serializableGyroscope::.ctor(Utils.SerializableQuaternion,Utils.SerializableVector3)
extern "C"  void serializableGyroscope__ctor_m669537414 (serializableGyroscope_t1293559986 * __this, SerializableQuaternion_t3902400085  ___a0, SerializableVector3_t4294681249  ___uA1, const RuntimeMethod* method)
{
	{
		// public serializableGyroscope(SerializableQuaternion a, SerializableVector3 uA) {
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// attitude = a;
		SerializableQuaternion_t3902400085  L_0 = ___a0;
		__this->set_attitude_0(L_0);
		// userAcceleration = uA;
		SerializableVector3_t4294681249  L_1 = ___uA1;
		__this->set_userAcceleration_1(L_1);
		// }
		return;
	}
}
// Utils.serializableGyroscope Utils.serializableGyroscope::op_Implicit(UnityEngine.Gyroscope)
extern "C"  serializableGyroscope_t1293559986 * serializableGyroscope_op_Implicit_m1484759557 (RuntimeObject * __this /* static, unused */, Gyroscope_t1705362817 * ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableGyroscope_op_Implicit_m1484759557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableGyroscope_t1293559986 * V_0 = NULL;
	{
		// return new serializableGyroscope(rValue.attitude, rValue.userAcceleration);
		Gyroscope_t1705362817 * L_0 = ___rValue0;
		// return new serializableGyroscope(rValue.attitude, rValue.userAcceleration);
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Gyroscope_get_attitude_m2606076698(L_0, /*hidden argument*/NULL);
		// return new serializableGyroscope(rValue.attitude, rValue.userAcceleration);
		SerializableQuaternion_t3902400085  L_2 = SerializableQuaternion_op_Implicit_m1941277880(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Gyroscope_t1705362817 * L_3 = ___rValue0;
		// return new serializableGyroscope(rValue.attitude, rValue.userAcceleration);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Gyroscope_get_userAcceleration_m929621315(L_3, /*hidden argument*/NULL);
		// return new serializableGyroscope(rValue.attitude, rValue.userAcceleration);
		SerializableVector3_t4294681249  L_5 = SerializableVector3_op_Implicit_m2796142158(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// return new serializableGyroscope(rValue.attitude, rValue.userAcceleration);
		serializableGyroscope_t1293559986 * L_6 = (serializableGyroscope_t1293559986 *)il2cpp_codegen_object_new(serializableGyroscope_t1293559986_il2cpp_TypeInfo_var);
		serializableGyroscope__ctor_m669537414(L_6, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0022;
	}

IL_0022:
	{
		// }
		serializableGyroscope_t1293559986 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Utils.serializablePointCloud::.ctor(System.Byte[])
extern "C"  void serializablePointCloud__ctor_m2350325965 (serializablePointCloud_t1992421910 * __this, ByteU5BU5D_t3397334013* ___inputPoints0, const RuntimeMethod* method)
{
	{
		// public serializablePointCloud(byte [] inputPoints)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// pointCloudData = inputPoints;
		ByteU5BU5D_t3397334013* L_0 = ___inputPoints0;
		__this->set_pointCloudData_0(L_0);
		// }
		return;
	}
}
// Utils.serializablePointCloud Utils.serializablePointCloud::op_Implicit(UnityEngine.Vector3[])
extern "C"  serializablePointCloud_t1992421910 * serializablePointCloud_op_Implicit_m2562631572 (RuntimeObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___vecPointCloud0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializablePointCloud_op_Implicit_m2562631572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	serializablePointCloud_t1992421910 * V_3 = NULL;
	{
		// if (vecPointCloud != null)
		Vector3U5BU5D_t1172311765* L_0 = ___vecPointCloud0;
		if (!L_0)
		{
			goto IL_0093;
		}
	}
	{
		// byte [] createBuf = new byte[vecPointCloud.Length * sizeof(float) * 3];
		Vector3U5BU5D_t1172311765* L_1 = ___vecPointCloud0;
		NullCheck(L_1);
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))*(int32_t)4))*(int32_t)3))));
		// for(int i = 0; i < vecPointCloud.Length; i++)
		V_1 = 0;
		goto IL_007e;
	}

IL_001c:
	{
		// int bufferStart = i * 3;
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((int32_t)L_2*(int32_t)3));
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].x ), 0, createBuf, (bufferStart)*sizeof(float), sizeof(float) );
		Vector3U5BU5D_t1172311765* L_3 = ___vecPointCloud0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		float L_5 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))->get_x_1();
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].x ), 0, createBuf, (bufferStart)*sizeof(float), sizeof(float) );
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_6 = BitConverter_GetBytes_m4095372044(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_7 = V_0;
		int32_t L_8 = V_2;
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].x ), 0, createBuf, (bufferStart)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m1586717258(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_6, 0, (RuntimeArray *)(RuntimeArray *)L_7, ((int32_t)((int32_t)L_8*(int32_t)4)), 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].y ), 0, createBuf, (bufferStart+1)*sizeof(float), sizeof(float) );
		Vector3U5BU5D_t1172311765* L_9 = ___vecPointCloud0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		float L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_y_2();
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].y ), 0, createBuf, (bufferStart+1)*sizeof(float), sizeof(float) );
		ByteU5BU5D_t3397334013* L_12 = BitConverter_GetBytes_m4095372044(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_13 = V_0;
		int32_t L_14 = V_2;
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].y ), 0, createBuf, (bufferStart+1)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m1586717258(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_12, 0, (RuntimeArray *)(RuntimeArray *)L_13, ((int32_t)((int32_t)((int32_t)((int32_t)L_14+(int32_t)1))*(int32_t)4)), 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].z ), 0, createBuf, (bufferStart+2)*sizeof(float), sizeof(float) );
		Vector3U5BU5D_t1172311765* L_15 = ___vecPointCloud0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		float L_17 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->get_z_3();
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].z ), 0, createBuf, (bufferStart+2)*sizeof(float), sizeof(float) );
		ByteU5BU5D_t3397334013* L_18 = BitConverter_GetBytes_m4095372044(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_19 = V_0;
		int32_t L_20 = V_2;
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].z ), 0, createBuf, (bufferStart+2)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m1586717258(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_18, 0, (RuntimeArray *)(RuntimeArray *)L_19, ((int32_t)((int32_t)((int32_t)((int32_t)L_20+(int32_t)2))*(int32_t)4)), 4, /*hidden argument*/NULL);
		// for(int i = 0; i < vecPointCloud.Length; i++)
		int32_t L_21 = V_1;
		V_1 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_007e:
	{
		// for(int i = 0; i < vecPointCloud.Length; i++)
		int32_t L_22 = V_1;
		Vector3U5BU5D_t1172311765* L_23 = ___vecPointCloud0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		// return new serializablePointCloud (createBuf);
		ByteU5BU5D_t3397334013* L_24 = V_0;
		// return new serializablePointCloud (createBuf);
		serializablePointCloud_t1992421910 * L_25 = (serializablePointCloud_t1992421910 *)il2cpp_codegen_object_new(serializablePointCloud_t1992421910_il2cpp_TypeInfo_var);
		serializablePointCloud__ctor_m2350325965(L_25, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		goto IL_00a0;
	}

IL_0093:
	{
		// return new serializablePointCloud(null);
		// return new serializablePointCloud(null);
		serializablePointCloud_t1992421910 * L_26 = (serializablePointCloud_t1992421910 *)il2cpp_codegen_object_new(serializablePointCloud_t1992421910_il2cpp_TypeInfo_var);
		serializablePointCloud__ctor_m2350325965(L_26, (ByteU5BU5D_t3397334013*)(ByteU5BU5D_t3397334013*)NULL, /*hidden argument*/NULL);
		V_3 = L_26;
		goto IL_00a0;
	}

IL_00a0:
	{
		// }
		serializablePointCloud_t1992421910 * L_27 = V_3;
		return L_27;
	}
}
// UnityEngine.Vector3[] Utils.serializablePointCloud::op_Implicit(Utils.serializablePointCloud)
extern "C"  Vector3U5BU5D_t1172311765* serializablePointCloud_op_Implicit_m1111497770 (RuntimeObject * __this /* static, unused */, serializablePointCloud_t1992421910 * ___spc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializablePointCloud_op_Implicit_m1111497770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3U5BU5D_t1172311765* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3U5BU5D_t1172311765* V_4 = NULL;
	{
		// if (spc.pointCloudData != null)
		serializablePointCloud_t1992421910 * L_0 = ___spc0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_1 = L_0->get_pointCloudData_0();
		if (!L_1)
		{
			goto IL_0092;
		}
	}
	{
		// int numVectors = spc.pointCloudData.Length / (3 * sizeof(float));
		serializablePointCloud_t1992421910 * L_2 = ___spc0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = L_2->get_pointCloudData_0();
		NullCheck(L_3);
		V_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))/(int32_t)((int32_t)12)));
		// Vector3 [] pointCloudVec = new Vector3[numVectors];
		int32_t L_4 = V_0;
		V_1 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)L_4));
		// for (int i = 0; i < numVectors; i++)
		V_2 = 0;
		goto IL_0083;
	}

IL_0027:
	{
		// int bufferStart = i * 3;
		int32_t L_5 = V_2;
		V_3 = ((int32_t)((int32_t)L_5*(int32_t)3));
		// pointCloudVec [i].x = BitConverter.ToSingle (spc.pointCloudData, (bufferStart) * sizeof(float));
		Vector3U5BU5D_t1172311765* L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		serializablePointCloud_t1992421910 * L_8 = ___spc0;
		NullCheck(L_8);
		ByteU5BU5D_t3397334013* L_9 = L_8->get_pointCloudData_0();
		int32_t L_10 = V_3;
		// pointCloudVec [i].x = BitConverter.ToSingle (spc.pointCloudData, (bufferStart) * sizeof(float));
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		float L_11 = BitConverter_ToSingle_m159411893(NULL /*static, unused*/, L_9, ((int32_t)((int32_t)L_10*(int32_t)4)), /*hidden argument*/NULL);
		((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->set_x_1(L_11);
		// pointCloudVec [i].y = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+1) * sizeof(float));
		Vector3U5BU5D_t1172311765* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		serializablePointCloud_t1992421910 * L_14 = ___spc0;
		NullCheck(L_14);
		ByteU5BU5D_t3397334013* L_15 = L_14->get_pointCloudData_0();
		int32_t L_16 = V_3;
		// pointCloudVec [i].y = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+1) * sizeof(float));
		float L_17 = BitConverter_ToSingle_m159411893(NULL /*static, unused*/, L_15, ((int32_t)((int32_t)((int32_t)((int32_t)L_16+(int32_t)1))*(int32_t)4)), /*hidden argument*/NULL);
		((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->set_y_2(L_17);
		// pointCloudVec [i].z = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+2) * sizeof(float));
		Vector3U5BU5D_t1172311765* L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		serializablePointCloud_t1992421910 * L_20 = ___spc0;
		NullCheck(L_20);
		ByteU5BU5D_t3397334013* L_21 = L_20->get_pointCloudData_0();
		int32_t L_22 = V_3;
		// pointCloudVec [i].z = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+2) * sizeof(float));
		float L_23 = BitConverter_ToSingle_m159411893(NULL /*static, unused*/, L_21, ((int32_t)((int32_t)((int32_t)((int32_t)L_22+(int32_t)2))*(int32_t)4)), /*hidden argument*/NULL);
		((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->set_z_3(L_23);
		// for (int i = 0; i < numVectors; i++)
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0083:
	{
		// for (int i = 0; i < numVectors; i++)
		int32_t L_25 = V_2;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0027;
		}
	}
	{
		// return pointCloudVec;
		Vector3U5BU5D_t1172311765* L_27 = V_1;
		V_4 = L_27;
		goto IL_009b;
	}

IL_0092:
	{
		// return null;
		V_4 = (Vector3U5BU5D_t1172311765*)NULL;
		goto IL_009b;
	}

IL_009b:
	{
		// }
		Vector3U5BU5D_t1172311765* L_28 = V_4;
		return L_28;
	}
}
// System.Void Utils.SerializableQuaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SerializableQuaternion__ctor_m970837903 (SerializableQuaternion_t3902400085 * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method)
{
	{
		// x = rX;
		float L_0 = ___rX0;
		__this->set_x_0(L_0);
		// y = rY;
		float L_1 = ___rY1;
		__this->set_y_1(L_1);
		// z = rZ;
		float L_2 = ___rZ2;
		__this->set_z_2(L_2);
		// w = rW;
		float L_3 = ___rW3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
extern "C"  void SerializableQuaternion__ctor_m970837903_AdjustorThunk (RuntimeObject * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method)
{
	SerializableQuaternion_t3902400085 * _thisAdjusted = reinterpret_cast<SerializableQuaternion_t3902400085 *>(__this + 1);
	SerializableQuaternion__ctor_m970837903(_thisAdjusted, ___rX0, ___rY1, ___rZ2, ___rW3, method);
}
// System.String Utils.SerializableQuaternion::ToString()
extern "C"  String_t* SerializableQuaternion_ToString_m1532411480 (SerializableQuaternion_t3902400085 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableQuaternion_ToString_m1532411480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		// return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1742103562, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		// }
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* SerializableQuaternion_ToString_m1532411480_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SerializableQuaternion_t3902400085 * _thisAdjusted = reinterpret_cast<SerializableQuaternion_t3902400085 *>(__this + 1);
	return SerializableQuaternion_ToString_m1532411480(_thisAdjusted, method);
}
// UnityEngine.Quaternion Utils.SerializableQuaternion::op_Implicit(Utils.SerializableQuaternion)
extern "C"  Quaternion_t4030073918  SerializableQuaternion_op_Implicit_m929172930 (RuntimeObject * __this /* static, unused */, SerializableQuaternion_t3902400085  ___rValue0, const RuntimeMethod* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new Quaternion(rValue.x, rValue.y, rValue.z, rValue.w);
		float L_0 = (&___rValue0)->get_x_0();
		float L_1 = (&___rValue0)->get_y_1();
		float L_2 = (&___rValue0)->get_z_2();
		float L_3 = (&___rValue0)->get_w_3();
		// return new Quaternion(rValue.x, rValue.y, rValue.z, rValue.w);
		Quaternion_t4030073918  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Quaternion__ctor_m2707026792((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		Quaternion_t4030073918  L_5 = V_0;
		return L_5;
	}
}
// Utils.SerializableQuaternion Utils.SerializableQuaternion::op_Implicit(UnityEngine.Quaternion)
extern "C"  SerializableQuaternion_t3902400085  SerializableQuaternion_op_Implicit_m1941277880 (RuntimeObject * __this /* static, unused */, Quaternion_t4030073918  ___rValue0, const RuntimeMethod* method)
{
	SerializableQuaternion_t3902400085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new SerializableQuaternion(rValue.x, rValue.y, rValue.z, rValue.w);
		float L_0 = (&___rValue0)->get_x_0();
		float L_1 = (&___rValue0)->get_y_1();
		float L_2 = (&___rValue0)->get_z_2();
		float L_3 = (&___rValue0)->get_w_3();
		// return new SerializableQuaternion(rValue.x, rValue.y, rValue.z, rValue.w);
		SerializableQuaternion_t3902400085  L_4;
		memset(&L_4, 0, sizeof(L_4));
		SerializableQuaternion__ctor_m970837903((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		SerializableQuaternion_t3902400085  L_5 = V_0;
		return L_5;
	}
}
// System.Void Utils.serializableTransform::.ctor(Utils.SerializableVector3,Utils.SerializableQuaternion)
extern "C"  void serializableTransform__ctor_m1434546217 (serializableTransform_t2202979937 * __this, SerializableVector3_t4294681249  ___p0, SerializableQuaternion_t3902400085  ___r1, const RuntimeMethod* method)
{
	{
		// public serializableTransform(SerializableVector3 p, SerializableQuaternion r) {
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// position = p;
		SerializableVector3_t4294681249  L_0 = ___p0;
		__this->set_position_0(L_0);
		// rotation = r;
		SerializableQuaternion_t3902400085  L_1 = ___r1;
		__this->set_rotation_1(L_1);
		// }
		return;
	}
}
// Utils.serializableTransform Utils.serializableTransform::op_Implicit(UnityEngine.Transform)
extern "C"  serializableTransform_t2202979937 * serializableTransform_op_Implicit_m504541074 (RuntimeObject * __this /* static, unused */, Transform_t3275118058 * ____object0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableTransform_op_Implicit_m504541074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableTransform_t2202979937 * V_0 = NULL;
	{
		// return new serializableTransform(_object.position, _object.rotation);
		Transform_t3275118058 * L_0 = ____object0;
		// return new serializableTransform(_object.position, _object.rotation);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m2304215762(L_0, /*hidden argument*/NULL);
		// return new serializableTransform(_object.position, _object.rotation);
		SerializableVector3_t4294681249  L_2 = SerializableVector3_op_Implicit_m2796142158(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = ____object0;
		// return new serializableTransform(_object.position, _object.rotation);
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m2617026815(L_3, /*hidden argument*/NULL);
		// return new serializableTransform(_object.position, _object.rotation);
		SerializableQuaternion_t3902400085  L_5 = SerializableQuaternion_op_Implicit_m1941277880(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// return new serializableTransform(_object.position, _object.rotation);
		serializableTransform_t2202979937 * L_6 = (serializableTransform_t2202979937 *)il2cpp_codegen_object_new(serializableTransform_t2202979937_il2cpp_TypeInfo_var);
		serializableTransform__ctor_m1434546217(L_6, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0022;
	}

IL_0022:
	{
		// }
		serializableTransform_t2202979937 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Utils.SerializableVector2::.ctor(System.Single,System.Single)
extern "C"  void SerializableVector2__ctor_m429252694 (SerializableVector2_t4294681248 * __this, float ___rX0, float ___rY1, const RuntimeMethod* method)
{
	{
		// x = rX;
		float L_0 = ___rX0;
		__this->set_x_0(L_0);
		// y = rY;
		float L_1 = ___rY1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
extern "C"  void SerializableVector2__ctor_m429252694_AdjustorThunk (RuntimeObject * __this, float ___rX0, float ___rY1, const RuntimeMethod* method)
{
	SerializableVector2_t4294681248 * _thisAdjusted = reinterpret_cast<SerializableVector2_t4294681248 *>(__this + 1);
	SerializableVector2__ctor_m429252694(_thisAdjusted, ___rX0, ___rY1, method);
}
// System.String Utils.SerializableVector2::ToString()
extern "C"  String_t* SerializableVector2_ToString_m1702069295 (SerializableVector2_t4294681248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableVector2_ToString_m1702069295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return String.Format("[{0}, {1}]", x, y);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		// return String.Format("[{0}, {1}]", x, y);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1192795215, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		// }
		String_t* L_7 = V_0;
		return L_7;
	}
}
extern "C"  String_t* SerializableVector2_ToString_m1702069295_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SerializableVector2_t4294681248 * _thisAdjusted = reinterpret_cast<SerializableVector2_t4294681248 *>(__this + 1);
	return SerializableVector2_ToString_m1702069295(_thisAdjusted, method);
}
// UnityEngine.Vector2 Utils.SerializableVector2::op_Implicit(Utils.SerializableVector2)
extern "C"  Vector2_t2243707579  SerializableVector2_op_Implicit_m2411644775 (RuntimeObject * __this /* static, unused */, SerializableVector2_t4294681248  ___rValue0, const RuntimeMethod* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new Vector2(rValue.x, rValue.y);
		float L_0 = (&___rValue0)->get_x_0();
		float L_1 = (&___rValue0)->get_y_1();
		// return new Vector2(rValue.x, rValue.y);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m847450945((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
// Utils.SerializableVector2 Utils.SerializableVector2::op_Implicit(UnityEngine.Vector2)
extern "C"  SerializableVector2_t4294681248  SerializableVector2_op_Implicit_m3172164755 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  ___rValue0, const RuntimeMethod* method)
{
	SerializableVector2_t4294681248  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new SerializableVector2(rValue.x, rValue.y);
		float L_0 = (&___rValue0)->get_x_0();
		float L_1 = (&___rValue0)->get_y_1();
		// return new SerializableVector2(rValue.x, rValue.y);
		SerializableVector2_t4294681248  L_2;
		memset(&L_2, 0, sizeof(L_2));
		SerializableVector2__ctor_m429252694((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		SerializableVector2_t4294681248  L_3 = V_0;
		return L_3;
	}
}
// System.Void Utils.SerializableVector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void SerializableVector3__ctor_m618413250 (SerializableVector3_t4294681249 * __this, float ___rX0, float ___rY1, float ___rZ2, const RuntimeMethod* method)
{
	{
		// x = rX;
		float L_0 = ___rX0;
		__this->set_x_0(L_0);
		// y = rY;
		float L_1 = ___rY1;
		__this->set_y_1(L_1);
		// z = rZ;
		float L_2 = ___rZ2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
extern "C"  void SerializableVector3__ctor_m618413250_AdjustorThunk (RuntimeObject * __this, float ___rX0, float ___rY1, float ___rZ2, const RuntimeMethod* method)
{
	SerializableVector3_t4294681249 * _thisAdjusted = reinterpret_cast<SerializableVector3_t4294681249 *>(__this + 1);
	SerializableVector3__ctor_m618413250(_thisAdjusted, ___rX0, ___rY1, ___rZ2, method);
}
// System.String Utils.SerializableVector3::ToString()
extern "C"  String_t* SerializableVector3_ToString_m2915338350 (SerializableVector3_t4294681249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableVector3_ToString_m2915338350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return String.Format("[{0}, {1}, {2}]", x, y, z);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		float L_6 = __this->get_z_2();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_7);
		// return String.Format("[{0}, {1}, {2}]", x, y, z);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral30107939, L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0032;
	}

IL_0032:
	{
		// }
		String_t* L_10 = V_0;
		return L_10;
	}
}
extern "C"  String_t* SerializableVector3_ToString_m2915338350_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SerializableVector3_t4294681249 * _thisAdjusted = reinterpret_cast<SerializableVector3_t4294681249 *>(__this + 1);
	return SerializableVector3_ToString_m2915338350(_thisAdjusted, method);
}
// UnityEngine.Vector3 Utils.SerializableVector3::op_Implicit(Utils.SerializableVector3)
extern "C"  Vector3_t2243707580  SerializableVector3_op_Implicit_m2676771180 (RuntimeObject * __this /* static, unused */, SerializableVector3_t4294681249  ___rValue0, const RuntimeMethod* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new Vector3(rValue.x, rValue.y, rValue.z);
		float L_0 = (&___rValue0)->get_x_0();
		float L_1 = (&___rValue0)->get_y_1();
		float L_2 = (&___rValue0)->get_z_2();
		// return new Vector3(rValue.x, rValue.y, rValue.z);
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m1555724485((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		// }
		Vector3_t2243707580  L_4 = V_0;
		return L_4;
	}
}
// Utils.SerializableVector3 Utils.SerializableVector3::op_Implicit(UnityEngine.Vector3)
extern "C"  SerializableVector3_t4294681249  SerializableVector3_op_Implicit_m2796142158 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  ___rValue0, const RuntimeMethod* method)
{
	SerializableVector3_t4294681249  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new SerializableVector3(rValue.x, rValue.y, rValue.z);
		float L_0 = (&___rValue0)->get_x_1();
		float L_1 = (&___rValue0)->get_y_2();
		float L_2 = (&___rValue0)->get_z_3();
		// return new SerializableVector3(rValue.x, rValue.y, rValue.z);
		SerializableVector3_t4294681249  L_3;
		memset(&L_3, 0, sizeof(L_3));
		SerializableVector3__ctor_m618413250((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		// }
		SerializableVector3_t4294681249  L_4 = V_0;
		return L_4;
	}
}
// System.Void Utils.SerializableVector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SerializableVector4__ctor_m1161519484 (SerializableVector4_t4294681242 * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method)
{
	{
		// public SerializableVector4(float rX, float rY, float rZ, float rW)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// x = rX;
		float L_0 = ___rX0;
		__this->set_x_0(L_0);
		// y = rY;
		float L_1 = ___rY1;
		__this->set_y_1(L_1);
		// z = rZ;
		float L_2 = ___rZ2;
		__this->set_z_2(L_2);
		// w = rW;
		float L_3 = ___rW3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
// System.String Utils.SerializableVector4::ToString()
extern "C"  String_t* SerializableVector4_ToString_m137239981 (SerializableVector4_t4294681242 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableVector4_ToString_m137239981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		// return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1742103562, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		// }
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Vector4 Utils.SerializableVector4::op_Implicit(Utils.SerializableVector4)
extern "C"  Vector4_t2243707581  SerializableVector4_op_Implicit_m3701091973 (RuntimeObject * __this /* static, unused */, SerializableVector4_t4294681242 * ___rValue0, const RuntimeMethod* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new Vector4(rValue.x, rValue.y, rValue.z, rValue.w);
		SerializableVector4_t4294681242 * L_0 = ___rValue0;
		NullCheck(L_0);
		float L_1 = L_0->get_x_0();
		SerializableVector4_t4294681242 * L_2 = ___rValue0;
		NullCheck(L_2);
		float L_3 = L_2->get_y_1();
		SerializableVector4_t4294681242 * L_4 = ___rValue0;
		NullCheck(L_4);
		float L_5 = L_4->get_z_2();
		SerializableVector4_t4294681242 * L_6 = ___rValue0;
		NullCheck(L_6);
		float L_7 = L_6->get_w_3();
		// return new Vector4(rValue.x, rValue.y, rValue.z, rValue.w);
		Vector4_t2243707581  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m3879980823((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0024;
	}

IL_0024:
	{
		// }
		Vector4_t2243707581  L_9 = V_0;
		return L_9;
	}
}
// Utils.SerializableVector4 Utils.SerializableVector4::op_Implicit(UnityEngine.Vector4)
extern "C"  SerializableVector4_t4294681242 * SerializableVector4_op_Implicit_m2818398069 (RuntimeObject * __this /* static, unused */, Vector4_t2243707581  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableVector4_op_Implicit_m2818398069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SerializableVector4_t4294681242 * V_0 = NULL;
	{
		// return new SerializableVector4(rValue.x, rValue.y, rValue.z, rValue.w);
		float L_0 = (&___rValue0)->get_x_1();
		float L_1 = (&___rValue0)->get_y_2();
		float L_2 = (&___rValue0)->get_z_3();
		float L_3 = (&___rValue0)->get_w_4();
		// return new SerializableVector4(rValue.x, rValue.y, rValue.z, rValue.w);
		SerializableVector4_t4294681242 * L_4 = (SerializableVector4_t4294681242 *)il2cpp_codegen_object_new(SerializableVector4_t4294681242_il2cpp_TypeInfo_var);
		SerializableVector4__ctor_m1161519484(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		SerializableVector4_t4294681242 * L_5 = V_0;
		return L_5;
	}
}
// System.Void VirtualRemote::.ctor()
extern "C"  void VirtualRemote__ctor_m2680452056 (VirtualRemote_t4041918011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualRemote__ctor_m2680452056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal VirtualRemote()
		RemoteBase__ctor_m965004078(__this, /*hidden argument*/NULL);
		// this.menuButton = new RemoteButtonInput();
		// this.menuButton = new RemoteButtonInput();
		RemoteButtonInput_t144791130 * L_0 = (RemoteButtonInput_t144791130 *)il2cpp_codegen_object_new(RemoteButtonInput_t144791130_il2cpp_TypeInfo_var);
		RemoteButtonInput__ctor_m2755742895(L_0, /*hidden argument*/NULL);
		// this.menuButton = new RemoteButtonInput();
		RemoteBase_set_menuButton_m1026035502(__this, L_0, /*hidden argument*/NULL);
		// this.homeButton = new RemoteButtonInput();
		// this.homeButton = new RemoteButtonInput();
		RemoteButtonInput_t144791130 * L_1 = (RemoteButtonInput_t144791130 *)il2cpp_codegen_object_new(RemoteButtonInput_t144791130_il2cpp_TypeInfo_var);
		RemoteButtonInput__ctor_m2755742895(L_1, /*hidden argument*/NULL);
		// this.homeButton = new RemoteButtonInput();
		RemoteBase_set_homeButton_m3953498266(__this, L_1, /*hidden argument*/NULL);
		// this.trigger = new RemoteButtonInput();
		// this.trigger = new RemoteButtonInput();
		RemoteButtonInput_t144791130 * L_2 = (RemoteButtonInput_t144791130 *)il2cpp_codegen_object_new(RemoteButtonInput_t144791130_il2cpp_TypeInfo_var);
		RemoteButtonInput__ctor_m2755742895(L_2, /*hidden argument*/NULL);
		// this.trigger = new RemoteButtonInput();
		RemoteBase_set_trigger_m2574827387(__this, L_2, /*hidden argument*/NULL);
		// this.touchPad = new RemoteTouchPadInput();
		// this.touchPad = new RemoteTouchPadInput();
		RemoteTouchPadInput_t1081319266 * L_3 = (RemoteTouchPadInput_t1081319266 *)il2cpp_codegen_object_new(RemoteTouchPadInput_t1081319266_il2cpp_TypeInfo_var);
		RemoteTouchPadInput__ctor_m1266933877(L_3, /*hidden argument*/NULL);
		// this.touchPad = new RemoteTouchPadInput();
		RemoteBase_set_touchPad_m246351705(__this, L_3, /*hidden argument*/NULL);
		// this.motion = new RemoteMotionInput();
		// this.motion = new RemoteMotionInput();
		RemoteMotionInput_t3548926620 * L_4 = (RemoteMotionInput_t3548926620 *)il2cpp_codegen_object_new(RemoteMotionInput_t3548926620_il2cpp_TypeInfo_var);
		RemoteMotionInput__ctor_m983228931(L_4, /*hidden argument*/NULL);
		// this.motion = new RemoteMotionInput();
		RemoteBase_set_motion_m3159704669(__this, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VirtualRemote::UpdateVirtualMotion(Utils.serializableBTRemote)
extern "C"  void VirtualRemote_UpdateVirtualMotion_m1731264728 (VirtualRemote_t4041918011 * __this, serializableBTRemote_t622411689 * ___s_bt0, const RuntimeMethod* method)
{
	{
		// this.motion.acceleration.setMotionSensorVector(s_bt.acceleration);
		// this.motion.acceleration.setMotionSensorVector(s_bt.acceleration);
		RemoteMotionInput_t3548926620 * L_0 = RemoteBase_get_motion_m1992541006(__this, /*hidden argument*/NULL);
		// this.motion.acceleration.setMotionSensorVector(s_bt.acceleration);
		NullCheck(L_0);
		RemoteMotionSensorInput_t841531810 * L_1 = RemoteMotionInput_get_acceleration_m2249299093(L_0, /*hidden argument*/NULL);
		serializableBTRemote_t622411689 * L_2 = ___s_bt0;
		NullCheck(L_2);
		SerializableVector3_t4294681249  L_3 = L_2->get_acceleration_2();
		// this.motion.acceleration.setMotionSensorVector(s_bt.acceleration);
		Vector3_t2243707580  L_4 = SerializableVector3_op_Implicit_m2676771180(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// this.motion.acceleration.setMotionSensorVector(s_bt.acceleration);
		NullCheck(L_1);
		RemoteMotionSensorInput_setMotionSensorVector_m4116154331(L_1, L_4, /*hidden argument*/NULL);
		// this.motion.rotationRate.setMotionSensorVector(s_bt.rotationRate);
		// this.motion.rotationRate.setMotionSensorVector(s_bt.rotationRate);
		RemoteMotionInput_t3548926620 * L_5 = RemoteBase_get_motion_m1992541006(__this, /*hidden argument*/NULL);
		// this.motion.rotationRate.setMotionSensorVector(s_bt.rotationRate);
		NullCheck(L_5);
		RemoteMotionSensorInput_t841531810 * L_6 = RemoteMotionInput_get_rotationRate_m3968900193(L_5, /*hidden argument*/NULL);
		serializableBTRemote_t622411689 * L_7 = ___s_bt0;
		NullCheck(L_7);
		SerializableVector3_t4294681249  L_8 = L_7->get_rotationRate_1();
		// this.motion.rotationRate.setMotionSensorVector(s_bt.rotationRate);
		Vector3_t2243707580  L_9 = SerializableVector3_op_Implicit_m2676771180(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		// this.motion.rotationRate.setMotionSensorVector(s_bt.rotationRate);
		NullCheck(L_6);
		RemoteMotionSensorInput_setMotionSensorVector_m4116154331(L_6, L_9, /*hidden argument*/NULL);
		// this.motion.orientation.setOrientationVector(s_bt.orientation);
		// this.motion.orientation.setOrientationVector(s_bt.orientation);
		RemoteMotionInput_t3548926620 * L_10 = RemoteBase_get_motion_m1992541006(__this, /*hidden argument*/NULL);
		// this.motion.orientation.setOrientationVector(s_bt.orientation);
		NullCheck(L_10);
		RemoteOrientationInput_t3303200544 * L_11 = RemoteMotionInput_get_orientation_m4167617963(L_10, /*hidden argument*/NULL);
		serializableBTRemote_t622411689 * L_12 = ___s_bt0;
		NullCheck(L_12);
		SerializableVector3_t4294681249  L_13 = L_12->get_orientation_0();
		// this.motion.orientation.setOrientationVector(s_bt.orientation);
		Vector3_t2243707580  L_14 = SerializableVector3_op_Implicit_m2676771180(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		// this.motion.orientation.setOrientationVector(s_bt.orientation);
		NullCheck(L_11);
		RemoteOrientationInput_setOrientationVector_m131086611(L_11, L_14, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VirtualRemote::UpdateVirtualButtons(Utils.serializableBTRemoteButtons)
extern "C"  void VirtualRemote_UpdateVirtualButtons_m4034499078 (VirtualRemote_t4041918011 * __this, serializableBTRemoteButtons_t2327889448 * ___s_bt0, const RuntimeMethod* method)
{
	{
		// homeButton.isPressed = s_bt.startButton;
		// homeButton.isPressed = s_bt.startButton;
		RemoteButtonInput_t144791130 * L_0 = RemoteBase_get_homeButton_m2789731865(__this, /*hidden argument*/NULL);
		serializableBTRemoteButtons_t2327889448 * L_1 = ___s_bt0;
		NullCheck(L_1);
		bool L_2 = L_1->get_startButton_0();
		// homeButton.isPressed = s_bt.startButton;
		NullCheck(L_0);
		RemoteButtonInput_set_isPressed_m495854157(L_0, L_2, /*hidden argument*/NULL);
		// menuButton.isPressed = s_bt.backButton;
		// menuButton.isPressed = s_bt.backButton;
		RemoteButtonInput_t144791130 * L_3 = RemoteBase_get_menuButton_m604848353(__this, /*hidden argument*/NULL);
		serializableBTRemoteButtons_t2327889448 * L_4 = ___s_bt0;
		NullCheck(L_4);
		bool L_5 = L_4->get_backButton_1();
		// menuButton.isPressed = s_bt.backButton;
		NullCheck(L_3);
		RemoteButtonInput_set_isPressed_m495854157(L_3, L_5, /*hidden argument*/NULL);
		// trigger.isPressed = s_bt.triggerButton;
		// trigger.isPressed = s_bt.triggerButton;
		RemoteButtonInput_t144791130 * L_6 = RemoteBase_get_trigger_m2301973040(__this, /*hidden argument*/NULL);
		serializableBTRemoteButtons_t2327889448 * L_7 = ___s_bt0;
		NullCheck(L_7);
		bool L_8 = L_7->get_triggerButton_2();
		// trigger.isPressed = s_bt.triggerButton;
		NullCheck(L_6);
		RemoteButtonInput_set_isPressed_m495854157(L_6, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VirtualRemote::UpdateVirtualTouchpad(Utils.serializableBTRemoteTouchPad)
extern "C"  void VirtualRemote_UpdateVirtualTouchpad_m7135690 (VirtualRemote_t4041918011 * __this, serializableBTRemoteTouchPad_t2020069673 * ___s_bt0, const RuntimeMethod* method)
{
	{
		// this.touchPad.isActive = s_bt.touchActive;
		// this.touchPad.isActive = s_bt.touchActive;
		RemoteTouchPadInput_t1081319266 * L_0 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_1 = ___s_bt0;
		NullCheck(L_1);
		bool L_2 = L_1->get_touchActive_0();
		// this.touchPad.isActive = s_bt.touchActive;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void RemoteTouchInput::set_isActive(System.Boolean) */, L_0, L_2);
		// this.touchPad.button.isPressed = s_bt.touchButton;
		// this.touchPad.button.isPressed = s_bt.touchButton;
		RemoteTouchPadInput_t1081319266 * L_3 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.button.isPressed = s_bt.touchButton;
		NullCheck(L_3);
		RemoteButtonInput_t144791130 * L_4 = RemoteTouchPadInput_get_button_m341096981(L_3, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_5 = ___s_bt0;
		NullCheck(L_5);
		bool L_6 = L_5->get_touchButton_1();
		// this.touchPad.button.isPressed = s_bt.touchButton;
		NullCheck(L_4);
		RemoteButtonInput_set_isPressed_m495854157(L_4, L_6, /*hidden argument*/NULL);
		// this.touchPad.xAxis.value = s_bt.touchPos.x;
		// this.touchPad.xAxis.value = s_bt.touchPos.x;
		RemoteTouchPadInput_t1081319266 * L_7 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.xAxis.value = s_bt.touchPos.x;
		NullCheck(L_7);
		RemoteAxisInput_t2770128439 * L_8 = RemoteTouchPadInput_get_xAxis_m960950733(L_7, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_9 = ___s_bt0;
		NullCheck(L_9);
		SerializableVector2_t4294681248 * L_10 = L_9->get_address_of_touchPos_2();
		float L_11 = L_10->get_x_0();
		// this.touchPad.xAxis.value = s_bt.touchPos.x;
		NullCheck(L_8);
		RemoteAxisInput_set_value_m2783159073(L_8, L_11, /*hidden argument*/NULL);
		// this.touchPad.yAxis.value = s_bt.touchPos.y;
		// this.touchPad.yAxis.value = s_bt.touchPos.y;
		RemoteTouchPadInput_t1081319266 * L_12 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.yAxis.value = s_bt.touchPos.y;
		NullCheck(L_12);
		RemoteAxisInput_t2770128439 * L_13 = RemoteTouchPadInput_get_yAxis_m3985318738(L_12, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_14 = ___s_bt0;
		NullCheck(L_14);
		SerializableVector2_t4294681248 * L_15 = L_14->get_address_of_touchPos_2();
		float L_16 = L_15->get_y_1();
		// this.touchPad.yAxis.value = s_bt.touchPos.y;
		NullCheck(L_13);
		RemoteAxisInput_set_value_m2783159073(L_13, L_16, /*hidden argument*/NULL);
		// this.touchPad.up.isActive = s_bt.upButton;
		// this.touchPad.up.isActive = s_bt.upButton;
		RemoteTouchPadInput_t1081319266 * L_17 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.up.isActive = s_bt.upButton;
		NullCheck(L_17);
		RemoteTouchInput_t3826108381 * L_18 = RemoteTouchPadInput_get_up_m1922390201(L_17, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_19 = ___s_bt0;
		NullCheck(L_19);
		bool L_20 = L_19->get_upButton_3();
		// this.touchPad.up.isActive = s_bt.upButton;
		NullCheck(L_18);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void RemoteTouchInput::set_isActive(System.Boolean) */, L_18, L_20);
		// this.touchPad.down.isActive = s_bt.downButton;
		// this.touchPad.down.isActive = s_bt.downButton;
		RemoteTouchPadInput_t1081319266 * L_21 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.down.isActive = s_bt.downButton;
		NullCheck(L_21);
		RemoteTouchInput_t3826108381 * L_22 = RemoteTouchPadInput_get_down_m3468287018(L_21, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_23 = ___s_bt0;
		NullCheck(L_23);
		bool L_24 = L_23->get_downButton_4();
		// this.touchPad.down.isActive = s_bt.downButton;
		NullCheck(L_22);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void RemoteTouchInput::set_isActive(System.Boolean) */, L_22, L_24);
		// this.touchPad.left.isActive = s_bt.leftButton;
		// this.touchPad.left.isActive = s_bt.leftButton;
		RemoteTouchPadInput_t1081319266 * L_25 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.left.isActive = s_bt.leftButton;
		NullCheck(L_25);
		RemoteTouchInput_t3826108381 * L_26 = RemoteTouchPadInput_get_left_m1876375955(L_25, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_27 = ___s_bt0;
		NullCheck(L_27);
		bool L_28 = L_27->get_leftButton_5();
		// this.touchPad.left.isActive = s_bt.leftButton;
		NullCheck(L_26);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void RemoteTouchInput::set_isActive(System.Boolean) */, L_26, L_28);
		// this.touchPad.right.isActive = s_bt.rightButton;
		// this.touchPad.right.isActive = s_bt.rightButton;
		RemoteTouchPadInput_t1081319266 * L_29 = RemoteBase_get_touchPad_m3247253518(__this, /*hidden argument*/NULL);
		// this.touchPad.right.isActive = s_bt.rightButton;
		NullCheck(L_29);
		RemoteTouchInput_t3826108381 * L_30 = RemoteTouchPadInput_get_right_m4022269406(L_29, /*hidden argument*/NULL);
		serializableBTRemoteTouchPad_t2020069673 * L_31 = ___s_bt0;
		NullCheck(L_31);
		bool L_32 = L_31->get_rightButton_6();
		// this.touchPad.right.isActive = s_bt.rightButton;
		NullCheck(L_30);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void RemoteTouchInput::set_isActive(System.Boolean) */, L_30, L_32);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
